$("body").on("click",'.check-btn',function(){
    var connectValue = $(this).data('btn');
    var connectuserValue = $(this).data('user');
    localStorage.setItem('connect',connectValue); 
    localStorage.setItem('connectuser',connectuserValue); 
});


$("body").on("click",'#loginbtn',function(){
    deletelocalstorage();
});

$("body").on("click",'#signupbtn',function(){
    deletelocalstorage();
});

function deletelocalstorage() {
    localStorage.removeItem('connect');
    localStorage.removeItem('connectuser');
}
var base_url = $('#base_url_js').val();

//Registration
$("body").on("click", ".register-btn", function(e) {
    $('#register_post_div_success').hide();
    e.preventDefault();
    $('#loading').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $(this).parents('.formregister').find('input[name="_token"]').val()
        }
    });
    var parentClass = $(this).parents('.formregister');
    var token = $(this).parents('.formregister').find('input[name="_token"]').val();
    var ajaxUrl = base_url+"/lawyer/register";
    var inputemail = parentClass.find('input[name="email"]').val();
    var inputpassword = parentClass.find('input[name="password"]').val();
    $.ajax({
        url: ajaxUrl,
        method: 'POST',
        data: {
            _token: token,
            user_role_id: parentClass.find('input[name="user_role_id"]').val(),
            firstname: parentClass.find('input[name="firstname"]').val(),
            lastname: parentClass.find('input[name="lastname"]').val(),
            gender: parentClass.find('select[name="gender"]').val(),
            email: inputemail,
            phone: parentClass.find('input[name="phone"]').val(),
            password: inputpassword,
            password_confirmation: parentClass.find('input[name="password_confirmation"]').val(),
        },
        success: function (result) {           
            console.log(result);         
            parentClass.find('.alert-danger').hide(); 
            /*var res=result.split(' ');  
            if (res[0]=="Unverified") {
                $('#userverify').val('1');
            }else{
                $("#userverify").val('0');
            }*/
            $('#checkuser').val(result);
            parentClass[0].reset();
            $('#userverify').val('1');
            var lawyerid = localStorage.getItem('connectuser');
            var ajaxfrndUrl = base_url + "/social/profile/add_friend/" + lawyerid + "/" + result;
            if(localStorage.getItem('connect') == "connect"){
                add_friend(lawyerid,ajaxfrndUrl,token,'login','Unverified',parentClass);
            }else if(localStorage.getItem('connect') == "booking"){
                $('#loading').css('display','none');  
                var redirectURL = base_url + "/social/home/appointment/" + lawyerid;
                window.location = redirectURL;    
            } else if(localStorage.getItem('connect') == "legal-question"){
                $('#loading').css('display','none');  
                $("#legal-qbtn").trigger('click'); 
            } else{
                $('#loading').css('display','none');  
                $('#register_post_div_success').show();
                $('#register_post_div_success').html('You have been successfully registered. Please check your email to verify <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'register_post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>');
            } 
            $("#registerModal").animate({ scrollTop: 0 }, 500);
            // setTimeout(function(){
            //     $('.alert').hide();
            // },10000);
        },
        error: function (result) {  
            $('#loading').css('display','none');     
            if (result.status === 400 || result.status === 500) {
                var response = JSON.parse(result.responseText);
                $.each(response, function (key, val) {
                    parentClass.find('.alert-danger').show();
                    parentClass.find('.alert-success').hide();  
                    var div_id = parentClass.find('.alert-danger').attr('id');
                    parentClass.find('.alert-danger').html(all_values(val, div_id));
                    // $('html, body').animate({
                    //     scrollTop: parentClass.find('.alert-danger').offset().top - 120
                    // }, 500);
                    $("#registerModal").animate({ scrollTop: 0 }, 500);
                });
            } 
            // setTimeout(function(){
            //     $('.alert').hide();
            // },10000);
        }

    });
});

//Login
$("body").on("click", ".login-btn", function(e) {
    e.preventDefault();
    $('#loading').css('display','block');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $(this).parents('.formlogin').find('input[name="_token"]').val()
        }
    });
    var parentClass = $(this).parents('.formlogin');
    var token = $(this).parents('.formlogin').find('input[name="_token"]').val();
    var ajaxUrl = base_url+"/lawyer/login";
    $('.alert').hide();
    var inputemail = parentClass.find('input[name="email"]').val();
    var inputpassword = parentClass.find('input[name="password"]').val();
    $.ajax({
        url: ajaxUrl,
        method: 'POST',
        data: {
            _token: token,
            email: inputemail,
            caseheading: parentClass.find('input[name="caseheading"]').val(),
            casecontent: parentClass.find('input[name="casecontent"]').html(),
            previous_url: parentClass.find('input[name="previous_url"]').val(),
            password: inputpassword,
            postquestion: true
        },
        success: function (result) { 
            var login_success=false;   
            console.log(result);         
            var res=result.split(' ');
            parentClass.find('.alert-danger').hide();   
            if (res[0]=="Unverified") {
                $('#userverify').val('1');
                $('#loading').css('display','none'); 
                var div_id = parentClass.find('.alert-danger').attr('id');
                parentClass.find('.alert-danger').html(all_values({email:'Please verify your email address'}, div_id));
                // parentClass.find('.alert-danger').html('Please veify your email address');                        
                parentClass.find('.alert-danger').show();
                parentClass.find('.alert-success').hide(); 
                return false;
            }else{
                $("#userverify").val('0');
            }
            $('#checkuser').val(res[1]);
            var lawyerid = localStorage.getItem('connectuser');
            var ajaxfrndUrl = base_url + "/social/profile/add_friend/" + lawyerid + "/" + res[1];
            if(localStorage.getItem('connect') == "connect"){
                $('#loading').css('display','none'); 
                add_friend(lawyerid,ajaxfrndUrl,token,'login',res[0],parentClass);
                login_success = true;
            }else if(localStorage.getItem('connect') == "booking"){
                $('#loading').css('display','none'); 
                var redirectURL = base_url + "/social/home/appointment/" + lawyerid;
                window.location = redirectURL;    
            } else if(localStorage.getItem('connect') == "legal-question"){
                $('#loading').css('display','none'); 
                $("#legal-qbtn").trigger('click'); 
                login_success = true;
            } else{
                if (res[0]!="Unverified") {
                    $('#loading').css('display','none'); 
                    login_success=true;
                }
            } if (login_success) {                
                setTimeout(function(){
                    var login_message = "Please complete your profile.";
                    localStorage.setItem('login_message',login_message); 
                    window.location = base_url+'/social/login/pro?email='+inputemail+'&salt='+inputpassword;
                },2000);
            }
            $("#signInModal").animate({ scrollTop: 0 }, 500);
            // setTimeout(function(){
            //     $('.alert').hide();
            // },10000);     
        },
        error: function (result) {
            $('#loading').css('display','none');     
            if (result.status === 400 || result.status === 500) {
                var response = JSON.parse(result.responseText);
                $.each(response, function (key, val) {
                    parentClass.find('.alert-danger').show();
                    parentClass.find('.alert-success').hide();  
                    var div_id = parentClass.find('.alert-danger').attr('id');
                    parentClass.find('.alert-danger').html(all_values(val, div_id));
                    // $('html, body').animate({
                    //     scrollTop: parentClass.find('.alert-danger').offset().top - 120
                    // }, 500);
                    $("#signInModal").animate({ scrollTop: 0 }, 500);
                });
            }
            // setTimeout(function(){
            //     $('.alert').hide();
            // },10000);
        }

    });
});
/*
function add_friend(lawyerid,ajaxfrndUrl,token,action,result,parentClass) { 
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': token
              }
          });
    $.ajax({
        url: ajaxfrndUrl,
        type: 'GET',
        dataType : 'json',
        success: function(msg) {
            console.log(msg);
            if (msg.success) {
                parentClass.find('.alert-success').show();
                if (result=="Unverified") {
                    parentClass.find('.alert-success').html(msg.message+'Please check your given email inbox/spam folder to confirm signup <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'login_post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>');
                }else{
                    parentClass.find('.alert-success').html(msg.message+'<button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'login_post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>');
                    if (action=='login') {
                        var email = parentClass.find('input[name="email"]').val();
                        var password = parentClass.find('input[name="password"]').val();
                        var alreadylogin = $('#alreadylogin').val();
                        if (alreadylogin=='') {
                            setTimeout(function(){
                                var login_message = msg.message+" Please complete your profile.";
                                localStorage.setItem('login_message',login_message); 
                                window.location = '/lawsmos/social/login/pro?email='+email+'&salt='+password;
                            },2000);
                        }
                    }
                }
                $('#friend_button_' + lawyerid).html(msg.message);
                $('#friend_button_' + lawyerid).addClass("disabled");
            }else if (msg.error) {
                parentClass.find('.alert-danger').show();
                parentClass.find('.alert-danger').html(msg.error_msg+'<button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'login_post_div_error\')" aria-label="Close"><span aria-hidden="true">×</span></button> ');
            }
        },error: function(msg){
            console.log(msg);
        }
    })
}
*/

$('body').on('click','.user-role-select',function(){
    $('.alert').hide();
    $('.alert').html('');
});

$('body').on('click','a',function(){
    var formid = $(this).data('target');
    $(formid).find('form').trigger('reset');
});

$('body').on('click','.pass_show',function(){
    var pass_inp = $(this).parent('.input-group-addon').siblings('.signup_password').val();
    var pass_inp_type = $(this).parent('.input-group-addon').siblings('.signup_password').attr('type');
    if(pass_inp.length > 0){
        if(pass_inp_type == "password"){
            $(this).parent('.input-group-addon').siblings('.signup_password').attr("type", "text");
            $(this).addClass("fa-eye-slash");
        }
        else{
            $(this).parent('.input-group-addon').siblings('.signup_password').attr("type", "password");
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa fa-eye");
        }
    }  
    else{
        $(this).parent('.input-group-addon').siblings('.signup_password').attr("type", "password");
        $(this).removeClass("fa-eye-slash");
        $(this).addClass("fa fa-eye");
    }

});
