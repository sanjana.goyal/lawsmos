
var base_url = $('#base_url_js').val();
var tmp_base_url = base_url.replace("social","");
function getsubcat(category_id){
    $.ajax({
        url: tmp_base_url+"getSubCategory",
        type: "GET",
        data: "category_id="+category_id,
        success: function (response) {
            $("#subcategory").html(response);
            var subcategory_id = $("#sub_cat").val();
            $("#subcategory").find('option[value='+subcategory_id+']').prop("selected",true);
        },  
    });
}
$(document).ready(function(){

    CKEDITOR.replace('description');
    CKEDITOR.replace('articledescription');
    CKEDITOR.replace('researchdescription');
    CKEDITOR.replace('researchdescription2');
    var category_id = $("#cat").val();
    setTimeout(function(){ getsubcat(category_id) }, 500);
});


$("body").on("click",".reference-links",function(e){
    e.preventDefault();
    $(".reference-links").removeClass("active");
    $(this).addClass("active");
    var iframe = $("iframe").attr('src');
    var get_value = iframe.split("?");
    var category_id_article = $('#catagory').val();

    $("iframe").attr('src',get_value[0]+"?"+$(this).data('href')+"&test&category="+category_id_article);
});

$("body").on("click",".editor-links",function(e){
    e.preventDefault();
    $(".editor-links").removeClass("active");
    $(this).addClass("active");
    $(".description-sec").hide();
    $(".dropdown").html('');
    if ($(this).data("target")=="case-builder-sec") {
        $(".sub-cat-li").hide();
    }else{
        $(".sub-cat-li").show();

    }
    $("."+$(this).data('target')).find(".dropdown").html('<button class="dropdown-toggle save-sec" type="button" data-toggle="dropdown">Save<span class="caret"></span></button><ul class="dropdown-menu"><li class="save-opt"><a href="#" data-target="#savemodal" data-toggle="modal" class="saveopt">Save as draft</a></li><li class="save-opt"><a href="#" data-target="#savemodal" data-toggle="modal" class="saveopt">Publish</a></li></ul>');
    $("#save").attr('data-btn',$(this).data('target'));
    $("."+$(this).data('target')).show();
});

$("body").on("change","#catagory",function (e){
    e.preventDefault();
    var category_id_article = $(this).find('option:selected').val();   
    var iframe = $("iframe").attr('src');
    var get_value = iframe.split("?");
    var category_id = $('#catagory').val();
    getsubcat(category_id);
    $("iframe").attr('src',get_value[0]+"?"+$(".reference-links.active").data('href')+"&test&category="+category_id_article);
});  


$("body").on("click",".hide-sec",function(e){
    e.preventDefault();
    $(".des-border-right").toggleClass('collapse-width', 300);
    $(".iframe-sec").toggle(500);
    $(this).find('i').toggleClass('fa-angle-double-left').toggleClass('fa-angle-double-right');
    var text = $(this).find("span").text();
    if (text == "Hide reference") {
        $(".reference-list-ul").hide();
        $(this).find("span").text("Show reference");
    }else{
        $(".reference-list-ul").show();
        $(this).find("span").text("Hide reference");                     
    }
});
$(document).ready(function() {  
    var page = localStorage.getItem('page');
    $("#"+page).trigger("click");
    jQuery("body").on("click",'.saveopt',function(){
        $("#save").text($(this).text());
        $("#save_as").val($(this).text());
    });
    jQuery("body").on("click",'.add_btn',function(e){ 
        e.preventDefault();      
        $(this).parent(".scope-field").clone().insertAfter($(this).parent(".scope-field"));
        $(this).parent(".scope-field").find('input').val('');
        renamelabels();
    }); 
    jQuery("body").on("click",'.add_jist',function(e){ 
        e.preventDefault();      
        $(this).siblings(".jist-content").slideDown();
        $(this).removeClass('add_jist');
        $(this).addClass('remove_jist');
        $(this).html('<i class="fa fa-minus"></i> Remove Jist');
    }); 
    jQuery("body").on("click",'.remove_jist',function(e){ 
        e.preventDefault();      
        $(this).siblings(".jist-content").slideUp();
        $(this).siblings(".jist-content").val('');
        $(this).removeClass('remove_jist');
        $(this).addClass('add_jist');
        $(this).html('<i class="fa fa-plus"></i> Add Jist');
    }); 
    jQuery("body").on("click",'.remove_btn',function(e){ 
        e.preventDefault();      
        $(this).parent(".scope-field").remove();
        renamelabels();
    });    
    function renamelabels() {
        var num = 1;
        $(".scope-field label").each(function(){
            $(this).html('Scope '+num);
            if (num!=1) {
                $(this).siblings(".scope-btn").removeClass('add_btn');
                $(this).siblings(".scope-btn").addClass('remove_btn');
                $(this).siblings(".scope-btn").html('<i class="fa fa-minus"></i> Remove');
            }
            num++;
        });
    }
    jQuery("body").on("click",'#save',function(e){  
        $(".title-error-info").html("");
        $(".description-error-info").html("");
        $(".catagory-error-info").html("");
        $(".subcategory-error-info").html("");
        $(".keywords-error-info").html("");
        $(".metadescription-error-info").html("");
        $(".articledescription-error-info").html("");
        var title = $('#title').val();
        var save_as = $('#save_as').val();
        var catagory = $("#catagory").children("option:selected").val();
        var error = false;
        var csrf_token_name = $("#csrf_token_name").val();
        var csrf_hash = $("#csrf_hash").val();
        if( title == ""){
            error = true;
            $(".title-error-info").html("This field is required");
        }
        if(catagory == ""){
            error = true;
            $(".catagory-error-info").html("This field is required");                
            $("#savemodal").modal('hide');
        }
        if (save_as=="Save as draft") {
            save_as = "Draft";
        }
        if ($('#save').data('btn') == 'case-builder-sec') {
            var casecontent = $("#cke_description").find(".cke_wysiwyg_div").text();            
            if(casecontent.trim() == ""){
                error = true;
                $(".description-error-info").html("This field is required");               
                $("#savemodal").modal('hide');
            }
            if(error == true){
                return false;
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var urlid = $("#caseid").val();               
            var ajaxurl = $("#caseurl").val(); 
            jQuery.ajax({
                url: ajaxurl,
                method: 'POST',
                data: {
                    casetitle: title,
                    catagory: catagory,
                    casecontent: casecontent.trim(),
                    save_as: save_as,
                    caseid: urlid,
                    csrf_test_name: csrf_hash
                },
                dataType: "json",
                beforeSend: function() {
                    // setting a timeout
                    $("#save").addClass('loading');
                    $(".save-sec").addClass('loading');
                },
                success: function(result){
                    $("#save").removeClass('loading');
                    $(".save-sec").removeClass('loading');        
                    $("#savemodal").modal('hide');
                    console.log(result);
                    if(result=='failed') {
                        var htmltext = '<div class="alert alert-danger validation"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please <a href=\'{{url("lawyer/login")}}\'>login</a> or <a href=\'{{url("lawyer/register")}}\'>register</a> to save this case!</div>';
                        $('#content-container').html(htmltext);
                    } else {
                        $('#content-container').html('<div class="alert alert-success validation"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Case Saved Successfully...</div>');
                        $("#cke_description").find(".cke_wysiwyg_div").text('');
                        $("#catagory").children("option").removeAttr('selected');
                    }
                    setTimeout(function(){
                        $('#content-container').html('');
                    },5000);
                },error: function(res){
                    console.log(res);
                }
            });
        }else if ($('#save').data('btn') == 'article-sec') {
            var articlecontent = $("#cke_articledescription").find(".cke_wysiwyg_div").text();   
            var keywords = $('#keywords').val();
            var metadescription = $('#meta-description').val();         
            var subcategory = $('#subcategory').val();         
            if(articlecontent.trim() == ""){
                error = true;
                $(".articledescription-error-info").html("This field is required");               
                $("#savemodal").modal('hide');
            }
            if(keywords == ""){
                error = true;
                $(".keywords-error-info").html("Keywords cannot be empty.");        
                $("#savemodal").modal('hide');
            }if(metadescription == ""){
                error = true;
                $(".metadescription-error-info").html("Meta Description cannot be empty");        
                $("#savemodal").modal('hide');
            }if(subcategory == ""){
                error = true;
                $(".subcategory-error-info").html("Subcategory cannot be empty");        
                $("#savemodal").modal('hide');
            }
            if(error == true){
                return false;
            }

            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            //     }
            // });

            var urlid = $("#articleid").val();               
            var ajaxurl = $("#articleurl").val();             
            console.log(ajaxurl);                
            jQuery.ajax({
                url: ajaxurl,
                method: 'get',
                data: {
                    title: title,
                    cat: catagory,
                    subcat: subcategory,
                    keywords: keywords,
                    metadescription: metadescription,
                    description: articlecontent.trim(),
                    save_as: save_as,                        
                    articleid: urlid,
                },
                dataType: "json",
                beforeSend: function() {
                    // setting a timeout
                    $("#save").addClass('loading');
                    $(".save-sec").addClass('loading');
                },
                success: function(result){
                    $("#save").removeClass('loading');
                    $(".save-sec").removeClass('loading');        
                    $("#savemodal").modal('hide');
                    console.log(result);
                    if(result=='failed') {
                        var htmltext = '<div class="alert alert-danger validation"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please <a href=\'{{url("lawyer/login")}}\'>login</a> or <a href=\'{{url("lawyer/register")}}\'>register</a> to save this case!</div>';
                        $('#content-container').html(htmltext);
                    } else {
                        $('#content-container').html('<div class="alert alert-success validation"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Article Saved Successfully..</div>');
                        $("#cke_description").find(".cke_wysiwyg_div").text('');
                        $("#catagory").children("option").removeAttr('selected');
                    }
                    setTimeout(function(){
                        $('#content-container').html('');
                    },5000);
                },error: function(res){

                    console.log(res);
                }
            });
        }else if ($('#save').data('btn') == 'research-sec') {
            var researchcontent = $("#cke_researchdescription").find(".cke_wysiwyg_div").text();   
            var researchcontent2 = $("#cke_researchdescription2").find(".cke_wysiwyg_div").text(); 
            var scopetext = $(".scope-field").find(".scope");
            var jist = $(".jist-content").val();
            var scope = [];
            for(var i = 0; i < scopetext.length; i++){
                scope[i] = $(scopetext[i]).val();
            }
            var subcategory = $('#subcategory').val();         
            if(researchcontent.trim() == ""){
                error = true;
                $(".researchdescription-error-info").html("This field is required");        
                $("#savemodal").modal('hide');
            }else {
                // //exclude  start and end white-space
                // researchcontent = researchcontent.replace(/(^\s*)|(\s*$)/gi, "");
                // //convert 2 or more spaces to 1  
                // researchcontent = researchcontent.replace(/[ ]{2,}/gi, " ");
                // // exclude newline with a start spacing  
                // researchcontent = researchcontent.replace(/\n /, "\n");
                // var researchcontent_length = researchcontent.split(' ').length;
                // if (researchcontent_length < 300) {
                //     error = true;
                //     $(".researchdescription-error-info").html("Content must have min. 300 words.");
                // }
            }if(researchcontent2.trim() == ""){
                error = true;
                $(".researchdescription2-error-info").html("This field is required");        
                $("#savemodal").modal('hide');
            }if(subcategory == ""){
                error = true;
                $(".subcategory-error-info").html("Subcategory cannot be empty");        
                $("#savemodal").modal('hide');
            }
            if(error == true){
                return false;
            }

            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var urlid = $("#researchid").val();               
            var ajaxurl = $("#researchurl").val();   
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                data: {
                    title: title,
                    cat: catagory,
                    subcat: subcategory,
                    description: researchcontent.trim(),
                    description2: researchcontent2.trim(),
                    save_as: save_as,
                    scope : scope,
                    jist : jist,                   
                    researchid: urlid,
                    csrf_token_name: csrf_hash
                },
                dataType: "json",
                beforeSend: function() {
                    // setting a timeout
                    $("#save").addClass('loading');
                    $(".save-sec").addClass('loading');
                },
                success: function(result){
                    $("#save").removeClass('loading');
                    $(".save-sec").removeClass('loading');        
                    $("#savemodal").modal('hide');
                    console.log(result);
                    if(result.message=='failed') {
                        var htmltext = '<div class="alert alert-danger validation"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Please <a href=\'{{url("lawyer/login")}}\'>login</a> or <a href=\'{{url("lawyer/register")}}\'>register</a> to save this case!</div>';
                        $('#content-container').html(htmltext);
                    } else if(result.message=='success') {
                        $('#content-container').html('<div class="alert alert-success validation"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Research Saved Successfully..</div>');
                        $("#cke_description").find(".cke_wysiwyg_div").text('');
                        $("#catagory").children("option").removeAttr('selected');
                        window.location.href = result.url;
                    }
                    setTimeout(function(){
                        $('#content-container').html('');
                    },5000);
                }
            });
        }
    });
});
   
$("iframe").on("load",function(){
    $('iframe').contents().find(".header-area-wrapper").remove();
    $('iframe').contents().find(".ls-spacer").remove();
    $('iframe').contents().find(".footer-area-wrapper").remove();
});