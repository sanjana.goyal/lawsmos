<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Actchapters extends Model
{
	protected $table = SOCIAL_DB_NAME.'.act_details';
    protected $fillable = ['id','act_id','chapter','heading'];
    public $timestamps = false;
}
