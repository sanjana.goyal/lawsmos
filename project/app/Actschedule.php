<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Actschedule extends Model
{
	protected $table = SOCIAL_DB_NAME.'.schedules';
    protected $fillable = ['id','act_id','heading','subheading','description'];
    public $timestamps = false;
}
