<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Actsections extends Model
{
	protected $table = SOCIAL_DB_NAME.'.sections';
    protected $fillable = ['id','act_id','chapter_id','section','description'];
    public $timestamps = false;
}
