<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Agency extends Model
{
	
	protected $table = SOCIAL_DB_NAME.'.agency';
    protected $fillable = ['agency_name','contact_name','email','phone','city','address','permanentAddr','registration_number','registration_proof','lat','logitude','LatitudeLongitude'];
    public $timestamps = false;
}
