<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Bareacts extends Model
{
	protected $table = SOCIAL_DB_NAME.'.acts';
    protected $fillable = ['id','title','description','link','created_at'];
    public $timestamps = false;
}
