<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Blog extends Model
{
    protected $fillable = ['created_by_user','title', 'details', 'photo', 'source', 'views', 'created_at', 'updated_at', 'status'];
	 public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('article_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
