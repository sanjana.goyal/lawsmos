<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Caseeditor extends Model
{
	
	protected $table = SOCIAL_DB_NAME.'.case_editor';
    protected $fillable = ['id','loggged_user_id','case_heading','case_body','case_category','created_at','last_updated','updated_at'];
    public $timestamps = false;
    
    
    public function socialuser(){
		return $this->belongsTo('App\Usersocial');
		
		}
}
