<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
    protected $fillable = ['cid','cat_name', 'cat_slug','photo','colors'];
    public $timestamps = false;

    public function users()
    {
    	return $this->hasMany('App\User');
    }
    public function qausers(){
		
		return $this->hasMany('App\Qausers');
		}
}
