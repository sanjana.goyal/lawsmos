<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Comments extends Model
{
    //
    protected $fillable = ['article_id','messagebody', 'user_id', 'status', 'created_at', 'updated_at'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
