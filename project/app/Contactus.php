<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Contactus extends Model
{
	protected $table = SOCIAL_DB_NAME.'.contactus';
    protected $fillable = ['ID','name','email','location','phone','message','created_at','updated_at'];
    public $timestamps = false;
}
