<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Firms extends Model
{
    //
     protected $fillable = ['name', 'slug', 'categoryid', 'type', 'pageviews', 'likes', 'profile_header', 'description', 'profile_avatar','posting_status','location','address','lat','logitude','LatitudeLongitude','email','phone','website','nonmembers_view','members','is_active'];


}
