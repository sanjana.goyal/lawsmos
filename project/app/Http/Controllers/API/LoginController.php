<?php

namespace App\Http\Controllers\API;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\crypt;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\User;
use App\Usersocial;
use App\Userqa;
use Validator;
use Laravel\Passport\Passport;

use Laravel\Passport\PassportServiceProvider;

use App\Http\Controllers\API\BaseController as BaseController;



class LoginController extends BaseController
{
    
    public function login(Request $request)
    {
     
        $validator = Validator::make($request->all(), [
            
            'email' => 'required|email',
            'password' => 'required',
           
        ]);


        if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());       
        }

		
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $success = "user";

		$error="failed";
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
          
            $user = Auth::user();
            
            //print_r($user);
           
             $success =  $user->createToken('MyApp')-> accessToken; 
            return $this->sendResponse($success, 'User Login successfully.');
        }else{
			
				return $this->sendError($error, 'User Login Failed.');
			
			}

     /*   $user = Auth::user(); 
        $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        return response()->json(['success' => $success], $this-> successStatus);
	*/
        //return $this->sendResponse($success, 'User login successfully.');
        //return response()->json(['error'=>'Unauthorised'], 401);
    }
    

}
