<?php

namespace App\Http\Controllers\API;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\crypt;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\User;
use App\Usersocial;
use App\Userqa;
use Validator;
use Laravel\Passport\Passport;

use Laravel\Passport\PassportServiceProvider;

use App\Http\Controllers\API\BaseController as BaseController;



class RegisterController extends BaseController
{
    
    public function register(Request $request)
    {
     
     
		
       
     
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'role' => 'required',
        ]);


        if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());       
        }

		
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['name'] = $input['name'];
        $input['username'] = $input['name'].rand(10,100);
        $input['user_role_id'] = $input['role'];
        
        
        $inputsocial=$request->all();
        
        $name=$input['name'];
        $nam=explode(" ",$name);
        
        $firstname=$nam[0];
        $lastname=$nam[1];
        
        if($input['role']==0){
			
				$markerid="Advocate";
			}
		else if($input['role']==1){
			
			$markerid="Seeker";
			
			}
		else if($input['role']==2)
		{
			
		$markerid="Student";
		}
		else if($input['role']==3)
		{
		$markerid="Professor";
		
		}
        
        
        $inputsocial['email']=$input['email'];
        $inputsocial['password']=bcrypt($input['password']);
        $inputsocial['username']=$input['username'];
        $inputsocial['user_role']=$input['role'];
        $inputsocial['first_name']=$firstname;
        $inputsocial['last_name']=$lastname;
        $inputsocial['profile_identification']=$input['role'];
        $inputsocial['MarkerId']=$markerid;
        
        
        
        
        
        $user = User::create($input);
        $usersocial = Usersocial::create($inputsocial);
        
        
        //$success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
		 $success['token'] =  $user->createToken('MyApp')-> accessToken; 

        return $this->sendResponse($success, 'User register successfully.');
    }
    

}
