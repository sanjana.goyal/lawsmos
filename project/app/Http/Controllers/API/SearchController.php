<?php

namespace App\Http\Controllers\API;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\crypt;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\User;
use App\Usersocial;
use App\Userqa;
use App\Agency;
use App\Qaposts;
use App\Judgement;

use Validator;

use App\Http\Controllers\API\BaseController as BaseController;



class SearchController extends BaseController
{
    
    public function search(Request $request)
    {
     
        $validator = Validator::make($request->all(), [
            
            'city' => 'required',
            'catagory' => 'required',
           
        ]);


        if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());       
        }
	
		$user = User::where('category_id','LIKE','%'.$request['catagory'].'%')->Where('city','LIKE','%'.$request['city'].'%')->get();
         
        $agency=Agency::where('city','LIKE','%'.$request['city'].'%')->get();
		$merged =  $agency->merge($user);
		$result = $merged->all();
 		return $result;
   	   
    }
    
		public function lastthree()
		{
		
		
		$questions = Qaposts::orderBy('postid', 'desc')->where("status", 1)->take(3)->get();
		
		print_r($questions);
		
		}
    
	public function searchjudgements(Request $request)
	{
	
		$validator = Validator::make($request->all(), [
            
            'court' => 'required',
            
           
        ]);
		
	
		$Judgement = Judgement::where('statecode','LIKE','%'.$request['court'].'%')->orWhere('year','LIKE','%'.$request['year'].'%')->orWhere('heading','LIKE','%'.$request['heading'].'%')->get();
         
		
		print_r($Judgement);
		
		
	}

}
