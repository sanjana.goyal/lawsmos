<?php
namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Caseeditor;

class UserLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:user', ['except' => ['logout']]);
    }

 	public function showLoginForm()
    {
      return view('user.login');
    }


	public function q2alogin(Request $request){
		
			echo "hello gupa ";
			$user = Auth::guard('user')->user();
			echo $email=$request->email;
			echo $password=$request->password;
			exit;
			
		    return redirect(url("qa/index.php?qa=login&to=index.php&email=$email"));
		}
   
   
    public function login(Request $request)
    {
		
		
		// Validate the form data		
		$this->validate($request,[
		    'email' => 'required|email',
		    'password' => 'required',
		]);
      // Attempt to log the user in
      if (Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password])) {
        // if successful, then redirect to their intended location
        //return redirect()->intended(route('user-dashboard'));
		$user = \Auth::guard('user')->user();
		if(!empty($request['caseheading'] || !(empty($request['casecontent'])))):
		$caseeditor = new Caseeditor();
		$caseeditor->loggged_user_id =  $user->id;
		$caseeditor->case_heading =  $request['caseheading'];
		$caseeditor->case_body = $request['casecontent'];
		$caseeditor->save(); 
		endif;		
       if (strpos($request->previous_url,'profile') !== false) 
	   {	
			//return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.'&sessionurl='.$request->previous_url.''));
			return redirect(url("social/login/pro?email=$request->email&salt=$request->password&sessionurl=$request->previous_url"));
		} 
		else if($request->previous_urlss==='postquestion')
		{
			return redirect(url("social/login/pro?email=$request->email&salt=$request->password&sessionurl=$request->previous_urlss"));
			//return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.'&sessionurlsss='.$request->previous_urlss.''));
			
		}
		else 
		{	
			return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
			 //return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.''));
		//	return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.'&sessionurl='.$request->previous_url.''));
		}
		
     return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
     //  return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.''));
        //return redirect($request->previous_url);
       // header("location: ".$request->previous_url);
      }
      // if unsuccessful, then redirect back to the login with the form data
      Session::flash('message',"f");
      return redirect()->back()->withInput($request->only('email'));
    
    }
    
    
    public function logout()
    {

		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				if($name!='cookielon' && $name!='cookielocation' && $name!='cookielat') {
						setcookie($name, '', time()-1000);
						setcookie($name, '', time()-1000, '/');
				}
			}
		}
        Auth::guard('user')->logout();
        return redirect('/');
    }    
}
