<?php
	namespace App\Http\Controllers\Auth;
	use Auth;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Session;
	use App\Caseeditor;
	class UserLoginController extends Controller {
	    public function __construct() {
	    	if(session('message')==='user_verified'){
	    		Session::flash('message', "user_verified");
	    	}

	        $this->middleware('guest:user', ['except' => ['logout']]);
	    }
	    public function showLoginForm() {
	        $userkeyarray = '';
	        return view('user.login', compact('userkeyarray'));
	    }
	    // public function showLoginFormWithVerification($id) {
	    //     $users = \DB::table(LAWYERS_DB_NAME . '.users')->get()->where('id', $id);
	    //     $userkeyarray = $users->toArray();
	    //     if ($userkeyarray) {
	    //         $userkey = array_keys($userkeyarray);
	    //         $user_verified = $users[$userkey[0]]->user_verified;
	    //         $user_id = $users[$userkey[0]]->id;
	    //         $user_email = $users[$userkey[0]]->id;
	    //         if ($user_verified == 1) {
	    //         	Session::flash('message', "user_verified");
	    //         }
	            
	    //     }
	    //     return redirect(url('lawyer/login'));
	    // }
	    public function q2alogin(Request $request) {
	        echo "hello gupa ";
	        $user = Auth::guard('user')->user();
	        echo $email = $request->email;
	        echo $password = $request->password;
	        exit;
	        return redirect(url("qa/index.php?qa=login&to=index.php&email=$email"));
	    }
	    public function login(Request $request) {
	        // Validate the form data
	        $validator = \Validator::make($request->all(), ['email' => 'required|email', 'password' => 'required', ]);
	        if ($validator->fails()) {
		        return response()->json(['failed' => $validator->getMessageBag()->toArray() ], 400);
		    }
	        $users = \DB::table(LAWYERS_DB_NAME . '.users')->get()->where('email', $request['email']);
	        $userkeyarray = $users->toArray();
	        if ($userkeyarray) {
	            $userkey = array_keys($userkeyarray);
	            $user_verified = $users[$userkey[0]]->user_verified;
	            $user_id = $users[$userkey[0]]->id;
	            if ($user_verified == 0) {
	            	if (isset($request['postquestion'])) {
	            		return "Unverified ".$user_id;exit();
	            	}
	            	Session::flash('message', "user_verify");
	        		return redirect()->back()->withInput($request->only('email'));
	            }
	            
	        }
	        // Attempt to log the user in
	        if (Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password])) {
	            // if successful, then redirect to their intended location
	            //return redirect()->intended(route('user-dashboard'));
	            $user = \Auth::guard('user')->user();
	            if (!empty($request['caseheading'] || !(empty($request['casecontent'])))):
	                $caseeditor = new Caseeditor();
	                $caseeditor->loggged_user_id = $user->id;
	                $caseeditor->case_heading = $request['caseheading'];
	                $caseeditor->case_body = $request['casecontent'];
	                $caseeditor->save();
	            endif;

            	if (isset($request['postquestion'])) {
	            	return "Verified ".$user_id;exit();
            	}

	            if (strpos($request->previous_url, 'profile') !== false) {
	                //return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.'&sessionurl='.$request->previous_url.''));
	                return redirect(url("social/login/pro?email=$request->email&salt=$request->password&sessionurl=$request->previous_url"));
	            } else if ($request->previous_urlss === 'postquestion') {
	                return redirect(url("social/login/pro?email=$request->email&salt=$request->password&sessionurl=$request->previous_urlss"));
	            } else {
	                //return redirect("http://localhost/lawsmos/social/login/pro?email=$request->email&salt=$request->password");
	                return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
	                //	return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.'&sessionurl='.$request->previous_url.''));
	                
	            }
	            return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
	            //  return redirect(url('social/login/pro?email='.$request->email.'&salt='.$request->password.''));
	            //return redirect($request->previous_url);
	            // header("location: ".$request->previous_url);
	            
	        }elseif (isset($request['postquestion'])) {
	           return response()->json(['failed' => ['Emails or password not correct!'] ], 400);
        	}
	        // if unsuccessful, then redirect back to the login with the form data
	        Session::flash('message', "f");
	        return redirect()->back()->withInput($request->only('email'));
	    }
	    public function login_ajax_check_verification(Request $request) {
	        // Validate the form data
	        $return['error'] = '';
	        $this->validate($request, ['email' => 'required|email', 'password' => 'required', ]);
	        $users = \DB::table(LAWYERS_DB_NAME . '.users')->get()->where('email', $request['email']);
	        $userkeyarray = $users->toArray();
	        if ($userkeyarray) {
	            $userkey = array_keys($userkeyarray);
	            $user_verified = $users[$userkey[0]]->user_verified;
	            $user_id = $users[$userkey[0]]->id;
	            if ($user_verified == 0) {
					$return['error'] = "Please check your email to verify your account.";
				}
	            
	        }					
			echo json_encode($return);
			exit();
	    }
	    public function logout() {
	        if (isset($_SERVER['HTTP_COOKIE'])) {
	            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
	            foreach ($cookies as $cookie) {
	                $parts = explode('=', $cookie);
	                $name = trim($parts[0]);
	                setcookie($name, '', time() - 1000);
	                setcookie($name, '', time() - 1000, '/');
	            }
	        }
	        Auth::guard('user')->logout();
	        return redirect('/');
	    }
	}
?>