<?php
namespace App\Http\Controllers\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\crypt;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\Caseeditor;
use App\User;
use App\Usersocial;
use App\Qausers;
use App\Rewardpoints;
use Validator;


class UserRegisterController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:user', ['except' => ['logout']]);
      
    }


 	public function showRegisterForm(Request $request)
    {
		
		$faqs = \DB::table(SOCIAL_DB_NAME.'.faq')->get()->toArray();
		
	
		$cats = Category::all()->where("parent_id",0);
		
		
		return view('user.register',compact('cats','faqs'));
    }
    public function showAdvocateRegisterForm($id,$name,$email){
		 $posts = \DB::table(SOCIAL_DB_NAME.'.pages')->get()->where('ID', $id);
		 $posts=$posts->toArray();
		 $key=array_keys($posts);
	     $ky=$key[0];
		 $firmname= $posts[$ky]->name;
     	 $first=explode(" ",$name);
   		 $firstname=$first[0];
		if(isset($first[1])){
			$lastname=$first[1];
	    }
	    else{
	    $lastname=""; 
		}
		$data=array('pageid'=>$id, 'firstname'=>$firstname,'lastname'=>$lastname, 'email'=>$email,'firmname'=>$firmname);
		$cats = Category::all();
		return view('user.registerform',compact('cats'))->with($data);
		}

		// public function userVerification($userid, $token)
		// {
		// 	$users = \DB::table(LAWYERS_DB_NAME.'.users')->get()->where('id', $userid);
		// 	$userkeyarray=$users->toArray();
		// 	$userkey=array_keys($userkeyarray);
		// 	$user_email=$users[$userkey[0]]->email;
		// 	$user_password=$users[$userkey[0]]->password;
		// 	$update = \DB::table(LAWYERS_DB_NAME.'.users')->where('ID', $userid)->update( [ 'user_verified' => 1 , 'verification_token'=>'']);
		// 	return redirect(url("lawyer/login/".$userid));
		// }
		
public function registerlink(Request $request){
      $this->validate($request, [
        'email'   => 'required|email|unique:users',
        'password' => 'required|confirmed',
        'firstname'=> 'required|alpha',
      ]);
       
       
       //register in laravel
        $user = new User;
        $input = $request->all();        
        $input['password'] = bcrypt($request['password']);
        $input['user_role_id']=0;
        $input['username']=$request['firstname'].$request['lastname'].rand(10,100);
		$input['name']=$request['firstname']." ".$request['lastname'];
		$input['source']="invite";
       
        $user->fill($input)->save();
        
        //register in social
        $socialuser= new Usersocial;
        $socialinput['ID']=$user['id'];   
        $socialinput['password']=bcrypt($request['password']);
		$socialinput['first_name']=$request['firstname'];
		$socialinput['username']=$input['username'];
		$socialinput['last_name']=$request['lastname'];
		$socialinput['email']=$request['email'];
		$socialinput['source']="registerlink";
		$socialinput['points']=500;
		$socialinput['bonus_points']=500;
		$socialinput['total_points']=500;
		$socialinput['ID']=$user['id'];
		$socialinput['profile_identification']=0;
		$socialinput['user_role']=5;
		$socialinput['IP']=request()->ip();
		
		
		//register in QA
        $Qausers= new Qausers;
        $qainput['passhash']=bcrypt($request['password']);
		$qainput['handle']=$input['username'];
		$qainput['email']=$request['email'];
		$qainput['level']=0;
		$qainput['userid']=$user['id'];
		$Qausers->fill($qainput)->save();

		//Update Reward points history
        $rewardpoints= new Rewardpoints;
		$rewardpointsinput['notes']="Registration";
		$rewardpointsinput['points']=500;
		$rewardpointsinput['userid']=$user['id'];
		$rewardpoints->fill($rewardpointsinput)->save();
	     
       // Auth::guard('user')->login($user); 

        /*
        $source="registerlink";
        $ch = curl_init();
        $data2 = 'source='.$source;
		curl_setopt($ch, CURLOPT_URL, url("api/registrationSocialApi.php"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);
		curl_exec($ch);
		curl_close($ch);
		*/
		$posts = \DB::table(SOCIAL_DB_NAME.'.users')->get()->where('email', $input['email']);
		$postkeyarray=$posts->toArray();
		$postkey=array_keys($postkeyarray);
		$userid=$posts[$postkey[0]]->ID;
		$values = array('pageid' =>  $input['firmid'],'userid' => $userid,'roleid'=>0);
		\DB::table(SOCIAL_DB_NAME.'.page_users')->insert($values);
		$update = \DB::table(SOCIAL_DB_NAME.'.users') ->where('ID', $userid)->update( [ 'is_agency_joined' => 1 , 'agency_id'=>$input['firmid']]);
      
      
		 //sending email
        
        $to_name = $request['firstname'];
		$to_email =$request['email'];
		$data = array("name"=>$request['firstname'],"username"=>$request['email'],"password"=>$request['password']);
        
       
        \Mail::send("email.register", $data, function($message) use ($to_name, $to_email) {
			
			$message->to($to_email, $to_name)->subject("Lawsmos registration");
		
        });
      
      
       return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
		}
		
		
   	
   	public function register1(Request $request) {
	    $validator = \Validator::make($request->all(), [
	    	'firstname' => 'required',
	    	'lastname' => 'required',
	    	'gender' => 'required',
	    	'email' => 'required|email|unique:users',
	    	'phone' => 'required|min:10|numeric',
	    	'password' => 'required|confirmed',
	    ],
	    [
	    	'firstname.required' => 'The first name field is required',
	    	'lastname.required' => 'The last name field is required',
	    	'gender.required' => 'The gender field is required',
	    	'email.required' => 'The email field is required',
	    	'email.email' => 'The email field has invalid value',
	    	'email.unique' => 'The Email already has been taken',
	    	'phone.required' => 'The phone field is required',
	    	'phone.phone' => 'The phone field has invalid value',
	    	'password.required' => 'The password field is required',
	    		'password.confirmed' => 'The Password confirmation doesn\'t match'
	    ]);
	    if ($validator->fails()) {
	        return response()->json(['failed' => $validator->getMessageBag()->toArray() ], 400);
	    }
	    //register in laravel
	    $user = new User;
	    $token = rand(1, 100000) . $request['email'];
	    $token = md5(sha1($token));
	    $input = $request->all();
	    $input['password'] = bcrypt($request['password']);
	    $input['user_role_id'] = $request['user_role_id'];
	    $input['verification_token'] = $token;
	    $input['phone'] = $request['phone'];
	    $input['username'] = $request['firstname'] . $request['lastname'] . rand(10, 100);
	    $input['name'] = $request['firstname'] . " " . $request['lastname'];
	    $input['source'] = "direct";
	    $user->fill($input)->save();
	    //register in social
	    $socialuser = new Usersocial;
	    $socialinput['ID'] = $user['id'];
	    $socialinput['password'] = bcrypt($request['password']);
	    $socialinput['first_name'] = $request['firstname'];
	    $socialinput['username'] = $input['username'];
	    $socialinput['last_name'] = $request['lastname'];
	    $socialinput['email'] = $request['email'];
	    $socialinput['source'] = "Direct";
	    $socialinput['ID'] = $user['id'];
	    $socialinput['verification_token'] = $token;
	    $socialinput['phone'] = $request['phone'];
	    $socialinput['profile_identification'] = $request['user_role_id'];
	    $socialinput['user_role'] = 5;
	    $socialinput['IP'] = request()->ip();
	    $socialinput['gender'] = $request['gender'];
	    $socialuser->fill($socialinput)->save();
	    //register in QA
	    $qausers = new Qausers;
	    $qainput['passhash'] = bcrypt($request['password']);
	    $qainput['handle'] = $request['firstname'] . " " . $request['lastname'];
	    $qainput['email'] = $request['email'];
	    $qainput['level'] = 0;
	    $qainput['userid'] = $user['id'];
	    $qausers->fill($qainput)->save();
	    //case added
	    if (!empty($request['caseheading'] || !(empty($request['casecontent'])))):
	        $caseeditor = new Caseeditor();
	        $caseeditor->loggged_user_id = $user->id;
	        $caseeditor->case_heading = $request['caseheading'];
	        $caseeditor->case_body = $request['casecontent'];
	        $caseeditor->save();
	    endif;
	    //sending email
	    $to_name = $request['firstname'];
	    $to_email = $request['email'];
	    $data = array("name" => $request['firstname'], "username" => $user->username, "password" => $request['password'], "baseUrl" => url("social/register/user_verification/" . $user['id'] . "/" . $token));
	    \Mail::send("email.register", $data, function ($message) use ($to_name, $to_email) {
	        $message->to($to_email, $to_name)->subject("Lawsmos registration");
	    });
	    Auth::guard('user')->login($user);
	    // $this->session->set_flashdata("globalmsg", "Your account is created successfully");
	    return redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
	}




    public function register_old(Request $request)
    {
    //   $this->validate($request, [
    //     'email'   => 'required|email|unique:users',
    //     'password' => 'required|confirmed',
    //     'firstname'=> 'required|alpha',
    //     'lastname'=>  'required|alpha',
	// 	'category_id' => 'required'
    //   ]);

  $validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
        'lastname'=>  'required',
		'gender' => 'required',
		'email'   => 'required|email|unique:users',
		'phone' => 'required|min:10|numeric',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'phone.required' => 'The phone field is required',
		'phone.phone' => 'The phone field has invalid value',
		'password.required' => 'The password field is required',
		'password.confirmed' => 'The Password confirmation doesn\'t match'
	]
);
	
	if($validator->fails()){
		return response()->json(['failed'=>$validator->getMessageBag()->toArray()],400);
	}

      //register in laravel
        $user = new User;
        $token = rand(1,100000) . $request['email'];
				$token = md5(sha1($token));
        $input = $request->all();        
        $input['password'] = bcrypt($request['password']);
        $input['user_role_id']=0;
        $input['verification_token'] = $token;
        $input['phone'] = $request['phone'];
        $input['username']=$request['firstname'].$request['lastname'].rand(10,100);
        $input['name']=$request['firstname']." ".$request['lastname'];
				$input['source']="direct";
        $user->fill($input)->save();
        
        
		//register in social	
        $socialuser= new Usersocial;
        $socialinput['ID']=$user['id'];   
        $socialinput['password']=bcrypt($request['password']);
				$socialinput['first_name']=$request['firstname'];
				$socialinput['username']=$input['username'];
				$socialinput['last_name']=$request['lastname'];
				$socialinput['email']=$request['email'];
				$socialinput['source']="Direct";
				$socialinput['ID']=$user['id'];
		    $socialinput['verification_token']=$token;
        $socialinput['phone'] = $request['phone'];
				$socialinput['profile_identification']=0;
				$socialinput['user_role']=5;
				$socialinput['IP']=request()->ip();
				$socialinput['gender']=$request['gender'];
				
			
				$socialuser->fill($socialinput)->save();
        
        //register in QA
        $qausers= new Qausers;
        $qainput['passhash']=bcrypt($request['password']);
				$qainput['handle']=$request['firstname']." ".$request['lastname'];
				$qainput['email']=$request['email'];
				$qainput['level']=0;
				$qainput['userid']=$user['id'];
				$qausers->fill($qainput)->save();
        
        //case added
       
        
        if(!empty($request['caseheading'] || !(empty($request['casecontent'])))):
        
					$caseeditor = new Caseeditor();
					$caseeditor->loggged_user_id =  $user->id;
					$caseeditor->case_heading =  $request['caseheading'];
					$caseeditor->case_body = $request['casecontent'];
					
					$caseeditor->save(); 
        
        endif;
        
        
         //sending email
        
				$to_name 		= $request['firstname'];
				$to_email 	= $request['email'];
				$data = array("name" => $request['firstname'],"username" => $request['email'],"password" => $request['password'],"baseUrl" => url("social/register/user_verification/" . $user['id'] . "/" . $token ));
        
       
        \Mail::send("email.register", $data, function($message) use ($to_name, $to_email) {
			
			$message->to($to_email, $to_name)->subject("Lawsmos registration");
		
        });
        
        //Auth::guard('user')->login($user); 
        // redirect(url("social/login/pro?email=$request->email&salt=$request->password"));

		// $this->session->set_flashdata("globalmsg", "Your account is created successfully");
  
        return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
    }


    public function register(Request $request) {
	    $validator = \Validator::make($request->all(), [
	    	'firstname' => 'required',
	    	'lastname' => 'required',
	    	'gender' => 'required',
	    	'email' => 'required|email|unique:users',
	    	'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
	    	'password' => 'required|string|min:10|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/|confirmed'
	    ],
	    [
	    	'firstname.required' => 'The first name field is required',
	    	'lastname.required' => 'The last name field is required',
	    	'gender.required' => 'The gender field is required',
	    	'email.required' => 'The email field is required',
	    	'email.email' => 'The email field has invalid value',
	    	'phone.required' => 'The phone field is required',
	    	'email.unique' => 'The Email already has been taken',
	    	// 'phone.regex' => 'The phone field should contain atleast 10 digits with country code',
	    	'password.required' => 'The password field is required',
	    	'password.regex' => 'The password should contain upper and lower case letters, numbers, and symbols like ! ” ? $ % ^ & )',
	    ]);
	    if ($validator->fails()) {
	        return response()->json(['failed' => $validator->getMessageBag()->toArray() ], 400);
	    }
	    //register in laravel
	    $user = new User;
	    $token = rand(1, 100000) . $request['email'];
	    $token = md5(sha1($token));
	    $input = $request->all();
	    $input['password'] = bcrypt($request['password']);
	    // Store the cipher method
			$ciphering = "AES-128-CTR";

			// Use OpenSSl Encryption method
			$iv_length = openssl_cipher_iv_length($ciphering);
			$options = 0;

			// Non-NULL Initialization Vector for encryption
			$encryption_iv = '1234567891011121';

			// Store the encryption key
			$encryption_key = "Lawsmos";
	    $input['temp_password'] = openssl_encrypt($request['password'], $ciphering, $encryption_key, $options, $encryption_iv);
	    $input['user_role_id'] = $request['user_role_id'];
	    $input['verification_token'] = $token;
	    $input['phone'] = $request['phone'];
	    $input['username'] = $request['firstname'] . $request['lastname'] . rand(10, 100);
	    $input['name'] = $request['firstname'] . " " . $request['lastname'];
	    $input['source'] = "direct";
	    $user->fill($input)->save();
	    //register in social
	    $socialuser = new Usersocial;
	    $socialinput['ID'] = $user['id'];
	    $socialinput['password'] = bcrypt($request['password']);
	    $socialinput['temp_password'] =  openssl_encrypt($request['password'], $ciphering, $encryption_key, $options, $encryption_iv);
	    $socialinput['first_name'] = $request['firstname'];
	    $socialinput['username'] = $input['username'];
	    $socialinput['last_name'] = $request['lastname'];
	    $socialinput['email'] = $request['email'];
	    $socialinput['source'] = "Direct";
	    $socialinput['ID'] = $user['id'];
	    $socialinput['verification_token'] = $token;
	    $socialinput['phone'] = $request['phone'];
	    $socialinput['profile_identification'] = $request['user_role_id'];
	    $socialinput['user_role'] = 5;
	    $socialinput['IP'] = request()->ip();
	    $socialinput['gender'] = $request['gender'];
	    $socialuser->fill($socialinput)->save();
	    //register in QA
	    $qausers = new Qausers;
	    $qainput['passhash'] = bcrypt($request['password']);
	    $qainput['handle'] = $request['firstname'] . " " . $request['lastname'];
	    $qainput['email'] = $request['email'];
	    $qainput['level'] = 0;
	    $qainput['userid'] = $user['id'];
	    $qausers->fill($qainput)->save();
	    //case added
	    if (!empty($request['caseheading'] || !(empty($request['casecontent'])))):
	        $caseeditor = new Caseeditor();
	        $caseeditor->loggged_user_id = $user->id;
	        $caseeditor->case_heading = $request['caseheading'];
	        $caseeditor->case_body = $request['casecontent'];
	        $caseeditor->save();
	    endif;
	    //sending email
	    $to_name = $request['firstname'];
	    $to_email = $request['email'];
	    $data = array("name" => $request['firstname'], "username" => $request['email'], "password" => $request['password'], "baseUrl" => url("social/register/user_verification/" . $user['id'] . "/" . $token), "report_url" => url("report/" . $user['id'] . "/" . $token));
	    \Mail::send("email.register", $data, function ($message) use ($to_name, $to_email) {
	        $message->to($to_email, $to_name)->subject("Lawsmos registration");
	    });
	    // Auth::guard('user')->login($user);
	    // if (!empty($request['search_lawyer'])) {
	    // 	redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
	    // 	return $user->id;
	    // }
	    // $this->session->set_flashdata("globalmsg", "Your account is created successfully");
	    // redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
	    return $user->id;
	}
	public function register2(Request $request) {
		//return $request->all();
	    $validator = \Validator::make($request->all(), [
	    	'firstname' => 'required',
	    	'lastname' => 'required',
	    	'gender' => 'required',
	    	'email' => 'required|email|unique:users',
	    	'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
	    	'password' => 'required|string|min:10|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/|confirmed'
	    ],
	    [
	    	'firstname.required' => 'The first name field is required',
	    	'lastname.required' => 'The last name field is required',
	    	'gender.required' => 'The gender field is required',
	    	'email.required' => 'The email field is required',
	    	'email.email' => 'The email field has invalid value',
	    	'phone.required' => 'The phone field is required',
	    	'email.unique' => 'The Email already has been taken',
	    	// 'phone.regex' => 'The phone field should contain atleast 10 digits with country code',
	    	'password.required' => 'The password field is required',
	    	'password.regex' => 'The password should contain upper and lower case letters, numbers, and symbols like ! ” ? $ % ^ & )',
	    	'password.confirmed' => 'The Password confirmation doesn\'t match'
	    ]);
	    if ($validator->fails()) {
	        return response()->json(['failed' => $validator->getMessageBag()->toArray() ], 400);
	    }
	    //register in laravel
	    $user = new User;
	    $token = rand(1, 100000) . $request['email'];
	    $token = md5(sha1($token));
	    $input = $request->all();
	    $input['password'] = bcrypt($request['password']);
	    // Store the cipher method
			$ciphering = "AES-128-CTR";

			// Use OpenSSl Encryption method
			$iv_length = openssl_cipher_iv_length($ciphering);
			$options = 0;

			// Non-NULL Initialization Vector for encryption
			$encryption_iv = '1234567891011121';

			// Store the encryption key
			$encryption_key = "Lawsmos";
	    $input['temp_password'] = openssl_encrypt($request['password'], $ciphering, $encryption_key, $options, $encryption_iv);
	    $input['user_role_id'] = $request['user_role_id'];
	    $input['verification_token'] = $token;
	    $input['phone'] = $request['phone'];
	    $input['username'] = $request['firstname'] . $request['lastname'] . rand(10, 100);
	    $input['name'] = $request['firstname'] . " " . $request['lastname'];
	    $input['source'] = "direct";
	    $user->fill($input)->save();
	    //register in social
	    $socialuser = new Usersocial;
	    $socialinput['ID'] = $user['id'];
	    $socialinput['password'] = bcrypt($request['password']);
	    $socialinput['temp_password'] =  openssl_encrypt($request['password'], $ciphering, $encryption_key, $options, $encryption_iv);
	    $socialinput['first_name'] = $request['firstname'];
	    $socialinput['username'] = $input['username'];
	    $socialinput['last_name'] = $request['lastname'];
	    $socialinput['email'] = $request['email'];
	    $socialinput['source'] = "Direct";
	    $socialinput['ID'] = $user['id'];
	    $socialinput['verification_token'] = $token;
	    $socialinput['phone'] = $request['phone'];
	    $socialinput['profile_identification'] = $request['user_role_id'];
	    $socialinput['user_role'] = 5;
	    $socialinput['IP'] = request()->ip();
	    $socialinput['gender'] = $request['gender'];
	    $socialuser->fill($socialinput)->save();
	    //register in QA
	    $qausers = new Qausers;
	    $qainput['passhash'] = bcrypt($request['password']);
	    $qainput['handle'] = $request['firstname'] . " " . $request['lastname'];
	    $qainput['email'] = $request['email'];
	    $qainput['level'] = 0;
	    $qainput['userid'] = $user['id'];
	    $qausers->fill($qainput)->save();
	    //case added
	    if (!empty($request['caseheading'] || !(empty($request['casecontent'])))):
	        $caseeditor = new Caseeditor();
	        $caseeditor->loggged_user_id = $user->id;
	        $caseeditor->case_heading = $request['caseheading'];
	        $caseeditor->case_body = $request['casecontent'];
	        $caseeditor->save();
	    endif;
	    //sending email
	    $to_name = $request['firstname'];
	    $to_email = $request['email'];
	    $data = array("name" => $request['firstname'], "username" => $request['email'], "password" => $request['password'], "baseUrl" => url("social/register/user_verification/" . $user['id'] . "/" . $token), "report_url" => url("report/" . $user['id'] . "/" . $token));
	    \Mail::send("email.register", $data, function ($message) use ($to_name, $to_email) {
	        $message->to($to_email, $to_name)->subject("Lawsmos registration");
	    });
	    // Auth::guard('user')->login($user);
	    // if (!empty($request['search_lawyer'])) {
	    // 	redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
	    // 	return $user->id;
	    // }
	    // $this->session->set_flashdata("globalmsg", "Your account is created successfully");
	    // redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
	    return $user->id;
	}

    public function registerServiceSeeker(Request $request)
    {
    //   $this->validate($request, [
    //     'email'   => 'required|email|unique:users',
    //     'password' => 'required|confirmed',
    //     'firstname'=> 'required|alpha',
    //     'lastname'=>  'required|alpha',
       
    //   ]);

	  /*$validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
		'lastname'=>  'required',
		'gender'=>  'required',
		'email'   => 'required|email|unique:users',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'password.required' => 'The password field is required',
	]
);*/
	   $validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
        'lastname'=>  'required',
		'gender' => 'required',
		'email'   => 'required|email|unique:users',
		'phone' => 'required|min:10|numeric',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'phone.required' => 'The phone field is required',
		'phone.phone' => 'The phone field has invalid value',
		'password.required' => 'The password field is required',
	]
);

	  if($validator->fails()){
		return response()->json(['failed'=>$validator->getMessageBag()->toArray()],400);
	}
       //register in laravel
        $user = new User;
        $input = $request->all();        
        $input['password'] = bcrypt($request['password']);
        $input['user_role_id']=1;
        $input['phone'] = $request['phone'];
        $input['username']=$request['firstname'].$request['lastname'].rand(10,100);
		$input['name']=$request['firstname']." ".$request['lastname'];
		$input['source']="direct";
        $user->fill($input)->save();
        
       //register in social	
        $socialuser= new Usersocial;
        $socialinput['ID']=$user['id'];
        $socialinput['password']=bcrypt($request['password']);
		$socialinput['first_name']=$request['firstname'];
		$socialinput['username']=$input['username'];
		$socialinput['last_name']=$request['lastname'];
		$socialinput['phone'] = $request['phone'];
		$socialinput['email']=$request['email'];
		$socialinput['source']="Direct";
		$socialinput['ID']=$user['id'];
		$socialinput['profile_identification']=1;
		$socialinput['user_role']=5;
		$socialinput['IP']=request()->ip();
		$socialinput['gender']=$request['gender'];
		
	
		$socialuser->fill($socialinput)->save();
         
        //register in QA
        $qausers= new Qausers;
        $qainput['passhash']=bcrypt($request['password']);
		$qainput['handle']=$request['firstname']." ".$request['lastname'];
		$qainput['email']=$request['email'];
		$qainput['level']=0;
		$qainput['userid']=$user['id'];
		$qausers->fill($qainput)->save();
		
        //Auth::guard('user')->login($user); 
        $user = Auth::guard('user')->user();
        
        //sending email
        
         //sending email
        
        $to_name = $request['firstname'];
		$to_email =$request['email'];
		$data = array("name"=>$request['firstname'],"username"=>$request['email'],"password"=>$request['password']);
        
       
        \Mail::send("email.register", $data, function($message) use ($to_name, $to_email) {
			
			$message->to($to_email, $to_name)->subject("Lawsmos registration");
		
        });
        
        return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
    }

    public function registerlawstudent(Request $request){


    //   $this->validate($request, [
    //     'email'   => 'required|email|unique:users',
    //     'password' => 'required|confirmed',
    //     'firstname'=> 'required|alpha',
    //     'lastname'=>  'required|alpha',
       
    //   ]);

	/*$validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
		'lastname'=>  'required',
		'gender'=>  'required',
		'email'   => 'required|email|unique:users',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'password.required' => 'The password field is required',
	]
);*/ $validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
        'lastname'=>  'required',
		'gender' => 'required',
		'email'   => 'required|email|unique:users',
		'phone' => 'required|min:10|numeric',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'phone.required' => 'The phone field is required',
		'phone.phone' => 'The phone field has invalid value',
		'password.required' => 'The password field is required',
	]
);

	  if($validator->fails()){
		return response()->json(['failed'=>$validator->getMessageBag()->toArray()],400);
	}
       //register in laravel
        $user = new User;
        $input = $request->all();     
        
        $input['password'] = bcrypt($request['password']);
        $input['user_role_id']=2;
		$input['phone'] = $request['phone'];
        $input['username']=$request['firstname'].$request['lastname'].rand(10,100);
		$input['name']=$request['firstname']." ".$request['lastname'];
		$input['source']="direct";
        $user->fill($input)->save();
        
         //register in social	
        $socialuser= new Usersocial;
        
        $socialinput['ID']=$user['id'];   
        $socialinput['password']=bcrypt($request['password']);
		$socialinput['first_name']=$request['firstname'];
		$socialinput['username']=$input['username'];
		$socialinput['last_name']=$request['lastname'];
		$socialinput['phone'] = $request['phone'];
		$socialinput['email']=$request['email'];
		$socialinput['source']="Direct";
		$socialinput['ID']=$user['id'];
		$socialinput['profile_identification']=2;
		$socialinput['user_role']=5;
		$socialinput['IP']=request()->ip();
		$socialinput['gender']=$request['gender'];
		$socialuser->fill($socialinput)->save();
         //register in QA
        $qausers= new Qausers;
        $qainput['passhash']=bcrypt($request['password']);
		$qainput['handle']=$request['firstname']." ".$request['lastname'];
		$qainput['email']=$request['email'];
		$qainput['level']=0;
		$qainput['userid']=$user['id'];
		$qausers->fill($qainput)->save();
		
       // Auth::guard('user')->login($user); 
        //$user = Auth::guard('user')->user();
          //sending email
        
        $to_name = $request['firstname'];
		$to_email =$request['email'];
		$data = array("name"=>$request['firstname'],"username"=>$request['email'],"password"=>$request['password']);
        
       
        \Mail::send("email.register", $data, function($message) use ($to_name, $to_email) {
			
			$message->to($to_email, $to_name)->subject("Lawsmos registration");
		
        });
		return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
    }

    public function registerProfessor(Request $request){
		
	// 	 $this->validate($request, [
    //     'email'   => 'required|email|unique:users',
    //     'password' => 'required|confirmed',
    //     'firstname'=> 'required|alpha',
    //     'lastname'=>  'required|alpha',
        
    //   ]);



	/*$validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
		'lastname'=>  'required',
		'gender'=>  'required',
		'email'   => 'required|email|unique:users',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'password.required' => 'The password field is required',
	]
);*/ $validator = \Validator::make($request->all(), [
        'firstname'=> 'required',
        'lastname'=>  'required',
		'gender' => 'required',
		'email'   => 'required|email|unique:users',
		'phone' => 'required|min:10|numeric',
        'password' => 'required|confirmed',
	],
	[
		'firstname.required' => 'The first name field is required',
		'lastname.required' => 'The last name field is required',
		'gender.required' => 'The gender field is required',
		'email.required' => 'The email field is required',
		'email.email' => 'The email field has invalid value',
		'phone.required' => 'The phone field is required',
		'phone.phone' => 'The phone field has invalid value',
		'password.required' => 'The password field is required',
	]
);

	  if($validator->fails()){
		return response()->json(['failed'=>$validator->getMessageBag()->toArray()],400);
	}
      
       //register in laravel
        $user = new User;
        $input = $request->all();        
        $input['password'] = bcrypt($request['password']);
        $input['user_role_id']=3;
		$input['phone'] = $request['phone'];
        $input['username']=$request['firstname'].$request['lastname'].rand(10,100);
		$input['name']=$request['firstname']." ".$request['lastname'];
		$input['source']="direct";
        $user->fill($input)->save();
        
		   //register in social	
        $socialuser= new Usersocial;
        $socialinput['ID']=$user['id'];
        $socialinput['password']=bcrypt($request['password']);
		$socialinput['first_name']=$request['firstname'];
		$socialinput['username']=$input['username'];
		$socialinput['last_name']=$request['lastname'];
		$socialinput['email']=$request['email'];
		$socialinput['phone'] = $request['phone'];
		$socialinput['source']="Direct";
		$socialinput['ID']=$user['id'];
		$socialinput['profile_identification']=3;
		$socialinput['user_role']=5;
		$socialinput['IP']=request()->ip();
		$socialinput['gender']=$request['gender'];
		$socialuser->fill($socialinput)->save();
        
        //register in QA
        $qausers= new Qausers;
        $qainput['passhash']=bcrypt($request['password']);
		$qainput['handle']=$request['firstname']." ".$request['lastname'];
		$qainput['email']=$request['email'];
		$qainput['level']=0;
		$qainput['userid']=$user['id'];
		$qausers->fill($qainput)->save();
        
        //Auth::guard('user')->login($user); 
       // $user = Auth::guard('user')->user();

		  //sending email
        
        $to_name = $request['firstname'];
		$to_email =$request['email'];
		$data = array("name"=>$request['firstname'],"username"=>$request['email'],"password"=>$request['password']);
        
       
        \Mail::send("email.register", $data, function($message) use ($to_name, $to_email) {
			
			$message->to($to_email, $to_name)->subject("Lawsmos registration");
		
        });
	
        return redirect(url("social/login/pro?email=$request->email&salt=$request->password"));
		}
			public function login2(Request $request)
			{
			

				$credentials = $this->array_trim($request->only('email', 'password'));
				$rules = ['email' => 'required|email|max:255',
				  'password' => 'required'
				];

				$validation = Validator::make($credentials, $rules);
				$errors = $validation->errors();
				$errors = json_decode($errors);
				if ($validation->passes()) {
				  if (Auth::attempt(['email' => trim($request->email),
				              'password' => $request->password,
				                  ], $request->has('remember'))) {

				  	if(!Auth::user()->user_verified){
				  		$message = 'Please verify your account.';
				  		Auth::logout();
				      return response()->json(['message' => $message,'status' => false], 200);
				  	} 
				      return response()->json(['redirect' => true, 'status' => true], 200);
				  } else {
				      $message = 'Invalid username or password';

				      return response()->json(['message' => $message,'status' => false], 200);
				  }
				} else {
					$message='<ul>';
					foreach($errors as $error){
						$message.='<li>'.$error[0].'</li>';
					}
					$message.='</ul>';
				  return response()->json(['message' => $message,'status' => false], 200);
				}
				
			}
			function array_trim($array)
{
    $array = (array) $array;
    foreach ($array as $k => $v) {
        if ($v == '' || $v == array()) {
            unset($array[$k]);
        } elseif (is_array($v)) {
            $array[$k] = array_trim($v);
        }
    }
    return $array;
}
public function checkUser($value='')
{
	dd(Auth::user());
}
  
}
