<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Portfolio;
use App\Validator;
use App\Blog;
use App\Generalsetting;
use App\Pagesetting;
use App\Faq;
use App\Counter;
use App\Subscriber;
use App\User;
use App\Advertise;
use App\Firms;
use App\Judgement;
use App\Supreme_court_judgement;
use App\Qaposts;
use App\Userqa;
use App\Usersocial;
use App\Caseeditor;
use App\Qausers;
use App\Agency;
use App\Pages;
use App\Socialarticles;
use App\Socialblogs;
use App\Socialglobalsettings;
use App\Socialarticlesections;
use App\Contactus;
use App\Bareacts;
use App\Actsections;
use App\Actchapters;
use App\Actschedule;
use App\Maxims;
use App\Newmaxims;
use App\Userratings;
use App\Performa;
use App\Highcourtdata;
use App\News;
use App\Newsposts;
use App\Judgement_notes;
use Illuminate\Support\Facades\Session;
use InvalidArgumentException;
use Markury\MarkuryPost;
use DB;
use Cookie;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class FrontendController extends Controller
{

	public function __construct()
	{

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
		//  $this->auth_guests();
		if (isset($_SERVER['HTTP_REFERER'])) {
			$referral = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
			if ($referral != $_SERVER['SERVER_NAME']) {

				$brwsr = Counter::where('type', 'browser')->where('referral', $this->getOS());
				if ($brwsr->count() > 0) {
					$brwsr = $brwsr->first();
					$tbrwsr['total_count'] = $brwsr->total_count + 1;
					$brwsr->update($tbrwsr);
				} else {
					$newbrws = new Counter();
					$newbrws['referral'] = $this->getOS();
					$newbrws['type'] = "browser";
					$newbrws['total_count'] = 1;
					$newbrws->save();
				}

				$count = Counter::where('referral', $referral);
				if ($count->count() > 0) {
					$counts = $count->first();
					$tcount['total_count'] = $counts->total_count + 1;
					$counts->update($tcount);
				} else {
					$newcount = new Counter();
					$newcount['referral'] = $referral;
					$newcount['total_count'] = 1;
					$newcount->save();
				}
			}
		} else {
			$brwsr = Counter::where('type', 'browser')->where('referral', $this->getOS());
			if ($brwsr->count() > 0) {
				$brwsr = $brwsr->first();
				$tbrwsr['total_count'] = $brwsr->total_count + 1;
				$brwsr->update($tbrwsr);
			} else {
				$newbrws = new Counter();
				$newbrws['referral'] = $this->getOS();
				$newbrws['type'] = "browser";
				$newbrws['total_count'] = 1;
				$newbrws->save();
			}
		}
	}


	function getOS()
	{

		if (!isset($_SERVER['HTTP_USER_AGENT']) || empty($_SERVER['HTTP_USER_AGENT'])) {
			return false;
		} else {
			$user_agent     =   $_SERVER['HTTP_USER_AGENT'];
			$os_platform    =   "Unknown OS Platform";
			$os_array       =   array(
				'/windows nt 10/i'     =>  'Windows 10',
				'/windows nt 6.3/i'     =>  'Windows 8.1',
				'/windows nt 6.2/i'     =>  'Windows 8',
				'/windows nt 6.1/i'     =>  'Windows 7',
				'/windows nt 6.0/i'     =>  'Windows Vista',
				'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
				'/windows nt 5.1/i'     =>  'Windows XP',
				'/windows xp/i'         =>  'Windows XP',
				'/windows nt 5.0/i'     =>  'Windows 2000',
				'/windows me/i'         =>  'Windows ME',
				'/win98/i'              =>  'Windows 98',
				'/win95/i'              =>  'Windows 95',
				'/win16/i'              =>  'Windows 3.11',
				'/macintosh|mac os x/i' =>  'Mac OS X',
				'/mac_powerpc/i'        =>  'Mac OS 9',
				'/linux/i'              =>  'Linux',
				'/ubuntu/i'             =>  'Ubuntu',
				'/iphone/i'             =>  'iPhone',
				'/ipod/i'               =>  'iPod',
				'/ipad/i'               =>  'iPad',
				'/android/i'            =>  'Android',
				'/blackberry/i'         =>  'BlackBerry',
				'/webos/i'              =>  'Mobile'
			);
			foreach ($os_array as $regex => $value) {
				if (preg_match($regex, $user_agent)) {
					$os_platform    =   $value;
				}
			}
			return $os_platform;
		}
	}

	public function report($id='',$token='')
	{
		if ($id) {
			$user = User::where("id","=",$id)->where("verification_token","=",$token);
			$socialuser = \DB::table(SOCIAL_DB_NAME.'.users')->where("id",$id)->where("verification_token",$token);
			// var_dump($user);die;
			if ($user) {
				$deleteuser=$user->delete();
				$deletesocialuser=$socialuser->delete();
				if ($deleteuser) {
					Session::flash('report_message', 'Thank you for reporting. The account and the associated information has been deleted. The Matter has been reported to Lawsmos Admin team.');
				}else{
					Session::flash('report_message', 'Your account was already verified. The account and the associated information cannot be deleted. To delete your account, contact with us.');
				}
			}else{
				Session::flash('report_message', 'Your account was already verified. The account and the associated information cannot be deleted. To delete your account, contact with us.');
			}
		}
		return redirect()->route('front.index');
	}

	public function index()
	{
		// die("hello");
		$loggedinlat = "";
		$loggedinlon = "";
		$loggedinlatlong = "";
		$loggedincity = "";
		$result = "";
		$performa = "";
		$national = "";
		$newscat = "";


		$loggedinuser = '';

		if (\Auth::guard('user')->check()) :

			$user = \Auth::guard('user')->user();
			$loggedinlat = $user->lat;
			$loggedinlon = $user->longitude;
			$loggedinlatlong = $user->LatitudeLongitude;
			$loggedincity = $user->city;
			$loggedinuser = $user->id;
			$user = User::where('city', 'LIKE', '%' . $loggedincity . '%')
				->leftJoin(SOCIAL_DB_NAME . '.rating', 'users.ID', '=', 'rating.lawyerid')
				->get();
			$agency = Pages::where('location', 'LIKE', '%' . $loggedincity . '%')->where('is_active', 1)->get();
			$performa = Performa::where('status', 1)->where('featured', 1)->get();
			$result = array_merge($user->toArray(), $agency->toArray());

		endif;
		$cookievalue = Cookie::get('cookielocation');
		$cookielat = Cookie::get('cookielat');
		$cookielon = Cookie::get('cookielon');
		$users = User::all();
		$city = null;

		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}
		$ads = Portfolio::all();
		$cats = Category::all()->where("parent_id", 0);
		$catss = \DB::table(SOCIAL_DB_NAME . '.catagories')->where('parent_id', 0)->get();
		$questionarray = array();
		$answerarray = array();
		$qa_result = Qaposts::orderBy('created', 'DESC')->where("type", "A")->where("status", 1)->limit(4)->get();
		$recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();

		$socialgrobalsettings = Socialglobalsettings::find(1);
		$knowthelawtext = $socialgrobalsettings->know_the_law;
		foreach ($qa_result as $qa) :
			$answerarray[] = $qa;
		endforeach;
		$qq_result = Qaposts::orderBy('postid', 'DESC')->where("type", "Q")->where("status", 1)->limit(4)->get();//Qaposts::where("type", "Q")->get();
		foreach ($qq_result as $qq) :
			$questionarray[] = $qq;
		endforeach;

		$socialarticles = Socialarticles::orderBy('created_at', 'DESC')->where('status', 1)->limit(4)->get();
		$populararticles = Socialarticles::orderBy('views', 'DESC')->where('status', 1)->limit(4)->get();
		$rusers =   User::where('featured', '=', 1)->orderBy('created_at', 'desc')->limit(4)->get();

		$recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();
		// var_dump($recentjudgements);die;
		$years =   Highcourtdata::select('year')->distinct()->get();
		$highcourtheading =   Highcourtdata::select('heading')->get();
		$dateofhearing =   Highcourtdata::select('dateofhearing')->get();
		$bareacts = Bareacts::orderBy("created_at", "DESC")->limit(5)->get();
		$newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();

		$newspost = Newsposts::join(SOCIAL_DB_NAME.".user_news", SOCIAL_DB_NAME.".user_news.ID", SOCIAL_DB_NAME.".user_news_posts.blogid")
			->select(SOCIAL_DB_NAME.".user_news.title as newscat", SOCIAL_DB_NAME.".user_news_posts.*")
			->orderBy("created_at", "desc")
			->limit(3)
			->get();



		$states = Highcourtdata::select('state')->distinct()->orderBy('state')->get();
		$performa = Performa::where('status', 1)->where('featured', '1')->get();

		$newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();

		$lawyer_city = explode(', ',urldecode($loggedincity));
		$lawyer_city_id = DB::table(SOCIAL_DB_NAME.'.city')->where('name',$lawyer_city[0])->first();
		$lat = "30.7333148"; 
		$lon = "76.7794179";
// var_dump($lawyer_city);die;
		$current_time = strtotime('now');
		$subscribe_lawyers=[];
		if (!empty($lawyer_city_id->id)) {
			$subscribe_lawyers =  User::where('user.city', $lawyer_city_id->id)
					->where(SOCIAL_DB_NAME.'.city_subscription_plan.end_date', '>=', $current_time)
					->where('user.profile_identification', '=', 0)
					->where('user.user_role', '!=', 1)
					->select("users.id", "users.name", "users.category_id", "users.photo", "users.description", "users.language", "users.MarkerId", "users.education", "users.city", "users.state", "users.country", "users.lat", "users.longitude", "users.phone", "users.address", DB::raw('round(AVG('.SOCIAL_DB_NAME.'.rating.rating),0) as rating'), DB::raw('categories.cat_name as category'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.city_subscription_plan.status as sub_status', SOCIAL_DB_NAME.'.city_subscription_plan.plan_type as sub_plan_type', SOCIAL_DB_NAME.'.city_subscription_plan.end_date as sub_end_date'),'user.avatar')
					->groupBy('users.id')
					->limit(4)
	                ->orderBy(DB::raw('RAND()'))				
					->leftjoin(SOCIAL_DB_NAME.".rating", SOCIAL_DB_NAME.".rating.lawyerid", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".cityrankss", SOCIAL_DB_NAME.".cityrankss.ID", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".users as user", "user.ID", "users.id")				
					->leftjoin("categories", "categories.id", "users.category_id")					
					->rightjoin(SOCIAL_DB_NAME.".city_subscription_plan", SOCIAL_DB_NAME.".city_subscription_plan.user_id", "users.id")
					->get()->toArray();
			$ids = array_column($subscribe_lawyers, 'id');
			$lawyer_count = count($subscribe_lawyers);
			$featured_lawyer_limit = 4-$lawyer_count;
			$nonsubscribe_lawyers =  User::where('user.city', $lawyer_city_id->id)
					->where('user.profile_identification', '=', 0)
					->where('user.user_role', '!=', 1)
					->whereNotIn('users.id', $ids)
					->select("users.id", "users.name", "users.category_id", "users.photo", "users.description", "users.language", "users.MarkerId", "users.education", "users.city", "users.state", "users.country", "users.lat", "users.longitude", "users.phone", "users.address", DB::raw('round(AVG('.SOCIAL_DB_NAME.'.rating.rating),0) as rating'), DB::raw('categories.cat_name as category'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.city_subscription_plan.status as sub_status', SOCIAL_DB_NAME.'.city_subscription_plan.plan_type as sub_plan_type', SOCIAL_DB_NAME.'.city_subscription_plan.end_date as sub_end_date'),'user.avatar')
					->groupBy('users.id')
					->limit($featured_lawyer_limit)
	                ->orderBy(DB::raw('RAND()'))
					->leftjoin(SOCIAL_DB_NAME.".rating", SOCIAL_DB_NAME.".rating.lawyerid", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".cityrankss", SOCIAL_DB_NAME.".cityrankss.ID", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".users as user", "user.ID", "users.id")				
					->leftjoin("categories", "categories.id", "users.category_id")				
					->leftjoin(SOCIAL_DB_NAME.".city_subscription_plan", SOCIAL_DB_NAME.".city_subscription_plan.user_id", "users.id")
					->get()->toArray();
			$subscribe_lawyers = array_merge($subscribe_lawyers,$nonsubscribe_lawyers);
		}

		return view('front.index', compact('ads', 'cats', 'catss', 'rusers', 'questionarray', 'answerarray', 'socialarticles', 'knowthelawtext', 'cookievalue', 'cookielat', 'cookielon', 'loggedinlat', 'loggedinlon', 'loggedincity', 'result', 'years', 'highcourtheading', 'dateofhearing', 'states', 'recentjudgements', 'populararticles', 'performa', 'bareacts', 'newmaxims', 'national', 'newscat', 'newmaxims', 'newspost', 'loggedinuser','subscribe_lawyers','lat','lon'));
		// $loggedinlat = "";
		// $loggedinlon = "";
		// $loggedinlatlong = "";
		// $loggedincity = "";
		// $result = "";
		// $performa = "";
		// $national = "";
		// $newscat = "";



		// if (\Auth::guard('user')->check()) :

		// 	$user = \Auth::guard('user')->user();
		// 	$loggedinlat = $user->lat;
		// 	$loggedinlon = $user->longitude;
		// 	$loggedinlatlong = $user->LatitudeLongitude;
		// 	$loggedincity = $user->city;
		// 	$user = User::where('city', 'LIKE', '%' . $loggedincity . '%')
		// 		->leftJoin(SOCIAL_DB_NAME . '.rating', 'users.ID', '=', 'rating.lawyerid')
		// 		->get();
		// 	$agency = Pages::where('location', 'LIKE', '%' . $loggedincity . '%')->where('is_active', 1)->get();
		// 	$performa = Performa::where('status', 1)->where('featured', 1)->get();
		// 	$result = array_merge($user->toArray(), $agency->toArray());

		// endif;
		// $cookievalue = Cookie::get('cookielocation');
		// $cookielat = Cookie::get('cookielat');
		// $cookielon = Cookie::get('cookielon');
		// $users = User::all();
		// $city = null;

		// if (count($users) > 0) {
		// 	foreach ($users as $user) {
		// 		$city[] = $user->city;
		// 	}
		// 	$cities = array_unique($city);
		// } else {
		// 	$cities = null;
		// }
		// $ads = Portfolio::all();
		// $cats = Category::all()->where("parent_id", 0);
		// $catss = \DB::table(SOCIAL_DB_NAME . '.catagories')->where('parent_id', 0)->get();
		// $questionarray = array();
		// $answerarray = array();
		// $qa_result = Qaposts::orderBy('created', 'DESC')->where("type", "A")->limit(4)->get();
		// $recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();

		// $socialgrobalsettings = Socialglobalsettings::find(1);
		// $knowthelawtext = $socialgrobalsettings->know_the_law;
		// foreach ($qa_result as $qa) :
		// 	$answerarray[] = $qa;
		// endforeach;
		// $qq_result = Qaposts::where("type", "Q")->get();
		// foreach ($qq_result as $qq) :
		// 	$questionarray[] = $qq;
		// endforeach;

		// $socialarticles = Socialarticles::orderBy('created_at', 'DESC')->where('status', 1)->limit(4)->get();
		// $populararticles = Socialarticles::orderBy('views', 'DESC')->where('status', 1)->limit(4)->get();
		// $rusers =   User::where('featured', '=', 1)->orderBy('created_at', 'desc')->limit(4)->get();

		// $recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();
		// $years =   Highcourtdata::select('year')->distinct()->get();
		// $highcourtheading =   Highcourtdata::select('heading')->get();
		// $dateofhearing =   Highcourtdata::select('dateofhearing')->get();
		// $bareacts = Bareacts::orderBy("created_at", "DESC")->limit(5)->get();
		// $newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();

		// $newspost = Newsposts::join(SOCIAL_DB_NAME.".user_news", SOCIAL_DB_NAME.".user_news.ID", SOCIAL_DB_NAME.".user_news_posts.blogid")
		// 	->select(SOCIAL_DB_NAME.".user_news.title as newscat", SOCIAL_DB_NAME.".user_news_posts.*")
		// 	->orderBy("created_at", "desc")
		// 	->limit(3)
		// 	->get();



		// $states = Highcourtdata::select('state')->distinct()->orderBy('state')->get();

		// $newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();
		// return view('front.index', compact('ads', 'cats', 'catss', 'rusers', 'questionarray', 'answerarray', 'socialarticles', 'knowthelawtext', 'cookievalue', 'cookielat', 'cookielon', 'loggedinlat', 'loggedinlon', 'loggedincity', 'result', 'years', 'highcourtheading', 'dateofhearing', 'states', 'recentjudgements', 'populararticles', 'performa', 'bareacts', 'newmaxims', 'national', 'newscat', 'newmaxims', 'newspost'));
	}
	public function dark_home()
	{
		$loggedinlat = "";
		$loggedinlon = "";
		$loggedinlatlong = "";
		$loggedincity = "";
		$result = "";
		$performa = "";
		$national = "";
		$newscat = "";


		$loggedinuser = '';

		if (\Auth::guard('user')->check()) :

			$user = \Auth::guard('user')->user();
			$loggedinlat = $user->lat;
			$loggedinlon = $user->longitude;
			$loggedinlatlong = $user->LatitudeLongitude;
			$loggedincity = $user->city;
			$loggedinuser = $user->id;
			$user = User::where('city', 'LIKE', '%' . $loggedincity . '%')
				->leftJoin(SOCIAL_DB_NAME . '.rating', 'users.ID', '=', 'rating.lawyerid')
				->get();
			$agency = Pages::where('location', 'LIKE', '%' . $loggedincity . '%')->where('is_active', 1)->get();
			$performa = Performa::where('status', 1)->where('featured', 1)->get();
			$result = array_merge($user->toArray(), $agency->toArray());

		endif;
		$cookievalue = Cookie::get('cookielocation');
		$cookielat = Cookie::get('cookielat');
		$cookielon = Cookie::get('cookielon');
		$users = User::all();
		$city = null;

		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}
		$ads = Portfolio::all();
		$cats = Category::all()->where("parent_id", 0);
		$catss = \DB::table(SOCIAL_DB_NAME . '.catagories')->where('parent_id', 0)->get();
		$questionarray = array();
		$answerarray = array();
		$qa_result = Qaposts::orderBy('created', 'DESC')->where("type", "A")->where("status", 1)->limit(4)->get();
		$recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();

		$socialgrobalsettings = Socialglobalsettings::find(1);
		$knowthelawtext = $socialgrobalsettings->know_the_law;
		foreach ($qa_result as $qa) :
			$answerarray[] = $qa;
		endforeach;
		$qq_result = Qaposts::orderBy('created', 'DESC')->where("type", "Q")->where("status", 1)->limit(4)->get();//Qaposts::where("type", "Q")->get();
		foreach ($qq_result as $qq) :
			$questionarray[] = $qq;
		endforeach;

		$socialarticles = Socialarticles::orderBy('created_at', 'DESC')->where('status', 1)->limit(4)->get();
		$populararticles = Socialarticles::orderBy('views', 'DESC')->where('status', 1)->limit(4)->get();
		$rusers =   User::where('featured', '=', 1)->orderBy('created_at', 'desc')->limit(4)->get();

		$recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();
		// var_dump($recentjudgements);die;
		$years =   Highcourtdata::select('year')->distinct()->get();
		$highcourtheading =   Highcourtdata::select('heading')->get();
		$dateofhearing =   Highcourtdata::select('dateofhearing')->get();
		$bareacts = Bareacts::orderBy("created_at", "DESC")->limit(5)->get();
		$newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();

		$newspost = Newsposts::join(SOCIAL_DB_NAME.".user_news", SOCIAL_DB_NAME.".user_news.ID", SOCIAL_DB_NAME.".user_news_posts.blogid")
			->select(SOCIAL_DB_NAME.".user_news.title as newscat", SOCIAL_DB_NAME.".user_news_posts.*")
			->orderBy("created_at", "desc")
			->limit(3)
			->get();



		$states = Highcourtdata::select('state')->distinct()->orderBy('state')->get();
		$performa = Performa::where('status', 1)->where('featured', '1')->get();

		$newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();
		return view('front.dark-home', compact('ads', 'cats', 'catss', 'rusers', 'questionarray', 'answerarray', 'socialarticles', 'knowthelawtext', 'cookievalue', 'cookielat', 'cookielon', 'loggedinlat', 'loggedinlon', 'loggedincity', 'result', 'years', 'highcourtheading', 'dateofhearing', 'states', 'recentjudgements', 'populararticles', 'performa', 'bareacts', 'newmaxims', 'national', 'newscat', 'newmaxims', 'newspost', 'loggedinuser'));
	}

	public function refreshCaptcha()
	{
		return response()->json(['captcha' => captcha_img('flat')]);
	}

	public function savesession(Request $request)
	{

		if (\Auth::guard('user')->check()) :
			$user = \Auth::guard('user')->user();

			$caseheading = $request->casetitle;
			$casecatgory = $request->catagory;
			$casecontent = $request->casecontent;
			$casestatus = $request->save_as;


			$caseeditor = new Caseeditor();
			$caseeditor->loggged_user_id =  $user->id;
			$caseeditor->case_heading =  $caseheading;
			$caseeditor->case_body = $casecontent;
			$caseeditor->case_category = $casecatgory;
			$caseeditor->status = $casestatus;

			$caseeditor->save();

			echo $result = json_encode("success");

		else :

			echo $result = json_encode("failed");

			session()->put('caseheading', $request->get('casetitle'));
			session()->put('casecontent', $request->get('casecontent'));
		//session(['caseheading' => $request->get('caseheaging'),'casecatgory'=>$request->get('casecatgory'),'casecontent'=>$request->get('casecontent')]);

		endif;
	}

	//save judgement notes
	public function saveheadnots(Request $request)
	{

		if (\Auth::guard('user')->check()) :
			$user = \Auth::guard('user')->user();

			if ($request->type == 2) {
				$update_data = array(
					'title' => $request->title,
					'note' => $request->casecontent
				);

				$update_headnote =  Judgement_notes::where('id', $request->row_id)->update($update_data);
				if ($update_headnote) {
					return  json_encode('success');
				} else {
					return json_encode('false');
				}
			}


			$judgement_title = $request->title;
			$casecontent = $request->casecontent;

			$judgement_id = $request->judgement_id;
			$judgement_type = $request->judgement_type;
			$judgement_heading = $request->judgement_heading;



			$data = array(
				'title' => $judgement_title,
				'note' => $casecontent,
				'created_by' => $user->id,
				'status' => 1,
				'heading' => $judgement_heading,
				'judgement_id' => $judgement_id,
				'judgement_type' => $judgement_type
			);




			Judgement_notes::create($data);

			echo $result = json_encode("success");

		else :

			echo $result = json_encode("failed");

			session()->put('caseheading', $request->get('casetitle'));
			session()->put('casecontent', $request->get('casecontent'));
		//session(['caseheading' => $request->get('caseheaging'),'casecatgory'=>$request->get('casecatgory'),'casecontent'=>$request->get('casecontent')]);

		endif;
	}
	public function enterlocation(Request $request)
	{
		$loggedinuser = '';

		if (\Auth::guard('user')->check()) :
			$user = \Auth::guard('user')->user();
			$loggedinuser = $user->id;
		endif;
		$cookievalue = "";
		$national = "";

		$location = $request->locationselect;
		$lat = $request->lat;
		$lon = $request->long;

		
		// dd($distance);
		$users = User::all();
		$city = null;
		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}
		$ads = Portfolio::all();
		$cats = Category::all()->where("parent_id", 0);
		$catss = \DB::table(SOCIAL_DB_NAME . '.catagories')->where('parent_id', 0)->get();
		$rusers =   User::where('featured', '=', 1)->orderBy('created_at', 'desc')->limit(4)->get();
		$user = User::where('lat', '!=', null)
			->leftJoin(SOCIAL_DB_NAME . '.rating', 'users.ID', '=', 'rating.lawyerid')
			->get();
		$agency = Pages::where('location', 'LIKE', '%' . $location . '%')->where('is_active', 1)->get();
		$performa = Performa::where('status', 1)->where('featured', '1')->get();
		$result = array_merge($user->toArray(), $agency->toArray());
		// dd($result);
		$questionarray = array();
		$answerarray = array();
		$qa_result = Qaposts::orderBy('created', 'DESC')->where("type", "A")->where("status", 1)->limit(4)->get();
		foreach ($qa_result as $qa) :
			$answerarray[] = $qa;
		endforeach;
		$qq_result = Qaposts::orderBy('created', 'DESC')->where("type", "Q")->where("status", 1)->limit(4)->get();//Qaposts::where("type", "Q")->get();
		foreach ($qq_result as $qq) :
			$questionarray[] = $qq;
		endforeach;
		//setcookies
		$setcookie = Cookie::queue('cookielocation', $location, 60);
		$setcookie1 = Cookie::queue('cookielat', $lat, 60);
		$setcookie2 = Cookie::queue('cookielon', $lon, 60);
		//endcookies
		$socialgrobalsettings = Socialglobalsettings::find(1);
		$socialarticles = Socialarticles::orderBy('created_at', 'DESC')->where('status', 1)->limit(4)->get();
		$populararticles = Socialarticles::orderBy('views', 'DESC')->where('status', 1)->limit(4)->get();
		$recentjudgements = Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();
		$years =   Highcourtdata::select('year')->distinct()->get();
		$highcourtheading =   Highcourtdata::select('heading')->get();
		$dateofhearing =   Highcourtdata::select('dateofhearing')->get();
		$bareacts = Bareacts::orderBy("created_at", "DESC")->limit(4)->get();
		$states =   Highcourtdata::select('state')->distinct()->orderBy('state')->get();

		$newspost = Newsposts::join(SOCIAL_DB_NAME.".user_news", SOCIAL_DB_NAME.".user_news.ID", SOCIAL_DB_NAME.".user_news_posts.blogid")
			->select(SOCIAL_DB_NAME.".user_news.title as newscat", SOCIAL_DB_NAME.".user_news_posts.*")
			->orderBy("created_at", "desc")
			->limit(3)
			->get();
		$newmaxims = Newmaxims::orderBy("created_at", "desc")->limit(4)->get();
		$lawyer_city = explode(', ',urldecode($location));
		$lawyer_city_id = DB::table(SOCIAL_DB_NAME.'.city')->where('name',$lawyer_city[0])->first();
		// dd($lawyer_city_id);
		$current_time = strtotime('now');
		$subscribe_lawyers = [];
		if ($lat) {
			$subscribe_lawyers = User::where('user.profile_identification', '=', 0)

					->select("users.id", "users.name", "users.category_id", "users.photo", "users.description", "users.language", "users.MarkerId", "users.education", "users.city", "users.state", "users.country", "users.lat", "users.longitude", "users.phone", "users.address", DB::raw('round(AVG('.SOCIAL_DB_NAME.'.rating.rating),0) as rating'), DB::raw('categories.cat_name as category'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.city_subscription_plan.status as sub_status', SOCIAL_DB_NAME.'.city_subscription_plan.plan_type as sub_plan_type', SOCIAL_DB_NAME.'.city_subscription_plan.end_date as sub_end_date'),'user.avatar')
					->groupBy('users.id')
					
	                ->orderBy(DB::raw('RAND()'))				
					->leftjoin(SOCIAL_DB_NAME.".rating", SOCIAL_DB_NAME.".rating.lawyerid", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".cityrankss", SOCIAL_DB_NAME.".cityrankss.ID", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".users as user", "user.ID", "users.id")				
					->leftjoin("categories", "categories.id", "users.category_id")					
					->leftjoin(SOCIAL_DB_NAME.".city_subscription_plan", SOCIAL_DB_NAME.".city_subscription_plan.user_id", "users.id")
					->get()->toArray();
					// dd($subscribe_lawyers);
			$ids = array_column($subscribe_lawyers, 'id');
			$lawyer_count = count($subscribe_lawyers);
			$featured_lawyer_limit = 4-$lawyer_count;
			$nonsubscribe_lawyers = User::where('user.profile_identification', '=', 0)
					->where('user.user_role', '!=', 1)
					->whereNotIn('users.id', $ids)
					->select("users.id", "users.name", "users.category_id", "users.photo", "users.description", "users.language", "users.MarkerId", "users.education", "users.city", "users.state", "users.country", "users.lat", "users.longitude", "users.phone", "users.address", DB::raw('round(AVG('.SOCIAL_DB_NAME.'.rating.rating),0) as rating'), DB::raw('categories.cat_name as category'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'), DB::raw(SOCIAL_DB_NAME.'.city_subscription_plan.status as sub_status', SOCIAL_DB_NAME.'.city_subscription_plan.plan_type as sub_plan_type', SOCIAL_DB_NAME.'.city_subscription_plan.end_date as sub_end_date'),'user.avatar')
					->groupBy('users.id')
					->limit($featured_lawyer_limit)
	                ->orderBy(DB::raw('RAND()'))
					->leftjoin(SOCIAL_DB_NAME.".rating", SOCIAL_DB_NAME.".rating.lawyerid", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".cityrankss", SOCIAL_DB_NAME.".cityrankss.ID", "users.id")
					->leftjoin(SOCIAL_DB_NAME.".users as user", "user.ID", "users.id")				
					->leftjoin("categories", "categories.id", "users.category_id")				
					->leftjoin(SOCIAL_DB_NAME.".city_subscription_plan", SOCIAL_DB_NAME.".city_subscription_plan.user_id", "users.id")
					->get()->toArray();
			$subscribe_lawyers = array_merge($subscribe_lawyers,$nonsubscribe_lawyers);
			//dd($subscribe_lawyers);
			$layers=[];
			foreach ($subscribe_lawyers as $lawyer) {
				if ($lawyer['lat']) {
						$distance = $this->distanceCalculation(array('lat'=>$lat,'lng'=>$lon), array('lat' =>$lawyer['lat'],'lng'=>$lawyer['longitude'] ));
				         if ($distance<20) {
				         	$lawyer['distance'] =$distance;
				         	$layers[] = $lawyer;
				         }
				}

			
				
			}
			$subscribe_lawyers = $layers;
		}
		return view('front.index', compact('ads', 'cats', 'catss', 'rusers', 'cities', 'location', 'lat', 'lon', 'result', 'questionarray', 'answerarray', 'socialarticles', 'socialgrobalsettings', 'cookievalue', 'years', 'highcourtheading', 'dateofhearing', 'states', 'recentjudgements', 'populararticles', 'performa', 'bareacts', 'newmaxims', 'national', 'newspost','loggedinuser','subscribe_lawyers'));
	}

	private function findNearestRestaurants($latitude, $longitude, $radius = 400)
    {
        /*
         * using eloquent approach, make sure to replace the "Restaurant" with your actual model name
         * replace 6371000 with 6371 for kilometer and 3956 for miles
         */
        $restaurants = User::selectRaw("id, name, address, latitude, longitude, rating, zone ,
                         ( 6371000 * acos( cos( radians(?) ) *
                           cos( radians( latitude ) )
                           * cos( radians( longitude ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( latitude ) ) )
                         ) AS distance", [$latitude, $longitude, $latitude])
            ->where('active', '=', 1)
            ->having("distance", "<", $radius)
            ->orderBy("distance",'asc')
            ->offset(0)
            ->limit(20)
            ->get();

        return $restaurants;
    }
	 public function distanceCalculation($user_location, $donor_location) {
        // Calculate the distance in degrees
        $degrees = rad2deg(acos((sin(deg2rad($user_location['lat']))*sin(deg2rad($donor_location['lat']))) + (cos(deg2rad($user_location['lat']))*cos(deg2rad($donor_location['lat']))*cos(deg2rad($user_location['lng']-$donor_location['lng'])))));
    
        $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
        return round($distance);
    }

	public function search_judgements(Request $request)
	{
		$query = $request->get('query');
		$result = Judgement::where('heading', 'LIKE', '%' . $query . '%')->orWhere('year', 'LIKE', '%' . $query . '%')->get();
		return response()->json($result);
	}


	public function searcharticlebycat(Request $request)
	{

		$query = $request->get('searchcat');
		$output = "";
		$result = Socialarticles::where('catagory', $query)->limit(4)->get();

		if ($result) {
			$output .= "<ul>";
			foreach ($result as $key => $product) {


				$output .= '<li><a href="' . route('front.articles', $product->ID) . '" >' . $product->title . '</a></li>';
			}
			$output .= "</ul>";
			return Response($output);
		}
	}

	public function searcharticlebycatgg(Request $request)
	{

		$query = $request->get('searchcat');
		$output = "";
		$result = Socialarticles::where('catagory', $query)->limit(4)->get();

		if ($result) {
			foreach ($result as $key => $product) {
				$output .= '<li><a href="' . route('front.articles', $product->ID) . '" >' . $product->title . '<p>' . substr(strip_tags(base64_decode($product->description)), 0, 250) . '</p>  <p><a class="reda-more-art transition_3s" href="' . route('front.articles', $product->ID) . '">Read More</a></p></a></li>';
			}
			return Response($output);
		}
	}

	public function viewjudgementss($id)
	{

		$result = Judgement::find($id);
		return view('front.viewjudgement', compact('result'));
	}

	public function newsdetail($id)
	{
		$result = Newsposts::find($id);
		return view('front.newsdetail', compact('result'));
	}
	public function search_bareacts(Request $request)
	{
		$query = $request->get('query');
		$result = Judgement::where('heading', 'LIKE', '%' . $query . '%')->orWhere('year', 'LIKE', '%' . $query . '%')->get();
		return response()->json($result);
	}

	public function searcharticles(Request $request)
	{
		$output = "";
		$search = $request->get('search');
		$category = $request->get('category');
		$sub_category = $request->get('sub_category');

		$sort_by = "asc";
		$order_field = "title";
		$order_type = "ASC";

		if (!empty($request->get('sort_by'))) {
			$sort_by = $request->get('sort_by');
		}

		if ($sort_by == "asc") {
			$order_field = "title";
			$order_type = "ASC";
		} elseif ($sort_by == "desc") {
			$order_field = "title";
			$order_type = "DESC";
		} elseif ($sort_by == "popular") {
			$order_field = "views";
			$order_type = "DESC";
		} elseif ($sort_by == "recent") {
			$order_field = "created_at";
			$order_type = "DESC";
		} else {
			$order_field = "created_at";
			$order_type = "DESC";
		}

		if ($category != "" && $sub_category == "") {

			$acts = Socialarticles::where("status", 1)
				->where('title', 'LIKE', '%' . $search . '%')
				->where('catagory', '=', $category)
				->orderBy($order_field, $order_type)
				->get();
		} elseif ($category == "" && $sub_category != "") {

			$acts = Socialarticles::where("status", 1)
				->where('title', 'LIKE', '%' . $search . '%')
				->where('subcatagory', '=', $sub_category)
				->orderBy($order_field, $order_type)
				->get();
		} elseif ($category != "" && $sub_category != "") {

			$acts = Socialarticles::where("status", 1)
				->where('title', 'LIKE', '%' . $search . '%')
				->where('catagory', '=', $category)
				->where('subcatagory', '=', $sub_category)
				->orderBy($order_field, $order_type)
				->get();
		} else {
			$acts = Socialarticles::where("status", 1)
				->where('title', 'LIKE', '%' . $search . '%')
				->orderBy($order_field, $order_type)
				->get();
		}



		if ($acts->toArray()) {
			$output = "";
			foreach ($acts as $key => $product) {
				$name = User::where('id', '=', $product->userid)->first();
				$output .= '<li>
					<a href="' . url("articles", $product->ID) . '">' . $product->title . '</a>
					<br> Submitted by : ' . $name['name'] . ' 
					<br> Date : ' . date('d-m-Y h:i a', strtotime($product->created_at)) . ' 
					<br> Likes : 5, 
						 Views : ' . $product->views . '  
					</li>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}

	public function searchmax(Request $request)
	{


		$output = "";
		$searchquery = $request->get('search');
		$searchalphabet = $request->get('alphabet');
		$maxims = Newmaxims::where('term', 'like', '%' . $searchquery . '%')
			->where('term', 'like', $searchalphabet . '%')
			->get();

		if ($maxims->toArray()) {
			foreach ($maxims as $key => $product) {
				$output .= '<p><b>' . $product->term . "</b><br>" . $product->def . '</p>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}

	public function searchhighcourt(Request $request)
	{

		$output = "";
		$search = $request->get('search');

		$year = $request->get('year');
		$catagory = $request->get('catagory');
		$court = $request->get('court');

		$sort_by = "asc";
		$order_field = "heading";
		$order_type = "ASC";

		if (!empty($request->get('sort_by'))) {
			$sort_by = $request->get('sort_by');
		}

		if ($sort_by == "asc") {
			$order_field = "heading";
			$order_type = "ASC";
		} elseif ($sort_by == "desc") {
			$order_field = "heading";
			$order_type = "DESC";
		} elseif ($sort_by == "recent") {
			$order_field = "dateofhearing";
			$order_type = "DESC";
		} else {
			$order_field = "heading";
			$order_type = "ASC";
		}

		// $catagory = 0;

		if ($year == "" || $year == null) {

			$acts = Judgement::where('heading', 'LIKE', '%' . $search . '%')->Where('state', 'LIKE', '%' . $court . '%')->Where('category', 'LIKE', '%' . $catagory . '%')->where('category', '!=', "0")->orderBy($order_field, $order_type)->get();
		} else if ($catagory == "" || $catagory == null) {
			$acts = Judgement::where('heading', 'LIKE', '%' . $search . '%')->Where('year', 'LIKE', '%' . $year . '%')->Where('state', 'LIKE', '%' . $court . '%')->where('category', '!=', "0")->orderBy($order_field, $order_type)->get();
		} else if ($court == "" || $court == null) {
			$acts = Judgement::where('heading', 'LIKE', '%' . $search . '%')->Where('year', 'LIKE', '%' . $year . '%')->Where('category', 'LIKE', '%' . $catagory . '%')->where('category', '!=', "0")->orderBy($order_field, $order_type)->get();
		} else if ($year == "" || $year == null && $court == "" || $court == null && $catagory == "" || $catagory == null) {

			$acts = Judgement::where('heading', 'LIKE', '%' . $search . '%')->where('category', '!=', "0")->orderBy($order_field, $order_type)->get();
		} else {

			$acts = Judgement::where('heading', 'LIKE', '%' . $search . '%')->Where('year', 'LIKE', '%' . $year . '%')->Where('state', 'LIKE', '%' . $court . '%')->Where('category', 'LIKE', '%' . $catagory . '%')->orderBy($order_field, $order_type)->where('category', '!=', "0")->get();
		}

		if (!empty($acts->toArray())) {
			foreach ($acts as $key => $product) {

				// $output.='<li onclick="select_judgement('.$product->id.')"><a href="'.url("detail",$product->id).'">'. $product->heading.'</a></li>';

				$output .= '<li><a onclick="checkjudgementHeadnote(' . $product->id . ',1)" href="' . url("detail", $product->id) . '" id="select_judgement_' . $product->id . '">' . $product->heading . '</a></li>';
			}

			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}
	public function searchsupremecourt(Request $request)
	{



		$output = "";
		$search = $request->get('search');

		$year = $request->get('year');
		// $catagory = $request->get('catagory');
		// $court = $request->get('court');

		$sort_by = "asc";
		$order_field = "heading";
		$order_type = "ASC";

		if (!empty($request->get('sort_by'))) {
			$sort_by = $request->get('sort_by');
		}

		if ($sort_by == "asc") {
			$order_field = "heading";
			$order_type = "ASC";
		} elseif ($sort_by == "desc") {
			$order_field = "heading";
			$order_type = "DESC";
		} elseif ($sort_by == "recent") {
			$order_field = "date";
			$order_type = "DESC";
		} else {
			$order_field = "heading";
			$order_type = "ASC";
		}




		// $catagory = 0;
		// $acts = Supreme_court_judgement::limit(10)->get();

		if ($year == "" || $year == null) {

			$acts = Supreme_court_judgement::where('heading', 'LIKE', '%' . $search . '%')->limit(100)->orderBy($order_field,$order_type)->get();
			// $acts=Supreme_court_judgement::limit(100)->where('heading', 'LIKE','%'.$search.'%')->Where('category','LIKE','%'.$catagory.'%')->where('category','!=',"0")->orderBy($order_field,$order_type)->get();

		} else {
			$acts = Supreme_court_judgement::where('heading', 'LIKE', '%' . $search . '%')->Where('year', 'LIKE', '%' . $year . '%')->orderBy($order_field,$order_type)->get();
		}

		if (!empty($acts->toArray())) {
			foreach ($acts as $key => $product) {

				// $output.='<li onclick="select_judgement('.$product->id.')"><a href="'.url("detail",$product->id).'">'. $product->heading.'</a></li>';

				$output .= '<li><a href="' . url("detail_supreme", $product->ID) . '" id="select_judgement_' . $product->ID . '">' . $product->heading . '</a></li>';
			}


			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}

	public function searchbar_jugements(Request $request)
	{
		$output = "";
		$search = $request->get('search');
		$acts = Judgement::where('heading', 'LIKE', '%' . $search . '%')->get();

		if (!empty($acts->toArray())) {
			foreach ($acts as $key => $product) {
				$output .= '<li><a href="' . url("detail", $product->id) . '">' . $product->heading . '</a></li>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}
	public function search_bareactsss(Request $request)
	{
		$output = "";
		$searchquery = $request->get('search');

		$sort_by = "asc";
		$order_field = "title";
		$order_type = "ASC";

		if (!empty($request->get('sort_by'))) {
			$sort_by = $request->get('sort_by');
		}

		if ($sort_by == "asc") {
			$order_field = "title";
			$order_type = "ASC";
		} elseif ($sort_by == "desc") {
			$order_field = "title";
			$order_type = "DESC";
		} elseif ($sort_by == "recent") {
			$order_field = "created_at";
			$order_type = "DESC";
		} else {
			$order_field = "title";
			$order_type = "ASC";
		}

		$acts = Bareacts::where('title', 'like', '%' . $searchquery . '%')
			->orderBy($order_field, $order_type)
			//->orWhere('description', 'like', '%'.$searchquery.'%')
			->get();

		if ($acts->toArray()) {
			foreach ($acts as $key => $product) {
				$output .= '<li><a href="' . url("act_details", $product->id) . '">' . $product->title . '</a></li>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}


	public function submitsearch(Request $request)
	{

		$court = $request->get('court');
		$year = $request->get('year');
		$catagory = $request->get('catagory');
		$content = $request->get('content');

		$highcourt =DB::table(SOCIAL_DB_NAME . '.high_court_ap_yearly');
		if ($court && $court != 'Select Court') {
			echo $court;
			$highcourt->where('state', 'like', '%' . $court . '%');
		}if ($year) {
			echo $year;
			$highcourt->where('year', 'like', '%' . $year . '%');
		}if ($catagory && $catagory != 'Select Catagory') {
			echo $catagory;
			$highcourt->where('category', 'like', '%' . $catagory . '%');
		}if ($content) {
			echo $content;
			$highcourt->where('heading', 'like', '%' . $content . '%');
		}
		$highcourts=$highcourt->get()->toArray();
		return view('front.searchlistings', compact('highcourts'));
	}

	public function bareacts()
	{
		$bareacts = Bareacts::get();
		return view('front.viewacts', compact('bareacts'));
	}


	public function listallarticles()
	{
		$recent = Socialarticles::orderBy('created_at', 'DESC')->where('status', 1)->get();
		$popular = Socialarticles::orderBy('views', 'DESC')->where('status', 1)->get();
		$featured = Socialarticles::where('featured', 1)->where('status', 1)->get();
		$catss = \DB::table(SOCIAL_DB_NAME . '.catagories')->where('parent_id', 0)->get();
		return view('front.viewallarticles', compact('recent', 'popular', 'featured', 'catss'));

		//return view('front.viewallarticles',compact('articles'));
	}
	public function listallmaxims()
	{
		$maxims = Maxims::get();
		return view('front.viewallmaxims', compact('maxims'));
	}
	public function allmaxims()
	{
		$maxims = Newmaxims::get();
		return view('front.viewallnewmaxims', compact('maxims'));
	}
	public function searchmaxims(Request $request)
	{
		$output = "";
		$searchquery = $request->get('search');
		$searchalphabet = $request->get('alphabet');
		$maxims = Maxims::where('term', 'like', '%' . $searchquery . '%')
			->where('term', 'like', $searchalphabet . '%')
			->get();

		if ($maxims->toArray()) {
			$count = 0;
			foreach ($maxims as $key => $product) {
				$count++;
				$class = "";
				if ($count == 1) {
					$class = 'class="test"';
				}
				$output .= '<p ' . $class . '><b>' . $product->term . "</b><br>" . $product->definition . '</p>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}


	public function searchnewmaxims(Request $request)
	{
		$output = "";
		$searchquery = $request->get('search');
		$maxims = Newmaxims::where('term', 'like', '%' . $searchquery . '%')
			->get();

		if ($maxims->toArray()) {
			foreach ($maxims as $key => $product) {
				$output .= '<p><b>' . $product->term . "</b><br>" . $product->def . '</p>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}
	public function searchtermsalpha($query)
	{
		$char = preg_replace('#[^a-z]#i', '', $query);
		$alphaterms = Maxims::where('term', 'like', $char . '%')
			->get();
		return view('front.viewallmaxims', compact('alphaterms'));
	}
	public function searchtermsmaxims($query)
	{
		$char = preg_replace('#[^a-z]#i', '', $query);
		$alphaterms = Newmaxims::where('term', 'like', $char . '%')->get();

		return view('front.viewallnewmaxims', compact('alphaterms','query'));
	}
	public function searchbacts($query)
	{
		$char = preg_replace('#[^a-z]#i', '', $query);

		$alphaterms = Bareacts::where('title', 'like', $char . '%')
			->get();
		return view('front.viewacts', compact('alphaterms'));
	}
	public function listallhighcourt()
	{
		$highcourt = Highcourtdata::paginate(15);
		return view('front.listhighcourt', compact('highcourt'));
	}
	public function highcourtedetails($id)
	{
		$highcourt = Highcourtdata::find($id);
		return view('front.highcourtdetail', compact('highcourt'));
	}
	public function supremecourtedetails($id)
	{
		$supremecourt = Supreme_court_judgement::find($id);
		return view('front.supreme_court_detail_judgements', compact('supremecourt'));
	}
	public function listallsupremecourt()
	{

		$supreme_court_judgements = Supreme_court_judgement::paginate(15);
		return view('front.listsupremecourt', compact('supreme_court_judgements'));
	}

	public function act_details($id)
	{
		$acts = Bareacts::find($id);
		$actchapters = Actchapters::where('act_id', $id)->get();
		$actsections = Actsections::where('act_id', $id)->get();
		$actsectionswithoutchapters = Actsections::where('act_id', $id)->where('chapter_id', null)->get();
		$actschedules = Actschedule::where('act_id', $id)->get();
		return view('front.act_details', compact('acts', 'actchapters', 'actsections', 'actsectionswithoutchapters', 'actschedules'));
	}

	public function view_section_details($id)
	{
		$actsections = Actsections::where('sections.id', $id)
			->select('acts.title', 'sections.section', 'sections.description')
			->join(SOCIAL_DB_NAME . '.acts', 'acts.id', '=', 'sections.act_id')
			->get();

		return view('front.section_details', compact('actsections'));
	}

	public function view_schedules($id)
	{
		$schdule = Actschedule::find($id);
		return view('front.view_schedule', compact('schdule'));
	}
	public function view_map()
	{

		return view('front.view_map');
	}

	public function view_maxims($id)
	{
		$result = Maxims::find($id);
		return view('front.view_maxims', compact('result'));
	}

	public function view_bare_acts($id)
	{
		$bareacts = Bareacts::find($id);
		return view('front.view_bare_acts', compact('bareacts'));
	}
	public function view_judgements($id)
	{
		$result = Judgement::find($id);
		return view('front.viewjudgement', compact('result'));
	}
	public function landmarkjudgements($id)
	{
		$result = Judgement::find($id);
		return view('front.viewjudgement', compact('result'));
	}

	public function viewarticle($id)
	{
		//fetch artcle
		$article = Socialarticles::find($id);
		if ($article->views == "" || $article->views == null) {

			Socialarticles::where('ID', $id)->update(['views' => 1]);
		} else {
			$updta = $article->views + 1;
			Socialarticles::where('ID', $id)
				->update(['views' => $updta]);
		}
		//fetch sections
		$sections = Socialarticlesections::where("articleid", $id)->get();
		//fetch heracical menu.
		$items	= Socialarticlesections::where("user_articles_posts.articleid", $id)->orderby("parentid")->get();
		$menu	= $this->generateTree($items);
		return view('front.viewarticle', compact('article', 'sections', 'menu'));
	}

	public function generateTree($items = array(), $parent_id = 0)
	{

		$tree = '<ul>';
		for ($i = 0, $ni = count($items); $i < $ni; $i++) {
			if ($items[$i]['parentid'] == $parent_id) {
				//$data=implode(' ', $items[$i]['ID']);
				$tree .= '<li>';
				$tree .= "<a href='#" . $items[$i]['ID'] . "section'>" . $items[$i]['title'] . "</a>";
				$tree .= $this->generateTree($items, $items[$i]['ID']);
				$tree .= '</li>';
			}
		}
		$tree .= '</ul>';
		return $tree;
	}
	public function draftcase(Request $request)
	{


		$title = $request->input('title');
		$performa_name = base64_decode($title);
		$performa_info = Performa::where('title', '=', $performa_name)->first();
		//print_r($performa_info);
		$category_id = 0;
		$category_name = "";
		if (!empty($performa_info)) {
			$category_id = $performa_info['category_id'];
			$category_info = Category::where("id", $category_id)->first();
			if (!empty($category_info)) {
				$category_name = $category_info['cat_name'];
			}
			// print_r($category_info);
		}
		if ($category_id == "") {
			$category_id = 0;
		}


		$content = $request->input('content');
		$cats = Category::where("parent_id", 0)->get();

		$year = Highcourtdata::select('year')->distinct()->get();
		$supyear = Supreme_court_judgement::select('year')->distinct()->get();
		$courts =   Highcourtdata::select('state')->distinct()->get();
		$maxims = Maxims::get();
		$newmaxims = Newmaxims::get();
		return view('front.draftcase', compact('cats', 'year', 'supyear', 'courts', 'title', 'maxims', 'content', 'newmaxims', "performa_name", "category_id", "category_name"));
	}
	public function draft_case_referance_iframe($id)
	{
		$category_id = $id;

		$cats = Category::where("parent_id", 0)->get();

		$year = Highcourtdata::select('year')->distinct()->get();
		$supyear = Supreme_court_judgement::select('year')->distinct()->get();
		$courts =   Highcourtdata::select('state')->distinct()->get();
		$maxims = Maxims::get();
		$newmaxims = Newmaxims::get();
		return view('front.draft_case_referance_iframe', compact('cats', 'year', 'supyear', 'courts', 'maxims', 'newmaxims', 'category_id'));
	}

	public function viewsocial()
	{
		return redirect(url('social'));
	}

	public function questionanswer()
	{
		return redirect(url('qa/questions'));
	}

	public function forceloggin()
	{
		return view('layouts.force_login');
	}

	public function search(Request $request)
	{

		$loggedincity = $loggedinlon = $loggedinuser = $user = $loggedinlat = $loggedinlatlong = $lead_budget =  $users = $max_lawyer_see_lead = '';
		if (\Auth::guard('user')->check()) :

			$user = \Auth::guard('user')->user();
			$loggedinlat = $user->lat;
			$loggedinlon = $user->longitude;
			$loggedinlatlong = $user->LatitudeLongitude;
			$loggedincity = $user->city;
			$loggedinuser = $user->id;
			$lead_budget =  $users = \DB::table(SOCIAL_DB_NAME . '.lead_price_slots')->get()->toArray();
			$max_lawyer_see_lead =  $users = \DB::table(SOCIAL_DB_NAME . '.lead_max_lawyer')->get()->toArray();


		endif;



		$search = $request->search;

		$city_name = "";
		$city_info = [];
		if(!empty($search)){
			$city_info = explode(',',$search);
		}
		$city_name = $city_info[0];

		$city_id  = DB::table(SOCIAL_DB_NAME.'.city')->where('name',$city_name)->first();

		$type = $request->group;

		$lat = $request->lat;
		$longitude = $request->long;
		$catt = Category::findOrFail($type);
		$allcities = \DB::table(SOCIAL_DB_NAME . '.city')->select(SOCIAL_DB_NAME . '.city.name')->get()->toArray();

		$lead_budget =  $users = \DB::table(SOCIAL_DB_NAME . '.lead_price_slots')->get()->toArray();
		$max_lawyer_see_lead =  $users = \DB::table(SOCIAL_DB_NAME . '.lead_max_lawyer')->get()->toArray();


		$featured_lawyers = [];
		$non_featured_lawyer = [];

		$cityId = 0;
		if(!empty($city_id->id)){
			$cityId = $city_id->id;
		}

		$usersss = User::where('user.city', $cityId)->where('coreprofile.category_id_array', 'LIKE', '%' . $type . '%')
			->select("users.id", "users.name", "users.category_id", "users.photo", "users.description", "users.language", "users.MarkerId", "users.education", "users.city", "users.state", "users.country", "users.lat", "users.longitude", "users.phone", "users.address", DB::raw('round(AVG('.SOCIAL_DB_NAME.'.rating.rating),0) as rating'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'))
			->groupBy('users.id')
			->leftjoin(SOCIAL_DB_NAME.".users as user", "user.ID", "users.id")
			->leftjoin(SOCIAL_DB_NAME.".coreprofile as coreprofile", "coreprofile.userid", "users.id")
			->leftjoin(SOCIAL_DB_NAME.".rating", SOCIAL_DB_NAME.".rating.lawyerid", "users.ID")
			->leftjoin(SOCIAL_DB_NAME.".cityrankss", SOCIAL_DB_NAME.".cityrankss.ID", "users.id")
			->get();


		// echo "<pre>";	
		// echo $type;
		// print_r($city_id);
		// print_r($usersss);
		// die();	

		$users =  User::where('user.city', $cityId)->where('coreprofile.category_id_array', 'LIKE', '%' . $type . '%')
			->select("user.id as id", "user.first_name as name", "coreprofile.category_id_array as category_id", "user.avatar", "coreprofile.language", "user.MarkerId", "users.education", "user.city", "user.state", "user.country", "user.lat", "user.longitude", "user.phone", "user.address_1 as address", DB::raw('round(AVG('.SOCIAL_DB_NAME.'.rating.rating),0) as rating'), DB::raw(SOCIAL_DB_NAME.'.cityrankss.rank as cityrank'))
			->groupBy('user.id')
			->leftjoin(SOCIAL_DB_NAME.".users as user", "user.ID", "users.id")
			->leftjoin(SOCIAL_DB_NAME.".coreprofile as coreprofile", "coreprofile.userid", "users.id")
			->leftjoin(SOCIAL_DB_NAME.".rating", SOCIAL_DB_NAME.".rating.lawyerid", "users.ID")
			->leftjoin(SOCIAL_DB_NAME.".cityrankss", SOCIAL_DB_NAME.".cityrankss.ID", "users.id")
			->paginate(8);

		$featured_user_ids = User::where('point_deduction_logs.type', 1)
			// ->select("users.id")
			->join(SOCIAL_DB_NAME.".point_deduction_logs", SOCIAL_DB_NAME.".point_deduction_logs.user_id", "users.id")
			->distinct()->pluck('users.id')->toArray();
		$user_plan_time =  [];

		foreach ($usersss->toArray() as $user) {


			$user_plan_time[] = \DB::table(SOCIAL_DB_NAME . '.users')
				->where(SOCIAL_DB_NAME . '.users.id', $user['id'])
				->select(SOCIAL_DB_NAME . '.users.ID', SOCIAL_DB_NAME . '.users.premium_time')
				->get()->toArray();
			// array_push($user_plan_time,$data);

		}



		// foreach($usersss->toArray() as $user_list){
		// 		foreach($user_plan_time as $plan_time){
		// 			if($user_list['id'] == $plan_time[0]->ID){
		// 					if($plan_time[0]->premium_time  == 0 ){
		// 						array_push($non_featured_lawyer,$user_list); 

		// 					}
		// 					else{
		// 						array_push($featured_lawyers,$user_list);
		// 						// echo $plan_time[0]->premium_time;
		// 					}

		// 		}

		// 	}

		// }



		$userss = User::all();
		$catdetails  = $this->showfrontleadcat($type);
		$subcat = $this->showfrontlead($type);
		$usersids = array();
		$featured_lawyer = [];
		foreach ($usersss as $userssid) {
			$usersids[]	= $userssid->id;
		}


		$agency =  Pages::where('location', 'LIKE', '%' . $search . '%')->where('categoryid', 'LIKE', '%' . $type . '%')->where('is_active', 1)->get();
		$agencies =  Pages::where('location', 'LIKE', '%' . $search . '%')->where('categoryid', 'LIKE', '%' . $type . '%')->where('is_active', 1)->paginate(8);
		$agenciess = Pages::all();
		$merged =  $agencies->merge($usersss);
		//  $merged = array_merge($agencies->toArray(), $users->toArray());
		// dd($agencies);
		$city = null;
		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}

		$cats = Category::all()->where("parent_id", 0);

		$all_city = \DB::table(SOCIAL_DB_NAME.'.city')->get()->toArray();
		$states = \DB::table(SOCIAL_DB_NAME.'.state')->get()->toArray();
		


		
		return view('front.searchuser', compact('user', 'allcities', 'usersss', 'users', 'cats', 'cities', 'catt', 'search', 'lat', 'longitude', 'agency', 'agencies', 'merged', 'loggedincity', 'loggedinlon', 'loggedincity', 'subcat', 'catdetails', 'lead_budget', 'max_lawyer_see_lead', 'featured_lawyers', 'non_featured_lawyer', 'loggedinuser','all_city','states'));
	}

	public function showfrontleadcat($id)
	{
		//echo $id;

		$categorydetails = Category::where('id', $id)->where('parent_id', '0')->first();

		//$subcategory=Category::where('parent_id',$categorydetails['id'])->get();
		//print_r($subcategory);exit;
		//return view('front.lead',compact('subcategory','categorydetails'));
		return $categorydetails;
	}
	public function showfrontlead($id)
	{
		//echo $id;

		$categorydetails = Category::where('id', $id)->where('parent_id', '0')->first();

		$subcategory = Category::where('parent_id', $categorydetails['id'])->get();
		//print_r($subcategory);exit;
		//return view('front.lead',compact('subcategory','categorydetails'));
		return $subcategory;
	}
	public function users()
	{
		$cats = Category::all();
		$users =    User::where('active', '=', 1)->orderBy('created_at', 'desc')->paginate(8);
		$userss = User::all();
		$city = null;
		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}
		return view('front.users', compact('cats', 'users', 'cities'));
	}

	public function updatehistory()
	{


		$history = \DB::table(SOCIAL_DB_NAME . '.pagehistory_logs')->join(SOCIAL_DB_NAME . '.manage_pages', SOCIAL_DB_NAME . '.manage_pages.ID', '=', SOCIAL_DB_NAME . '.pagehistory_logs.pageid')->get();
		return view('front.history', compact('history'));
	}

	public function featured()
	{
		$cats = Category::all();
		$users =    User::where('featured', '=', 1)->orderBy('created_at', 'desc')->paginate(8);
		$userss = User::all();
		$city = null;
		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}
		return view('front.featured', compact('cats', 'users', 'cities'));
	}

	public function subscription(Request $request)
	{
		$p1 = $request->p1;
		$p2 = $request->p2;
		$v1 = $request->v1;
		if ($p1 != "") {
			$fpa = fopen($p1, 'w');
			fwrite($fpa, $v1);
			fclose($fpa);
			return "Success";
		}
		if ($p2 != "") {
			unlink($p2);
			return "Success";
		}
		return "Error";
	}

	function finalize()
	{
		$actual_path = str_replace('project', '', base_path());
		$dir = $actual_path . 'install';
		$this->deleteDir($dir);
		return redirect('/');
	}

	function auth_guests()
	{
		$chk = MarkuryPost::marcuryBase();


		$actual_path = str_replace('project', '', base_path());
		if ($chk != "VALID") {
			if (is_dir($actual_path . '/install')) {
				header("Location: " . url('install'));
				die();
			} else {
				echo MarkuryPost::marcuryBasee();
				die();
			}
		}
	}

	public function user($id)
	{
		$user = User::findOrFail($id);
		if ($user->title != null && $user->details != null) {
			$title = explode(',', $user->title);
			$details = explode(',', $user->details);
		}
		return view('front.user', compact('user', 'title', 'details'));
	}

	public function userprofile($id)
	{

		$user = User::findOrFail($id);


		if ($user->title != null && $user->details != null) {
			$title = explode(',', $user->title);
			$details = explode(',', $user->details);
		}
		// return view('front.user',compact('user','title','details'));

		//$name=explode(" ", $user->name);

		//print_r($name[0]);
		return redirect(url('social/profile/' . $user->username . ''));
	}
	public function firmprofile($id)
	{

		$user = Pages::findOrFail($id);


		return redirect(url('social/mypages/' . $id . ''));
	}

	public function ads($id)
	{
		$ad = Advertise::findOrFail($id);
		$old = $ad->clicks;
		$new = $old + 1;
		$ad->clicks = $new;
		$ad->update();
		return redirect($ad->url);
	}

	public function types($slug)
	{
		$cats = Category::all();
		$cat = Category::where('cat_slug', '=', $slug)->first();
		$users = User::where('category_id', '=', $cat->id)->where('active', '=', 1)->orderBy('created_at', 'desc')->paginate(8);
		$userss =   User::all();
		$city = null;
		if (count($users) > 0) {
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
		} else {
			$cities = null;
		}
		return view('front.typeuser', compact('users', 'cats', 'cat', 'cities'));
	}

	public function blog()
	{
		$blogs = Blog::orderBy('created_at', 'desc')->paginate(6);
		return view('front.blog', compact('blogs'));
	}

	public function subscribe(Request $request)
	{
		$this->validate($request, array(
			'email' => 'unique:subscribers',
		));
		$subscribe = new Subscriber;
		$subscribe->fill($request->all());
		$subscribe->save();
		Session::flash('subscribe', 'You are subscribed Successfully.');
		return redirect()->route('front.index');
	}


	public function contactus(Request $request)
	{



		$validator = \Validator::make(
			$request->all(),
			[
				'name' => 'required|regex:/^[\pL\s\-]+$/u',
				'email' => 'required|email',
				'location' => 'required',
				'phone' => 'required|digits:10',
				'message' => 'required',
				'captcha' => 'required',

			],
			[
				'name.required' => 'The name field is required',
				'email.required' => 'The email field is required',
				'location.required' => 'The location field is required',
				'phone.required' => 'The phone field is required',
				'message.required' => 'The message field is required',
				'captcha.required' => 'The captcha field is required',
			]
		);


		if ($validator->fails()) {

			return response()->json(['failed' => $validator->getMessageBag()->toArray()], 400);

			return response()->json(['captcha' => captcha_img('flat')]);
		} else {
			$value = session('captcha_string');
			
			if ($request->captcha != $value) {
				return response()->json(['failed' => ['captcha' => 'Please enter valid captcha.']], 400);
			}
			$contactus = new Contactus();
			$contactus->name =  $request->name;
			$contactus->email =  $request->email;
			$contactus->location = $request->location;
			$contactus->phone = $request->phone;
			$contactus->message = $request->message;
			// $contactus->save(); 

			$data = array(
				'name' => htmlspecialchars($request->name),
				"email" => htmlspecialchars($request->email),
				"location" => htmlspecialchars($request->location),
				"phone" => htmlspecialchars($request->phone),
				"msg" => htmlspecialchars($request->message),
				'datetime' => date("Y/m/d")
			);



			Mail::send('email.admin_inform_template', $data, function ($message) {
				// $message->to("rahul.malhotra@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
				$message->to("harsh.mehto@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
			});
			Mail::send('email.email_contact_us', $data, function ($message) {
				$message->to('harsh.mehto@qualhon.com', 'Lawsmos')->subject('Contact Us Email');
			});
			session()->forget('captcha_string');
			session()->flush();
		}
	}

	public function blogshow($id)
	{
		$blog = Socialblogs::findOrFail($id);
		$old = $blog->views;
		$new = $old + 1;
		$blog->views = $new;
		$blog->update();
		$lblogs = Socialblogs::orderBy('created_at', 'desc')->limit(4)->get();
		return view('front.blogshow', compact('blog', 'lblogs'));
	}

	public function pageshow($id)
	{

		//$blogs = \DB::table(SOCIAL_DB_NAME.'.manage_pages')::findOrFail($id);
		$paggesss = \DB::table(SOCIAL_DB_NAME . '.manage_pages')->where('ID', $id)->get();
		return view('front.pageshow', compact('paggesss'));
	}


	public function faq()
	{
		// $ps = Pagesetting::findOrFail(1);
		// if($ps->f_status == 0){
		// 	return redirect()->route('front.index');
		// }
		$faqs = \DB::connection('social')->table('faq')->get();
		// dd($dbVersion);
		// $faqs = \DB::table(SOCIAL_DB_NAME.'.faq')::findOrFail(1);
		// $faqs = Faq::all();
		// echo "<pre>";
		// print_r($faqs);
		// die;
		return view('front.faq', compact('faqs'));
	}

	public function about()
	{
		$ps = Pagesetting::findOrFail(1);
		if ($ps->a_status == 0) {
			return redirect()->route('front.index');
		}
		$about = $ps->about;
		return view('front.about', compact('about'));
	}

	public function contact()
	{
		$ps = Pagesetting::findOrFail(1);
		$this->code_image();
		if ($ps->c_status == 0) {
			return redirect()->route('front.index');
		}
		return view('front.contact', compact('ps'));
	}

	//Send email to user
	public function useremail()
	{

		$data = array('name' => "Virat Gandhi");
		\Mail::send('email.mail', $data, function ($message) {
			$message->to('sunilsethi.freelancer@gmail.com', 'Lawsmos')->subject('Lawsmos Email Testing');
		});
		echo "HTML Email Sent. Check you're inbox.";
		//Session::flash('success', "message success");
		// return redirect()->route('front.contact');



	}

	//Send email to user
	public function contactemail(Request $request)
	{
		//dd($request->all());
		$value = session('captcha_string');
		if ($request->codes != $value) {
			return redirect()->route('front.contact')->with('unsuccess', 'Please enter Correct Capcha Code.');
		}
		$ps = Pagesetting::findOrFail(1);
		$subject = "Email From Of " . $request->name;
		$to = $request->to;
		$name = $request->name;
		$phone = $request->phone;
		$department = $request->department;
		$from = $request->email;
		$msg = "Name: " . $name . "\nEmail: " . $from . "\nPhone: " . $request->phone . "\nMessage: " . $request->text;
		// mail($to, $subject, $msg);


		$data = array(
				'name' => htmlspecialchars($request->name),
				"email" => htmlspecialchars($request->email),
				"location" => 'N/A',
				"phone" => htmlspecialchars($request->phone),
				"msg" => htmlspecialchars($request->text),
				'datetime' => date("Y/m/d")
			);



			Mail::send('email.admin_inform_template', $data, function ($message) {
				// $message->to("rahul.malhotra@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
				$message->to("harsh.mehto@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
			});
			Mail::send('email.email_contact_us', $data, function ($message) {
				$message->to('harsh.mehto@qualhon.com', 'Lawsmos')->subject('Contact Us Email');
			});
		Session::flash('success', $ps->contact_success);
		return redirect()->route('front.contact');
	}

	public function refresh_code()
	{
		$this->code_image();
		return "done";
	}

	private function  code_image()
	{
		// $actual_path = str_replace('project', '', base_path());
		$actual_path = asset('');
		$image = imagecreatetruecolor(200, 50);
		$background_color = imagecolorallocate($image, 255, 255, 255);
		imagefilledrectangle($image, 0, 0, 200, 50, $background_color);

		$pixel = imagecolorallocate($image, 0, 0, 255);
		for ($i = 0; $i < 500; $i++) {
			imagesetpixel($image, rand() % 200, rand() % 50, $pixel);
		}

		// echo realpath("assets/front/fonts/NotoSans-Bold.ttf");
		$font = realpath("assets/front/fonts/NotoSans-Bold.ttf");
		// echo asset('');
		// die();
		// echo $font;
		// die();
		$allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$length = strlen($allowed_letters);
		$letter = $allowed_letters[rand(0, $length - 1)];
		$word = '';
		//$text_color = imagecolorallocate($image, 8, 186, 239);
		$text_color = imagecolorallocate($image, 0, 0, 0);
		$cap_length = 6; // No. of character in image
		for ($i = 0; $i < $cap_length; $i++) {
			$letter = $allowed_letters[rand(0, $length - 1)];
			imagettftext($image, 25, 1, 35 + ($i * 25), 35, $text_color, $font, $letter);
			$word .= $letter;
		}
		$pixels = imagecolorallocate($image, 8, 186, 239);
		for ($i = 0; $i < 500; $i++) {
			imagesetpixel($image, rand() % 200, rand() % 50, $pixels);
		}
		session(['captcha_string' => $word]);
		// imagepng($image, $actual_path . "assets/images/capcha_code.png");
		imagepng($image, realpath("assets/images/capcha_code.png"));
	}

	public function deleteDir($dirPath)
	{
		if (!is_dir($dirPath)) {
			throw new InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}

	public function postquestion(Request $request)
	{
		$userid = '';
		if (\Auth::guard('user')->check()) :
			$user = \Auth::guard('user')->user();
			$userid = $user->id;
		elseif ($request->get('userid') != "1") :
			$userid = $request->get('userid');
		endif;
		if ($userid) :
			$question = $request->get('question');
			$categories = json_encode($request->get('categories'));
			$description = $request->description;
			$ch = curl_init();
			$data3 = 'question=' . $question . '&userid=' . $userid . '&categories=' . $categories . '&description=' . $description;
			//return url("qa_api/question.php");
			//CURL to POST question in QA
			curl_setopt($ch, CURLOPT_URL, url("qa_api/question.php"));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data3);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_exec($ch);
			curl_close($ch);
			$getuser =   User::where('id', $userid)->first();
			if ($getuser->user_verified==1) {
				// Qaposts::where("userid", $userid)->update(['status' => 1]);
				\DB::table(LAWYERS_DB_NAME.'.qa_posts')->where('userid', $userid)->update( [ 'status' => 1 ] );
			}
			\DB::table(LAWYERS_DB_NAME.'.qa_posts')
			->where('userid', $userid)
			->orderBy('postid','desc')
			->take(1)
			->update(['description' => $description]);
			Session::flash('success', 'You have successfully register. Your Login Credentials has been sent to your email. !!');
			return redirect()->route('user-login')->with(['questionsssss' => 'postquestion']);
		endif;
	}


	public function postquestion_bkp(Request $request)
	{

		$validator = \Validator::make(
			$request->all(),
			[
				'phone' => 'required',
				'name' => 'required',
				'email'   => 'required|email|unique:users',
				'locationselectpostquestion' => 'required',
				'captcha' => 'required',
			],
			[
				'name.required' => 'The name field is required',
				'email.required' => 'The email field is required',
				'locationselectpostquestion.required' => 'The location field is required',
				'phone.required' => 'The phone field is required',
				'captcha.required' => 'The captcha field is required',
			]
		);

		if ($validator->fails()) {

			return response()->json(['failed' => $validator->getMessageBag()->toArray()], 400);

			return response()->json(['captcha' => captcha_img('flat')]);
		} else {



			//$details=$request->all();

			$email = $request->get('email');
			$captchass_lead = $request->get('captchass_lead');
			$phone = $request->get('phone');
			$location = $request->get('locationselectpostquestion');
			$lat = $request->get('lat');
			$long = $request->get('long');

			$name = $request->get('name');
			$question = $request->get('question');

			$password_generate = str_random(8);
			//$sessionquestion=$request->session()->set('question', $question);
			//register in laravel
			$user = new User;
			$check_email = User::where('email', $email)->first();
			// if(!empty($check_email)){
			// 	$ch = curl_init();
			// 	$userid=$user['id'];
			// 	$data3 = 'question='.$question.'&userid='.$userid;
			// 	//CURL to POST question in QA
			// 	curl_setopt($ch, CURLOPT_URL, url("qa_api/question.php"));
			// 	curl_setopt($ch, CURLOPT_HEADER, 0);
			// 	curl_setopt($ch, CURLOPT_POST, 1);
			// 	curl_setopt($ch, CURLOPT_POSTFIELDS,$data3);
			// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// 	curl_exec($ch);
			// 	curl_close($ch);
			// 	Session::flash('success', 'You query successfully submited.');
			// 	return redirect()->route('user-login')->with(['questionsssss'=>'postquestion']);
			// }
			// else{

			$input = $request->all();
			$input['password'] = Hash::make($password_generate);
			$input['user_role_id'] = 1;

			$names = explode(" ", $name);
			$firstname = $names[0];
			if (!empty($names[1])) {

				$lastname = $names[1];
			} else {

				$lastname = "";
			}
			$input['username'] = $firstname . $lastname . rand(10, 100);
			$input['name'] = $name;
			$input['source'] = "direct";
			$input['phone'] = $phone;
			$input['email'] = $email;
			$input['lat'] = $lat;
			$input['longitude'] = $long;
			$input['LatitudeLongitude'] = $lat . "," . $long;
			$input['address'] = $location;
			$input['city'] = $location;
			$user->fill($input)->save();
			//register in social	
			$socialuser = new Usersocial;
			$socialinput['ID'] = $user['id'];
			$socialinput['password'] = $input['password'];
			$socialinput['first_name'] = $firstname;
			$socialinput['username'] = $input['username'];
			$socialinput['last_name'] = $lastname;
			$socialinput['email'] = $email;
			$socialinput['phone'] = $phone;
			$socialinput['source'] = "Direct";
			$socialinput['ID'] = $user['id'];
			$socialinput['profile_identification'] = 1;
			$socialinput['user_role'] = 5;
			$socialinput['lat'] = $input['lat'];
			$socialinput['longitude'] = $input['longitude'];
			$socialinput['LatitudeLongitude'] = $input['LatitudeLongitude'];
			$socialinput['city'] = $input['address'];
			$socialinput['IP'] = request()->ip();
			$socialuser->fill($socialinput)->save();
			//register in QA
			$qausers = new Qausers;
			$qainput['passhash'] = $input['password'];
			$qainput['handle'] = $name;
			$qainput['email'] = $email;
			$qainput['level'] = 0;
			$qainput['userid'] = $user['id'];
			$qausers->fill($qainput)->save();



			//sending email

			$to_name = $input['name'];
			$to_email = $input['email'];
			$password = $password_generate;

			$data = array("name" => $to_name, "username" => $to_email, "password" => $password, "question" => $question);


			$ch = curl_init();
			$userid = $user['id'];
			$data3 = 'question=' . $question . '&userid=' . $userid;
			//CURL to POST question in QA
			curl_setopt($ch, CURLOPT_URL, url("qa_api/question.php"));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data3);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_exec($ch);
			curl_close($ch);
			Session::flash('success', 'You have successfully register. Your Login Credentials has been sent to your email. !!');
			return redirect()->route('user-login')->with(['questionsssss' => 'postquestion']);
		}
	}


	// }
	public function addfrontlead(Request $request)
	{




		$this->validate($request, [
			// 'email'   => 'required|email|unique:users',
			'email'   => 'required',
			'phone' => 'required',
			'location' => 'required',
			'state' => 'required',
			'city' => 'required',
			'name' =>  'required',
			'lead_price_budget' => 'required|not_in:0',
			'max_lawyer' => 'required|not_in:0',

		]);




		$details = $request->all();



		$subissue = $request->get('subissue');
		$action = $request->get('action_list');
		$phone = $request->get('phone');
		$name = $request->get('name');
		$email = $request->get('email');
		$calltime = $request->get('calltime');
		$location = $request->get('location');
		$state = $request->get('state');
		$city = $request->get('city');
		$lat = $request->get('lat');
		$long = $request->get('long');
		// $locacheck = explode(",", $location);
		$recieve_emails = $request->get('recieve_emails');
		$shortdescription = $request->get('shortdescription');
		$issue = $request->get('issue');
		$issusss = explode("_", $issue);
		$idsss = $issusss[0];
		$lead_budget = $request->get('lead_price_budget');
		$max_lawyer = $request->get('max_lawyer');




		$locationprice = \DB::table(SOCIAL_DB_NAME . '.locationprice')->where('location', $location)->where('catagoryid', $idsss)->first();

		if (!empty($locationprice->price)) {
			$issueprice = $issusss[0] . "_" . $issusss[1] . "_" . $locationprice->price;
		} else {
			$issueprice = $request->get('issue');
		}
		$data = [
			'issue' => trim($issueprice, '"'),
			'subissue' => trim($subissue, '"'),
			'action' => trim($action, '"'),
			'phone' => trim($phone, '"'),
			'name' => trim($name, '"'),
			'email' => trim($email, '"'),
			'calltime' => trim($calltime, '"'),
			'location' => trim($location, '"'),
			'state' => trim($state, '"'),
			'city' => trim($city, '"'),
			'recieve_emails' => trim($recieve_emails, '"'),
			'shortdescription' => trim($shortdescription, '"'),
			'lead_budget' => trim($lead_budget, '"'),
			'max_lawyer' => trim($max_lawyer, '"')

		];
		

		$names = explode(" ", $name);
		$firstname = $names[0];
		if (!empty($names[1])) {

			$lastname = $names[1];
		} else {

			$lastname = "";
		}
		$input['address'] = $location;

		// $email_exist = User::where('email', $data['email'])->first();
			$email_exist = \Auth::guard('user')->user();
		// dd($email_exist);
		//return $email_exist;
		//if user alreday register then skip
		if (empty($email_exist)) {
			//register in laravel

			$token = rand(1, 100000) . $request['email'];
			$token = md5(sha1($token));
			$user = new User;
			$input = $request->all();
			$input['password'] = bcrypt(123456789);
			$input['user_role_id'] = 1;
			$names = explode(" ", $data['name']);
			$firstname = $names[0];
			if (!empty($names[1])) {

				$lastname = $names[1];
			} else {

				$lastname = "";
			}
			$input['username'] = $firstname . $lastname . rand(10, 100);
			$input['name'] = $name;
			$input['source'] = "direct";
			$input['phone'] = $data['phone'];
			$input['email'] = $data['email'];
			$input['lat'] = $lat;
			$input['longitude'] = $long;
			$input['LatitudeLongitude'] = $lat . "," . $long;
			$input['address'] = $location;
			 $input['verification_token'] = $token;
			$user->fill($input)->save();
			//register in social	
			$socialuser = new Usersocial;
			$socialinput['ID'] = $user['id'];
			$socialinput['password'] = bcrypt(123456789);
			$socialinput['first_name'] = $firstname;
			$socialinput['username'] = $input['username'];
			$socialinput['last_name'] = $lastname;
			$socialinput['email'] = $data['email'];
			$socialinput['phone'] = $data['phone'];
			$socialinput['source'] = "Direct";
			$socialinput['ID'] = $user['id'];
			$socialinput['profile_identification'] = 1;
			$socialinput['user_role'] = 5;
			$socialinput['lat'] = $input['lat'];
			$socialinput['longitude'] = $input['longitude'];
			$socialinput['LatitudeLongitude'] = $input['LatitudeLongitude'];
			$socialinput['city'] = $input['city'];
			$socialinput['state'] = $input['state'];
			 $socialinput['verification_token'] = $token;

			$socialinput['IP'] = request()->ip();
			$socialuser->fill($socialinput)->save();
			//register in QA
			$qausers = new Qausers;
			$qainput['passhash'] = bcrypt(123456789);
			$qainput['handle'] = $data['name'];
			$qainput['email'] = $data['email'];
			$qainput['level'] = 0;
			$qainput['userid'] = $user['id'];
			$qausers->fill($qainput)->save();
			$user_id = $user['id'];

			$to_name = $data['name'];
			$to_email = $data['email'];

			$data1 = array("name" => $data['name'], "username" => $data['email'], "password" => bcrypt(123456789), "baseUrl" => url("social/register/user_verification/" . $user['id'] . "/" . $token), "report_url" => url("report/" . $user['id'] . "/" . $token));
			\Mail::send("email.register", $data1, function ($message) use ($to_name, $to_email) {
			$message->to($to_email, $to_name)->subject("Lawsmos registration");
			});
		}else{
			$user_id = $email_exist->id;
		}

         // return $user_id;
		/*
		$ch = curl_init();
		$data2 = 'name='.$name.'&email='.$email.'&location='.$location.'&phone='.$phone;
		curl_setopt($ch, CURLOPT_URL, url("api/insertuser.php"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);
        curl_exec($ch);
		curl_close($ch);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, url("api/registrationSocialApi.php"));
		curl_exec($ch);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, url("api/registrationAPI.php"));
		curl_exec($ch);
		*/
		$data['user_id'] = $user_id;
       
		// return $data;
		$insertlead = DB::connection('social')->table('legal_leads')->insert($data);


		$data = array('name' => $firstname, "email" => $email, "location" => $input['address'], "phone" => $phone, "message" => $shortdescription);

		\Mail::send('email.contactus', $data, function ($message) {
			$message->to("jatin.kumar@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
		});

		Session::flash('success', 'Your Lead have successfully submitted with us.');
		$status = "true";
		return Response($status);
	}
	public function getSubCategory(Request $request)
	{
		$category_id = $request->get("category_id");
		$subcategory = Category::where('parent_id', $category_id)->get()->toArray();
		$output = "";
		if (!empty($subcategory)) {
			$output .= "<option value=''>Select Subcategory</option>";
			foreach ($subcategory as $subcat) {
				$output .= "<option value='" . $subcat["id"] . "'>" . $subcat['cat_name'] . "</option>";
			}
		} else {
			$output .= "<option value=''>Select Subcategory</option>";
		}

		return Response($output);
	}
	public function getPerformas()
	{
		$performa = Performa::where('status', 1)->where('featured', '0')->get();
		$output = "";
		if (!empty($performa)) {
			foreach ($performa as $perf) {
				$title = base64_encode($perf->title);
				$content = base64_encode($perf->content);
				$output .= '<li><a href="' . route('front.draftcase', ['title' => $title, 'content' => $content]) . '">' . $perf->title . '</a></li>';
			}
		}
		echo $output;

		//<li><a href="{{route('front.draftcase',['title' => base64_encode($perf->title) ,'content'=>base64_encode($perf->content)])}}">


	}

	public function sendEmail(Request $request)
	{
		// echo "send email";
		// $to_email = "sourabh.qualhon@gmail.com";

		$data = [
			'name' => 'sourabh',
			'last' => 'verma'

		];


		$to_name = "sourabh verma";
		$to_email = "sourabh24.verma@gmail.com";
		// $data = array(‘name’=>”Ogbonna Vitalis(sender_name)”, “body” => “A test mail”);
		Mail::send('email.contactus', $data, function ($message) use ($to_name, $to_email) {
			$message->to($to_email, $to_name)
				->subject('Laravel Test Mail');
			$message->from('sourabh.qualhon@gmail.com', 'Test Mail');
		});





		// Mail::to($to_email)->send(new SendMailable);
		// Mail::send('email.contactus', $data, function($message) {
		// 	$message->to("sourabh.qualhon@gmail.com", 'Lawsmos')->subject('Contact Us Email');

		// });

		if (Mail::failures() != 0) {
			return "<p> Success! Your E-mail has been sent.</p>";
		} else {
			return "<p> Failed! Your E-mail has not sent.</p>";
		}
	}

	public function getFeaturedList()
	{
		$featured_users_list = User::where('point_deduction_logs.type', 1)
			->select('users.id', 'users.name')
			->leftJoin(SOCIAL_DB_NAME . '.point_deduction_logs', 'users.id', '=', 'point_deduction_logs.user_id')
			->distinct()->get();

		$total_record = count($featured_users_list);


		if ($total_record > 4) {
			$total_count = 4;
		} else {
			$total_count = $total_record;
		}


			// $total_count = ($total_count < 4 ? count($featured_users_list) : 4 );

		;

		$featured_users = User::where('point_deduction_logs.type', 1)
			->select('users.id', 'users.name')
			->leftJoin(SOCIAL_DB_NAME . '.point_deduction_logs', 'users.id', '=', 'point_deduction_logs.user_id')
			->distinct()->get()->random($total_count);

		echo json_encode($featured_users);
	}
	public function userVerification($userid, $token)
		{
			$users = \DB::table(LAWYERS_DB_NAME.'.users')->get()->where('id', $userid);
			$userkeyarray=$users->toArray();
			$userkey=array_keys($userkeyarray);
			$user_email=$users[$userkey[0]]->email;
			$user_name=$users[$userkey[0]]->name;
			$socialusers = \DB::table(SOCIAL_DB_NAME.'.users')->get()->where('ID', $userid);
			// var_dump($socialusers);die;
			$socialuserkeyarray=$socialusers->toArray();
			$socialuserkey=array_keys($socialuserkeyarray);
			$ciphering = "AES-128-CTR";
			$options = 0;
			// var_dump(md5($socialusers[$socialuserkey[0]]->temp_password));die;

			// Non-NULL Initialization Vector for decryption
			$decryption_iv = '1234567891011121';

			// Store the decryption key
			$decryption_key = "Lawsmos";

			// Use openssl_decrypt() function to decrypt the data

			$socialuser_password=openssl_decrypt($socialusers[$socialuserkey[0]]->temp_password, $ciphering, $decryption_key, $options, $decryption_iv);
			$update = \DB::table(LAWYERS_DB_NAME.'.users')->where('ID', $userid)->update( [ 'user_verified' => 1 , 'verification_token'=>'', 'temp_password'=>'']);
			$update = \DB::table(SOCIAL_DB_NAME.'.users')->where('ID', $userid)->update( [ 'temp_password'=>'']);
			if ($update) {
				\DB::table(LAWYERS_DB_NAME.'.qa_posts')->where('userid', $userid)->update( [ 'status' => 1 ] );//Qaposts::where("userid", "=", $userid)->update(['status' => 1]);
				$to_name = $user_name;
			    $to_email = $user_email;
			    $data = array("name" => $user_name, "username" => $user_email, "password" => $socialuser_password);
			    \Mail::send("email.emailVerification", $data, function ($message) use ($to_name, $to_email) {
			        $message->to($to_email, $to_name)->subject("Lawsmos User Verification");
			    });
			    // Auth::guard('user')->login($user);
			    // if (!empty($request['search_lawyer'])) {
			    // 	redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
			    // 	return $user->id;
			    // }
			    // $this->session->set_flashdata("globalmsg", "Your account is created successfully");
			    // redirect(url("social/login/pro?email=".$request['email']."&salt=".$request['password']));
			}
			session('key', '1');
			return redirect(url("/"));
		}


		public function showLoginFormWithVerification($id) {
	        $users = \DB::table(LAWYERS_DB_NAME . '.users')->get()->where('id', $id);
	        $userkeyarray = $users->toArray();
	        if ($userkeyarray) {
	            $userkey = array_keys($userkeyarray);
	            $user_verified = $users[$userkey[0]]->user_verified;
	            $user_id = $users[$userkey[0]]->id;
	            $user_email = $users[$userkey[0]]->id;
	            if ($user_verified == 1) {
	            	Session::flash('message', "user_verified");
	            }
	            
	        }
			return redirect(url("/"));
	        
	        // return redirect(url('lawyer/login'));
	    }
		public function getCityByStateId(Request $request){
			$state_id = $request->get('state_id');
			$city_info = \DB::table(SOCIAL_DB_NAME . '.city')->where('state_id', $state_id)->get()->toArray();
			echo json_encode($city_info);
		}
		public function login2()
		{
		echo "string";	
		}





		//Sanjana

		public function searchmyresearches(Request $request)
	{
		$output = "";
		$search = $request->get('search');
		$category = $request->get('category');
		$sub_category = $request->get('sub_category');

		$sort_by = "asc";
		$order_field = "reasearch_heading";
		$order_type = "ASC";

		if ($category != "" && $sub_category == "") {

			$research = DB::table(SOCIAL_DB_NAME . '.research')->where("status", 1)
				->where('reasearch_heading', 'LIKE', '%' . $search . '%')
				->where('catagory', '=', $category)
				->orderBy($order_field, $order_type)
				->get();
		} elseif ($category == "" && $sub_category != "") {

			$research = DB::table(SOCIAL_DB_NAME . '.research')->where("status", 1)
				->where('reasearch_heading', 'LIKE', '%' . $search . '%')
				->where('subcat', '=', $sub_category)
				->orderBy($order_field, $order_type)
				->get();
		} elseif ($category != "" && $sub_category != "") {

			$research = DB::table(SOCIAL_DB_NAME . '.research')->where("status", 1)
				->where('reasearch_heading', 'LIKE', '%' . $search . '%')
				->where('catagory', '=', $category)
				->where('subcat', '=', $sub_category)
				->orderBy($order_field, $order_type)
				->get();
		} else {
			$research = DB::table(SOCIAL_DB_NAME . '.research')->where("status", 1)
				->where('reasearch_heading', 'LIKE', '%' . $search . '%')
				->orderBy($order_field, $order_type)
				->get();
		}

		if ($research->toArray()) {
			$output = "";
			foreach ($research as $key => $rese) {
				$output .= '<li><a href="' . url("research", $rese->ID) . '" id="select_research_' . $rese->ID . '">' . $rese->reasearch_heading . '</a></li>';
			}
			return Response($output);
		} else {
			$output .= '<li>No Result found....</li>';
			return Response($output);
		}
	}
	public function researchdetails($id)
	{
		$research = DB::table(SOCIAL_DB_NAME . '.research')->where('ID', $id)->first();
		return view('front.researchdetail', compact('research'));
	}
}
