<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Blog;
use App\Http\Requests\UpdateValidationRequest;
use Auth;
class UserBlogController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth:admin');
    }

  public function index()
    {
		
		$user = Auth::guard('user')->user();
		$user->id;
        $blogs = Blog::orderBy('id','desc')->get()->where('created_by_user', $user->id);
        return view('user.blog.index',compact('blogs'));
    }


    public function create()
    {
        return view('user.blog.create');
    }
    public function store(UpdateValidationRequest $request)
    {
		
		
		$user = Auth::guard('user')->user();
		$user->id;
		
        $blog = new Blog();
        $data = $request->all();
		$data['created_by_user']= $user->id;
        if ($file = $request->file('photo')) 
         {      
            $name = time().$file->getClientOriginalName();
            $file->move('assets/images',$name);           
            $data['photo'] = $name;
        } 
        $blog->fill($data)->save();
        return redirect()->route('user-blogs-index')->with('success','New Blog Added Successfully.');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('user.blog.edit',compact('blog'));
    }

    public function update(UpdateValidationRequest $request, $id)
    {

		$user = Auth::guard('user')->user();
		$user->id;

        $blog = Blog::findOrFail($id);
        $data = $request->all();
		$data['created_by_user']= $user->id;
		
		
            if ($file = $request->file('photo')) 
            {              
                $name = time().$file->getClientOriginalName();
                $file->move('assets/images',$name);
                if($blog->photo != null)
                {
                    unlink(public_path().'/assets/images/'.$blog->photo);
                }            
            $data['photo'] = $name;
            } 
        $blog->update($data);
        return redirect()->route('user-blog-index')->with('success','Blog Updated Successfully.');
    }
    public function destroy($id)
    {

        $blog = Blog::findOrFail($id);
        if($blog->photo == null){
        $blog->delete();
        return redirect()->route('user-blogs-index')->with('success','Blog Deleted Successfully.');
        }
        unlink(public_path().'/assets/images/'.$blog->photo);
        $blog->delete();
        return redirect()->route('user-blogs-index')->with('success','Blog Deleted Successfully.');
    }
    
}
