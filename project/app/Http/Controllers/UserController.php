<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Category;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\Language;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function index()
    {
    	$user = Auth::guard('user')->user();
    	
    	$title = explode(',', $user->title);
            $details = explode(',', $user->details);
            $education = explode(',', $user->education);
        if($user->title!=null && $user->details!=null && $user->education!=null)
        {
            $title = explode(',', $user->title);
            $details = explode(',', $user->details);
            $education = explode(',', $user->education);
            
        }
        return view('user.dashboard',compact('user','title','details','education'));
    }
	public function q2alogin(Request $request){
		
			$user = Auth::guard('user')->user();
			$email=$user->email;
        
            //return redirect(url("questions/index.php?qa=login&to=index.php"));
			//$data=array('email'=>'chadda@gmail.com', 'password'=>'123456789');
            // return redirect(url("qa/index.php?qa=login&to=questions&email=$email"));
            // return redirect(url("qa/index.php?qa=ajax&to=questions&email=$email"));
			return redirect(url("qa/questions"));
		}
		
  public function usersocial(){
	    $user = Auth::guard('user')->user();
	    $ch = curl_init();
		$email=$user->email;
		$password=$user->password;
		$data2 = 'email='.$email;
		curl_setopt($ch, CURLOPT_URL, url("social/login/pro"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, 'Content-Type: application/x-www-form-urlencoded');
		curl_exec($ch);
		// close cURL resource, and free up system resources
		curl_close($ch);
	  
	  }
    public function mycases(){
			return "My Cases Coming soon...";
		}
    public function profile()
    {
    	    $user = Auth::guard('user')->user();
    	    $title = explode(',', $user->title);
            $details = explode(',', $user->details);
            $education = explode(',', $user->education);
        if($user->title!=null && $user->details!=null && $user->education!=null )
        {
            $title = explode(',', $user->title);
            $details = explode(',', $user->details);
            $education = explode(',', $user->education);
            
        }
        $cats = Category::all();
        return view('user.profile',compact('user','cats','title','details','education'));
    }

    public function resetform()
    {
        $user = Auth::guard('user')->user();
        return view('user.reset',compact('user'));
    }

    public function reset(Request $request)
    {
        $input = $request->all();  
        
        $user = Auth::guard('user')->user();
      
        if ($request->cpass){
            if (Hash::check($request->cpass, $user->password)){
                if ($request->newpass == $request->renewpass){
                    $input['password'] = bcrypt($request->newpass);
                   
                   
                }else{
                    echo 'Confirm password does not match.';exit;
                    // Session::flash('unsuccess', 'Confirm password does not match.');
                    // return redirect()->back();
                }
            }else{
                echo 'Current password Does not match.';exit;
                // Session::flash('unsuccess', 'Current password Does not match.');
                // return redirect()->back();
            }
        }
        $user->update($input);

        // print_r($input);
      
        $ch = curl_init();
		$email=$user->email;
        // $password=Hash::make($request->newpass);
        $password=bcrypt($request->newpass);
		$data2 = 'email='.$email.'&password='.$password;
         
        
        curl_setopt($ch, CURLOPT_URL, env('APP_URL')."/api/resetPasswordSocial.php");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, 'Content-Type: application/x-www-form-urlencoded');
		curl_exec($ch);
		// close cURL resource, and free up system resources
        curl_close($ch);
        exit;
        // echo "test";die;
        // Session::flash('success', 'Successfully updated your password');
        // return redirect()->back();
    }

    public function profileupdate(Request $request)
    {
		$input = $request->all(); 
        $input['category_id']=implode(',', $request->category_id);
		//$inputcatagory=explode(',',  $input['speciality_catagories']);
		//$input['category_id']=$inputcatagory[0];
		//return $input['title'];
        $user = Auth::guard('user')->user(); 
		//$input['bar_councel_id']=$request->councel_registration_no;
        if(!in_array(null,$request->title_qual)){
			 $input['education'] = implode(',', $request->title_qual);  
			
			}
		else{
			if(in_array(null, $request->title_qual))
            {
                $input['education'] = null;  
            }
            else
            {
                $title = explode(',', $user->education);
               
                $input['education'] = implode(',', $title);  
            }
			
			}
        if(!in_array(null, $request->title) && !in_array(null, $request->details))
        {             
            $input['title'] = implode(',', $request->title);  
            $input['details'] = implode(',', $request->details);                 
        }
        else
        {
            if(in_array(null, $request->title) || in_array(null, $request->details))
            {
                $input['title'] = null;  
                $input['details'] = null;
            }
            else
            {
                $title = explode(',', $user->title);
                $details = explode(',', $user->details);
                $input['title'] = implode(',', $title);  
                $input['details'] = implode(',', $details);
            }
        }
        $user = Auth::guard('user')->user();     
            if($file = $request->file('photo')) 
            {              
                $name = time().$file->getClientOriginalName();
                $file->move('assets/images',$name);
                if($user->photo != null)
                {
                    unlink(public_path().'/assets/images/'.$user->photo);
                }            
            $input['photo'] = $name;
            } 
            if(strpos($request->address,'&')===true)
            {
                $input['address'] = str_replace("&","and",$request->address);
            }
        if (!empty($request->special)) 
         {
            $input['special'] = implode(',', $request->special);       
         }
        if (empty($request->special)) 
         {
            $input['special'] = null;       
         }
        $user->update($input);
        $language = Language::find(1);
        Session::flash('success', $language->success);
        return redirect()->route('user-profile');
    }

    public function publish()
    {
        $user = Auth::guard('user')->user();
        $user->status = 1;
        $user->active = 1;
        $user->update();
        return redirect(route('user-dashboard'))->with('success','Successfully Published The Profile.');
    } 

    public function feature()
    {
        $user = Auth::guard('user')->user();
        $user->is_featured = 1;
        $user->featured = 1;
        $user->update();
        return redirect(route('user-dashboard'))->with('success','Successfully Featured The Profile.');
    } 

}
