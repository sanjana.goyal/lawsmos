<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Judgement extends Model
{
	
	protected $table = SOCIAL_DB_NAME.'.high_court_ap_yearly';
    protected $fillable = ['ID','year','dateofhearing','heading','content','sourceurl','statecode','state'];
    public $timestamps = false;
    
}
