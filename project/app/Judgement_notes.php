<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Judgement_notes extends Model
{
    //
    protected $table = SOCIAL_DB_NAME.'.judgement_headnotes';
    protected $fillable = ['id','title','note','heading','judgement_id','judgement_type','created_by','status','created_at','updated_at'];
    // public $timestamps = false;
}



