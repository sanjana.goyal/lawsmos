<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Maxims extends Model
{
	protected $table = SOCIAL_DB_NAME.'.maxims';
    protected $fillable = ['ID','term','definition'];
    public $timestamps = false;
}
