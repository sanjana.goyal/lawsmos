<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Newmaxims extends Model
{
	protected $table = SOCIAL_DB_NAME.'.maxims-new';
    protected $fillable = ['ID','term','def','created_at'];
    public $timestamps = true;
}
