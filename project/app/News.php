<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class News extends Model
{
	protected $table = SOCIAL_DB_NAME.'.user_news';
    protected $fillable = ['ID','title','userid','description','timestamp'];
    public $timestamps = true;
}
