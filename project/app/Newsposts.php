<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Newsposts extends Model
{
	protected $table = SOCIAL_DB_NAME.'.user_news_posts';
    protected $fillable = ['ID','blogid','title','body','status','timestamp','last_updated','image','views','created_at'];
    public $timestamps = false;
}
