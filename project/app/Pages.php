<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Pages extends Model
{
   protected $table = SOCIAL_DB_NAME.'.pages';
    protected $fillable = ['name','MarkerId','type','categoryid','description','location','address','lat','logitude','LatitudeLongitude','email','phone','website','is_active'];
    public $timestamps = false;
}
