<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Performa extends Model
{
	protected $table = SOCIAL_DB_NAME.'.performas';
    protected $fillable = ['ID','title','content','status','created_at','updated_at'];
    public $timestamps = false;
}
