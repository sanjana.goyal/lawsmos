<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Generalsetting;
use App\Blog;
use App\Sociallink;
use App\Seotool;
use App\Pagesetting;
use App\Language;
use App\Advertise;
use App\Socialblogs;
use Route;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
		
        view()->composer('*',function($settings){
            $settings->with('gs', Generalsetting::find(1));
            $settings->with('sl', Sociallink::find(1));
            $settings->with('seo', Seotool::find(1));
            $settings->with('ps', Pagesetting::find(1));
            $settings->with('lang', Language::find(1));
            $settings->with('lblogs', Blog::orderBy('created_at', 'desc')->limit(4)->get());
            $settings->with('socialblogs', Socialblogs::orderBy('created_at', 'desc')->limit(4)->get());
            
            $settings->with('lblogsss', \DB::table(SOCIAL_DB_NAME.'.manage_pages')->where('status',0)->orderBy('created_at', 'desc')->get());
            $settings->with('ad728x90', Advertise::inRandomOrder()->where('size','728x90')->where('status',1)->first());
            $settings->with('ad300x250', Advertise::inRandomOrder()->where('size','300x250')->where('status',1)->first());
            $metaid = 16;
            $supremejudgementid = '';
            $judgementid = '';
            $articleid = '';
            if (Request::path()=='/') {
                $metaid = 16;
            }elseif (Request::path()=='bareacts') {
                $metaid = 19;
            }elseif (Request::path()=='articles') {
                $metaid = 17;
            }elseif (Request::path()=='supremecourt') {
                $metaid = 18;
            }elseif (Request::path()=='pages/'.request()->route('id')) {
                $metaid = request()->route('id');
            }elseif (Request::path()=='detail/'.request()->route('id')) {
                $judgementid = request()->route('id');
            }elseif (Request::path()=='detail_supreme/'.request()->route('id')) {
                $supremejudgementid = request()->route('id');
            }elseif (Request::path()=='articles/'.request()->route('id')) {
                $articleid = request()->route('id');
            }
            if ($articleid) {
                $settings->with('meta', \DB::table(SOCIAL_DB_NAME . '.user_articles')->where('ID', $articleid)->first());
            }elseif ($judgementid) {
                $settings->with('meta', \DB::table(SOCIAL_DB_NAME . '.high_court_ap_yearly')->where('ID', $judgementid)->first());
            }elseif ($supremejudgementid) {
                $settings->with('meta', \DB::table(SOCIAL_DB_NAME . '.supreme')->where('ID', $supremejudgementid)->first());
            }else{
                $settings->with('meta', \DB::table(SOCIAL_DB_NAME . '.manage_pages')->where('ID', $metaid)->first());
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
