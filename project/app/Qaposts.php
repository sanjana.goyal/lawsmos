<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Qaposts extends Model
{
	
	protected $table = LAWYERS_DB_NAME.'.qa_posts';
    protected $fillable = ['postid','userid','upvotes','downvotes','netvotes','views','title','content','tags','created','type','parentid','categoryid', 'category_ids','updated','status'];
    public $timestamps = true;
    
}

