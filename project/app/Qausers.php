<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Qausers extends Authenticatable
{
    protected $guard = LAWYERS_DB_NAME.'.qa_users';
    protected $table = LAWYERS_DB_NAME.'.qa_users';
    protected $fillable = ['handle', 'email', 'passhash','created','level','userid'];
    protected $hidden = [
        'passhash'
    ];  
    protected $remember_token = false;  
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}
