<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Rewardpoints extends Model
{
	protected $table = SOCIAL_DB_NAME.'.rewards_points_earn';
    protected $fillable = ['ID','userid','points','notes','created_at','update_at'];
    public $timestamps = false;
}
