<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Socialarticles extends Model
{
	
	protected $table = SOCIAL_DB_NAME.'.user_articles';
    protected $fillable = ['ID','title','catagory','subcatagory','userid','description','meta_description','keywords','timestamp','status','reviewby','created_at','updated_at'];
    public $timestamps = false;
    
    
    public function socialuser(){
		return $this->belongsTo('App\Usersocial');
		
		}
}
