<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Socialarticlesections extends Model
{
	protected $table = SOCIAL_DB_NAME.'.user_articles_posts';
    protected $fillable = ['ID','articleid','title','body','status','parentid','views','image'];
    public $timestamps = false;
}
