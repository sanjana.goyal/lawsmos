<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Socialblogs extends Model
{
	protected $table = SOCIAL_DB_NAME.'.user_blog_posts';
    protected $fillable = ['blogid','ID','title','body','status','timestamp','last_updated','image','views'];
    public $timestamps = false;
}
