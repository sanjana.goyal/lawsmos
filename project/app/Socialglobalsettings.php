<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Socialglobalsettings extends Model
{
	protected $table = SOCIAL_DB_NAME.'.site_settings';
    protected $fillable = ['site_name','site_desc','know_the_law','site_email','site_logo'];
    public $timestamps = false;
}
