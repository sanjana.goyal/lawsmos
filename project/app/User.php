<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
class User extends Authenticatable 
{

    use HasApiTokens, Notifiable;
    protected $guard = 'user';
    protected $fillable = ['id','name','username','gender', 'category_id', 'photo', 'description', 'language', 'age','user_role_id', 'education', 'residency', 'profession','registration_no','councel_registration_no','year_reg','practice_city','snr_desgn', 'city','lat','longitude','LatitudeLongitude','address', 'phone', 'fax', 'email','f_url','g_url','t_url','l_url','password','title','details','is_featured','status','active','featured','web','special','speciality_catagories','source','user_verified','verification_token','temp_password'];
    protected $hidden = [
        'password', 'remember_token',
    ];  

    protected $remember_token = false;  
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
	 public  function ratings()
    {
	
			return $this->hasMany('App\Userratings','lawyerid');
	}
	
}
