<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Userqa extends Authenticatable
{
    protected $guard = 'userqa	';
    protected $fillable = ['email','handle', 'passhash'];
    protected $hidden = [
        'passhash'
    ];  
    protected $remember_token = false;  
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}
