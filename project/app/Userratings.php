<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Userratings extends Model
{
	
	protected $table = SOCIAL_DB_NAME.'.rating';
    protected $fillable = ['ID','lawyerid','seekerid','rating','review','comment','created_at','updated_at'];
    public $timestamps = false;
    
    protected $guarded=[];
    
    
    public function socialuser(){
		return $this->belongsTo('App\Usersocial');
		
		}
	public function user(){
		return $this->belongsTo('App\User');
		
		}
}
