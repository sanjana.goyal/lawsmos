<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
class Usersocial extends Authenticatable 
{

use HasApiTokens, Notifiable;
	protected $table = SOCIAL_DB_NAME.'.users';
   
	protected $guard = SOCIAL_DB_NAME.'.users';

    protected $fillable = ['ID','email','phone', 'password', 'username','gender','user_role','IP', 'first_name', 'last_name','lat','longitude','LatitudeLongitude','city', 'joined','joined_date', 'source','profile_identification','points','bonus_points','total_points','online_timestamp','avatar','user_verified','verification_token','temp_password'];

    protected $hidden = [
        'password'
    ];  
	protected $guarded=[];
	
    protected $remember_token = false;  
	public $timestamps = false;

	public function articles()
    {
        return $this->hasMany('App\Socialarticles');
    }
    
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
    
    public function ratings()
    {
	
	return $this->hasMany('App\Userratings');
	}
}
