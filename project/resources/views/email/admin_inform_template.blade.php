<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
        <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">


<title></title>
<style>
.wrapper {
	width: 100%;
	table-layout: fixed;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}
    @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700;800&display=swap');
</style>
</head>

<body>
    <div style=" border: 6px solid #000;background: #f3f3f3;margin: 50px auto;padding: 20px 54px 24px 54px;width: 700px;">
    <div>
        <h2 style="font-size: 16px;text-align: right;margin:0;color:#000">Dated : {{$datetime}}</h2>
        </div>
        <section>
            <div class="logo" style="display:block;text-align:center;padding-top: 40px;">
                <!-- @if(Auth::guard('user')->check())   -->
                    <a href="{{route('front.social')}}">
                        <img src="{{asset('assets/images/logo-black-pp.png')}}" alt="">
                    </a>
                    <!-- @else -->
                    <a href="{{route('front.index')}}">
                         <img src="{{asset('assets/images/logo-black-pp.png')}}" alt="">
                    </a>

                    <!-- @endif -->

                </div>
            </section>
            <section>
           
                <h4 style="margin-top: 32px;font-family: 'Open Sans', sans-serif;margin: 20px 0 18px;font-size: 20px;font-weight: 700;color:#111">Dear Team,</h4>

                <p style="font-family: 'Open Sans', sans-serif;margin: 20px 0 18px;font-size: 16px;font-weight: 500;color: #111;">We have received a query from <strong> {{$name}} </strong>. Kindly check the following details. ,</p>
                
<!--
                <ul style="padding: 0;list-style: none;    font-family: 'Open Sans', sans-serif;">
                <li style="margin:0 4px 0;font-size:15px;color:#000;position:relative">Name : <span style="font-size:15px;font-weight:600">name</span></li>
                <li style="margin:0 4px 0;font-size:15px;color:#000;position:relative">Email : <span style="font-size:15px;font-weight:600">email </span></li>
                <li style="margin:0 4px 0;font-size:15px;color:#000;position:relative">Phone : <span style="font-size:15px;font-weight:600">phone </span></li>
                <li style="margin:0 4px 0;font-size:15px;color:#000;position:relative">Location : <span style="font-size:15px;font-weight:600">location </span></li>
                <li style="margin:0 4px 0;font-size:15px;color:#000;position:relative">Message : <span style="font-size:15px;font-weight:600">message </span></li>
                </ul>
-->
                <table>
                <tbody>
                    <tr>
                    <th style="text-align: left;padding: 3px 10px;font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">Name :</th>
                    <td style="font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">{{$name}}</td>
                    </tr>
                     <tr>
                    <th style="text-align: left;padding: 3px 10px;font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">Email :</th>
                    <td style="font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">{{$email}}</td>
                    </tr>
                     <tr>
                    <th style="text-align: left;padding: 3px 10px;font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">Phone :</th>
                    <td style="font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">{{$phone}}</td>
                    </tr>
                     <tr>
                    <th style="text-align: left;padding: 3px 10px;font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">Location :</th>
                    <td style="font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">{{$location}} </td>
                    </tr>
                     <tr>
                    <th style="text-align: left;padding: 3px 10px;font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">Message :</th>
                    <td style="font-size:15px;color:#000;font-family: 'Open Sans', sans-serif;">{{$msg}}</td>
                    </tr>
                    </tbody>
                </table>
                <p style="font-family: 'Open Sans', sans-serif;margin: 20px 0 18px;font-size: 16px;font-weight: 500;color: #111;">Thanks for contact us.Your query is registered and we will revert back to you shortly.
                </p>
        </section>
    
    </div>
</body>
</html>
