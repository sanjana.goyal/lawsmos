<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
<title></title>
<style>
.wrapper {
	width: 100%;
	table-layout: fixed;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}
</style>
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
    <center class="wrapper" style="width:100%;table-layout:fixed;
    -webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fff;">
           <table style="border:4px solid #000;background: #fff;margin: 50px 0;padding: 22px 54px 24px;width: 700px;">
    <tbody>
        <!-- <tr>
            <td>
              <table style="width: 166px;margin:0 auto;padding-bottom: 16px;">
                  <tbody>
                      <tr>
                          <td>
                                <img src="{{asset('assets/images/'.$gs->logo)}}" style="max-width:100%">
                          </td>
                      </tr>
                  </tbody>
              </table>
            </td>
        </tr> -->
        <tr>
            <td>
                <h2 style="font-family: 'Open Sans', sans-serif;text-align: center;color:#0c0c0c;font-weight: 400;font-size: 34px;
                letter-spacing: 1px;">HI THERE!</h2>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-family: 'Open Sans', sans-serif;margin: 20px 0 18px;font-size: 16px;"><strong>Dear {{$name}},</strong></p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin-bottom: 11px;">Thank you for dropping your query. Our team is working diligently to assist you with your request. We will respond to you in one to two business days.

                </p>
            </td>
        </tr>
        <tr>
        <td>
             <p style="font-family: 'Open Sans', sans-serif;margin: 20px 0 0;font-size: 14px;">Thank you,</p>
            <p style="font-family: 'Open Sans', sans-serif;margin: 0 0 18px;font-size: 14px;">Team Lawsmos</p>
            </td>
        </tr>
    
     
    </tbody>
</table>
        </center>
</body>
</html>
