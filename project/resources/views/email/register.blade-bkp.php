<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
<title></title>
<style>
.wrapper {
	width: 100%;
	table-layout: fixed;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}
</style>
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#fff;">
    <center class="wrapper" style="width:100%;table-layout:fixed;
    -webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fff;">
           <table style="border:2px solid #000;background: #fff;margin: 50px 0;padding: 40px 54px 24px;width: 700px;">
    <tbody>
        <tr>
            <td>
              <table style="width: 166px;margin:0 auto;padding-bottom: 16px;">
                  <tbody>
                      <tr>
                          <td>
                                <img src="{{asset('assets/images/'.$gs->logo)}}" style="max-width:100%">
                          </td>
                      </tr>
                  </tbody>
              </table>
            </td>
        </tr>
        <tr>
            <td>
                <h2 style="font-family: 'Open Sans', sans-serif;text-align: center;color:#0c0c0c;font-weight: 400;font-size: 34px;
                letter-spacing: 1px;">HI THERE!</h2>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-family: 'Open Sans', sans-serif;margin: 20px 0 18px;font-size: 16px;"><strong>Dear Guest,</strong></p>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin-bottom: 11px;">Welcome to the Lawsmos family. It is for people like yourself that lawsmos rocks. You are really important to us and we don’t want to lose you ever, so please save your following login credentials.
                </p>
            </td>
        </tr>
        <tr>
        <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">1. Username : {{ $username }}</p></td>
        </tr>
        <tr>
        <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">2. Password : {{ $password }}</p></td>
        </tr>
        <tr>
        <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">3. Registered Email {{ $username }}</p></td>
        </tr>

        <tr>
            <td>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">To verify your account, Click here :</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;"><a href="{{ $baseUrl }}" style="color: #fff; border-radius: 3px; text-decoration: none; background-color: #333; padding: 10px 20px; font-size: 14px; font-family: Arial, sans-serif;">Verify account</a></p>
            </td>
        </tr>
       
            <tr>
           <td><p style="font-family: 'Open Sans', sans-serif;margin:20px 0 0px;font-size: 15px;font-weight: 600;">Here are a few tips to give you a head-start on your Lawsmos journey:</p></td>
           </tr>
           <tr>
           <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">- Heads' up on how to use Lawsmos and its popular services.</p></td>
           </tr>
           <tr>
           <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">- How to feature on top search results for lawyers in your city.</p></td>
           </tr>
           <tr>
               <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">- How to pull leads and connect with the clients.</p></td>
           </tr>
           <tr>
               <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">- How to use the research engine and case builder.</p></td>
           </tr>
           <tr>
               <td><p style="font-family: 'Open Sans', sans-serif;font-size: 15px;line-height: 28px;margin: 0;">- How to find jobs on lawsmos</p></td>
           </tr>
        <tr>
            <td>
                    <table style="margin:0 auto;margin-top: 70px;">
                            <tbody>
                                <tr>
                                    <td>
                                          <p style="font-family: 'Open Sans', sans-serif;font-size: 14px;margin:0;">Looking forward to you having a good time on LAWSMOS</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
            </td>
        </tr>
    </tbody>
</table>
        </center>
</body>
</html>
