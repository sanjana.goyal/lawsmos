@extends('layouts.front')
@section('content')
      <div class="section-padding about-area-wrapper wow fadeInUp">
               <div class="container">
                   
                        <div class="section-title  text-center">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
                            <h2>{{$lang->abouts}}</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                    
                </div>
                   <div class="back__content">
                  
                        {!! $about !!}
                      
                   </div>
               </div>
           </div>

@endsection