@extends('layouts.front')
@section('content')
<style>
	.baretable_content{
		padding-top:60px;
	}

</style>
<div class="container">
<div class="row">
	<div class="col-md-12 baretable_content">
		<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
		<h4><?php echo strip_tags($acts->title); ?></h4>
        <h3>Contents</h3>
        <div class="bare_wrap">
		<table id="bareact__table" class="table dataTable table-striped table-hover" role="grid" aria-describedby="friends-table_info">
		<thead>
						
							<tr>
									<th>Sections</th>
									<th>Particulars</th>
									
							</tr>
		</thead>
		<tbody>
			@foreach($actsectionswithoutchapters as $actssectionwithot)
			
			<tr>
					<td><?php $ccg=explode(".",$actssectionwithot->section); 
											 
						 echo $ccg[0]; ?></td>
			@if($actssectionwithot->description==null || $actssectionwithot->description=="")

					<td><?php echo $ccg[1]; ?></td>
										
						@else
						<td><a href="<?php echo url('view_section_details/'.$actssectionwithot->id);?>"><?php echo $ccg[1];   ?></a></td>
					
			@endif
					<!-- <td><a href="<?php //echo site_url('admin/deletesectionwithoutcontnet/'.$actssectionwithot->id.'/'.$actssectionwithot->act_id);?>">Delete</a></td> -->
				</tr>
			@endforeach
			
			@foreach ($actchapters as $chapters)
							<tr>
									<th> <?php 
									$ch=explode(" ",$chapters->chapter);	
									
									echo $ch[0]; ?> <?php echo $ch[1]; ?></th>
                                <td></td> 
									 
									
									
							</tr>
							@foreach($actsections as $actssectio)

								@if($chapters->chapterid==$actssectio->chapterid)

									<tr>
										<td><?php 
										
											 $ccg=explode(".",$actssectio->section);
											 
											
										      echo $ccg[0];
										
										
										 ?></td>
                                     
										
										<?php if($actssectio->description==null || $actssectio->description==""): ?>
										@else
										<td><a href="<?php echo url('view_section_details/'.$actssectio->id);?>"><?php  echo $ccg[1];  ?></a></td>
										<?php endif; ?>
										
										
										</tr>
									@endif
							@endforeach
@endforeach


@foreach($actschedules as $sch)
	<tr>
		<td>{{ $sch->heading }}</td>
		<td></td>
		<td><a href="<?php echo url('view_schedules/'.$sch->id);?>">{{ $sch->subheading }}</a></td>
	
	</tr>
	
@endforeach
			</tbody>
			</table>
	</div>
        </div>
</div>
</div>
@endsection
