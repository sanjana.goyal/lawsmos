@extends('layouts.front')
@section('content')
<div class="section-padding blog-post-wrapper blogpage">
            <div class="container">
                <div class="blog-detail-wrapper">
                <div class="row">
                    <div class="col-md-12">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
                        <div class="section-title">
                            <h2>{{$blog->title}}</h2>
                            <ul>
                                <li><i class="fa fa-clock-o"></i> {{$blog->created_at}}</li>
                               
                                <li><i class="fa fa-eye"></i>{{$blog->views}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p><img src="{{asset('assets/images/'.$blog->image)}}" alt=""></p>
                       <div class="row">
                           <div class="col-md-12">
                               <div class="entry-content">
                                {!!$blog->body!!}
                                </div>
                           </div>
                           
                       </div>
                                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                    <a class="a2a_dd" href=""></a>
                                    <a class="a2a_button_facebook" href=""></a>
                                    <a class="a2a_button_twitter" href=""></a>
                                    <a class="a2a_button_google_plus" href=""></a>
                                </div>
                                <script async src="https://static.addtoany.com/menu/page.js"></script>
                    </div>
                </div>
                
                <hr>
                
                 <div class="all-comments">
						<div class="comments-block">
							
						</div>
                 
                 </div>
                
                
                <hr>
                
               @if(Auth::guard('user')->check())     
                
                <div class="card">
					<div class="card-block">
					
						<form method="POST" action={{ route('user-comment-update',$blog->ID) }}>
						
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
							
								<textarea name="body" placeholder="Your Comments Here..." class="form-control"></textarea>
							</div>
							
							
							<div class="form-group">
							
								<button type="submit" class="btn btn-primary">Add Comment</button>
							</div>
						</form>
					</div>
				
                
                </div>
               @else
				 <a href="{{route('user-login')}}">{{$lang->signin}}</a> Or 
               <a href="{{route('user-register')}}">{{$lang->signup}}</a> To Add Comments
               
              @endif
              
    </div>
            </div>
        </div>

@endsection
