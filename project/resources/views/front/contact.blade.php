

@extends('layouts.front')
@section('content')
<div class="section-padding contact-area-wrapper wow fadeInUp">
            <div class="container">
                        <div class="section-title pb_50 text-center">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
                            <h2>{{$lang->contacts}}</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                 
                <div class="content__outer">
                    <div class="block__two">
                        <ul>
                             @if($gs->street != null) 
                            <li class="contact-info con__img">
                            <img src="{{url('social/uploads/471a56b1cc76a8736a902c04bc1be1d7.png')}}" alt="lawsoms">
                            </li>
                          <li class="contact-info">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <!-- {{$gs->street}} -->
                                #588, Sector 6, Panchkula, Haryana 134109
                            </li>
                            @endif

                            @if($gs->phone != null || $gs->fax != null ) 
                            <li class="contact-info">

                                 <i class="fa fa-phone" aria-hidden="true"></i>
                                @if($gs->phone != null && $gs->fax != null)
                                <!-- <a href="tel:{{$gs->phone}}">+{{$gs->phone}}</a> -->
                                <a href="tel:077107 77770">077107 77770</a>
                                <br>
                                <a href="tel:{{$gs->fax}}">+{{$gs->fax}}</a>
                                @elseif($gs->phone != null)
                            <!-- <a href="tel:{{$gs->phone}}">+{{$gs->phone}}</a> -->
                            <a href="tel:077107 77770">077107 77770</a>

                                @else($gs->fax != null)
                                <a href="tel:{{$gs->fax}}">+{{$gs->fax}}</a>
                                @endif

                            </li>
                            @endif

                            @if($gs->site != null || $gs->email != null )
                            <li class="contact-info">                               
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                @if($gs->site != null && $gs->email != null) 
                                <a href="{{$gs->site}}">{{$gs->site}}</a>
                                <br>
                                <!-- <a href="mailto:{{$gs->email}}">{{$gs->email}}</a> -->
                                <a href="mailto:info.lawsmos@gmail.com">info.lawsmos@gmail.com</a>
                                @elseif($gs->site != null)
                                <a href="{{$gs->site}}">https://lawsmos.com</a>
                                @else
                                <!-- <a href="mailto:{{$gs->email}}">{{$gs->email}}</a> -->
                                <a href="mailto:info.lawsmos@gmail.com">info.lawsmos@gmail.com</a>
                                @endif                                                                
                            </li>
                            @endif
                        </ul>
                    </div>
                    <div class="block__one">
                        <h3>{{$ps->contact_title}}</h3>
                        <p>{{$ps->contact_text}}</p>
                        <div class="comments-area">
                            <div class="comments-form">
                                <form action="{{route('front.contact.submit')}}" id="contform" method="POST">
                                    {{csrf_field()}}
                                <!-- Success message -->
                                        @include('includes.form-success') 
                                <input type="hidden" name="to" value="{{$ps->contact_email}}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input name="name" placeholder="{{$lang->con}}" required="" type="text">
                                        </div>
                                        <div class="col-md-6">
                                            <input name="phone" placeholder="{{$lang->cop}}" type="tel">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input name="email" placeholder="{{$lang->coe}}" required="" type="email">
                                        </div>
                                    </div>
                                    <p><textarea name="text" id="comment" placeholder="{{$lang->cor}}" cols="30" rows="10"  required=""></textarea></p>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <img id="codeimg" src="{{url('assets/images')}}/capcha_code.png">
                                            <span style="cursor: pointer;margin-left:10px" class="refresh_code"><i class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>
                                        </div>
                                          <div class="col-md-6 col-sm-12 col-xs-12">

                                            <input name="codes" placeholder="Enter Code" required="" type="text">
                                        </div>
                                    </div>
                                    <input name="contact_btn" value="Send Message" type="submit" class="contct-btn transition_3s">
                                </form>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script>
        $('.refresh_code').click(function () {
            $.get('{{url('lawyer/refresh_code')}}', function(data, status){
                $('#codeimg').attr("src","{{url('assets/images')}}/capcha_code.png?time="+ Math.random());
            });
        })
    </script>
@stop