@extends('layouts.front')
@section('styles')
@endsection
@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>

<section id="searchlawyers">
    <div id="map-canvas" style="width: 100%; height: 700px; display:block;"></div>
    <div class="container">
        <div class="hero-form">
            <div class="hero-form-wrapper">
                <form action="{{route('user.search')}}" method="GET">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input id="locationselect" name="search" placeholder="Enter Your Location" type="text" class="form-control" value="<?php if (!empty($location)) { echo $location;} ?>">
                                    <input type="hidden" class="form-control" name="lat" value="<?php echo (!empty($lat)) ? $lat : ''; ?>" id="us3-lat" />
                                    <input type="hidden" class="form-control" name="long" value="<?php echo (!empty($lon)) ? $lon : ''; ?>" id="us3-lon" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 cl-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <select name="group" id="blood_grp" class="form-control" required>
                                        <option value="">{{$lang->sbg}}</option>
                                        @foreach($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="form-group text-center">
                                <input type="submit" class="btn btn-block hero-btn" name="button" value="{{$lang->search}}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Starting of All - sectors area -->
<section id="searchlawyers">
    <div class="section-padding all-sectors-wrap ">
        <div class="container">
            <div class="row">
                <h5 class="text-center">Ask for Assistance</h5>
                <div class="col-md-12">
                    <div class="section-title pb_50 text-center">
                        <h2>What service do you need?</h2>
                        <div class="section-borders">

                            <span class="black-border"></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row service-needed">
                @foreach($cats as $cat)
                <ul>
                    <li>
                        <a href="{{route('front.lead',$cat->id)}}">
                            <div class="single-campaignCategories-area service-content-p change{{$cat->id}} text-center">
                                <h4>{{$cat->cat_name}}</h4>
                            </div>
                        </a>
                    </li>
                </ul>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- Ending of All - sectors area -->
<!-- Ending of All - sectors area -->

<section class="two-part-set">
    <div class="container">
        <h2>Have a Legal Question ?</h2>
        @include('includes.form-success')
        <div class="hlq-flex">
            <div class="hlq-left">
                <h3>Recently Answered Questions</h3>
                @if($questionarray)
                    @foreach($questionarray as $quas)
                        @php 
                            $total_ans = \DB::table("qa_posts")->where("type", "A")->where("parentid", $quas['postid'])->get()->count(); 
                            $user = \DB::table(SOCIAL_DB_NAME . '.users')->where("ID", $quas['userid'])->first();
                        @endphp
                        <div class="raq-single">
                            <div class="raq-left">
                                @if($user)
                                    @if($user->avatar)
                                        <img src="{{asset('social/uploads/'.$user->avatar)}}" alt="Indian Law">
                                    @else
                                        <img src="{{asset('assets/front/img/law-set-image.jpg')}}" alt="Indian Law">
                                    @endif
                                    <strong>{{$user->first_name}}</strong>
                                    <span>{{$user->city}}</span>
                                @endif
                            </div>
                            <div class="raq-right">
                                <p>{{ $quas['title'] }}</p>
                                <ul>
                                    <li>{{date('d M, Y',strtotime($quas['created']))}}</li>
                                    <li>{{date('H:i a',strtotime($quas['created']))}}</li>
                                    <li>{{$total_ans}} Answers</li>
                                    <li> <a href="{{url('qa',$quas['postid'])}}">read answers</a></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="hlq-right">
                <h3>Ask your Legal Question</h3>
                <p>Questions are answered by Lawsmos community of Practicing Advocates, Lawyers, Law Professors and Students.</p>
                <div class="lq-form">
                    <form id="regForm" class="legal-question" action="{{route('front.postquestions')}}" method="get">
                        <div class="home-sec-heading">
                            @include('includes.form-error')
                            @include('includes.form-success')
                            <div class="alert alert-success" style="display:none" id="question_post_div_success"></div>
                            <div class="alert alert-danger" style="display:none" id="question_post_div_error">
                                <button type="button" class="close" onclick="hide_post_div_error('question_post_div_error');" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                        <textarea name="question" id="question" placeholder="Briefly mention your Question/Paradox and try to be specific with your question."></textarea>
                        <input type="hidden" name="checkuser" id="checkuser" value="{{$loggedinuser}}">
                        
                        <select class="form-control selectpicker" multiple title="Select Categories" data-max-options="4" name="" id="catagories_select_for_Query" required="">  
                        <option value="">SELECT RELATED CATEGORIES <span>(up to 4)</span></option>
                            @foreach($catss as $cat)
                            <option value="{{$cat->cid}}">{{$cat->c_name}}</option>
                            @endforeach
                        </select>
                        <input type="submit" value="Submit" id="legal-qbtn">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section> 
<!--question_wrap section ends-->
<section class="draft-case-new">
    <div class="container">
        <div class="fraft-flex">
            <div class="draft-content">
                <h2>DRAFT YOUR CASE</h2>
                <ul class="list-wrap" id="draft_case">
                    @if(!empty($performa))
                    @foreach($performa as $perf)
                    <li><a href="{{route('front.draftcase',['title' => base64_encode($perf->title) ,'content'=>base64_encode($perf->content)])}}">
                            {{ $perf->title }}</a></li>
                    @endforeach
                    @endif
                    <span id="view-more-draft-result"></span>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--know-law-sec starts-->
<section class="know-law-new">
    <div class="top-tile">
        <h2>Know The Law</h2>
    </div>
    <div class="container">
    <h4 class="ts-tile">Articles</h4>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="single-vts">
                    <h3>pick of the day </h3>
                    <ul>
                        <li><a href="javascript:void(0);">How can I get free Legal advice ?</a></li>
                    </ul>
                    <div class="s-more">
                        <a href="{{route('front.listallarticles')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-vts">
                    <h3>latest</h3>
                    <ul class="">
	                    @if(!empty($socialarticles))
	                    @foreach($socialarticles as $sarticles)
	                    <li><a href="{{route('front.articles',$sarticles->ID)}}">{{$sarticles->title}}</a></li>
	                    @endforeach
	                    @endif
	                </ul>
	                <div class="s-more">
	                    <a href="{{route('front.listallarticles')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
	                </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-vts">
                    <h3>popular </h3>
                    <ul class="">
                        @if(!empty($populararticles))
                        @foreach($populararticles as $sarticles)
                        <li><a href="{{route('front.articles',$sarticles->ID)}}">{{$sarticles->title}}</a></li>
                        @endforeach
                        @endif
                     </ul>
                        <div class="s-more">
                            <a href="{{route('front.listallarticles')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-vts">
                    <h3>search Category </h3>
                    <select id="catagories_select" class="form-control">
                    <option>Select Category</option>
                    @foreach($catss as $cat)
                    <option value="{{$cat->cid}}">{{$cat->c_name}}</option>
                    @endforeach
                </select>
                <div id="result"></div>
                    <div class="s-more">
                        <a href="{{route('front.listallarticles')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="judge-new">
	<div class="container">
		<h2>Judgements</h2>
		<div class="row">
			<div class="col-md-6">
			    <div class="judge-left">
			        <h3>Recent</h3>
			    	<ul class="list-wrap" id="judgements_list">
	                    @if(!empty($recentjudgements))
	                    @foreach($recentjudgements as $judge)
	                    <li><a href="{{ URL('detail/'.$judge->id)}}">{{$judge->heading}}</a></li>
	                    @endforeach
	                    @endif
	                </ul>
	                <div class="s-more">
                        <a href="{{route('front.listallhighcourt')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
			    </div>
			</div>
			<div class="col-md-6">
			    <div class="judge-right">
			    	<form action="{{ url('submitsearch') }}" method="GET">
			    		{{ csrf_field() }}
						<div class="search-judge-wrap">
			        		<h3>search judgements</h3>
			        		<div class="form-group">
			        			<select class="form-control" name="court">
			            			<option>Select Court</option>
			           				@foreach($states as $state)
						            <option value="{{$state->state}}">{{$state->state}}</option>
						            @endforeach
						        </select>
			    			</div>
			    			<div class="form-group">
						        <input type="text" name="year" placeholder="Year" class="form-control" onkeypress="return isNumber(event)" />
						    </div>
						    <div class="form-group">
						        <select class="form-control" name="catagory">
						            <option>Select Catagory</option>
						            @foreach($catss as $catg)
						            <option value="{{$catg->cid}}">{{$catg->c_name}}</option>
						            @endforeach

						        </select>
						    </div>
						    <div class="form-group">
						        <input type="text" name="content" id="search-landmark" class="form-control" placeholder="Content" />
						    </div>
						    <div class="form-group">
						        <input type="submit" name="search" class="search-btn-home" value="Search Judgment" />
						    </div>
						</div>
					</form>
			    </div>
			</div>
		</div>
	</div>
</section>
<section class="judge-new with-Bareacts">
    <div class="container">
        <h2>Bareacts</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="judge-left">
                    <h3>Recent</h3>
                    <ul class="list-wrap">
                    @if(!empty($bareacts))
                    @foreach($bareacts as $bareact)
                    <li><a href="{{ URL('act_details/'.$bareact->id)}}">{{ $bareact->title }}</a></li>     
                    @endforeach
                    @endif
                </ul>
                    <div class="s-more">
                        <a href="{{route('front.bareacts')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="judge-left">
                    <h3>Popular</h3>
                    <ul class="list-wrap">
                    <li><a href="{{URL('act_details/257')}}">Constitution of India</a></li>
                    <li><a href="{{URL('act_details/521')}}">Indian Penal Code</a></li>
                    <li><a href="{{URL('act_details/224')}}">Code of criminal procedure</a></li>
                    <li><a href="{{URL('act_details/521')}}">Code of civil procedure</a></li>
                    <li><a href="{{URL('act_details/521')}}">Indian Evidence Acts</a></li>
                </ul>
                    <div class="s-more">
                        <a href="{{route('front.bareacts')}}">See more <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>
<!--know-law-sec ends-->
<!-- maxims section start-->
<section class="maxi-set">
	<div class="container">
		<h2>Maxims</h2>
	</div>
	<div class="max-flex">
		<div class="maxileft">
			<ul>
		    	@if(!empty($newmaxims))
		        @foreach($newmaxims as $newmaxim)
	            <li>
	                <a href="{{ URL('view_maxims/'.$newmaxim->ID)}}">{{ $newmaxim->term }}</a>
	                <span>{{ $newmaxim->def }}</span>
	            </li>
		        @endforeach
		    	@endif
		    </ul>
			<div class="s-more">
				<a href="{{route('front.allmaxims')}}">See Maxims library <i class="fa fa-angle-right" aria-hidden="true"></i></a>
			</div>
		</div>
		<div class="maxiright">
			<img src="{{asset('assets/front/img/law-set-image.jpg')}}" alt="Indian Law">
		</div>
	</div>
</section>
<!-- maxims section end-->
<!--lwyerind-sec starts-->
<section class="wl-outer" id="aboutus">
	<div class="container">
	    <div class="wl-flex">
			<div class="wl-left">
				<h3>What is Lawsmos ?</h3>
			    <p>India's largest online legal community which enables its users to:</p>
			    <ul>
	                <li>Find the best lawyers in their city.</li>
	                <li>Seek answers to any law related question from expert legal minds of the field and top lawyers.</li>
	                <li>Access laws, bareacts, landmark judgments of India.</li>
	                <li>Read subject wise articles and legal research by top lawyers.</li>
	                <li>Browse lawyer profile and connect with the right set of lawyers.</li>
	                <li>Get expert legal advice.</li>
	                <li>Rate and review lawyers.</li>
	                <li>Find the best lawyers within budget.</li>
	                <li>Cross-check if a case is being correctly handled.</li>
	                <li>Draft a case/ notice/ complaint/ deed etc. using free case builder.</li>
	                <li>Conduct legal research with the help of artificial intelligence and advance research tools.</li>
	            </ul>
			</div>
			<div class="wl-right  judge-right">
			    <h3>Contact us</h3>
				<div class="contactfowrap">
					<div class="alert alert-success" id="contactus_div_success" style="display:none"></div>
					<div class="alert alert-danger" id="contactus_div_danger" style="display:none">
					    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
					    </button>
					</div>
					<div id="errors"></div>
					<!--@include('includes.form-error')
					@include('includes.form-success')--> 
					<form id="contactform">
					    <div class="form-group">
					        <input type="text" name="name" id="contact_name" class="form-control" placeholder="Name" value="{{ old('name') }}" maxlength="40" />
					    </div>
					    <div class="form-group">
					        <input type="email" name="email" id="contact_email" class="form-control" placeholder="Email" value="{{ old('email') }}" maxlength="40"/>
					    </div>
					    <div class="form-group">
					        <input id="locationselects" name="locationselects" placeholder="Enter Location" type="text" class="form-control" value="{{ old('locationselects') }}" maxlength="60"/>
					        <input type="hidden" class="form-control" name="lat" value="" id="us3-lat" value="{{ old('lat') }}" />
					        <input type="hidden" class="form-control" name="long" value="" id="us3-lon" value="{{ old('long') }}" />
					    </div>
					    <div class="form-group">
					        <input type="tel" name="phone" id="contact_phone" class="form-control" placeholder="Phone number" onkeypress="return isNumber(event)" value="{{ old('phone') }}" maxlength="10"/>
					    </div>
					    <div class="form-group">
					        <textarea name="message" id="contact_message" class="form-control" placeholder="Message">{{ old('message') }}</textarea>
					    </div>
					    <div class="form-group cap-group">
					        <input type="text" class="form-control" placeholder="Captcha" name="captcha" id="contact_captchasss">
					        <div class="captcha">
					            <span>{!! captcha_img('flat') !!}</span>
					            <button type="button" class="btn btn-success" id="autoclick"><i class="fa fa-refresh" id="refresh"></i></button>
					        </div>
					    </div>
					    <button class="btn legal-q-button contact-home-btn" id="ajaxSubmit">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!--lwyerind-sec ends-->
<!--legal-blog-sec starts-->
<section class="news-blog-outer">
    <div class="container">
        <div class="blog-news-flex">
            <div class="left-news">
	            <h2 class="lb-head">legal news</h2>
	            <ul class="nav nav-tabs">
		            <li class="active"><a data-toggle="tab" href="#leaglnews1">National</a></li>
		            <li><a data-toggle="tab" href="#leaglnews2">State</a></li>
		            <li><a data-toggle="tab" href="#leaglnews3">International</a></li>
	            </ul>
            	<div class="tab-content">
	            	@if($newspost)
	                @php $legalcount=1; @endphp
	                @foreach($newspost as $nt)
                    <div id="leaglnews{{$legalcount}}" class="tab-pane fade <?php echo ($legalcount==1) ? 'in active' : ''; ?>">
                       <div class="ns-content">
                            @if($nt->image)
                                <img src="{{asset('social/uploads/'.$nt->image)}}" alt="Indian Law">
                            @else
                                <img src="{{asset('assets/front/img/news-image.jpg')}}" alt="Indian Law">
                            @endif
                           <h4>{{$nt->title}}</h4>    
                           <p>{{strip_tags($nt->body)}} <a href="{{ URL('newsdetail/'.$nt->ID)}}">Read More...</a></p>
                        </div>
                    </div>
                    @php $legalcount++; @endphp
		            @endforeach
		            @endif
		        </div>
		    </div>
		    <div class="right-blog-set">
		    	<h2 class="lb-head">Blog <a href="{{route('front.blog')}}">View more</a></h2>
		    	<ul class="blog-list">
		    		@foreach($socialblogs as $sblog)
		    		<li>
		    			<div class="bleg-l-image" style="background-image:url({{asset('social/uploads/'.$sblog->image)}})"></div>
                        <div class="blog-text-area">
                            <div class="head-b-flex">
                                <div class="head-bleft">
                            <h2>{{$sblog->title}}</h2>
                            <span>{{date('d M, Y , H:i a',strtotime($sblog->created_at))}}</span>
                            </div>
                            <a href="{{route('front.blogshow',$sblog->ID)}}">View Details</a>
                            </div>
                            <p>{{substr(strip_tags($sblog->body),0,250)}}</p>
                            <a href="{{route('front.blogshow',$sblog->ID)}}">View Details</a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>
<!--legal-blog-sec ends-->
<!--register-modal starts-->
<div class="modal fade" id="registerModal" role="dialog">
    <div class="modal-dialog"><div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Sign Up</h4>
        </div>
        <div class="modal-body"> 
            <div class="form-resp-wraper">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="dropdown">
                        <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">Select Registration Type<span class="caret"></span></a>
                        <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                            <li class="active"><a href="#dropdown2" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">Register as Service Seeker</a></li>
                            <li class=""><a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">Register as Advocate</a></li>
                            <li class=""><a href="#dropdown3" role="tab" id="dropdown3-tab" data-toggle="tab" aria-controls="dropdown3" aria-expanded="false">Register as law Student</a></li>
                            <li class=""><a href="#dropdown4" role="tab" id="dropdown4-tab" data-toggle="tab" aria-controls="dropdown4" aria-expanded="false">Register as law Professor</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="sign-up-tab-outer">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#reg-seeker" class="user-role-select" aria-controls="reg-seeker" data-id="reg-seeker" data-role="0" role="tab" data-toggle="tab">Service Seeker</a></li>
                    <li role="presentation">
                        <a href="#reg-advocate" class="user-role-select" aria-controls="reg-advocate" role="tab" data-toggle="tab" data-role="1" data-id="reg-advocate">Advocate</a>
                    </li>
                    <li role="presentation"><a href="#reg-student" class="user-role-select" aria-controls="reg-student" role="tab" data-toggle="tab" data-role="2" data-id="reg-student">Researcher/Student</a></li>
                    <li role="presentation"><a href="#reg-pro" class="user-role-select" aria-controls="reg-pro" role="tab" data-toggle="tab" data-role="3" data-id="reg-pro">Law Professor</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="reg-seeker">
                        <form action="#" method="POST" id="register-form-serviceSeeker" class="formregister">
                            {{csrf_field()}}
                            @include('includes.form-error')
                            @include('includes.form-success')
                            <!-- <div class="alert alert-success" style="display:none"></div>
                            <div class="alert alert-danger" style="display:none" id="reg_post_div_error">
                                <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error('reg_post_div_error')" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div> -->
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="hidden" name="user_role_id" class="form-control user_role_id" value="0">
                                    <input name="firstname" class="form-control" placeholder="First Name" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input name="lastname" class="form-control" placeholder="Last Name" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <select name="gender" class="form-control">
                                        <option value="">Select gender</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <input name="email" class="form-control" placeholder="{{$lang->sue}}" type="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input name="phone" class="form-control" placeholder="{{$lang->suph}}" type="phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-unlock-alt"></i>
                                    </div>
                                    <input class="form-control" name="password" placeholder="{{$lang->sup}}" type="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-unlock-alt"></i>
                                    </div>
                                    <input class="form-control" name="password_confirmation" placeholder="{{$lang->sucp}}" type="password">
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="button" class="btn register-btn" name="button">{{$lang->signup}}</button>
                            </div>                                    
                            <div class="form-group text-center">
                                <a  data-dismiss="modal" data-toggle="modal" data-target="#signInModal" href="#">{{$lang->al}}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>      
    </div>
</div>
<!--register-modal ends-->
<!--login-modal starts-->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog"><div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Sign In</h4>
        </div>
        <div class="modal-body">
            <form action="{{route('user-login-submit')}}" method="POST" class="formlogin">
                {{csrf_field()}}
                @include('includes.form-error')
                @include('includes.form-success')
                <!-- <div class="alert alert-success" style="display:none"></div>
                <div class="alert alert-danger" style="display:none" id="login_post_div_error">
                    <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error('login_post_div_error')" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div> -->
                <?php 
                    if (isset($_SERVER['HTTP_REFERER'])) {
                        $page = $_SERVER['HTTP_REFERER'];
                        $_SESSION['page'] = $page;
                    } else {
                        $_SESSION['page'] = "";
                    }
                    //echo "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
                ?>
                <input type="hidden" name="previous_urlss" value="{{ Session::get('questionsssss') }}">
                <input type="hidden" name="localstorage" value="">
                <input type="hidden" name="previous_url" value="<?php echo $_SESSION['page']; ?>">
                <input  name="caseheading" class="form-control" value="{{ session()->get('caseheading') }}" type="hidden">
                <textarea  name="casecontent" class="form-control"  style="display:none;">{{ session()->get('casecontent') }}</textarea>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <input name="email" class="form-control" placeholder="{{$lang->sie}}" type="email" required="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-unlock-alt"></i>
                        </div>
                        <input class="form-control" name="password" id="password" placeholder="{{$lang->spe}}" type="password" required="">
                        <span class="input-group-addon"><i id="pass_show"  class="fa fa-eye"></i> </span>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn login-btn" name="button">{{$lang->signin}}</button>
                </div>
                <div class="form-group text-center">
                    <a  data-dismiss="modal" data-toggle="modal" data-target="#registerModal" href="#">{{$lang->cn}}</a>
                </div>
            </form>
        </div>       
    </div>
</div>
<!--login-modal ends-->
<span id="siteseal">
    <script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=THgxChxYP3v78jnuP70khppQrlBQY4rgOGDTx31KTfuLVTvN2qUJviRYWjFX"></script>
</span>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript">
    $('.selectpicker').data('max-options', 4).selectpicker('refresh');
    $('#legal-qbtn').click(function(e) {
        var question = $('#question').val();
        var categories = $('#catagories_select_for_Query').val();
        if (question && categories && jQuery.inArray("", categories) != 0) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "{{ url('postquestion') }}",
                method: 'GET',
                data: {
                    question: $('#question').val(),
                    categories: $('#catagories_select_for_Query').val(),
                    userid: $('#checkuser').val(),
                },
                success: function(result) {
                    console.log(result);
                },
                error: function(result) {
                    if (result.status === 400 || result.status === 422) {
                        var response = JSON.parse(result.responseText);
                        $.each(response, function(key, val) {
                            $('#regForm')[0].reset(); 
                            $('#question_post_div_error').show();
                            $('#question_post_div_error').html(all_values(val,"question_post_div_error"));
                            $('html, body').animate({
                                scrollTop: $("#question_post_div_error").offset().top -120
                            }, 500); 

                        });
                    }
                }
            });
        }else{
            $('#question_post_div_error').show();
            $('#question_post_div_error').html('Fields should not be empty<button type="button" class="close" onclick="hide_post_div_error(\'question_post_div_error\');" aria-label="Close"><span aria-hidden="true">×</span></button>');
            $('html, body').animate({
                scrollTop: $("#question_post_div_error").offset().top -120
            }, 500);
        }
    });
    $(document).ready(function() {
    	var last_valid_selection = null;
    	$('#catagories_select_for_Query').change(function(event) {
    		if ($(this).val().length > 4) {
    			$(this).val(last_valid_selection);
    		} else {
    			last_valid_selection = $(this).val();
    		}
    	});


    	$("select#catagories_select").change(function() {
            var selectedCatagory = $(this).children("option:selected").val();
            $.ajax({
                type: 'get',
                async: true,
                url: '{{URL::to("searcharticlebycat")}}',
                data: {
                    'searchcat': selectedCatagory
                },
                success: function(data) {
                	//console.log(data);
                	if (data == "") {
                        $("div#result").html("<p>No Articles found in this catagory....</p>");
                    } else {
                        $("div#result").html(data);
                    }
                }
            });

        });
        $('.ratingbar').rating({
            showCaption: true,
            showClear: false,
            size: 'sm',
            hoverEnabled: false,
            touchstart: false,
        });
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        $("#view-more-draft").click(function() {
            $.ajax({
                type: 'get',
                async: true,
                url: '{{URL::to("getPerformas")}}',
                data: {},
                success: function(data) {
                    $("#view-more-draft-result").html(data);
                    $("#view-more-draft").hide();
                }
            });
        });
        let featured_data = [];
        //featuresd list get
        $.ajax({
            type: 'get',
            async: true,
            url: '{{URL::to("getFeaturedList")}}',
            data: {},
            success: function(data) {
                let obj = JSON.parse(data);
                obj.forEach(function(element) {
                    console.log(element);
                    featured_data += `<li><a href="{{URL::to("lawyer-user-profile/` + element.id + `")}}">
                   ` + element.name + `</a></li>
                  
                   <span id="view-more-draft-result">

                   </span>`;
                });
                $("#featured_list").append(featured_data);
                // $("#view-more-draft").hide();
            }
        });

      	$('#pass_show').click(function(){
      		var pass_inp = $('#password').val();
      		if(pass_inp.length > 0){
	          	if($('#password').attr('type') == "password"){
					$("#password").attr("type", "text");
					$("#pass_show").addClass("fa-eye-slash");
	            } else{
	                $("#password").attr("type", "password");
	                $("#pass_show").removeClass("fa-eye-slash");
	                $("#pass_show").addClass("fa fa-eye");
	            }
	      	}  else{
		        $("#password").attr("type", "password");
		        $("#pass_show").removeClass("fa-eye-slash");
		        $("#pass_show").addClass("fa fa-eye");
		    }
	  	});
    });
    $('#search-maxims').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/viewmaximslaravel') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_c">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_c">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });


    $('#search-BareActs').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/view_bare_acts') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_b">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_b">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });

    $('#search-article').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/view_judgements_laravel') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });
    $('#search-landmark').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/view_landmark_judgements_laravel') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });
    $("input#search-maxims").keyup(function() {
    	$("input#search-BareActs").prop("disabled", true);
        $("input#search-article").prop("disabled", true);
        $("input#search-landmark").prop("disabled", true);
    });
    $("input#search-BareActs").keyup(function() {
    	$("input#search-maxims").prop("disabled", true);
        $("input#search-article").prop("disabled", true);
        $("input#search-landmark").prop("disabled", true);
    });
    $("input#search-article").keyup(function() {
    	$("input#search-BareActs").prop("disabled", true);
        $("input#search-maxims").prop("disabled", true);
        $("input#search-landmark").prop("disabled", true);
    });
    $("input#search-landmark").keyup(function() {
    	$("input#search-BareActs").prop("disabled", true);
        $("input#search-article").prop("disabled", true);
        $("input#search-maxims").prop("disabled", true);
    });
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselects'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            document.getElementById("us3-lat").value = lat;
            document.getElementById("us3-lon").value = lon;
        });
    });

    $('i#refresh').click(function() {
    	$.ajax({
	        type: 'GET',
	        url: '{{ url("refreshcaptcha") }}',
	        success: function(data) {
	        	$(".captcha span").html(data.captcha);
	        }
	    });
    });
    $('button#ajaxSubmit').click(function(e) {
    	e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: "{{ url('contactus') }}",
            method: 'GET',
            data: {
                name: $('#contact_name').val(),
                email: $('#contact_email').val(),
                location: $('#locationselects').val(),
                phone: $('#contact_phone').val(),
                message: $('#contact_message').val(),
                captcha: $('#contact_captchasss').val(),
            },
            success: function(result) {
                $('#contactus_div_success').show();
                $('#contactus_div_success').html("Form Submitted successfully. ");
                $('#contactus_div_danger').hide();
                $('#contactform')[0].reset();
                $('button#autoclick').trigger('click');
            },
            error: function(result) {
                if (result.status === 400) {
                    var response = JSON.parse(result.responseText);
                    $.each(response, function(key, val) {
                        $('#contactus_div_danger').show();
                        $('#contactus_div_danger').html(all_values(val,"contactus_div_danger"));
                        $('html, body').animate({
                            scrollTop: $("#contactus_div_danger").offset().top -120
                        }, 500);
                        $('button#autoclick').trigger('click');
                    });
                }
            }
        });
    });
    function all_values(obj,div_id) {
        var keys = _keys(obj);
        var lengths = keys.length;
        var values = Array(lengths);
        var ulHtml = '<button type="button" class="close" onclick="hide_post_div_error('+ "'" +div_id+ "'" +');" aria-label="Close"><span aria-hidden="true">×</span></button>';
        ulHtml += "<ul>";
        for (var i = 0; i < lengths; i++) {
            ulHtml += "<li>" + obj[keys[i]] + "</li>"
            values[i] = obj[keys[i]] + "<br>";

        }
        ulHtml += "<ul>";
        return ulHtml;
    }
    function hide_post_div_error(div_id){
        $('#'+div_id).hide();        
    }

    function _keys(obj) {
        if (!isObject(obj)) return [];
        if (Object.keys) return Object.keys(obj);
        var keys = [];
        for (var key in obj)
            if (_.has(obj, key)) keys.push(key);
        return keys;
    }

    function isObject(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    }

    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselectsss'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            document.getElementById("us3-latss").value = lat;
            document.getElementById("us3-lonss").value = lon;
        });
    });
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselectpostquestion'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();

            console.log(lat);
            console.log(lon);

            document.getElementById("us3-latpostq").value = lat;
            document.getElementById("us3-lonpostq").value = lon;
        });
    });
</script>
<script type="text/javascript">
	var map;
    var geocoder;
    var marker;
    var people = new Array();
    var latlng;
    var infowindow;

    function hidePopup() {
        $("#location_popup").hide();
    }
    $(document).ready(function() {
        ViewCustInGoogleMap();
    });
    function ViewCustInGoogleMap() {
    	var mapOptions = {
    		center: new google.maps.LatLng(
    			<?php 
    				if (isset($lat) && isset($lon)) {
		    			echo $lat . "," . $lon;
		    		} else if (isset($loggedinlat) && isset($loggedinlon)) {
		    			echo $loggedinlat . "," . $loggedinlon;
		    		} else {
		    			echo 0, 0;
		    		} 
		    	?>
		    ), // Coimbatore = (11.0168445, 76.9558321)
            zoom: 13,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        // Get data from database. It should be like below format or you can alter it.

        var data = '<?php if (isset($result)) { echo json_encode($result); } ?>';
        people = JSON.parse(data);
        for (var i = 0; i < people.length; i++) {
            setMarker(people[i]);
        }
    }
    function setMarker(people) {
        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();
        if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
            geocoder.geocode({
                'address': people["addr"]
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                    marker = new google.maps.Marker({

                        position: latlng,
                        map: map,
                        draggable: false,
                        html: people["name"],
                        icon: "images/marker/" + people["MarkerId"] + ".png"

                    });
                    //marker.setPosition(latlng);
                    //map.setCenter(latlng);
                    google.maps.event.addListener(marker, 'click', function(event) {
                        infowindow.setContent(this.html);
                        infowindow.setPosition(event.latLng);
                        infowindow.open(map, this);
                    });
                }
                /*
                else {
                    alert(people["name"] + " -- " + people["addr"] + ". This address couldn't be found");
                }
                */
            });
        } else {

            if (people["MarkerId"] == "Lawyer") {

                if (people["photo"] == "" || people["photo"] == null) {

                    var photo = "default-avatar-profile-icon-vector-18942381.jpg";
                } else {

                    var photo = people["photo"];
                }

                if (people["name"] == "" || people["name"] == null) {

                    var name = "";
                } else {
                    name = people["name"];
                }

                if (people["education"] == "" || people["education"] == null) {

                    var education = "";

                } else {


                    education = people["education"];
                }
                if (people["phone"] == "" || people["phone"] == null) {
                    var phone = "";
                } else {

                    phone = people["phone"];
                }

                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false, // cant drag it
                    html: '<div jstcache="2" id ="location_popup" style="min-width: 194px !important;"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"><button  type="reset" id="can_btn_lawyer"  onclick = "hidePopup()" class="btn btn-default pull-right">x</button></div> <div class="address" style="display:inline"> <div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"><img src="<?php echo url("social/"); ?>/uploads/' + photo + '" class="img img-thumbnail"  alt="no image found.." style="float:left;margin: 7px;display:inline-block;padding: 0;width: 72px;height: 72px;border-radius: 50%;"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width" style="display: inline-block;margin-top: 12px;font-weight: 600;text-transform: capitalize;font-size: 14px;" >' + name + '<br></div><div style="font-size:13px;font-weight:300">' + education + '<br></div> <div style="font-size:13px;font-weight:300">' + phone + '<br></div></div> </div>',
                    icon: '{{url("assets/images/blue-dot.png")}}',


                    //http://maps.google.com/mapfiles/ms/icons/blue-dot.png
                    animation: google.maps.Animation.DROP,
                    // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });

            } else {
                if (people["name"] == "" || people["name"] == null) {

                    var name = "";
                } else {
                    name = people["name"];
                }
                if (people["phone"] == "" || people["phone"] == null) {
                    var phone = "";
                } else {

                    phone = people["phone"];
                }

                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false,
                    mapTypeControl: false, // cant drag it
                    html: '<div jstcache="2"  id="location_popup" style="min-width: 160px !important;padding:10px" ><button  type="reset" id="can_btn_name"  onclick = "hidePopup()" class="btn btn-default pull-right">x</button> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" jsinstance="0" style="display:inline-block;margin-top:2px;font-weight:600;text-transform:capitalize;font-size:14px" class="address-line full-width" jsan="7.address-line,7.full-width">' + people["MarkerId"] + '</div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width">' + name + ',' + people["location"] + '<p style="font-size:13px;font-weight:300">' + phone + '</p></div> </div> </div>',
                    icon: '{{url("assets/images/spotlight-poi2.png")}}',
                    animation: google.maps.Animation.DROP,
                    // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });


            }
        }
    }
    var currentTab = 0; // Current tab is set to be the first tab (0)
    //showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>
@endsection