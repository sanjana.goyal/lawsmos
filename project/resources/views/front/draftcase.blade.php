
@extends('layouts.front')
@section('content')


<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<style>
.error{
	color:red;
}
.result-draft
{

height:1100px;
overflow:scroll;
}

/* width */
::-webkit-scrollbar {
  width: 20px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: black; 
 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #black; 
}

div#judgements-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

}
div#bareacts-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#article-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#performa-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
 
</style>
<div class="drafttt-page">
    <div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="section-title">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
				<h2>Draft - {{ base64_decode($title) }}</h2>
				<p><b>Category :</b> {{$category_name}}</p>
			</div>
			<div class="form-group">
				<label for="title">Following is the sample draft for reference which may further be modified as per requirement</label>
				<textarea rows="4" cols="66" name="editor1" class="" id="casecontent" style="visibility: visible !important; ">{{ base64_decode($content) }}</textarea>
				<p class="description-error-info error"></p>
			</div>
			<div class="form-group ">
				<label for="title">Save as</label>
				<input type="text" name="title" class="form-control" id="casetitle" value="" />
				<p class="title-error-info error"></p>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-danger" id="submitBtn">Save Case</button> 
			</div>
			<div id="content-container"></div>
			</div>
			<div class="col-md-6 referencess">
				<div class="section-title">
				<h2>References</h2>
			</div>
			
			<iframe class="reference" src="<?php echo url('draft_case_referance_iframe/'.$category_id); ?>"  width="100%" height="1300px" style="border:none;"></iframe>	
                           
		</div>
				
	</div>
</div>
                
              
 <script>
	
	$(document).ready(function(){
	
		jQuery('#submitBtn').click(function(e){
		 
			$(".title-error-info").html("");
			$(".description-error-info").html("");

			var casetitle = $('#casetitle').val();
			var casecontent = $('#casecontent').val();
			var error = false;

			if(casetitle == ""){
				error = true;
				$(".title-error-info").html("This field is required");
			}
			if(casecontent == ""){
				error = true;
				$(".description-error-info").html("This field is required");
			}
			if(error == true){
				return false;
			}
			e.preventDefault();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			});
			jQuery.ajax({
				url: "{{ url('caseeditor/save') }}",
				method: 'get',
				data: {
					casetitle: $('#casetitle').val(),
					catagory: $("select#catagory").children("option:selected").val(),
					casecontent: $('#casecontent').val(),
				},
				dataType: "json",
				success: function(result){
					if(result=='failed')
					{
						$('#content-container').html("Please <a href='{{url('lawyer/login')}}'>login</a> or <a href='{{url('lawyer/register')}}'>register</a> to save this case!");
					}else{
						$('#content-container').html("Case Saved Successfully...");
					}
				}
			});
		});
	});
 
 </script>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>

<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

<script>
$('.reference').load(function(){
	$('.reference').contents().find('.header-area-wrapper').hide();
    $('.reference').contents().find('.footer-area-wrapper').hide();
});
</script>

<script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>



@endsection
