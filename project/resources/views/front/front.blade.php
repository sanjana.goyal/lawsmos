<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="{{$seo->meta_keys}}">
        <meta name="author" content="GeniusOcean">
        <title>{{$gs->title}}</title>
        <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/slicknav.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">
        <link rel="icon" type="image/png" href="{{asset('assets/images/'.$gs->favicon)}}">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgriKllmz3R9e7ORquSKDyASj_DP4csuo&libraries=places"></script>
        @include('styles.design')
        @yield('styles')
    @if(Auth::guard('user')->check())

    @endif
    </head>
    <body>
	
	<!-- Popup Div Starts Here -->
	



@if(\Request::is('/')) 
 
@if(!(Auth::guard('user')->check()))   



<div id="abc">
<div id="popupContact">
<!-- Contact Us Form -->
<div class="row">
<div class="col-md-12">
	

<form  id="formlocation" action="{{ url('enterlocation') }}" method="GET" name="formlocation">
<div class="form-group">

 {{-- <span id="cancel" class="glyphicon glyphicon-remove" onclick ="div_hide()"></span> --}}

<h4>dTell us the location from where you are finding lawyers?</h4>	

	<input id="locationselect" name="locationselect" placeholder="Enter Your Location" type="text" class="form-control" value="<?php if(isset($cookievalue)){ echo $cookievalue; } ?>" required />
	 <input type="hidden" class="form-control" name="lat"  value="<?php if(isset($cookielat)){ echo $cookielat; } ?>"  id="us3-lat" />
		<input type="hidden" class="form-control" name="long"  value="<?php if(isset($cookielon)){ echo $cookielon; } ?>" id="us3-lon" />
</div>
    <div class="city-icons">
        <span class="city-1">
    <span class="icon-museum svg-icons"></span>
     <h2><a href="{{ url('enterlocation?locationselect=Chandigarh%2C+India&lat=30.7333148&long=76.7794179') }}">chandigarh</a></h2>
    </span>
          <span class="city-1">
    <span class="icon-museum svg-icons"></span>
     <h2>ludhina</h2>
    </span>
          <span class="city-1">
    <span class="icon-museum svg-icons"></span>
     <h2>jalandhar</h2>
    </span>
          <span class="city-1">
    <span class="icon-museum svg-icons"></span>
     <h2>pathankot</h2>
    </span>
    </div>  
    

<div class="popup-form-btn">
    <p><a href="javascript:void(0);">View All Cities</a></p>
<button type="submit" class="btn login-btn" id="locationsubmit">Send</button>
    </div>
</form>

</div>
</div>
</div>
<!-- Popup Div Ends Here -->
</div>



@endif	



@endif		
   <div id="covergr"></div>
        <!-- Starting of Header area -->
        <div class="header-top-area">
            <div class="container">

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="top-column-left">
                            <ul>
                                <li>
                                    <i class="fa fa-envelope"></i> {{$gs->email}}
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i> {{$gs->phone}}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="top-column-right">
                            <ul class="top-social-links">
                                <li class="top-social-links-li">
                                    @if($sl->f_status == 1)
                                    <a href="{{$sl->facebook}}"><i class="fa fa-facebook"></i></a>
                                    @endif
                  
  
                   @if($sl->t_status == 1)
                                    <a href="{{$sl->twitter}}"><i class="fa fa-twitter"></i></a>
                                    @endif
                                    @if($sl->l_status == 1)
                                    <a href="{{$sl->linkedin}}"><i class="fa fa-linkedin"></i></a>
                                    @endif
                                    @if($sl->g_status == 1)
                                    <a href="{{$sl->gplus}}"><i class="fa fa-google"></i></a>
                                    @endif
                                </li>
                         @if(Auth::guard('user')->check())   
                         @else
                                <li><a href="{{route('user-login')}}">{{$lang->signIn}}</a></li>
                                <li><a href="{{route('user-register')}}">{{$lang->signUp}}</a></li>

                         @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-area-wrapper">
            <div class="container">
                    <div class="navbar-header">
                        <div class="logo test">
                            
                             @if(Auth::guard('user')->check())  
                            <a href="{{route('front.social')}}">
                                <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                            </a>
                            @else
                             <a href="{{route('front.index')}}">
                                <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                            </a>
                            
                            @endif
                           
                        </div>
                        <div id="mobile-menu-wrap"></div>
                    </div>
                        <div class="mainmenu">

                            
                    @if(Auth::guard('user')->check())  

                    <?php $user=Auth::guard('user')->check(); 

                    $user = Auth::guard('user')->user();

                    $check_user_type=$user->user_role_id;

                    if( $check_user_type==1){
//{{route('user-social')}}
                    ?>
                         <ul id="menuResponsive">
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                        
                                        {{-- 
											
											<li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
											 <li><a href="{{route('user-profile')}}">CORE PROFILE</a></li>
											 <li class="menuLi"><a style="cursor: pointer;"> ARTICLES BLOGGING <i class="fa fa-angle-down"></i></a>
                                        <ul class="menuUl">
											 <li><a href="{{route('front.blog')}}">All Articles Blogs</a></li>
											<li><a href="{{route('user-blogs-index')}}">Create New Articles</a></li>
                                        </ul>
										
										<li><a href="">CREATE AGENCY</a></li> 
										 
                                        <li><a href="{{route('user-dashboard')}}">{{$lang->dashboard}}</a></li>
                                        <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                        
                                        <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
										 --}}
                                         <li><a href="{{route('user-q2a')}}">SEARCH</a></li>
                                         <li><a href="{{route('user-q2a')}}">MY SOCIAL PROFILE</a></li>
                                         <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                          
                                        
                                       {{--   <li class="menuLi"><a style="cursor: pointer;">ADVOCATE {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a> --}}
										 
										<ul class="menuUl">
                                    </ul>
                                    </li>
                              
                        </ul>
                    <?php }else if($check_user_type==2){

                        ?> 
                        <ul id="menuResponsive">
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                     <li><a href="">WIKI MEDIA</a></li>
                                   
                                   {{--    
                                        <li class="menuLi"><a style="cursor: pointer;"> ARTICLES BLOGGING <i class="fa fa-angle-down"></i></a>
                                        <ul class="menuUl">
											 <li><a href="{{route('front.blog')}}">All Articles Blogs</a></li>
											<li><a href="{{route('user-blogs-index')}}">Create New Articles</a></li>
                                        </ul>
											
                                        </li>
                                    --}}
                                         <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
                                         <li><a href="{{route('user-q2a')}}" >MY SOCIAL NETWORK</a></li>
                                        
                                          <li class="menuLi"><a style="cursor: pointer;">LAW STUDENT {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a>
                                     <ul class="menuUl">
										 
                                      {{--   <li><a href="{{route('user-dashboard')}}">{{$lang->dashboard}}</a></li> 
                                        <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li> --}}
                                        <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                        <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                    </ul>
                                    </li>
								                            
                               
                               
                        </ul>

                        <?php
                    }else if($check_user_type==3){
						
						?> 
						
						<ul id="menuResponsive">
                                     
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                     
                                     <li><a href="{{route('user-dashboard')}}">SEARCH LAYWERS</a></li>
                                         
                                     {{-- <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li> --}}
									 <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
									 <li><a href="{{route('user-q2a')}}">GO SOCIAL	</a></li>
									
									
									<li class="menuLi"><a style="cursor: pointer;">PROFESSOR {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a>
									<ul class="menuUl">
										 
                                     {{-- <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li> --}}
                                     <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                     <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                    </ul>
                                    </li>
                                 
                        </ul>
						<?php
						}
                    else{
						
						?>
                        <ul id="menuResponsive">
                                     
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                     
                                     <li><a href="{{route('user-dashboard')}}">SEARCH LAYWERS</a></li>
                                         
                                     {{-- <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li> --}}
									 <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
									 <li><a href="{{route('user-q2a')}}">GO SOCIAL	</a></li>
									
									
									<li class="menuLi"><a style="cursor: pointer;">COMMONERS {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a>
									<ul class="menuUl">
										 
                                     {{-- <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li> --}}
                                     <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                     <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                    </ul>
                                    </li>
                                 
                        </ul>

                    <?php } ?>
                        @else
                        <ul id="menuResponsive">
                               {{-- <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li> 
								   
								     @if(Auth::guard('user')->check())  
                                <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li>
                                
                                 @else
                                
                                 <li><a href="{{route('front.logginplease')}}">{{$lang->blog}}</a></li>
								 @endif
                                <li><a href="{{route('front.index')}}">SEARCH</a></li>
								   --}}
                                <li><a class="active"href="#searchlawyers" >Find a lawyer</a></li>
                                <li><a href="#post-question">Ask a Lawyer</a></li>
                                <li><a href="#knowhelaw">Know the law</a></li>
                                <li><a href="#draftcase">Draft case</a></li>
                                <li><a href="#ourblogs">Our Blogs</a></li>
                                <li><a href="#aboutus">About Us</a></li>
                                <li><a href="#contactus">Contact Us</a></li>
                                <li class="sign-in"><a href="{{route('user-login')}}">Sign In</a></li>
                                <li class="sign-up"><a href="{{route('user-register')}}">Sign Up</a></li>
                                {{--  <li><a href="{{route('front.featured')}}">{{$lang->fh}}</a></li>
                                <li><a href="{{route('front.users')}}">{{$lang->h}}</a></li>

                                @if($ps->a_status == 1)
                                <li><a href="{{route('front.about')}}">{{$lang->about}}</a></li>
                                @endif
                                <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li>
                                @if($ps->f_status == 1)
                                <li><a href="{{route('front.faq')}}">{{$lang->faq}}</a></li>
                                @endif
                                @if($ps->c_status == 1)
                                <li><a href="{{route('front.contact')}}">{{$lang->contact}}</a></li>
                                @endif --}}

                         @endif
                            </ul>
 
                        </div>
            </div>
        </div>
        <!-- Ending of Header area -->
        <!-- Starting of Hero area -->
        @yield('content')
        <!-- starting of subscribe newsletter area -->
        <div class="subscribe-newsletter-wrapper">
            <div class="container"> 
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="subscribe-newsletter-area">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                    <h4>{{$lang->ston}}</h4>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                    <form action="{{route('front.subscribe.submit')}}" method="POST">
                                        {{csrf_field()}}
                                        <input type="email" name="email" placeholder="{{$lang->supl}}" required>
                                        <button type="submit" class="btn">{{$lang->s}}</button>
                                    </form>
                                    <p>
                                    @if(Session::has('subscribe'))
                                        {{ Session::get('subscribe') }}
                                    @endif
                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach
                                </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- Ending of subscribe newsletter area -->
        <!-- starting of footer area -->
        <footer class="section-padding footer-area-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="single-footer-area">
                            <div class="footer-title">{{$lang->about}}</div>
                            <div class="footer-content">
                                <p>{{$gs->about}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="single-footer-area">
                            <div class="footer-title">{{$lang->fl}}</div>
                            <div class="footer-content">
                                <ul class="about-footer">
                                    <li><a href="{{route('front.index')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->home}}</a></li>
                                @if($ps->a_status == 1)
                                    <li><a href="{{route('front.about')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->about}}</a></li>
                                @endif
                                @if($ps->f_status == 1)
                                    <li><a href="{{route('front.faq')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->faq}}</a></li>
                                @endif
                                @if($ps->c_status == 1)
                                    <li><a href="{{route('front.contact')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->contact}}</a></li>
                                @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="single-footer-area">
                            <div class="footer-title">All Pages</div>
                            <div class="footer-content">
                                <ul class="latest-tweet">
                                    @foreach($lblogsss as $lblog)
                                    
                                    
                                     <li><a href="{{route('front.pageshow',$lblog->ID)}}"><i class="fa fa-caret-right"></i> {{$lblog->meta_title}}</a></li>
									
                                    @endforeach
                                    
                                    <li><a href="{{route('front.updatehistory')}}"><i class="fa fa-caret-right"></i>Update History</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-5 col-sm-6 col-xs-12">
                        <div class="single-footer-area">
                            <div class="footer-title">{{$lang->contact}}</div>
                            <div class="footer-content">
                                <div class="contact-info">
                                @if($gs->street != null)                                    
                                  <p class="contact-info">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        {{$gs->street}}
                                    </p>
                                @endif
                                @if($gs->phone != null)   
                                    <p class="contact-info">
                                         <i class="fa fa-phone" aria-hidden="true"></i>
                                        <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a>
                                    </p>
                                @endif
                                @if($gs->email != null)   
                                    <p class="contact-info">
                                         <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                                    </p>
                                @endif
                                @if($gs->site != null)   
                                    <p class="contact-info">
                                        <i class="fa fa-globe" aria-hidden="true"></i>
                                        <a href="{{$gs->site}}">{{$gs->site}}</a>
                                    </p>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="footer-copyright-area">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                      <p class="copy-right-side">
                        {!!$gs->footer!!}
                      </p>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="footer-social-links">
                        <ul>
                            @if($sl->f_status == 1)
                            <li><a class="facebook" href="{{$sl->facebook}}">
                                <i class="fa fa-facebook"></i>
                            </a></li>
                            @endif
                            @if($sl->g_status == 1)
                            <li><a class="google" href="{{$sl->gplus}}">
                                <i class="fa fa-google"></i>
                            </a></li>
                            @endif
                            @if($sl->t_status == 1)
                            <li><a class="twitter" href="{{$sl->twitter}}">
                                <i class="fa fa-twitter"></i>
                            </a></li>
                            @endif
                            @if($sl->l_status == 1)
                            <li><a class="tumblr" href="{{$sl->linkedin}}">
                                <i class="fa fa-linkedin"></i>
                            </a></li>
                            @endif
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </footer>
        <!-- Ending of footer area -->

        <script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/front/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/front/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('assets/front/js/wow.js') }}"></script>
        <script src="{{ asset('assets/front/js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ asset('assets/front/js/main.js') }}"></script>
        {!! $seo->google_analytics !!}
        <script type="text/javascript">
        $(window).load(function(){
            setTimeout(function(){
                $('#cover').fadeOut(1000);
            },1000)
        });
        </script>
<script type="text/javascript">
 jQuery(document).ready(function(){

    $("#register-form-lawyer").hide();
    $("#register-form-serviceSeeker").hide();
    $("#register-form-law-student").hide();
    $("#register-form-law-professor").hide();
    $("#register-form-university").hide();
    $("#user_role_id").change(function(){
        var selectedRegisterType = $(this).children("option:selected").val();
        if(selectedRegisterType==0){

            $("#register-form-lawyer").show();
            $("#register-form-serviceSeeker").hide();
             $("#register-form-law-student").hide();
             $("#register-form-law-professor").hide();
             $("#register-form-university").hide();

        }
        else if(selectedRegisterType==1){
             $("#register-form-lawyer").hide();
              $("#register-form-law-student").hide();
             $("#register-form-serviceSeeker").show();
             $("#register-form-law-professor").hide();
             $("#register-form-university").hide();

        }
        else if(selectedRegisterType==2){

             $("#register-form-lawyer").hide();
              $("#register-form-serviceSeeker").hide();
             $("#register-form-law-student").show();
             $("#register-form-law-professor").hide();
             $("#register-form-university").hide();

        }else if(selectedRegisterType==3){
			
			
			 $("#register-form-lawyer").hide();
              $("#register-form-serviceSeeker").hide();
             $("#register-form-law-student").hide();
             $("#register-form-law-professor").show();
             $("#register-form-university").hide();
			
			}
		else if(selectedRegisterType==4){
			
			 $("#register-form-lawyer").hide();
              $("#register-form-serviceSeeker").hide();
             $("#register-form-law-student").hide();
             $("#register-form-law-professor").hide();
             $("#register-form-university").show();
			
			}
        else{
             $("#register-form-lawyer").hide();
             $("#register-form-serviceSeeker").hide();
             $("#register-form-law-student").hide();
             $("#register-form-university").hide();
        }
    });
  $("#abc").css("display","block");
  $("#abc").css("z-index","99");
 
 
	
 
 
});


//Function To Display Popup
function div_show() {
document.getElementById('abc').style.display = "block";
}
//Function to Hide Popup
function div_hide(){
document.getElementById('abc').style.display = "none";
}

google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselect'));
        google.maps.event.addListener(places, 'place_changed', function () {
			  var place = places.getPlace();
                   // Do watever wif teh value!
                   var lat=place.geometry.location.lat();
                   var lon=place.geometry.location.lng();
				document.getElementById("us3-lat").value = lat;
				document.getElementById("us3-lon").value = lon;
        });
    });



if(document.getElementById('locationselect').value==""){


google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselect'));
        google.maps.event.addListener(places, 'place_changed', function () {
			  var place = places.getPlace();
                   // Do watever wif teh value!
                   var lat=place.geometry.location.lat();
                   var lon=place.geometry.location.lng();
				document.getElementById("us3-lat").value = lat;
				document.getElementById("us3-lon").value = lon;
        });
    });
   
 }
 else
 {
	  document.getElementById("formlocation").style.display="none";
	  document.formlocation.submit();
	 
	} 
  
</script>
        @yield('scripts')
    </body>
</html>
