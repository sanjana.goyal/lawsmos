@extends('layouts.front')
@section('content')

<style>
	small.make-database {
    display: none;
}
</style>
<br>
<br>
<section class="judgment_details">
<div class="container">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<h1>High Court Judgements </h1>
<input type="hidden" name="judgement_id" id="judgement_id" value="{{$highcourt->id}}">	
<input type="hidden" name="judgement_heading" id="judgement_heading" value="{{ html_entity_decode($highcourt->heading)}}">	
<input type="hidden" name="judgement_type" id="judgement_type" value="1">	

		
	
			
			<p>
				{!! base64_decode(html_entity_decode($highcourt->content)) !!}
			
			</p>

			
	


</div>
</section>
@endsection
