@extends('layouts.front')
@section('content')

<div class="histort-page">
<div class="container">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>

			<h1> History </h1>

@if(!empty($history))
<table id="history-table" class="table table-striped table-hover table-bordered dataTable no-footer" role="grid" aria-describedby="friends-table_info">


<tr>
	
	
	<th>Page Name</th>
	<th>History_notes</th>
	<th>Version</th>
	<th>last_updated</th>
	
	
</tr>



@foreach($history as $history)
	<tr>
		
		<td>{{ $history->meta_title }}</td>
		<td><?php echo html_entity_decode($history->history_notes); ?></td>
		<td>{{ $history->version }}</td>
		<td>{{ $history->last_updated }}</td>
		
	
	</tr>
   
@endforeach


</table>
@endif
</div>

</div>
@endsection
