@extends('layouts.front')
@section('styles')
<style type="text/css">
    /* Hide the list on focus of the input field */
datalist {
    display: none;
}
/* specifically hide the arrow on focus */
input::-webkit-calendar-picker-indicator {
    display: none;
}
ul.about-list li {
    padding: 11px;
}

.single-campaignCategories-area h4:hover{
	
	background: #ff9332;
}
	
.single-campaignCategories-area h4 {
    
    height: 82px;
    color: #ffff;
    background: #007bcc;
    padding: 27px;
    margin: -3px;
    margin-bottom: 13px;
}
.blog-thumb-wrapper img {
    width: 100%;
    height: 205px;
}

.qa-box {
    padding: 24px;
}

#regForm {
    background-color: #ffffff;
    margin: 0px auto;
    font-family: Raleway;
    padding: 10px;
    width: 87%;
    min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}
/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

input.form-control.knowthelaw {
    height: 49px;
}

input.btn.btn-block.btn-primary.knowthelaw {
    height: 45px;
    font-weight: 700;
}
</style>
@endsection
@section('content')
<style>
</style>

<section id="searchlawyers">
       <div id="map-canvas" style="width: 100%; height: 600px; display:block;">
	   </div>
	 <div class="container">
                <div class="row">
                   <div class="col-lg-12  col-md-6  col-sm-6  col-xs-12 ">
                        <div class="hero-form">
                            <div class="hero-form-header">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
                                <h3>{{$lang->ss}}</h3>
                                <hr>
                            </div>
                            <div class="hero-form-wrapper">
                                <form action="{{route('user.search')}}" method="GET">
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<div class="input-group">
												  <div class="input-group-addon">
													  <i class="fa fa-fw fa-home"></i>
												  </div>
													 <input id="locationselect" name="search" placeholder="Enter Your Location" type="text" class="form-control" value="<?php if(!empty($location)){ echo $location; }?>">
													 <input type="hidden" class="form-control" name="lat"  value="<?php if(!empty($lat)){ echo $lat; } ?>"  id="us3-lat" />
													 <input type="hidden" class="form-control" name="long"  value="<?php if(!empty($lon)){ echo $lon; } ?>" id="us3-lon" />
												</div>
											  </div>
                                      </div>
                                      
                                      <div class="col-md-4">
                                      <div class="form-group">
                                        <div class="input-group">
                                          <div class="input-group-addon">
                                              <i class="fa fa-fw fa-cog"></i>
                                          </div>
                                          <select name="group" id="blood_grp" class="form-control" required>
                                              <option value="">{{$lang->sbg}}</option>
                                            @foreach($cats as $cat)
                                              <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                       </div>
                                       
                                      <div class="col-md-4"> 
                                      <div class="form-group text-center">
                                            <input type="submit" class="btn btn-block hero-btn" name="button" value="{{$lang->search}}">
                                      </div>
                                      </div>
                                      </div>
                                </form>
                            </div>
                        </div>
                    </div>
                   </div>
                </div>
            </div>
         <!-- Ending of Hero area -->
        </section>
        <!-- Starting of All - sectors area -->
      <section id="searchlawyers">
        <div class="section-padding all-sectors-wrap">
            <div class="container">
                <div class="row">
					<h5 class="text-center">Ask for Assistance</h5>
                    <div class="col-md-12">
                        <div class="section-title pb_50 text-center">
                            <h2>What service do you need?</h2>
                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        @foreach($cats as $cat)
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.lead',$cat->id)}}">
                            <div class="single-campaignCategories-area change{{$cat->id}} text-center">
                            <h4>{{$cat->cat_name}}</h4>
                        </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
         </section>
        <!-- Ending of All - sectors area -->
        <!-- Ending of Team area -->
     <!-- Starting of POST A QUESTION -->
    <section id="post-question">
        <div class="section-padding blog-area-wrapper">
            <div class="container">
                <div class="row">
					<h5 class="text-center">Post Question</h5>
                    <div class="col-md-12">
                        <div class="section-title pb_50 text-center">
                            <h2>HAVE A LEGAL QUESTION</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                     <div class="col-md-12">
						<form id="regForm" action="{{route('front.postquestions')}}" method="get">
									<div class="tab">
									   <div class="form-group">
												<input type="text" class="form-control" name="question" placeholder="Ask your question?" />
										</div>
									</div>
									<div class="tab">
										<h1>Please Fill the details</h1>
									 <div class="form-group">
										<label for="phone">Phone</label>
										<input placeholder="Phone" class="form-control"  name="phone">
									</div>
									   <div class="form-group">
										<label for="name">Name</label>
										<input placeholder="Name" class="form-control" name="name">
									  </div>
									   <div class="form-group">
											<label for="email">Email</label>
											<input placeholder="Email"  class="form-control"  name="email" type="email">
									  </div>
									  <div class="form-group">
										<label for="location">Location</label>
										 <input id="locationselectsss" type="text" class="form-control"  name="locationselect" size="50" placeholder="Enter a location">
										  <input type="hidden" class="form-control" name="lat"  value=""  id="us3-latss" />
											<input type="hidden" class="form-control" name="long"  value="" id="us3-lonss" />
									 
									 
									 </div>
							</div>

<div style="overflow:auto;">
	  <div style="float:right;">
		<button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
		<button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
	  </div>
	</div>
	<!-- Circles which indicates the steps of the form: -->
	<div style="text-align:center;margin-top:40px;">
	  <span class="step"></span>
	  <span class="step"></span>
	</div>
	</form>
</div>
                <div class="row">
                     <div class="col-md-12">
						 <h3 class="text-center">Recently Answered Questions</h3>
                         <div class="owl-carousel blog-area-slider">
                           @foreach($questionarray as  $quas)
								@foreach ($answerarray as $answ)
									@if($answ['parentid'] == $quas['postid'])
									 <a href="{{url('qa',$quas['postid'])}}" class="single-blog-box">
										<div class="qa-box">
											<p class="blog-meta">{{date('d M, Y , H:i a',strtotime($answ['created']))}}
											</p>
											<h4>Question: {{ $quas['title'] }} </h4>
											<p class="blog-meta-text">Answer: {{ substr(strip_tags($answ['content']),0,200) }}</p>
											<span class="boxed-btn blog">View More</span>
										</div>
									</a>
									@endif
								 @endforeach
                            @endforeach
                         </div>
                     </div>
                </div>
            </div>
        </div>
        <!-- Ending of post a question -->
    </section> 
     <!-- Starting of Know the Law -->
     <section id="knowhelaw">
        <div class="section-padding blog-area-wrapper">
            <div class="container">
                <div class="row">
					<h5 class="text-center">Articles</h5>
                    <div class="col-md-12">
                        <div class="section-title pb_50 text-center">
                            <h2>KNOW THE LAW</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
             
                <div class="row">
                     <div class="col-md-12">
						 @if(!empty($knowthelawtext))
						 
							{{ $knowthelawtext }}
						 @else
						<h3>Learn About Teh Law</h3>
						<p>Are you facing a legal issue, or just looking for more information about a specific legal topic? FindLaw's Learn About teh Law section is teh perfect starting point. Learn About teh Law features informational articles about a wide variety of legal topics, as well as specific information about subjects such as how to hire an attorney and understanding your state's unique laws.</p>
						<p>Learn About the Law articles are arranged by area of law, sometimes referred to as a "practice area". Scroll down to find the area of law dat most closely matches your needs, and tan click into the section to find a breakdown of dat area of law and a listing of specific articles. Not sure which area of law is right for you? Try searching for your topic using the search box at the top right of the page, or asking the FindLaw Community to point you towards teh right information resources for your needs.</p>
						 @endif
                     </div>
                </div>
               
				<br>
                <div class="row">
                     <div class="col-md-12">
						 <h3>Recent Articles</h3>
							<div class="owl-carousel blog-area-slider">
								@foreach($socialarticles as $sarticles)
								 <a href="{{route('front.articles',$sarticles->ID)}}" class="single-blog-box">
								   
								   {{--
								   <div class="blog-thumb-wrapper">
									   <img src="{{asset('social/uploads/'.$sarticles->image)}}" alt="Blog Image">
									</div>
								  --}}
                                <div class="blog-text">
                                    <p class="blog-meta">{{date('d M, Y , H:i a',strtotime($sarticles->created_at))}}
                                    </p>
                                    <h4>{{$sarticles->title}}</h4>
                                    
                                    <p class="blog-meta-text">{{substr(strip_tags(base64_decode($sarticles->description)),0,250)}}</p>
                                   
                                   
                                    <span class="boxed-btn blog">{{$lang->vd}}</span>
                                </div>
                            </a>
                            @endforeach
						</div>
                         
                         
                     </div>
                </div>
           
           <br>
           
			  <div class="row">
                     <div class="col-md-6">
                        <div class="know-the-law-thumb-wrapper">
							<img src="{{asset('assets/images/books.png')}}" alt="Law Books Image" height="400">
						</div> 
                     </div>
                       <div class="col-md-6">
					    <div class="row">
							<h5 class="text-left">Judgements</h5>
							<div class="col-md-12">
							<div class="section-title pb_50 text-left">
								<h2>Browse Legal Articles</h2>

									<div class="section-borders text-left">
										<span></span>
										<span class="black-border text-left"></span>
										<span></span>
									</div>
									</div>
								</div>
						</div>
                         <form action="#" method="POST">
                         
							<div class="form-group">
								<input type="text" name="judgements" class="form-control knowthelaw" placeholder="Search Judgements" />
							</div>
							<div class="form-group">
								<input type="text" name="BareAct" class="form-control knowthelaw" placeholder="Search BareAct" />
							</div>
							<div class="form-group">
								<input type="text" name="Maxims" class="form-control knowthelaw" placeholder="Search Maxims" />
							</div>
							<div class="form-group">
								<input type="text" name="Landmark" class="form-control knowthelaw" placeholder="Landmark Judgement" />
							</div>
							<div class="form-group">
								<input type="submit" name="search" class="btn btn-block btn-primary knowthelaw" value="SEARCH" />
							</div>
                         </form>
                     </div>
                </div>
           
            </div>
        </div>
        </section>
        <!-- Ending of Know the Law -->
        
        
        
        
     <!-- Starting of Know the Law -->
     <section id="draftcase">
        <div class="section-padding blog-area-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title pb_50 text-center">
                            <h2>DRAFT YOUR CASE</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
             
                <div class="row">
                      

                    <div class="col-md-3 col-sm-4 col-xs-6">
                       
                      
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            
                            <h4>Legal notice/ reply
to legal notice</h4>
                        </div>
                     
                        </a>
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            
                        <h4>Criminal complaint/defense</h4>
                        </div>
                        </a>
                       
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
						<h4>Plaint/ reply</h4>
                        </div>
                        </a>
                        
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            
                            <h4>Divorce petition/reply</h4>
                        </div>
                        </a>
                        
                    </div>
                  
                   <div class="col-md-3 col-sm-4 col-xs-6">
                       
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            
                            <h4>Writ petition</h4>
                        </div>
                        </a>
                       
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            
                            <h4>Lease deed</h4>
                        </div>
                        </a>
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            <h4>Business contract</h4>
                        </div>
                        </a>
                    </div>
                     <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            <h4>Employment contract</h4>
                        </div>
                        </a>
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            <h4>Maintenance application/ reply</h4>
                        </div>
                        </a>
                        </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            <h4>Consumer case</h4>
                        </div>
                        </a>
                    </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                         <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
                            <h4>Domestic violence application/ reply</h4>
                        </div>
                        </a>
                     </div>
                   <div class="col-md-3 col-sm-4 col-xs-6">
                        <a href="{{route('front.draftcase')}}" class="draftcase">
                            <div class="single-campaignCategories-area change text-center">
									<h4>Others</h4>
								</div>
                        </a>
                    </div>
                 </div>
            </div>
        </div>
        </section>
        <!-- Ending of Know the Law -->
        <!-- Starting of blog area -->
        <section id="ourblogs">
        <div class="section-padding blog-area-wrapper">
            <div class="container">
                <div class="row">
					<h5 class="text-center">FROM OUR BLOGS</h5>
                    <div class="col-md-12">
                        <div class="section-title pb_50 text-center">
                            <h2>LEGAL NEWS</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
             
                <div class="row">
                     <div class="col-md-12">
                         <div class="owl-carousel blog-area-slider">
                            @foreach($socialblogs as $sblog)
                             <a href="{{route('front.blogshow',$sblog->ID)}}" class="single-blog-box">
                               <div class="blog-thumb-wrapper">
                                   <img src="{{asset('social/uploads/'.$sblog->image)}}" alt="Blog Image">
                               </div>
                                <div class="blog-text">
                                    <p class="blog-meta">{{date('d M, Y , H:i a',strtotime($sblog->created_at))}}
                                    </p>
                                    <h4>{{$sblog->title}}</h4>
                                    <p class="blog-meta-text">{{substr(strip_tags($sblog->body),0,250)}}</p>
                                    <span class="boxed-btn blog">{{$lang->vd}}</span>
                                </div>
                            </a>
                            @endforeach
                         </div>
                     </div>
                </div>
            </div>
        </div>
        </section>
        <!-- Ending of blog area -->
		<section id="aboutus">
			 <div class="section-padding blog-area-wrapper">
				<div class="container">
					<div class="row">
						<h5 class="text-center">ABOUT US</h5>
						<div class="col-md-12">
							
							<div class="section-title pb_50 text-center">
								<h2>What is lawyerIND?</h2>

								<div class="section-borders">
									<span></span>
									<span class="black-border"></span>
									<span></span>
								</div>
							</div>
						</div>
					</div>
               
                 <div class="row">
					 <h5 class="text-center">India's largest online legal community which enables its users to:</h5>
                     <div class="col-md-8">
							<ul class="about-list">
								<li>Find the best lawyers in their city.</li>
								<li>Seek answers to any law realted question from expert legal minds of the
field and top lawyers.</li>
								<li>Access laws, bareacts, landmark judgments of India.</li>
								<li>Read subject wise articles and legal research by top lawyers.</li>
								<li>Browse lawyer profile and connect with the right set of lawyers.</li>
								<li>Get expert legal advice.</li>
								<li>Rate and review lawyers.</li>
								<li>Find the best lawyers within budget.</li>
								<li>Cross-check if a case is being correctly handled.</li>
								<li>Draft a case/ notice/ complaint/ deed etc. using free case builder.</li>
								<li>Conduct legal research with the help of artificial intelligence and advance
research tools.</li>
							</ul>
                     </div>
                     <div class="col-md-4">
							 <img src="{{asset('assets/images/2019-08-06.png')}}" alt="Indian Law">
                     </div>
                </div>
                
               </div>
	</section>
<section id="contactus">
			 <div class="section-padding blog-area-wrapper">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="section-title pb_50 text-center">
								<h2>CONTACT US</h2>
								<div class="section-borders">
									<span></span>
									<span class="black-border"></span>
									<span></span>
								</div>
							</div>
						</div>
					</div>
                 <div class="row">
                     <div class="col-md-8 col-md-offset-2">
							<div class="alert alert-success" style="display:none"></div>
							<div class="alert alert-danger" style="display:none"></div>
							<div id="errors"></div>
							 @include('includes.form-error')
                            	@include('includes.form-success')
							<form id="contactform">
								<div class="form-group">
									<label for="name">Name</label>
									<input type="text" name="name" id="name" class="form-control" placeholder="Enter Name"  />
								</div>
								<div class="form-group">
									<label for="email">Email</label>
									<input type="email" name="email" id="email"  class="form-control" placeholder="Enter Email"  />
								</div>
								<div class="form-group">
									<label for="locationselect">Location</label>
									<input id="locationselects"   name="locationselects" placeholder="Enter Your Location" type="text" class="form-control"   />
									<input type="hidden" class="form-control" name="lat"  value=""  id="us3-lat" />
									<input type="hidden" class="form-control" name="long"  value="" id="us3-lon" />
								</div>
								<div class="form-group">
									<label for="phone">Phone Number</label>
									<input type="tel" name="phone" id="phone"  class="form-control" placeholder="Enter Phone"  />
								</div>
									<div class="form-group">
									<label for="message">Message</label>
									<textarea name="message" id="message"  class="form-control" ></textarea>
								</div>
								<button  class="btn btn-danger" id="ajaxSubmit">Submit</button>
							</form>
                     </div>
                </div>
               </div>
	</section>
<script>
google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselects'));
        google.maps.event.addListener(places, 'place_changed', function () {
			  var place = places.getPlace();
                   // Do watever wif teh value!
                   var lat=place.geometry.location.lat();
                   var lon=place.geometry.location.lng();
				document.getElementById("us3-lat").value = lat;
				document.getElementById("us3-lon").value = lon;
        });
    });
    
    
google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselectsss'));
        google.maps.event.addListener(places, 'place_changed', function () {
			  var place = places.getPlace();
                   // Do watever wif teh value!
                   var lat=place.geometry.location.lat();
                   var lon=place.geometry.location.lng();
				document.getElementById("us3-latss").value = lat;
				document.getElementById("us3-lonss").value = lon;
        });
    });
    
     $(document).ready(function(){
            $('#ajaxSubmit').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               $.ajax({
                  url: "{{ url('contactus') }}",
                  method: 'GET',
                  data: {
                     name: $('#name').val(),
                     email: $('#email').val(),
                     location: $('#locationselects').val(),
                     phone: $('#phone').val(),
                     message: $('#message').val(),
                  },
                  success: function(result){
                    
                     $('.alert-success').show();
                     $('.alert-success').html(result.success);
                     $('.alert-danger').hide();
                   
                  },
                  error: function(result){
					 if(result.status === 400 ) {
						var response = JSON.parse(result.responseText);
							$.each(response, function (key, val) {
								
								 $('.alert-danger').show();
								$('.alert-danger').html(all_values(val));
								
								//$("#errors").append("<li class='alert alert-danger'>"+all_values(val)+"</li>");	
									 
							});
					}
					  //  $('.alert-danger').show();
					  // $('.alert-danger').html(result.failed);

					}
                  
                  });
               });
             });
 function all_values(obj) {
    var keys = _keys(obj);
    var lengths = keys.length;
    var values = Array(lengths);
    for (var i = 0; i < lengths; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  }
function _keys(obj) 
 {
    if (!isObject(obj)) return [];
    if (Object.keys) return Object.keys(obj);
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    return keys;
  }
function isObject(obj)   
{  
    var type = typeof obj;  
    return type === 'function' || type === 'object' && !!obj;  
  } 
</script>

{{--
        <div class="section-padding testimonial-wrapper mb_50 overlay" style="background-image: url({{asset('assets/images/'.$gs->bgimg)}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title pb_50 text-center">
                            <h2>{{$lang->hcs}}</h2>

                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel t_carousel10">
                            @foreach($ads as $ad)
                            <div class="single_testimonial text-center">
                                <div class="border_extra mb_50 pos_relative">
                                    <div class="author_info">
                                        <h4>{{$ad->client}}</h4>
                                        <span>{{$ad->designation}}</span>
                                        <p class="author_comment color_66">{{$ad->review}}</p>
                                    </div>
                                </div>
                                <div class="author_img radius_100p pos_relative"><img src="{{asset('assets/images/'.$ad->photo)}}" class="radius_100p" alt="author"></div>
                            </div>                        
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
   
       --}}
    
<script>
	
		var map;
        var geocoder;
        var marker;
        var people = new Array();
        var latlng;
        var infowindow;
		
        $(document).ready(function() {
			
            ViewCustInGoogleMap();
        });
       
       
        function ViewCustInGoogleMap() {

            var mapOptions = {
                center: new google.maps.LatLng(<?php if(isset($lat) && isset($lon)){ echo $lat.",".$lon; }else if(isset($loggedinlat) && isset($loggedinlon)){ echo $loggedinlat.",".$loggedinlon; }else{ echo 0,0; } ?>),   // Coimbatore = (11.0168445, 76.9558321)
                zoom: 13,
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
			
			

            // Get data from database. It should be like below format or you can alter it.
			
			var data='<?php if(isset($result)){ echo json_encode($result); }  ?>';
			 // var data = '[{"id": 183,"name": "Arjun gupta","username": "Arjun", "LatitudeLongitude": "30.596050759460997,76.84414168413082","MarkerId": "lawyer"},{"id": 183,"name": "SBP housing","username": "Mr Parminder singh", "LatitudeLongitude": "30.5881,76.8476","MarkerId": "lawyer"}]';
           //   var data = '[{ "name": "sunielsethy", "addr": "Chandigarh", "LatitudeLongitude": "30.7333148,76.7794179", "MarkerId": "Customer" },{ "DisplayText": "abcd", "ADDRESS": "Coimbatore-641042", "LatitudeLongitude": "11.0168445,76.9558321", "MarkerId": "Customer"}]';

            people = JSON.parse(data); 

            for (var i = 0; i < people.length; i++) {
                setMarker(people[i]);
            }


        }

		
        function setMarker(people) {
            geocoder = new google.maps.Geocoder();
            infowindow = new google.maps.InfoWindow();
            if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
                geocoder.geocode({ 'address': people["addr"] }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                        marker = new google.maps.Marker({
                            
                            position: latlng,
                            map: map,
                            draggable: false,
                            html: people["name"],
                            icon: "images/marker/" + people["MarkerId"] + ".png"
                        
                        });
                        //marker.setPosition(latlng);
                        //map.setCenter(latlng);
                        google.maps.event.addListener(marker, 'click', function(event) {
                            infowindow.setContent(this.html);
                            infowindow.setPosition(event.latLng);
                            infowindow.open(map, this);
                        });
                    }
                    /*
                    else {
                        alert(people["name"] + " -- " + people["addr"] + ". This address couldn't be found");
                    }
                    */
                });
            }
            else {
				
				if(people["MarkerId"]=="Lawyer"){
                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false,               // cant drag it
                    html: '<div jstcache="2"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"><img src="<?php echo url("social/"); ?>/uploads/'+people["photo"]+'" class="img img-thumbnail" width="100" height="100" alt="no image found.." /></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width">'+people["name"]+','+people["education"]+'</div> </div> </div>',
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    animation: google.maps.Animation.DROP,
                    // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });
                
			}else{
				
				var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false,  
                    mapTypeControl: false,             // cant drag it
                    html: '<div jstcache="2"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width">'+people["MarkerId"]+'</div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width">'+people["name"]+','+people["description"]+'</div> </div> </div>',
                    //icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    animation: google.maps.Animation.DROP,
                       // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });
				
				
				}
            }
        }
       
       </script>
   <script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
@endsection
