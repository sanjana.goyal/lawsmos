@extends('layouts.front')
@section('styles')
@endsection
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    #can_btn {
        /* border: 2px solid #000 !important; */
        padding: 2px 3px;
        border-radius: 4px !important;
        /* background: #000 !important; */
        font-size: 10px;
    }

    #draft_case {
        overflow-y: scroll;
        height: 20vh;
    }

    #draf_div {
        margin-top: 0.5rem;
    }

    #draft_heading {
        text-align: center;
    }

    #ask_laywer {
        background: #000;
        color: #fff;
        text-transform: uppercase;
        padding: 13px 8px;
        font-size: 16px;
        margin-bottom: 1px;
        text-align: center;
        margin-bottom: 1.2rem;
    }

    .featured_lawyers {
    background-color: #000;
    padding: 50px 0 50px;
    border-bottom: 1px solid #1e1e1e;
}

    .featured_lawyers h2{
        font-size: 25px;
        text-align: center;
        font-weight: 600;
        text-transform: uppercase;
        margin-bottom: 50px;
    }

    .featured_lawyers h2, .featured_lawyers .content-white{
        color: #fff;
    }

    button #contact_us_cn_btn {
        background-color: black;
        font-size: 10px;
        padding: 2px 4px;
        float: right;
    }
   
</style>

<section id="searchlawyers">
    <div id="map-canvas" style="width: 100%; height: 500px; display:block;"></div>
    <div class="container">
        <div class="hero-form">
            <div class="hero-form-wrapper">
                <form action="{{route('user.search')}}" method="GET">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <input id="locationselect" name="search" placeholder="Enter Your Location" type="text" class="form-control" value="<?php if (!empty($location)) { echo $location;} ?>" required>
                                    <input type="hidden" class="form-control" name="lat" value="<?php if (!empty($lat)) {
                                                                                                    echo $lat;
                                                                                                } ?>" id="us3-lat" />
                                    <input type="hidden" class="form-control" name="long" value="<?php if (!empty($lon)) {
                                                                                                        echo $lon;
                                                                                                    } ?>" id="us3-lon" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 cl-xs-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <select name="group" id="blood_grp" class="form-control" required>
                                        <option value="">{{$lang->sbg}}</option>
                                        @foreach($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 col-sm-2">
                            <div class="form-group text-center">
                                <input type="submit" class="btn btn-block hero-btn" name="button" value="{{$lang->search}}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <!-- Ending of Hero area -->
</section>
<!-- Starting of All - sectors area -->
<section id="searchlawyers">
    <div class="section-padding all-sectors-wrap ">
        <div class="container">
            <div class="row">
                <h5 class="text-center">Ask for Assistance</h5>
                <div class="col-md-12">
                    <div class="section-title pb_50 text-center">
                        <h2>What service do you need?</h2>
                        <div class="section-borders">

                            <span class="black-border"></span>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row service-needed">
                @foreach($cats as $cat)
                <ul>
                    <li>
                        <a href="{{route('front.lead',$cat->id)}}">
                            <div class="single-campaignCategories-area service-content-p change{{$cat->id}} text-center">
                                <h4>{{$cat->cat_name}}</h4>
                            </div>
                        </a>
                    </li>
                </ul>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- Ending of All - sectors area -->
<section class="featured_lawyers">
    <div class="container">
        <h2 class="text-center">Featured Lawyers</h2>
        <div class="row">
            <div class="uti-featured-flex">
                @php $userInfo = array(); @endphp
                @if($subscribe_lawyers)
                    @foreach($subscribe_lawyers as $lawyer)
                        <?php 
                            $profile = \DB::table(SOCIAL_DB_NAME.'.coreprofile')->where('userid', $lawyer['id'])->first();

                            $practice_of_interst = \DB::table(SOCIAL_DB_NAME.'.catagories')->whereIn('cid', explode(',', $profile->unpracticing_fields_of_interest))->get();
                            // dd($practice_of_interst);
                            $practice_area = '';
                            if(count($practice_of_interst)){
                            foreach ($practice_of_interst as $value) {
                            $array[] = $value->c_name;
                            }
                            $practice_area = implode(',', $array);
                            }else{
                            $practice_area = 'N/A';
                            }
                            // dd($practice_area);
                            $lawyerInfo[] = $lawyer; 
                            $topratedreview = \DB::table(SOCIAL_DB_NAME.'.rating')->where('lawyerid',$lawyer['id'])->orderby('rating','desc')->first();
                            $ratingcount = \DB::table(SOCIAL_DB_NAME.'.rating')->where('lawyerid',$lawyer['id'])->orderby('rating','desc')->get()->count();
                               $ratingvalue = \DB::table(SOCIAL_DB_NAME.'.rating')->where('lawyerid',$lawyer['id'])->orderby('rating','desc')->get()->avg('rating');
                                $exp = \DB::table(SOCIAL_DB_NAME.'.coreprofile')->where('userid',$lawyer['id'])->first();
                                
                            // var_dump($lawyerInfo);
                            if($loggedinuser){  
                                $friendrequest = \DB::table(SOCIAL_DB_NAME.'.user_friend_requests')->where('friendid',$lawyer['id'])->where('userid',$loggedinuser)->first();
                                $friend = \DB::table(SOCIAL_DB_NAME.'.user_friends')->where('friendid',$lawyer['id'])->where('userid',$loggedinuser)->first();
                            }
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="team_common">
                                <div class="profile-header-avatar">
                                    <a href="{{route('front.user.profile',$lawyer['id'])}}"><img src="{{ $lawyer['avatar'] ? asset('social/uploads/'.$lawyer['avatar']):asset('social/uploads/2ae79bbb8e5fa5205464b5f42523a484.png')}}" alt="member image"></a>
                                    <div class="exp-class">{{$exp->total_exp}} Year(s) Experience</div>
                                </div>

                                <div class="content">
                                    <a href="{{route('front.user.profile',$lawyer['id'])}}" class="law_n fw_600 content-white">{{$lawyer['name']}}</a>
                                    <!---p class="content-white">Location: {{ $lawyer['city'] }} </p--->

                                  
                                    <div class="rank-bar">
                                        <div class="rank-point">Rank #3</div>

                                    <input class="ratingbar" value="{{ number_format($ratingvalue,1) }}" class="rating" data-min="0" data-max="5" data-step="1"> <div class="inner_rating" style="color: white;">{{ number_format($ratingvalue,1) }}, {{$ratingcount}} Users</div>

                                    </div>

                                      @if($lawyer['cityrank'])
                                    <div class="content-white"><strong>#{{ $lawyer['cityrank'] }}</strong> in {{ $lawyer['city'] }}</div>
                                    @endif
                                    <!---p class="content-white"> 
                                        <strong>Review:</strong> 
                                        @if($topratedreview) {{ $topratedreview->comment }} @endif                                    
                                    </p--->
                                    <p class="cat-tool-tip">
                                    <span class="d_block transition_3s content-white">  <?php echo $lawyer['description'] != '' ? $lawyer['description'] :  'N/A';  ?></span>
                                    <a href="#" data-toggle="tooltip" title="<?php echo $lawyer['description'] != '' ? $lawyer['description'] :  'N/A';  ?>">View all</a>
                                    </p>
                                   <!--  <p class="lap-pratice"><span>Area of practice: <?php echo $practice_area != '' ? $practice_area :  'N/A';  ?></span> <a href="#" data-toggle="tooltip" title="<?php echo $practice_area != '' ? $practice_area :  'N/A';  ?>">View all</a> -->

                                    </p>


                                </div>
                                <div class="vp-wrap vp-flex-area"> 
                                        <a class="btn btn-xs" href="{{route('front.user.profile',$lawyer['id'])}}">View Profile</a>
                                        <?php if ($loggedinuser){ if($friendrequest){ ?>
                                            <a href="#" class="btn btn-success btn-sm disabled" id="friend_button_<?php echo $lawyer['id'] ?>">Connection request sent</a>
                                            <a class="btn btn-xs" href="<?php echo url('social/home/appointment'); ?>/<?php echo $lawyer['id']; ?>">Book Appointment</a>
                                        <?php } else { ?>
                                            <a href="javascript:void(0);" class="btn btn-xs" onclick="add_friend('<?php echo $lawyer['id'] ?>','<?php echo url('social/profile/add_friend/').'/'.$lawyer['id']; ?>')" id="friend_button_<?php echo $lawyer['id'] ?>">Connect</a>
                                            <a class="btn btn-xs" href="<?php echo url('social/home/appointment'); ?>/<?php echo $lawyer['id']; ?>">Book Appointment</a>
                                        <?php } } else { ?>
                                            <a href="#" class="btn btn-xs check-btn" id="friend_button_<?php echo $lawyer['id'] ?>" data-user="{{$lawyer['id']}}" data-btn="connect" data-toggle="modal" data-target="#registerModal">Connect</a>
                                            <a href="#" class="btn btn-xs check-btn" data-user="{{$lawyer['id']}}" data-btn="booking" data-toggle="modal" data-target="#registerModal">Book Appointment</a>
                                        <?php } ?>
                                    </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <!-- <div class="team_common"> -->
                            <div class="content content-white">
                                <h4 class="text-center">No Record Found</h4>
                            </div>
                        <!-- </div> -->
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<section class="two-part-set" id="post-question">
    <div class="container">
        <h2 class="sep-line-heading">Have a Legal Question ?</h2>
        
                        @include('includes.form-success')
        <div class="hlq-flex">
            <div class="hlq-left">
                <h3>Recently Answered Questions</h3>
                @if($questionarray)
                    @foreach($questionarray as $quas)
                        @php 
                            $total_ans = \DB::table("qa_posts")->where("type", "A")->where("parentid", $quas['postid'])->get()->count(); 
                            $user = \DB::table(SOCIAL_DB_NAME . '.users')->where("ID", $quas['userid'])->first();
                        @endphp
                        <div class="raq-single">
                            <div class="raq-left">
                                @if($user)
                                    @if($user->avatar)
                                        <img src="{{asset('social/uploads/'.$user->avatar)}}" alt="Indian Law">
                                    @else
                                        <img src="{{asset('assets/front/img/law-set-image.jpg')}}" alt="Indian Law">
                                    @endif
                                   
                                    <!--span>{{$user->city}}</span--->
                                @endif
                            </div>
                            <div class="raq-right">
                            <strong class="f-name-react">{{ isset($user->first_name) ? $user->first_name : '' }}</strong>                                                                
                                <div class="d-time-blue">
                                    <span>{{date('d M, Y',strtotime('+12.5 hours +30 minutes',strtotime($quas['created'])))}}</span> , 
                                    <span>{{date('H:i a',strtotime('+12 hours  +30 minutes',strtotime($quas['created'])))}}</span>
                                </div>
                                <p>{{ $quas['title'] }}</p>
                                <ul>
                                    <li><i class="fa fa-eye" aria-hidden="true"></i> <span>{{$quas['views']}}</span></li>
                                    <li>Answers <span>{{$total_ans}}</span></li>
                                    <li> <a href="{{url('qa',$quas['postid'])}}"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="hlq-right">
                <h3>Ask your Legal Question</h3>
                <div class="wl-left">
                    <ul>
                        <li>Questions are answered by Lawsmos community of Practicing 
                        Advocates, Lawyers, Law Professors and Students.</li>
                        <li>You can choose to hide your identity and ask as anonymous</li>
                        <li>Be reasonable & Logical</li>
                        <li>Be Respectful & Polite</li>
                        <li>Absurd, Disrespectful, or Illogical questions will be discarded</li>
                    </ul>
                </div>

                <p></p>

                <div class="lq-form">
                    <form id="regForm" class="legal-question" action="{{route('front.postquestions')}}" method="get">
                        <div class="home-sec-heading">
                            {{csrf_field()}}
                            @include('includes.form-error')
                            @include('includes.form-success')

                            <div class="alert alert-success" style="display:none" id="post_div_success"></div>
                            <div class="alert alert-danger" style="display:none" id="post_div_error">
                                <button type="button" class="close" onclick="hide_post_div_error('post_div_error');" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                            </div>
                        </div>
                        <input name="question" id="question" type="text" placeholder="Ask your Question">
                        <textarea name="description" id="description" placeholder="Add any relevant details to your question so as to get context specific answers (optional)"></textarea>
                        <input type="hidden" name="checkuser" id="checkuser" value="{{$loggedinuser}}">
                        <input type="hidden" name="alreadylogin" id="alreadylogin" value="{{$loggedinuser}}">
                        
                        <select class="form-control selectpicker" multiple title="Select Categories" data-max-options="4" name="" id="catagories_select_for_Query" required="">  
                        <option value="" disabled="">SELECT RELATED CATEGORIES <span>(up to 4)</span></option>
                            @foreach($catss as $cat)
                            <option value="{{$cat->cid}}">{{$cat->c_name}}</option>
                            @endforeach
                        </select>
                        <input type="submit" value="Submit" id="legal-qbtn" data-btn="legal-question" data-user="" class="check-btn">
                    </form>

                </div>
            </div>
        </div>
    </div>
</section> 

<!--question_wrap section ends-->

<section class="draft-case-new" id="draftcase">
    <div class="container">
        <div class="fraft-flex">
            <div class="draft-content">
                <h2 class="sep-line-heading">DRAFT YOUR CASE</h2>
                <ul class="list-wrap" id="draft_case">

                        @if(!empty($performa))
                        @foreach($performa as $perf)
                        <li><a href="{{route('front.draftcase',['title' => base64_encode($perf->title) ,'content'=>base64_encode($perf->content)])}}">
                                {{ $perf->title }}</a></li>
                        @endforeach
                        @endif
                        <span id="view-more-draft-result">

                        </span>
                    </ul>
            </div>
        </div>
    </div>
</section>


<!--know-law-sec starts-->

<section class="know-law-new" id="knowhelaw">
    <div class="top-tile">
        <h2 class="sep-line-heading">Know The Law</h2>
    </div>
    <div class="container">
    <h4 class="ts-tile">Articles</h4>
        <div class="row">
            <!-- <div class="col-md-12">
            <div class="arti-search">
                   
                    <select id="catagories_select" class="form-control">
                    <option>Select Category</option>
                    @foreach($catss as $cat)
                    <option value="{{$cat->cid}}">{{$cat->c_name}}</option>
                    @endforeach
                 </select>
                 <div id="result"></div>                   
                </div>
            </div> -->
            <div class="col-md-4 col-sm-4">
                <div class="single-vts">
                    <h3>pick of the day </h3>
                            <ul>
                                <li><a href="javascript:void(0);">How can I get free Legal advice ?</a></li>
                            </ul>
                            <div class="s-more">
                                <a href="{{route('front.listallarticles')}}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="single-vts">
                    <h3>latest </h3>
                    <ul class="">
                    @if(!empty($socialarticles))
                    @foreach($socialarticles as $sarticles)
                    <li><a href="{{route('front.articles',$sarticles->ID)}}">{{$sarticles->title}}</a></li>
                    @endforeach
                    @endif
                </ul>
                            <div class="s-more">
                                <a href="{{route('front.listallarticles')}}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="single-vts">
                    <h3>popular </h3>
                    <ul class="">
                        @if(!empty($populararticles))
                        @foreach($populararticles as $sarticles)
                        <li><a href="{{route('front.articles',$sarticles->ID)}}">{{$sarticles->title}}</a></li>
                        @endforeach
                        @endif
                     </ul>
                        <div class="s-more">
                            <a href="{{route('front.listallarticles')}}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </div>
                </div>
            </div>
      
        </div>
    </div>
</section>


<section class="judge-new">
<div class="container">

<div class="row">
<div class="col-md-7">
<h2>Recent Judgements</h2>
    <div class="judge-left">
    <ul class="list-wrap" id="judgements_list">
                    @if(!empty($recentjudgements))
                    @foreach($recentjudgements as $judge)
                    <li><a href="{{ URL('detail/'.$judge->id)}}">{{$judge->heading}}
                <ul class="judge-list-bar">
                    <li>Date of Hearing: <strong>{{$judge->dateofhearing}}</strong></li>
                    <li>Year: <strong>{{$judge->year}}</strong></li>
                    <li>State: <strong>{{$judge->state}}</strong></li>
                </ul>
                </a></li>
                    @endforeach
                    @endif
                </ul>
                <div class="s-more">
                                <a href="{{route('front.listallhighcourt')}}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
    </div>
</div>
<div class="col-md-5">

    <div class="judge-right">
    <h2>Search Judgements</h2>
    <form action="{{ url('submitsearch') }}" method="GET">

{{ csrf_field() }}
<div class="search-judge-wrap">

    <div class="form-group">
        <select class="form-control" name="court">
            <option value="">Select Court</option>
            @foreach($states as $state)
            <option value="{{$state->state}}">{{$state->state}}</option>
            @endforeach

        </select>
    </div>
    <div class="form-group">
        <input type="text" name="year" placeholder="Year" class="form-control" onkeypress="return isNumber(event)" />
    </div>
    <div class="form-group">
        <select class="form-control" name="catagory">
            <option value="">Select Catagory</option>
            @foreach($catss as $catg)
            <option value="{{$catg->cid}}">{{$catg->c_name}}</option>
            @endforeach

        </select>
    </div>
    <div class="form-group">
        <input type="text" name="content" id="search-landmark" class="form-control" placeholder="Content" />
    </div>
    <div class="form-group">
        <input type="submit" name="search" class="search-btn-home" value="Search Judgment" />
    </div>
</div>
</div>
</form>
    </div>
</div>
</div>

</div>
</section>



<section class="judge-new with-Bareacts">
    <div class="container">
        <h2>Bareacts</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="judge-left">
                    <h3>Recent</h3>
                    <ul class="list-wrap">
                    @if(!empty($bareacts))
                    @foreach($bareacts as $bareact)
                    <li><a href="{{ URL('act_details/'.$bareact->id)}}">{{ $bareact->title }}</a></li>

                    @endforeach
                    @endif
                </ul>
                    <div class="s-more see-all">
                        <a href="{{route('front.bareacts')}}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="judge-left">
                    <h3>Popular</h3>
                    <ul class="list-wrap">
                    <li><a href="{{URL('act_details/257')}}">Constitution of India</a></li>
                    <li><a href="{{URL('act_details/521')}}">Indian Penal Code</a></li>
                    <li><a href="{{URL('act_details/224')}}">Code of criminal procedure</a></li>
                    <li><a href="{{URL('act_details/521')}}">Code of civil procedure</a></li>
                    <li><a href="{{URL('act_details/521')}}">Indian Evidence Acts</a></li>
                </ul>
                    <div class="s-more">
                        <a href="{{route('front.bareacts')}}">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>

            

        </div>

    </div>
</section>

<!--know-law-sec ends-->
<!-- maxims section start-->


<section class="maxi-set">
<div class="container">
<h2>Maxim of the day</h2>

<div class="max-flex">
<div class="maxiright max-qutes">

<p>
      @if(!empty($newmaxims))

      <?php $key = rand(0,count($newmaxims)-1) ?>
<a href="<?=env('APP_URL')?>view_maxims/384">{{$newmaxims[$key]->term}}</a>
{{$newmaxims[$key]->def}}
</p>
 @endif
                                        </div>
<div class="maxileft">
<ul>
    @if(!empty($newmaxims))
        @foreach($newmaxims as $newmaxim)
            <li>
                <a href="{{ URL('view_maxims/'.$newmaxim->ID)}}">
                    <strong>{{ $newmaxim->term }}</strong><br>
                    {{ $newmaxim->def }}
                </a>
            </li>
        @endforeach
    @endif

                <!-- <li><a href="javascript:void(0);">Intentia caeca mala.</a>
                    <span>Culpable recklessness is intent.</span>
                </li>

                <li><a href="javascript:void(0);">Furiosus absentis loco est.</a>
                    <span>The insane are not present (i.e. cannot be witnesses).</span>
                </li>

                <li><a href="javascript:void(0);">Procedure Ordine placitande servato servatur et jus.</a>
                    <span>When proper procedure is observed the law is observed.</span>
                </li>

                <li><a href="javascript:void(0);">Furiosus solo furore punitur.</a>
                    <span>A madman is punished only by his madness.</span>
                </li> -->
                 </ul>
                 <div class="s-more">
                        <a href="{{route('front.allmaxims')}}">See Maxims library <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
</div>


</div>
</div>
</section>

<!-- maxims section end-->

<!--legal-blog-sec starts-->

<section class="news-blog-outer" id="ourblogs">
    <div class="container">
        <div class="blog-news-flex">
            <div class="left-news">
            <h2 class="lb-head">legal news</h2>
            <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#leaglnews1">National</a></li>
            <li><a data-toggle="tab" href="#leaglnews2">State</a></li>
            <li><a data-toggle="tab" href="#leaglnews3">International</a></li>
            </ul>
            <div class="tab-content">
            @if($newspost)
                @php $legalcount=1; @endphp
                @foreach($newspost as $nt)
                    <div id="leaglnews{{$legalcount}}" class="tab-pane fade <?php echo ($legalcount==1) ? 'in active' : ''; ?>">
                       <div class="ns-content">
                            @if($nt->image)
                                <img src="{{asset('social/uploads/'.$nt->image)}}" alt="Indian Law">
                            @else
                                <img src="{{asset('assets/front/img/news-image.jpg')}}" alt="Indian Law">
                            @endif
                           <h4>{{$nt->title}}</h4>    
                           <p> <span class="legal-news-content">{{strip_tags($nt->body)}} &nbsp;</span> <a href="{{ URL('newsdetail/'.$nt->ID)}}"> Read More... </a></p>                               
                        </div>
                    </div>
                    @php $legalcount++; @endphp
                @endforeach
            @endif
       <!--      <div id="leaglnews1" class="tab-pane fade">
            <div class="ns-content">
                   
               <img src="{{asset('assets/front/img/news-image.jpg')}}" alt="Indian Law">
               <h4>Shakti Kapoor International</h4>    
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat <a href="#">Read More...</a></p>
                   
                </div>
                

            </div>
            <div id="menu2" class="tab-pane fade">
            <div class="ns-content">
                   
               <img src="{{asset('assets/front/img/news-image.jpg')}}" alt="Indian Law">
               <h4>John Jordan jonadan</h4>   
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat <a href="#">Read More...</a></p>
                   
                </div>
                

            </div> -->
            </div>

            </div>
            <div class="right-blog-set">
            <h2 class="lb-head">Blog <a href="{{route('front.blog')}}">View more</a></h2>
            <ul class="blog-list">
                        @foreach($socialblogs as $sblog)
                        <li>
                            <div class="bleg-l-image" style="background-image:url({{asset('social/uploads/'.$sblog->image)}})">
                            </div>
                            <div class="blog-text-area">
                                <div class="head-b-flex">
                                    <div class="head-bleft">
                                <h2>{{$sblog->title}}</h2>
                                <span>{{date('d M, Y , H:i a',strtotime($sblog->created_at))}}</span>
                                </div>
                                <a href="{{route('front.blogshow',$sblog->ID)}}">View Details</a>
                                </div>
                                <p>{{substr(strip_tags($sblog->body),0,250)}}</p>
                                <a href="{{route('front.blogshow',$sblog->ID)}}">View Details</a>
                            </div>

                            
                                
                           

                        </li>
                        @endforeach
                    </ul>

            </div>
        </div>
    </div>
</section>


<!--lwyerind-sec starts-->
<section class="wl-outer" id="aboutus">
<div class="container">
    <div class="wl-flex">
<div class="wl-left">
<h3>What is Lawsmos ?</h3>
    <p>India's largest online legal community which enables its users to:</p>
    <ul>
                <li>Find the best lawyers in their city.</li>
                <li>Seek answers to any law related question from expert legal minds of the field and top lawyers.</li>
                <li>Access laws, bareacts, landmark judgments of India.</li>
                <li>Read subject wise articles and legal research by top lawyers.</li>
                <li>Browse lawyer profile and connect with the right set of lawyers.</li>
                <li>Get expert legal advice.</li>
                <li>Rate and review lawyers.</li>
                <li>Find the best lawyers within budget.</li>
                <li>Cross-check if a case is being correctly handled.</li>
                <li>Draft a case/ notice/ complaint/ deed etc. using free case builder.</li>
                <li>Conduct legal research with the help of artificial intelligence and advance research tools.</li>
            </ul>
</div>

<div class="wl-right  judge-right" id="contactus">
    <h3 >Contact us</h3>
<div class="contactfowrap">
<div class="alert alert-success" id="contactus_div_success" style="display:none"></div>
<div class="alert alert-danger" id="contactus_div_danger" style="display:none">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
</div>
<div id="errors"></div>
<!--@include('includes.form-error')
@include('includes.form-success')--> 
<form id="contactform">
    <div class="form-group">
    <i class="fa fa-user" aria-hidden="true"></i>
  <input type="text" name="name" id="contact_name" class="form-control" placeholder="Name" value="{{ old('name') }}" maxlength="40" />
    </div>

    <div class="form-group">
    <i class="fa fa-envelope" aria-hidden="true"></i>
        <input type="email" name="email" id="contact_email" class="form-control" placeholder="Email" value="{{ old('email') }}" maxlength="40"/>
    </div>
    <div class="form-group">
    <i class="fa fa-map-marker" aria-hidden="true"></i>
        <input id="locationselects" name="locationselects" placeholder="Enter Location" type="text" class="form-control" value="{{ old('locationselects') }}" maxlength="60"/>
        <input type="hidden" class="form-control" name="lat" value="" id="us3-lat" value="{{ old('lat') }}" />
        <input type="hidden" class="form-control" name="long" value="" id="us3-lon" value="{{ old('long') }}" />
    </div>
    <div class="form-group">
    <i class="fa fa-phone" aria-hidden="true"></i>
        <input type="tel" name="phone" id="contact_phone" class="form-control" placeholder="Phone number" onkeypress="return isNumber(event)" value="{{ old('phone') }}" maxlength="10"/>
    </div>
    <div class="form-group">
    <i class="fa fa-list" aria-hidden="true"></i>
        <textarea name="message" id="contact_message" class="form-control" placeholder="Message">{{ old('message') }}</textarea>
    </div>
    <div class="form-group cap-group">
        <input type="text" class="form-control" placeholder="Captcha" name="captcha" id="contact_captchasss">
         <img id="codeimg" src="{{url('assets/images')}}/capcha_code.png">
                                            <span style="cursor: pointer;margin-left:10px" class="refresh_code"><i class="fa fa-refresh fa-2x" style="margin-top: 10px;"></i></span>
    </div>
    <button class="btn legal-q-button contact-home-btn" id="ajaxSubmit" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading">Submit</button>
    <!-- <button type='submit' class="btn btn-primary btm-sm pull-right" value="Оформить договор" data-loading-text="<i class='fa fa-spinner fa-spin '></i> @Localization.Uploading">Оформить договор</button> -->

</form>
</div>
</div>

</div>
</div>
</section>

<!--lwyerind-sec ends-->

<span id="siteseal">
    <script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=THgxChxYP3v78jnuP70khppQrlBQY4rgOGDTx31KTfuLVTvN2qUJviRYWjFX"></script>
</span>

<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script type="text/javascript">
    $('.legal-news-content').each(function(){
        $(this).text($(this).text().substring(0,200));
    });
   

    var check2 = '<?php if(isset($_GET['type'])) echo($_GET['type']); else echo false; ?>';
    // alert(check2);
   if (check2 && check2!=0) { 
    $('html, body').animate({
        scrollTop: $("#"+check2).offset().top
    }, 2000);}
       

      $('.refresh_code').click(function () {
            $.get('{{url("lawyer/refresh_code")}}', function(data, status){
                $('#codeimg').attr("src","{{url('assets/images')}}/capcha_code.png?time="+ Math.random());
            });
        })
    $('.selectpicker').data('max-options', 4).selectpicker('refresh');
    
    $(document).ready(function() {

      var last_valid_selection = null;

      $('#catagories_select_for_Query').change(function(event) {

        if ($(this).val().length > 4) {

          $(this).val(last_valid_selection);
        } else {
          last_valid_selection = $(this).val();
        }
      });
    });


    $('body').on('click','#legal-qbtn',function(ev) {
        ev.preventDefault();//alert('hello');
        $(".alert-danger").hide();
        $(".alert-success").hide();
        var question = $('#question').val();
        var categories = $('#catagories_select_for_Query').val();
        // alert(categories);
        // alert(question);
        if (question && categories && jQuery.inArray("", categories) != 0) {
            $("#signInModal").modal('hide');
            $("#registerModal").modal('hide');
            $('.formregister').trigger("reset");
                $('.formlogin').trigger("reset");
            var checkuser = $('#checkuser').val();
            if (checkuser) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                $('#post_div_success').hide();
                $('#post_div_error').hide();

                $.ajax({
                    url: "{{ url('postquestion') }}",
                    method: 'GET',
                    data: {
                        question: $('#question').val(),
                        description: $('#description').val(),
                        categories: $('#catagories_select_for_Query').val(),
                        userid: $('#checkuser').val(),
                    },
                    success: function(result) {
                        console.log(result);
                        $('#post_div_success').show();
                        var userverify = $("#userverify").val();
                        if (userverify == "1") {
                            var success_message = 'Your question is on hold! Please check your given email inbox/spam folder to confirm signup and your question will automatically be submitted<button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>';
                            $('#checkuser').val('');
                            $(".alert-danger").html('');
                        }else{
                            var success_message = 'Your query has been submitted successfully.<button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>';
                            var email = $('.formlogin').find('input[name="email"]').val();
                            var password = $('.formlogin').find('input[name="password"]').val();
                            var alreadylogin = $('#alreadylogin').val();
                            if (alreadylogin=='') {
                                setTimeout(function(){
                                    var login_message = "Your query has been submitted successfully. Please complete your profile.";
                                    localStorage.setItem('login_message',login_message); 
                                    window.location = '/lawsmos/social/login/pro?email='+email+'&salt='+password;
                                },2000);
                            }
                        }
                        $('#post_div_success').html(success_message);

                        $('.filter-option').html('Select Categories');
                        $("#catagories_select_for_Query option").each(function(){
                            $(this).prop("selected", false);
                        });
                        $('ul.dropdown-menu.inner').find('li').removeClass('selected');
                        $('ul.dropdown-menu.inner').find('.glyphicon').remove();

                        $('#regForm')[0].reset();
                        $('html, body').animate({
                            scrollTop: $('#post_div_success').offset().top - 120
                        }, 500);
                        // $('button#autoclick').trigger('click');
                    },
                    error: function(result) {

                        if (result.status === 400 || result.status === 422) {
                            var response = JSON.parse(result.responseText);
                            $.each(response, function(key, val) {

                                
                                $('#post_div_error').show();

                                $('#post_div_error').html(all_values(val,"post_div_error"));
                                $('html, body').animate({
                                    scrollTop: $("#post_div_error").offset().top -120
                                }, 500);
                                // $('#post_form')[0].reset();
                                // $('button#autoclick').trigger('click');
                                //$("#errors").append("<li class='alert alert-danger'>"+all_values(val)+"</li>");   

                            });
                        }
                        //  $('.alert-danger').show();
                        // $('.alert-danger').html(result.failed);

                    }

                });
            }else{
                $("#registerModal").modal('show');
            }
        }else{
            $('#post_div_error').show();
            $('#post_div_error').html('Fields should not be empty<button type="button" class="close" onclick="hide_post_div_error(\'post_div_error\');" aria-label="Close"><span aria-hidden="true">×</span></button>');
            $('html, body').animate({
                scrollTop: $("#post_div_error").offset().top -120
            }, 500);
        }
    });
</script>

<script type="text/javascript">
//     @if(session('key'))
//     $(document).ready(function(){
//    alert("Your account verified successfully.");
// });
    // @endif

    $('#search-maxims').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/viewmaximslaravel') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_c">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_c">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });


    $('#search-BareActs').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/view_bare_acts') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_b">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_b">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });

    $('#search-article').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/view_judgements_laravel') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });
    $('#search-landmark').autocomplete({
        delay: 300,
        minLength: 2,
        source: function(request, response) {
            $.ajax({
                type: "GET",
                url: "{{ url('social/case_listings/view_landmark_judgements_laravel') }}",
                data: {
                    query: request.term
                },
                dataType: 'JSON',
                success: function(msg) {
                    response(msg);
                }
            });
        },
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        create: function() {
            $(this).data('ui-autocomplete')._renderItem = function(ul, item) {
                if (item.type == "user") {
                    return $('<li class="clearfix search-option-user">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                } else if (item.type == "page") {
                    return $('<li class="clearfix search-option-page">')
                        .append('<div class="search-user-info"><a href="' + item.url + '" target="iframe_a">' + item.label + '</a></div>')
                        .appendTo(ul);
                }
            };
        }
    });
</script>

<script>
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselects'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            document.getElementById("us3-lat").value = lat;
            document.getElementById("us3-lon").value = lon;
        });
    });


    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselectsss'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            document.getElementById("us3-latss").value = lat;
            document.getElementById("us3-lonss").value = lon;
        });
    });
    google.maps.event.addDomListener(window, 'load', function() {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselectpostquestion'));
        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();

            console.log(lat);
            console.log(lon);

            document.getElementById("us3-latpostq").value = lat;
            document.getElementById("us3-lonpostq").value = lon;
        });
    });

    $(document).ready(function() {

        /*
              search-BareActs
      search-article
      search-landmark
         
         */


        $("input#search-maxims").keyup(function() {

            $("input#search-BareActs").prop("disabled", true);
            $("input#search-article").prop("disabled", true);
            $("input#search-landmark").prop("disabled", true);

        });
        $("input#search-BareActs").keyup(function() {

            $("input#search-maxims").prop("disabled", true);
            $("input#search-article").prop("disabled", true);
            $("input#search-landmark").prop("disabled", true);

        });
        $("input#search-article").keyup(function() {

            $("input#search-BareActs").prop("disabled", true);
            $("input#search-maxims").prop("disabled", true);
            $("input#search-landmark").prop("disabled", true);

        });
        $("input#search-landmark").keyup(function() {

            $("input#search-BareActs").prop("disabled", true);
            $("input#search-article").prop("disabled", true);
            $("input#search-maxims").prop("disabled", true);

        });



        $('i#refresh').click(function() {


            $.ajax({
                type: 'GET',
                url: '{{ url("refreshcaptcha") }}',
                success: function(data) {


                    $(".captcha span").html(data.captcha);
                }
            });

        });


        $('button#ajaxSubmit').click(function(e) {

            $('.alert').hide();

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var $this = $(this);
            $this.button('loading');
            $.ajax({
                url: "{{ url('contactus') }}",
                method: 'GET',
                data: {
                    name: $('#contact_name').val(),
                    email: $('#contact_email').val(),
                    location: $('#locationselects').val(),
                    phone: $('#contact_phone').val(),
                    message: $('#contact_message').val(),
                    captcha: $('#contact_captchasss').val(),



                },
                success: function(result) {
                    $this.button('reset');
                    $('.refresh_code').trigger('click');
                    $('#contactus_div_success').show();
                    $('#contactus_div_success').html("Form Submitted successfully. ");
                    $('#contactus_div_danger').hide();

                    $('#contactform')[0].reset();
                    $('button#autoclick').trigger('click');

                },
                error: function(result) {
                    $this.button('reset');
                    $('.refresh_code').trigger('click');
                    if (result.status === 400) {
                        var response = JSON.parse(result.responseText);
                        $.each(response, function(key, val) {
                            // console.log(val);
                            //   console.log(all_values(val));
                            $('#contactus_div_danger').show();
                            $('#contactus_div_danger').html(all_values(val,"contactus_div_danger"));
                            $('html, body').animate({
                                scrollTop: $("#contactus_div_danger").offset().top -120
                            }, 500);
                            // window.location.hash = "#contactus_div_danger";
                            $('button#autoclick').trigger('click');
                            //$("#errors").append("<li class='alert alert-danger'>"+all_values(val)+"</li>");   

                        });
                    }
                    //  $('.alert-danger').show();
                    // $('.alert-danger').html(result.failed);

                }

            });
        });
    });
</script>

{{--
        <div class="section-padding testimonial-wrapper mb_50 overlay" style="background-image: url({{asset('assets/images/'.$gs->bgimg)}});">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="section-title pb_50 text-center">
                <h2>{{$lang->hcs}}</h2>

                <div class="section-borders">

                    <span class="black-border"></span>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="owl-carousel t_carousel10">
                @foreach($ads as $ad)
                <div class="single_testimonial text-center">
                    <div class="border_extra mb_50 pos_relative">
                        <div class="author_info">
                            <h4>{{$ad->client}}</h4>
                            <span>{{$ad->designation}}</span>
                            <p class="author_comment color_66">{{$ad->review}}</p>
                        </div>
                    </div>
                    <div class="author_img radius_100p pos_relative"><img src="{{asset('assets/images/'.$ad->photo)}}" class="radius_100p" alt="author"></div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>


--}}

<script>

    var map;
    var geocoder;
    var marker;
    var people = new Array();
    var latlng;
    var infowindow;

    function hidePopup() {
        $("#location_popup").hide();
    }

    $(document).ready(function() {

        ViewCustInGoogleMap();

    });

    function ViewCustInGoogleMap() {
        var stylesArray = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]
        var mapOptions = {
            center: new google.maps.LatLng(<?php if (isset($lat) && isset($lon)) {
                                                echo $lat . "," . $lon;
                                            } else if (isset($loggedinlat) && isset($loggedinlon)) {
                                                echo $loggedinlat . "," . $loggedinlon;
                                            } else {
                                                echo 0, 0;
                                            } ?>), // Coimbatore = (11.0168445, 76.9558321)
            zoom: 13,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: stylesArray
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        // Get data from database. It should be like below format or you can alter it.

        var data = '<?php  if (isset($result)) {
                        echo json_encode($result);
                    }  ?>';

        //console.log(data);
        // var data = '[{"id": 183,"name": "Arjun gupta","username": "Arjun", "LatitudeLongitude": "30.596050759460997,76.84414168413082","MarkerId": "lawyer"},{"id": 183,"name": "SBP housing","username": "Mr Parminder singh", "LatitudeLongitude": "30.5881,76.8476","MarkerId": "lawyer"}]';
        //   var data = '[{ "name": "sunielsethy", "addr": "Chandigarh", "LatitudeLongitude": "30.7333148,76.7794179", "MarkerId": "Customer" },{ "DisplayText": "abcd", "ADDRESS": "Coimbatore-641042", "LatitudeLongitude": "11.0168445,76.9558321", "MarkerId": "Customer"}]';\
        var check = '<?php echo(Auth::user()); ?>';
        if(check){
             data = JSON.stringify(data);
        }
       
       
        people = JSON.parse(data);
        // people = JSON.parse(people);
       console.log(people);
        for (var i = 0; i < people.length; i++) {
            setMarker(people[i]);
        }


    }

    function setMarker(people) {

        geocoder = new google.maps.Geocoder();
        infowindow = new google.maps.InfoWindow();
        if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
            geocoder.geocode({
                'address': people["addr"]
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                    marker = new google.maps.Marker({

                        position: latlng,
                        map: map,
                        draggable: false,
                        html: people["name"],
                        icon: "images/marker/" + people["MarkerId"] + ".png"

                    });
                    //marker.setPosition(latlng);
                    //map.setCenter(latlng);
                    google.maps.event.addListener(marker, 'click', function(event) {
                        infowindow.setContent(this.html);
                        infowindow.setPosition(event.latLng);
                        infowindow.open(map, this);
                    });
                }
                /*
                else {
                    alert(people["name"] + " -- " + people["addr"] + ". This address couldn't be found");
                }
                */
            });
        } else {

            if (people["MarkerId"] == "Lawyer") {

                if (people["photo"] == "" || people["photo"] == null) {

                    var photo = "default-avatar-profile-icon-vector-18942381.jpg";
                } else {

                    var photo = people["photo"];
                }

                if (people["name"] == "" || people["name"] == null) {

                    var name = "";
                } else {
                    name = people["name"];
                }

                if (people["education"] == "" || people["education"] == null) {

                    var education = "";

                } else {


                    education = people["education"];
                }
                if (people["phone"] == "" || people["phone"] == null) {
                    var phone = "";
                } else {

                    phone = people["phone"];
                }

                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false, // cant drag it
                    html: '<div jstcache="2" id ="location_popup" style="min-width: 194px !important;"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"><button  type="reset" id="can_btn_lawyer"  onclick = "hidePopup()" class="btn btn-default pull-right">x</button></div> <div class="address" style="display:inline"> <div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"><img src="<?php echo url("social/"); ?>/uploads/' + photo + '" class="img img-thumbnail"  alt="no image found.." style="float:left;margin: 7px;display:inline-block;padding: 0;width: 72px;height: 72px;border-radius: 50%;"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width" style="display: inline-block;margin-top: 12px;font-weight: 600;text-transform: capitalize;font-size: 14px;" >' + name + '<br></div><div style="font-size:13px;font-weight:300">' + education + '<br></div> <div style="font-size:13px;font-weight:300">' + phone + '<br></div></div> </div>',
                    icon: '{{url("assets/images/blue-dot.png")}}',


                    //http://maps.google.com/mapfiles/ms/icons/blue-dot.png
                    animation: google.maps.Animation.DROP,
                    // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });

            } else {
                if (people["name"] == "" || people["name"] == null) {

                    var name = "";
                } else {
                    name = people["name"];
                }
                if (people["phone"] == "" || people["phone"] == null) {
                    var phone = "";
                } else {

                    phone = people["phone"];
                }

                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false,
                    mapTypeControl: false, // cant drag it
                    html: '<div jstcache="2"  id="location_popup" style="min-width: 160px !important;padding:10px" ><button  type="reset" id="can_btn_name"  onclick = "hidePopup()" class="btn btn-default pull-right">x</button> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" jsinstance="0" style="display:inline-block;margin-top:2px;font-weight:600;text-transform:capitalize;font-size:14px" class="address-line full-width" jsan="7.address-line,7.full-width">' + people["MarkerId"] + '</div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width">' + name + ',' + people["location"] + '<p style="font-size:13px;font-weight:300">' + phone + '</p></div> </div> </div>',
                    icon: '{{url("assets/images/spotlight-poi2.png")}}',
                    animation: google.maps.Animation.DROP,
                    // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });


            }
        }
    }
</script>
<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    //showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script>
    $(document).ready(function() {

        $("select#catagories_select").change(function() {
            var selectedCatagory = $(this).children("option:selected").val();


            $.ajax({
                type: 'get',
                async: true,
                url: '{{URL::to("searcharticlebycat")}}',

                data: {
                    'searchcat': selectedCatagory
                },
                success: function(data) {

                    // alert(data);

                    if (data == "<ul></ul>") {
                        $("div#result").html("<p>No Articles found in this catagory....</p>");
                    } else {

                        $("div#result").html(data);
                    }



                }
            });

        });
        // Rating change
        /*
  $('.ratingbar').on('rating:change', function(event, value, caption) {
    var id = this.id;
        var splitid = id.split('_');
        var lawyerid = splitid[1];
        var seekerid = splitid[2];
        $.ajax({
         
          type: 'GET',
          data: {lawyerid:lawyerid,seekerid:seekerid,rating:value},
          success: function(response){
             $('#averagerating_'+lawyerid).text(response);
          }
        });
  });
 */
    });
    
</script>
<script>

</script>
<script>
    $(document).ready(function() {



        //   $('#can_btn').click(function(){
        //       alert('ppp');
        //       $('#location_popup').hide();
        //   })

    });
$('.ratingbar').rating({
            showCaption: false,
            showClear: false,
            size: 'sm',
            hoverEnabled: false,
            touchstart: false,

        });
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        $("#view-more-draft").click(function() {
            $.ajax({
                type: 'get',
                async: true,
                url: '{{URL::to("getPerformas")}}',
                data: {},
                success: function(data) {
                    $("#view-more-draft-result").html(data);
                    $("#view-more-draft").hide();
                }
            });
        });
        let featured_data = [];
        //featuresd list get
        $.ajax({
            type: 'get',
            async: true,
            url: '{{URL::to("getFeaturedList")}}',
            data: {},
            success: function(data) {
                let obj = JSON.parse(data);
                obj.forEach(function(element) {
                    console.log(element);
                    featured_data += `<li><a href="{{URL::to("lawyer-user-profile/` + element.id + `")}}">
                   ` + element.name + `</a></li>
                  
                   <span id="view-more-draft-result">

                   </span>`;
                });

                $("#featured_list").append(featured_data);
                // $("#view-more-draft").hide();
            }
        });

    
function add_friend(lawyerid,ajaxfrndUrl,token='',action='',result='',parentClass='') { 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        url: ajaxfrndUrl,
        type: 'GET',
        dataType : 'json',
        success: function(msg) {            
            $('#loading').css('display','none');  
            console.log(msg);
            if (msg.success) {
                if (action) {
                    parentClass.find('.alert-success').show();
                    if (result=="Unverified") {
                        parentClass.find('.alert-success').html(msg.message+'Please check your given email inbox/spam folder to confirm signup <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'login_post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>');
                    }else{
                        parentClass.find('.alert-success').html(msg.message+'<button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'login_post_div_success\')" aria-label="Close"><span aria-hidden="true">×</span></button>');
                        if (action=='login') {
                            var email = parentClass.find('input[name="email"]').val();
                            var password = parentClass.find('input[name="password"]').val();
                            var alreadylogin = $('#alreadylogin').val();
                            if (alreadylogin=='') {
                                setTimeout(function(){
                                    var login_message = msg.message+" Please complete your profile.";
                                    localStorage.setItem('login_message',login_message); 
                                    window.location = '/lawsmos/social/login/pro?email='+email+'&salt='+password;
                                },2000);
                            }
                        }
                    }
                }
                $('#friend_button_' + lawyerid).html(msg.message);
                $('#friend_button_' + lawyerid).addClass("disabled");
            }else if (msg.error) {
                if (action) {
                    parentClass.find('.alert-danger').show();
                    parentClass.find('.alert-danger').html(msg.error_msg+'<button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error(\'login_post_div_error\')" aria-label="Close"><span aria-hidden="true">×</span></button> ');
                }
            }
        },error: function(msg){
            console.log(msg);
        }
    })
}
</script>
<script type="text/javascript">
    //   $('#pass_show').click(function(){
    //   var pass_inp = $('#password').val();
    
    //   if(pass_inp.length > 0){
    //       if($('#password').attr('type') == "password"){
    //           $("#password").attr("type", "text");
    //           $("#pass_show").addClass("fa-eye-slash");
    //         }
    //         else{
    //             $("#password").attr("type", "password");
    //             $("#pass_show").removeClass("fa-eye-slash");
    //             $("#pass_show").addClass("fa fa-eye");
                
    //         }
    //   }  
    //   else{
    //     $("#password").attr("type", "password");
    //     $("#pass_show").removeClass("fa-eye-slash");
    //     $("#pass_show").addClass("fa fa-eye");
    //   }
    
    
      // if($('#password').attr('type') == "password"){
      //   $("#password").attr("type", "text");
      //   $("#pass_show").addClass("fa-eye-slash");
      // }else{
      //   $("#password").attr("type", "password");
      //   $("#pass_show").removeClass("fa-eye-slash");
      //   $("#pass_show").addClass("fa fa-eye");
      // }
                    
      
     
    
    });

</script>
@endsection