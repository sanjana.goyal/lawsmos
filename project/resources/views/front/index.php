<style>
    .btn:active,
    .btn:focus {
        background: #a9a3a3 !important;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/js/star-rating.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/css/star-rating.min.css" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="row profile-mp" style="margin-left: 1%;">
    <div class="col-md-12">
        <div class="profile-header profile-banner" style="background: url(<?php echo base_url() . $this->settings->info->upload_path_relative . "/" . $user->profile_header ?>) center center; background-size: cover;">
            <div class="profile-header-avatar">
                <img src="<?php echo base_url() . $this->settings->info->upload_path_relative . "/" . $user->avatar ?>">
            </div>
            <div class="profile-header-name">
                <?php
                echo $user->first_name . $user->last_name;
                if ($user->verified) :
                ?>
                    <img src="<?php echo base_url() ?>images/verified_badge.png" width="14" data-placement="top" data-toggle="tooltip" title="<?php echo lang("ctn_695") ?>">
                <?php endif; ?>
            </div>
        </div>
        <div class="profile-header-bar clearfix">
            <ul>
                <li target="_blank" class="active"><a href="<?php echo site_url("profile/" . $user->username) ?>"><?php echo lang("ctn_880") ?></a></li>
                <li><a href="javascript:void(0)">Resume</a></li>
                <li><a target="_blank" href="<?php echo site_url("profile/albums/" . $user->ID) ?>"><?php echo lang("ctn_483") ?></a></li>
                <li><a target="_blank" href="<?php echo site_url("profile/friends/" . $user->ID) ?>"><?php echo lang("ctn_493") ?></a></li>
                <li><a href="javascript:void(0)">Cases</a></li>
                <li><a href="javascript:void(0)">Research</a></li>
                <li><a href="javascript:void(0)">Articles</a></li>
                <li><a href="javascript:void(0)">Headnotes</a></li>
                <li><a href="javascript:void(0)">Q&A</a></li>
            </ul>
            <div class="pull-right profile-friend-box">
                <button type="button" class="btn btn-default btn-sm flag-profile" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><span class="glyphicon glyphicon-flag"></span></button>
            </div>
            <?php
            if ($this->user->loggedin && $this->user->info->ID == $user->ID && $page != "view_as_public") {
            ?>
                <div class="pull-right profile-friend-box">
                    <a type="button" class="btn btn-default btn-sm" href="<?php echo site_url("profile/" . $user->username . "?page=view_as_public") ?>" style="padding-bottom:6px;" target="_blank">View as Public</a>
                </div>
            <?php
            }
            ?>

        </div>
        <div class="row separator">
            <div class="col-md-4 profile-title">
                <?php
                if ($this->user->loggedin) :
                    if ($user->ID != $this->user->info->ID || $page == "view_as_public") :
                ?>
                        <div class="page-block half-separator rank-p">
                            <div class="page-block-tidbit">
                                <a class="btn btn-primary" href="tel: <?php echo $user->phone; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $user->phone; ?>">Call</a>
                                <a class="btn btn-primary" href="<?php echo base_url("messages/") ?>">Message</a>
                                <?php if ($friend_flag) : ?>
                                    <button type="button" class="btn btn-success btn-sm" id="friend_button_<?php echo $user->ID ?>"><span class="glyphicon glyphicon-ok"></span> <?php echo lang("ctn_493") ?></button>
                                <?php else : ?>
                                    <?php if ($request_flag) : ?>
                                        <button type="button" class="btn btn-success btn-sm disabled" id="friend_button_<?php echo $user->ID ?>">Connection request sent</button>
                                    <?php else : ?>
                                        <?php if (!$user->allow_friends) : ?>
                                            <button type="button" class="btn btn-success btn-sm" onclick="add_friend(<?php echo $user->ID ?>)" id="friend_button_<?php echo $user->ID ?>">Add connection</button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php
                    endif;
                else :
                    ?>
                    <div class="page-block half-separator rank-p">
                        <div class="page-block-tidbit">
                            <a class="btn btn-primary" href="<?php echo base_url("login/") ?>" data-toggle="tooltip" data-placement="bottom" title="Please login first">Call</a>
                            <a class="btn btn-primary" href="<?php echo base_url("login/") ?>" data-toggle="tooltip" data-placement="bottom" title="Please login first">Message</a>
                            <a class="btn btn-primary" href="<?php echo base_url("login/") ?>" data-toggle="tooltip" data-placement="bottom" title="Please login first">Add connection</a>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 0) :
                ?>
                    <div class="page-block half-separator rank-u">
                        <div class="page-block-title">
                            <span class="glyphicon glyphicon-edit"></span>
                            <a href="javascript:void(0);"><?php echo $user->first_name . " " . $user->last_name; ?> Rating and Review</a>
                        </div>
                        <div class="page-block-tidbit">
                            <input value="<?php if (!empty($get_rating_value)) {
                                                echo $get_rating_value;
                                            } ?>" class="ratingbar" data-min="0" data-max="5" data-step="1">
                            <p><b>Average rating</b> : <?php if (!empty($get_rating_value)) {
                                                            echo $get_rating_value;
                                                        }; ?> out of 5 </p>
                            <p><b>Total rating</b> : <?php if (!empty($total_rating_count)) {
                                                            echo $total_rating_count;
                                                        }; ?></p>
                            <p><b>Recent review</b> : <?php if (!empty($recent_best_review)) {
                                                            echo $recent_best_review['comment'];
                                                        }; ?></p>
                            <p><a href="javascript:void(0)" style="text-decoration: underline;">Click here to read all reviews</a></p>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 0) :
                ?>
                    <div class="page-block half-separator rank-p">
                        <div class="page-block-title">
                            <span class="glyphicon glyphicon-bookmark"></span> <a href="javascript:void(0);">Lawsmos ranking</a>
                        </div>
                        <div class="page-block-tidbit">
                            <ul>
                                <li><a href="javascript:void(0);"> <b>#<?php echo $cityrank; ?></b> in <?php echo $user->city; ?></a> </li>
                                <li><a href="javascript:void(0);"> <b>#<?php echo $staterank; ?></b> in <?php echo $user->state; ?></a></li>
                                <li><a href="javascript:void(0);"> <b>#<?php echo $countryrank; ?></b> in <?php echo $user->country; ?></a></li>
                                <li><a href="javascript:void(0);"><b>Total Score :</b><?php echo $user->total_points; ?> </a></li>
                            </ul>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 0) :
                ?>
                    <div class="page-block half-separator rank-b">
                        <div class="page-block-title">
                            Practice highlights
                        </div>
                        <div class="page-block-tidbit">
                            <ul>
                                <li>
                                    <b>City :</b>
                                    <?php echo !empty($practice_info['practice_city']) ? $practice_info['practice_city'] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Area :</b>
                                    <?php echo !empty($practice_info['practice_area']) ? $practice_info['practice_area'] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Courts :</b>
                                    <?php echo !empty($practice_info['practice_courts']) ? $practice_info['practice_courts'] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Years of practice :</b>
                                    <?php echo ($practice_info['practice_years'] >= 0) ? $practice_info['practice_years'] : 'N/A' ?> years
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 0) :
                ?>
                    <div class="page-block half-separator rank-ac">
                        <div class="page-block-title">
                            <span class="glyphicon glyphicon-list-alt"></span> <a href="javascript:void(0);">Book appointment</a>
                        </div>
                        <div class="page-block-tidbit">
                            <?php
                            if (isset($this->user->info->ID)) {
                                if ($user->ID != $this->user->info->ID) {
                                    if ($user->profile_identification != $this->user->info->profile_identification) {
                            ?>
                                        <div class="page-block">
                                            <div class="align-center">
                                                <div class="project-info project-block align-center">
                                                    <a href="<?php echo site_url('home/appointment'); ?>/<?php echo $user->ID; ?>">Request An Appointment </a>
                                                </div>
                                                <div class="project-info project-block align-center">
                                                </div>
                                                <div class="project-info project-block align-center">
                                                </div>
                                            </div>
                                        </div>
                                <?php  }
                                }
                            } else { ?>
                                <div class="page-block">
                                    <div class="align-center">
                                        <div class="project-info project-block align-center">
                                            <a href="<?php echo site_url('home/appointment'); ?>/<?php echo $user->ID; ?>">Request An Appointment</a>
                                        </div>
                                        <div class="project-info project-block align-center">
                                        </div>
                                        <div class="project-info project-block align-center">
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 0) :
                    $location = explode(',', $user->city);
                ?>
                    <div class="page-block half-separator rank-p">
                        <div class="page-block-title">
                            <span class="glyphicon glyphicon-bookmark"></span> <a href="javascript:void(0);">Office address</a>
                        </div>
                        <div class="page-block-tidbit">
                            <p><?php echo $user->address_1 . ',<br>' . $user->city; ?></p>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 3 || $user->profile_identification == 2) :
                ?>
                    <div class="page-block half-separator">
                        <div class="page-block-title">
                            Basic Info
                        </div>
                        <div class="page-block-tidbit">
                            Date of Birth: <?php echo date('d F, Y', strtotime($core_profile['date_of_birth'])); ?>
                        </div>
                    </div>
                    <div class="page-block half-separator">
                        <div class="page-block-title">
                            Educational Details
                        </div>
                        <div class="page-block-tidbit">
                            <div class="page-block-title" style="font-size: 14px;border-bottom:0">School & High School:</div>
                            <div><?php echo "<b>10th:</b> " . $core_profile['school_10'] . ", " . $core_profile['board_10'] . " passed in " . $core_profile['year_pass_10']; ?></div>
                            <div><?php echo "<b>12th</b>: " . $core_profile['school_12'] . ", " . $core_profile['board_12'] . " passed in " . $core_profile['year_pass_12']; ?>
                            </div>
                            <div class="page-block-title" style="font-size: 14px;border-bottom:0;margin-top: 20px;">College/ Degree:</div>
                            <div>
                                <?php
                                // var_dump(json_decode($core_profile['degree_array']));
                                if ($core_profile['degree_array']) :
                                    $arr_count = 1;
                                    foreach (json_decode($core_profile['degree_array']) as $degreekey => $degreevalue) :
                                        echo '<b>' . $arr_count . '.</b> Passed ' . strtoupper($degreevalue) . ' in ' . json_decode($core_profile['yearend_array'])[$degreekey] . ' from ' . strtoupper(json_decode($core_profile['college_array'])[$degreekey]) . ', ' . strtoupper(json_decode($core_profile['university_array'])[$degreekey]) . ' University';
                                        $arr_count++;
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="page-block half-separator">
                        <div class="page-block-title">
                            Training and Professional Experience
                        </div>
                        <div class="page-block-tidbit">
                            <ul>
                                <li>
                                    <b>Organization Name :</b>
                                    <?php echo !empty($core_profile['training_name_of_organization']) ? $core_profile['training_name_of_organization'] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Training State :</b>
                                    <?php echo !empty($core_profile['training_state']) ? getStateName(json_decode($core_profile['training_state'])[0]) : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Training City :</b>
                                    <?php echo !empty($core_profile['training_city']) ? getCityName(json_decode($core_profile['training_city'])[0]) : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Contact Person :</b>
                                    <?php echo !empty($core_profile['contact_person_designation']) ? json_decode($core_profile['contact_person_designation'])[0] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Contact Person Designation :</b>
                                    <?php echo !empty($core_profile['contact_person_name']) ? json_decode($core_profile['contact_person_name'])[0] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Contact Person Phone Number :</b>
                                    <?php echo !empty($core_profile['contact_person_phone_no']) ? json_decode($core_profile['contact_person_phone_no'])[0] : 'N/A' ?>
                                </li>
                                <li>
                                    <b>Contact Person Email :</b>
                                    <?php echo !empty($core_profile['contact_person_email_address']) ? json_decode($core_profile['contact_person_email_address'])[0] : 'N/A' ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php
                endif;
                if ($user->profile_identification == 1) :
                ?>
                    <div class="page-block half-separator">
                        <div class="page-block-title">
                            Basic Info
                        </div>
                        <div class="page-block-tidbit">
                            <div><b>Date of Birth :</b> <?php echo date('d F, Y', strtotime($core_profile['date_of_birth'])); ?></div>
                            <div><b>Address :</b> <?php echo $core_profile['address']; ?></div>
                            <div><b>City :</b> <?php echo getCityName($core_profile['city']); ?></div>
                            <div><b>State :</b> <?php echo getStateName($core_profile['state']); ?></div>
                            <div><b>Pincode :</b> <?php echo $core_profile['pincode']; ?></div>
                        </div>
                    </div>
                    <?php
                    if ($core_profile['seeker_type'] == 1) :
                    ?>
                        <div class="page-block half-separator">
                            <div class="page-block-title">
                                Education & Financial Information
                            </div>
                            <div class="page-block-tidbit">
                                <div>
                                    <?php
                                    if ($core_profile['education_level'] != 0) :
                                        echo "<b>Education Level :</b>";
                                        echo ($core_profile['education_level'] == 1) ? "Dropout" : "";
                                        echo ($core_profile['education_level'] == 2) ? "High School" : "";
                                        echo ($core_profile['education_level'] == 3) ? "Graduate" : "";
                                        echo ($core_profile['education_level'] == 4) ? "Post Graduate" : "";
                                        echo ($core_profile['education_level'] == 5) ? "Doctorate" : "";
                                    endif;
                                    ?>
                                </div>
                                <div><b>Degree :</b> <?php echo $core_profile['degree']; ?></div>
                                <div><b>Occupation :</b> <?php echo $core_profile['occupation']; ?></div>
                                <div><b>Annual Income :</b> <?php echo $core_profile['annual_income']; ?></div>
                            </div>
                        </div>
                    <?php
                    endif;
                    if ($core_profile['seeker_type'] == 2) :
                    ?>
                        <div class="page-block half-separator">
                            <div class="page-block-title">
                                About Organization
                            </div>
                            <div class="page-block-tidbit">
                                <div><b>Organization :</b> <?php echo $core_profile['organization_name']; ?></div>
                                <div><b>Organization Type :</b>
                                    <?php
                                    echo ($core_profile['organization_type'] == 1) ? "Public Ltd. Company" : "";
                                    echo ($core_profile['organization_type'] == 2) ? "Private Ltd. Company" : "";
                                    echo ($core_profile['organization_type'] == 3) ? "State Enterprise" : "";
                                    echo ($core_profile['organization_type'] == 4) ? "LLP" : "";
                                    echo ($core_profile['organization_type'] == 5) ? "Partnership Firm" : "";
                                    echo ($core_profile['organization_type'] == 6) ? "Proprietorship Firm" : "";
                                    echo ($core_profile['organization_type'] == 7) ? "NGO" : "";
                                    echo ($core_profile['organization_type'] == 8) ? "Trust" : "";
                                    echo ($core_profile['organization_type'] == 9) ? "Welfare Society" : "";
                                    ?>
                                </div>
                                <div><b>Head Office Location :</b> <?php echo $core_profile['office_location']; ?></div>
                                <div><b>Category :</b> <?php echo getCategoryName($core_profile['category']); ?></div>
                                <div><b>Number of Employees :</b> <?php echo $core_profile['no_of_employees']; ?></div>
                            </div>
                        </div>
                    <?php
                    endif;
                endif;
                if ($this->settings->info->enable_google_ads_pages) :
                    ?>
                    <div class="page-block half-separator cbh">
                        <?php include(APPPATH . "/views/home/google_ads.php"); ?>
                    </div>
                <?php
                endif;
                if ($this->settings->info->enable_rotation_ads_pages) :
                    include(APPPATH . "/views/home/rotation_ads.php");
                endif;
                ?>
            </div>
            <div class="col-md-8">
                <?php
                // if ($this->user->loggedin) :
                if ($this->user->loggedin && $this->user->info->ID == $user->ID && $page != "view_as_public") :
                    if (($user->post_profile && ($this->user->info->ID == $user->ID || $friend_flag)) || !$user->post_profile) :
                        $editor_placeholder = lang("ctn_579") . " " . $user->first_name . "'s " . lang("ctn_614") . " ...";
                        $target_type = "user_profile";
                        $targetid = $user->ID;
                        include(APPPATH . "views/feed/editor.php");
                    endif;
                endif;
                ?>
                <div id="home_posts">
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($this->user->loggedin) : ?>
    <?php echo form_open(site_url("profile/report_profile/" . $user->ID)) ?>
    <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-flag"></span> Report <?php echo $user->first_name ?> <?php echo $user->last_name ?></h4>
                </div>
                <div class="modal-body ui-front form-horizontal">
                    <div class="form-group">
                        <label for="p-in" class="col-md-4 label-heading">Reason for Reporting</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="reason">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
                    <input type="submit" class="btn btn-primary" value="Report">
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php echo form_close() ?>

<script>
    var profile_identification = '<?php echo $user->profile_identification; ?>';
    if (profile_identification == 0) {
        $(document).ready(function() {
            $('.ratingbar').rating({
                showCaption: true,
                showClear: false,
                size: 'sm',
                hoverEnabled: false,
                touchstart: false,
                displayOnly: true,
            });
        });
    }

    $(document).ready(function() {
        setTimeout(function() {
            $('.global_msg').fadeOut(1000);
        }, 8000);
        $.ajax({
            url: global_base_url + 'feed/load_user_posts/<?= $user->ID ?>',
            type: 'GET',
            data: {},
            success: function(msg) {
                $('#home_posts').html(msg);
                $('#home_posts').jscroll({
                    nextSelector: '.load_next'
                });
            }
        })
    });
    $('form input').keydown(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
</script>