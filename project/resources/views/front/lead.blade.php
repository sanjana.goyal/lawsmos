@extends('layouts.front')
@section('styles')

<style>
#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  width: 70%;
  min-width: 300px;
}
.service-steps .form-check {
    float: left;
    padding: 19px;
}

input.form-check-input {
    width: auto;
    font-size: 17px;
    font-family: 'Roboto', sans-serif;
    border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
   cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

.service_requests #regForm {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

.service_requests h1 {
  text-align: center;  
}

.service_requests input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
.service_requests input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.service_requests .tab {
  display: none;
}

.service_requests button {
  background-color: #4CAF50;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  cursor: pointer;
}

.service_requests button:hover {
  opacity: 0.8;
}

.service_requests #prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.service_requests .step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.service_requests .step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.service_requests .step.finish {
  background-color: #4CAF50;
}
.service_requests ul, ol {
    margin: 0;
    padding: 0;
    list-style: none;
}
 
.service_requests tab li {
    padding: 10px 15px;
    /* margin: 8px 0; */
    border-radius: 3px;
    /* cursor: pointer; */
    color: #333333;
    border: 1px solid #eee;
    position: relative;
    background-color: #fff;
    width: 100%;
    transition: all 500ms ease;
    min-height: 50px;
}




.service_requests .step-1-content div {
  clear: both;
  overflow: hidden;
}

.service_requests .fstep-1-content label {
  width: 100%;
  border-radius: 3px;
  border: 1px solid #D1D3D4;
  font-weight: normal;
}

.service_requests .step-1-content input[type="radio"]:empty,
.fstep-1-content input[type="checkbox"]:empty {
  display: none;
}

.service_requests .step-1-content input[type="radio"]:empty ~ label,
.service_requests .step-1-content input[type="checkbox"]:empty ~ label {
  position: relative;
  line-height: 2.5em;
  text-indent: 3.25em;
  margin-top: 2em;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

.service_requests .step-1-content input[type="radio"]:empty ~ label:before,
.service_requests .step-1-content input[type="checkbox"]:empty ~ label:before {
  position: absolute;
  display: block;
  top: 0;
  bottom: 0;
  left: 0;
  content: '';
  width: 2.5em;
  background: #D1D3D4;
  border-radius: 3px 0 0 3px;
}

.service_requests .step-1-content input[type="radio"]:hover:not(:checked) ~ label,
.service_requests .step-1-content input[type="checkbox"]:hover:not(:checked) ~ label {
  color: #888;
}

.service_requests .step-1-content input[type="radio"]:hover:not(:checked) ~ label:before,
.service_requests .step-1-content input[type="checkbox"]:hover:not(:checked) ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #C2C2C2;
}

.service_requests .step-1-content input[type="radio"]:checked ~ label,
.service_requests .step-1-content input[type="checkbox"]:checked ~ label {
  color: #777;
}

.service_requests .step-1-content input[type="radio"]:checked ~ label:before,
.service_requests .step-1-content input[type="checkbox"]:checked ~ label:before {
  content: '\2714';
  text-indent: .9em;
  color: #333;
  background-color: #ccc;
}

.service_requests .step-1-content input[type="radio"]:focus ~ label:before,
.step-1-content input[type="checkbox"]:focus ~ label:before {
  box-shadow: 0 0 0 3px #999;
}

.service_requests .step-1-content-default input[type="radio"]:checked ~ label:before,
.service_requests .step-1-content-default input[type="checkbox"]:checked ~ label:before {
  color: #333;
  background-color: #ccc;
}

.service_requests .step-1-content-primary input[type="radio"]:checked ~ label:before,
.step-1-content-primary input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #337ab7;
}

.service_requests .step-1-content-success input[type="radio"]:checked ~ label:before,
.service_requests .step-1-content-success input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5cb85c;
}

.service_requests .step-1-content-danger input[type="radio"]:checked ~ label:before,
.service_requests .step-1-content-danger input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #d9534f;
}

.service_requests .step-1-content-warning input[type="radio"]:checked ~ label:before,
.service_requests .step-1-content-warning input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #f0ad4e;
}

.service_requests .step-1-content-info input[type="radio"]:checked ~ label:before,
.service_requests .step-1-content-info input[type="checkbox"]:checked ~ label:before {
  color: #fff;
  background-color: #5bc0de;
}

.service_requests ul.time-details li {
    float: left;
    padding: 31px;
}

.service_requests h6.recieve-call-heading {
    padding: 39px 0px 2px 2px;
    font-size: 27px;
}

</style>
@endsection
@section('content')

<form id="regForm" class="service-steps" action="{{route('front.addlead')}}" method="get">

<input type="hidden" name="issue" value="{{$categorydetails->id}}_{{$categorydetails->cat_name}}_{{$categorydetails->price}}"/>

<!-- One "tab" for each step in the form: -->

@if(!$subcategory->isEmpty())

<div class="tab">	
    <h2 class="text-center">What do you need help with?</h2>	
	@foreach($subcategory as $cat)
	 @if($loop->first)
		 
	<div class="form-check">
	  <input class="form-check-input" type="radio" value="{{$cat->id}}_{{$cat->cat_name}}" name="subissue" id="" checked />
	  <label class="form-check-label" for="exampleRadios1">
		{{$cat->cat_name}}
	  </label>
	</div> 
	@else	 
	 <div class="form-check">
	  <input class="form-check-input" type="radio" value="{{$cat->id}}_{{$cat->cat_name}}" name="subissue" id="" checked />
	  <label class="form-check-label" for="exampleRadios1">
		{{$cat->cat_name}}
	  </label>
	</div>
	@endif
	@endforeach
</div>
@endif 

<div class="tab">
 <h2 class="text-center">What type of legal assistance do you require?</h2>
  
    <div class="form-check">
	   <input type="radio" class="form-check-input" value="Legal Consultation" name="action_list" id="subchild1"/>
	   <label class="form-check-label" for="subchild1">Legal Consultation</label>
	</div>
 
  <div class="form-check">
	   <input type="radio" class="form-check-input" value="Legal Representation"  name="action_list" id="subchild2" checked  />
       <label class="form-check-label" for="subchild2">Legal Representation</label>
  </div>
 <div class="form-check">
    	<input type="radio" class="form-check-input"  value="Petition Filing" name="action_list" id="subchild3"  />
        <label class="form-check-label" for="subchild3">Petition Filing</label>
  </div>
<div class="form-check">
  	  <input type="radio" class="form-check-input" value="Document Preparation"  name="action_list" id="subchild4"  />
      <label class="form-check-label" for="subchild4">Document Preparation</label>
  </div>
  <div class="form-check">
  	 <input type="radio" class="form-check-input" value="Legal Notice"  name="action_list" id="subchild5"  />
       <label class="form-check-label" for="subchild5">Legal Notice</label>
  </div>
 <div class="form-check">
  	   <input type="radio"  class="form-check-input" value="Alternate Dispute Resolution"  name="action_list" id="subchild6"  />
       <label class="form-check-label" for="subchild6">Alternate Dispute Resolution</label>
  </div>
 <div class="form-check">
  	  <input type="radio" class="form-check-input" value="Arbitration"  name="action_list" id="subchild7"  />
       <label class="form-check-label" for="subchild7">Arbitration</label>
 </div>
  <div class="form-check">
		<input type="radio" class="form-check-input" value="Document Verification"  name="action_list" id="subchild8"  />
		<label class="form-check-label" for="subchild8">Document Verification</label>
  </div>
</div>

<div class="tab">
	<h2 class="text-center">Share your contact details to proceed</h2>
<div class="col-md-6">
 <div class="form-group">
  	<label for="phone">Phone</label>
    <input placeholder="Phone" class="form-control"  name="phone">
</div>
</div>
<div class="col-md-6">
 <div class="form-group">
  	<label for="name">Name</label>
    <input placeholder="Name" class="form-control" name="name">
 </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
  		<label for="email">Email</label>
		<input placeholder="Email"  class="form-control"  name="email" type="email">
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
  	 <label for="location">Location</label>	
     <input id="location" type="text" class="form-control"  name="location" placeholder="Enter a location" >
     <input type="hidden" class="form-control" name="lat"  value=""  id="us3-lataa" />
	 <input type="hidden" class="form-control" name="long"  value="" id="us3-lonaa" />
  </div>
</div>
<div class="col-md-12">
  <div class="form-group">
  	 <label for="shortdescription">Short description</label>
	 <textarea  name="shortdescription" class="form-control" required></textarea>
 </div>
</div>
  <div class="col-md-12">
      <h6> I would like to recieve emails</h6>
      <div class="form-check">
	   <input type="radio" class="form-check-input" value="Yes" name="emailtime" checked />
	   <label class="form-check-label rchjemal" for="">Yes</label>
	  </div>
	  
	  <div class="form-check">
	   <input type="radio" class="form-check-input" value="no" name="emailtime" checked />
	   <label class="form-check-label rchjemal" for="">No</label>
	  </div>
  </div>

  
  
  
    <h6 class="recieve-call-heading">I would like to recieve calls on</h6>   
    <ul class="time-details">
		<li><input type="radio" value="Any Time" name="calltime"  checked  />Any Time</li>
		<li><input type="radio" value="12PM to 3PM" name="calltime"  />12PM to 3PM</li>
		<li><input type="radio" value="3PM to 6PM" name="calltime"  />3PM to 6PM</li>
		<li><input type="radio" value="6PM to 9PM" name="calltime"   />6PM to 9PM</li>
    </ul>
  </p>
</div>

<div class="col-md-12 text-center">
   <button  type="button" class="legal-q-button legal-btn" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
   <button type="button" class="legal-q-button legal-btn" id="nextBtn" onclick="nextPrev(1)">Next</button>
</div>

<!-- Circles which indicates the steps of the form: -->
<div class="col-md-12 text-center" style="margin-top: 40px;">
@if(!$subcategory->isEmpty())
  <span class="step"></span>
@endif 
  <span class="step"></span>
  <span class="step"></span>
 
</div>

</form>
 


<script type="text/javascript">
          google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('location'));
        google.maps.event.addListener(places, 'place_changed', function () {
			  var place = places.getPlace();
                   // Do watever wif teh value!
                   var lat=place.geometry.location.lat();
                   var lon=place.geometry.location.lng();
				document.getElementById("us3-lataa").value = lat;
				document.getElementById("us3-lonaa").value = lon;
        });
    });
   
       </script>
<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}
function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>
@endsection



