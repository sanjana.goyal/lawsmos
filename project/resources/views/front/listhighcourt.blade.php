@extends('layouts.front')
@section('content')
<br>
<br>
<div class="container">
<div class="courtlist_outer">

			<div class="form-group" >

<section class="hight-judgments">
<div class="container"><a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<h1>High Court Judgements </h1>
					<div class="form-group" >
							<input type="text" class="form-controller form-control" id="search" name="search" placeholder="Search..">

					</div>
				<div id="result"></div>
	
	<div class="list">

		<ul class="bareacts-list courtlist">

		<ul class="bareacts-list">

			
			@foreach($highcourt as $hcgtrt)
					
				<li><a href="{{ url('detail/'.$hcgtrt->id) }}">{{ $hcgtrt->heading }}</a></li>

				
			@endforeach	
</ul>
</div>
<?php echo $highcourt->links(); ?>
</div>

</div>

</section>
</div>
</div>
</div>
<script type="text/javascript">

$('#search').on('keyup',function(){
	

var value=$(this).val();
if (!this.value) {
       $('.list').show();
       $('#result').hide();
    }
else{
$.ajax({
type : 'get',
async: true,
url : '{{URL::to("searchbar_jugements")}}',
data:{'search': value},
success:function(data){
	console.log(data);
$("#result").html(data);
 
$('#result').show();

$('.list').hide();

}
});

}


});

$('#search').on('keydown',function(){
	
	var value=$(this).val();
if(value=="" || value==null)
{
	$('.list').show();
}
	});
</script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script type="text/javascript">
$('li').filter(function(){
    return $.trim($(this).html()) == '';
}).hide()
</script>
@endsection
