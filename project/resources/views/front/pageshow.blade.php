@extends('layouts.front')
@section('content')


@foreach($paggesss as $paggesss)
<div class="section-padding blog-post-wrapper wow fadeInUp" style="visibility: visible; ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
                        <div class="section-title">
                            <h2 style="">{{$paggesss->meta_title}} </h2>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                       
                       <div class="row">
                           <div class="col-md-8">
                               <div class="entry-content">
									<p><?php  echo html_entity_decode($paggesss->content) ?></p>
                                </div>
                           </div>
                          
                       </div>
                                
                    </div>
                </div>
                
                
                
                
                
              
				
           
              
              
            </div>
        </div>

@endforeach

@endsection
