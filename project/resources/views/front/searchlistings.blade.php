@extends('layouts.front')
@section('content')


<div class="container">
<div class="row">
	<div class="col-md-12 backend-text">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<h1 class="text-left">High Court Data:</h1>
<ul>
@foreach($highcourts as $highcourtd)
	
	<li><a href="{{url('detail',$highcourtd->id)}}"> {{ $highcourtd->heading }}</li>	</a>
	 
@endforeach

</ul>
	</div>
</div>
</div>



@endsection
