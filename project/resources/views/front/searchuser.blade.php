@extends('layouts.front')
@section('styles')

<style type="text/css">
  /* Hide the list on focus of the input field */
  datalist {
    display: none;
  }

  /* specifically hide the arrow on focus */
  input::-webkit-calendar-picker-indicator {
    display: none;
  }

  .progress-pp {
    display: none;
  }
  .name-err{
    color: red;
    padding: 2px;
    display: none;
  }
  .error_class{
    color: red;
    padding: 2px;
  }


  /* steps form  */


 
</style>



{{--

    <script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7IZt-36CgqSGDFK8pChUdQXFyKIhpMBY&sensor=true" type="text/javascript"></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgriKllmz3R9e7ORquSKDyASj_DP4csuo&libraries=places"></script>

--}}

@endsection
@section('content')



<div id="map-canvas" style="width: 100%; height: 500px; display:block;">

</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">

      <div class="hero-form">
        <div class="hero-form-wrapper">
          <form action="{{route('user.search')}}" method="GET">

            <div class="row">
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                  <div class="input-group">

                    {{-- <input list="cities" type="search" name="search" class="form-control" placeholder="Enter City" value="{{$search}}"> --}}



                    <input id="locationselect" maxlength="100" name="search" placeholder="Enter Your Location" type="text" class="form-control" value="<?php if (!empty($search)) : echo $search;
                                                                                                                                        endif; ?>">
                    <input type="hidden" class="form-control" name="lat" value="<?php if (!empty($lat)) : echo $lat;
                                                                                endif; ?>" id="us3-lat" />
                    <input type="hidden" class="form-control" name="long" value="<?php if (!empty($longitude)) : echo $longitude;
                                                                                  endif; ?>" id="us3-lon" />



                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="form-group">
                  <div class="input-group">

                    <select name="group" id="blood_grp" class="form-control" required="">
                      <option value="">{{$lang->sbg}}</option>
                      @foreach($cats as $cat)
                      <option value="{{$cat->id}}" {{$catt->id == $cat->id ? "selected":""}}>{{$cat->cat_name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="form-group text-center">
                  <input class="btn btn-block hero-btn" name="button" value="Search" type="submit">
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>







<section class="section-padding avail-lawyer-wrapper">
  <div id="flash_message"></div>

  <div class="container-fluid" style="display: flex;">
    <div class="avail-content" style="width:100%">
      <h3>{{count($usersss)}} {{$lang->dni}} {{$catt->cat_name}}</h3>

      <?php $userInfo = array(); ?>


      @foreach($users->chunk(4) as $userChunk)


      <div class="row">
        @foreach($userChunk as $user)

        <?php $userInfo[] = $user; //var_dump($user->category); 
        ?>

        <?php
         $profile = \DB::table('social.coreprofile')->where('userid', $user->id)->first();
        
          $practice_of_interst = \DB::table('social.catagories')->whereIn('cid', explode(',', $profile->unpracticing_fields_of_interest))->get();
           // dd($practice_of_interst);
           $practice_area = '';
           if(count($practice_of_interst)){
            foreach ($practice_of_interst as $value) {
             $array[] = $value->c_name;
           }
           $practice_area = implode(',', $array);
           }else{
            $practice_area = 'N/A';
           }
           

        $topratedreview = \DB::table('social.rating')->where('lawyerid', $user->id)->orderby('rating', 'desc')->first();

        // if ($loggedinuser) {
          $friendrequest = \DB::table('social.user_friend_requests')->where('friendid', $user->id)->where('userid', $loggedinuser)->first();
          $friend = \DB::table('social.user_friends')->where('friendid', $user->id)->where('userid', $loggedinuser)->first();
          $cityDetails = \DB::table('social.city')->where('id', $user->city)->first();
        // }
        ?>

        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="team_common">
            <div class="profile-header-avatar">

              <a href="{{route('front.user.profile',$user->id)}}"><img src="{{ $user->photo ? asset('social/uploads/'.$user->photo):asset('/social/uploads/default.png')}}" alt="member image"></a>
              {{-- <a href="{{route('front.user',$user->id)}}"><img src="{{ $user->photo ? asset('assets/images/'.$user->photo):''}}" alt="member image"></a> --}}
            </div>


            <div class="content">

              <a href="{{route('front.user.profile',$user->id)}}" class="law_n fw_600">{{$user->name}}</a>
              <p>Location: @if(isset($cityDetails->name )){{ $cityDetails->name }} @else N/A @endif </p>
              <!--   <?php //var_dump($user); 
                      ?>  -->
              @if($user->cityrank)
              <div><strong>#{{ $user->cityrank }}</strong> in {{ $user->city }}</div>
              @endif
              <!-- <p>Call Me: {{ $user->phone }}</p> -->
              <input id="ratingbar" value="{{ $user->rating }}" class="rating" data-min="0" data-max="5" data-step="1">
              <p> <strong>Review:</strong> @if($topratedreview != "") {{ $topratedreview->comment }} @else N/A @endif</p>

              <span class="d_block transition_3s"> @if($lang != "") {{$lang->bg}} @else N/A @endif @if($user->category != ""){{$user->category->cat_name}} @else N/A @endif</span>
              <span class="d_block transition_3s">Area of practice:  <?php echo $practice_area != '' ? $practice_area :  'N/A';  ?></span>

              <!--   <p class="lap-pratice"><span>Area of practice: <?php echo $practice_area != '' ? $practice_area :  'N/A';  ?></span> <a href="#" data-toggle="tooltip" title="<?php echo $practice_area != '' ? $practice_area :  'N/A';  ?>">View all</a> -->

                                    </p>
              <!--
                                    <ul class="social_contact pt_10" style="padding-left: 0px;">
                                        @if($user->f_url != null)
                                        <li>
                                        <a href="{{$user->f_url ? $user->f_url:'https://www.facebook.com/'}}" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        @endif
                                        @if($user->t_url != null)
                                        <li>
                                        <a href="{{$user->t_url ? $user->t_url:'https://twitter.com/'}}" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        @endif
                                        @if($user->l_url != null)
                                        <li>
                                        <a href="{{$user->l_url ? $user->l_url:'https://www.linkedin.com/'}}" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        @endif
                                        @if($user->g_url != null)
                                        <li>
                                        <a href="{{$user->g_url ? $user->g_url:'https://plus.google.com/'}}" title="Google-plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        @endif
                                    </ul>
-->
              <div class="vp-wrap vp-flex-area">
                <a class="btn btn-xs" target="_blank" href="{{route('front.user.profile',$user->id)}}">View Profile</a>
                <?php if ($loggedinuser) {
                  if ($friendrequest) { ?>
                    <a href="#" class="btn btn-success btn-sm disabled" id="friend_button_<?php echo $user->id ?>">Connection request sent</a>
                    <a class="btn btn-xs" href="<?php echo url('social/home/appointment'); ?>/<?php echo $user->id; ?>">Book Appointment</a>
                  <?php } else { ?>

                    <a href="javascript:void(0);" class="btn btn-xs" onclick="add_friend('<?php echo $user->id ?>','<?php echo url('social/profile/add_friend/') . '/' . $user->id; ?>')" id="friend_button_<?php echo $user->id ?>">Connect</a>
                    <a class="btn btn-xs" href="<?php echo url('social/home/appointment'); ?>/<?php echo $user->id; ?>">Book Appointment</a>
                  <?php }
                } else { ?>

                  <a href="#" class="btn btn-xs connect-frnd" data-user="{{$user->id}}" data-frnd="1" data-toggle="modal" data-target="#signInModal">Connect</a>
                  <a href="#" class="btn btn-xs connect-frnd" data-user="{{$user->id}}" data-frnd="0" data-toggle="modal" data-target="#signInModal">Book Appointment</a>
                <?php } ?>

              </div>
            </div>

          </div>
        </div>
        @endforeach

      </div>
      @endforeach

    </div>
    <div class="text-center">
      {!! $users->appends(['search' => $search, 'group' =>$catt->id, 'button' =>'Search'])->links() !!} </div>
  </div>
</section>








<section class="law__firm">
  <div id="flash_message"></div>

  <div class="container-fluid" style="display: flex;">
    <div class="avail-content" style="width:100%">
      <h3>{{count($agency)}} Law Firm(s) Found</h3>

      <?php $userInfo = array(); ?>


     @foreach($agencies->chunk(4) as $agencyChunk)


      <div class="row">
         @foreach($agencyChunk as $agency)

       <?php

        $agencyChunk[] = $agency; ?>

       <!--  <?php
        $topratedreview = \DB::table('social.rating')->where('lawyerid', $user->id)->orderby('rating', 'desc')->first();
        if ($loggedinuser) {
          $friendrequest = \DB::table('social.user_friend_requests')->where('friendid', $user->id)->where('userid', $loggedinuser)->first();
          $friend = \DB::table('social.user_friends')->where('friendid', $user->id)->where('userid', $loggedinuser)->first();
        }
        ?> -->


        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="team_common">
            <div class="profile-header-avatar">

              <a href="{{route('front.lawfirm.profile',$agency->ID)}}"><img src="{{ $agency->profile_avatar ? asset('social/uploads/'.$agency->profile_avatar):asset('/social/uploads/default.png')}}" alt="member image"></a>
              {{-- <a href="{{route('front.user',$agency->ID)}}"><img src="{{ $agency->profile_avatar ? asset('assets/images/'.$agency->profile_avatar):''}}" alt="member image"></a> --}}
            </div>


            <div class="content">

              <a href="{{route('front.lawfirm.profile',$agency->ID)}}" class="law_n fw_600">{{$agency->name}}</a>
              <p>Location: {{ $agency->location }} </p>
              <!--   <?php //var_dump($user); 
                      ?>  -->
             <!--  @if($user->cityrank)
              <div><strong>#{{ $user->cityrank }}</strong> in {{ $user->city }}</div>
              @endif -->
              <!-- <p>Call Me: {{ $user->phone }}</p> -->
              <input id="ratingbar" value="{{ $user->rating }}" class="rating" data-min="0" data-max="5" data-step="1">
              <p> <strong>Review:</strong> @if($topratedreview != "") {{ $topratedreview->comment }} @else N/A @endif</p>
              <span class="d_block transition_3s"> @if($lang != "") {{$lang->bg}} @else N/A @endif @if($agency->category != ""){{$agency->category->cat_name}} @else N/A @endif</span>
              <!--
                                    <ul class="social_contact pt_10" style="padding-left: 0px;">
                                        @if($user->f_url != null)
                                        <li>
                                        <a href="{{$user->f_url ? $user->f_url:'https://www.facebook.com/'}}" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        @endif
                                        @if($user->t_url != null)
                                        <li>
                                        <a href="{{$user->t_url ? $user->t_url:'https://twitter.com/'}}" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        @endif
                                        @if($user->l_url != null)
                                        <li>
                                        <a href="{{$user->l_url ? $user->l_url:'https://www.linkedin.com/'}}" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        @endif
                                        @if($user->g_url != null)
                                        <li>
                                        <a href="{{$user->g_url ? $user->g_url:'https://plus.google.com/'}}" title="Google-plus" target="_blank"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        @endif
                                    </ul>
-->
             

          </div>
        </div>
        @endforeach

      </div>
      @endforeach

    </div>
    <div class="text-center">
      {!! $users->appends(['search' => $search, 'group' =>$catt->id, 'button' =>'Search'])->links() !!} </div>
  </div>
 <!--  <div class="container-fluid">
    <div class="avail-content firm_content">
      <h3>{{count($agencies)}} Law Firm(s) Found </h3>

      <?php $agencyChunk = array(); ?>
      @foreach($agencies->chunk(4) as $agencyChunk)


      <div class="row">
        @foreach($agencyChunk as $agency)

        <?php

        $agencyChunk[] = $agency; ?>



        <div class="col-lg-2 col-md-3 col-sm-6">
          <div class="team_common">

            <div class="content">

              <a href="{{route('front.lawfirm.profile',$agency->ID)}}" class="d_inline fw_600">{{$agency->name}}</a>
              {{-- <a href="{{route('front.user',$user->id)}}" class="d_inline fw_600">{{$agency->name}}</a> --}}


            </div>
          </div>
        </div>
        @endforeach

      </div>
      @endforeach
    </div>
    <div class="text-center">
      {!! $users->appends(['search' => $search, 'group' =>$catt->id, 'button' =>'Search'])->links() !!}
    </div>
  </div> -->
</section>

<section class="pop_up_sec">

  <div class="modal my-pop-up modelClass" id="pop__up" role="dialog">

    <div class="modal-dialog">
      <div class="progress-pp">

        <div class="progress progress-striped">
          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" id="first_prog" style="width: 0%">
            <span id="progresscount"> </span>
          </div>

        </div>

      </div>

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>


        <div class="pop-body">

          <form id="regForm" class="service-steps" action="{{route('front.addlead')}}" method="get">

            <input type="hidden" name="issue" id="issue" value="{{$catdetails->id}}_{{$catdetails->cat_name}}_{{$catdetails->price}}" />

            <!-- One "tab" for each step in the form: -->

            <!-- @if(!$subcat->isEmpty())

            <div class="tab">
              <h2 class="text-center">What do you need help with?</h2>
              @foreach($subcat as $cat)
              @if($loop->first)

              <div class="form-check">
                <input class="form-check-input" type="radio" value="{{$cat->id}}_{{$cat->cat_name}}" name="subissue" id="subissue" checked />
                <label class="form-check-label" for="exampleRadios1">
                  {{$cat->cat_name}}
                </label>
              </div>
              @else
              <div class="form-check">
                <input class="form-check-input" type="radio" value="{{$cat->id}}_{{$cat->cat_name}}" name="subissue" id="subissue" checked />
                <label class="form-check-label" for="exampleRadios1">
                  {{$cat->cat_name}}
                </label>
              </div>
              @endif
              @endforeach
            </div>
            @endif -->

            <!-- <div class="tab">
              <h2 class="text-center">What type of legal assistance do you require?</h2>

              <div class="form-check">
                <input type="radio" class="form-check-input" value="Legal Consultation" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild1">Legal Consultation</label>
              </div>

              <div class="form-check">
                <input type="radio" class="form-check-input" value="Legal Representation" name="action_list" id="action_list" checked />
                <label class="form-check-label" for="subchild2">Legal Representation</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" value="Petition Filing" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild3">Petition Filing</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" value="Document Preparation" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild4">Document Preparation</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" value="Legal Notice" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild5">Legal Notice</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" value="Alternate Dispute Resolution" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild6">Alternate Dispute Resolution</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" value="Arbitration" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild7">Arbitration</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" value="Document Verification" name="action_list" id="action_list" />
                <label class="form-check-label" for="subchild8">Document Verification</label>
              </div>
            </div> -->

            <div class="tab tabClass">
              <h2 class="text-center">Share your contact details to proceed</h2>
              @if(!$subcat->isEmpty())
              <div class="col-md-6">
                <div class="form-group">
                  <!-- <label for="subissue">What do you need help with?*</label> -->
                  <select class="form-control"  name="subissue" id="subissue">
                    <option value="">Select what do you need help with?</option>
                    <?php
                    foreach ($subcat as $cat) {
                    ?>
                      <option value="<?php echo $cat->id.'_'.$cat->cat_name; ?>"><?php echo $cat['cat_name'] ?></option>
                    <?php
                    }
                    ?>
                  </select>
                   <div class="subissue-err error_class" style="display:none">Please select any option</div>
                </div>
              </div>
              @endif
              <div class="col-md-6">
                <div class="form-group">
                 <!--  <label for="action_list">What type of legal assistance do you require?*</label> -->
                  <select class="form-control"  name="action_list" id="action_list" required="">
                    <option value="">Select what type of legal assistance do you require?</option>
                    <option value="Legal Consultation">Legal Consultation</option>
                    <option value="Legal Representation">Legal Representation</option>
                    <option value="Petition Filing">Petition Filing</option>
                    <option value="Document Preparation">Document Preparation</option>
                    <option value="Legal Notice">Legal Notice</option>
                    <option value="Alternate Dispute Resolution">Alternate Dispute Resolution</option>
                    <option value="Arbitration">Arbitration</option>
                    <option value="Document Verification">Document Verification</option>
                  </select>
                  <div class="action_list-err error_class" style="display:none">Please select any option</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                 <!--  <label for="phone">Phone Number*</label> -->
                  <input type="tel" name="phone" id="phone" class="form-control" placeholder="Phone Number" onkeypress="return isNumber(event)" maxlength="10"/>
                  <div class="phone-err error_class" style="display:none">Please enter a valid phone number</div>

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <!-- <label for="name">Name*</label> -->
                  <input type="text"  name="name" id="name" class="form-control" placeholder="Name" maxlength="40" />
                  <div class="name-err">Please enter valid name</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                 <!--  <label for="email">Email*</label> -->
                  <input placeholder="Email" class="form-control" name="email" id="email" type="email" maxlength="100">
                  <div class="email-err error_class" style="display:none">Please enter valid email</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <!-- <label for="txtPlaces">Location*</label> -->
                  <input id="location" maxlength="100" type="text" class="form-control" value="Chandigarh, India" name="location" id="location" placeholder="Enter a location">
                  <div class="location-err error_class" style="display:none">Location field is required</div>
                  <input type="hidden" class="form-control" name="lat" value="30.7333" id="us3-lataa" />
                  <input type="hidden" class="form-control" name="long" value="76.7794" id="us3-lonaa" />

                  <!-- <input id="txtPlaces" name="search" placeholder="Enter Your Location" type="text" class="form-control" value=""> -->
                  <!-- <input type="hidden" class="form-control" name="lat"  value=""  id="us3-lat" />
													 <input type="hidden" class="form-control" name="long"  value="" id="us3-lon" /> -->

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <!--  <label for="state">State*</label> -->
                  <select class="form-control" name="state" id="state" onchange="getCity()">
                    <option value="">Select State*</option>
                    @foreach($states as $all_states)
                    <option value="{{$all_states->id}}">{{$all_states->name}}</option>
                    @endforeach
                    <!-- <?php foreach ($states as $all_states) : ?>
                        <option value="<?php echo $all_states->id ?>"><?php echo $all_states->name; ?></option>
                      <?php endforeach; ?> -->
                  </select>
                  <div class="state-err error_class" style="display:none">Please select any option</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                     <!-- label for="city">City*</label> -->
                  <select class="form-control dropdown_state" name="city" id="city">

                    <option value="">Select City*</option>
                  </select>
                  <div class="city-err error_class" style="display:none">Please select any option</div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <!-- <label for="shortdescription">Short description*</label> -->
                  <textarea name="shortdescription" maxlength="500" class="form-control" placeholder="Description" id="shortdescription" required></textarea>
                  <div class="shortdescription-err error_class" style="display:none">This field is required</div>
                </div>
              </div>
              <!-- <div class="col-md-12"> -->
                <div class="col-md-6">
                  <div class="form-group">
                   <!--  <label>Set Lead Budget*</label> -->
                    <select class="form-control" name="lead_price_budget" id="lead_price_budget" required>
                      <option value=""> Select Budget</option>
                      @foreach($lead_budget as $budget)
                      <option value="{{$budget->id}}">{{$budget->lead_price_slot}}</option>
                      @endforeach
                    </select>
                    <div class="lead_price_budget-err error_class" style="display:none">Please select any option</div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
               <!--      <label>Number Of Lawyers Who Can Contact You*</label> -->
                    <select name="max_lawyer" id="max_lawyer" class="form-control" required>
                      <option value="">Select Lawyer Count</option>
                      @foreach($max_lawyer_see_lead as $lawyer_max)

                      <option value="{{$lawyer_max->id}}">{{$lawyer_max->lawyer_max}}</option>
                      @endforeach

                    </select>
                    <div class="max_lawyer-err error_class" style="display:none">Please select any option</div>
                  </div>
                </div>

              <!-- </div> -->

              <div class="col-md-12">
                <h6> You would like to recieve emails*</h6>
                <div class="form-check">
                  <input type="radio" class="form-check-input" value="Yes" name="emailtime"  required/>
                  <label class="form-check-label rchjemal" for="">Yes</label>
                </div>

                <div class="form-check">
                  <input type="radio" class="form-check-input" value="no" name="emailtime"  />
                  <label class="form-check-label rchjemal" for="">No</label>
                </div>
              </div>
              <div class="col-md-12">
                <div class="emailtime-err error_class" style="display:none">Please select any option</div>
             


              <h6 class="recieve-call-heading">You would like to recieve calls on*</h6>
              <ul class="time-details">
                <li><input type="radio" value="Any Time" name="calltime" required />Any Time</li>
                <li><input type="radio" value="12PM to 3PM" name="calltime" />12PM to 3PM</li>
                <li><input type="radio" value="3PM to 6PM" name="calltime" />3PM to 6PM</li>
                <li><input type="radio" value="6PM to 9PM" name="calltime" />6PM to 9PM</li>
                <li><div class="calltime-err error_class" style="display:none">Please select any option</div></li>
              </ul>
              </p>
            </div>

            <div class="col-md-12 text-center final">
              <button type="button" class="legal-q-button legal-btn" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
              <button type="button" class="legal-q-button legal-btn" id="nextBtn" onclick="nextPrev(1)"></button>
            </div>

            <!-- Circles which indicates the steps of the form: -->
            <!-- <div class="col-md-12 text-center" style="margin-top: 40px;">
              <span class="step"></span>
            </div> -->

          </form>




        </div>


      </div>

    </div>



  </div>
</section>

<script type="text/javascript">

  function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }

  

  $(".rating").rating({
    size: 'sm',
    displayOnly: true,
    showCaption: false
  });


  var currentTab = 0; // Current tab is set to be the first tab (0)
  showTab(currentTab); // Display the current tab

  function showTab(n) {



    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";

    //... and fix the Previous/Next buttons:
    if (n == 0) {

      document.getElementById("prevBtn").style.display = "none";
      document.getElementById("first_prog").style.width = "0%";

    } else {

      document.getElementById("first_prog").style.width = "50%";
      document.getElementById("progresscount").innerHTML = "50% complete";
      document.getElementById("prevBtn").style.display = "inline";

    }
    if (n == (x.length - 1)) {
      document.getElementById("nextBtn").innerHTML = "Submit";
      document.getElementById("progresscount").innerHTML = "100% complete";
      document.getElementById("first_prog").style.width = "100%";


    } else {


      document.getElementById("nextBtn").innerHTML = "Next";
    }
    //... and run a function that will display the correct step indicator:


    fixStepIndicator(n)
  }

  function nextPrev(n) {
    // This function will figure out which tab to display
    var x = document.getElementsByClassName("tab");




    // Exit the function if any field in the current tab is invalid:
    if (n == 1 && !validateForm()) return false;
    // Hide the current tab:
    // x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    // alert(currentTab);
    if (currentTab >= x.length) {
      // alert("ok");
      // ... the form gets submitted:
      // $(".final").hide();

      // document.getElementById("regForm").submit();
      // $("#pop__up").modal('hide');
      leadSave();


      // return false;
    }
    // Otherwise, display the correct tab:

    if (currentTab <= 3) {
      showTab(currentTab);
    }


  }

  function validateForm() {

    // This function deals with validation of the form fields
    $('input').removeClass('invalid');
    $('select').removeClass('invalid');
    var x, y, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByTagName("input");

    z = x[currentTab].getElementsByTagName("select");


    // A loop that checks every input field in the current tab:
    for (i = 0; i < y.length; i++) {
      // If a field is empty...
      if (y[i].value == "") {
        // add an "invalid" class to the field:
        y[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      }
    }

    for (i = 0; i < z.length; i++) {
      if (z[i].value == "") {
        // add an "invalid" class to the field:
        z[i].className += " invalid";
        // and set the current valid status to false
        valid = false;
      } else {
        z[i].classList.remove("invalid");
      }
    }
    $('.name-err').hide();
      // document.getElementById("regForm").submit();
      // $("#pop__up").modal('hide');
      var name = document.getElementById('name').value;
      var validatename = validName(name);
      if (!validatename) {
        $('#name').addClass('invalid');
        $('.name-err').show();
        valid = false;
      }else{
          $('.name-err').hide();
      }
       var phone = document.getElementById('phone').value;
   
      if (!phone) {
        $('#phone').addClass('invalid');
        $('.phone-err').show();
        valid = false;
      }else{
          $('.phone-err').hide();
      }
      @if(!$subcat->isEmpty())
       var subissue = document.getElementById('subissue').value;
      
      if (!subissue) {
        $('#subissue').addClass('invalid');
        $('.subissue-err').show();
        valid = false;
      }else{
          $('.subissue-err').hide();
      }
      @endif
      var action_list = document.getElementById('action_list').value;
      
      if (!action_list) {
        $('#action_list').addClass('invalid');
        $('.action_list-err').show();
        valid = false;
      }else{
          $('.action_list-err').hide();
      }

       var email = document.getElementById('email').value;
      
      if (!email) {
        $('#email').addClass('invalid');
        $('.email-err').show();
        valid = false;
      }else{
          $('.email-err').hide();
      }


      var location = document.getElementById('location').value;
      
      if (!location) {
        $('#location').addClass('invalid');
        $('.location-err').show();
        valid = false;
      }else{
          $('.location-err').hide();
      }

      var state = document.getElementById('state').value;
      
      if (!state) {
        $('#state').addClass('invalid');
        $('.state-err').show();
        valid = false;
      }else{
          $('.state-err').hide();
      }


      var city = document.getElementById('city').value;
      
      if (!city) {
        $('#city').addClass('invalid');
        $('.city-err').show();
        valid = false;
      }else{
          $('.city-err').hide();
      }


      var shortdescription = document.getElementById('shortdescription').value;
      
      if (!shortdescription) {
        $('#shortdescription').addClass('invalid');
        $('.shortdescription-err').show();
        valid = false;
      }else{
          $('.shortdescription-err').hide();
      }


      var lead_price_budget = document.getElementById('lead_price_budget').value;
      
      if (!email) {
        $('#lead_price_budget').addClass('invalid');
        $('.lead_price_budget-err').show();
        valid = false;
      }else{
          $('.lead_price_budget-err').hide();
      }

      var max_lawyer = document.getElementById('max_lawyer').value;
      
      if (!max_lawyer) {
        $('#max_lawyer').addClass('invalid');
        $('.max_lawyer-err').show();
        valid = false;
      }else{
          $('.max_lawyer-err').hide();
      }

       var emailtime = $("input[name='emailtime']:checked").val();
        if (!emailtime) {
        
        $('.emailtime-err').show();
        valid = false;
      }else{
          $('.emailtime-err').hide();
      }
       var calltime = $("input[name='calltime']:checked").val();
        if (!calltime) {
        
        $('.calltime-err').show();
        valid = false;
      }else{
          $('.calltime-err').hide();
      }
       
    // If the valid status is true, mark the step as finished and valid:
    // if (valid) {
    //   document.getElementsByClassName("step")[currentTab].className += " finish";
    // }
    return valid; // return the valid status
  }

  function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    // var i, x = document.getElementsByClassName("step");
    // for (i = 0; i < x.length; i++) {
    //   x[i].className = x[i].className.replace(" active", "");
    // }
    // //... and adds the "active" class on the current step:
    // x[n].className += " active";
  }


  function validName(n){
    var nameRegex = /^[a-zA-Z\-]+$/;
    if(n.match(nameRegex) == null){
      return false;
    }
    else{
      return true;
    }
  }
  function leadSave() {
    // alert('ljgjk');
    var subissue = $('#subissue').val();
    var action = $('#action_list').val();
    var phone = $('#phone').val();
    var name = $('#name').val();
    var email = $('#email').val();
    var calltime = $('input[name=calltime]:checked').val();;
    var location = $('#city').val();
    var state_id = $('#state').val();
    var city_id = $('#city').val();
    var recieve_emails = $('input[name=emailtime]:checked').val();;
    var shortdescription = $('#shortdescription').val();
    var issue = $('#issue').val();
    var lead_price_budget = $('#lead_price_budget').val();
    var max_lawyer = $('#max_lawyer').val();



    console.log(subissue)
    console.log(action)
    console.log(phone)
    console.log(name)
    console.log(email)
    console.log(calltime)
    console.log(location)
    console.log(recieve_emails)
    console.log(shortdescription)
    console.log(issue)
    console.log(lead_price_budget)
    console.log(max_lawyer)
    // e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });

    $.ajax({

      url: "{{route('front.addlead')}}",
      method: "GET",
      dataType: 'json', // payload is json
      contentType: 'application/json',
      data: {
        subissue: JSON.stringify(subissue),
        action_list: JSON.stringify(action),
        phone: JSON.stringify(phone),
        name: JSON.stringify(name),
        email: JSON.stringify(email),
        calltime: JSON.stringify(calltime),
        phone: JSON.stringify(phone),
        location: location,
        state: state_id,
        city: city_id,
        recieve_emails: recieve_emails,
        shortdescription: JSON.stringify(shortdescription),
        issue: JSON.stringify(issue),
        lead_price_budget: lead_price_budget,
        max_lawyer: max_lawyer,
      },
      // headers: {
      //   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      //   },
      success: function(data) {
        $("#pop__up").modal('hide');
    // alert('ljgfjk');
        var obj = data;
        if (obj == true) {
          // $('#flash_message').html('<h4 id="p1" style="color:green;text-align:center">Your lead have successfully submitted with us.</h4>');
          toastr.success("Your Lead have successfully submitted with us.");

          // setTimeout(function() {
          //   $("#flash_message #p1").fadeOut();;
          // }, 5000);

        }
        console.log(obj);
        return true;
      },
      error: function(error) {
        console.log(`Error ${error}`);
        return false;
      }

    });




  }
</script>

<script type="text/javascript">
  google.maps.event.addDomListener(window, 'load', function() {
    var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
    google.maps.event.addListener(places, 'place_changed', function() {

    });
  });
</script>



<script type="text/javascript">
  //  google.maps.event.addDomListener(window, 'load', function () {
  //   var txtPlaces = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));

  //     google.maps.event.addListener(txtPlaces, 'place_changed', function () {
  //       console.log('place changed')
  // 		  var txtPlace = txtPlaces.getPlace();
  //                  // Do watever wif teh value!
  //                  var lat=txtPlace.geometry.location.lat();
  //                  var lon=txtPlace.geometry.location.lng();
  // 			document.getElementById("us3-lat").value = lat;
  // 			document.getElementById("us3-lon").value = lon;
  //       });


  // });

  // google.maps.event.addDomListener(window, 'load', function () {
  //     var places = new google.maps.places.Autocomplete(document.getElementById('location'));
  //     google.maps.event.addListener(places, 'place_changed', function () {
  // 	  var place = places.getPlace();
  //                // Do watever wif teh value!
  //                var lat=place.geometry.location.lat();
  //                var lon=place.geometry.location.lng();
  // 		document.getElementById("us3-lataa").value = lat;
  // 		document.getElementById("us3-lonaa").value = lon;
  //     });




  // });
</script>


<script>
  var map;
  var geocoder;
  var marker;
  var people = new Array();
  var latlng;
  var infowindow;

  $(document).ready(function() {

    ViewCustInGoogleMap();


  });


  function ViewCustInGoogleMap() {

    var mapOptions = {
      center: new google.maps.LatLng(<?php if (isset($lat) && isset($longitude)) {
                                        echo $lat . "," . $longitude;
                                      } else {
                                        echo "0,0";
                                      } ?>), // Coimbatore = (11.0168445, 76.9558321)
      zoom: 13,
      mapTypeControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);



    // Get data from database. It should be like below format or you can alter it.
    var data = '<?php echo json_encode($merged); ?>';

    //var data = '[{"id": 183,"name": "Arjun gupta","username": "Arjun", "LatitudeLongitude": "30.596050759460997,76.84414168413082","MarkerId": "lawyer"},{"id": 183,"name": "SBP housing","username": "Mr Parminder singh", "LatitudeLongitude": "30.5881,76.8476","MarkerId": "lawyer"}]';
    //var data = '[{ "name": "sunielsethy", "addr": "New Delhi", "LatitudeLongitude": "28.5603,77.1617", "MarkerId": "Customer" },{ "DisplayText": "abcd", "ADDRESS": "Coimbatore-641042", "LatitudeLongitude": "11.0168445,76.9558321", "MarkerId": "Customer"}]';

    people = JSON.parse(data);

    console.log(people);

    for (var i = 0; i < people.length; i++) {
      setMarker(people[i]);
    }







  }

  function setMarker(people) {
    geocoder = new google.maps.Geocoder();
    infowindow = new google.maps.InfoWindow();
    if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
      geocoder.geocode({
        'address': people["address"]
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
          marker = new google.maps.Marker({

            position: latlng,
            map: map,
            draggable: false,
            mapTypeControl: false,
            html: people["name"],
            icon: "images/marker/" + people["MarkerId"] + ".png"


          });
          //marker.setPosition(latlng);
          //map.setCenter(latlng);
          google.maps.event.addListener(marker, 'click', function(event) {
            infowindow.setContent(this.html);
            infowindow.setPosition(event.latLng);
            infowindow.open(map, this);
          });
        } else {
          // alert(people["name"] + " -- " + people["address"] + ". This address couldn't be found");
        }
      });
    } else {

      if (people["MarkerId"] == "Lawyer") {
        var latlngStr = people["LatitudeLongitude"].split(",");
        var lat = parseFloat(latlngStr[0]);
        var lng = parseFloat(latlngStr[1]);
        latlng = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable: false,
          mapTypeControl: false, // cant drag it
          html: '<div jstcache="2" style="min-width: 167px !important;"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address" style="display:inline"> <div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"><img src="<?php echo url("social/"); ?>/uploads/' + people["photo"] + '" class="img img-thumbnail" width="100" height="100" alt="no image found.."  style="float:left;margin-right:5px;display:inline-block;padding: 0;width: 37%;height: 70px;" ></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width" style="display: inline-block;margin-top: 5px;font-weight: 600;text-transform: capitalize;font-size: 14px;" >' + people["name"] + ',<br></div><div style="font-size:13px;font-weight:300">' + people["education"] + '<br><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true" style="margin:5px 0"></i></a></div> </div> </div>',
          icon: '{{url("assets/images/blue-dot.png")}}',
          animation: google.maps.Animation.DROP,



          // Content display on marker click
          //icon: "images/marker.png"       // Give ur own image
        });
        //marker.setPosition(latlng);
        //map.setCenter(latlng);
        google.maps.event.addListener(marker, 'click', function(event) {
          infowindow.setContent(this.html);
          infowindow.setPosition(event.latLng);
          infowindow.open(map, this);
        });
      } else {

        var latlngStr = people["LatitudeLongitude"].split(",");
        var lat = parseFloat(latlngStr[0]);
        var lng = parseFloat(latlngStr[1]);
        latlng = new google.maps.LatLng(lat, lng);
        marker = new google.maps.Marker({
          position: latlng,
          map: map,
          draggable: false,
          mapTypeControl: false, // cant drag it
          html: '<div jstcache="2" style="min-width: 115px !important;padding:10px"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div> <div class="address"> <div jstcache="4" jsinstance="0" style="display:inline-block;margin-top:2px;font-weight:600;text-transform:capitalize;font-size:14px" class="address-line full-width" jsan="7.address-line,7.full-width">' + people["MarkerId"] + '</div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width">' + people["name"] + ',' + people["description"] + '</div> </div> </div>',

          icon: '{{url("assets/images/spotlight-poi2.png")}}',

          animation: google.maps.Animation.DROP,



          // Content display on marker click
          //icon: "images/marker.png"       // Give ur own image
        });
        //marker.setPosition(latlng);
        //map.setCenter(latlng);
        google.maps.event.addListener(marker, 'click', function(event) {
          infowindow.setContent(this.html);
          infowindow.setPosition(event.latLng);
          infowindow.open(map, this);
        });


      }
    }



  }

  function getCity() {
    var error = 0;
    var final_list = "";
    var state_id = "";
    state_id = $('#state').val();
    final_list = $('#city');

    if (state_id == 0 || state_id == "") {
      error = 1;
    }
    if (error == 1) {
      return false;
    } else {
      var cities = [];
      var all_city = "";

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        url: "{{route('get-city-by-state-id')}}",
        method: "GET",
        dataType: 'json', // payload is json
        contentType: 'application/json',
        beforeSend: function() {
          $('#notification-loaders').fadeIn(100);
        },
        complete: function() {
          $('#notification-loaders').css("display", "none");
        },
        data: {
          state_id: state_id
        },
        success: function(data) {
          var obj = data;
          for (let index = 0; index < obj.length; index++) {
            cities += `<option value="${obj[index].id}" id="">${obj[index].name}</option>`;
          }
          final_list.html(cities);
        },
        error: function(error) {
          console.log(`Error ${error}`);
        }
      });
    }
  }
</script>
<script>
  $(document).ready(function() {
    $("#pop__up").modal('show');

    $('.connect-frnd').click(function() {
      var connectValue = $(this).data('frnd');
      var connectuserValue = $(this).data('user');
      localStorage.setItem('connect', connectValue);
      localStorage.setItem('connectuser', connectuserValue);
    });

  });


  function add_friend(lawyerid, ajaxUrl, action = '') {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: ajaxUrl,
      type: 'GET',
      dataType: 'json',
      success: function(msg) {
        console.log(msg);
        if (msg.error) {
          alert(msg.error_msg);
          if (action == 'register') {
            $('.formregister')[0].reset();
            window.location.reload();
          }
          return;
        }
        if (msg.success) {
          if (action == 'register') {
            $('.formregister')[0].reset();
            window.location.reload();
          }
          $('#friend_button_' + userid).html(msg.message);
          $('#friend_button_' + userid).addClass("disabled");
        }
      },
      error: function(msg) {
        console.log(msg);
      }
    })
  }
</script>
@endsection