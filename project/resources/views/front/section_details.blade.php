@extends('layouts.front')
@section('content')
<div class="container">

		<div class="dtail-act-wrap">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
		@foreach($actsections as $details)
		
		
			<h1>{{ $details->title }}</h1>
			<p>{{!! html_entity_decode($details->description) !!}}</p>
			
		@endforeach
	</div>
	</div>


@endsection
