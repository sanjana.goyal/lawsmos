@extends('layouts.front')
@section('content')

<style>
	small.make-database {
    display: none;
    }     
    section.judgment_details h1 {
        padding-top:57px !important;
    }
</style>
<br>
<br>
<section class="judgment_details">
<div class="container">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<h1>Supreme Court Judgements </h1>

<input type="hidden" name="judgement_id" id="judgement_id" value="{{$supremecourt->ID}}">	
<input type="hidden" name="judgement_heading" id="judgement_heading" value="{{ html_entity_decode($supremecourt->heading)}}">	
<input type="hidden" name="judgement_type" id="judgement_type" value="2">	

		
	
			
			<p>
				{!! base64_decode(html_entity_decode($supremecourt->content)) !!}
			
			</p>

			
	


</div>
</section>
@endsection
