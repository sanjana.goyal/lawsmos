@extends('layouts.front')
@section('content')
<div class="container">

	<div class="row">
		<div class="col-md-12">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
			<h1>{{  $result->term }} </h1>
			
			<p>{{ strip_tags($result->definition) }} </p>
			
			
		</div>
	
	</div>

</div>
@endsection
