@extends('layouts.front')
@section('content')


<div class="container">
    <div class="whole-wrapperr">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<h1>Bareacts</h1>
<center>
<?php  
                          $character = range('A', 'Z');  
                          echo '<ul class="pagination">';  
                          foreach($character as $alphabet)  
                          {  
                               echo "<li><a href=".url('searchbacts/'.$alphabet).">$alphabet</a></li>";  
                               
                          }  
                          echo '
							<li><a href="'.url('bareacts').'">View All</a></li>
                          </ul>'; 
                          
                           
                     ?> 
</center>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			
					<div class="form-group" >
							
							<input type="text" class="form-controller form-control" id="search" name="search" placeholder="Search.."/>							
					</div>
		<div id="result"></div>
		<ul class="bareacts-list">
			
			@if(!empty($alphaterms))
			
			@foreach($alphaterms as $acts)

				<li><a href="{{ url('act_details',$acts->id) }}">{{ $acts->title }}</a></li>

			@endforeach
			
			
			@endif
			
			@if(!empty($bareacts))
			@foreach($bareacts as $acts)

				<li><a href="{{ url('act_details',$acts->id) }}">{{ $acts->title }}</a></li>

			@endforeach
			@endif
			
			
</ul>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

$('#search').on('keyup',function(){
	

var value=$(this).val();
if (!this.value) {
       $('.bareacts-list').show();
       $('#result').hide();
    }
else{
$.ajax({
type : 'get',
async: true,
url : '{{URL::to("searchacts")}}',

data:{'search': value},
success:function(data){
$("#result").html(data);
 
$('#result').show();

$('.bareacts-list').hide();

}
});

}


});

$('#search').on('keydown',function(){
	
	var value=$(this).val();
if(value=="" || value==null)
{
	$('.bareacts-list').show();
}
	});
</script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

@endsection
