@extends('layouts.front')
@section('content')
<div class="container">
    <div class="whole-wrapperr">
		<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
        	<h1 align="center">All Articles</h1>
      
<div class="articl_side_list">
    <h2>Catagories</h2>
    <ul>
		
			@foreach($catss as $cat)
				<li><a href="javascript:void(0);">{{$cat->c_name}}</a></li>
			@endforeach
    </ul>
    </div>
    <div class="art_content-wrapper">
     
						     <div class="art-tabbs">
                                 <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#Recent" aria-controls="Recent" role="tab" data-toggle="tab">Recent</a></li>
    <li role="presentation"><a href="#Popular" aria-controls="Popular" role="tab" data-toggle="tab">Popular</a></li>
    <li role="presentation"><a href="#Featured" aria-controls="Featured" role="tab" data-toggle="tab">Featured</a></li>
	<li role="presentation"><a href="#bycatagory" aria-controls="Catagories" role="tab" data-toggle="tab">Articles by Catagory</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="Recent">
        
        <ul class="short-para">
					  @foreach($recent as $art)
							<li><a href="{{route('front.articles',$art->ID)}}">{{$art->title}}</a>
								<p>Short Description: {{ $art->meta_description }}</p>
                <!-- <p>Description: {!! base64_decode($art->description) !!}</p> -->
								<p><a class="reda-more-art transition_3s" href="{{route('front.articles',$art->ID)}}">Read More</a></p>
							</li>
					  @endforeach
					</ul>
      </div>
      
       <div role="tabpanel" class="tab-pane active" id="bycatagory">
        
			<select id="catagories_select" class="form-control">
                                <option>Select Catagory</option>
                                @foreach($catss as $cat)

                                  <option value="{{$cat->cid}}">{{$cat->c_name}}</option>
                                @endforeach
                            </select>
            <ul class="short-para">                
				<div id="result"></div>
            </ul>
            
      </div>
      
		<div role="tabpanel" class="tab-pane" id="Popular">
				   <ul class="short-para">
					  @foreach($popular as $art)
							<li><a href="{{route('front.articles',$art->ID)}}">{{$art->title}}</a>
								<p>Short Description: {{base64_decode($art->description)}}</p>
								<p><a class="reda-more-art transition_3s" href="{{route('front.articles',$art->ID)}}">Read More</a></p>
							</li>
					  @endforeach
					</ul>
		</div>
    <div role="tabpanel" class="tab-pane" id="Featured">
					<ul class="short-para">
					  @foreach($featured as $art)
							<li><a href="{{route('front.articles',$art->ID)}}">{{$art->title}}</a>
								<p>Short Description: {{base64_decode($art->description)}}</p>
								<p><a class="reda-more-art transition_3s" href="{{route('front.articles',$art->ID)}}">Read More</a></p>
							</li>
					  @endforeach
					</ul>
      </div>

  </div>
        </div>
				

	</div>
		
</div>
    </div>

<script>

$(document).ready(function(){

$("select#catagories_select").change(function(){
        var selectedCatagory = $(this).children("option:selected").val();
      

        $.ajax({
type : 'get',
async: true,
url : '{{URL::to("searcharticlebycatgg")}}',

data:{'searchcat': selectedCatagory},
success:function(data){

console.log(data);

if(data=="")
{
	$("div#result").html("<p>No Articles found in this catagory....</p>");
}

else{

$("div#result").html(data);
}							   
				 
	

}
});





    });


  });
  
  </script>
@endsection
