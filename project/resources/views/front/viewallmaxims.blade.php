@extends('layouts.front')
@section('content')
<style>
	#maxms_heading{
		padding-top: 60px;
	}
</style>
<h2 align="center" id="maxms_heading">All Legal Terms</h2>

 
<div class="container">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<div class="well">
<?php  
                          $character = range('A', 'Z');  
                          echo '<ul class="pagination">';  
                          foreach($character as $alphabet)  
                          {  
                               echo "<li><a href=".url('searchterms/'.$alphabet).">$alphabet</a></li>";  
                               
                          }  
                          echo '
							<li><a href="'.url('maxims').'">View All</a></li>
                          </ul>'; 
                          
                           
                     ?> 
						
					
				
					
					
					<div class="form-group" >
							<input type="text" class="form-controller form-control" id="search" name="search" placeholder="Search..">
							
					</div>
				<div id="resultt"></div>
				
				
				@if(!empty($alphaterms))
					<div class="list">	  
						@foreach($alphaterms as $terms)
								
								 <div class="list_blockk">
								<h4>{{$terms->term}}</h4>
								<p>{{strip_tags($terms->definition)}}</p>
                        </div>
								
							 
						@endforeach
					
				</div>
					@endif
					
			@if(!empty($maxims))
				<div class="list">	  
					  @foreach($maxims as $maxims)
                    <div class="list_blockk">
								<h4>{{$maxims->term}}</h4>
								<p>{{strip_tags($maxims->definition)}}</p>
                        </div>
					  @endforeach
				</div>
				
			@endif
						
						</div>
					</div>
		



<script type="text/javascript">

$('#search').on('keyup',function(){
	

var value=$(this).val();
if (!this.value) {
       $('.list').show();
       $('#resultt').hide();
    }
else{
$.ajax({
type : 'get',
async: true,
url : '{{URL::to("searchmaxims")}}',

data:{'search': value},
success:function(data){
$("#resultt").html(data);
 
$('#resultt').show();

$('.list').hide();

}
});

}


});

$('#search').on('keydown',function(){
	
	var value=$(this).val();
if(value=="" || value==null)
{
	$('.list').show();
}
	});
</script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

@endsection
