@extends('layouts.front')
@section('content')

<div class="container">
<div class="well">
<div class="row">
	<div class="col-md-12">
<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
						
					<h1>{{ $article->title }}</h1>
						<p>{!! base64_decode($article->description) !!}</p>
					<div class="section-content">	
					  <!-- @if(!empty($menu))
								<div class="section-tree art-tree">
									<h5>Contents: </h5>
									<?php echo $menu; ?>
								</div>
					  @endif -->
					  
					  @foreach($sections as $section)
							<div class="submne" id="<?php echo $section['ID']."section"; ?>">
								<h4><?php	echo $section['title']; ?> </h4>
								<p><?php echo base64_decode($section['body']);  ?></p>
							</div>
					  @endforeach
						
						</div>
					</div>
			</div>
</div>
</div>
</div>
@endsection
