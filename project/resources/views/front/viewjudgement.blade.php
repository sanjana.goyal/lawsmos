@extends('layouts.front')
@section('content')

<div class="container">
<div class="row">
	<div class="col-md-12 backend-text">
		<a class='go_back_button' href='javascript: history.go(-1)'>Go Back</a>
<h1>{{ $result->heading }}</h1>
<p>{{ strip_tags(base64_decode($result->content))  }}</p>
	</div>
</div>
</div>

@endsection
