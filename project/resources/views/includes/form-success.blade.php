@if (Session::has('success'))
      
      <div class="alert alert-success validation">
      <button type="button" class="close global_msg" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p class="text-left">{{ Session::get('success') }}</p> 
      </div>

@endif

@if (Session::has('unsuccess'))
      
      <div class="alert alert-danger validation">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p class="text-left">{{ Session::get('unsuccess') }}</p> 
      </div>

@endif

@if(session('message')==='f')
      <div class="alert alert-danger validation">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p>Credentials doesn't match.</p>
      </div>
@endif 

@if(session('message')==='user_verify')
      <div class="alert alert-danger validation">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p>Please check your email to verify your account.</p>
      </div>
@endif 

@if(session('message')==='user_verified')
      <div class="alert alert-success validation">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p>Your account has been verified successfully.</p>
      </div>
@endif 

<script type="text/javascript">
$(document).ready(function() {
    // alert('goooo');
    setTimeout(function(){ 
        $('.global_msg').fadeOut(1000);        
       }, 8000);
});

</script>