<!-- starting of footer area -->

<input type="hidden" id="base_url_js" value="{{url('/')}}">
<footer class="section-padding footer-area-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 ">
                <ul class="footerlist">
                    <li><a href="{{url('faq')}}">FAQ</a></li>
                    <li><a href="{{url('contact')}}">Contact Us</a></li>
                    <li><a href="{{url('bareacts')}}">Bare Acts</a></li>
                    <li><a href="{{url('articles')}}">Articles</a></li>
                    <li><a href="{{url('maxims')}}">Legal Terms</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <ul class="footerlist">
                    <li><a href="{{url('pages/9')}}">Community Guidelines</a></li>
                    <li><a href="{{url('pages/13')}}">Cookies Policy </a></li>
                    <li><a href="{{url('pages/14')}}">Membership Policy</a></li>
                    <li><a href="{{url('pages/12')}}">Privacy Policy</a></li>
                    <li><a href="{{url('pages/11')}}">Terms of Use</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <ul class="footerlist">
                    <li><a href="{{url('pages/10')}}">Liability Disclaimer</a></li>
                    <li><a href="{{url('history')}}">Update History</a></li>
                    <li><a href="{{url('supremecourt')}}">Supreme Court Judgements</a></li>
                    <li><a href="{{url('highcourt')}}">High Court Judgements</a></li>
                    <li><a href="{{url('allmaxims')}}">Maxims</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-footer-area">
                    <div class="footer-title">{{$lang->contact}}</div>
                    <div class="footer-content">
                        <div class="contact-info">
                            @if($gs->street != null)
                            <p class="contact-info">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <!-- {{$gs->street}} -->
                                #588, Sector 6, Panchkula, Haryana 134109
                            </p>
                            @endif
                            @if($gs->phone != null)
                            <p class="contact-info">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <!-- <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a> -->
                                <a href="tel:077107 77770">077107 77770</a>
                            </p>
                            @endif
                            @if($gs->email != null)
                            <p class="contact-info">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <!-- <a href="mailto:{{$gs->email}}">{{$gs->email}}</a> -->
                                <a href="mailto:info.lawsmos@gmail.com">info.lawsmos@gmail.com</a>
                            </p>
                            @endif
                            <!-- @if($gs->site != null)
                                <p class="contact-info">
                                    <i class="fa fa-globe" aria-hidden="true"></i>
                                    <a href="{{$gs->site}}">{{$gs->site}}</a>
                                </p>
                                @endif -->
                        </div>
                    </div>
                </div>
                <div class="footer-social-links">
                    <ul>
                        @if($sl->f_status == 1)
                        <li><a target="_blank" class="facebook" href="{{$sl->facebook}}">
                            <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        @endif
                        @if($sl->g_status == 1)
                        <li><a target="_blank" class="google" href="{{$sl->gplus}}">
                            <i class="fa fa-google"></i>
                            </a>
                        </li>
                        @endif
                        @if($sl->t_status == 1)
                        <li><a target="_blank" class="twitter" href="{{$sl->twitter}}">
                            <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        @endif
                        @if($sl->l_status == 1)
                        <li><a target="_blank" class="tumblr" href="{{$sl->linkedin}}">
                            <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <!--
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer-area">
                    <div class="footer-title">{{$lang->about}}</div>
                    <div class="footer-content">
                        <p>{{$gs->about}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer-area">
                    <div class="footer-title">{{$lang->fl}}</div>
                    <div class="footer-content">
                        <ul class="about-footer">
                            <li><a href="{{route('front.index')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->home}}</a></li>
                        @if($ps->a_status == 1)
                            <li><a href="{{route('front.about')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->about}}</a></li>
                        @endif
                        @if($ps->f_status == 1)
                            <li><a href="{{route('front.faq')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->faq}}</a></li>
                        @endif
                        @if($ps->c_status == 1)
                            <li><a href="{{route('front.contact')}}"><i class="fa fa-caret-right"></i> &nbsp;{{$lang->contact}}</a></li>
                        @endif
                        <li><a href="{{route('front.bareacts')}}"><i class="fa fa-caret-right"></i> &nbsp;Bare Acts</a></li>
                        <li><a href="{{route('front.listallarticles')}}"><i class="fa fa-caret-right"></i> &nbsp;Articles</a></li>
                        <li><a href="{{route('front.listallmaxims')}}"><i class="fa fa-caret-right"></i> &nbsp;Legal Terms</a></li>
                        
                        <li><a href="{{route('front.listallhighcourt')}}"><i class="fa fa-caret-right"></i> &nbsp;High Court Judgements</a></li>
                        <li><a href="{{route('front.listallsupreme')}}"><i class="fa fa-caret-right"></i> &nbsp;Supreme Court Judgements</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="single-footer-area">
                    <div class="footer-title">All Pages</div>
                    <div class="footer-content">
                        <ul class="latest-tweet">
                            @foreach($lblogsss as $lblog)
                            
                            
                             <li><a href="{{route('front.pageshow',$lblog->ID)}}"><i class="fa fa-caret-right"></i> {{$lblog->meta_title}}</a></li>
            
                            @endforeach
                            
                            <li><a href="{{route('front.updatehistory')}}"><i class="fa fa-caret-right"></i> &nbsp;Update History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-5 col-sm-6 col-xs-12">
                <div class="single-footer-area">
                    <div class="footer-title">{{$lang->contact}}</div>
                    <div class="footer-content">
                        <div class="contact-info">
                        @if($gs->street != null)                                    
                          <p class="contact-info">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                {{$gs->street}}
                            </p>
                        @endif
                        @if($gs->phone != null)   
                            <p class="contact-info">
                                 <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="tel:{{$gs->phone}}">{{$gs->phone}}</a>
                            </p>
                        @endif
                        @if($gs->email != null)   
                            <p class="contact-info">
                                 <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="mailto:{{$gs->email}}">{{$gs->email}}</a>
                            </p>
                        @endif
                        @if($gs->site != null)   
                            <p class="contact-info">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <a href="{{$gs->site}}">{{$gs->site}}</a>
                            </p>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
            -->
    </div>
    <div class="footer-copyright-area">
        <div class="container">
            <div class="copyright">
                <p class="copy-right-side">
                    COPYRIGHT <?php echo date("Y"); ?> B&B Associates LLP 
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Ending of footer area -->
<!-- Modal -->
<div class="modal fade dark-modal-side" id="registerModal" role="dialog" style="overflow: scroll;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sign Up</h4>
            </div>
            <div class="modal-body">
                <div class="form-resp-wraper">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="dropdown">
                            <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">Select Registration Type<span class="caret"></span></a>
                            <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                <li class="active"><a href="#dropdown2" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">Redgister as Service Seeker</a></li>
                                <li class=""><a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">Register as Advocate</a></li>
                                <li class=""><a href="#dropdown3" role="tab" id="dropdown3-tab" data-toggle="tab" aria-controls="dropdown3" aria-expanded="false">Register as law Student</a></li>
                                <li class=""><a href="#dropdown4" role="tab" id="dropdown4-tab" data-toggle="tab" aria-controls="dropdown4" aria-expanded="false">Register as law Professor</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sign-up-tab-outer">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#reg-seeker" class="user-role-select" aria-controls="reg-seeker" data-id="reg-seeker" data-role="1" role="tab" data-toggle="tab">Service Seeker</a></li>
                        <li role="presentation">
                            <a href="#reg-advocate" class="user-role-select" aria-controls="reg-advocate" role="tab" data-toggle="tab" data-role="0" data-id="reg-advocate">Advocate</a>
                        </li>
                        <li role="presentation"><a href="#reg-student" class="user-role-select" aria-controls="reg-student" role="tab" data-toggle="tab" data-role="2" data-id="reg-student">Researcher/Student</a></li>
                        <li role="presentation"><a href="#reg-pro" class="user-role-select" aria-controls="reg-pro" role="tab" data-toggle="tab" data-role="3" data-id="reg-pro">Law Professor</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="reg-seeker">
                            <form action="#" method="POST" id="register-form-serviceSeeker" class="formregister">
                                {{csrf_field()}}
                                @include('includes.form-error')
                                @include('includes.form-success')
                                <div class="alert alert-success" style="display:none" id="register_post_div_success"></div>
                                <div class="alert alert-danger" style="display:none" id="register_post_div_error">
                                    <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error('register_post_div_error')" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="lest-flex-ragistration">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="hidden" name="user_role_id" class="form-control user_role_id" value="1">
                                            <input name="firstname" class="form-control" placeholder="First Name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input name="lastname" class="form-control" placeholder="Last Name" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <select name="gender" class="form-control">
                                                <option value="">Select gender</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                                <option value="3">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </div>
                                            <input name="email" class="form-control" placeholder="{{$lang->sue}}" type="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input name="phone" class="form-control" placeholder="{{$lang->suph}}" type="phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-unlock-alt"></i>
                                            </div>
                                            <input class="form-control signup_password" name="password" placeholder="{{$lang->sup}}" type="password">

                                            <span class="input-group-addon"><i class="fa fa-eye pass_show"></i> </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-unlock-alt"></i>
                                            </div>
                                            <input class="form-control signup_password" name="password_confirmation" placeholder="{{$lang->sucp}}" type="password">

                                            <span class="input-group-addon"><i class="fa fa-eye pass_show"></i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="button" class="btn register-btn" name="button">{{$lang->signup}}</button>
                                </div>
                                <div class="form-group text-center">
                                    <a  data-dismiss="modal" data-toggle="modal" data-target="#signInModal" href="#">{{$lang->al}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade dark-modal-side in" id="signInModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sign In</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('user-login-submit')}}" method="POST" class="formlogin">
                    {{csrf_field()}}
                    @include('includes.form-error')
                    @include('includes.form-success')
                    <!-- <div class="alert alert-success"> Your Account verified successfully.</div> -->
                    <div class="alert alert-success" style="display:none" id="login_post_div_success"></div>
                    <div class="alert alert-danger" style="display:none" id="login_post_div_error">
                        <button type="button" class="close" data-dismiss="alert" onclick="hide_post_div_error('login_post_div_error')" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <?php 
                        if (isset($_SERVER['HTTP_REFERER'])) {
                            $page = $_SERVER['HTTP_REFERER'];
                            $_SESSION['page'] = $page;
                        } else {
                            $_SESSION['page'] = "";
                        }
                        //echo "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
                        ?>
                    <input type="hidden" name="previous_urlss" value="{{ Session::get('questionsssss') }}">
                    <input type="hidden" name="localstorage" value="">
                    <input type="hidden" name="previous_url" value="<?php echo $_SESSION['page']; ?>">
                    <input  name="caseheading" class="form-control" value="{{ session()->get('caseheading') }}" type="hidden">
                    <textarea  name="casecontent" class="form-control"  style="display:none;">{{ session()->get('casecontent') }}</textarea>
                    <div class="lest-flex-ragistration">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input name="email" class="form-control" placeholder="{{$lang->sie}}" type="email" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-unlock-alt"></i>
                                </div>
                                <input class="form-control signup_password" name="password" id="password" placeholder="{{$lang->spe}}" type="password" required="">
                                <span class="input-group-addon"><i class="pass_show fa fa-eye"></i> </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn login-btn" name="button">{{$lang->signin}}</button>
                    </div>
                    <div class="form-group text-center">
                        <a  data-dismiss="modal" data-toggle="modal" data-target="#registerModal" href="#">{{$lang->cn}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="userverify" id="userverify" value="0">

<script src="{{ asset('assets/front/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/front/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/front/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/front/js/wow.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.slicknav.min.js') }}"></script>
<script src="{{ asset('assets/front/js/main.js') }}"></script>

<!-- Signup and Signin -->

<script src="{{url('assets/front/js/register-login.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#page-popup").css("display", "block");
        $("#page-popup").css("z-index", "99");
        $("form").attr("autocomplete", "nope");
    });
    $(".close").click(function () {
        $('.modal').modal('hide');
    });
    $("body").on("click",".user-role-select",function(){
        var id = $(this).data('id');
        var role = $(this).data('role');
        $('.register-pane').attr('id',id);
        $('#registerModal').find('.tab-pane.active').attr('id',id);
        $('.formregister').attr('id',id+"-form");
        $('.user_role_id').val(role);
    });
    function all_values(obj, div_id) {
        var keys = _keys(obj);
        console.log(obj);
        var lengths = keys.length;
        var values = Array(lengths);
        var ulHtml = '<button type="button" class="close" onclick="hide_post_div_error(' + "'" + div_id + "'" + ');" aria-label="Close"><span aria-hidden="true">×</span></button>';
        ulHtml += "<ul>";
        for (var i = 0; i < lengths; i++) {
            ulHtml += "<li>" + obj[keys[i]] + "</li>"
            values[i] = obj[keys[i]] + "<br>";

        }
        ulHtml += "<ul>";
        return ulHtml;
    }

    function hide_post_div_error(div_id) {
        $('#' + div_id).hide();
        // $.scrollTo($('#post_div_error'), 1000);

    }

    function _keys(obj) {
        if (!isObject(obj)) return [];
        if (Object.keys) return Object.keys(obj);
        var keys = [];
        for (var key in obj)
            if (_.has(obj, key)) keys.push(key);
        return keys;
    }

    function isObject(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    }
</script>
{!! $seo->google_analytics !!}
<script type="text/javascript">
    $(window).load(function () {
        setTimeout(function () {
            $('#cover').fadeOut(1000);
        }, 1000)
    });
</script>
<script type="text/javascript">
    var x = document.getElementById("demo");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else {
            x.innerText = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        lat = position.coords.latitude;
        lon = position.coords.longitude;
        latlon = new google.maps.LatLng(lat, lon)
        mapholder = document.getElementById('mapholder')
        mapholder.style.height = '250px';
        mapholder.style.width = '100%';

        var myOptions = {
            center: latlon,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL
            }
        };
        var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
        var marker = new google.maps.Marker({
            position: latlon,
            map: map,
            title: "You are here!"
        });
    }

    function showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                x.innerText = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                x.innerText = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                x.innerText = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                x.innerText = "An unknown error occurred."
                break;
        }
    }
    function showPosition() {
        if (navigator.geolocation) {


            navigator.geolocation.getCurrentPosition(function (position) {

                var lat = parseFloat(position.coords.latitude);

                alert(lat);

                var lng = parseFloat(position.coords.longitude);
                var positionInfo = "Your current position is (" + "Latitude: " + lat + ", " + "Longitude: " + lng + ")";
                document.getElementById("result").innerHTML = positionInfo;
            });
        } else {
            alert("Sorry, your browser does not support HTML5 geolocation.");
        }
    }
    function parse_place(place) {
        var location = [];


        for (var ac = 0; ac < place.address_components.length; ac++) {
            var component = place.address_components[ac];
            switch (component.types[0]) {
                case 'locality':
                    var city = component.long_name;
                    break;
                case 'administrative_area_level_1':
                    location['state'] = component.long_name;
                    break;
                case 'country':
                    location['country'] = component.long_name;
                    break;
            }
        };


        return city;
    }
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('locationselect'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            // Do watever wif teh value!
            //    alert("hello");

            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();

            var country = place.address_components[0].short_name;

            //    var url = '<?php echo url(''); ?>/enterlocation?locationselect=Chandigarh%2C+India&lat=30.7333148&long=76.7794179';


            var city = parse_place(place);

            var url = "locationselect=" + city + "%2C+India&lat=" + lat + "&long=" + lon;

            var url = "{{ url('enterlocation?') }}" + url;
            console.log(url);


            document.getElementById("us3-lat").value = lat;
            document.getElementById("us3-lon").value = lon;
            //    document.getElementById("us3-country").value = country;
            window.location.href = url;
        });
    });

    if (document.getElementById('locationselect').value == "<?php if (isset($details->city)) { echo $details->city; }; ?>" || document.getElementById('locationselect').value == "") {
        google.maps.event.addDomListener(window, 'load', function() {
            var places = new google.maps.places.Autocomplete(document.getElementById('locationselect'));
            google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();

            // console.log(place);

            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            var country = place.address_components;


            // console.log(country[0]['short_name'];
            document.getElementById("us3-lat").value = lat;
            document.getElementById("us3-lon").value = lon;
            document.getElementById("us3-country").value = country;
            });
        });

    } else {
       //   document.getElementById("formlocation").style.display="none";
       //   document.formlocation.submit();

    }
    
</script>
@yield('scripts')
</body>
</html>