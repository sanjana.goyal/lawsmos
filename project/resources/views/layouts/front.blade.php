<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="title" content="{{$meta->meta_title}}">
    <meta name="keywords" content="{{$meta->metakeywords}}">
    @if(Request::path() != 'articles' && Request::path()!='detail/'.request()->route('id') &&
    Request::path()!='detail_supreme/'.request()->route('id') && Request::path()!='articles/'.request()->route('id'))
    <meta name="content" content="{{$meta->content}}">
    @endif
    <meta name="description" content="{{$meta->meta_description}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="keywords" content="{{$seo->meta_keys}}"> -->
    <meta name="author" content="GeniusOcean">
    <title>{{$gs->title}}</title>
    <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/slicknav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/fonts/avenir/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{asset('assets/images/'.$gs->favicon)}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAs_aXhGEJHas4iKrCkQJQtayUSprdVxAk&libraries=places"></script>-->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAp613NfAwqpgpzboBMgj1eoHTZOzglGAA&libraries=places"></script> -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL5Pfz_N5zaYOeDIp6cCA4hNNMzDVzoIM&libraries=places"></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgriKllmz3R9e7ORquSKDyASj_DP4csuo&libraries=places">
    </script>
    <link href="{{ asset('assets/bootstrap-star-rating/css/star-rating.min.css')  }}" type='text/css' rel='stylesheet'>
    <script src="{{ asset('assets/bootstrap-star-rating/js/star-rating.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo url('social/ckeditor/ckeditor.js'); ?>"></script>
    @include('styles.design')
    @yield('styles')
    @if(Auth::guard('user')->check())
    @endif
    <style>
        /* @media(max-width:768px) and (min-width:481px) {
            .mainmenu {
                display: none;
            }

            .slicknav_menu {
                display: block;
            }

            .slicknav_btn {
                background-color: #000 !important;
                border: 2px solid #fff;
                border-radius: 4px;
            }

            .slicknav_menu {
                display: block;
                background-color: transparent;
                float: right;
                top: 20px;
                position: absolute;
                right: 20px;
            }

            span.slicknav_icon.slicknav_no-text {
                margin-left: 5px;
                margin-bottom: 5px;
            }

            ul.slicknav_nav {
                position: absolute;
                left: 0;
                width: 100%;
                margin-top: 10px;
            }.slicknav_nav li {
    border: none !important;
}.slicknav_nav li a {
    color: #fff;
    font-size: 16px;
    font-weight: 400;
    margin-bottom: 11px;
}
        } */
    </style>
</head>

<body class="{{Request::path()}}">
    <div id="loading">
        <div id="loading-content">
            Loading...
        </div>
    </div>
    <!-- Popup Div Starts Here -->
    @if(\Request::is('/'))
    <div id="page-popup">
        <div id="popupContact">
            <!-- Contact Us Form -->
            <div class="col-md-12">
                <form id="formlocation" action="{{ url('enterlocation') }}" method="GET" name="formlocation">
                    <input type="hidden" name="type"
                        value="<?php if(isset($_GET['type'])) echo($_GET['type']); else echo 0; ?>">
                    <h4>Tell us the location from where you are finding lawyers?</h4>
                    <div class="pop-f-flex">
                        <div class="form-group">
                            {{-- <span id="cancel" class="glyphicon glyphicon-remove" onclick ="div_hide()"></span> --}}
                            <div class="input-group has-search" style="width: 100%;">
                                <div id="result">
                                    <!--Position information will be inserted here-->
                                </div>
                                <input id="locationselect" name="locationselect" placeholder="Enter Your Location"
                                    type="text" class="form-control"
                                    value="<?php if (!empty($cookievalue)) {
                                                                                                                                                                echo $cookievalue;
                                                                                                                                                            } ?>"
                                    required />
                                <div class="input-group-append">
                                    {{-- <a class="btn btn-secondary form-control-feedback" type="button" id="btnAction" onClick="locate()"> <img src="{{ url('assets/images/gps.png') }}"
                                    alt="Law"> </a> --}}
                                </div>
                                <input type="hidden" class="form-control" name="country" value="<?php if (!empty($cookielat)) {
                                                                                                    echo $cookielat;
                                                                                                } ?>"
                                    id="us3-country" />
                                <input type="hidden" class="form-control" name="lat" value="<?php if (!empty($cookielat)) {
                                                                                                echo $cookielat;
                                                                                            } ?>" id="us3-lat" />
                                <input type="hidden" class="form-control" name="long" value="<?php if (!empty($cookielon)) {
                                                                                                    echo $cookielon;
                                                                                                } ?>" id="us3-lon" />
                            </div>
                        </div>
                        <div class="popup-form-btn">
                            <button type="submit" class="btn" id="locationsubmit">Send</button>
                        </div>
                    </div>
                    <h3 align="center">Popular Cities</h3>
                    <div class="city-icons">
                        <a
                            href="{{ url('enterlocation?locationselect=Chandigarh%2C+India&lat=30.7333148&long=76.7794179') }}&type=<?php if(isset($_GET['type'])) echo($_GET['type']); else echo 0; ?> ">
                            <span class="city-1">
                                <span class="city-i-img"><img src="{{ url('assets/front/img/chandigarh.png') }}" alt=""
                                        title="" /></span>
                                <h2>chandigarh</h2>
                            </span>
                        </a>
                        <a
                            href="{{ url('enterlocation?locationselect=Mumbai%2C+Maharashtra%2C+India&lat=19.0759837&long=72.87765590000004') }}&type=<?php if(isset($_GET['type'])) echo($_GET['type']); else echo 0; ?>">
                            <span class="city-1">
                                <span class="city-i-img"><img src="{{url('assets/front/img/mumbai.png')}}" alt=""
                                        title="" /></span>
                                <h2>Mumbai</h2>
                            </span>
                        </a>
                        <a
                            href="{{ url('enterlocation?locationselect=Delhi%2C+India&lat=28.68627380000001&long=77.22178310000004') }}&type=<?php if(isset($_GET['type'])) echo($_GET['type']); else echo 0; ?> ">
                            <span class="city-1">
                                <span class="city-i-img"><img src="{{url('assets/front/img/ncr.png')}}" alt=""
                                        title="" /></span>
                                <h2>NCR</h2>
                            </span>
                        </a>
                        <a
                            href="{{ url('enterlocation?locationselect=Bengaluru%2C+Karnataka%2C+India&lat=12.9715987&long=77.59456269999998') }}&type=<?php if(isset($_GET['type'])) echo($_GET['type']); else echo 0; ?> ">
                            <span class="city-1">
                                <span class="city-i-img"><img src="{{url('assets/front/img/bengaluru.png')}}" alt=""
                                        title="" /></span>
                                <h2>Bengaluru</h2>
                            </span>
                        </a>
                        <a
                            href="{{ url('enterlocation?locationselect=Ahmedabad%2C+Gujarat%2C+India&lat=23.022505&long=72.57136209999999') }}&type=<?php if(isset($_GET['type'])) echo($_GET['type']); else echo 0; ?>">
                            <span class="city-1">
                                <span class="city-i-img"><img src="{{url('assets/front/img/ahmedabad.png')}}" alt=""
                                        title="" /></span>
                                <h2>Ahmedabad</h2>
                            </span>
                        </a>
                    </div>
                </form>
            </div>
        </div>
        <!-- Popup Div Ends Here -->
    </div>
    @endif
    <div id="covergr"></div>
    <!-- Starting of Header area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="top-column-left">
                        <ul>
                            <li>
                                <i class="fa fa-envelope"></i> {{$gs->email}}
                            </li>
                            <li>
                                <i class="fa fa-phone"></i> {{$gs->phone}}
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="top-column-right">
                        <ul class="top-social-links">
                            <li class="top-social-links-li">
                                @if($sl->f_status == 1)
                                <a href="{{$sl->facebook}}"><i class="fa fa-facebook"></i></a>
                                @endif
                                @if($sl->t_status == 1)
                                <a href="{{$sl->twitter}}"><i class="fa fa-twitter"></i></a>
                                @endif
                                @if($sl->l_status == 1)
                                <a href="{{$sl->linkedin}}"><i class="fa fa-linkedin"></i></a>
                                @endif
                                @if($sl->g_status == 1)
                                <a href="{{$sl->gplus}}"><i class="fa fa-google"></i></a>
                                @endif
                            </li>
                            @if(Auth::guard('user')->check())
                            @else
                            <li><a href="{{route('user-login')}}">{{$lang->signIn}}</a></li>
                            <li><a href="{{route('user-register')}}">{{$lang->signUp}}</a></li>
                            <!-- <li><a href="#" data-toggle="modal" data-target="#registerModal">{{$lang->signUp}}</a></li> -->
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-area-wrapper">
        <div class="container">
            <div class="navbar-header">
                <div class="logo">
                    @if(Auth::guard('user')->check())
                    <a href="{{route('front.social')}}">
                        <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                    </a>
                    @else
                    <a href="{{route('front.index')}}">
                        <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                    </a>
                    @endif
                </div>
                <div id="mobile-menu-wrap"></div>
            </div>
            <div class="mainmenu">
                @if(Auth::guard('user')->check())
                <?php $user = Auth::guard('user')->check();
                $user = Auth::guard('user')->user();
                $check_user_type = $user->user_role_id;
                if ($check_user_type == 1) {
                    //{{route('user-social')}}
                ?>
                <ul id="menuResponsive">
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}?type=draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}?type=ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}?type=aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}?type=contactus">Contact Us</a></li>
                    @endif
                    <li style="min-width: 100px;text-align: right;margin-left: 20px;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span> {{ $user->username }}
                        </a>
                        <div class="dropdown-menu" style="background: #000;" aria-labelledby="navbarDropdownMenuLink">
                            <div><a class="dropdown-item" href="{{route('front.social')}}">Newsfeed</a></div>
                            <div><a class="dropdown-item" href="{{url('social/profile/'. $user->username)}}">My
                                    Profile</a></div>
                            <div><a class="dropdown-item" href="{{route('user-logout')}}">Log Out</a></div>
                        </div>
                    </li>
                    <!--  <li style="min-width: 100px;text-align: right;margin-left: 20px;">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       <span class="glyphicon glyphicon-user"></span> {{ $user->username }} 
                                    </a>
                                    <div class="dropdown-menu" style="background: #000;" aria-labelledby="navbarDropdownMenuLink">
                                        <div><a class="dropdown-item" href="{{route('front.social')}}">My Profile</a></div>
                                        <div><a class="dropdown-item" href="{{route('user-logout')}}">Log Out</a></div>
                                    </div>
                                </li> -->
                </ul>
                {{--
                         <ul id="menuResponsive">
											 <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A </a></li>
                <li><a href="{{route('user-profile')}}">CORE PROFILE</a></li>
                <li class="menuLi"><a style="cursor: pointer;"> ARTICLES BLOGGING <i class="fa fa-angle-down"></i></a>
                    <ul class="menuUl">
                        <li><a href="{{route('front.blog')}}">All Articles Blogs</a></li>
                        <li><a href="{{route('user-blogs-index')}}">Create New Articles</a></li>
                    </ul>
                <li><a href="">CREATE AGENCY</a></li>
                <li><a href="{{route('user-dashboard')}}">{{$lang->dashboard}}</a></li>
                <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                <li><a href="{{route('user-q2a')}}">Search</a></li>
                <li><a href="{{route('user-q2a')}}">Social Profile</a></li>
                <li class="sign-in"><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                <li class="menuLi"><a style="cursor: pointer;">ADVOCATE {{$lang->dashboard}} <i
                            class="fa fa-angle-down"></i></a>
                    <ul class="menuUl">
                    </ul>
                    --}}
                </li>
                </ul>
                <?php } else if ($check_user_type == 2) {
                ?>
                <ul id="menuResponsive">
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}?type=draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}?type=ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}?type=aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}?type=contactus">Contact Us</a></li>
                    @endif
                    <li style="min-width: 100px;text-align: right;margin-left: 20px;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span> {{ $user->username }}
                        </a>
                        <div class="dropdown-menu" style="background: #000;" aria-labelledby="navbarDropdownMenuLink">
                            <div><a class="dropdown-item" href="{{route('front.social')}}">Newsfeed</a></div>
                            <div><a class="dropdown-item" href="{{url('social/profile/'. $user->username)}}">My
                                    Profile</a></div>
                            <div><a class="dropdown-item" href="{{route('user-logout')}}">Log Out</a></div>
                        </div>
                    </li>
                </ul>
                {{--
                        <ul id="menuResponsive">
                                        <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                <li><a href="">WIKI MEDIA</a></li>
                <li class="menuLi"><a style="cursor: pointer;"> ARTICLES BLOGGING <i class="fa fa-angle-down"></i></a>
                    <ul class="menuUl">
                        <li><a href="{{route('front.blog')}}">All Articles Blogs</a></li>
                        <li><a href="{{route('user-blogs-index')}}">Create New Articles</a></li>
                    </ul>
                </li>
                <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A </a></li>
                <li><a href="{{route('user-q2a')}}">MY SOCIAL NETWORK</a></li>
                <li class="menuLi"><a style="cursor: pointer;">LAW STUDENT {{$lang->dashboard}} <i
                            class="fa fa-angle-down"></i></a>
                    <ul class="menuUl">
                        <li><a href="{{route('user-dashboard')}}">{{$lang->dashboard}}</a></li>
                        <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li>
                        <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                        <li class="sign-in"><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                    </ul>
                    --}}
                </li>
                </ul>
                <?php
                } else if ($check_user_type == 3) {
                ?>
                <ul id="menuResponsive">
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}?type=draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}?type=ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}?type=aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}?type=contactus">Contact Us</a></li>
                    @endif
                    <li style="min-width: 100px;text-align: right;margin-left: 20px;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span> {{ $user->username }}
                        </a>
                        <div class="dropdown-menu" style="background: #000;" aria-labelledby="navbarDropdownMenuLink">
                            <div><a class="dropdown-item" href="{{route('front.social')}}">Newsfeed</a></div>
                            <div><a class="dropdown-item" href="{{url('social/profile/'. $user->username)}}">My
                                    Profile</a></div>
                            <div><a class="dropdown-item" href="{{route('user-logout')}}">Log Out</a></div>
                        </div>
                    </li>
                </ul>
                <?php
                } else {
                ?>
                <ul id="menuResponsive">
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=searchlawyers">Find a lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=post-question">Ask a Lawyer</a></li>
                    <li><a href="{{ URL::to('/') }}?type=knowhelaw">Know the law</a></li>
                    <li><a href="{{ URL::to('/') }}?type=draftcase">Draft case</a></li>
                    <li><a href="{{ URL::to('/') }}?type=ourblogs">Our Blogs</a></li>
                    <li><a href="{{ URL::to('/') }}?type=aboutus">About Us</a></li>
                    <li><a href="{{ URL::to('/') }}?type=contactus">Contact Us</a></li>
                    @endif
                    <li style="min-width: 100px;text-align: right;margin-left: 20px;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span> {{ $user->username }}
                        </a>
                        <div class="dropdown-menu" style="background: #000;" aria-labelledby="navbarDropdownMenuLink">
                            <div><a class="dropdown-item" href="{{route('front.social')}}">Newsfeed</a></div>
                            <div><a class="dropdown-item" href="{{url('social/profile/'. $user->username)}}">My
                                    Profile</a></div>
                            <div><a class="dropdown-item" href="{{route('user-logout')}}">Log Out</a></div>
                        </div>
                    </li>
                </ul>
                <?php } ?>
                @else
                <ul id="menuResponsive">
                    @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                    @endif
                    {{-- <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                    @if(Auth::guard('user')->check())
                    <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li>
                    @else
                    <li><a href="{{route('front.logginplease')}}">{{$lang->blog}}</a></li>
                    @endif
                    <li><a href="{{route('front.index')}}">SEARCH</a></li>
                    --}}
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#searchlawyers">Find a lawyer</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=searchlawyers">Find a lawyer</a></li>
                    @endif
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=post-question">Ask a Lawyer</a></li>
                    @endif
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=knowhelaw">Know the law</a></li>
                    @endif
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=draftcase">Draft case</a></li>
                    @endif
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=ourblogs">Our Blogs</a></li>
                    @endif
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=aboutus">About Us</a></li>
                    @endif
                    @if(Request::url() == URL::to('/') || strpos(Request::url(), 'enterlocation') !== false)
                    <li><a href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                    @else
                    <li><a href="{{ URL::to('/') }}?type=contactus">Contact Us</a></li>
                    @endif
                    <!--   <li class="sign-in"><a href="{{route('user-login')}}">Sign In</a></li> -->
                    <li class="sign-in"><a id="loginbtn" href="#" data-toggle="modal" data-target="#signInModal">Sign
                            In</a></li>
                    <!-- <li class="sign-up"><a href="{{route('user-register')}}">Sign Up</a></li> -->
                    <li class="sign-up"><a id="signupbtn" href="#" data-toggle="modal" data-target="#registerModal">Sign
                            Up</a></li>
                    <!-- <li class="sign-in"><a href="#" data-toggle="modal" data-target="#signInModal">Sign In</a></li>
                    <li class="sign-up"><a href="#" data-toggle="modal" data-target="#registerModal">Sign Up</a></li> -->
                    {{-- <li><a href="{{route('front.featured')}}">{{$lang->fh}}</a></li>
                    <li><a href="{{route('front.users')}}">{{$lang->h}}</a></li>
                    @if($ps->a_status == 1)
                    <li><a href="{{route('front.about')}}">{{$lang->about}}</a></li>
                    @endif
                    <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li>
                    @if($ps->f_status == 1)
                    <li><a href="{{route('front.faq')}}">{{$lang->faq}}</a></li>
                    @endif
                    @if($ps->c_status == 1)
                    <li><a href="{{route('front.contact')}}">{{$lang->contact}}</a></li>
                    @endif --}}
                    @endif
                </ul>
            </div>
        </div>
    </div>
    <div class="ls-spacer"></div>
    <!-- Ending of Header area -->
    <!-- Starting of Hero area -->
    @yield('content')
    <!-- starting of subscribe newsletter area -->
    <div class="subscribe-newsletter-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="subscribe-newsletter-area">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                <h4>{{$lang->ston}}</h4>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                                <form action="{{route('front.subscribe.submit')}}" method="POST">
                                    {{csrf_field()}}
                                    <input type="email" name="email" placeholder="{{$lang->supl}}" required>
                                    <button type="submit" class="btn">{{$lang->s}}</button>
                                </form>
                                <p>
                                    @if(Session::has('subscribe'))
                                    {{ Session::get('subscribe') }}
                                    @endif
                                    @foreach($errors->all() as $error)
                                    {{$error}}
                                    @endforeach
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('report_message'))
    <div class="email_report">
        {{ Session::get('report_message') }}
    </div>
    @endif
    @include('layouts.footer')