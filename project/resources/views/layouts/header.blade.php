<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="{{$seo->meta_keys}}">
        <meta name="author" content="GeniusOcean">
        <title>{{$gs->title}}</title>
        <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/owl.carousel.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/slicknav.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/fonts/avenir/stylesheet.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">
        <link rel="icon" type="image/png" href="{{asset('assets/images/'.$gs->favicon)}}">
        <script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAp613NfAwqpgpzboBMgj1eoHTZOzglGAA&libraries=places"></script> -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgriKllmz3R9e7ORquSKDyASj_DP4csuo&libraries=places"></script>
       
     
        @include('styles.design')
        @yield('styles')
    @if(Auth::guard('user')->check())

    @endif
<style>
.has-search .form-control {
    padding-left: 2.375rem;
}
.has-search .form-control-feedback {
    position: absolute;
    z-index: 9;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}
.btn.btn-secondary {
	background: transparent;
	border: none !important;
	right: 8px;
	top: 5px;
	width: 8%;
	cursor: pointer !important;
	height: 39px;
}
.btn-secondary img {
    max-width: none;
}
</style>
	
    </head>
    <body>

<!-- Starting of Header area -->
        <div class="header-top-area">
            <div class="container">

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="top-column-left">
                            <ul>
                                <li>
                                    <i class="fa fa-envelope"></i> {{$gs->email}}
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i> {{$gs->phone}}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="top-column-right">
                            <ul class="top-social-links">
                                <li class="top-social-links-li">
                                    @if($sl->f_status == 1)
                                    <a href="{{$sl->facebook}}"><i class="fa fa-facebook"></i></a>
                                    @endif
                  
  
                   @if($sl->t_status == 1)
                                    <a href="{{$sl->twitter}}"><i class="fa fa-twitter"></i></a>
                                    @endif
                                    @if($sl->l_status == 1)
                                    <a href="{{$sl->linkedin}}"><i class="fa fa-linkedin"></i></a>
                                    @endif
                                    @if($sl->g_status == 1)
                                    <a href="{{$sl->gplus}}"><i class="fa fa-google"></i></a>
                                    @endif
                                </li>
                         @if(Auth::guard('user')->check())   
                         @else
                                <li><a href="{{route('user-login')}}">{{$lang->signIn}}</a></li>
                                <li><a href="{{route('user-register')}}">{{$lang->signUp}}</a></li>
                                <!-- <li><a href="#" data-toggle="modal" data-target="#registerModal">{{$lang->signUp}}</a></li> -->

                         @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-area-wrapper">
            <div class="container">
                    <div class="navbar-header">
                        <div class="logo">
                            
                             @if(Auth::guard('user')->check())  
                            <a href="{{route('front.social')}}">
                                <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                            </a>
                            @else
                             <a href="{{route('front.index')}}">
                                <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                            </a>
                            
                            @endif
                           
                        </div>
                        <div id="mobile-menu-wrap"></div>
                    </div>
                        <div class="mainmenu">

                            
                    @if(Auth::guard('user')->check())  

                    <?php $user=Auth::guard('user')->check(); 

                    $user = Auth::guard('user')->user();

                    $check_user_type=$user->user_role_id;

                    if( $check_user_type==0){
//{{route('user-social')}}
                    ?>
                         <ul id="menuResponsive">
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                        
                                        {{-- 
											
											<li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
											 <li><a href="{{route('user-profile')}}">CORE PROFILE</a></li>
											 <li class="menuLi"><a style="cursor: pointer;"> ARTICLES BLOGGING <i class="fa fa-angle-down"></i></a>
                                        <ul class="menuUl">
											 <li><a href="{{route('front.blog')}}">All Articles Blogs</a></li>
											<li><a href="{{route('user-blogs-index')}}">Create New Articles</a></li>
                                        </ul>
										
										<li><a href="">CREATE AGENCY</a></li> 
										 
                                        <li><a href="{{route('user-dashboard')}}">{{$lang->dashboard}}</a></li>
                                        <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                        
                                        <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
										 --}}
                                         <li><a href="{{route('user-q2a')}}">Search</a></li>
                                         <li><a href="{{route('user-q2a')}}">Social Profile</a></li>
                                         <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                          
                                        
                                       {{--   <li class="menuLi"><a style="cursor: pointer;">ADVOCATE {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a> --}}
										 
										<ul class="menuUl">
                                    </ul>
                                    </li>
                              
                        </ul>
                    <?php }else if($check_user_type==2){

                        ?> 
                        <ul id="menuResponsive">
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                     <li><a href="">WIKI MEDIA</a></li>
                                   
                                   {{--    
                                        <li class="menuLi"><a style="cursor: pointer;"> ARTICLES BLOGGING <i class="fa fa-angle-down"></i></a>
                                        <ul class="menuUl">
											 <li><a href="{{route('front.blog')}}">All Articles Blogs</a></li>
											<li><a href="{{route('user-blogs-index')}}">Create New Articles</a></li>
                                        </ul>
											
                                        </li>
                                    --}}
                                         <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
                                         <li><a href="{{route('user-q2a')}}" >MY SOCIAL NETWORK</a></li>
                                        
                                          <li class="menuLi"><a style="cursor: pointer;">LAW STUDENT {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a>
                                     <ul class="menuUl">
										 
                                      {{--   <li><a href="{{route('user-dashboard')}}">{{$lang->dashboard}}</a></li> 
                                        <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li> --}}
                                        <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                        <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                    </ul>
                                    </li>
								                            
                               
                               
                        </ul>

                        <?php
                    }else if($check_user_type==3){
						
						?> 
						
						<ul id="menuResponsive">
                                     
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                     
                                     <li><a href="{{route('user-dashboard')}}">SEARCH LAYWERS</a></li>
                                         
                                     {{-- <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li> --}}
									 <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
									 <li><a href="{{route('user-q2a')}}">GO SOCIAL	</a></li>
									
									
									 <li class="menuLi"><a style="cursor: pointer;">PROFESSOR {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a>
									 
									 <ul class="menuUl">
										 
										 {{-- <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li> --}}
										 <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
										 <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                     </ul>
                                    
                                    
                                    </li>
                                 
                        </ul>
						<?php
						}
                    else{
						
						?>
                        <ul id="menuResponsive">
                                     
                                     <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li>
                                     
                                     <li><a href="{{route('user-dashboard')}}">SEARCH LAYWERS</a></li>
                                         
                                     {{-- <li><a href="{{route('user-profile')}}">{{$lang->edit}}</a></li> --}}
									 <li><a href="{{route('user-q2a')}}" target="_blank">ASK Q&A  </a></li>
									 <li><a href="{{route('user-q2a')}}">GO SOCIAL	</a></li>
									
									
									<li class="menuLi"><a style="cursor: pointer;">COMMONERS {{$lang->dashboard}} <i class="fa fa-angle-down"></i></a>
									<ul class="menuUl">
										 
                                     {{-- <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li> --}}
                                     <li><a href="{{route('user-reset')}}">{{$lang->reset}}</a></li>
                                     <li><a href="{{route('user-logout')}}">{{$lang->logout}}</a></li>
                                    </ul>
                                    </li>
                                 
                        </ul>

                    <?php } ?>
                        @else
                        <ul id="menuResponsive">
							
							
							
							
							
							 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
								
								
								 
								
								@endif
							
								 
							
                               {{-- <li><a href="{{route('front.index')}}">{{$lang->home}}</a></li> 
								   
								     @if(Auth::guard('user')->check())  
                                <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li>
                                
                                 @else
                                
                                 <li><a href="{{route('front.logginplease')}}">{{$lang->blog}}</a></li>
								 @endif
                                <li><a href="{{route('front.index')}}">SEARCH</a></li>
								   --}}
								 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                <li><a href="{{ URL::to('/') }}#searchlawyers" >Find a lawyer</a></li>
                                @else
                                <li><a class="header-menu-link" href="{{ URL::to('/') }}#searchlawyers" >Find a lawyer</a></li>
                               
                                @endif
                                
                                 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                <li><a href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                                
                                @else
                                <li><a class="header-menu-link" href="{{ URL::to('/') }}#post-question">Ask a Lawyer</a></li>
                                 @endif
                                 
                                 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                 <li><a href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                                 @else
                                 <li><a class="header-menu-link" href="{{ URL::to('/') }}#knowhelaw">Know the law</a></li>
                                 @endif
                                 
                                 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                <li><a href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                                @else
                                <li><a class="header-menu-link" href="{{ URL::to('/') }}#draftcase">Draft case</a></li>
                                 @endif
                                
                                 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                <li><a href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                                @else
                                <li><a class="header-menu-link" href="{{ URL::to('/') }}#ourblogs">Our Blogs</a></li>
                                 @endif
                                 
                                 
                                 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                <li><a href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                                
                                 @else
                                <li><a class="header-menu-link" onclick="return test()" href="{{ URL::to('/') }}#aboutus">About Us</a></li>
                                 @endif
                                
                                 @if(url()->current()==url('lawyer/login') || url()->current()==url('lawyer/register'))
                                <li><a href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                                @else
                                <li><a class="header-menu-link" href="{{ URL::to('/') }}#contactus">Contact Us</a></li>
                                 @endif
                                
                                <li class="sign-in"><a href="{{route('user-login')}}">Sign In</a></li>
                                <li class="sign-up"><a href="{{route('user-register')}}">Sign Up</a></li>
                                <!-- <li class="sign-in"><a href="#" data-toggle="modal" data-target="#signInModal">Sign In</a></li>
                                <li class="sign-up"><a href="#" data-toggle="modal" data-target="#registerModal">Sign Up</a></li> -->
                                {{--  <li><a href="{{route('front.featured')}}">{{$lang->fh}}</a></li>
                                <li><a href="{{route('front.users')}}">{{$lang->h}}</a></li>

                                @if($ps->a_status == 1)
                                <li><a href="{{route('front.about')}}">{{$lang->about}}</a></li>
                                @endif
                                <li><a href="{{route('front.blog')}}">{{$lang->blog}}</a></li>
                                @if($ps->f_status == 1)
                                <li><a href="{{route('front.faq')}}">{{$lang->faq}}</a></li>
                                @endif
                                @if($ps->c_status == 1)
                                <li><a href="{{route('front.contact')}}">{{$lang->contact}}</a></li>
                                @endif --}}

                         @endif
                            </ul>
                            
 
                        </div>
            </div>
        </div>
        <div class="las-spacer"></div>  