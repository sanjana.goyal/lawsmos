<!-- Section: Sidebar -->
<section class="home-filters-sidebar">
    <!-- Section: Filters -->
    <section class="mb-4">            
        @if($allcities)
            <h5 class="font-weight-bold mb-3">Select City</h5>
            <div class="form-select pl-0 mb-3">
                <select class="filter_cities">
                    @foreach($allcities as $cityname)
                        <option value="{{ $cityname->name }}" <?php echo ($cities[0]==$cityname->name) ? "selected" : ""; ?>>{{ $cityname->name }}</option>
                    @endforeach
                </select>
            </div>
        @endif
    </section>
    <section class="mb-4">
        @if($cats)
            <h5 class="font-weight-bold mb-3">Select Categories</h5>
            @foreach($cats as $cat)
                <div class="form-check pl-0 mb-3">
                    <input type="checkbox" class="form-check-input filled-in" id="{{$cat->cat_name}}" value="{{$cat->cat_name}}">
                    <label class="form-check-label small text-uppercase card-link-secondary" for="new">{{$cat->cat_name}}</label>
                </div>
            @endforeach
        @endif
    </section>
    <section class="mb-4">
        <h5 class="font-weight-bold mb-3">Price</h5>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="under1000" name="pricefilter">
            <label class="form-check-label small text-uppercase card-link-secondary" for="under1000">Under 1000</label>
        </div>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="under2000" name="pricefilter">
            <label class="form-check-label small text-uppercase card-link-secondary" for="under2000">Under 2000<label>
        </div>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="under5000" name="pricefilter">
            <label class="form-check-label small text-uppercase card-link-secondary" for="under5000">Under 5000</label>
        </div>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="above5000" name="pricefilter">
            <label class="form-check-label small text-uppercase card-link-secondary" for="above5000">5000 and above</label>
        </div>
    </section>
    <section class="mb-4">
        <h5 class="font-weight-bold mb-3">Availablity</h5>
        <div class="form-check pl-0 mb-3">
            <input type="checkbox" class="form-check-input" id="availablity" name="availablity">
            <label class="form-check-label small text-uppercase card-link-secondary" for="availablity">Current Availability</label>
        </div>
    </section>    
    <section class="mb-4">
        <h5 class="font-weight-bold mb-3">Gender</h5>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="male" name="gender">
            <label class="form-check-label small text-uppercase card-link-secondary" for="male">Male</label>
        </div>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="female" name="gender">
            <label class="form-check-label small text-uppercase card-link-secondary" for="female">Female</label>
        </div>
        <div class="form-check pl-0 mb-3">
            <input type="radio" class="form-check-input" id="others" name="gender">
            <label class="form-check-label small text-uppercase card-link-secondary" for="others">Others</label>
        </div>
    </section>
    <section class="mb-4">
        <h5 class="font-weight-bold mb-3">Top Rated</h5>
        <div class="form-check pl-0 mb-3">
            <input type="checkbox" class="form-check-input" id="highrated" name="highrated">
            <label class="form-check-label small text-uppercase card-link-secondary" for="highrated">Above 4 star Rated</label>
        </div>
    </section>    
    <!-- Section: Filters -->
</section>
<!-- Section: Sidebar -->