<style type="text/css">
	
    #cover {
        background: url({{asset('assets/images/'.$gs->loader)}}) no-repeat scroll center center #FFF;
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 9999;
        }

/*Handyman color: #06357a*/
.section-borders span.black-border {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.header-top-area {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.header-top-area .top-social-links li a:hover {color: {{$gs->colors == null ? '#06357a':$gs->colors}};}

.header-top-area .top-social-links .top-social-links-li a:hover {color: {{$gs->colors == null ? '#06357a':$gs->colors}};}

.header-area-wrapper {
    border-top: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
    border-bottom: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.mainmenu li ul {
    border-top: 3px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.mainmenu li ul li {
    border-bottom: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.slicknav_btn {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}}; 
}

.slicknav_nav {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}}; 

}
.slicknav_nav .slicknav_row:hover, .slicknav_nav a:hover {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}}; 
}

.hero-form {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}}ad;
}

.hero-btn {background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};}

.profile-contact-area .btn {background: {{$gs->colors == null ? '#06357a':$gs->colors}}; border-color: {{$gs->colors == null ? '#06357a':$gs->colors}};}

.team_style2 .team_common{
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.team_style2 .member_info .overlay2 {
    background: {{$gs->colors == null ? '#06357a':$gs->colors}} none repeat scroll 0 0; 
}
.testimonial-wrapper .section-borders span.black-border {background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};}
.testimonial-wrapper .owl-prev,
.testimonial-wrapper .owl-next {
    box-shadow: 0 0 1px {{$gs->colors == null ? '#06357a':$gs->colors}};
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.single-blog-box {
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.boxed-btn.blog {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}}; 
}
.boxed-btn.blog:hover {color: {{$gs->colors == null ? '#06357a':$gs->colors}}; border-color: {{$gs->colors == null ? '#06357a':$gs->colors}};}
.blog-area-wrapper .owl-carousel .owl-nav .owl-prev,
.blog-area-wrapper .owl-carousel .owl-nav .owl-next {
    color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.post-heading {border-bottom: 2px solid {{$gs->colors == null ? '#06357a':$gs->colors}};}

.post-sidebar-area li a:hover {color: {{$gs->colors == null ? '#06357a':$gs->colors}};}
.single-all-blogs-box {
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.comments-form input[type="submit"], 
.comments-form button[type="submit"]  {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.comments-form input[type="submit"]:hover, 
button[type="submit"]:hover {
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
    color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.contact-info i.fa {
    color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.styled-faq .panel-title {background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};}

.subscribe-newsletter-area {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.login-form {
    border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.login-icon {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.login-title {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}
.login-area .section-borders span {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.login-form .input-group-addon {
    color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

.login-btn {
    background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

            .pagination>.disabled>a:focus, 
            .pagination>.disabled>a:hover, 
            .pagination>.disabled>span, 
            .pagination>.disabled>span:focus, 
            .pagination>.disabled>span:hover {
                color: {{$gs->colors == null ? '#06357a':$gs->colors}};
            }

            .pagination>.disabled>a, 
            .pagination>.disabled>a:focus, 
            .pagination>.disabled>a:hover, 
            .pagination>.disabled>span, 
            .pagination>.disabled>span:focus, 
            .pagination>.disabled>span:hover {
                border-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
            }

            .pagination>.active>a, 
            .pagination>.active>a:focus, 
            .pagination>.active>a:hover, 
            .pagination>.active>span, 
            .pagination>.active>span:focus, 
            .pagination>.active>span:hover {
                background-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
                border-color: {{$gs->colors == null ? '#06357a':$gs->colors}};

            }
            .pagination>li>a, .pagination>li>span {
                border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};

            }
            .pagination>li>a, .pagination>li>span {
                color: {{$gs->colors == null ? '#06357a':$gs->colors}};
                border: 1px solid {{$gs->colors == null ? '#06357a':$gs->colors}};
            }
.pagination>li>a:focus, .pagination>li>a:hover, .pagination>li>span:focus, .pagination>li>span:hover {
    border-color: {{$gs->colors == null ? '#06357a':$gs->colors}};
}

</style>