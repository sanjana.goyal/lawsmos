@include('layouts.header')


<section class="login-area">
            <div class="container">
                <div class="form-wrapper-p forget-pass-wrap">
                    <div class="left-box-p">
                            <h2>Forgot Password</h2>
                                <p>Please enter your email! </p>
                            </div>
                  
                        <div class="login-form sign-in-form-wrap">
<!--
                            <div class="login-icon"><i class="fa fa-user"></i></div>
                            
                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
-->
<!--
                            
                            <div class="login-title text-center">{{$lang->fpt}}</div>
-->

                            <form action="{{route('user-forgot-submit')}}" method="POST">
                              {{csrf_field()}}
                              @include('includes.form-success')
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-envelope"></i>
                                  </div>
                                  <input name="email" class="form-control" placeholder="{{$lang->fpe}}" type="email">
                                </div>
                              </div>
                              <div class="form-group text-center">
                                    <button type="submit" class="btn login-btn" name="button">{{$lang->fpb}}</button>
                              </div>
                            </form>
                        </div>
                   
                </div>
            </div>
        </section>

