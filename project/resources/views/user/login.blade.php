@include('layouts.header')
<style>
    .verification-login-message {
    text-align: center;
    color: #fff;
    background: #00000078;
    padding: 10px;
    }.verification-login-message a {
    color: #fff;
    font-weight: 700;
    text-decoration: underline;
    }
</style>
<section class="login-area">
    <div class="container">
        <div class="form-wrapper-p">
            <div class="verification-message">
                <?php //var_dump(redirect()->back()->getTargetUrl()); ?>
                @php 
                    $successdisplay="none";
                    $errordisplay="none"; 
                @endphp
                @if($userkeyarray)
                    @foreach($userkeyarray as $user)
                        @if($user->user_verified == 0)
                            @php 
                                $errordisplay="block"; 
                            @endphp
                        @else                            
                            @php 
                                $successdisplay="block"; 
                            @endphp
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="login-content-area">
                <div class="left-box-p">
                    <h2>Sign In</h2>
                    <p>Please fill in this form to Log In! </p>
                </div>
                <div class="login-form sign-in-form-wrap">
                    <!--
                        <div class="login-icon"><i class="fa fa-user"></i></div>
                        
                        <div class="section-borders">
                            <span></span>
                            <span class="black-border"></span>
                            <span></span>
                        </div>
                        
                        <div class="login-title text-center">{{$lang->signin}}</div>
                        -->
                    <form action="{{route('user-login-submit')}}" method="POST" class="loginform">
                        {{csrf_field()}}
                        @include('includes.form-error')
                        @include('includes.form-success')
                        <!-- <div class="alert alert-success" style="display: {{$successdisplay}}">Your account is Verified Successfully.</div>
                        <div class="alert alert-danger"  style="display: {{$errordisplay}}">Please check your email to verify your account.
                        </div> -->
                        <?php 
                            if(isset($_SERVER['HTTP_REFERER'])){
                            
                            $page =  $_SERVER['HTTP_REFERER'];
                            
                            $_SESSION['page'] = $page;
                            }
                            else
                            {
                            
                            $_SESSION['page']="";
                            }
                            //echo "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
                            ?>
                        <input type="hidden" name="previous_urlss" value="{{ Session::get('questionsssss') }}">
                        <input type="hidden" name="previous_url" value="<?php echo $_SESSION['page']; ?>">
                        <input  name="caseheading" class="form-control" value="{{ session()->get('caseheading') }}" type="hidden">
                        <textarea  name="casecontent" class="form-control"  style="display:none;">{{ session()->get('casecontent') }}</textarea>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input name="email" class="form-control" placeholder="{{$lang->sie}}" type="email" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-unlock-alt"></i>
                                </div>
                                <input class="form-control" name="password" id="password" placeholder="{{$lang->spe}}" type="password" required="">
                                <span class="input-group-addon"><i id="pass_show"  class="fa fa-eye"></i> </span>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn login-btn user-login-btn" name="button">{{$lang->signin}}</button>
                        </div>
                        <div class="form-group text-center">
                            <a href="{{route('user-forgot')}}">{{$lang->fpw}}</a>
                            &nbsp;
                            <a href="{{route('user-register')}}">{{$lang->cn}}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    // $('body').on('click', '.user-login-btn', function (e) {
    //     e.preventDefault();
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    //         }
    //     });
    //     $('.alert-danger').hide();
    //     $('.alert-success').hide();
    //     var parentClass = $(this).parents('.loginform');
    //     var ajaxUrl = "{{ url('lawyer/login_ajax') }}";
    //     // var ajaxUrl = parentClass.attr("action");
    //     var inputemail = parentClass.find('input[name="email"]').val();
    //     var inputpassword = parentClass.find('input[name="password"]').val();
    //     $.ajax({
    //         url: ajaxUrl,
    //         method: 'POST',
    //         data: {
    //             _token: "{{ csrf_token() }}",
    //             email: inputemail,
    //             password: inputpassword,
    //         },
    //         dataType:"json",
    //         success: function (result) {
    //             console.log(result);
    //             // if (result.error) {
    //             //     parentClass.find('.alert-danger').show();
    //             //     parentClass.find('.alert-danger').html(result.error);
    //             // }else{
    //                 // parentClass.find('.alert-success').html("You have been successfully logged in.");
    //                 // parentClass.find('.alert-success').show();
    //                 // $('html, body').animate({
    //                 //     scrollTop: parentClass.find('.alert-success').offset().top - 120
    //                 // }, 500);
    //                 $('.loginform').submit();
    //                 // window.location = result.url;
    //             // }
    //         },
    //         error: function (result) {
    //             console.log(result);
    //             var response = JSON.parse(result.responseText);
    //             $.each(response, function (key, val) {
    //                 parentClass.find('.alert-danger').show();
    //                 parentClass.find('.alert-success').hide();
    //                 var div_id = parentClass.find('.alert-danger').attr('id');
    //                 parentClass.find('.alert-danger').html(all_values(val, div_id));
    //                 $('html, body').animate({
    //                     scrollTop: parentClass.find('.alert-danger').offset().top - 120
    //                 }, 500);
    //             });
    //         }
    //     });
    // });

    $('#pass_show').click(function(){
      var pass_inp = $('#password').val();
    
      if(pass_inp.length > 0){
          if($('#password').attr('type') == "password"){
              $("#password").attr("type", "text");
              $("#pass_show").addClass("fa-eye-slash");
            }
            else{
                $("#password").attr("type", "password");
                $("#pass_show").removeClass("fa-eye-slash");
                $("#pass_show").addClass("fa fa-eye");
                
            }
      }  
      else{
        $("#password").attr("type", "password");
        $("#pass_show").removeClass("fa-eye-slash");
        $("#pass_show").addClass("fa fa-eye");
      }
    
    
      // if($('#password').attr('type') == "password"){
      //   $("#password").attr("type", "text");
      //   $("#pass_show").addClass("fa-eye-slash");
      // }else{
      //   $("#password").attr("type", "password");
      //   $("#pass_show").removeClass("fa-eye-slash");
      //   $("#pass_show").addClass("fa fa-eye");
      // }
                    
      
     
    
    });
    
</script>