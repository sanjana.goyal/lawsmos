@include('layouts.header')

<section class="login-banner registration-banner-style">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-12">
                <div class="banner-content">
                    <h1>10,000 Lawyers are helping 15 Lac users every month at Lawsmos</h1>
                    <p>Join India's leading lawyer network and grow your practice</p>
                </div>
            </div>
            <div class="col-md-7 col-sm-12 bannerfoouter">
                <div class="banner-form">
                    <h2>Sign Up for free</h2>
                    <div class="login-form no-log-b">
                        <div class="form-resp-wraper">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="dropdown">
                                    <a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="false">Select Registration Type<span class="caret"></span></a>
                                    <ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
                                        <li class="active"><a href="#dropdown2" role="tab" id="dropdown2-tab" data-toggle="tab" aria-controls="dropdown2" aria-expanded="false">Register as Service Seeker</a></li>
                                        <li class=""><a href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1" aria-expanded="false">Register as Advocate</a></li>
                                        <li class=""><a href="#dropdown3" role="tab" id="dropdown3-tab" data-toggle="tab" aria-controls="dropdown3" aria-expanded="false">Register as law Student</a></li>
                                        <li class=""><a href="#dropdown4" role="tab" id="dropdown4-tab" data-toggle="tab" aria-controls="dropdown4" aria-expanded="false">Register as law Professor</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="sign-up-tab-outer">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#reg-seeker" aria-controls="reg-seeker" role="tab" data-toggle="tab">Service Seeker</a></li>
                                <li role="presentation">
                                    <a href="#reg-advocate" aria-controls="reg-advocate" role="tab" data-toggle="tab">Advocate</a>
                                </li>
                                <li role="presentation"><a href="#reg-student" aria-controls="reg-student" role="tab" data-toggle="tab">Researcher/Student</a></li>
                                <li role="presentation"><a href="#reg-pro" aria-controls="settings" role="tab" data-toggle="tab">Law Professor</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="reg-advocate">
                                    <form action="{{route('user-register-submit')}}" method="POST" id="register-form-advocate" class="formregister">
                                        {{csrf_field()}}
                                        @include('includes.form-error')
                                        <div class="alert alert-success" id="register_form_advocate_div_success" style="display:none"></div>
                                        <div class="alert alert-danger" id="register_form_advocate_div_danger" style="display:none">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input name="caseheading" id="caseheading_reg_advocate" class="form-control" value="{{ session()->get('caseheading') }}" type="hidden">
                                                <textarea name="casecontent" id="casecontent_reg_advocate" class="form-control" style="display:none;">{{ session()->get('casecontent') }}</textarea>
                                                <input type="hidden" name="user_role_id" id="user_role_id_reg_advocate" class="form-control" value="0">
                                                <input name="firstname" id="firstname_reg_advocate" class="form-control" placeholder="First Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input name="lastname" id="lastname_reg_advocate" class="form-control" placeholder="Last Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <select name="gender" id="gender_reg_advocate" class="form-control">
                                                    <option value="">Select gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <input name="email" class="form-control" id="email_reg_advocate" placeholder="{{$lang->sue}}" type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input name="phone" class="form-control" id="phone_reg_advocate" placeholder="{{$lang->suph}}" type="phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" name="password" id="password_reg_advocate" placeholder="{{$lang->sup}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" name="password_confirmation" id="password_confirmation_reg_advocate" placeholder="{{$lang->sucp}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" id="reg_advocate"  class="btn register-btn login-btn" name="button">{{$lang->signup}}</button>
                                        </div>
                                        <div class="form-group text-center">
                                            <a href="{{route('user-login')}}">{{$lang->al}}</a>
                                        </div>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane active" id="reg-seeker">
                                    <form action="{{route('serviceSeeker-register-submit')}}" method="POST" id="register-form-serviceSeeker" class="formregister">
                                        {{csrf_field()}}
                                        @include('includes.form-error')
                                        <div class="alert alert-success" id="register_form_seeker_div_success" style="display:none"></div>
                                        <div class="alert alert-danger" id="register_form_seeker_div_danger" style="display:none"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="hidden" name="user_role_id" id="user_role_id_reg_seeker" class="form-control" value="1">
                                                <input name="firstname" id="firstname_reg_seeker" class="form-control" placeholder="First Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input name="lastname" id="lastname_reg_seeker" class="form-control" placeholder="Last Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <select name="gender" id="gender_reg_seeker" class="form-control">
                                                    <option value="">Select gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <input name="email" id="email_reg_seeker" class="form-control" placeholder="{{$lang->sue}}" type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input name="phone" id="phone_reg_seeker" class="form-control" placeholder="{{$lang->suph}}" type="phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control"  id="password_reg_seeker" name="password" placeholder="{{$lang->sup}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" id="password_confirmation_reg_seeker" name="password_confirmation" placeholder="{{$lang->sucp}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button"  id="reg_seeker" class="btn register-btn login-btn" name="button">{{$lang->signup}}</button>
                                        </div>
                                        <div class="form-group text-center">
                                            <a href="{{route('user-login')}}">{{$lang->al}}</a>
                                        </div>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="reg-student">
                                    <form action="{{route('lawstudent-register-submit')}}" method="POST" id="register-form-law-student" class="formregister">
                                        {{csrf_field()}}
                                        @include('includes.form-error')
                                        <div class="alert alert-success" id="register_form_lawstudent_div_success" style="display:none"></div>
                                        <div class="alert alert-danger" id="register_form_lawstudent_div_danger" style="display:none"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="hidden" name="user_role_id" id="user_role_id_reg_lawstudent" class="form-control" value="2">
                                                <input name="firstname" id="firstname_reg_lawstudent" class="form-control" placeholder="First Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input name="lastname" id="lastname_reg_lawstudent" class="form-control" placeholder="Last Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <select name="gender" id="gender_reg_lawstudent" class="form-control">
                                                    <option value="">Select gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <input name="email" id="email_reg_lawstudent" class="form-control" placeholder="{{$lang->sue}}" type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input name="phone" id="phone_reg_lawstudent" class="form-control" placeholder="{{$lang->suph}}" type="phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" id="password_reg_lawstudent" name="password" placeholder="{{$lang->sup}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" id="password_confirmation_reg_lawstudent" name="password_confirmation" placeholder="{{$lang->sucp}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" id="reg_lawstudent" class="btn register-btn login-btn" name="button">{{$lang->signup}}</button>
                                        </div>
                                        <div class="form-group text-center">
                                            <a href="{{route('user-login')}}">{{$lang->al}}</a>
                                        </div>
                                    </form>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="reg-pro">
                                    <form action="{{route('lawprofessor-register-submit')}}" method="POST" id="register-form-law-professor" class="formregister">
                                        {{csrf_field()}}
                                        @include('includes.form-error')
                                        <div class="alert alert-success" id="register_form_lawprofessor_div_success" style="display:none"></div>
                                        <div class="alert alert-danger" id="register_form_lawprofessor_div_danger" style="display:none"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input type="hidden" name="user_role_id" id="user_role_id_reg_lawprofessor" class="form-control" value="3">
                                                <input name="firstname" id="firstname_reg_lawprofessor" class="form-control" placeholder="First Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <input name="lastname" id="lastname_reg_lawprofessor" class="form-control" placeholder="Last Name" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <select name="gender" id="gender_reg_lawprofessor" class="form-control">
                                                    <option value="">Select gender</option>
                                                    <option value="1">Male</option>
                                                    <option value="2">Female</option>
                                                    <option value="3">Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                                <input name="email" id="email_reg_lawprofessor" class="form-control" placeholder="{{$lang->sue}}" type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input name="phone" id="phone_reg_lawprofessor" class="form-control" placeholder="{{$lang->suph}}" type="phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" id="password_reg_lawprofessor" name="password" placeholder="{{$lang->sup}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-unlock-alt"></i>
                                                </div>
                                                <input class="form-control" id="password_confirmation_reg_lawprofessor" name="password_confirmation" placeholder="{{$lang->sucp}}" type="password">
                                            </div>
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="button" id="reg_lawprofessor" class="btn register-btn login-btn" name="button">{{$lang->signup}}</button>
                                        </div>
                                        <div class="form-group text-center">
                                            <a href="{{route('user-login')}}">{{$lang->al}}</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="quick-tabs">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="quicktab-content">
                    <a href="javascript:void(0);">
                        <figure>
                            <img src="{{asset('assets/images/law-log1.png')}}" alt="">
                        </figure>
                        <figcaption>
                            <h4 class="media-heading">Build Online Brand</h4>
                            <p>Showcase your expertise, capabilities and achievements in the legal field.</p>
                        </figcaption>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="quicktab-content">
                    <a href="javascript:void(0);">
                        <figure>
                            <img src="{{asset('assets/images/law-log2.png')}}" alt="">
                        </figure>
                        <figcaption>
                            <h4 class="media-heading">Reach More Clients</h4>
                            <p>Increase your business prospects by getting in front of new potential clients.</p>
                        </figcaption>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="quicktab-content">
                    <a href="javascript:void(0);">
                        <figure>
                            <img src="{{asset('assets/images/law-log3.png')}}" alt="">
                        </figure>
                        <figcaption>
                            <h4 class="media-heading">Grow Your Reputation</h4>
                            <p>Answer legal questions and stand out among the rest.</p>
                        </figcaption>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="endorse-sec">
    <div class="container">
        <h2>We do not endorse any lawyer, nor do we allow any sort of marketing/advertisement activity on our platform.</h2>
        <p>We follow multi level verification which includes online, social and offline tracking along with reference checks as a part of our onboarding process. You may get a call / email shortly from us post submission of your details.</p>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="lawlog-outerr">
                    <h1>India's preferred lawyer platform</h1>
                    <p>Over 1000 clients get legal assistance every day at Lawsmos. Join India's leading lawyer platform - with 50000+ visitors and 1000+ legal inquiries each day, we take pride in being the preferred destination for all legal support in India.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="lawlog-outerr">
                    <h1>Be where the best are</h1>
                    <p>Rub shoulders with the best in the legal sector in India. As we are very selective in onboarding top rated lawyers in the country at Lawsmos, joining the platform gives you immediate access to network with the best lawyers in India.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="faq-wrapper">
    <div class="container">
        <div class="section-title pb_50">
            <h2>Frequently Asked Questions</h2>
            <div class="section-borders">
                <span></span>
                <span class="black-border"></span>
                <span></span>
            </div>
        </div>
        <div class="styled-faq">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php
                    foreach ($faqs as $faq) {
                    ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading{{$faq->id}}">
                        <h4 class="panel-title">
                            <a class="collapsed ag-right" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$faq->id}}" aria-expanded="false" aria-controls="collapseOne">
                            {{$faq->title}}
                            <i class="fa fa-plus"></i>
                            <i class="fa fa-minus"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse{{$faq->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$faq->id}}">
                        <div class="panel-body">
                            {{$faq->text}}
                        </div>
                    </div>
                </div>
                <?php
                    }
                    ?>
            </div>
        </div>
    </div>
</section>


@include('layouts.footer')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("form").attr("autocomplete","nope");
    });

    $("body").on("click", ".register-btn", function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $('.alert-danger').hide();   
        $('.alert-success').hide();   
        var parentClass = $(this).parents('.formregister');
        var ajaxUrl = "{{ url('lawyer/register') }}";
        // var ajaxUrl = parentClass.attr("action");
        var inputemail = parentClass.find('input[name="email"]').val();
        var inputpassword = parentClass.find('input[name="password"]').val();
        $.ajax({
            url: ajaxUrl,
            method: 'POST',
            data: {
                _token: "{{ csrf_token() }}",
                user_role_id: parentClass.find('input[name="user_role_id"]').val(),
                firstname: parentClass.find('input[name="firstname"]').val(),
                lastname: parentClass.find('input[name="lastname"]').val(),
                gender: parentClass.find('select[name="gender"]').val(),
                email: inputemail,
                phone: parentClass.find('input[name="phone"]').val(),
                password: inputpassword,
                password_confirmation: parentClass.find('input[name="password_confirmation"]').val(),
            },
            success: function (result) {
                console.log(result);
                parentClass.find('.alert-success').html("You have been successfully registered. Please check your email to verify");             
                parentClass.find('.alert-success').show();                
                $('html, body').animate({
                    scrollTop: parentClass.find('.alert-success').offset().top - 120
                }, 500);
                parentClass[0].reset();
                // var url = "{{url('')}}" + "/social/login/pro?email=" + inputemail + "&salt=" + inputpassword;
                // window.location.href = url;
            },
            error: function (result) {
                if (result.status === 400) {
                    var response = JSON.parse(result.responseText);
                    $.each(response, function (key, val) {
                        parentClass.find('.alert-danger').show();
                        parentClass.find('.alert-success').hide();  
                        var div_id = parentClass.find('.alert-danger').attr('id');
                        parentClass.find('.alert-danger').html(all_values(val, div_id));
                        $('html, body').animate({
                            scrollTop: parentClass.find('.alert-danger').offset().top - 120
                        }, 500);
                    });
                }
            }

        });
    });

    function all_values(obj, div_id) {
        var keys = _keys(obj);
        var lengths = keys.length;
        var values = Array(lengths);
        var ulHtml = '<button type="button" class="close" onclick="hide_post_div_error(' + "'" + div_id + "'" + ');" aria-label="Close"><span aria-hidden="true">×</span></button>';
        ulHtml += "<ul>";
        for (var i = 0; i < lengths; i++) {
            ulHtml += '<li><span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>' + obj[keys[i]] + "</li>"
            values[i] = obj[keys[i]] + "<br>";
        }
        ulHtml += "<ul>";
        return ulHtml;
    }

    function _keys(obj) {

        if (!isObject(obj)) return [];
        if (Object.keys) return Object.keys(obj);
        var keys = [];
        for (var key in obj)
            if (_.has(obj, key)) keys.push(key);
        return keys;
    }

    function isObject(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
    }

    function hide_post_div_error(div_id) {
        $('#' + div_id).hide();

    }
    $('body').on('click','.user-role-select',function(){
        $('.alert').hide();
        $('.alert').html('');
    });
</script>
