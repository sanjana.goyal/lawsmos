@extends('layouts.front')
@section('content')

<style>
    #register-form-lawyer{
        display:block !important;
    }
</style>

<section class="login-area">
            <div class="container">
                <div class="form-wrapper-p">
                    <div class="left-box-p">
                            <h2>{{$lang->signup}}</h2>
                             <p>Please fill in this form to create an account! </p>
                            </div>
                        <div class="login-form link-formm">
                            
                            
<!--
                            <div class="section-borders">
                                <span></span>
                                <span class="black-border"></span>
                                <span></span>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-fw fa-cog"></i>
                                  </div>
                                  <select name="user_role_id" id="user_role_id" class="form-control" required="">
                                      <option value="">Select Registration Type</option>
                                      <option value="0">Register as Advocate</option>
                                      
                                  </select>
                                </div>
                              </div>
-->

                            <form action="{{route('user-register-link-submit')}}" method="POST" id="register-form-lawyer" class="formregister">
                              {{csrf_field()}}
                              @include('includes.form-error')
                              
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                     <i class="fa fa-file-text" aria-hidden="true"></i>
                                  </div>
									
									<input type="hidden" name="firmid" value="<?php echo $pageid; ?>" />
									<input name="pagename" class="form-control" placeholder="Page Name" type="text" value="<?php echo $firmname; ?>" readonly>
                                
                                
                                </div>
                              </div>
                              
                              
                              
                               <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                  </div>
                                  
                                  
                                  <input name="firstname" class="form-control" placeholder="First Name" type="text" value="<?php if(isset($firstname)){ echo $firstname; } ?>" readonly>
                                
                                
                                </div>
                              </div>
                              
                               <?php if(!empty($lastname) || $lastname!=""){?> 
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                  </div>
                                  
                               
                                  <input name="lastname" class="form-control" placeholder="Last Name" type="text" value="<?php  if(isset($lastname)){ echo $lastname; } ?>" readonly />
								
                                </div>
                              </div>
                              <?php } ?>
                            
                              
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-fw fa-cog"></i>
                                  </div>
                                  <select name="category_id" id="blood_grp" class="form-control" required="">
                                    <option value="">{{$lang->sbg}}</option>
                                    @foreach($cats as $cat)
                                      <option value="{{$cat->id}}">{{$cat->cat_name}}</option>

                                    @endforeach
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-envelope"></i>
                                  </div>
                                  <input name="email" class="form-control" placeholder="{{$lang->sue}}" type="email" value="<?php  echo $email; ?>" readonly />
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                        <i class="fa fa-unlock-alt"></i>
                                    </div>
                                  <input class="form-control" name="password" placeholder="{{$lang->sup}}" type="password">
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                    </div>
                                  <input class="form-control" name="password_confirmation" placeholder="{{$lang->sucp}}" type="password">
                                </div>
                              </div>
                              <div class="form-group text-center">
                                    <button type="submit" class="btn login-btn" name="button">{{$lang->signup}}</button>
                              </div>
                              <div class="form-group text-center">
                                    <a href="{{route('user-login')}}">{{$lang->al}}</a>
                              </div>
                            </form>





                           

                           
                            
                         
                            
                        

                           
                           
                           
                           
                        </div>
                   
                </div>
            </div>
        </section>
      
@endsection

