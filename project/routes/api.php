<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::post('register', 'API\RegisterController@register');
Route::get('login', 'API\LoginController@login');


Route::get('search', 'API\SearchController@search');
Route::get('questions', 'API\SearchController@lastthree');
Route::get('judgements', 'API\SearchController@searchjudgements');


