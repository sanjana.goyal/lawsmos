<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login2', 'Auth\UserRegisterController@login2');
Route::get('checkUser', 'Auth\UserRegisterController@checkUser');

Route::get('send_email','FrontendController@sendEmail')->name('sendEmail');

Route::get('admin_email', function () {
  return view('email.admin_inform_template');
});
//Route::get('admin_email', function () {
//  return view('email.email_contact_us');
//});



  Route::get('/searchterms/{name}','FrontendController@searchtermsalpha')->name('front.searchtermsalphassss');
  Route::get('/dark-home','FrontendController@dark_home')->name('front.dark-home');
  Route::get('/searchtermsmaxims/{name}','FrontendController@searchtermsmaxims')->name('front.searchtermsmaxims');
  Route::get('/searchbacts/{name}','FrontendController@searchbacts')->name('front.searchbacts');
  Route::get('/','FrontendController@index')->name('front.index');
  Route::get('/report/{userid}/{token}','FrontendController@report')->name('front.report');
  Route::get('/socialprofile','FrontendController@viewsocial')->name('front.social');
  Route::get('/Q&A','FrontendController@questionanswer')->name('front.questionanswer');
  Route::get('/Q&Aaa','FrontendController@forceloggin')->name('front.logginplease');
  Route::get('/lawyers','FrontendController@users')->name('front.users');
  Route::get('/act_details/{id}','FrontendController@act_details')->name('front.act_details');
  Route::get('/detail/{id}','FrontendController@highcourtedetails')->name('front.highcourtedetails');
  Route::get('/research/{id}','FrontendController@researchdetails')->name('front.researchdetails');
  Route::get('/detail_supreme/{id}','FrontendController@supremecourtedetails')->name('front.highcourtedetails');
  Route::get('/view_section_details/{id}','FrontendController@view_section_details')->name('front.view_section_details');
  Route::get('/view_schedules/{id}','FrontendController@view_schedules')->name('front.view_schedules');
  Route::get('/view_maxims/{id}','FrontendController@view_maxims')->name('front.view_maxims');
  Route::get('/bareacts/{id}','FrontendController@view_bare_acts')->name('front.view_bare_acts');
  Route::get('/newsdetail/{id}','FrontendController@newsdetail')->name('front.newsdetail');
  Route::get('/viewjudgements/{id}','FrontendController@view_judgements')->name('front.view_judgements');
  Route::get('/landmarkjudgements/{id}','FrontendController@landmarkjudgements')->name('front.landmarkjudgements');
  Route::get('/sendmail','FrontendController@useremail')->name('front.sendmail');
  Route::get('/searchmaxims','FrontendController@searchmaxims')->name('front.searchmaxims');
  Route::get('/searchnewmaxims','FrontendController@searchnewmaxims')->name('front.searchnewmaxims');
  Route::get('/searchmyresearches','FrontendController@searchmyresearches')->name('front.search_researches');

  Route::get('/getSubCategory','FrontendController@getSubCategory')->name('front.getSubCategory');
 
  
  Route::get('/submitsearch','FrontendController@submitsearch')->name('front.submitsearch');
  Route::get('/searchhighcourt','FrontendController@searchhighcourt')->name('front.searchhighcourt');
  Route::get('/searchsupremecourt','FrontendController@searchsupremecourt')->name('front.searchsupremecourt');
  Route::get('/searchbar_jugements','FrontendController@searchbar_jugements')->name('front.searchbar_jugements');
  Route::get('/searcharticles','FrontendController@searcharticles')->name('front.searcharticles');
  Route::get('/searchmax','FrontendController@searchmax')->name('front.searchmax');
  Route::get('/searcharticlebycat','FrontendController@searcharticlebycat')->name('front.searcharticlebycat');
  Route::get('/searcharticlebycatgg','FrontendController@searcharticlebycatgg')->name('front.searcharticlebycatgg');
  Route::get('/viewmap','FrontendController@view_map')->name('front.map');
  Route::get('refreshcaptcha', 'FrontendController@refreshCaptcha');
  Route::get('/draftcase','FrontendController@draftcase')->name('front.draftcase');
  Route::get('/getPerformas','FrontendController@getPerformas')->name('front.getPerformas');
  Route::get('/getFeaturedList','FrontendController@getFeaturedList')->name('front.getFeaturedList');
  Route::get('/draft_case_referance_iframe/{id}','FrontendController@draft_case_referance_iframe')->name('front.draft_case_referance_iframe');
  //search judgements, bareacts, maxims, landmark judgements. 
  Route::get('search/judgements','FrontendController@search_judgements')->name('front.searchjudgements');
  Route::get('caseeditor/save','FrontendController@savesession')->name('front.searchjudgements');
  Route::get('headnots/save','FrontendController@saveheadnots')->name('front.searchjudgements');
  Route::get('search/bareacts','FrontendController@search_bareacts')->name('front.searchbareacts');
  Route::get('searchacts','FrontendController@search_bareactsss')->name('front.search_bareactsss');
  Route::get('/lawyers/featured','FrontendController@featured')->name('front.featured');
  Route::get('/lawyer-profile/{id}','FrontendController@user')->name('front.user');
  Route::get('/viewjudgement/{id}','FrontendController@viewjudgementss');
  Route::get('/lawyer-user-profile/{id}','FrontendController@userprofile')->name('front.user.profile');
  Route::get('/firm-profile/{id}','FrontendController@firmprofile')->name('front.lawfirm.profile');
  Route::get('/category/{slug}','FrontendController@types')->name('front.types');
  Route::get('/lawyers/search/','FrontendController@search')->name('user.search');
  Route::get('/enterlocation','FrontendController@enterlocation')->name('user.enterlocation');
  Route::get('/faq','FrontendController@faq')->name('front.faq');
  Route::get('/bareacts','FrontendController@bareacts')->name('front.bareacts');
  Route::get('/articles','FrontendController@listallarticles')->name('front.listallarticles');
  Route::get('/maxims','FrontendController@listallmaxims')->name('front.listallmaxims');
  Route::get('/allmaxims','FrontendController@allmaxims')->name('front.allmaxims');
  Route::get('/highcourt','FrontendController@listallhighcourt')->name('front.listallhighcourt');
  Route::get('/supremecourt','FrontendController@listallsupremecourt')->name('front.listallsupreme');
  Route::get('/ads/{id}','FrontendController@ads')->name('front.ads');
  Route::get('/about','FrontendController@about')->name('front.about');
  Route::get('/contact','FrontendController@contact')->name('front.contact');
  Route::get('/blog','FrontendController@blog')->name('front.blog');
  Route::get('/blog/{id}','FrontendController@blogshow')->name('front.blogshow');
  Route::get('/articles/{id}','FrontendController@viewarticle')->name('front.articles');
  Route::get('/pages/{id}','FrontendController@pageshow')->name('front.pageshow');
  Route::get('/history/','FrontendController@updatehistory')->name('front.updatehistory');
  Route::post('/contact','FrontendController@contactemail')->name('front.contact.submit');
  Route::post('/subscribe','FrontendController@subscribe')->name('front.subscribe.submit');
  Route::post('/lawyer/contact','FrontendController@useremail')->name('front.user.submit');
  Route::get('/lawyer/refresh_code','FrontendController@refresh_code');
  Route::get('/frontlead/','FrontendController@showfrontlead')->name('front.lead');
  Route::get('/addfrontlead','FrontendController@addfrontlead')->name('front.addlead');
  Route::get('/contactus','FrontendController@contactus')->name('front.contactus');
  Route::get('/postquestion','FrontendController@postquestion')->name('front.postquestions');
  Route::prefix('lawyer')->group(function() {
  Route::get('/dashboard', 'UserController@index')->name('user-dashboard');
  Route::get('/my-cases', 'UserController@mycases')->name('my-cases');
  Route::get('/q2a', 'UserController@q2alogin')->name('user-q2a');
  Route::get('/social', 'UserController@usersocial')->name('user-social');
  Route::get('/blog', 'UserBlogController@index')->name('user-blogs-index');
  Route::get('/blog/create', 'UserBlogController@create')->name('user-blog-create');
  Route::post('/blog/create', 'UserBlogController@store')->name('user-blog-store');
  Route::get('/blog/edit/{id}', 'UserBlogController@edit')->name('user-blog-edit');
  Route::post('/blog/{id}/comments', 'CommentController@store')->name('user-comment-update');  
  Route::post('/blog/edit/{id}', 'UserBlogController@update')->name('user-blog-update');  
  Route::get('/blog/delete/{id}', 'UserBlogController@destroy')->name('user-blog-delete'); 
  Route::post('/comment/store', 'CommentController@store')->name('comment.add');
  //Route::get('/ServiceSeekerdashboard', 'UserController@serviceSeekerdashboard')->name('service-seeker-dashboard');
  Route::get('/reset', 'UserController@resetform')->name('user-reset');
  Route::post('/reset', 'UserController@reset')->name('user-reset-submit');
  Route::get('/profile', 'UserController@profile')->name('user-profile'); 
  Route::post('/profile', 'UserController@profileupdate')->name('user-profile-update'); 
  Route::get('/forgot', 'Auth\UserForgotController@showforgotform')->name('user-forgot');
  Route::post('/forgot', 'Auth\UserForgotController@forgot')->name('user-forgot-submit');
  Route::get('/login', 'Auth\UserLoginController@showLoginForm')->name('user-login');

  // Route::get('/login/{id}', 'Auth\UserLoginController@showLoginFormWithVerification')->name('user-login-verification');
  
  Route::get('/login/{id}', 'FrontendController@showLoginFormWithVerification')->name('user-login-verification');
  Route::get('/user_verification/{id}/{token}', 'FrontendController@userVerification')->name('user-verification-link');
  Route::post('/login_ajax', 'Auth\UserLoginController@login_ajax_check_verification')->name('user-login-check-verification');
  Route::post('/login', 'Auth\UserLoginController@login')->name('user-login-submit');
  Route::get('/register', 'Auth\UserRegisterController@showRegisterForm')->name('user-register');
  Route::get('/registerlink/{id}/{email}/{name}', 'Auth\UserRegisterController@showAdvocateRegisterForm')->name('user-register-link');
  // Route::get('/user_verification/{id}/{token}', 'Auth\UserRegisterController@userVerification')->name('user-verification-link');
  Route::post('/registerlink', 'Auth\UserRegisterController@registerlink')->name('user-register-link-submit');
  Route::post('/register', 'Auth\UserRegisterController@register')->name('user-register-submit');
   Route::post('/register2', 'Auth\UserRegisterController@register2')->name('user-register-submit');
  Route::post('/universityregister', 'Auth\UserRegisterController@universityregister')->name('university-register-submit');
  Route::post('/ServiceSeekerRegister', 'Auth\UserRegisterController@registerServiceSeeker')->name('serviceSeeker-register-submit');
  Route::post('/lawstudentRegister', 'Auth\UserRegisterController@registerlawstudent')->name('lawstudent-register-submit');
  Route::post('/lawprofessorRegister', 'Auth\UserRegisterController@registerProfessor')->name('lawprofessor-register-submit');
  Route::get('/logout', 'Auth\UserLoginController@logout')->name('user-logout');
  Route::post('/payment', 'PaymentController@store')->name('payment.submit');
  Route::get('/payment/cancle', 'PaymentController@paycancle')->name('payment.cancle');
  Route::get('/payment/return', 'PaymentController@payreturn')->name('payment.return');
  Route::get('/publish', 'UserController@publish')->name('user-publish'); 
  Route::get('/feature', 'UserController@feature')->name('user-feature');   
  });
  
  Route::get('finalize', 'FrontendController@finalize');
  Route::post('the/genius/ocean/2441139', 'FrontendController@subscription');
  Route::post('/user/payment/notify', 'PaymentController@notify')->name('payment.notify');
  Route::post('/stripe-submit', 'StripeController@store')->name('stripe.submit');
  Route::prefix('admin')->group(function() {
  Route::get('/dashboard', 'AdminController@index')->name('admin-dashboard');
  Route::get('/profile', 'AdminController@profile')->name('admin-profile'); 
  Route::post('/profile', 'AdminController@profileupdate')->name('admin-profile-update'); 
  Route::get('/reset-password', 'AdminController@passwordreset')->name('admin-password-reset');
  Route::post('/reset-password', 'AdminController@changepass')->name('admin-password-change');
  Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin-login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin-login-submit');
  Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin-logout');
  Route::get('/lawyers', 'AdminUserController@index')->name('admin-user-index');
  Route::get('/lawyers/create', 'AdminUserController@create')->name('admin-user-create');
  Route::post('/lawyers/create', 'AdminUserController@store')->name('admin-user-store');
  Route::get('/lawyers/edit/{id}', 'AdminUserController@edit')->name('admin-user-edit');
  Route::post('/lawyers/update/{id}', 'AdminUserController@update')->name('admin-user-update');
  Route::get('/lawyers/delete/{id}', 'AdminUserController@destroy')->name('admin-user-delete');
  Route::get('/lawyers/status/{id1}/{id2}', 'AdminUserController@status')->name('admin-user-st');
  Route::get('/category', 'CategoryController@index')->name('admin-cat-index');
  Route::get('/category/create', 'CategoryController@create')->name('admin-cat-create');
  Route::post('/category/create', 'CategoryController@store')->name('admin-cat-store');
  Route::get('/category/edit/{id}', 'CategoryController@edit')->name('admin-cat-edit');
  Route::post('/category/update/{id}', 'CategoryController@update')->name('admin-cat-update');
  Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('admin-cat-delete');
  Route::get('/faq', 'FaqController@index')->name('admin-fq-index');
  Route::get('/faq/create', 'FaqController@create')->name('admin-fq-create');
  Route::post('/faq/create', 'FaqController@store')->name('admin-fq-store');
  Route::get('/faq/edit/{id}', 'FaqController@edit')->name('admin-fq-edit');
  Route::post('/faq/update/{id}', 'FaqController@update')->name('admin-fq-update');
  Route::post('/faqup', 'PageSettingController@faqupdate')->name('admin-faq-update');
  Route::get('/faq/delete/{id}', 'FaqController@destroy')->name('admin-fq-delete');
  Route::get('/blog', 'AdminBlogController@index')->name('admin-blog-index');
  Route::get('/blog/create', 'AdminBlogController@create')->name('admin-blog-create');
  Route::post('/blog/create', 'AdminBlogController@store')->name('admin-blog-store');
  Route::get('/blog/edit/{id}', 'AdminBlogController@edit')->name('admin-blog-edit');
  Route::post('/blog/edit/{id}', 'AdminBlogController@update')->name('admin-blog-update');  
  Route::get('/blog/delete/{id}', 'AdminBlogController@destroy')->name('admin-blog-delete'); 
  Route::get('/testimonial', 'PortfolioController@index')->name('admin-ad-index');
  Route::get('/testimonial/create', 'PortfolioController@create')->name('admin-ad-create');
  Route::post('/testimonial/create', 'PortfolioController@store')->name('admin-ad-store');
  Route::get('/testimonial/edit/{id}', 'PortfolioController@edit')->name('admin-ad-edit');
  Route::post('/testimonial/edit/{id}', 'PortfolioController@update')->name('admin-ad-update');  
  Route::get('/testimonial/delete/{id}', 'PortfolioController@destroy')->name('admin-ad-delete');
  Route::get('/advertise', 'AdvertiseController@index')->name('admin-adv-index');
  Route::get('/advertise/st/{id1}/{id2}', 'AdvertiseController@status')->name('admin-adv-st');
  Route::get('/advertise/create', 'AdvertiseController@create')->name('admin-adv-create');
  Route::post('/advertise/create', 'AdvertiseController@store')->name('admin-adv-store');
  Route::get('/advertise/edit/{id}', 'AdvertiseController@edit')->name('admin-adv-edit');
  Route::post('/advertise/edit/{id}', 'AdvertiseController@update')->name('admin-adv-update');  
  Route::get('/advertise/delete/{id}', 'AdvertiseController@destroy')->name('admin-adv-delete'); 
  Route::get('/page-settings/about', 'PageSettingController@about')->name('admin-ps-about');
  Route::post('/page-settings/about', 'PageSettingController@aboutupdate')->name('admin-ps-about-submit');
  Route::get('/page-settings/contact', 'PageSettingController@contact')->name('admin-ps-contact');
  Route::post('/page-settings/contact', 'PageSettingController@contactupdate')->name('admin-ps-contact-submit');
  Route::get('/social', 'SocialSettingController@index')->name('admin-social-index');
  Route::post('/social/update', 'SocialSettingController@update')->name('admin-social-update');
  Route::get('/seotools/analytics', 'SeoToolController@analytics')->name('admin-seotool-analytics');
  Route::post('/seotools/analytics/update', 'SeoToolController@analyticsupdate')->name('admin-seotool-analytics-update');
  Route::get('/seotools/keywords', 'SeoToolController@keywords')->name('admin-seotool-keywords');
  Route::post('/seotools/keywords/update', 'SeoToolController@keywordsupdate')->name('admin-seotool-keywords-update');
  Route::get('/general-settings/logo', 'GeneralSettingController@logo')->name('admin-gs-logo');
  Route::post('/general-settings/logo', 'GeneralSettingController@logoup')->name('admin-gs-logoup');
  Route::get('/general-settings/favicon', 'GeneralSettingController@fav')->name('admin-gs-fav');
  Route::post('/general-settings/favicon', 'GeneralSettingController@favup')->name('admin-gs-favup');
  Route::get('/general-settings/loader', 'GeneralSettingController@load')->name('admin-gs-load');
  Route::post('/general-settings/loader', 'GeneralSettingController@loadup')->name('admin-gs-loadup');
  Route::get('/general-settings/payments', 'GeneralSettingController@payments')->name('admin-gs-payments');
  Route::post('/general-settings/payments', 'GeneralSettingController@paymentsup')->name('admin-gs-paymentsup');
  Route::get('/general-settings/contents', 'GeneralSettingController@contents')->name('admin-gs-contents');
  Route::post('/general-settings/contents', 'GeneralSettingController@contentsup')->name('admin-gs-contentsup');
  Route::get('/general-settings/bgimg', 'GeneralSettingController@bgimg')->name('admin-gs-bgimg');
  Route::post('/general-settings/bgimgup', 'GeneralSettingController@bgimgup')->name('admin-gs-bgimgup');
  Route::get('/general-settings/about', 'GeneralSettingController@about')->name('admin-gs-about');
  Route::post('/general-settings/about', 'GeneralSettingController@aboutup')->name('admin-gs-aboutup');
  Route::get('/general-settings/address', 'GeneralSettingController@address')->name('admin-gs-address');
  Route::post('/general-settings/address', 'GeneralSettingController@addressup')->name('admin-gs-addressup');
  Route::get('/general-settings/footer', 'GeneralSettingController@footer')->name('admin-gs-footer');
  Route::post('/general-settings/footer', 'GeneralSettingController@footerup')->name('admin-gs-footerup');
  Route::get('/general-settings/bg-info', 'GeneralSettingController@bginfo')->name('admin-gs-bginfo');
  Route::post('/general-settings/bg-info', 'GeneralSettingController@bginfoup')->name('admin-gs-bginfoup');
  Route::get('/subscribers', 'SubscriberController@index')->name('admin-subs-index');
  Route::get('/subscribers/download', 'SubscriberController@download')->name('admin-subs-download');
  Route::get('/languages', 'LanguageController@lang')->name('admin-lang-index');
  Route::post('/languages', 'LanguageController@langup')->name('admin-lang-submit');
  Route::get('/get-city-by-state-id', 'FrontendController@getCityByStateId')->name('get-city-by-state-id');


  });
