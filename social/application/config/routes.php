<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:addpage
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = "home";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Custom Routes
$route['profile/(:any)'] = "profile/index/$1";
$route['bonus/(:num)'] = "user_settings/bonus/$1";
$route['mypages/(:num)'] = "pages/view/$1";
$route['myuniversity/(:num)'] = "pages/viewuniversity/$1";

$route['home/(:num)'] = "home/index/$1";
$route['myaccount']="profile/studentaccount";

$route['albums/(:num)']="profile/albums/$1";
$route['profile']="home/comppleteprofile";
$route['metadata']="profile/metadata";
$route['profile_law_student']="home/profile_law_student";
$route['profile1']="home1/comppleteprofile";
$route['appointments']="user_settings/myappointment";
$route['messages']="chat";
$route['myconnections/(:num)']="profile/friends/$1";
$route['settings']="user_settings";

$route['edit_profile_lawyer']="user_settings/edit_profile_lawyer";
$route['edit_profile_lawyer_step_1']="user_settings/edit_profile_lawyer_step_1";
$route['edit_profile_lawyer_step_2']="user_settings/edit_profile_lawyer_step_2";
$route['edit_profile_lawyer_step_3']="user_settings/edit_profile_lawyer_step_3";
$route['edit_profile_lawyer_step_4']="user_settings/edit_profile_lawyer_step_4";
$route['edit_profile_lawyer_step_5']="user_settings/edit_profile_lawyer_step_5";
$route['edit_profile_lawyer_step_6']="user_settings/edit_profile_lawyer_step_6";


$route['edit_profile_student_step_1']="user_settings/edit_profile_student_step_1";
$route['edit_profile_student_step_2']="user_settings/edit_profile_student_step_2";
$route['edit_profile_student_step_3']="user_settings/edit_profile_student_step_3";
$route['edit_profile_student_step_4']="user_settings/edit_profile_student_step_4";
$route['edit_profile_student_step_5']="user_settings/edit_profile_student_step_5";


$route['edit_profile_seeker']="user_settings/edit_profile_seeker";
$route['update_profile_seeker']="user_settings/update_profile_seeker";
$route['update_profile_seeker']="user_settings/update_profile_seeker";

$route['edit_profile_student']="user_settings/edit_profile_student";
$route['edit_student_profile']="user_settings/edit_student_profile";

$route['edit_profile_professor']="user_settings/edit_profile_professor";
$route['edit_professor_profile']="user_settings/edit_professor_profile";


$route['my_resume']="profile/my_resume";
$route['my_resume_pdf']="profile/my_resume_pdf";
$route['my_resume_public/(:num)']="profile/my_resume_public/$1";
$route['appointment']="user_settings/myappointments";
$route['servicerequests']="user_settings/myservicerequests";
$route['research']="case_listings/researchIndex";
$route['casebuilder']="case_listings";
$route['articles']="articles/create";
$route['articles/save_article']="articles/save_article";
$route['article/view']="articles/your";
$route['article/list']="articles/listarticles";
$route['articles/sections']="articles/sections";

$route['leads']="service_request/serviceleads";
$route['service_request']="service_request";
$route['connectionrequests']="user_settings/friend_requests";
$route['addpage']="pages/add";
$route['adduniversity']="pages/adduniversity";

$route['pendingpage']="pages/pendingpage";
$route['funds']="funds";
$route['admin']="admin";
$route['homep']="home/index/4";
$route['viewleads']="service_request/serviceleads";
$route['upgrade/advocate']="profile/upgradeaccount";
$route['update/password']="user_settings/change_password";
$route['privacy/settings']="user_settings/privacy";
$route['socialnetwork']="user_settings/social_networks";
$route['create/agency']="user_settings/create_agency";
$route['create/university']="user_settings/create_university";

$route['usersettings']="user_settings";
$route['myjudgements']="user_settings/myjudgments";
$route['create/article']="articles/create_article_pro";

$route['editarticle']="articles/edit_article_pro";
$route['ratings']="ratings/view_appointment_history";
$route['leadershipboard']="user_settings/leadership";
$route['invite_contacts']="user_settings/invitecontacts";
$route['invite']="user_settings/process";
$route['send']="user_settings/send_email_to_gmail_list";
$route['createblog']="blog/your";
$route['allposts']="blog/new_posts";
$route['sendmail']="user_settings/sendmail";

$route['headnotes']="headnotes";



$route['funds/account_statement']="funds/accountStatement";
//$route['funds/account_statement']="funds/accountStatement";


