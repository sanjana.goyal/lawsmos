<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Admin extends CI_Controller 

{



	public function __construct() 

	{

		parent::__construct();

		$this->load->model("admin_model");

		$this->load->model("user_model");

		$this->load->model("blog_model");

		$this->load->model("agency_model");

		$this->load->model("feed_model");

		$this->load->helper('custom_helper');



		if (!$this->user->loggedin) $this->template->error(lang("error_1"));

		if(!$this->common->has_permissions(array("admin", "admin_settings",

			"admin_members", "admin_payment"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

	}





	public function index() 

	{	

		$this->template->loadData("activeLink", 

			array("admin" => array("general" => 1)));

		$this->template->loadContent("admin/index.php", array(

			)

		);



	}



	public function blogs() 

	{	

		$this->template->loadData("activeLink", 

			array("admin" => array("blogs" => 1)));

			

		$blogs = $this->admin_model->get_all_blog();

		

		

		$this->template->loadContent("admin/blogs.php", array(

		

			"allblogs"=>$blogs->result(),

			)

		);



	}

	

	public function news()

	{

		$this->template->loadData("activeLink", 

			array("admin" => array("news" => 1)));

			

		$news = $this->admin_model->get_all_news();

		

		

		$this->template->loadContent("admin/news.php", array(

		

			"news"=>$news->result(),

			)

		);

	

	}

	

	

	public function create_pro() 

	{

		

		

		

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}

		$title = $this->common->nohtml($this->input->post("title"));

		$description = $this->common->nohtml($this->input->post("description"));



		$private = intval($this->input->post("private"));



		if(empty($title)) {

			$this->template->error(lang("error_172"));

		}



		$blog = $this->blog_model->get_user_blog($this->user->info->ID);

		/*

		if($blog->num_rows() > 0) {

			$this->template->error(lang("error_178"));

		}

		*/

		



		$this->blog_model->add_blog(array(

			"title" => $title,

			"description" => $description,

			"userid" => $this->user->info->ID,

			"private" => $private,

			"timestamp" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_107"));

		redirect(site_url("admin/blogs"));

	}

	

	

	public function create_news_pro() 

	{

		

		

	if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}

		$title = $this->common->nohtml($this->input->post("title"));

		$description = $this->common->nohtml($this->input->post("description"));



		$private = intval($this->input->post("private"));



		if(empty($title)) {

			$this->template->error(lang("error_172"));

		}



		$blog = $this->blog_model->get_user_news($this->user->info->ID);

		/*

		if($blog->num_rows() > 0) {

			$this->template->error(lang("error_178"));

		}

		*/

		



		$this->blog_model->add_news(array(

			"title" => $title,

			"description" => $description,

			"userid" => $this->user->info->ID,

			"private" => $private,

			"timestamp" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", "News catagory Added Successfully");

		redirect(site_url("admin/news"));

	}

	

	

	

	

	

	

	

	public function add_post_pro() 

	{

		

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}

		$blog = $this->blog_model->get_user_blog($this->user->info->ID);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_177"));

		}

		$blog = $blog->row();



		$title = $this->common->nohtml($this->input->post("title"));

		$status = intval($this->input->post("status"));

		$blog_post = $this->lib_filter->go($this->input->post("blog_post"));

		$post_to_timeline = intval($this->input->post("post_to_timeline"));

		$blogid = intval($this->input->post("blogid"));



		if(empty($title)) {

			$this->template->error(lang("error_174"));

		}



		if(empty($blog_post)) {

			$this->template->error(lang("error_175"));

		}



		// Upload

		$this->load->library("upload");



		$blog_image = "";

		if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {

			$this->upload->initialize(array( 

		       "upload_path" => $this->settings->info->upload_path,

		       "overwrite" => FALSE,

		       "max_filename" => 1500,

		       "encrypt_name" => TRUE,

		       "remove_spaces" => TRUE,

		        "allowed_types" => 'jpg|gif|png|jpeg|JPG|PNG',

		       "max_size" => $this->settings->info->file_size,

		       "max_width" => 1500,

		       "max_height" => 1500

		    ));



		    if (!$this->upload->do_upload("userfile")) {

		    	$this->template->error(lang("error_21")

		    	.$this->upload->display_errors());

		    }



		    $data = $this->upload->data();



		    $blog_image = $data['file_name'];

		}



		$time = 0;

		if($status == 1) {

			$time = time();

		}



		$postid = $this->blog_model->add_post(array(

			"blogid"=>$blogid,

			"title" => $title,

			"body" => $blog_post,

			"timestamp" => $time,

			"status" => $status,

			"image" => $blog_image,

			"last_updated" => time()

			)

		);



		if($post_to_timeline && $status == 1) {

			// Make post

			// Add a feed post

			$postid = $this->feed_model->add_post(array(

				"userid" => $this->user->info->ID,

				"blog_postid" => $postid,

				"timestamp" => time(),

				"post_as" => "user",

				)

			);

		}



		$this->session->set_flashdata("globalmsg", lang("success_110"));

		redirect(site_url("admin/blogs"));



	}

	public function add_news_post_pro() 

	{

		

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}

		$blog = $this->blog_model->get_user_news($this->user->info->ID);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_177"));

		}

		$blog = $blog->row();



		$title = $this->common->nohtml($this->input->post("title"));

		$status = intval($this->input->post("status"));

		$blog_post = $this->lib_filter->go($this->input->post("blog_post"));

		$post_to_timeline = intval($this->input->post("post_to_timeline"));

		$blogid = intval($this->input->post("blogid"));



		if(empty($title)) {

			$this->template->error(lang("error_174"));

		}



		if(empty($blog_post)) {

			$this->template->error(lang("error_175"));

		}



		// Upload

		$this->load->library("upload");



		$blog_image = "";

		if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {

			$this->upload->initialize(array( 

		       "upload_path" => $this->settings->info->upload_path,

		       "overwrite" => FALSE,

		       "max_filename" => 1500,

		       "encrypt_name" => TRUE,

		       "remove_spaces" => TRUE,

		        "allowed_types" => 'jpg|gif|png|jpeg|JPG|PNG',

		       "max_size" => $this->settings->info->file_size,

		       "max_width" => 1500,

		       "max_height" => 1500

		    ));



		    if (!$this->upload->do_upload("userfile")) {

		    	$this->template->error(lang("error_21")

		    	.$this->upload->display_errors());

		    }



		    $data = $this->upload->data();



		    $blog_image = $data['file_name'];

		}



		$time = 0;

		if($status == 1) {

			$time = time();

		}



		$postid = $this->blog_model->add_news_post(array(

			"blogid"=>$blogid,

			"title" => $title,

			"body" => $blog_post,

			"timestamp" => $time,

			"status" => $status,

			"image" => $blog_image,

			"last_updated" => time()

			)

		);



		if($post_to_timeline && $status == 1) {

			// Make post

			// Add a feed post

			$postid = $this->feed_model->add_news_post(array(

				"userid" => $this->user->info->ID,

				"blog_postid" => $postid,

				"timestamp" => time(),

				"post_as" => "user",

				)

			);

		}



		$this->session->set_flashdata("globalmsg", lang("success_110"));

		redirect(site_url("admin/news_posts"));



	}

	

	

	

	

	

	

	

	

	

public function articles()

{

	$this->template->loadData("activeLink", 

			array("admin" => array("articles" => 1)));

			



	$this->template->loadContent("admin/articles.php", array(

			)

	);		



}



public function article_posts()

{

	$this->template->loadData("activeLink", 

			array("admin" => array("article_posts" => 1)));

			



	$this->template->loadContent("admin/articles_post.php", array(

			)

	);		



}



public function performas(){

	

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("performas" => 1)));



		$cats  = $this->user_model->get_catagories_data()->result_array();



		// echo "<pre>";

		// print_r($cats);

		// die();



		$data['cats'] = $cats;

		$this->template->loadContent("admin/performas.php", $data);

	

	}

	

	public function upgrade_requests()

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("upgrade_requests" => 1)));



		$this->template->loadContent("admin/upgrade_requests.php", array(

			)

		);

	

	}

	

	public function special_words()

	{

		 if(!$this->common->has_permissions(array("admin",

				"special_words"), $this->user)) {

				$this->template->error(lang("error_2"));

			}



		$this->template->loadData("activeLink", 

				array("admin" => array("special_words" => 1)));

		 

		

		$this->template->loadContent("admin/specialwords.php", array(

				)

			);

    

    }

    

    

    public function viewperformas()

    {

	

	$this->load->library("datatables");



		$this->datatables->set_default_order("performas.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"performas.title" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_performas()

		);

		$blogs = $this->admin_model->get_all_performas($this->datatables);



		foreach($blogs->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->title,

				$this->admin_model->get_category_name($r->category_id),

				

				'<a href="' . site_url("admin/edit_performa/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_performa/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	

	

	}

	

	public function delete_performa($id)

	{

	

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		

		$id = intval($id);

		$delete = $this->admin_model->delete_performa($id);

		$this->session->set_flashdata("globalmsg", "Performa Delete Successfully");

		redirect(site_url("admin/performas"));

	

	

	}

    

    public function view_special_words_page()

    {

		$this->load->library("datatables");



		$this->datatables->set_default_order("special_words.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"special_words.word" => 0

				 ),

				 1 => array(

				 	"special_words.link" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_words()

		);

		$blogs = $this->admin_model->get_all_special_words($this->datatables);



		foreach($blogs->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->word,

				"<a href='".$r->link."'>".$r->link."</a>",

				

				'<a href="' . site_url("admin/edit_special_word/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_special_word/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	

	}

	

	

	public function delete_special_word($id)

	{

	

	if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

	

	$id = intval($id);

	$delete = $this->admin_model->delete_special_word($id);

	$this->session->set_flashdata("globalmsg", "Word Delete Successfully");

	redirect(site_url("admin/special_words"));

	}

	

	public function deletesectionwithoutcontnet($id,$actid)

	{

		

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

	

	$id = intval($id);

	//echo $id; 

	$delete = $this->admin_model->delete_sectionwithoutchapter($id);

	$this->session->set_flashdata("globalmsg", "Section Delete Successfully");

	redirect(site_url("admin/view_bare_acts/".$actid));

	}

	public function deletechaptersss($id,$actid)

	{

		

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

	

	$id = intval($id);

	//echo $id; 

	$delete = $this->admin_model->delete_chaptersss($id);

	$this->session->set_flashdata("globalmsg", "Chapter Delete Successfully");

	redirect(site_url("admin/view_bare_acts/".$actid));

	}

	

	

	

	public function view_section_details($id)

	{

	

	if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

	

	$id = intval($id);

	

	$section_desc = $this->admin_model->view_section_details($id);

	

	$section_desc=$section_desc->row();

	

	

	$this->template->loadContent("admin/view_section_details.php", array(

	

			"section_desc"=>$section_desc,

	

			)

		);

	

	

	}

	

	

	

	

	

	

	

	

	

	

	

	public function categories(){

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("categories" => 1)));



		$parentcat= $this->db->where("parent_id",0)->get("catagories")->result();

		$this->template->loadContent("admin/categories.php", array(



				"parentcatagory"=>$parentcat,



			)

		);

		

		}



		public function get_total_judgements() 

		{	

		$this->template->loadData("activeLink", 

			array("admin" => array("judgements" => 1)));



		$this->template->loadContent("admin/judgements.php", array());



	}







	public function add_bareacts()

	{

		

	$this->template->loadData("activeLink", 

			array("admin" => array("add_bareacts" => 1)));



		$this->template->loadContent("admin/add_bareacts.php", array());

	

	}



	public function blog_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("user_blogs.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"user_blogs.title" => 0

				 ),

				 1 => array(

				 	"users.username" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_blogs()

		);

		$blogs = $this->admin_model->get_blogs($this->datatables);



		foreach($blogs->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->title,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				'<a href="' . site_url("admin/edit_blog/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_blog/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}

	

	

	public function news_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("user_news.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"user_news.title" => 0

				 ),

				 1 => array(

				 	"users.username" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_news()

		);

		$blogs = $this->admin_model->get_news($this->datatables);



		foreach($blogs->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->title,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				'<a href="' . site_url("admin/edit_news_catagory/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_news/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}

	public function articles_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("user_articles.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"user_articles.title" => 0

				 ),

				 1 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

					"catagories.c_name" => 0

				)

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_articles()

		);

		$blogs = $this->admin_model->get_articles($this->datatables);



		foreach($blogs->result() as $r) {

			//  print_r($blogs->result());

			//  die();

			$this->datatables->data[] = array(

				$r->title,

				$r->c_name,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				// '<a href="' . site_url("admin/edit_article/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>'. 
				'<a href="' . site_url("admin/delete_article/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			

			);

		

		//<a href="' . site_url("admin/preview_article/" . $r->ID) .'" class="btn btn-warning btn-xs" title="Preview"><span class="glyphicon glyphicon-cog"></span></a>  <a href="' . site_url("admin/preview_article/" . $r->ID) .'" class="btn btn-warning btn-xs" title="Set as Featured"><span class="glyphicon glyphicon-cog"></span></a>

		}

		echo json_encode($this->datatables->process());

	}



	public function get_preview_article_sections($id)

	{

	

		$this->load->model("article_model");

		$id = intval($id);

		$blog = $this->article_model->get_preview_article_sections($id);

		

		redirect(site_url("articles/preview/".$id));

	

	}



	public function delete_article($id)

	{

	

		$this->load->model("article_model");

		$id = intval($id);

		$blog = $this->article_model->get_user_articless($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$this->article_model->delete_post_article_main($id);

		// Delete posts

		$this->article_model->delete_post_article_sectionss($blog->ID);

		$this->session->set_flashdata("globalmsg", lang("success_104"));

		redirect(site_url("admin/articles"));

	

	}

	public function edit_blog($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$blog = $this->blog_model->get_blog($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("blogs" => 1)));

		$this->template->loadContent("admin/edit_blog.php", array(

			"blog" => $blog

			)

		);

	}

	public function edit_news_catagory($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$blog = $this->blog_model->get_news($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("news" => 1)));

		$this->template->loadContent("admin/edit_news_catgory.php", array(

			"blog" => $blog

			)

		);

	}

	public function edit_special_word($id) 

	{

		

		$id = intval($id);

		$words = $this->admin_model->get_total_words_by_id($id);

		

		$words = $words->row();



	

			

			

		$this->template->loadContent("admin/edit_special_word.php", array(

			"words" => $words

			)

		);

	}

	

	



	public function edit_special_word_pro($id) 

	{

		

		$id = intval($id);

		$blog = $this->admin_model->get_total_words_by_id($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		

		$blog = $blog->row();

		$word = $this->common->nohtml($this->input->post("word"));

		$link = $this->common->nohtml($this->input->post("link"));



		

		if(empty($word)) {

			$this->template->error(lang("error_172"));

		}



		$this->admin_model->update_special_word($id, array(

			"word" => $word,

			"link" => $link,

			"updated_at" => date('Y-m-d'),

			)

		);



		$this->session->set_flashdata("globalmsg", "Word updated successfuly");

		redirect(site_url("admin/special_words"));

	}

	public function edit_blog_pro($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$blog = $this->blog_model->get_blog($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$title = $this->common->nohtml($this->input->post("title"));

		$description = $this->common->nohtml($this->input->post("description"));



		$private = intval($this->input->post("private"));



		if(empty($title)) {

			$this->template->error(lang("error_172"));

		}



		$this->blog_model->update_blog($id, array(

			"title" => $title,

			"description" => $description,

			"private" => $private,

			"timestamp" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_103"));

		redirect(site_url("admin/blogs"));

	}

	public function edit_news_cat_pro($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$blog = $this->blog_model->get_news($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$title = $this->common->nohtml($this->input->post("title"));

		$description = $this->common->nohtml($this->input->post("description"));



		$private = intval($this->input->post("private"));



		if(empty($title)) {

			$this->template->error(lang("error_172"));

		}



		$this->blog_model->update_news_cat($id, array(

			"title" => $title,

			"description" => $description,

			"private" => $private,

			"timestamp" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", "News catagory update successfuly. ");

		redirect(site_url("admin/news"));

	}



	public function delete_blog($id, $hash) 

	{

		$this->load->model("blog_model");

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$blog = $this->blog_model->get_blog($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$this->blog_model->delete_blog($id);

		// Delete posts

		$this->blog_model->delete_all_blog_posts($blog->ID);

		$this->session->set_flashdata("globalmsg", lang("success_104"));

		redirect(site_url("admin/blogs"));



	}

	public function delete_news($id, $hash) 

	{

		$this->load->model("blog_model");

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$blog = $this->blog_model->get_news($id);

		if($blog->num_rows() == 0) {

			$this->template->error(lang("error_171"));

		}

		$blog = $blog->row();



		$this->blog_model->delete_news($id);

		// Delete posts

		$this->blog_model->delete_all_news_posts($blog->ID);

		$this->session->set_flashdata("globalmsg", "News Catagory with All News Posts Delete Successfully.,..");

		redirect(site_url("admin/news"));



	}



	public function blog_posts() 

	{

		$this->template->loadData("activeLink", 

			array("admin" => array("blog_posts" => 1)));

		$this->template->loadContent("admin/blog_posts.php", array(

			)

		);

	}

	public function news_posts() 

	{

		$this->template->loadData("activeLink", 

			array("admin" => array("news_posts" => 1)));

		$this->template->loadContent("admin/news_posts.php", array(

			)

		);

	}

	

	

	public function manage_pages()

	{

		

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("members" => 1)));

		

		$user_roles = $this->admin_model->get_user_roles();



		$fields = $this->user_model->get_custom_fields(array("register"=>1));



		$this->template->loadContent("admin/manage_pages.php", array(

			"user_roles" => $user_roles,

			"fields" => $fields

			)

		);	

	

	}



	public function blog_post_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("user_blog_posts.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 1 => array(

				 	"user_blog_posts.title" => 0

				 ),

				 1 => array(

				 	"user_blog_posts.status" => 0

				 ),

				 2 => array(

				 	"user_blog_posts.views" => 0

				 ),

				 3 => array(

				 	"users.username" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_blog_posts()

		);

		$blogs = $this->admin_model->get_blog_posts($this->datatables);



		foreach($blogs->result() as $r) {



			if($r->status == 0) {

				$status = lang("ctn_768");

			} elseif($r->status == 1) {

				$status = lang("ctn_794");

			}



			if(!empty($r->image)) {

				$image = '<img src="'.base_url() . $this->settings->info->upload_path_relative.'/' . $r->image.'" class="blog-post-thumb">';

			} else {

				$image = "";

			}



			if($r->timestamp > 0) {

				$time = date($this->settings->info->date_format, $r->timestamp);

			} else {

				$time = lang("ctn_795");

			}

			 

			$this->datatables->data[] = array(

				$image,

				$r->title,

				$status,

				$r->views,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				'<a href="' . site_url("admin/edit_blog_post/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_blog_post/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		

		echo json_encode($this->datatables->process());

	

	}

	public function news_post_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("user_news_posts.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 1 => array(

				 	"user_news_posts.title" => 0

				 ),

				 1 => array(

				 	"user_news_posts.status" => 0

				 ),

				 2 => array(

				 	"user_news_posts.views" => 0

				 ),

				 3 => array(

				 	"users.username" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_news_posts()

		);

		$blogs = $this->admin_model->get_news_posts($this->datatables);



		foreach($blogs->result() as $r) {



			if($r->status == 0) {

				$status = lang("ctn_768");

			} elseif($r->status == 1) {

				$status = lang("ctn_794");

			}



			if(!empty($r->image)) {

				$image = '<img src="'.base_url() . $this->settings->info->upload_path_relative.'/' . $r->image.'" class="blog-post-thumb">';

			} else {

				$image = "";

			}



			if($r->timestamp > 0) {

				$time = date($this->settings->info->date_format, $r->timestamp);

			} else {

				$time = lang("ctn_795");

			}

			 

			$this->datatables->data[] = array(

				$image,

				$r->title,

				$status,

				$r->views,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				'<a href="' . site_url("admin/edit_news_post_pro/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_news_post/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		

		echo json_encode($this->datatables->process());

	

	}



	public function edit_blog_post($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$post = $this->blog_model->get_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("blog_posts" => 1)));

		$this->template->loadContent("admin/edit_blog_post.php", array(

			"post" => $post

			)

		);

	}

	public function edit_news_post_pro($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$post = $this->blog_model->get_news_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("news_posts" => 1)));

		$this->template->loadContent("admin/edit_news_post.php", array(

			"post" => $post

			)

		);

	}



	public function edit_blog_post_pro($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$post = $this->blog_model->get_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("blog_posts" => 1)));



		$title = $this->common->nohtml($this->input->post("title"));

		$status = intval($this->input->post("status"));

		$blog_post = $this->lib_filter->go($this->input->post("blog_post"));



		if(empty($title)) {

			$this->template->error(lang("error_174"));

		}



		if(empty($blog_post)) {

			$this->template->error(lang("error_175"));

		}



		// Upload

		$this->load->library("upload");



		$blog_image = $post->image;

		if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {

			$this->upload->initialize(array( 

		       "upload_path" => $this->settings->info->upload_path,

		       "overwrite" => FALSE,

		       "max_filename" => 300,

		       "encrypt_name" => TRUE,

		       "remove_spaces" => TRUE,

		       "allowed_types" => "gif|png|jpg|jpeg",

		       "max_size" => $this->settings->info->file_size,

		       "max_width" => 800,

		       "max_height" => 800

		    ));



		    if (!$this->upload->do_upload("userfile")) {

		    	$this->template->error(lang("error_21")

		    	.$this->upload->display_errors());

		    }



		    $data = $this->upload->data();



		    $blog_image = $data['file_name'];

		}



		if($status && $post->timestamp == 0) {

			$time = time();

		} else {

			$time = $post->timestamp;

		}



		$this->blog_model->update_post($id, array(

			"title" => $title,

			"body" => $blog_post,

			"status" => $status,

			"image" => $blog_image,

			"timestamp" => $time,

			"last_updated" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_105"));

		redirect(site_url("admin/blog_posts"));

	}

	public function edit_news_post_prosss($id) 

	{

		$this->load->model("blog_model");

		$id = intval($id);

		$post = $this->blog_model->get_news_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("blog_posts" => 1)));



		$title = $this->common->nohtml($this->input->post("title"));

		$status = intval($this->input->post("status"));

		$blog_post = $this->lib_filter->go($this->input->post("blog_post"));



		if(empty($title)) {

			$this->template->error(lang("error_174"));

		}



		if(empty($blog_post)) {

			$this->template->error(lang("error_175"));

		}



		// Upload

		$this->load->library("upload");



		$blog_image = $post->image;

		if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {

			$this->upload->initialize(array( 

		       "upload_path" => "uploads",

		       "overwrite" => FALSE,

		       "max_filename" => 300,

		       "encrypt_name" => TRUE,

		       "remove_spaces" => TRUE,

		       "allowed_types" => "gif|png|jpg|jpeg",

		       "max_size" => $this->settings->info->file_size,

		       "max_width" => 800,

		       "max_height" => 800

		    ));



		    if (!$this->upload->do_upload("userfile")) {

		    	$this->template->error(lang("error_21")

		    	.$this->upload->display_errors());

		    }



		    $data = $this->upload->data();



		    $blog_image = $data['file_name'];

		}



		if($status && $post->timestamp == 0) {

			$time = time();

		} else {

			$time = $post->timestamp;

		}



		$this->blog_model->update_news_post($id, array(

			"title" => $title,

			"body" => $blog_post,

			"status" => $status,

			"image" => $blog_image,

			"timestamp" => $time,

			"last_updated" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", "News Post update successfuly..");

		redirect(site_url("admin/news_posts"));

	}



	public function delete_blog_post($id, $hash) 

	{

		$this->load->model("blog_model");

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$post = $this->blog_model->get_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->blog_model->delete_post($id);

		$this->session->set_flashdata("globalmsg", lang("success_106"));

		redirect(site_url("admin/blog_posts"));

	}

	public function delete_news_post($id, $hash) 

	{

		$this->load->model("blog_model");

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$post = $this->blog_model->get_news_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->blog_model->delete_news_post($id);

		$this->session->set_flashdata("globalmsg", lang("success_106"));

		redirect(site_url("admin/news_posts"));

	}



	public function verified_requests() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("verified_requests" => 1)));

		$this->template->loadContent("admin/verified_requests.php", array(

			)

		);

	}

	

	

	public function verify_agency()

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("verify_agency" => 1)));

		$this->template->loadContent("admin/verify_agency.php", array(

			)

		);

			

    }

    

    public function verify_university()

    {

	

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("verify_university" => 1)));

		$this->template->loadContent("admin/verify_university.php", array(

			)

		);

	

	

	}

    

    

    public function upgrade_requests_page()

    {

	

	if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->load->library("datatables");

		$this->datatables->set_default_order("verification_proof.ID", "desc");

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

				 	"verification_proof.userid" => 0

				 ),

				 2 => array(

				 	"verification_proof.proof" => 0

				 ),

				 3 => array(

				 	"verification_proof.disclaimer" => 0

				 ),

				 

			)

		);

	$this->datatables->set_total_rows(

			$this->admin_model->get_total_upgrade_req()

		);

	$req = $this->admin_model->get_upgrade_requests($this->datatables);

	foreach($req->result() as $r) {

			if($r->disclaimer==1){

					$disclaimer="Accepted";

				}

			else{

				$disclaimer= "Not Accepted";

				}

			if($r->status==0){	

		    $htmlbutton='

						 <a href="'.site_url("admin/accept_upgrade_requests/" . $r->userid ."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_623").'</a> 

						 <a href="'.site_url("admin/reject_verified_agency/" . $r->userid . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_624").'</a> 

					';

			}

			else if($r->status==1){

				  $htmlbutton='

						 <a href="'.site_url("admin/downgrade_upgrade_requests/" . $r->userid ."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">Downgrade</a> 

					';

				}

			else if($r->status==2){

			 $htmlbutton='

						 <a href="'.site_url("admin/upgrade_student_to_advocate/" . $r->userid ."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">Upgrade</a> 

					';

				}

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				'<a href="'.base_url().$this->settings->info->upload_path_relative.'/'.$r->proof.'" download>Download Proof Document</a>',

				$disclaimer,

				$htmlbutton,

				);

		}

		echo json_encode($this->datatables->process());

	}

    

    

    public function upgrade_student_to_advocate($id,$hash)

    {

	

	if($hash != $this->security->get_csrf_hash()) {

				$this->template->error(lang("error_6"));

			}

		$id = intval($id);

			$this->user_model->update_user($id, array(

				"profile_identification" => 0,

			)

		);

		$this->admin_model->update_upgrade_reqests_table($id, array(

				"status" => 1,

			)

		);

		$this->session->set_flashdata("globalmsg", "Profile has been Upgraded...");

		redirect(site_url("admin/upgrade_requests"));

	}

    

    public function downgrade_upgrade_requests($id,$hash)

    {

		if($hash != $this->security->get_csrf_hash()) {

				$this->template->error(lang("error_6"));

			}

		$id = intval($id);

			$this->user_model->update_user($id, array(

				"profile_identification" => 2,

			)

		);

		$this->admin_model->update_upgrade_reqests_table($id, array(

				"status" => 2,

			)

		);

		$this->session->set_flashdata("globalmsg", "Profile has been Downgraded...");

		redirect(site_url("admin/upgrade_requests"));

	}

    

    public function accept_upgrade_requests($id,$hash)

    {

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

			$this->user_model->update_user($id, array(

				"profile_identification" => 0,

			)

		);

		$this->admin_model->update_upgrade_reqests_table($id, array(

				"status" => 1,

			)

		);

		$this->session->set_flashdata("globalmsg", "Request is Accepted...");

		redirect(site_url("admin/upgrade_requests"));

	}

	

	public function verify_university_page()

	{

	$this->load->library("datatables");

	$this->datatables->set_default_order("university.ID", "desc");

	// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

				 	"universities.universityname" => 0

				 ),

				 2 => array(

				 	"university.contact_name" => 0

				 ),

				 3 => array(

				 	"university.uni_email" => 0

				 ),

				 4 => array(

				 	"university.uni_phone" => 0

				 ),

				 5 => array(

				 	"university.proof" => 0

				 ),

				 6 => array(

				 	"university.city" => 0

				 ),

				 7 => array(

				 	"university.map_location" => 0

				 ),

				 8 => array(

				 	"university.p_address" => 0

				 ),

				 9 => array(

				 	"university.created_at" => 0

				 ),

				 10 => array(

				 	"university.status" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model->get_total_universities()

		);

		$req = $this->admin_model->get_verify_universities_requests($this->datatables);

		foreach($req->result() as $r) {

			if($r->status==1){

				$status=lang("ctn_851");

				}

			if($r->status==2){

				$status=lang("ctn_850");

				}

		    if($r->status==3){

				$status=lang("ctn_853");

				}

			if($r->status==4){

				$status=lang("ctn_855");

				}

			 if($r->status==1){

					$htmlbutton='

						 <a href="'.site_url("admin/accept_university/" . $r->ID . "/" . $r->uni_email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_623").'</a> 

						 <a href="'.site_url("admin/reject_verified_agency/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_624").'</a> 

					';

				 }

		     if($r->status==2){

				 $htmlbutton='

						 <a href="'.site_url("admin/suspend_verified_agency/" . $r->ID . "/" . $r->uni_email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_854").'</a> 

					';

				 

				 }

			 if($r->status==3){

				 

				 $htmlbutton='

						<a href="'.site_url("admin/accept_university/" . $r->ID . "/" . $r->uni_email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_857").'</a> 

						<a href="'.site_url("admin/delete_verified_agency/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_859").'</a> 

					';

			  }

			if($r->status==4){

				$htmlbutton='

						 <a href="'.site_url("admin/accept_verified_agency/" . $r->ID . "/" . $r->uni_email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_623").'</a> 

						 <a href="'.site_url("admin/delete_verified_agency/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_859").'</a> 

					';

				}

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				$r->universityname,$r->contact_name,$r->uni_email,$r->uni_phone,'<a href="'.base_url().$this->settings->info->upload_path_relative.'/'.$r->proof.'" download><img  src="'.base_url().$this->settings->info->upload_path_relative.'/'.$r->proof.'" width="100" height="100" id="admin_agency_image" alt="'.$r->proof.'"/></a>',$r->city,$r->map_location,$r->p_address,$r->created_at,$status,$htmlbutton,

				);

		}

		echo json_encode($this->datatables->process());

	}

    

    

    public function verify_agency_page(){

		$this->load->library("datatables");

		$this->datatables->set_default_order("agency.id", "desc");

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

				 	"agency.agency_name" => 0

				 ),

				 2 => array(

				 	"agency.contact_name" => 0

				 ),

				 3 => array(

				 	"agency.email" => 0

				 ),

				 4 => array(

				 	"agency.phone" => 0

				 ),

				 5 => array(

				 	"agency.city" => 0

				 ),

				 6 => array(

				 	"agency.address" => 0

				 ),

				 7 => array(

				 	"agency.registration_number" => 0

				 ),

				 8 => array(

				 	"agency.registration_proof" => 0

				 ),

				 9 => array(

				 	"agency.registration_proof" => 0

				 ),

				 10 => array(

				 	"agency.status" => 0

				 ),

				 11 =>array(

				 	"agency.created_at" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model->get_total_agencies()

		);

		$req = $this->admin_model->get_verify_agency_requests($this->datatables);

		foreach($req->result() as $r) {

			if($r->status==1){

				$status=lang("ctn_851");

				}

			if($r->status==2){

				$status=lang("ctn_850");

				}

		    if($r->status==3){

				$status=lang("ctn_853");

				}

			if($r->status==4){

				$status=lang("ctn_855");

				}

			 if($r->status==1){

					$htmlbutton='

						 <a href="'.site_url("admin/accept_verified_agency/" . $r->id . "/" . $r->email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_623").'</a> 

						 <a href="'.site_url("admin/reject_verified_agency/" . $r->id . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_624").'</a> 

					';

				 }

		     if($r->status==2){

				 $htmlbutton='

						 <a href="'.site_url("admin/suspend_verified_agency/" . $r->id . "/" . $r->email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_854").'</a> 

					';

				 }

			 if($r->status==3){

				 $htmlbutton='

						<a href="'.site_url("admin/accept_verified_agency/" . $r->id . "/" . $r->email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_857").'</a> 

						<a href="'.site_url("admin/delete_verified_agency/" . $r->id . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_859").'</a> 

					';

			  }

			if($r->status==4){

				$htmlbutton='

						 <a href="'.site_url("admin/accept_university/" . $r->id . "/" . $r->email."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_623").'</a> 

						 <a href="'.site_url("admin/delete_verified_agency/" . $r->id . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_859").'</a> 

					';

				}

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				$r->agency_name,$r->contact_name,$r->email,$r->phone,$r->city,$r->address,$r->registration_number,'<a href="'.base_url().$this->settings->info->upload_path_relative.'/'.$r->registration_proof.'" download><img  src="'.base_url().$this->settings->info->upload_path_relative.'/'.$r->registration_proof.'" width="100" height="100" id="admin_agency_image" alt="'.$r->registration_proof.'"/></a>',$status,$r->created_at, $htmlbutton,

				);

		}

		echo json_encode($this->datatables->process());

		}



	public function verified_request_page() 

	{

		$this->load->library("datatables");

		$this->datatables->set_default_order("verified_requests.ID", "desc");

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

				 	"verified_requests.about" => 0

				 ),

				 2 => array(

				 	"verified_requests.timestamp" => 0

				 ),

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_verified_requests()

		);

		$req = $this->admin_model->get_verified_requests($this->datatables);

		foreach($req->result() as $r) {

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				$r->about,

				date($this->settings->info->date_format, $r->timestamp),

				'<a href="'.site_url("admin/accept_verified/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_verified/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> '

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function reject_verified_agency($id, $hash)

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verify_agency_request($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		$this->user_model->update_user($req->created_by_user_id, array(

				"agency_verified" => 4

			)

		);

		$this->agency_model->update_agency($req->created_by_user_id, array(

				"status" => 4

			)

		);

		$user = $this->user_model->get_user_by_id($req->created_by_user_id);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_747"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_88"));

		redirect(site_url("admin/verify_agency"));

	}

	



	public function reject_verified($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verified_request($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		// Set status to active

		$this->admin_model->delete_verified_request($id);

		$user = $this->user_model->get_user_by_id($req->userid);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_747"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_88"));

		redirect(site_url("admin/verified_requests"));

	}





	public function delete_verified_agency($id, $hash){

			if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verify_agency_request($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		$this->user_model->update_user($req->created_by_user_id, array(

				"agency_verified" => 0,

				"is_agency_joined"=>0,

			)

		);

		$this->agency_model->update_agency($req->created_by_user_id, array(

				"status" => 0

			)

		);

	    $this->admin_model->delete_agency_verified_request($id);

		$user = $this->user_model->get_user_by_id($req->created_by_user_id);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_803"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_113"));

		redirect(site_url("admin/verify_agency"));

		}





	public function suspend_verified_agency($id,$email, $hash){

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verify_agency_request($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		$this->user_model->update_user($req->created_by_user_id, array(

				"agency_verified" => 3

			)

		);

		$this->agency_model->update_agency($req->created_by_user_id, array(

				"status" => 3,

			)

		);

		

		$this->agency_model->update_page_by_email($email,array("is_active" => 0));

		

		$user = $this->user_model->get_user_by_id($req->created_by_user_id);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_802"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_112"));

		redirect(site_url("admin/verify_agency"));

		

		

		}



	public function accept_university($id,$email,$hash){

			if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verify_universityyy_requests($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		$this->user_model->update_user($req->created_by_user_id, array(

				"agency_verified" => 2

			)

		);

		$this->agency_model->update_university($req->created_by_user_id, array(

				"status" => 2,

			)

		);

		$this->agency_model->update_page_by_email($email,array("is_active" => 0));

		$user = $this->user_model->get_user_by_id($req->created_by_user_id);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_798"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_87"));

		redirect(site_url("admin/verify_university"));

		}





	public function accept_verified_agency($id,$email, $hash)

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verify_agency_request($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		$this->user_model->update_user($req->created_by_user_id, array(

				"agency_verified" => 2

			)

		);

		$this->agency_model->update_agency($req->created_by_user_id, array(

				"status" => 2,

			)

		);

		$this->agency_model->update_page_by_email($email,array("is_active" => 1));

		$user = $this->user_model->get_user_by_id($req->created_by_user_id);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_798"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_87"));

		redirect(site_url("admin/verify_agency"));

	}

	public function accept_verified($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$req = $this->admin_model->get_verified_request($id);

		if($req->num_rows() == 0) {

			$this->template->error(lang("error_155"));

		}

		$req = $req->row();

		$this->admin_model->delete_agency_verified_request($id);

		$this->user_model->update_user($req->userid, array(

			"verified" => 1

			)

		);

		$user = $this->user_model->get_user_by_id($req->userid);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "user_settings",

				"timestamp" => time(),

				"message" => lang("ctn_748"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}

		$this->session->set_flashdata("globalmsg", lang("success_87"));

		redirect(site_url("admin/verified_requests"));

	}



	public function promoted_posts() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("promoted_posts" => 1)));

		$this->template->loadContent("admin/promoted_posts.php", array(

			)

		);

	}



	public function promoted_post_page() 

	{

		$this->load->library("datatables");

		$this->datatables->set_default_order("promoted_posts.ID", "desc");

		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"promoted_posts.postid" => 0

				 ),

				 1 => array(

				 	"promoted_posts.status" => 0

				 ),

				 2 => array(

				 	"promoted_posts.location" => 0

				 ),

				 3 => array(

				 	"promoted_posts.user_type" => 0

				 ),

				 4 => array(

				 	"promoted_posts.pageviews" => 0

				 ),

				 5 => array(

				 	"users.username" => 0

				 ),

				 6 => array(

				 	"promoted_posts.timestamp" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_promoted_posts()

		);

		$ads = $this->admin_model->get_promoted_posts($this->datatables);

		foreach($ads->result() as $r) {

			$options = "";

			if($r->status == 0) {

				$status = lang("ctn_701");

				$options .= '<a href="'.site_url("admin/accept_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> ';

			} elseif($r->status == 1) {

				$status = lang("ctn_702");

			} elseif($r->status == 2) {

				$status = lang("ctn_703");

			}

			$this->datatables->data[] = array(

				'<a href="'.site_url("home/index/3?postid=" . $r->postid).'">'.lang("ctn_749").'</a>',

				

				$r->location,

				unserialize($r->user_type),

				$status,

				$r->pageviews,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				date($this->settings->info->date_format, $r->timestamp),

				$options . '<a href="' . site_url("admin/edit_promoted_post/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_promoted_post/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function edit_promoted_post($id) 

	{

		$id = intval($id);

		$post = $this->admin_model->get_promoted_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_156"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("promoted_posts" => 1)));

		$this->template->loadContent("admin/edit_promoted_post.php", array(

			"post" => $post

			)

		);

	}



	public function edit_promoted_post_pro($id) 

	{

		$id = intval($id);

		$post = $this->admin_model->get_promoted_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_156"));

		}

		$post = $post->row();



		$status = intval($this->input->post("status"));

		$pageviews = intval($this->input->post("pageviews"));



		$this->admin_model->update_promoted_post($id, array(

			"status" => $status,

			"pageviews" => $pageviews

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_89"));

		redirect(site_url("admin/promoted_posts"));

	}



	public function delete_promoted_post($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$post = $this->admin_model->get_promoted_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_156"));

		}

		$post = $post->row();



		$this->admin_model->delete_promoted_post($id);

		$this->session->set_flashdata("globalmsg", lang("success_90"));

		redirect(site_url("admin/promoted_posts"));

	}



	public function accept_post($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$post = $this->admin_model->get_promoted_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_156"));

		}

		$post = $post->row();



		// Set status to active

		$this->admin_model->update_promoted_post($id, array(

			"status" => 2,

			)

		);



		$user = $this->user_model->get_user_by_id($post->userid);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "home/index/3?postid=" . $post->postid,

				"timestamp" => time(),

				"message" => lang("ctn_750"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}



		$this->session->set_flashdata("globalmsg", lang("success_91"));

		redirect(site_url("admin/promoted_posts"));

	}



	public function reject_post($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$post = $this->admin_model->get_promoted_post($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_156"));

		}

		$post = $post->row();

		// Calculate credits to give back

		$amount = floatval($post->pageviews/1000);

		$credits = $amount * $this->settings->info->credit_price_pageviews;

		// Set status to active

		$this->admin_model->delete_promoted_post($id);

		// Send notification

		$user = $this->user_model->get_user_by_id($post->userid);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "home/index/3?postid=" . $post->postid,

				"timestamp" => time(),

				"message" => lang("ctn_751") . " " . $credits ." " . lang("ctn_350"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);



			// Update points

			$this->user_model->update_user($user->ID, array(

				"points" => $user->points + $credits

				)

			);

		}



		$this->session->set_flashdata("globalmsg", lang("success_92"));

		redirect(site_url("admin/promoted_posts"));

	}



	public function rotation_ads() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("rotation_ads" => 1)));

		$this->template->loadContent("admin/rotation_ads.php", array(

			)

		);

	}



	public function rotation_ad_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("rotation_ads.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"rotation_ads.name" => 0

				 ),

				 1 => array(

				 	"rotation_ads.status" => 0

				 ),

				 2 => array(

				 	"rotation_ads.pageviews" => 0

				 ),

				 3 => array(

				 	"users.username" => 0

				 ),

				 4 => array(

				 	"rotation_ads.timestamp" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_rotation_ads()

		);

		$ads = $this->admin_model->get_rotation_ads($this->datatables);



		foreach($ads->result() as $r) {



			$options = "";



			if($r->status == 0) {

				$status = lang("ctn_701");

				$options .= '<a href="'.site_url("admin/accept_ad/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_ad/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> ';

			} elseif($r->status == 1) {

				$status = lang("ctn_702");

			} elseif($r->status == 2) {

				$status = lang("ctn_703");

			}



			 

			$this->datatables->data[] = array(

				$r->name,

				$status,

				$r->pageviews,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				date($this->settings->info->date_format, $r->timestamp),

				$options . '<a href="' . site_url("admin/edit_rotation_ad/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/delete_rotation_ad/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function add_rotation_ad() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$name = $this->common->nohtml($this->input->post("name"));

		$advert = $this->lib_filter->go($this->input->post("advert"));

		$status = intval($this->input->post("status"));

		$pageviews = intval($this->input->post("pageviews"));



		if(empty($name)) {

			$this->template->error(lang("error_157"));

		}



		$this->admin_model->add_rotation_ad(array(

			"name" => $name,

			"advert" => $advert,

			"status" => $status,

			"pageviews" => $pageviews,

			"userid" => $this->user->info->ID,

			"timestamp" => time()

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_93"));

		redirect(site_url("admin/rotation_ads"));

	}



	public function edit_rotation_ad($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$ad = $this->admin_model->get_rotation_ad($id);

		if($ad->num_rows() == 0) {

			$this->template->error(lang("error_158"));

		}

		$ad = $ad->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("rotation_ads" => 1)));

		$this->template->loadContent("admin/edit_rotation_ad.php", array(

			"ad" => $ad

			)

		);

	}



	public function edit_rotation_ad_pro($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$ad = $this->admin_model->get_rotation_ad($id);

		if($ad->num_rows() == 0) {

			$this->template->error(lang("error_158"));

		}

		$ad = $ad->row();



		$name = $this->common->nohtml($this->input->post("name"));

		$advert = $this->lib_filter->go($this->input->post("advert"));

		$status = intval($this->input->post("status"));

		$pageviews = intval($this->input->post("pageviews"));



		if(empty($name)) {

			$this->template->error(lang("error_157"));

		}



		$this->admin_model->update_rotation_ad($id, array(

			"name" => $name,

			"advert" => $advert,

			"status" => $status,

			"pageviews" => $pageviews

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_94"));

		redirect(site_url("admin/rotation_ads"));

	}



	public function delete_rotation_ad($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$ad = $this->admin_model->get_rotation_ad($id);

		if($ad->num_rows() == 0) {

			$this->template->error(lang("error_158"));

		}



		$this->admin_model->delete_rotation_ad($id);

		$this->session->set_flashdata("globalmsg", lang("success_95"));

		redirect(site_url("admin/rotation_ads"));

	}



	public function accept_ad($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$ad = $this->admin_model->get_rotation_ad($id);

		if($ad->num_rows() == 0) {

			$this->template->error(lang("error_158"));

		}

		$ad = $ad->row();



		// Set status to active

		$this->admin_model->update_rotation_ad($id, array(

			"status" => 2,

			)

		);



		$user = $this->user_model->get_user_by_id($ad->userid);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "funds",

				"timestamp" => time(),

				"message" => lang("ctn_752"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);

		}



		$this->session->set_flashdata("globalmsg", lang("success_96"));

		redirect(site_url("admin/rotation_ads"));

	}



	public function reject_ad($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$ad = $this->admin_model->get_rotation_ad($id);

		if($ad->num_rows() == 0) {

			$this->template->error(lang("error_158"));

		}

		$ad = $ad->row();



		// Calculate credits to give back

		$amount = floatval($ad->pageviews/1000);

		$credits = $amount * $this->settings->info->credit_price_pageviews;



		// Set status to active

		$this->admin_model->delete_rotation_ad($id);





		// Send notification

		$user = $this->user_model->get_user_by_id($ad->userid);

		if($user->num_rows() > 0) {

			$user = $user->row(); 

			// Alert ad buyer

			$this->user_model->increment_field($user->ID, "noti_count", 1);

			$this->user_model->add_notification(array(

				"userid" => $user->ID,

				"url" => "funds",

				"timestamp" => time(),

				"message" => lang("ctn_753") . " " . $credits ." " . lang("ctn_350"),

				"status" => 0,

				"fromid" => $this->user->info->ID,

				"username" => $user->username,

				"email" => $user->email,

				"email_notification" => $user->email_notification

				)

			);



			// Update points

			$this->user_model->update_user($user->ID, array(

				"points" => $user->points + $credits

				)

			);

		}



		$this->session->set_flashdata("globalmsg", lang("success_97"));

		redirect(site_url("admin/rotation_ads"));

	}



	public function ad_settings() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("ad_settings" => 1)));



		$username = "";

		if($this->settings->info->rotation_ad_alert_user > 0) {

			$user = $this->user_model->get_user_by_id($this->settings->info->rotation_ad_alert_user);

			if($user->num_rows() == 0) {

				$username = "";

			} else {

				$user = $user->row();

				$username = $user->username;

			}

		}



		$this->template->loadContent("admin/ad_settings.php", array(

			"username" => $username

			)

		);

	}



	public function ad_settings_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$enable_google_ads_feed = 

			intval($this->input->post("enable_google_ads_feed"));

		$enable_google_ads_pages = 

			intval($this->input->post("enable_google_ads_pages"));

		$enable_rotation_ads_feed = 

			intval($this->input->post("enable_rotation_ads_feed"));

		$enable_rotation_ads_pages = 

			intval($this->input->post("enable_rotation_ads_pages"));

		$credit_price_pageviews = intval($this->input->post("credit_price_pageviews"));

		$rotation_ad_alert_user = $this->common->nohtml($this->input->post("rotation_ad_alert_user"));

		$enable_promote_post = intval($this->input->post("enable_promote_post"));

		$enable_verified_buy = intval($this->input->post("enable_verified_buy"));

		$verified_cost = floatval($this->input->post("verified_cost"));

		$enable_verified_requests = intval($this->input->post("enable_verified_requests"));



		$userid = 0;

		if(!empty($rotation_ad_alert_user)) {

			$user = $this->user_model->get_user_by_username($rotation_ad_alert_user);

			if($user->num_rows() == 0) {

				$this->template->error(lang("error_52"));

			}

			$user = $user->row();

			$userid = $user->ID;

		}

		



		$this->admin_model->updateSettings(

			array(

				"enable_google_ads_feed" => $enable_google_ads_feed,

				"enable_google_ads_pages" => $enable_google_ads_pages,

				"enable_rotation_ads_feed" => $enable_rotation_ads_feed,

				"enable_rotation_ads_pages" => $enable_rotation_ads_pages,

				"credit_price_pageviews" => $credit_price_pageviews,

				"rotation_ad_alert_user" => $userid,

				"enable_promote_post" => $enable_promote_post,

				"enable_verified_buy" => $enable_verified_buy,

				"verified_cost" => $verified_cost,

				"enable_verified_requests" => $enable_verified_requests

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_13"));

		redirect(site_url("admin/ad_settings"));

	}



	public function reports() 

	{

		$this->template->loadData("activeLink", 

			array("admin" => array("reports" => 1)));

		$this->template->loadContent("admin/reports.php", array(

			)

		);

	}



	public function report_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("reports.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"page_categories.name" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_reports()

		);

		$reports = $this->admin_model->get_reports($this->datatables);



		foreach($reports->result() as $r) {



			if(isset($r->reported_username)) {

				$report = $this->common->get_user_display(array("username" => $r->reported_username, "avatar" => $r->reported_avatar, "online_timestamp" => $r->reported_online_timestamp, "first_name" => $r->reported_first_name, "last_name" => $r->reported_last_name));

				$type = lang("ctn_339");

			} else {

				$report = "<a href=''>" . $r->page_name . "</a>";

				$type = lang("ctn_552");

			}

			 

			$this->datatables->data[] = array(

				$report,

				$type,

				$r->reason,

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				date($this->settings->info->date_format, $r->timestamp),

				'<a href="' . site_url("admin/delete_report/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function delete_report($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$report = $this->admin_model->get_report($id);

		if($report->num_rows() == 0) {

			$this->template->error(lang("error_81"));

		}

		$report = $report->row();



		$this->admin_model->delete_report($id);

		$this->session->set_flashdata("globalmsg", lang("success_48"));

		redirect(site_url("admin/reports"));

	}



	public function page_categories() 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("page_categories" => 1)));



		$this->template->loadContent("admin/page_categories.php", array(

			)

		);

	}



	public function page_categories_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("page_categories.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"page_categories.name" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_page_categories()

		);

		$categories = $this->admin_model->get_page_categories($this->datatables);



		foreach($categories->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->name,

				$r->description,

				$r->price,

				'<a href="' . site_url("admin/edit_page_cat/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . site_url("admin/delete_page_cat/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>

				

				<a href="' . site_url("admin/view_sub_cats/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'"  class="btn btn-success btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-eye-open"></span></a>

				'

			);

		}

		echo json_encode($this->datatables->process());

	}







	public function add_performa()

	{

		 $title = $this->common->nohtml($this->input->post("title"));

		 $status = $this->common->nohtml($this->input->post("status"));

		 $category_id = $this->input->post("category_id");

		 $content = $this->common->nohtml($this->input->post("content"));

		 $featured = $this->common->nohtml($this->input->post("featured"));



		//  print_r($_POST);

		//  die();



	

		if(empty($title)) {

			$this->template->error(lang("error_82"));

		}

		if(empty($category_id)) {

			$this->template->error("Category field is required");

		}

		$this->admin_model->add_performa(array(

			"title" => $title,

			"content" => $content,

			"category_id" => $category_id,

			"status" => $status,

			"featured" => $featured

			)

		);



		$this->session->set_flashdata("globalmsg", "Performa Add Successfully...");

		redirect(site_url("admin/performas"));

	}

	public function edit_performa($id){

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("performas" => 1)));



		$cats  = $this->user_model->get_catagories_data()->result_array();

		$info  = $this->admin_model->get_performa_data($id);

		// echo "<pre>";

		// print_r($info);

		// die();

		$data['cats'] = $cats;

		$data['info'] = $info;

		$this->template->loadContent("admin/edit_performas.php", $data);

	}

	public function submit_edit_performa(){

		// echo "<pre>";

		// print_r($_POST);

		// die();



		$title = $this->common->nohtml($this->input->post("title"));

		$status = $this->common->nohtml($this->input->post("status"));

		$category_id = $this->input->post("category_id");

		$content = $this->common->nohtml($this->input->post("content"));

		$featured = $this->common->nohtml($this->input->post("featured"));



		

		if(empty($title)) {

			$this->template->error(lang("error_82"));

		}

		if(empty($category_id)) {

			$this->template->error("Category field is required");

		}

		$id = $this->input->post("performa_id");

		$data = array(

			"title" => $title,

			"content" => $content,

			"category_id" => $category_id,

			"status" => $status,

			"featured" => $featured

		);

		$this->admin_model->update_performa($id,$data);



		$this->session->set_flashdata("globalmsg", "Performa Add Successfully...");

		redirect(site_url("admin/performas"));

	}



	public function add_page_category() 

	{

		$name = $this->common->nohtml($this->input->post("name"));

		$desc = $this->common->nohtml($this->input->post("description"));

		$price = $this->common->nohtml($this->input->post("price"));



		if(empty($name)) {

			$this->template->error(lang("error_82"));

		}



		$this->admin_model->add_page_category(array(

			"name" => $name,

			"description" => $desc,

			"price" => $price

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_49"));

		redirect(site_url("admin/page_categories"));

	}

	public function add_new_word() 

	{

		 $word = $this->common->nohtml($this->input->post("word"));

		 $link = $this->common->nohtml($this->input->post("link"));

		



		if(empty($word)) {

			$this->template->error(lang("error_82"));

		}



		  $wordcount=$this->admin_model->get_total_words_by_link("http://www.asia.com");

	

		

		

		if($wordcount==0){

		$this->admin_model->add_new_special_word(array(

			"word" => $word,

			"link" => $link,

			

			)

		);



		$this->session->set_flashdata("globalmsg", "Word Added Successfully..");

		redirect(site_url("admin/special_words"));

		}

		else{

		

		



		$this->session->set_flashdata("globalmsg", "Link Already Exists");

		redirect(site_url("admin/special_words"));

		}

	}



	public function edit_page_cat($id) {

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("page_categories" => 1)));



		$id = intval($id);

		$category = $this->admin_model->get_page_category($id);

		if($category->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}

		$category = $category->row();



		$this->template->loadContent("admin/edit_page_category.php", array(

			"category" => $category

			)

		);

	}





	public function view_sub_cats($id)

	{

	

	if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		

	$id = intval($id);

	$category = $this->admin_model->get_subcat_by_cat_id($id);

	$category = $category->result();

	$this->template->loadContent("admin/view_sub_catss.php", array(

			"category" => $category

			)

		);

	

	}

	

	public function add_bare_acts_with_cat($id)

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		

		$id = intval($id);

		$category = $this->admin_model->get_category($id);

		

		$bareact=$this->admin_model->get_bareacts();

		$category=$category->row();

		$bareact=$bareact->result();

		$this->template->loadContent("admin/connect_barect.php", array(

			"category" => $category,

			"bareact" => $bareact,

			)

		);

		

	}

	

	public function addbareacts()

	{

		

		

		$catid = $this->input->post("cat_id");

		$bareact = serialize($this->input->post("bareact"));

		$data=array(

		

			"cat_id"=>$catid,

			"bare_act_ids"=>$bareact,

			"updated_at"=> date('Y-m-d H:i:s'),

			

		);

		

		$selectbareact=$this->admin_model->selectbareactconect($catid);

		

		if($selectbareact==0){

		

		$bareact=$this->admin_model->addbareconnects($data);

		$this->session->set_flashdata("globalmsg", "Catagory connected to bareacts successfuly....");

		redirect(site_url("admin/add_bare_acts_with_cat/".$catid));

		}else

		{

			$bareact=$this->admin_model->updatebareconnects($catid,$data);

			$this->session->set_flashdata("globalmsg", "Catagory uodate to bareacts successfuly....");

			redirect(site_url("admin/add_bare_acts_with_cat/".$catid));

		

		}

	

	}



	public function edit_page_cat_pro($id) 

	{

		$id = intval($id);

		$category = $this->admin_model->get_page_category($id);

		if($category->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}

		$category = $category->row();



		$name = $this->common->nohtml($this->input->post("name"));

		$desc = $this->common->nohtml($this->input->post("description"));

		$price = $this->common->nohtml($this->input->post("price"));



		if(empty($name)) {

			$this->template->error(lang("error_82"));

		}



		$this->admin_model->update_page_category($id, array(

			"name" => $name,

			"description" => $desc,

			"price" => $price

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_50"));

		redirect(site_url("admin/page_categories"));

	}



	public function delete_page_cat($id, $hash) 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("page_categories" => 1)));

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$category = $this->admin_model->get_page_category($id);

		if($category->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}



		$this->admin_model->delete_page_category($id);

		$this->session->set_flashdata("globalmsg", lang("success_51"));

		redirect(site_url("admin/page_categories"));

	}



	public function custom_fields() 

	{	

		if(!$this->common->has_permissions(array("admin",

			"admin_members"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("custom_fields" => 1)));

		$fields = $this->admin_model->get_custom_fields(array());

		$this->template->loadContent("admin/custom_fields.php", array(

			"fields" => $fields

			)

		);



	}



	public function add_custom_field_pro() 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_members"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$name = $this->common->nohtml($this->input->post("name"));

		$type = intval($this->input->post("type"));

		$options = $this->common->nohtml($this->input->post("options"));

		$required = intval($this->input->post("required"));

		$edit = intval($this->input->post("edit"));

		$profile = intval($this->input->post("profile"));

		$help_text = $this->common->nohtml($this->input->post("help_text"));

		$register = intval($this->input->post("register"));



		if(empty($name)) {

			$this->template->error(lang("error_75"));

		}



		if($type < 0 || $type > 4) {

			$this->template->error(lang("error_76"));

		}



		// Add

		$this->admin_model->add_custom_field(array(

			"name" => $name,

			"type" => $type,

			"options" => $options,

			"required" => $required,

			"edit" => $edit,

			"profile" => $profile,

			"help_text" => $help_text,

			"register" => $register

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_37"));

		redirect(site_url("admin/custom_fields"));

	}



	public function edit_custom_field($id) 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_members"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("custom_fields" => 1)));

		$id = intval($id);

		$field = $this->admin_model->get_custom_field($id);

		if($field->num_rows() == 0) {

			$this->template->error(lang("error_77"));

		}



		$field = $field->row();

		$this->template->loadContent("admin/edit_custom_field.php", array(

			"field" => $field

			)

		);

	}



	public function edit_custom_field_pro($id) 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_members"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$field = $this->admin_model->get_custom_field($id);

		if($field->num_rows() == 0) {

			$this->template->error(lang("error_77"));

		}



		$field = $field->row();



		$name = $this->common->nohtml($this->input->post("name"));

		$type = intval($this->input->post("type"));

		$options = $this->common->nohtml($this->input->post("options"));

		$required = intval($this->input->post("required"));

		$edit = intval($this->input->post("edit"));

		$profile = intval($this->input->post("profile"));

		$help_text = $this->common->nohtml($this->input->post("help_text"));

		$register = intval($this->input->post("register"));



		if(empty($name)) {

			$this->template->error(lang("error_75"));

		}



		if($type < 0 || $type > 4) {

			$this->template->error(lang("error_76"));

		}



		// Add

		$this->admin_model->update_custom_field($id, array(

			"name" => $name,

			"type" => $type,

			"options" => $options,

			"required" => $required,

			"edit" => $edit,

			"profile" => $profile,

			"help_text" => $help_text,

			"register" => $register

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_38"));

		redirect(site_url("admin/custom_fields"));

	}



	public function delete_custom_field($id, $hash) 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_members"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$field = $this->admin_model->get_custom_field($id);

		if($field->num_rows() == 0) {

			$this->template->error(lang("error_77"));

		}



		$this->admin_model->delete_custom_field($id);

		$this->session->set_flashdata("globalmsg", lang("success_39"));

		redirect(site_url("admin/custom_fields"));

	}



	public function premium_users($page=0) 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_payment"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("premium_users" => 1)));



		$this->template->loadContent("admin/premium_users.php", array(

			)

		);

	}



	public function premium_users_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("users.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 3 => array(

				 	"payment_plans.name" => 0

				 ),

				 4 => array(

				 	"users.premium_time" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_premium_users_count()

		);

		$users = $this->admin_model->get_premium_users($this->datatables);



		foreach($users->result() as $r) {

			  $time = $this->common->convert_time($r->premium_time); 

			  unset($time['mins']);

			  unset($time['secs']);

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

				$r->first_name . " " . $r->last_name,

				$r->email,

				$r->name,

				$this->common->get_time_string($time),

				date($this->settings->info->date_format, $r->joined),

				'<a href="' . site_url("admin/edit_member/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . site_url("admin/delete_member/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function user_roles() 

	{

		if(!$this->user->info->admin) $this->template->error(lang("error_2"));

		$this->template->loadData("activeLink", 

			array("admin" => array("user_roles" => 1)));

		$roles = $this->admin_model->get_user_roles();



		$permissions = $this->get_default_permissions();



		$this->template->loadContent("admin/user_roles.php", array(

			"roles" => $roles,

			"permissions" => $permissions

			)

		);

	}



	public function add_user_role_pro() 

	{

		if(!$this->user->info->admin) $this->template->error(lang("error_2"));



		$name = $this->common->nohtml($this->input->post("name"));

		if (empty($name)) $this->template->error(lang("error_64"));



		$permissions = $this->get_default_permissions();



		$user_roles = $this->input->post("user_roles");

		foreach($user_roles as $ur) {

			$ur = intval($ur);

			foreach($permissions as $key => $p) {

				if($p['id'] == $ur) {

					$permissions[$key]['selected'] = 1;

				}

			}

		}



		$data = array();

		foreach($permissions as $k=>$v) {

			$data[$k] = $v['selected'];

		}

		$data['name'] = $name;



		$this->admin_model->add_user_role(

			$data

		);



		$this->session->set_flashdata("globalmsg", lang("success_30"));

		redirect(site_url("admin/user_roles"));

	}



	public function edit_user_role($id) 

	{

		if(!$this->user->info->admin) $this->template->error(lang("error_2"));

		$id = intval($id);

		$role = $this->admin_model->get_user_role($id);

		if ($role->num_rows() == 0) $this->template->error(lang("error_65"));



		$role = $role->row();

		$this->template->loadData("activeLink", 

			array("admin" => array("user_roles" => 1)));



		$permissions = $this->get_default_permissions();

		foreach($permissions as $k=>$v) {

			if($role->{$k}) {

				$permissions[$k]['selected'] = 1;

			}

		}



		$this->template->loadContent("admin/edit_user_role.php", array(

			"role" => $role,

			"permissions" => $permissions

			)

		);

	}



	private function get_default_permissions() 

	{

		$urp = $this->admin_model->get_user_role_permissions();

		$permissions = array();

		foreach($urp->result() as $r) {

			$permissions[$r->hook] = array(

				"name" => lang($r->name),

				"desc" => lang($r->description),

				"id" => $r->ID,

				"class" => $r->classname,

				"selected" => 0

			);

		}

		return $permissions;

	}



	public function edit_user_role_pro($id) 

	{

		if(!$this->user->info->admin) $this->template->error(lang("error_2"));

		$id = intval($id);

		$role = $this->admin_model->get_user_role($id);

		if ($role->num_rows() == 0) $this->template->error(lang("error_65"));



		$name = $this->common->nohtml($this->input->post("name"));

		if (empty($name)) $this->template->error(lang("error_64"));



		$permissions = $this->get_default_permissions();



		$user_roles = $this->input->post("user_roles");

		foreach($user_roles as $ur) {

			$ur = intval($ur);

			foreach($permissions as $key => $p) {

				if($p['id'] == $ur) {

					$permissions[$key]['selected'] = 1;

				}

			}

		}



		$data = array();

		foreach($permissions as $k=>$v) {

			$data[$k] = $v['selected'];

		}

		$data['name'] = $name;





		$this->admin_model->update_user_role($id, 

			$data

		);

		$this->session->set_flashdata("globalmsg", lang("success_31"));

		redirect(site_url("admin/user_roles"));

	}



	public function delete_user_role($id, $hash) 

	{

		if(!$this->user->info->admin) $this->template->error(lang("error_2"));

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$group = $this->admin_model->get_user_role($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_65"));



		$this->admin_model->delete_user_role($id);

		// Delete all user groups from member



		$this->session->set_flashdata("globalmsg", lang("success_32"));

		redirect(site_url("admin/user_roles"));

	}



	public function payment_logs($page = 0) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}



		$page = intval($page);

		$this->template->loadData("activeLink", 

			array("admin" => array("payment_logs" => 1)));



		$this->template->loadContent("admin/payment_logs.php", array(

			)

		);

	}



	public function payment_logs_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("users.joined", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 2 => array(

				 	"payment_logs.amount" => 0

				 ),

				 3 => array(

				 	"payment_logs.timestamp" => 0

				 ),

				 4 => array(

				 	"payment_logs.processor" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_payment_logs_count()

		);

		$logs = $this->admin_model->get_payment_logs($this->datatables);



		foreach($logs->result() as $r) {

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

				$r->email,

				number_format($r->amount, 2),

				date($this->settings->info->date_format, $r->timestamp),

				$r->processor

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function payment_plans() 

	{



		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("payment_plans" => 1)));

		$plans = $this->admin_model->get_payment_plans();



		$this->template->loadContent("admin/payment_plans.php", array(

			"plans" => $plans

			)

		);

	}



	public function add_payment_plan() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		$name = $this->common->nohtml($this->input->post("name"));

		$desc = $this->common->nohtml($this->input->post("description"));

		$cost = " ";

		$color = $this->common->nohtml($this->input->post("color"));

		$fontcolor = $this->common->nohtml($this->input->post("fontcolor"));

		$days = " ";

		$icon = $this->common->nohtml($this->input->post("icon"));



		$this->admin_model->add_payment_plan(array(

			"name" => $name,

			"cost" => $cost,

			"hexcolor" => $color,

			"days" => $days,

			"description" => $desc,

			"fontcolor" => $fontcolor,

			"icon" => $icon

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_25"));

		redirect(site_url("admin/payment_plans"));

	}



	public function edit_payment_plan($id,$type) 

	{

	

		

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadExternal(

			'<script src="'.base_url().'scripts/libraries/jscolor.min.js"></script>'

		);

		$this->template->loadData("activeLink", 

			array("admin" => array("payment_plans" => 1)));

		$id = intval($id);

		$plan = $this->admin_model->get_payment_plan($id);

		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));



		if($type==0){

			$this->template->loadContent("admin/edit_payment_plan.php", array(

				"plan" => $plan->row(),

				'type' => $type

				)

			);	

		}

		elseif($type==1){

			$this->template->loadContent("admin/edit_search_payment_plan.php", array(

				"plan" => $plan->row(),

				'type' => $type

				)

			);

		}



	}



	public function edit_payment_plan_pro($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$plan = $this->admin_model->get_payment_plan($id);

		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));



		$name = $this->common->nohtml($this->input->post("name"));

		$desc = $this->common->nohtml($this->input->post("description"));

		$cost = " ";

		$color = $this->common->nohtml($this->input->post("color"));

		$fontcolor = $this->common->nohtml($this->input->post("fontcolor"));

		$days = " ";

		$icon = $this->common->nohtml($this->input->post("icon"));



		$this->admin_model->update_payment_plan($id, array(

			"name" => $name,

			"cost" => $cost,

			"hexcolor" => $color,

			"days" => $days,

			"description" => $desc,

			"fontcolor" => $fontcolor,

			"icon" => $icon

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_26"));

		redirect(site_url("admin/payment_plans"));

	}

	

	public function set_city_price() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

		array("admin" => array("payment_plans" => 1)));

		$this->template->loadContent("admin/set_cities_price.php",array()

			

		);

		

	}



	public function set_city_cat_price() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadContent("admin/set_cities_cat_price.php",array()

			

		);

	}

	





	public function set_city_price_pro() 

	{	

		$this->load->library("datatables");

		$this->datatables->set_default_order("city_plan_pricelist.name", "asc");



		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		

		// $cities = $this->admin_model->get_all_cities();

		

		// if($cities->num_rows() == 0) $this->template->error(lang("error_61"));



		$this->datatables->ordering(

			array(

				 0 => array(

				 	"city_plan_pricelist.name" => 0

				 ),

				 1 => array(

				 	"city_plan_pricelist.price" => 1

				 ),

			

				

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_city_records()

		);

		

		$cities = $this->admin_model->get_all_cities($this->datatables);

	

		

		foreach($cities->result() as $r) {

			

			$options = "";

			// if($r->status == 0) {

			// 	$status = lang("ctn_701");

			// 	$options .= '<a href="'.site_url("admin/accept_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> ';

			// } elseif($r->status == 1) {

			// 	$status = lang("ctn_702");

			// } elseif($r->status == 2) {

			// 	$status = lang("ctn_703");

			// }

			$this->datatables->data[] = array(

			

				

				$r->name,

				$r->price,

				// $options .'<a href="' . site_url("admin/change_city_price/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-cog"></span></a>  <a href="' . site_url("admin/delete_promoted_post/" . $r->id . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>' 

				$options .'<a href="' . site_url("admin/change_city_price/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-cog"></span></a>' 

			);

		}



		

		echo json_encode($this->datatables->process());



		// $this->template->loadContent("admin/set_cities_price.php", array(

		// 	"cities" => $cities->result()

		// 	)

		// );

			

	}

	public function set_cities_cat_price_pro() 

	{

		$this->load->library("datatables");

		$this->datatables->set_default_order("search_plan_pricelist.name", "asc");



		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		

		// $cities = $this->admin_model->get_all_cities();

		

		// if($cities->num_rows() == 0) $this->template->error(lang("error_61"));



		$this->datatables->ordering(

			array(

				 0 => array(

				 	"search_plan_pricelist.name" => 0

				 ),

				 1 => array(

				 	"search_plan_pricelist.catagory_name" => 0

				 ),

				 2 => array(

				 	"search_plan_pricelist.price" => 0

				 ),

			

				

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_city__cat_records()

		);

		

		$cities = $this->admin_model->get_all_cities_cat($this->datatables);

	

		

		foreach($cities->result() as $r) {

			

			$options = "";

			// if($r->status == 0) {

			// 	$status = lang("ctn_701");

			// 	$options .= '<a href="'.site_url("admin/accept_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> ';

			// } elseif($r->status == 1) {

			// 	$status = lang("ctn_702");

			// } elseif($r->status == 2) {

			// 	$status = lang("ctn_703");

			// }

			$this->datatables->data[] = array(

			

				

				$r->name,

				$r->catagory_name,

				$r->price,

				// $options .'<a href="' . site_url("admin/change_city_cat_price/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-cog"></span></a>  <a href="' . site_url("admin/delete_promoted_post/" . $r->id . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>' 

				$options .'<a href="' . site_url("admin/change_city_cat_price/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-cog"></span></a>' 

			);

		}



		

		echo json_encode($this->datatables->process());



		// $this->template->loadContent("admin/set_cities_price.php", array(

		// 	"cities" => $cities->result()

		// 	)

		// );

			

	}







	public function delete_payment_plan($id, $hash) 

	{

		

		

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}



		$id = intval($id);

		$plan = $this->admin_model->get_payment_plan($id);

		//check plan is buy by any user

		$purchansed_plan = $this->user_model->isPlanBy($id);

	

		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));

		if(!empty($purchansed_plan[0])) $this->template->error(lang("error_200"));

		

		$this->admin_model->delete_payment_plan($id);

		$this->session->set_flashdata("globalmsg", lang("success_27"));

		

		$response = array(

			'status' => true

		);

		

		

		echo json_encode($response);

		

		// redirect(site_url("admin/payment_plans"));

	}



	public function payment_settings() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("payment_settings" => 1)));

		$this->template->loadContent("admin/payment_settings.php", array(

			)

		);

	}



	public function payment_settings_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_payment) {

			$this->template->error(lang("error_2"));

		}

		$paypal_email = $this->common->nohtml(

			$this->input->post("paypal_email"));

		$paypal_currency = $this->common->nohtml(

			$this->input->post("paypal_currency"));

		$payment_enabled = intval($this->input->post("payment_enabled"));

		$payment_symbol = $this->common->nohtml(

			$this->input->post("payment_symbol"));



		$stripe_secret_key = $this->common->nohtml($this->input->post("stripe_secret_key"));

		$stripe_publish_key = $this->common->nohtml($this->input->post("stripe_publish_key"));

		$checkout2_secret = $this->common->nohtml($this->input->post("checkout2_secret"));

		$checkout2_accountno = $this->common->nohtml($this->input->post("checkout2_accountno"));



		// update

		$this->admin_model->updateSettings(

			array(

				"paypal_email" => $paypal_email,

				"paypal_currency" => $paypal_currency,

				"payment_enabled" => $payment_enabled,

				"payment_symbol" => $payment_symbol,

				"stripe_secret_key" => $stripe_secret_key,

				"stripe_publish_key" => $stripe_publish_key,

				"checkout2_secret" => $checkout2_secret,

				"checkout2_accountno" => $checkout2_accountno

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_24"));

		redirect(site_url("admin/payment_settings"));



	}



	public function email_members() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("email_members" => 1)));

		$groups = $this->admin_model->get_user_groups();

		$this->template->loadContent("admin/email_members.php", array(

			"groups" => $groups

			)

		);

	}



	public function email_members_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$usernames = $this->common->nohtml($this->input->post("usernames"));

		$groupid = intval($this->input->post("groupid"));

		$title = $this->common->nohtml($this->input->post("title"));

		$message = $this->lib_filter->go($this->input->post("message"));



		if ($groupid == -1) {

			// All members

			$users = array();

			$usersc = $this->admin_model->get_all_users();

			foreach ($usersc->result() as $r) {

				$users[] = $r;

			}

		} else {

			$usernames = explode(",", $usernames);



			$users = array();

			foreach ($usernames as $username) {

				if (empty($username)) continue;

				$user = $this->user_model->get_user_by_username($username);

				if ($user->num_rows() == 0) {

					$this->template->error(lang("error_3") . $username);

				}

				$users[] = $user->row();

			}



			if ($groupid > 0) {

				$group = $this->admin_model->get_user_group($groupid);

				if ($group->num_rows() == 0) {

					$this->template->error(lang("error_4"));

				}



				$users_g = $this->admin_model->get_all_group_users($groupid);

				$cusers = $users;



				foreach ($users_g->result() as $r) {

					// Check for duplicates

					$skip = false;

					foreach ($cusers as $a) {

						if($a->userid == $r->userid) $skip = true;

					}

					if (!$skip) {

						$users[] = $r;

					}

				}

			}



		}



		foreach ($users as $r) {

			$this->common->send_email($title, $message, $r->email);

		}



		$this->session->set_flashdata("globalmsg", lang("success_1"));

		redirect(site_url("admin/email_members"));

	}



	public function user_groups() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("user_groups" => 1)));

		$groups = $this->admin_model->get_user_groups();

		$this->template->loadContent("admin/groups.php", array(

			"groups" => $groups

			)

		);

	}



	public function add_group_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$name = $this->common->nohtml($this->input->post("name"));

		$default = intval($this->input->post("default_group"));

		if (empty($name)) $this->template->error(lang("error_5"));





		$this->admin_model->add_group(

			array(

				"name" =>$name,

				"default" => $default,

				)

			);

		$this->session->set_flashdata("globalmsg", lang("success_2"));

		redirect(site_url("admin/user_groups"));

	}



	public function edit_group($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$group = $this->admin_model->get_user_group($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$this->template->loadData("activeLink", 

			array("admin" => array("user_groups" => 1)));



		$this->template->loadContent("admin/edit_group.php", array(

			"group" => $group->row()

			)

		);

	}



	public function edit_group_pro($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$group = $this->admin_model->get_user_group($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$name = $this->common->nohtml($this->input->post("name"));

		$default = intval($this->input->post("default_group"));

		if (empty($name)) $this->template->error(lang("error_5"));



		$this->admin_model->update_group($id, 

			array(

				"name" =>$name,

				"default" => $default

				)

		);

		$this->session->set_flashdata("globalmsg", lang("success_3"));

		redirect(site_url("admin/user_groups"));

	}



	public function delete_group($id, $hash) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$group = $this->admin_model->get_user_group($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$this->admin_model->delete_group($id);

		// Delete all user groups from member

		$this->admin_model->delete_users_from_group($id); 



		$this->session->set_flashdata("globalmsg", lang("success_4"));

		redirect(site_url("admin/user_groups"));

	}



	public function view_group($id, $page=0) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("user_groups" => 1)));

		$id = intval($id);

		$page = intval($page);

		$group = $this->admin_model->get_user_group($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$users = $this->admin_model->get_users_from_groups($id, $page);



		$this->load->library('pagination');

		$config['base_url'] = site_url("admin/view_group/" . $id);

		$config['total_rows'] = $this->admin_model

			->get_total_user_group_members_count($id);

		$config['per_page'] = 20;

		$config['uri_segment'] = 4;



		include (APPPATH . "/config/page_config.php");



		$this->pagination->initialize($config); 



		$this->template->loadContent("admin/view_group.php", array(

			"group" => $group->row(),

			"users" => $users,

			"total_members" => $config['total_rows']

			)

		);



	}



	public function add_user_to_group_pro($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$group = $this->admin_model->get_user_group($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$usernames = $this->common->nohtml($this->input->post("usernames"));

		$usernames = explode(",", $usernames);



		$users = array();

		foreach ($usernames as $username) {

			$user = $this->user_model->get_user_by_username($username);

			if($user->num_rows() == 0) $this->template->error(lang("error_3") 

				. $username);

			$users[] = $user->row();

		}



		foreach ($users as $user) {

			// Check not already a member

			$userc = $this->admin_model->get_user_from_group($user->ID, $id);

			if ($userc->num_rows() == 0) {

				$this->admin_model->add_user_to_group($user->ID, $id);

			}

		}



		$this->session->set_flashdata("globalmsg", lang("success_5"));

		redirect(site_url("admin/view_group/" . $id));

	}



	public function remove_user_from_group($userid, $id, $hash) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$userid = intval($userid);

		$group = $this->admin_model->get_user_group($id);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$user = $this->admin_model->get_user_from_group($userid, $id);

		if ($user->num_rows() == 0) $this->template->error(lang("error_7"));



		$this->admin_model->delete_user_from_group($userid, $id);

		$this->session->set_flashdata("globalmsg", lang("success_6"));

		redirect(site_url("admin/view_group/" . $id));

	}



	public function email_templates() 

	{

		if(!$this->user->info->admin) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("email_templates" => 1)));



		$languages = $this->config->item("available_languages");



		$this->template->loadContent("admin/email_templates.php", array(

			"languages" => $languages

			)

		);

	}



	public function email_template_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("email_templates.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"email_templates.title" => 0

				 ),

				 1 => array(

				 	"email_templates.hook" => 0

				 ),

				 2 => array(

				 	"email_templates.language" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_email_templates()

		);

		$templates = $this->admin_model->get_email_templates($this->datatables);



		foreach($templates->result() as $r) {



			$this->datatables->data[] = array(

				$r->title,

				$r->hook,

				$r->language,

				'<a href="'.site_url("admin/edit_email_template/" . $r->ID).'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="bottom" title="'.lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a> <a href="'.site_url("admin/delete_email_template/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onclick="return confirm(\''.lang("ctn_317").'\')" data-toggle="tooltip" data-placement="bottom" title="'.lang("ctn_57").'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function add_email_template() 

	{

		$title = $this->common->nohtml($this->input->post("title"));

		$template = $this->lib_filter->go($this->input->post("template"));

		$hook = $this->common->nohtml($this->input->post("hook"));

		$language = $this->common->nohtml($this->input->post("language"));



		$this->admin_model->add_email_template(array(

			"title" => $title,

			"message" => $template,

			"hook" => $hook,

			"language" => $language

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_41"));

		redirect(site_url("admin/email_templates"));

	}



	public function edit_email_template($id) 

	{

		if(!$this->user->info->admin) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("email_templates" => 1)));

		$id = intval($id);



		$email_template = $this->admin_model->get_email_template($id);

		if ($email_template->num_rows() == 0) {

			$this->template->error(lang("error_8"));

		}



		$languages = $this->config->item("available_languages");



		$this->template->loadContent("admin/edit_email_template.php", array(

			"email_template" => $email_template->row(),

			"languages" => $languages

			)

		);

	}



	public function edit_email_template_pro($id) 

	{

		if(!$this->user->info->admin) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("email_templates" => 1)));

		$id = intval($id);

		$email_template = $this->admin_model->get_email_template($id);

		if ($email_template->num_rows() == 0) {

			$this->template->error(lang("error_8"));

		}



		$title = $this->common->nohtml($this->input->post("title"));

		$template = $this->lib_filter->go($this->input->post("template"));

		$hook = $this->common->nohtml($this->input->post("hook"));

		$language = $this->common->nohtml($this->input->post("language"));



		$this->admin_model->update_email_template($id, array(

			"title" => $title,

			"message" => $template,

			"hook" => $hook,

			"language" => $language

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_7"));

		redirect(site_url("admin/email_templates"));

	}



	public function delete_email_template($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);



		$email_template = $this->admin_model->get_email_template($id);

		if ($email_template->num_rows() == 0) {

			$this->template->error(lang("error_8"));

		}



		$this->admin_model->delete_email_template($id);

		$this->session->set_flashdata("globalmsg", lang("success_42"));

		redirect(site_url("admin/email_templates"));

	}



	public function ipblock() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("ipblock" => 1)));



		$ipblock = $this->admin_model->get_ip_blocks();



		$this->template->loadContent("admin/ipblock.php", array(

			"ipblock" => $ipblock

			)

		);

	}



	public function add_ipblock() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$ip = $this->common->nohtml($this->input->post("ip"));

		$reason = $this->common->nohtml($this->input->post("reason"));



		if (empty($ip)) $this->template->error(lang("error_10"));



		$this->admin_model->add_ipblock($ip, $reason);

		$this->session->set_flashdata("globalmsg", lang("success_8"));

		redirect(site_url("admin/ipblock"));

	}



	public function delete_ipblock($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$ipblock = $this->admin_model->get_ip_block($id);

		if ($ipblock->num_rows() == 0) $this->template->error(lang("error_11"));



		$this->admin_model->delete_ipblock($id);

		$this->session->set_flashdata("globalmsg", lang("success_9"));

		redirect(site_url("admin/ipblock"));

	}



	public function members() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("members" => 1)));

		

		$user_roles = $this->admin_model->get_user_roles();



		$fields = $this->user_model->get_custom_fields(array("register"=>1));



		$this->template->loadContent("admin/members.php", array(

			"user_roles" => $user_roles,

			"fields" => $fields

			)

		);

	}

	

	

	

public function members_page_settings() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("manage_pages.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"manage_pages.meta_title" => 0

				 ),

				 1 => array(

				 	"manage_pages.meta_description" => 0

				 ),

				 2 => array(

				 	"manage_pages.updated_at" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_pages_count()

		);

		

		

		$members = $this->admin_model->get_site_pages($this->datatables);



	

		foreach($members->result() as $r) {

			

				if($r->status==0){

					

						$option='<a href="'.site_url("admin/edit_page/" . $r->ID).'" class="btn btn-warning btn-xs" title="'.lang("ctn_55").'" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-edit"></span></a> <a href="'.site_url("admin/deactivate_page/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onclick="return confirm(\''.lang("ctn_317").'\')" title="Deactivate Page" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-ok-circle"></span></a>  <a href="'.site_url("admin/view_edit_logs/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs"  title="View Edit History" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-play-circle"></span></a>';

				

					}

				else if($r->status==1){

					

						$option='<a href="'.site_url("admin/edit_page/" . $r->ID).'" class="btn btn-warning btn-xs" title="'.lang("ctn_55").'" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-edit"></span></a> <a href="'.site_url("admin/activate_page/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onclick="return confirm(\''.lang("ctn_317").'\')" title="Activate Page" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-ban-circle"></span></a>  <a href="'.site_url("admin/view_edit_logs/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs"  title="View Edit History" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-play-circle"></span></a>';

				

					}

			

			$this->datatables->data[] = array(

				

				

				$r->meta_title,

				$r->meta_description,

				

				$r->updated_at,

				$option

				

			);

		}

		echo json_encode($this->datatables->process());

	}

	

	

	

	

	public function view_edit_logs($id)

	{

		

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		

		

		$this->template->loadContent("admin/view_page_logs.php", array(

		

			"id"=>$id,

		));

	

	}

	public function edit_page($id){

	

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		

		$fields=$this->admin_model->getpage_details($id);

		

		

		$this->template->loadContent("admin/edit_pages.php", array(

			

			"fields" => $fields->result(),

			)

		);

		

	

	

	}

	

	

	public function deactivate_page($id){

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		$data=array(

			"status"=>1,

		);

		$this->admin_model->deactive_page_status($id,$data);

		

		$this->session->set_flashdata("globalmsg", "Page Deactivate Successfully!");

		redirect(site_url("admin/manage_pages"));

			

		

		}

	public function activate_page($id)

	{

		

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		

		$data=array(

			"status"=>0,

		);

		$this->admin_model->active_page_status($id,$data);

		

		$this->session->set_flashdata("globalmsg", "Page Activate successfully!");

		redirect(site_url("admin/manage_pages"));

	

	}

	

	public function delete_page($id){

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		$this->admin_model->delete_page_admin($id);

		

		$this->session->set_flashdata("globalmsg", "Page Deleted Successfully!");

		redirect(site_url("admin/manage_pages"));

		

		}

	

	

	public function view_page_history_logs($id){

		$this->load->library("datatables");



		//$this->datatables->set_default_order("users.joined", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"manage_pages.meta_title" => 0

				 ),

				 1 => array(

				 	"pagehistory_logs.history_notes" => 0

				 ),

				 2 => array(

				 	"pagehistory_logs.version" => 0

				 ),

				 3 => array(

				 	"pagehistory_logs.last_updated" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_logs_count($id)

		);

		$members = $this->admin_model->get_history_notes_pages($id,$this->datatables);



		foreach($members->result() as $r) {

			

			$this->datatables->data[] = array(

				

				$r->meta_title,

				html_entity_decode($r->history_notes),

				$r->version,

				$r->last_updated,

				

			

			);

		}

		echo json_encode($this->datatables->process());

		

		}



	public function members_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("users.joined", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

				 	"users.first_name" => 0

				 ),

				 2 => array(

				 	"users.last_name" => 0

				 ),

				 3 => array(

				 	"users.email" => 0

				 ),

				 4 => array(

				 	"user_roles.name" => 0

				 ),

				 5 => array(

				 	"users.joined" => 0

				 ),

				 6 => array(

				 	"users.oauth_provider" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->user_model

				->get_total_members_count()

		);

		$members = $this->user_model->get_members_admin($this->datatables);



		foreach($members->result() as $r) {

			if($r->oauth_provider == "google") {

				$provider = "Google";

			} elseif($r->oauth_provider == "twitter") {

				$provider = "Twitter";

			} elseif($r->oauth_provider == "facebook") {

				$provider = "Facebook";

			} else {

				$provider =  lang("ctn_196");

			}

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				$r->first_name,

				$r->last_name,

				$r->email,

				$this->common->get_user_role($r),

				date($this->settings->info->date_format, $r->joined),

				$provider,

				'<a href="'.site_url("admin/edit_member/" . $r->ID).'" class="btn btn-warning btn-xs" title="'.lang("ctn_55").'" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-edit"></span></a> <a href="'.site_url("admin/delete_member/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onclick="return confirm(\''.lang("ctn_317").'\')" title="'.lang("ctn_57").'" data-toggle="tooltip" data-placement="bottom"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}



	public function member_user_groups($id) 

	{

		if(!$this->common->has_permissions(array("admin", "admin_members"),

		 $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("members" => 1)));

		$id = intval($id);



		$member = $this->user_model->get_user_by_id($id);

		if ($member->num_rows() ==0 ) $this->template->error(lang("error_13"));



		$member = $member->row();



		// Groups

		$user_groups = $this->user_model->get_user_groups($id);

		$groups = $this->admin_model->get_user_groups();





		$this->template->loadContent("admin/member_user_groups.php", array(

			"member" => $member,

			"user_groups" => $user_groups,

			"groups" => $groups

			)

		);

	}



	public function add_member_to_group_pro($id) 

	{

		if(!$this->common->has_permissions(array("admin", "admin_members"),

		 $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);



		$member = $this->user_model->get_user_by_id($id);

		if ($member->num_rows() ==0 ) $this->template->error(lang("error_13"));



		$member = $member->row();



		$groupid = intval($this->input->post("groupid"));



		$group = $this->admin_model->get_user_group($groupid);

		if ($group->num_rows() == 0) $this->template->error(lang("error_4"));



		$userc = $this->admin_model->get_user_from_group($id, $groupid);

		if ($userc->num_rows() > 0) {

			$this->template->error(lang("error_84"));

		}



		$this->admin_model->add_user_to_group($member->ID, $groupid);

		



		$this->session->set_flashdata("globalmsg", lang("success_5"));

		redirect(site_url("admin/member_user_groups/" . $id));



	}



	public function edit_member($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("members" => 1)));

		$id = intval($id);



		$member = $this->user_model->get_user_by_id($id);

		if ($member->num_rows() ==0 ) $this->template->error(lang("error_13"));



		$user_groups = $this->user_model->get_user_groups($id);

		$user_roles = $this->admin_model->get_user_roles();

		$fields = $this->user_model->get_custom_fields_answers(array(

			), $id);



		$this->template->loadContent("admin/edit_member.php", array(

			"member" => $member->row(),

			"user_groups" => $user_groups,

			"user_roles" => $user_roles,

			"fields" => $fields

			)

		);

	}



	public function edit_member_pro($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		$fields = $this->user_model->get_custom_fields_answers(array(

			), $id);



		$member = $this->user_model->get_user_by_id($id);

		if ($member->num_rows() ==0 ) $this->template->error(lang("error_13"));



		$member = $member->row();



		$this->load->model("register_model");

		$email = $this->input->post("email", true);

		$first_name = $this->common->nohtml(

			$this->input->post("first_name", true));

		$last_name = $this->common->nohtml(

			$this->input->post("last_name", true));

		$pass = $this->common->nohtml(

			$this->input->post("password", true));

		$username = $this->common->nohtml(

			$this->input->post("username", true));

		$user_role = intval($this->input->post("user_role"));

		$aboutme = $this->common->nohtml($this->input->post("aboutme"));

		$points = abs($this->input->post("credits"));

		$active = intval($this->input->post("active"));

		$verified = intval($this->input->post("verified"));



		$address_1 = $this->common->nohtml($this->input->post("address_1"));

		$address_2 = $this->common->nohtml($this->input->post("address_2"));

		$city = $this->common->nohtml($this->input->post("city"));

		$state = $this->common->nohtml($this->input->post("state"));

		$zipcode = $this->common->nohtml($this->input->post("zipcode"));

		$country = $this->common->nohtml($this->input->post("country"));



		if (strlen($username) < 3) $this->template->error(lang("error_14"));



		if (!preg_match("/^[a-z0-9_]+$/i", $username)) {

			$this->template->error(lang("error_15"));

		}



		if ($username != $member->username) {

			if (!$this->register_model->check_username_is_free($username)) {

				 $this->template->error(lang("error_16"));

			}

		}



		if (!empty($pass)) {

			if (strlen($pass) <= 5) {

				 $this->template->error(lang("error_17"));

			}

			$pass = $this->common->encrypt($pass);

		} else {

			$pass = $member->password;

		}



		$this->load->helper('email');

		$this->load->library('upload');



		if (empty($email)) {

				$this->template->error(lang("error_18"));

		}



		if (!valid_email($email)) {

			$this->template->error(lang("error_19"));

		}



		if ($email != $member->email) {

			if (!$this->register_model->checkEmailIsFree($email)) {

				 $this->template->error(lang("error_20"));

			}

		}



		if($user_role != $member->user_role) {

			if(!$this->user->info->admin) {

				$this->template->error(lang("error_66"));

			}

		}

		if($user_role > 0) {

			$role = $this->admin_model->get_user_role($user_role);

			if($role->num_rows() == 0) $this->template->error(lang("error_65"));

		}



		if ($_FILES['userfile']['size'] > 0) {

				$this->upload->initialize(array( 

			       "upload_path" => $this->settings->info->upload_path,

			       "overwrite" => FALSE,

			       "max_filename" => 300,

			       "encrypt_name" => TRUE,

			       "remove_spaces" => TRUE,

			       "allowed_types" => "gif|jpg|png|jpeg|",

			       "max_size" => 1000,

			       "max_width" => $this->settings->info->avatar_width,

			       "max_height" => $this->settings->info->avatar_height

			    ));



			    if (!$this->upload->do_upload()) {

			    	$this->template->error(lang("error_21")

			    	.$this->upload->display_errors());

			    }



			    $data = $this->upload->data();



			    $image = $data['file_name'];

			} else {

				$image= $member->avatar;

			}



		// Custom Fields

		// Process fields

		$answers = array();

		foreach($fields->result() as $r) {

			$answer = "";

			if($r->type == 0) {

				// Look for simple text entry

				$answer = $this->common->nohtml($this->input->post("cf_" . $r->ID));



				if($r->required && empty($answer)) {

					$this->template->error(lang("error_78") . $r->name);

				}

				// Add

				$answers[] = array(

					"fieldid" => $r->ID,

					"answer" => $answer

				);

			} elseif($r->type == 1) {

				// HTML

				$answer = $this->common->nohtml($this->input->post("cf_" . $r->ID));



				if($r->required && empty($answer)) {

					$this->template->error(lang("error_78") . $r->name);

				}

				// Add

				$answers[] = array(

					"fieldid" => $r->ID,

					"answer" => $answer

				);

			} elseif($r->type == 2) {

				// Checkbox

				$options = explode(",", $r->options);

				foreach($options as $k=>$v) {

					// Look for checked checkbox and add it to the answer if it's value is 1

					$ans = $this->common->nohtml($this->input->post("cf_cb_" . $r->ID . "_" . $k));

					if($ans) {

						if(empty($answer)) {

							$answer .= $v;

						} else {

							$answer .= ", " . $v;

						}

					}

				}



				if($r->required && empty($answer)) {

					$this->template->error(lang("error_78") . $r->name);

				}

				$answers[] = array(

					"fieldid" => $r->ID,

					"answer" => $answer

				);



			} elseif($r->type == 3) {

				// radio

				$options = explode(",", $r->options);

				if(isset($_POST['cf_radio_' . $r->ID])) {

					$answer = intval($this->common->nohtml($this->input->post("cf_radio_" . $r->ID)));

					

					$flag = false;

					foreach($options as $k=>$v) {

						if($k == $answer) {

							$flag = true;

							$answer = $v;

						}

					}

					if($r->required && !$flag) {

						$this->template->error(lang("error_78") . $r->name);

					}

					if($flag) {

						$answers[] = array(

							"fieldid" => $r->ID,

							"answer" => $answer

						);

					}

				}



			} elseif($r->type == 4) {

				// Dropdown menu

				$options = explode(",", $r->options);

				$answer = intval($this->common->nohtml($this->input->post("cf_" . $r->ID)));

				$flag = false;

				foreach($options as $k=>$v) {

					if($k == $answer) {

						$flag = true;

						$answer = $v;

					}

				}

				if($r->required && !$flag) {

					$this->template->error(lang("error_78") . $r->name);

				}

				if($flag) {

					$answers[] = array(

						"fieldid" => $r->ID,

						"answer" => $answer

					);

				}

			}

		}





		$this->user_model->update_user($id, 

			array(

				"username" => $username,

				"email" => $email,

				"first_name" => $first_name,

				"last_name" => $last_name,

				"password" => $pass,

				"user_role" => $user_role,

				"avatar" => $image,

				"aboutme" => $aboutme,

				"points" => $points,

				"active" => $active,

				"address_1" => $address_1,

				"address_2" => $address_2,

				"city" => $city,

				"state" => $state,

				"zipcode" => $zipcode,

				"country" => $country,

				"verified" => $verified

				)

		);



		// Update CF

		// Add Custom Fields data

		foreach($answers as $answer) {

			// Check if field exists

			$field = $this->user_model->get_user_cf($answer['fieldid'], $id);

			if($field->num_rows() == 0) {

				$this->user_model->add_custom_field(array(

					"userid" => $id,

					"fieldid" => $answer['fieldid'],

					"value" => $answer['answer']

					)

				);

			} else {

				$this->user_model->update_custom_field($answer['fieldid'], 

					$id, $answer['answer']);

			}

		}





		$this->session->set_flashdata("globalmsg", lang("success_10"));

		redirect(site_url("admin/members"));

	}



	public function add_member_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->load->model("register_model");

		$email = $this->input->post("email", true);

		$first_name = $this->common->nohtml(

			$this->input->post("first_name", true));

		$last_name = $this->common->nohtml(

			$this->input->post("last_name", true));

		$pass = $this->common->nohtml(

			$this->input->post("password", true));

		$pass2 = $this->common->nohtml(

			$this->input->post("password2", true));

		$captcha = $this->input->post("captcha", true);

		$username = $this->common->nohtml(

			$this->input->post("username", true));

		$user_role = intval($this->input->post("user_role"));



		if($user_role > 0) {

			$role = $this->admin_model->get_user_role($user_role);

			if($role->num_rows() == 0) $this->template->error(lang("error_65"));

			$role = $role->row();

			if($role->admin || $role->admin_members || $role->admin_settings 

				|| $role->admin_payment) {

				if(!$this->user->info->admin) {

					$this->template->error(lang("error_67"));

				}

			}

		}





		if (strlen($username) < 3) $this->template->error(lang("error_14"));



		if (!preg_match("/^[a-z0-9_]+$/i", $username)) {

			$this->template->error(lang("error_15"));

		}



		if (!$this->register_model->check_username_is_free($username)) {

			 $this->template->error(lang("error_16"));

		}



		if ($pass != $pass2) $this->template->error(lang("error_22"));



		if (strlen($pass) <= 5) {

			 $this->template->error(lang("error_17"));

		}



		$this->load->helper('email');



		if (empty($email)) {

				$this->template->error(lang("error_18"));

		}



		if (!valid_email($email)) {

			$this->template->error(lang("error_19"));

		}



		if (!$this->register_model->checkEmailIsFree($email)) {

			 $this->template->error(lang("error_20"));

		}



		$fields = $this->user_model->get_custom_fields_answers(array(

			), 0);

		// Custom Fields

		// Process fields

		$answers = array();

		foreach($fields->result() as $r) {

			$answer = "";

			if($r->type == 0) {

				// Look for simple text entry

				$answer = $this->common->nohtml($this->input->post("cf_" . $r->ID));



				if($r->required && empty($answer)) {

					$fail = lang("error_158") . $r->name;

				}

				// Add

				$answers[] = array(

					"fieldid" => $r->ID,

					"answer" => $answer

				);

			} elseif($r->type == 1) {

				// HTML

				$answer = $this->common->nohtml($this->input->post("cf_" . $r->ID));



				if($r->required && empty($answer)) {

					$fail = lang("error_158") . $r->name;

				}

				// Add

				$answers[] = array(

					"fieldid" => $r->ID,

					"answer" => $answer

				);

			} elseif($r->type == 2) {

				// Checkbox

				$options = explode(",", $r->options);

				foreach($options as $k=>$v) {

					// Look for checked checkbox and add it to the answer if it's value is 1

					$ans = $this->common->nohtml($this->input->post("cf_cb_" . $r->ID . "_" . $k));

					if($ans) {

						if(empty($answer)) {

							$answer .= $v;

						} else {

							$answer .= ", " . $v;

						}

					}

				}



				if($r->required && empty($answer)) {

					$fail = lang("error_158") . $r->name;

				}

				$answers[] = array(

					"fieldid" => $r->ID,

					"answer" => $answer

				);



			} elseif($r->type == 3) {

				// radio

				$options = explode(",", $r->options);

				if(isset($_POST['cf_radio_' . $r->ID])) {

					$answer = intval($this->common->nohtml($this->input->post("cf_radio_" . $r->ID)));

					

					$flag = false;

					foreach($options as $k=>$v) {

						if($k == $answer) {

							$flag = true;

							$answer = $v;

						}

					}

					if($r->required && !$flag) {

						$fail = lang("error_158") . $r->name;

					}

					if($flag) {

						$answers[] = array(

							"fieldid" => $r->ID,

							"answer" => $answer

						);

					}

				}



			} elseif($r->type == 4) {

				// Dropdown menu

				$options = explode(",", $r->options);

				$answer = intval($this->common->nohtml($this->input->post("cf_" . $r->ID)));

				$flag = false;

				foreach($options as $k=>$v) {

					if($k == $answer) {

						$flag = true;

						$answer = $v;

					}

				}

				if($r->required && !$flag) {

					$fail = lang("error_158") . $r->name;

				}

				if($flag) {

					$answers[] = array(

						"fieldid" => $r->ID,

						"answer" => $answer

					);

				}

			}

		}



		if(!empty($fail)) {

			$this->template->error($fail);

		}



		$pass = $this->common->encrypt($pass);

		$this->register_model->add_user(array(

			"username" => $username,

			"email" => $email,

			"first_name" => $first_name,

			"last_name" => $last_name,

			"password" => $pass,

			"user_role" => $user_role,

			"IP" => $_SERVER['REMOTE_ADDR'],

			"joined" => time(),

			"joined_date" => date("n-Y"),

			"active" => 1

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_11"));

		redirect(site_url("admin/members"));

	

	}

	

	

	

	public function edit_pages_pro(){

		  $id=$this->input->post("metaid");

		  $metatitle=$this->input->post("metatitle");

		  $metadescription=$this->input->post("metadescription");

		  $metakeyword=$this->input->post("metakey");

		  $content=$this->input->post("pagecontent");

		  $version=$this->input->post("versionc");

		  $historynotes=$this->input->post("hotnotes");

		  $lastupdated= date('Y-m-d H:i:s');   

					 

			$data=array(

						"meta_title"=>$metatitle,

						"meta_description"=>$metadescription,

						"metakeywords"=>$metakeyword,

						"content"=>htmlentities($content),

						"versioncontrol"=>$version,

						"historynotes"=>$historynotes,

						"updated_at"=>$lastupdated,

					 );  

				$data2=array(

					"pageid"=>$id,

					"history_notes"=>$historynotes,

					"version"=>$version,

					"last_updated"=>$lastupdated,

				);

				$query=$this->admin_model->update_pages_to_pro($id,$data);

				$query1=$this->admin_model->insert_pages_history($data2);

				$this->session->set_flashdata("globalmsg", "Page Update Successfully!");

				redirect(site_url("admin/manage_pages"));



			}

	

	

	public function add_pages_pro()

	{

	

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

	$this->load->helper(array('form', 'url'));

	$this->load->library('form_validation');

	$this->form_validation->set_rules('metatitle', 'meta title', 'required');

	$this->form_validation->set_rules('metadescription', 'meta description', 'required');

	$this->form_validation->set_rules('metakey', 'meta keywords', 'required');

	$this->form_validation->set_rules('pagecontent', 'Page content', 'required');

	$this->form_validation->set_rules('versionc', 'version', 'required');

	if ($this->form_validation->run() == TRUE)

                {

					 $metatitle=$this->input->post("metatitle");

					 $metadescription=$this->input->post("metadescription");

					 $metakeyword=$this->input->post("metakey");

					 $content=$this->input->post("pagecontent");

					 $version=$this->input->post("versionc");

					 $historynotes=$this->input->post("historynotes");

					 $lastupdated= date('Y-m-d H:i:s');   

					 $data=array(

						"meta_title"=>$metatitle,

						"meta_description"=>$metadescription,

						"metakeywords"=>$metakeyword,

						"content"=>htmlentities($content),

						"versioncontrol"=>$version,

						"historynotes"=>htmlentities($historynotes),

						"updated_at"=>$lastupdated,

					 );  

				$query=$this->admin_model->add_pages_to_pro($data);

				$this->session->set_flashdata("globalmsg", "Page Created Successfully!");

				redirect(site_url("admin/manage_pages"));	 

                }

               else{

				    return redirect("admin/manage_pages");

				   }

}



	public function delete_member($id, $hash) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$member = $this->user_model->get_user_by_id($id);

		if ($member->num_rows() ==0 ) $this->template->error(lang("error_13"));



		$this->user_model->delete_user($id);

		// Delete user from user group

		$this->admin_model->delete_user_from_all_groups($id);

		

		$this->session->set_flashdata("globalmsg", lang("success_12"));

		redirect(site_url("admin/members"));

	}



	public function social_settings() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("social_settings" => 1)));

		$this->template->loadContent("admin/social_settings.php", array(

			)

		);

	}



	public function social_settings_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$disable_social_login = 

			intval($this->input->post("disable_social_login"));

		$twitter_consumer_key = 

			$this->common->nohtml($this->input->post("twitter_consumer_key"));

		$twitter_consumer_secret = 

			$this->common->nohtml($this->input->post("twitter_consumer_secret"));

		$facebook_app_id = 

			$this->common->nohtml($this->input->post("facebook_app_id"));

		$facebook_app_secret = 

			$this->common->nohtml($this->input->post("facebook_app_secret"));

		$google_client_id = 

			$this->common->nohtml($this->input->post("google_client_id"));

		$google_client_secret = 

			$this->common->nohtml($this->input->post("google_client_secret"));



		$this->admin_model->updateSettings(

			array(

				"disable_social_login" => $disable_social_login,

				"twitter_consumer_key" => $twitter_consumer_key,

				"twitter_consumer_secret" => $twitter_consumer_secret,

				"facebook_app_id" => $facebook_app_id, 

				"facebook_app_secret"=> $facebook_app_secret,  

				"google_client_id" => $google_client_id,

				"google_client_secret" => $google_client_secret,

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_13"));

		redirect(site_url("admin/social_settings"));

	}



	public function settings() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("settings" => 1)));

		$roles = $this->admin_model->get_user_roles();

		$layouts = $this->admin_model->get_layouts();

		$this->template->loadContent("admin/settings.php", array(

			"roles" => $roles,

			"layouts" => $layouts

			)

		);

	}



	public function settings_pro() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$site_name = $this->common->nohtml($this->input->post("site_name"));

		$site_desc = $this->common->nohtml($this->input->post("site_desc"));

		$site_email = $this->common->nohtml($this->input->post("site_email"));

		$upload_path = $this->common->nohtml($this->input->post("upload_path"));

		$know_the_law = $this->common->nohtml($this->input->post("know_the_law"));

		$file_types = $this->common

			->nohtml($this->input->post("file_types"));

		$file_size = intval($this->input->post("file_size"));

		$upload_path_rel = 

			$this->common->nohtml($this->input->post("upload_path_relative"));

		$register = intval($this->input->post("register"));

		$avatar_upload = intval($this->input->post("avatar_upload"));

		$disable_captcha = intval($this->input->post("disable_captcha"));

		$date_format = $this->common->nohtml($this->input->post("date_format"));

		$login_protect = intval($this->input->post("login_protect"));

		$activate_account = intval($this->input->post("activate_account"));

		$default_user_role = intval($this->input->post("default_user_role"));

		$secure_login = intval($this->input->post("secure_login"));

		$page_slugs = intval($this->input->post("page_slugs"));

		$disable_chat = intval($this->input->post("disable_chat"));



		$google_recaptcha = intval($this->input->post("google_recaptcha"));

		$google_recaptcha_secret = $this->common->nohtml($this->input->post("google_recaptcha_secret"));

		$google_recaptcha_key = $this->common->nohtml($this->input->post("google_recaptcha_key"));



		$logo_option = intval($this->input->post("logo_option"));



		$avatar_width = intval($this->input->post("avatar_width"));

		$avatar_height = intval($this->input->post("avatar_height"));

		$cache_time = intval($this->input->post("cache_time"));



		$user_display_type = intval($this->input->post("user_display_type"));

		$calendar_picker_format = $this->common->nohtml($this->input->post("calendar_picker_format"));

		$resize_avatar = intval($this->input->post("resize_avatar"));



		$public_profiles = intval($this->input->post("public_profiles"));

		$public_pages = intval($this->input->post("public_pages"));

		$public_blogs = intval($this->input->post("public_blogs"));

		$enable_blogs = intval($this->input->post("enable_blogs"));



		



		// Validate

		if (empty($site_name) || empty($site_email)) {

			$this->template->error(lang("error_23"));

		}

		$this->load->library("upload");



		if ($_FILES['userfile']['size'] > 0) {

			$this->upload->initialize(array( 

		       "upload_path" => $this->settings->info->upload_path,

		       "overwrite" => FALSE,

		       "max_filename" => 300,

		       "encrypt_name" => TRUE,

		       "remove_spaces" => TRUE,

		       "allowed_types" => $this->settings->info->file_types,

		       "max_size" => 2000,

		       "xss_clean" => TRUE

		    ));



		    if (!$this->upload->do_upload()) {

		    	$this->template->error(lang("error_21") 

		    	.$this->upload->display_errors());

		    }



		    $data = $this->upload->data();



		    $image = $data['file_name'];

		} else {

			$image= $this->settings->info->site_logo;

		}



		$this->admin_model->updateSettings(

			array(

				"site_name" => $site_name,

				"site_desc" => $site_desc,

				"know_the_law" => $know_the_law,

				"upload_path" => $upload_path,

				"upload_path_relative" => $upload_path_rel, 

				"site_logo"=> $image,  

				"site_email" => $site_email,

				"register" => $register,

				"avatar_upload" => $avatar_upload,

				"file_types" => $file_types,

				"disable_captcha" => $disable_captcha,

				"date_format" => $date_format,

				"file_size" => $file_size,

				"login_protect" => $login_protect,

				"activate_account" => $activate_account,

				"default_user_role" => $default_user_role,

				"secure_login" => $secure_login,

				"google_recaptcha" => $google_recaptcha,

				"google_recaptcha_secret" => $google_recaptcha_secret,

				"google_recaptcha_key" => $google_recaptcha_key,

				"logo_option" => $logo_option,

				"avatar_height" => $avatar_height,

				"avatar_width" => $avatar_width,

				"cache_time" => $cache_time,

				"user_display_type" => $user_display_type,

				"page_slugs" => $page_slugs,

				"disable_chat" => $disable_chat,

				"calendar_picker_format" => $calendar_picker_format,

				"resize_avatar" => $resize_avatar,

				"public_profiles" => $public_profiles,

				"public_pages" => $public_pages,

				"public_blogs" => $public_blogs,

				"enable_blogs" => $enable_blogs

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_13"));

		redirect(site_url("admin/settings"));

	}



	public function showcategories() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		

		$this->load->library("datatables");

		$this->datatables->set_default_order("catagories.CID", "asc");

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"catagories.c_name" => 0,

				 ),

				  1 => array(

				 	"catagories.parent_id" => 0,

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_categories()

		);

		$categories = $this->admin_model->get_categories($this->datatables);

		foreach($categories->result() as $r) {

			$this->datatables->data[] = array(

				$r->c_name,

				$r->price,

				$r->parent_id,

				'<a href="' . site_url("admin/edit_cat_pro/" . $r->cid) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . site_url("admin/delete_cat/" . $r->cid . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}







	public function moderator_requests()

	{

	

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		$this->template->loadData("activeLink", 

			array("admin" => array("moderator_requests" => 1)));

			

			$this->template->loadContent("admin/moderator_requests.php", array());

	}

	

	public function moderators_requests_page()

	{

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		$this->load->library("datatables");

		$this->datatables->set_default_order("users.ID", "desc");

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.username" => 0

				 ),

				 1 => array(

				 	"users.total_points" => 0

				 ),

				 2 => array(

				 	"users.moderator_apply" => 0

				 ),

				 3 => array(

				 	"users.Is_moderator" => 0

				 ),

				 4 => array(

				 	"users.user_role" => 0

				 ),

				 

			)

		);

	

	$this->datatables->set_total_rows(

			$this->admin_model->get_total_modrat_req()

		);

		



	$req = $this->admin_model->get_mod_requests($this->datatables);

	

	foreach($req->result() as $r) {

			 

			if($r->Is_moderator==0){

				

					$status="Pending";

				}

			else{

				

				$status= "Approved";

				}

				

			

			if($r->Is_moderator==0){	

		    $htmlbutton='

						 <a href="'.site_url("admin/accept_modr_requests/" . $r->userid ."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">'.lang("ctn_623").'</a> 

						 <a href="'.site_url("admin/reject_modr_requests/" . $r->userid . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Law Firm?\')">'.lang("ctn_624").'</a> 

					';

			}

			else if($r->Is_moderator==1){

				

				  $htmlbutton='

						 <a href="'.site_url("admin/cancel_modr/" . $r->userid ."/". $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">Deactive Moderator</a> 

					';

				}

			else{

			 $htmlbutton='

						 <a href="'.site_url("admin/reactivemodr/" . $r->userid ."/". $this->security->get_csrf_hash()).'" class="btn btn-warning btn-xs" onClick="javascript:return confirm(\'Are you sure to Accept Law Firm?\')">Reactivate Moderator</a> 

					';

				}

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				$r->total_points,

				$status,

				$htmlbutton,

				);

		}

		echo json_encode($this->datatables->process());

	}









	public function reactivemodr($id)

	{

	if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

	}

	$id = intval($id);

	$data=array(

		"Is_moderator"=>1,

		

	);

	$this->user_model->update_user($id,$data);

	$this->session->set_flashdata("globalmsg","Moderator is activated again..");

	redirect(site_url("admin/moderator_requests"));

	}

	public function cancel_modr($id)

	{

	

	if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

	$id = intval($id);

	$data=array(

		"Is_moderator"=>2,

	);

	$this->user_model->update_user($id,$data);

	$this->session->set_flashdata("globalmsg","Moderator is deactive. you can reactivate again any time!");

	redirect(site_url("admin/moderator_requests"));

	}



	public function reject_modr_requests($id){

			if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

			

		$users=$this->user_model->get_user_by_id($id);

		

		$users=$users->row();

		

	

		$this->template->loadContent("admin/reject_moderator.php", array(

			"users"=>$users,

			

			)

		);

		}



	public function rejectmodrerators($id)

		{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

			$reason=$this->input->post('reason');

	$data=array(

		"Is_moderator"=>4,

		"moderator_reject_reason"=>$reason,

		

	);

	$this->user_model->update_user($id,$data);

	$this->session->set_flashdata("globalmsg","Reject successfully!");

	redirect(site_url("admin/moderator_requests"));

		}





	public function accept_modr_requests($id)

	{

	

	if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

	

	$id = intval($id);

	$data=array(

		"Is_moderator"=>1,

	);

	$this->user_model->update_user($id,$data);

	$this->session->set_flashdata("globalmsg","Request is accepted!");

	redirect(site_url("admin/moderator_requests"));

	}



	

	







	public function judgements(){

		

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

			$this->template->loadData("activeLink", 

			array("admin" => array("judgements" => 1)));

		



	$this->template->loadContent("admin/judgements.php", array());

	}

	public function supreme(){

		

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

			$this->template->loadData("activeLink", 

			array("admin" => array("supreme" => 1)));

		



	$this->template->loadContent("admin/supreme.php", array());

	}





	public function show_supreme_judgements() {
	    if (!$this->user->info->admin && !$this->user->info->admin_members) {
	        $this->template->error(lang("error_2"));
	    }
	    $this->load->library("datatables");
	    $this->datatables->set_default_order("supreme.ID", "desc");
	    // Set page ordering options that can be used
	    $this->datatables->ordering(array(0 => array("supreme.year" => 0)));
	    $this->datatables->set_total_rows($this->admin_model->get_total_supreme_judgements());
	    $blogs = $this->admin_model->get_supreme_judgements($this->datatables);
	    foreach ($blogs->result() as $r) {
	        $this->datatables->data[] = array(
	        	$r->year,  
	        	$r->heading, 
	        	'<a href="' . site_url("admin/edit_supreme_judgements/" . $r->ID) . '" class="btn btn-warning btn-xs" title="' . lang("ctn_55") . '"><span class="glyphicon glyphicon-edit"></span></a>  
	        	<a href="' . site_url("admin/mark_as_landmark_supreme_judgement/" . $r->ID) . '" class="btn btn-primary btn-xs" title="Mark as Landmark Judgement"><span class="glyphicon glyphicon-cog"></span></a>'
	        );
	    }
	    echo json_encode($this->datatables->process());
	}







	public function show_judgements(){



	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		$this->load->library("datatables");

		$this->datatables->set_default_order("high_court_ap_yearly.ID", "desc");

		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"high_court_ap_yearly.year" => 0

				 ),	

				 1 => array(

				 	"high_court_ap_yearly.state" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_judgements()

		);

		$blogs = $this->admin_model->get_judgements($this->datatables);



		foreach($blogs->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->year,

				$r->c_name,

				$r->heading,

				$r->state,

			    '<a href="' . site_url("admin/edit_judgements/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  <a href="' . site_url("admin/mark_as_landmark_judgement/" . $r->id) .'" class="btn btn-primary btn-xs" title="Mark as Landmark Judgement"><span class="glyphicon glyphicon-cog"></span></a>'

			

			);

		

		}

		

		echo json_encode($this->datatables->process());





	}



	

	public function show_bareacts_titles(){



	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		$this->load->library("datatables");

		$this->datatables->set_default_order("high_court_ap_yearly.ID", "desc");

		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"high_court_ap_yearly.year" => 0

				 ),	

				 1 => array(

				 	"high_court_ap_yearly.state" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model->get_total_bareacts_count()

		);

		$blogs = $this->admin_model->get_bareacts($this->datatables);

		



		foreach($blogs->result() as $r) {

			 

			$this->datatables->data[] = array(

				

				$r->id,

				strip_tags($r->title),

			   

			   

			    '<a href="' . site_url("admin/add_bare_act_chapters/" . $r->id) .'" class="btn btn-warning btn-xs" title="Add Chapters"><span class="glyphicon glyphicon-plus"></span></a>  

			    <a href="' . site_url("admin/add_bare_act_sections/" . $r->id) .'" class="btn btn-primary btn-xs" title="Add Act Sections"><span class="glyphicon glyphicon-plus"></span></a>   

			    <a href="' . site_url("admin/add_bare_act_sections/" . $r->id) .'" class="btn btn-success btn-xs" title="Add Act SCHEDULE"><span class="glyphicon glyphicon-plus"></span></a>  

			    <a href="' . site_url("admin/add_bare_act_sections/" . $r->id) .'" class="btn btn-info btn-xs" title="Add Act Rules"><span class="glyphicon glyphicon-plus"></span></a>

			

			    <a href="' . site_url("admin/view_bare_acts/" . $r->id) .'" class="btn btn-danger btn-xs" title="view Bare Act"><span class="glyphicon glyphicon-eye-open"></span></a>



			    <a href="' . site_url("admin/add_bare_acts_with_cat/" . $r->id) .'" class="btn btn-danger btn-xs" title="view Bare Act"><span class="glyphicon glyphicon-eye-open"></span></a>

			    '

			

			);

		

		}

		

		echo json_encode($this->datatables->process());





	}

	

	



	public function view_bare_acts($id){



		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

			}



		$this->load->model("admin_model");

		$id = intval($id);



		$bareacts = $this->admin_model->view_bare_acts_by_id($id);

		$actschapters = $this->admin_model->view_bare_acts_chapters_by_id($id);

		$actssectionss = $this->admin_model->view_bare_acts_sectionss_by_id($id);



		$actssectionwithoutchapters = $this->admin_model->actssectionwithoutchapters($id);

		$actschapters =$actschapters->result();

		

		$bareacts=$bareacts->row();

		$actssectionss=$actssectionss->result();



		$actssectionwithoutchapters=$actssectionwithoutchapters->result();



		$this->template->loadContent("admin/view_acts_content.php", array(

			"actschapters" => $actschapters,

			"bareacts"=>$bareacts,

			"actssectionss"=>$actssectionss,

			"actssectionwithoutchapters"=>$actssectionwithoutchapters,

			)

		);

	}

	

	public function add_bare_act_sections($id)

	{

	

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

			}

	

		$this->load->model("admin_model");

		$id = intval($id);

		$acts = $this->admin_model->get_bare_act_by_id($id);

		$chapters = $this->admin_model->get_chapters_by_act_id($id);

		if($acts->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

	

		$acts=$acts->row();

		$chapters=$chapters->result();

		

		

	

	$this->template->loadContent("admin/add_act_sections.php", array(

			"acts" => $acts,

			"chapters" => $chapters,

			

			)

		);

	

	}

	

	

	public function add_act_sectionss($id)

	{

	

	$this->load->model("admin_model");

		$id = intval($id);

		

		$chapter_id = $this->common->nohtml($this->input->post("select_chapter"));

	 	$section_title = $this->common->nohtml($this->input->post("section_title"));

	 	$section_name = $this->common->nohtml($this->input->post("section_name"));

	 	$section_desc = $this->common->nohtml($this->input->post("section_desc"));

	

		if($chapter_id=="")

		{

		

		$chapter_id=null;

		}

	

		$data=array(

		

			"act_id"=> $id,

			"chapter_id"=> $chapter_id,

			"section_title"=> $section_title,

			"section_name"=> $section_name,

			"section_desc"=> $section_desc,

			

		);

		

		

			

			$post = $this->admin_model->save_sections($data);

			$this->session->set_flashdata("globalmsg","Section added successfully.");

			redirect(site_url("admin/add_bare_act_sections/".$id));

		

		

		

		

		

		

		

	}

	

	public function add_bare_act_chapters($id)

	{

	

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

			}

	

		$this->load->model("admin_model");

		$id = intval($id);

		$acts = $this->admin_model->get_bare_act_by_id($id);

		if($acts->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

	

		$acts=$acts->row();

		

		

	

	$this->template->loadContent("admin/add_act_chapters.php", array(

			"acts" => $acts,

			

			)

		);

	

	}

	

	public function add_act_chaptrs($id)

	{

		$this->load->model("admin_model");

		$id = intval($id);

		$acts = $this->admin_model->get_bare_act_by_id($id);

		

		$chapter_title = $this->common->nohtml($this->input->post("chapter_title"));

		$chapter_name = $this->common->nohtml($this->input->post("chapter_name"));

		$chapter_desc = $this->common->nohtml($this->input->post("chapter_desc"));

		

		$data=array(

		

			"act_id"=> $id,

			"chapter_title"=> $chapter_title,

			"chapter_name"=> $chapter_name,

			"chapter_desc"=> $chapter_desc,

			

		);

		

		//print_r($data);

		

		$post = $this->admin_model->get_added_chapters($chapter_title,$id);

		if($post->num_rows() == 0)

		{

			

			$post = $this->admin_model->save_chapters($data);

			$this->session->set_flashdata("globalmsg","Chapter added successfully.");

			redirect(site_url("admin/add_bare_act_chapters/".$id));

		}

		else

		{

			

			$this->session->set_flashdata("globalmsg","Chapter already added.");

			redirect(site_url("admin/add_bare_act_chapters/".$id));

		}

		

		

	

	}



	public function mark_as_landmark_judgement($id)

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		

		$data=array(

			"status"=>1

		);

		

		$post = $this->admin_model->mark_as_landmark_judgement($id,$data);

		

		$this->session->set_flashdata("globalmsg","Judgement Successfully Marked as Landmark!");

		redirect(site_url("admin/judgements"));

	}

	public function mark_as_landmark_supreme_judgement($id)

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$id = intval($id);

		

		$data=array(

			"status"=>1

		);

		

		$post = $this->admin_model->mark_as_landmark_supreme_judgement($id,$data);

		

		$this->session->set_flashdata("globalmsg","Supreme Judgement Successfully Marked as Landmark!");

		redirect(site_url("admin/supreme"));

	}



	public function edit_judgements($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->load->model("admin_model");

		$id = intval($id);

		$post = $this->admin_model->get_judgement_cat($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("admin_model" => 1)));

		

		$parentcat= $this->db->where("parent_id",0)->get("catagories")->result();

		

		$this->template->loadContent("admin/edit_judgements.php", array(

			"judgements" => $post,

			"parentcat" => $parentcat

			)

		);



		

	}

	public function edit_supreme_judgements($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		

		$this->load->model("admin_model");

		$id = intval($id);
		if ($this->input->post('update_supreme_judgement')) {

			$metatitle=$this->input->post('metatitle');


			$metadescription=$this->input->post('metadescription');


			$metakey=$this->input->post('metakey');

			$data=array(

				// "category"=>$catid,
				"meta_title"=>$metatitle,
				"meta_description"=>$metadescription,
				"metakeywords"=>$metakey,

			);

			$this->admin_model->update_supreme_judgement_category($id,$data);

			$this->session->set_flashdata("globalmsg","Supreme Judgement Meta data updated!");

			redirect(site_url("admin/supreme"));
		}
		$post = $this->admin_model->get_supreme_judgement_cat($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("admin_model" => 1)));

		

		$parentcat= $this->db->where("parent_id",0)->get("catagories")->result();

		

		$this->template->loadContent("admin/edit_supreme_judgements.php", array(

			"supreme_judgements" => $post,

			"parentcat" => $parentcat

			)

		);



		

	}
	public function edit_article($id) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

			$this->template->error(lang("error_2"));

		}

		$this->load->model("admin_model");

		$id = intval($id);

		// if ($this->input->post('metatitle')) {


		// 	$catid=$this->input->post('cat_option');


		// 	$metatitle=$this->input->post('metatitle');


		// 	$metadescription=$this->input->post('metadescription');


		// 	$metakey=$this->input->post('metakey');

		// 	$data=array(

		// 		"catagory"=>$catid,
		// 		"meta_title"=>$metatitle,
		// 		"meta_description"=>$metadescription,
		// 		"metakeywords"=>$metakey,

		// 	);

		// 	$this->admin_model->update_article_category($id,$data);

		// 	$this->session->set_flashdata("globalmsg","Article Category and Meta updated!");

		// 	redirect(site_url("admin/articles"));
		// }

		$post = $this->admin_model->get_articles_cat($id);

		if($post->num_rows() == 0) {

			$this->template->error(lang("error_173"));

		}

		$post = $post->row();



		$this->template->loadData("activeLink", 

			array("admin" => array("admin_model" => 1)));

		

		$parentcat= $this->db->where("parent_id",0)->get("catagories")->result();

		

		$this->template->loadContent("admin/edit_articles.php", array(

			"articles" => $post,

			"parentcat" => $parentcat

			)

		);



		

	}





public function update_judg_cat($id)

{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		$this->load->model("admin_model");

		$id = intval($id);

		$catid=$this->input->post('cat_option');


		$metatitle=$this->input->post('metatitle');


		$metadescription=$this->input->post('metadescription');


		$metakey=$this->input->post('metakey');

		$data=array(

			"category"=>$catid,
			"meta_title"=>$metatitle,
			"meta_description"=>$metadescription,
			"metakeywords"=>$metakey,

		);

		$this->admin_model->update_judgement_category($id,$data);

		$this->session->set_flashdata("globalmsg","Judgement Category and Meta data updated!");

		redirect(site_url("admin/judgements"));



}



	public function add_category() 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		$name = $this->common->nohtml($this->input->post("name"));

		//$desc = $this->common->nohtml($this->input->post("description"));

		$price = $this->common->nohtml($this->input->post("price"));

		$cat_slug=preg_replace('/\s+/', '-', $name);

		if(empty($name)) {

			$this->template->error(lang("error_82"));

		}



		$this->admin_model->add_category(array(

			"c_name" => $name,

			"cat_slug"=>$cat_slug,

			"price" => $price

			)

		);



			$add_cat_tmp_url = str_replace("/social","",base_url());

			$add_cat_url = $add_cat_tmp_url."lawyers/api/add_catagories_to_laravel.php";



			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $add_cat_url);

			curl_exec($ch);

		$this->session->set_flashdata("globalmsg", lang("success_115"));

		redirect(site_url("admin/categories"));

	}





public function add_category_price_location(){

		 

		 if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		 $parentid = $this->common->nohtml($this->input->post("parentcat"));

		 $location = $this->common->nohtml($this->input->post("location_city"));

		 $locations=explode(",",$location);

		 $loca= $locations[0];

		//$desc = $this->common->nohtml($this->input->post("description"));

		 $price = $this->common->nohtml($this->input->post("price"));

		 //$cat_slug=preg_replace('/\s+/', '-', $subcatname);

		if(empty($location)) {

			$this->template->error(lang("error_82"));

		}

		$this->admin_model->add_category_price_location(array(

				"catagoryid"=>$parentid,

				"location" =>$loca,

				"price"=>$price,

			

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_115"));

		redirect(site_url("admin/categories"));

	}



	public function add_sub_category(){

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}

		 $parentid = $this->common->nohtml($this->input->post("parentcat"));

		 $subcatname = $this->common->nohtml($this->input->post("name"));

		 $price = $this->common->nohtml($this->input->post("price"));

		 $cat_slug=preg_replace('/\s+/', '-', $subcatname);

			if(empty($subcatname)) {

				$this->template->error(lang("error_82"));

			}

		$this->admin_model->add_sub_category(array(

			"parent_id"=>$parentid,

			"c_name" =>$subcatname,

			"cat_slug"=>$cat_slug,

			"price" => $price

			)

		);

		$this->session->set_flashdata("globalmsg", lang("success_115"));

		redirect(site_url("admin/categories"));

	}



	public function edit_cat($id) 

	{

	if(!$this->user->info->admin && !$this->user->info->admin_members) {

		$this->template->error(lang("error_2"));

	}	

		$id = intval($id);

		$category = $this->admin_model->get_category($id);

		if($category->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}

		$category = $category->row();



		$name = $this->common->nohtml($this->input->post("name"));

		//$desc = $this->common->nohtml($this->input->post("description"));

		$price = $this->common->nohtml($this->input->post("price"));



		$cat_slug=preg_replace('/\s+/', '-', $name);



		if(empty($name)) {

			$this->template->error(lang("error_82"));

		}



		$this->admin_model->update_category($id, array(

			"c_name" => $name,

			"cat_slug"=>$cat_slug,

			"price" => $price

			)

		);



$ch = curl_init();

$data2 = 'cname='.$name.'&cat_slug='.$cat_slug.'&price='.$price.'&id='.$id;

	curl_setopt($ch, CURLOPT_URL, "http://18.188.193.80/lawyers/api/edit_catagory.php");

		curl_setopt($ch, CURLOPT_HEADER, 0);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);

		curl_exec($ch);

curl_close($ch);

		$this->session->set_flashdata("globalmsg", lang("success_116"));

		redirect(site_url("admin/categories"));

	}



	public function edit_cat_pro($id) {

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("categories" => 1)));



		$id = intval($id);

		$category = $this->admin_model->get_category($id);

		if($category->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}

		$category = $category->row();



		$this->template->loadContent("admin/edit_categories.php", array(

			"category" => $category

			)

		);

	}



	public function delete_cat($id, $hash) 

	{

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("categories" => 1)));

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$category = $this->admin_model->get_category($id);

		if($category->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}

		$ch = curl_init();

		$data2 = 'catid='.$id;

		curl_setopt($ch, CURLOPT_URL, "http://18.188.193.80/lawyers/api/delete_catagory.php");

		curl_setopt($ch, CURLOPT_HEADER, 0);

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);

		curl_exec($ch);

		curl_close($ch);

		$this->admin_model->delete_category($id);

		$this->session->set_flashdata("globalmsg", lang("success_117"));

		redirect(site_url("admin/categories"));

	}

	public function faq_category(){

		$this->template->loadData("activeLink", 

				array("admin" => array("faq_category" => 1)));

		$this->template->loadContent("admin/add_faq_category.php", array(

			)

		);

	}

	public function submit_faq_category(){

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}

		$faq_category_name = $this->common->nohtml($this->input->post("faq_category_name"));



		if(empty($faq_category_name)) {

			$this->template->error("Category name can't be empty.");

		}



		$check_faq_category = $this->admin_model->check_category_name($faq_category_name);

		if(!empty($check_faq_category)){

			$this->template->error("Category name already exist !");

		}



		$this->admin_model->add_faq_category(array(

			"faq_category_name" => $faq_category_name,

			"created_at" => date("Y-m-d h:i:s")

			)

		);



		$this->session->set_flashdata("globalmsg", "FAQ Category has been created.");

		redirect(site_url("admin/faq_category"));



		

	}

	public function faq_category_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("faq_category.ID", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"faq_category.faq_category_name" => 0

				 )

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_faq_category()

		);

		$faq_category = $this->admin_model->get_faq_category($this->datatables);



		foreach($faq_category->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->faq_category_name,

				'<a href="' . site_url("admin/edit_faq_category/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  

				<a href="' . site_url("admin/delete_faq_category/" . $r->id . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}

	public function delete_faq_category($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$faq_category = $this->admin_model->get_faq_category_by_id($id);

		if(empty($faq_category)) {

			$this->template->error("Invalid FAQ Category ID.");

		}



		$this->admin_model->delete_faq_category($id);

		// Delete posts

		//$this->blog_model->delete_all_blog_posts($blog->ID);

		$this->session->set_flashdata("globalmsg", "FAQ Category has been successfully deleted.");

		redirect(site_url("admin/faq_category"));



	}

	public function edit_faq_category($id) 

	{



		$id = intval($id);

		$faq_category = $this->admin_model->get_faq_category_by_id($id);

		if(empty($faq_category)) {

			$this->template->error("Invalid FAQ Category ID.");

		}

		$this->template->loadData("activeLink", 

				array("admin" => array("faq_category" => 1)));

		$this->template->loadContent("admin/edit_faq_category.php", array(

			"faq_category" => $faq_category

			)

		);

	}

	public function submit_edit_faq_category() 

	{

		

		$id = intval($_POST['id']);

		$faq_category = $this->admin_model->get_faq_category_by_id($id);

		if(empty($faq_category)) {

			$this->template->error("Invalid FAQ Category ID.");

		}

 

		$faq_category_name = $this->common->nohtml($this->input->post("faq_category_name"));



		if(empty($faq_category_name)) {

			$this->template->error("Category name can't be empty.");

		}



		$check_faq_category = $this->admin_model->check_category_name_by_id($id,$faq_category_name);

		if(!empty($check_faq_category)){

			$this->template->error("Category name already exist !");

		}



		$this->admin_model->update_faq_category($id, array(

			"faq_category_name" => $faq_category_name,

			"updated_at" => date("Y-m-d h:i:s")

			)

		);



		$this->session->set_flashdata("globalmsg", "FAQ Category has been successfully updated.");

		redirect(site_url("admin/faq_category"));

	}

	public function faq(){

		$faq_category	=	$this->admin_model->get_all_faq_category(); 

		$this->template->loadData("activeLink", 

				array("admin" => array("faq" => 1)));

		$this->template->loadContent("admin/add_faq.php", array(

			'faq_category'=>$faq_category

			)

		);

	}

	public function submit_faq(){

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}

		$faq_title = $this->common->nohtml($this->input->post("faq_title"));

		$faq_text = $this->common->nohtml($this->input->post("faq_text"));

		$faq_category_id = $this->common->nohtml($this->input->post("faq_category_id"));



		if(empty($faq_title)) {

			$this->template->error("FAQ title name can't be empty.");

		}

		if(empty($faq_text)) {

			$this->template->error("FAQ text name can't be empty.");

		}

		if(empty($faq_category_id)) {

			$this->template->error("FAQ category name can't be empty.");

		}



		$this->admin_model->add_faq(array(

			"title" => $faq_title,

			"text" => $faq_text,

			"category_id" => $faq_category_id,

			"created_at" => date("Y-m-d h:i:s")

			)

		);



		$this->session->set_flashdata("globalmsg", "FAQ has been created.");

		redirect(site_url("admin/faq"));

	

	}

	public function faq_page() 

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("faq.id", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"faq.title" => 0

				 ),

				 1 => array(

					"faq.text" => 0

				)

			)

		);

		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_faq()

		);

		$faq = $this->admin_model->get_faq($this->datatables);



		foreach($faq->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->title,

				$r->text,

				$this->admin_model->get_faq_category_name($r->category_id),

				'<a href="' . site_url("admin/edit_faq/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a>  

				<a href="' . site_url("admin/delete_faq/" . $r->id . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}

	public function delete_faq($id, $hash) 

	{

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$faq = $this->admin_model->get_faq_by_id($id);

		if(empty($faq)) {

			$this->template->error("Invalid FAQ ID.");

		}



		$this->admin_model->delete_faq($id);

		// Delete posts

		//$this->blog_model->delete_all_blog_posts($blog->ID);

		$this->session->set_flashdata("globalmsg", "FAQ has been successfully deleted.");

		redirect(site_url("admin/faq"));



	}

	public function edit_faq($id) 

	{



		$id = intval($id);

		$faq = $this->admin_model->get_faq_by_id($id);

		$faq_category	=	$this->admin_model->get_all_faq_category(); 

		if(empty($faq)) {

			$this->template->error("Invalid FAQ  ID.");

		}

		$this->template->loadData("activeLink", 

				array("admin" => array("faq" => 1)));

		$this->template->loadContent("admin/edit_faq.php", array(

			"faq" => $faq,

			"faq_category" => $faq_category

			)

		);

	}

	public function submit_edit_faq(){



		

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		if(!$this->user->loggedin) {

			redirect(site_url("login"));

		}



		$id = intval($_POST['id']);

		$faq = $this->admin_model->get_faq_by_id($id);

		if(empty($faq)) {

			$this->template->error("Invalid FAQ Category ID.");

		}



		$faq_title = $this->common->nohtml($this->input->post("faq_title"));

		$faq_text = $this->common->nohtml($this->input->post("faq_text"));

		$faq_category_id = $this->common->nohtml($this->input->post("faq_category_id"));



		if(empty($faq_title)) {

			$this->template->error("FAQ title name can't be empty.");

		}

		if(empty($faq_text)) {

			$this->template->error("FAQ text name can't be empty.");

		}

		if(empty($faq_category_id)) {

			$this->template->error("FAQ category name can't be empty.");

		}



		$this->admin_model->update_faq($id,array(

				"title" => $faq_title,

				"text" => $faq_text,

				"category_id" => $faq_category_id,

				"updated_at" => date("Y-m-d h:i:s")

			)

		);



		$this->session->set_flashdata("globalmsg", "FAQ has been updated.");

		redirect(site_url("admin/faq"));

	

	}



		public function change_city_price($id) 

	{



		$id = intval($id);

		$plan_detail = $this->admin_model->city_plan_detail($id);

	

		// $faq_category	=	$this->admin_model->get_all_faq_category(); 

		if(empty($plan_detail->result())) {

			$this->template->error("Invalid FAQ  ID.");

		}



		$this->template->loadContent("admin/change_plan_price.php", array(

			"plan_detail" => $plan_detail->result()

			)

		);

	}

		public function change_city_plan_pro() 

	{

		

		$data = array(

			'id' => $_POST['city_id'],

			'city_price' => $_POST['price']

			

		);

		

		$plan_detail = $this->admin_model->city_plan_price_change($data);

		

		$this->session->set_flashdata("globalmsg", "City Price Has Been Updated.");

			// redirect(site_url("admin/set_city_price"));

		$response = [];

		if($plan_detail){

			$this->session->set_flashdata("globalmsg", "Search Feature Plan Price Has Been Updated.");

			$response = array(

				"status" => "true"

			);

		}

		else{

			$response = array(

				"status" => "false"

			);

		}

		

			echo json_encode($response);



	

	}

		public function change_city_cat_price($id) 

	{



		$id = intval($id);

		$plan_detail = $this->admin_model->city__cat_plan_detail($id);

	

		// $faq_category	=	$this->admin_model->get_all_faq_category(); 

		if(empty($plan_detail->result())) {

			$this->template->error("Invalid FAQ  ID.");

		}



		$this->template->loadContent("admin/change_cat_city_plan_price.php", array(

			"plan_detail" => $plan_detail->result()

			)

		);

	}

		public function change_city_cat_plan_pro() 

	{

		

		$data = array(

			'id' => $_POST['city_id'],

			'city_price' => $_POST['price']

			

		);

		

		$plan_detail = $this->admin_model->city_cat_plan_price_change($data);

		$response = [];

		if($plan_detail){

			$this->session->set_flashdata("globalmsg", "Search Feature Plan Price Has Been Updated.");

			$response = array(

				"status" => "true"

			);

		}

		else{

			$response = array(

				"status" => "true"

			);

		}

		

			echo json_encode($response);

			// redirect(site_url("admin/set_city_cat_price"));	

		



		// if($plan_detail){

		// 	$this->session->set_flashdata("globalmsg", "City Price Has Been Updated.");

		// 	redirect(site_url("admin/set_city_price"));

		// }

	

	}



	public function lawyers_plans($id,$type) 

	{

		$id = intval($id);

		$type = $type;

		if($type == 0){

			$this->template->loadData("activeLink", 

			array("admin" => array("payment_plans" => 1)));

			$this->template->loadContent("admin/get_lawyers_city_plan.php", array(

				"id" => $id,

				'type'=> $type,

				)

			);

		}

		else{

			$this->template->loadData("activeLink", 

			array("admin" => array("payment_plans" => 1)));

			$this->template->loadContent("admin/get_lawyers_search_plan.php", array(

				"id" => $id,

				'type'=> $type,

				)

			);

		}

		

		

	}



	// get lawyers all city plan

	public function get_lawyers_city_plans($plan_id,$plan_type) 

	{	

		

		$current_date = date('d-m-Y');

		$current_date = strtotime($current_date);



		$this->load->library("datatables");

		$this->datatables->set_default_order("city_subscription_plan.id", "desc");

		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"city_subscription_plan.id" => 0

				 ),

				

				 1 => array(

				 	"city_subscription_plan.city_name" => 0

				 ),

				 2 => array(

				 	"city_subscription_plan.start_date" => 0

				 ),

				 3 => array(

				 	"city_subscription_plan.end_date" => 0

				 )

				

			)

		);

		$ads = $this->admin_model->get_lawyer_all_plans($plan_id,$this->datatables);

		$all_plan = $ads->result();

		if($ads->num_rows() > 0){

			$all_plan_data = array_map(function($all_plan) use($current_date){

				if($all_plan->end_date > $current_date){

					$all_plan->status = 1;

				}

				else{

					$all_plan->status = 0;

				}

				

			

				return $all_plan;

			},$all_plan);

		}



		



		$this->datatables->set_total_rows(

			$this->admin_model->get_total_active_city_plan_count($plan_id,$plan_type)

		);

		foreach($all_plan_data as $r) {

		

			$options = "";

			// if($r->status == 0) {

			// 	$status = lang("ctn_701");

			// 	$options .= '<a href="'.site_url("admin/accept_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> ';

			// } elseif($r->status == 1) {

			// 	$status = lang("ctn_702");

			// } elseif($r->status == 2) {

			// 	$status = lang("ctn_703");

			// }

			$this->datatables->data[] = array(

				// '<a href="'.site_url("home/index/3?postid=" . $r->postid).'">'.lang("ctn_749").'</a>',

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				

				// $r->name,

				$r->city_name,

				date('d-M-Y',$r->start_date),

				date('d-M-Y',$r->end_date),

				($r->status == 1 ? "Active" : "Inactive")

				

				// date($this->settings->info->date_format, $r->timestamp),

				

			);

		}

		echo json_encode($this->datatables->process());

	}





	//get lawyers all search plan

	public function get_lawyers_search_plans($plan_id,$plan_type) 

	{

		

		$current_date = date('d-m-Y');

		$current_date = strtotime($current_date);



		$this->load->library("datatables");

		$this->datatables->set_default_order("city_subscription_plan.id", "desc");

		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"city_subscription_plan.id" => 0

				 ),

				 1 => array(

				 	"city_subscription_plan.city_name" => 0

				 ),

				 2 => array(

				 	"city_subscription_plan.category_name" => 0

				 ),

				 3 => array(

				 	"city_subscription_plan.start_date" => 0

				 ),

				 4 => array(

				 	"city_subscription_plan.end_date" => 0

				 ),

				 5 => array(

				 	"city_subscription_plan.status" => 1

				 )

				 

			)

		);

		$ads = $this->admin_model->get_lawyer_all_plans($plan_id,$this->datatables);

		$all_plan = $ads->result();

		if($ads->num_rows() > 0){

			$all_plan_data = array_map(function($all_plan) use($current_date){

				if($all_plan->end_date > $current_date){

					$all_plan->status = 1;

				}

				else{

					$all_plan->status = 0;

				}

				

			

				return $all_plan;

			},$all_plan);

		}



		$this->datatables->set_total_rows(

			$this->admin_model->get_total_active_search_plan_count($plan_id,$plan_type)

		);

		foreach($ads->result() as $r) {

		

			$options = "";

			// if($r->status == 0) {

			// 	$status = lang("ctn_701");

			// 	$options .= '<a href="'.site_url("admin/accept_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs">'.lang("ctn_623").'</a> <a href="'.site_url("admin/reject_post/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs">'.lang("ctn_624").'</a> ';

			// } elseif($r->status == 1) {

			// 	$status = lang("ctn_702");

			// } elseif($r->status == 2) {

			// 	$status = lang("ctn_703");

			// }

			$this->datatables->data[] = array(

				// '<a href="'.site_url("home/index/3?postid=" . $r->postid).'">'.lang("ctn_749").'</a>',

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp, "first_name" => $r->first_name, "last_name" => $r->last_name)),

				

				// $r->name,

				$r->city_name,

				$r->category_name,

				date('d-M-Y',$r->start_date),

				date('d-M-Y',$r->end_date),

				($r->status == 1 ? "Active" : "Inactive")

				

				// date($this->settings->info->date_format, $r->timestamp),

				

			);

		}

		echo json_encode($this->datatables->process());

	}

	public function lawyer_list(){

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}

		$list_type = "pending";

		$list_type_url = $this->uri->segment("3");

		$list_type_active["list_type"] = "pending";

		if($list_type_url == "pending") {

			$list_type = "pending";

			$list_type_active["list_type"] = "pending";

		}elseif($list_type_url == "verified") {

			$list_type = "verified";

			$list_type_active["list_type"] = "verified";

		}elseif($list_type_url == "rejected") {

			$list_type = "rejected";

			$list_type_active["list_type"] = "rejected";

		}else{

			redirect(site_url("admin/lawyer_list/pending"));

		}

		$this->template->loadData("activeLink", 

			array(

				"admin" => array(

					"lawyer_list" => 

							$list_type_active

						

					)

				)

			);

		$this->template->loadContent("admin/lawyers_list.php", array(

			"list_type" => $list_type

			)

		);

		

	}



	public function lawyer_list_data()

	{



		$list_type_url = $this->uri->segment("3");

		if($list_type_url == "pending") {

			$list_type = "0";

		}

		if($list_type_url == "verified") {

			$list_type = "1";

		}

		if($list_type_url == "rejected") {

			$list_type = "2";

		}

		// echo $list_type_url;

		// die();

		$this->load->library("datatables");

		$this->datatables->set_default_order("users.first_name", "asc");

		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"users.first_name" => 0

				 ),

				 1 => array(

					"users.email" => 0

				),

				2 => array(

					"users.verified" => 0

				),

				3 => array(

					"coreprofile.created_at" => 0

				)

			)

		);

		

		$this->datatables->set_total_rows(

			$this->admin_model->get_total_lawyers($list_type)

		);

		$req = $this->admin_model->get_lawyer_list($this->datatables,$list_type);

		foreach($req->result() as $r) {

			$status = "Unverified";

			if($r->admin_verified == "1"){

				$status = "Verified";

			}

			if($r->admin_verified == "12"){

				$status = "Rejected";

			}

			$htmlbutton='<a target="_blank" href="'.site_url("admin/view_lawyer_detail/" . $r->userid ."/").'" class="btn btn-success btn-xs" >View detail</a>';

			$this->datatables->data[] = array(

					$r->first_name,

					$r->email,

					$status,

					$r->created_at,

					$htmlbutton

				);

		}

		echo json_encode($this->datatables->process());

	}



	public function view_lawyer_detail(){

		

		$user_id = $this->uri->segment('3');

		$user_info = $this->admin_model->get_user_info($user_id);

		if(empty($user_info)){

			redirect(site_url("admin/lawyer_list"));

		}

		

		$coreprofile = $this->admin_model->get_coreprofile($user_id);

		$consultation_timing = $this->admin_model->get_consultation_timing($user_id);

		$verification_logs = $this->admin_model->get_verification_log($user_id);

		$this->template->loadData("activeLink", 

			array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("admin/lawyer_detail.php",array("user_info" => $user_info,"coreprofile"=>$coreprofile,"verification_logs"=>$verification_logs,"consultation_timing"=>$consultation_timing));

	}

	public function approveLawyer(){

		$lawyer_id = $this->input->post("lawyer_id");

		$message = $this->input->post("message");

		$status = $this->input->post("status");

		$user_info = $this->admin_model->get_user_info($lawyer_id);

		$data['admin_verified'] = $status;

		$verification_log = array(

			'user_id' => $lawyer_id,

			'admin_id' => $this->user->info->ID,

			"message" => $message,

			"status" => $status,

			"created_at" => date("Y-m-d h:i:s")

		);

		// echo "<pre>";

		// print_r($_POST);

		// die();

		$body = "";

		if(empty($user_info)){

			$result['status'] = false;

			// redirect(site_url("admin/lawyer_list"));

		}else{

			$result['status'] = true;

			$this->admin_model->update_user($data,$lawyer_id);

			$this->admin_model->insert_user_verification_log($verification_log);

			if($status == 1){

				$updatepoints = $this->admin_model->updatepoints($lawyer_id,array(

					"points" => $user_info['points']+3000,

					"bonus_points" => $user_info['bonus_points']+3000,

					"total_points" => $user_info['total_points']+3000,

				));

				

				$this->admin_model->add_rewards_points(array(

					"userid" => $lawyer_id,

					"notes" => "Profile verification",

					"points" => 3000

					)

				);

				$this->admin_model->add_score(array(

					"userid" => $lawyer_id,

					"notes" => "Profile verification",

					"score" => 3000

					)

				);



				$body = "Your profile has been approved successfully. 3000 bonus points has been credited in your account";

				$this->session->set_flashdata("globalmsg", "Profile has been approved successfully.");

			}

			if($status == 2){

				$body = "Your profile has been rejected.";

				$body .= "Reason : " . $message;

				$this->session->set_flashdata("globalmsg", "Profile has been rejected successfully.");

			}

		}

		$this->load->library('email');

        $this->email->from("admin@lawsmos.com");

		$this->email->to($user_info['email']);

        $this->email->subject("Account Verification");

        $this->email->message($body);

        $this->email->send();

		echo json_encode($result);

	}



	// Court

	public function verify_court(){

		$court_id = $this->input->post("court_id");

		$court_name = $this->input->post("court_name");

		$status = "1";

		$court_data = array(

			'court_name' => $court_name,

			"status" => $status

		);

		$update_court = $this->admin_model->update_court_data($court_data,$court_id);

		if(empty($update_court)){

			$result['status'] = false;

		}else{

			$result['status'] = true;

		}

		

		echo json_encode($result);

	}



	public function court_list($page = 0) 

	{

		if(!$this->user->info->admin && !$this->user->info->admin_settings) {

			$this->template->error(lang("error_2"));

		}



		$this->template->loadData("activeLink", 

			array("admin" => array("court_list" => 1)));



		$this->template->loadContent("admin/court_list.php", array(

			)

		);

	}



	public function court_list_ajax()  {

		$this->load->library("datatables");



		$this->datatables->set_default_order("courts.id", "asc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				0 => array(

					"courts.court_name" => 0

				)

			)

		);



		$this->datatables->set_total_rows(

			$this->admin_model

				->get_total_courts()

		);

		$courts = $this->admin_model->get_courts($this->datatables);



		foreach($courts->result() as $r) {

			 

			$this->datatables->data[] = array(

				$r->court_name,				

				($r->status == 1 ? "Active" : "Inactive"),

				'<a href="' . site_url("admin/edit_court/" . $r->id) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a> 



				<a href="' . site_url("admin/delete_court/" . $r->id . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>'

			);

		}

		echo json_encode($this->datatables->process());

	}

	public function add_court() {

		$name = $this->common->nohtml($this->input->post("court-name"));

		$status = $this->common->nohtml($this->input->post("status"));

		if(empty($name)) {

			$this->template->error(lang("error_82"));

		}

		$this->admin_model->add_court(array(

			"court_name" => $name,

			"status" => $status,

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_129"));

		redirect(site_url("admin/court_list"));

	}





	public function edit_court($id) {

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("court_list" => 1)));



		$id = intval($id);

		$court = $this->admin_model->get_court($id);

		if($court->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}

		$court = $court->row();		



		if ($this->input->post("court-name")) {

			$name = $this->common->nohtml($this->input->post("court-name"));

			$status = $this->common->nohtml($this->input->post("status"));

			$this->admin_model->update_court($id, array(

				"court_name" => $name,

				"status" => $status,

				)

			);



			$this->session->set_flashdata("globalmsg", lang("success_130"));

			redirect(site_url("admin/court_list"));

		}else{

			$this->template->loadContent("admin/edit_court.php", array(

				"court" => $court

				)

			);

		}

	}

	public function delete_court($id, $hash)  {

		if(!$this->common->has_permissions(array("admin",

			"admin_settings"), $this->user)) {

			$this->template->error(lang("error_2"));

		}

		$this->template->loadData("activeLink", 

			array("admin" => array("court_list" => 1)));

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);

		$court = $this->admin_model->get_court($id);

		if($court->num_rows() == 0) {

			$this->template->error(lang("error_83"));

		}



		$this->admin_model->delete_court($id);

		$this->session->set_flashdata("globalmsg", lang("success_131"));

		redirect(site_url("admin/court_list"));

	}

	public function metadata()
	{
		$this->template->loadContent("admin/metadata.php");
	}



}



?>

