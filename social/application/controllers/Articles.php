<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		$this->load->model("home_model");
		$this->load->model("article_model");
		$this->load->model("login_model");
		$this->load->model("register_model");
		$this->load->model("admin_model");

		$this->template->loadData(
			"activeLink",
			array("home" => array("general" => 1))
		);
		// If the user does not have premium. 
		// -1 means they have unlimited premium.
		$this->template->set_layout("client/themes/titan.php");
		$this->template->set_error_view("error/login_error.php");
	}


	public function create()
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$s = $this->user_model->get_catagories();
		$result = $s->result();
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent("articles/create.php", array(
			"catagory" => $result,
			"getprofile" => $getprofile
		));
	}


	public function subcatg()
	{
		$id = $_GET['category'];
		$subcat = $this->user_model->get_sub_catag($id);
		$resultsub = $subcat->result();
		foreach ($resultsub as $subcat) {
			echo "<option value='" . $subcat->cid . "'>$subcat->c_name</option>";
		}
	}

	public function save_article(){
		try{
			if (!$this->user->loggedin) {
				// redirect(site_url("login"));
				echo json_encode("failed");exit;
			}
			// var_dump($this->input->get());die;
			$title = $this->common->nohtml($this->input->get("title"));
			$descriptions = $this->input->get("description");
			$metadescriptions = $this->common->nohtml($this->input->get("metadescription"));
			$keywords = $this->common->nohtml($this->input->get("keywords"));
			$cat = $this->common->nohtml($this->input->get("cat"));
			$subcat = $this->common->nohtml($this->input->get("subcat"));
			$id = $this->common->nohtml($this->input->get("articleid"));
			$publishstatus = $this->common->nohtml($this->input->get("save_as"));
			$data = array(
				"title" => $title,
				"meta_title" => $title,
				"catagory" => $cat,
				"subcatagory" => $subcat,
				"description" => base64_encode($descriptions),
				"meta_description" => $metadescriptions,
				"keywords" => $keywords,
				"metakeywords" => $keywords,
				"userid" => $this->user->info->ID,
				"private" => 0,
				"timestamp" => time(),
				"status" => 1,
				"article_status" => $publishstatus
			);
			if ($id) {
				$res = $this->article_model->update_article($id,$data);
			}else{
				$res = $this->article_model->add_article($data);
			}
			if ($res) { echo json_encode("success"); }
			else{ echo json_encode("error"); }
			exit;
		}catch(Exception $e){
			echo json_encode(['error'=>$e->getMessage()]);
		}

	}

	public function create_article_pro()
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$title = $this->common->nohtml($this->input->post("title"));
		// $descriptions = $this->common->nohtml($this->input->post("description"));
		$descriptions = $this->input->post("description");
		$metadescriptions = $this->common->nohtml($this->input->post("metadescription"));

		$linkarr = array();

		$arr = $this->admin_model->get_all_special_wordss();
		$arr = $arr->result();

		$wordsarry = array();
		$wordslink = array();

		$keywords = $this->common->nohtml($this->input->post("keywords"));
		$cat = $this->common->nohtml($this->input->post("cat"));
		$subcat = $this->common->nohtml($this->input->post("subcat"));

		$private = intval($this->input->post("private"));

		if (empty($title)) {
			$this->session->set_flashdata("error_title", "Title Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($descriptions)) {
			$this->session->set_flashdata("error_des", "Descriptions Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($metadescriptions)) {
			$this->session->set_flashdata("error_des", "Meta Descriptions Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($keywords)) {
			$this->session->set_flashdata("error_key", "Keywords Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($cat)) {
			$this->session->set_flashdata("error_cat", "Catagory Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($subcat)) {
			$this->session->set_flashdata("error_subcat", "Subcatagory Cannot be empty.");
			redirect(site_url("articles"));
		}
		if ($private == NULL) {
			$this->session->set_flashdata("error_private", "Private Cannot be empty.");
			redirect(site_url("articles"));
		}


		$this->article_model->add_article(
			array(
				"title" => $title,
				"meta_title" => $title,
				"catagory" => $cat,
				"subcatagory" => $subcat,
				"description" => base64_encode($descriptions),
				"meta_description" => $metadescriptions,
				"keywords" => $keywords,
				"metakeywords" => $keywords,
				"userid" => $this->user->info->ID,
				"private" => ($private == 2 ? 0 : 1),
				"timestamp" => time(),
				"status" => 1
			)
		);
		$this->session->set_flashdata("globalmsg", "Article Created Successfully..");
		redirect(site_url("article/view"));
	}

	public function  your($type = 0, $hashtag = "")
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}



		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location = $this->user->info->city;
		$location = explode(",", $location);
		$city = $location[0];
		$leads = $this->home_model->get_leads($city);
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview = $getarticlesforreview->result_array();



		$article = $this->article_model->get_user_article($this->user->info->ID);
		$article = $article->row();
		$this->template->loadContent("articles/your.php", array(
			"article" => $article,

			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd" => $topviewd,
			"getprofile" => $getprofile,
			"leads" => $leads,
			"countmessage" => $countmessage,
			"getarticlesforreview" => $getarticlesforreview,
		));
	}

	public function list_articles()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$article = $this->article_model->get_user_article($this->user->info->ID);

		$article = $article->row();
		$this->load->library("datatables");
		$this->datatables->set_default_order("user_articles.timestamp", "desc");
		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				1 => array(
					"user_articles.title" => 0
				),
				2 => array(
					"user_articles.description" => 0
				),
				3 => array(
					"user_articles.private" => 0
				),
				4 => array(
					"user_articles.keywords" => 0
				),

			)
		);
		$this->datatables->set_total_rows(
			$this->article_model->get_total_articles($this->user->info->ID)
		);
		$posts = $this->article_model->get_articles($this->user->info->ID, $this->datatables);

		foreach ($posts->result() as $r) {
			// if($r->Is_reviewed==0){
			// $options = '<a href="' . site_url("articles/add_edit_sections/" . $r->ID) .'" class="btn btn-success btn-xs" title="Add Sections"><span class="glyphicon glyphicon-plus"></span></a> <a href="' . site_url("articles/edit_article/" . $r->ID) .'" class="btn btn-warning btn-xs" title="'. lang("ctn_55").'"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . site_url("articles/delete_article/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>';
			// }
			// else if($r->Is_reviewed==1){
			// 	$options = '<a href="' . site_url("articles/add_edit_sections/" . $r->ID) .'" class="btn btn-success btn-xs" title="Add Sections"><span class="glyphicon glyphicon-plus"></span></a> <a href="' . site_url("articles/delete_article/" . $r->ID . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="'. lang("ctn_57") .'"><span class="glyphicon glyphicon-trash"></span></a>';
			// 	}
			// else{
			$options = '<a href="' . site_url("articles/add_edit_sections/" . $r->ID) . '" class="btn btn-success btn-xs" title="Add Sections"><span class="glyphicon glyphicon-plus"></span></a> <a href="' . site_url("articles/edit_article/" . $r->ID) . '" class="btn btn-warning btn-xs" title="' . lang("ctn_55") . '"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . site_url("articles/delete_article/" . $r->ID . "/" . $this->security->get_csrf_hash()) . '" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="' . lang("ctn_57") . '"><span class="glyphicon glyphicon-trash"></span></a>';
			// }


			$this->datatables->data[] = array(
				$r->title,
				$r->c_name,
				"<p id='description'>" . base64_decode($r->description) . "</p>",
				$r->keywords,
				$options,
			);
		}



		echo  json_encode($this->datatables->process());
	}


	public function edit_article($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$article = $this->article_model->get_user_articless($id);
		$article = $article->row();
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);

		$s = $this->user_model->get_catagories();
		$result = $s->result();

		// echo "<pre>";
		// print_r($article);
		// die();
		$this->template->loadContent("articles/editarticle.php", array(
			"article" => $article,
			"getprofile" => $getprofile,
			"catagory" => $result,
		));
	}

	public function edit_article_pro()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$title = $this->common->nohtml($this->input->post("title"));
		// $descriptions = $this->common->nohtml($this->input->post("description"));
		$descriptions = $this->input->post("description");
		$metadescriptions = $this->common->nohtml($this->input->post("metadescription"));

		$keywords = $this->common->nohtml($this->input->post("keywords"));
		$cat = $this->common->nohtml($this->input->post("cat"));
		$subcat = $this->common->nohtml($this->input->post("subcat"));

		$private = intval($this->input->post("private"));
		$articleid = $this->common->nohtml($this->input->post("articleid"));

		if (empty($title)) {
			$this->session->set_flashdata("error_title", "Title Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($descriptions)) {
			$this->session->set_flashdata("error_des", "Descriptions Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($metadescriptions)) {
			$this->session->set_flashdata("error_des", "Meta Descriptions Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($keywords)) {
			$this->session->set_flashdata("error_key", "Keywords Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($cat)) {
			$this->session->set_flashdata("error_cat", "Catagory Cannot be empty.");
			redirect(site_url("articles"));
		}
		if (empty($subcat)) {
			$this->session->set_flashdata("error_subcat", "Subcatagory Cannot be empty.");
			redirect(site_url("articles"));
		}
		if ($private == NULL) {
			$this->session->set_flashdata("error_private", "Private Cannot be empty.");
			redirect(site_url("articles"));
		}


		$this->article_model->update_article(
			$articleid,
			array(
				"title" => $title,
				"meta_title" => $title,
				"catagory" => $cat,
				"subcatagory" => $subcat,
				"description" => base64_encode($descriptions),
				"meta_description" => $metadescriptions,
				"keywords" => $keywords,
				"metakeywords" => $keywords,
				"userid" => $this->user->info->ID,
				"private" => ($private == 2 ? 0 : 1),
				"timestamp" => time(),
				"status" => 1
			)
		);
		$this->session->set_flashdata("globalmsg", "Article updated Successfully..");
		redirect(site_url("article/view"));
	}

	public function listarticles()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$article = $this->article_model->get_user_article($this->user->info->ID);
		$article = $article->row();
		$this->template->loadContent("articles/list.php", array(
			"article" => $article
		));
	}


	public function delete_article($id, $hash)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$post = $this->article_model->get_post_article_main($id);
		if ($post->num_rows() == 0) {
			$this->template->error(lang("error_173"));
		}
		$this->article_model->delete_post_article_main($id);
		$this->session->set_flashdata("globalmsg", "Article deleted Successfully..");
		redirect(site_url("article/view"));
	}

	public function add_edit_sections($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$sections = $this->article_model->get_user_article_sections($id);
		$artile = $this->article_model->get_user_articless($id);
		if ($sections->num_rows() == 0) {
			$sections = "";
			$this->template->loadContent(
				"articles/createsections.php",
				array(
					"sections" => $sections,
					"id" => $id,
				),
				1
			);
		}
		$sections = $sections->row();
		$artile = $artile->row();
		$this->template->loadContent(
			"articles/sections.php",
			array(
				"sections" => $artile,
				"id" => $id,
			)
		);
	}
	public function createsection($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$sections = $this->article_model->get_user_article_sections_parent($id);
		$sections = $sections->result_array();
		$this->template->loadContent(
			"articles/createsections.php",
			array(
				"sections" => $sections,
			)
		);
	}

	public function add_section_pro()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$blog = $this->article_model->get_user_article($this->user->info->ID);
		if ($blog->num_rows() == 0) {
			$this->template->error(lang("error_177"));
		}
		$blog = $blog->result_array();
		$title = $this->common->nohtml($this->input->post("title"));
		$parentid = $this->common->nohtml($this->input->post("parentid"));
		$artcleid = $this->common->nohtml($this->input->post("artcleid"));
		$section_contents = $this->lib_filter->go($this->input->post("section_content"));

		$arr = $this->admin_model->get_all_special_wordss();

		$arr = $arr->result();

		foreach ($arr as $a) {
			if (stripos($section_contents, $a->word) !== false) {
				//echo "word found".$a->word;	
				$word[] = $a->word;
				$link[] = "<a href='" . $a->link . "'>" . $a->word . "</a>";
			} else {
				//echo "no words found";
			}
		}

		// $a="<a href=''>".$word."</a>";


		$section_content =  str_replace($word, $link, $section_contents);


		if (empty($title)) {
			$this->session->set_flashdata("globalmsg", "Title cannot be empty");
			redirect(site_url("articles/add_edit_sections/" . $artcleid));
		}
		if (empty($section_content)) {
			$this->session->set_flashdata("globalmsg", "Section Content Cannot be empty..");
			redirect(site_url("articles/add_edit_sections/" . $artcleid));
		}

		// Upload
		$this->load->library("upload");
		$blog_image = "";
		if (isset($_FILES['userfile']) && $_FILES['userfile']['size'] > 0) {
			$this->upload->initialize(array(
				"upload_path" => $this->settings->info->upload_path,
				"overwrite" => FALSE,
				"max_filename" => 300,
				"encrypt_name" => TRUE,
				"remove_spaces" => TRUE,
				"allowed_types" => "gif|png|jpg|jpeg",
				"max_size" => $this->settings->info->file_size,
				"max_width" => 800,
				"max_height" => 800
			));

			if (!$this->upload->do_upload("userfile")) {
				$this->template->error(lang("error_21")
					. $this->upload->display_errors());
			}

			$data = $this->upload->data();

			$blog_image = $data['file_name'];
		}
		$time = 0;
		$postid = $this->article_model->add_article_section(
			array(
				"title" => $title,
				"body" => base64_encode($section_content),
				"timestamp" => time(),
				"parentid" => $parentid,
				"image" => $blog_image,
				"articleid" => $artcleid,
				"last_updated" => time()
			)
		);
		$this->session->set_flashdata("globalmsg", "Section Added Successfully..");
		redirect(site_url("articles/add_edit_sections/" . $artcleid));
	}


	public function preview($id)
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$article = $this->article_model->get_user_articless_preview($id);
		$article = $article->row();
		$sections = $this->article_model->get_preview_article_sections($id);
		$sections = $sections->result_array();
		$items	= $this->article_model->get_tree_menu($id);
		$items = $items->result_array();
		$menu	= $this->article_model->generateTree($items);
		$this->template->loadContent(
			"articles/preview.php",
			array(

				"aboutarticle" => $article,
				"sections" => $sections,
				"menu" => $menu,
			)
		);
	}


	public function your_article_sections($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$article = $this->article_model->get_user_articless($id);
		if ($article->num_rows() == 0) {
			$this->template->error(lang("error_177"));
		}
		$article = $article->row();
		$this->load->library("datatables");
		$this->datatables->set_default_order("user_articles_posts.last_updated", "desc");
		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				1 => array(
					"user_articles_posts.title" => 0
				),
				2 => array(
					"user_articles_posts.status" => 0
				),
				3 => array(
					"user_articles_posts.last_updated" => 0
				),
				4 => array(
					"user_articles_posts.views" => 0
				),

			)
		);
		$this->datatables->set_total_rows(
			$this->article_model
				->get_total_article_posts($article->ID)
		);
		$posts = $this->article_model->get_article_posts($article->ID, $this->datatables);
		foreach ($posts->result() as $r) {
			if ($r->status == 0) {
				$status = lang("ctn_768");
			} elseif ($r->status == 1) {
				$status = lang("ctn_794");
			}
			if (!empty($r->image)) {
				$image = '<img src="' . base_url() . $this->settings->info->upload_path_relative . '/' . $r->image . '" class="blog-post-thumb">';
			} else {
				$image = "";
			}

			if ($r->last_updated > 0) {
				$time = date($this->settings->info->date_format, $r->last_updated);
			} else {
				$time = lang("ctn_795");
			}
			$options = '<a href="' . site_url("articles/edit_section_posts/" . $r->ID) . '" class="btn btn-warning btn-xs" title="Edit Sections"><span class="glyphicon glyphicon-edit"></span></a> <a href="' . site_url("articles/delete_post/" . $r->ID . "/" . $this->security->get_csrf_hash()) . "/" . $id . '" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="' . lang("ctn_57") . '"><span class="glyphicon glyphicon-trash"></span></a>';
			$this->datatables->data[] = array(
				$r->title,
				$r->position_order,
				$time,
				$r->views,
				$options
			);
		}
		echo json_encode($this->datatables->process());
	}

	public function edit_section_posts($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$article = $this->article_model->get_user_article_sectionssss($id);
		$parent = $this->article_model->get_user_article_sections_parent($id);
		$article = $article->row();
		$this->template->loadContent("articles/editarticlesections.php", array(
			"sections" => $article
		));
	}
	public function edit_section_posts_pro()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$title = $this->common->nohtml($this->input->post("title"));
		$description = $this->common->nohtml($this->input->post("section_content"));
		$sectionid = $this->common->nohtml($this->input->post("sectionid"));
		$articleid = $this->common->nohtml($this->input->post("articleid"));
		if (empty($title)) {
			$this->template->error(lang("error_172"));
		}
		$this->article_model->update_article_sections(
			$sectionid,
			array(
				"title" => $title,
				"body" => base64_encode($description),
				"last_updated" => time()
			)
		);
		$this->session->set_flashdata("globalmsg", "Section updated Successfully..");
		redirect(site_url("articles/add_edit_sections/" . $articleid));
	}

	public function delete_post($id, $hash, $redirectid)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$post = $this->article_model->get_user_article_sectionssss($id);
		if ($post->num_rows() == 0) {
			$this->template->error(lang("error_173"));
		}
		$this->article_model->delete_post_article_sectionss($id);
		$this->session->set_flashdata("globalmsg", "Section delete Successfully..");
		redirect(site_url("articles/add_edit_sections/" . $redirectid));
	}

	public function review($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$id = intval($id);
		$post = $this->article_model->get_user_articless($id);
		if ($post->num_rows() == 0) {
			$this->template->error(lang("error_173"));
		}
		$this->article_model->update_review_status($id, array(
			"Is_reviewed" => 1,
		));
		$this->session->set_flashdata("globalmsg", "Your Article has been submitted successfully!");
		redirect(site_url("article/view"));
	}



	public function reviewarticles($id)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$id = intval($id);
		$post = $this->article_model->get_user_articless_preview($id);
		if ($post->num_rows() == 0) {
			$this->template->error(lang("error_173"));
		}
		$post = $post->row();
		$sections = $this->article_model->get_user_article_sections($id);
		$sections = $sections->result_array();
		$items	= $this->article_model->get_tree_menu($id);
		$items = $items->result_array();
		$menu	= $this->article_model->generateTree($items);
		$this->article_model->update_review_status($id, array(
			"on_hold" => 1,
			"reviewby" => $this->user->info->ID,
		));
		$this->template->loadContent("home/reviewarticle.php", array(
			"sections" => $sections,
			"aboutarticle" => $post,
			"menu" => $menu,
		));
	}



	function saveedit()
	{

		$coulmn = $_GET["heading"];
		$editvalue = $_GET["editval"];
		$id = $_GET["id"];

		$data = array(

			"title" => $editvalue,
		);

		$this->article_model->edit_heading($id, $data);
	}



	function savesectiontitle()
	{

		$coulmn = $_GET["heading"];
		$editvalue = $_GET["editval"];
		$id = $_GET["id"];

		$data = array(

			"title" => $editvalue,
		);

		$this->article_model->edit_section_heading($id, $data);
	}
	function saveArticleContent()
	{

		$coulmn = $_GET["heading"];
		$editvalue = $_GET["editval"];
		$id = $_GET["id"];

		$data = array(

			"description" => base64_encode($editvalue),
		);


		$this->article_model->edit_article_content($id, $data);
	}
	function savesectioncontent()
	{

		$coulmn = $_GET["heading"];
		$editvalue = $_GET["editval"];
		$id = $_GET["id"];

		$data = array(

			"body" => base64_encode($editvalue),
		);

		$this->article_model->edit_section_content($id, $data);
	}

	public function close_article($id, $hash)
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$this->article_model->update_review_status($id, array(
			"on_hold" => 0,
			"reviewby" => null,
		));
		redirect(site_url());
	}

	public function rejectarticle($id, $hash)
	{




		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);

		$this->article_model->update_review_status($id, array(
			"on_hold" => 0,
			"reviewby" => $this->user->info->ID,
			"Is_reviewed" => 3,
		));


		/* update moderator point */

		$userid = $this->user->info->ID;
		$points = $this->user->info->points;
		$bonus_points = $this->user->info->bonus_points;
		$total_points = $this->user->info->total_points;
		$cal_points = $points + 20;
		$cal_bonus_points = $bonus_points + 20;
		$cal_total_points = $total_points + 20;

		$updatepoints = $this->home_model->updatepoints($userid, array(
			"points" => $cal_points,
			"bonus_points" => $cal_bonus_points,
			"total_points" => $cal_total_points,
		));

		$this->page_model->add_history(
			array(
				"userid" => $userid,
				"notes" => "Article(s) Moderate",
				"points" => 100
			)
		);




		redirect(site_url());
	}

	public function approvearticle($id, $hash)
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$this->article_model->update_review_status($id, array(
			"on_hold" => 0,
			"reviewby" => $this->user->info->ID,
			"Is_reviewed" => 2,
			"status" => 1,
		));
		/* update moderator point */
		$userid = $this->user->info->ID;
		$points = $this->user->info->points;
		$bonus_points = $this->user->info->bonus_points;
		$total_points = $this->user->info->total_points;
		$cal_points = $points + 100;
		$cal_bonus_points = $bonus_points + 100;
		$cal_total_points = $total_points + 100;
		$updatepoints = $this->home_model->updatepoints($userid, array(
			"points" => $cal_points,
			"bonus_points" => $cal_bonus_points,
			"total_points" => $cal_total_points,
		));

		$this->page_model->add_history(
			array(
				"userid" => $userid,
				"notes" => "Article(s) Moderate",
				"points" => 100
			)
		);
		/* update user points who posted this article point */
		$post = $this->article_model->get_user_articless_preview($id);
		if ($post->num_rows() == 0) {
			$this->template->error(lang("error_173"));
		}
		$post = $post->row();
		$useridss = $post->useridss;
		$points = $post->points;
		$bonus_points = $post->bonus_points;
		$total_points = $post->total_points;
		$cal_points = $points + 200;
		$cal_bonus_points = $bonus_points + 200;
		$cal_total_points = $total_points + 200;

		$this->page_model->add_history(
			array(
				"userid" => $useridss,
				"notes" => "Article(s) Approval",
				"points" => 100
			)
		);


		$this->session->set_flashdata("globalmsg", "Kudos! You have successfully reviewed an article.");
		redirect(site_url());
	}
}
