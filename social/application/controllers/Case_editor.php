<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Case_editor extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		$this->load->model("home_model");
		$this->load->model("login_model");
		$this->load->model("register_model");
		// If the user does not have premium. 
		// -1 means they have unlimited premium
		$this->template->set_layout("client/themes/titan.php");
	}

	public function index()
	{
		$this->load->library('Ckeditor');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$userid=$this->user->info->ID;
		$case_id = $this->input->get('id');

		$case_info = $this->user_model->getCaseInfo($case_id);
		if(empty($case_info)){
			redirect(site_url('case_listings'));
		}
		
		$categories = $this->user_model->get_catagories_data()->result();

		$data['case_info'] = $case_info;
		$data['cats'] = $categories;
		$this->template->loadContent("case_editor/index.php", $data);
		
	}

public function research_editor()
{
	$this->load->library('Ckeditor');
	if(!$this->user->loggedin) {
		redirect(site_url("login"));
	}
	$id = $_GET['id'];
	$userid=$this->user->info->ID;
	$query=$this->user_model->get_reasearch_by_user($userid,$id)->row_array();
	// print_r($query);die("hello");
	if(empty($query)){
		redirect(site_url('case_listings/createreasearch'));
	}

	$subcat=$this->user_model->get_catagories_by_id($query['subcat'])->row_array();

	$saveddata = $this->db->get_where('user_saved_data', array('userid' => $userid))->result();
	$this->template->loadContent("reasearch_editor/index.php", array(
		"saveddata"=>$saveddata,
		"query" => $query,
		"subcat" => $subcat

	));
}

	public function saveresearch(){
		try{
			if (!$this->user->loggedin) {
				$result = [
					"message" 	=> 	"failed",
					"url" 		=> 	site_url("login")
				];
				echo json_encode($result);exit;
			}
			$userid 		= 		$this->user->info->ID;
			$title 			= 		$this->common->nohtml($this->input->post("title"));
			$descriptions 	= 		$this->input->post("description");
			$descriptions2 	= 		$this->input->post("description2");
			$scope 			= 		$this->input->post("scope");
			$jist 			= 		$this->input->post("jist");
			$cat 			= 		$this->common->nohtml($this->input->post("cat"));
			$subcat 		= 		$this->common->nohtml($this->input->post("subcat"));
			$id 			= 		$this->common->nohtml($this->input->post("researchid"));
			$publishstatus 	= 		$this->common->nohtml($this->input->post("save_as"));
			$data 			= 		array(
				"userid" 			=>  	$userid,
				"reasearch_heading"	=> 		$title,
				"catagory" 			=> 		$cat,
				"subcat" 			=> 		$subcat,
				"research_body" 	=>		base64_encode($descriptions),
				"research_body2" 	=>		base64_encode($descriptions2),
				"status"			=> 		$publishstatus,
		        "research_jist"		=>		$jist,
		        "scope"				=>		json_encode($scope),
			);
			if ($id) {
				$res = $this->user_model->update_reasearch_by_rowid($id,$data);
				$save_id = $id;
			}else{
				$insert=$this->user_model->insert_research($data);
				$save_id = $this->db->insert_id();
			}
			$url = "research";
			if ($save_id) { 
				$result = [
					"message" 	=> 	"success",
					"url" 		=> 	site_url($url)
				];
			} else{ 
				$result = [
					"message" 	=> 	"error",
					"url" 		=> 	''
				]; 
			}
			echo json_encode($result);exit;
		}catch(Exception $e){			
			$result = [
				"message" 		=> 	$e->getMessage(),
				"url" 			=> 	''
			]; 
			echo json_encode($result);exit;
		}
	}

public function createreasearch()
{
	$id = $this->user->info->ID;
 	$case_heading=$_POST['title'];
	$catg=$_POST['cat'];
	$subcatg=$_POST['subcat'];
 	$body = htmlentities($_POST["descriptions"]);
 	$body2 = htmlentities($_POST["descriptions2"]);
 	$status = $_POST["save_as"];
 	$scope = json_encode($_POST["scope"]);
 	$jist = $_POST["jist"];
	// die("hello");
	// $id=$this->user->info->ID;
 // 	$body = htmlentities($_POST["editor1"]);
	// 		//$value=$_REQUEST['editor1'];	
 // 	$case_heading=$_POST['case_heading'];
	// $catg=$_POST['catg'];
	// $subcatg=$_POST['subcatg'];
   // $current_timestamp=new \Datetime('now');
	$data = array(
        'userid'=>$id,
        'reasearch_heading'=>$case_heading,
        'catagory'=>$catg,
        'subcat'=>$subcatg,
        'research_body'=>$body,
        'research_body2'=>$body2,
        'status'=>$status,
        'research_jist'=>$jist,
        'scope'=>$scope,
    );
	$insert=$this->user_model->insert_research($data);
	$insert_id = $this->db->insert_id();
	//   $url="case_editor/research_editor?id=".$insert_id;
	$url = "research";
	if($insert_id){
		// $this->session->set_flashdata('success', "<span class='alert alert-success'>Research successfuly Created</span>"); 
		echo json_encode(["url"=>site_url($url),"message"=>"success"]);
	}else{
		echo json_encode(["message"=>"failed"]);
	}
	 
	//  return redirect(site_url($url));
}

public function create(){

	$id=$this->user->info->ID;
	$body = htmlentities($_POST["editor1"]);
			//$value=$_REQUEST['editor1'];	
	$case_heading=$_POST['case_heading'];
   // $current_timestamp=new \Datetime('now');
	$data = array(
        'loggged_user_id'=>$id,
        'case_body'=>$body,
        'case_heading'=>$case_heading,
      );
	$res = $this->db->insert('case_editor',$data);
    $insert_id = $this->db->insert_id();
	$url="case_editor?id=".$insert_id;
	if($res==true)
		{
			$this->session->set_flashdata('success', "<span class='alert alert-success'>Case successfuly Created</span>"); 
			return redirect(site_url($url));
		}
		else{
			$this->session->set_flashdata('error', "<span class='alert alert-danger'>Case Not Created</span>");
			return redirect(site_url($url));
		}
	}

public function save(){

		$userid=$this->user->info->ID;
		$db = get_instance()->db->conn_id;
		$body = htmlentities($_POST["editor1"]);
			//$value=$_REQUEST['editor1'];	
		$case_heading=$_POST['case_heading'];
		$case_id=$_POST['caseid'];
		$category=$_POST['catagory'];
		$current_timestamp=new \Datetime('now');
		$current_timestamp=(array)$current_timestamp;
		$current_timestamp=$current_timestamp['date'];
		$wherearray = array('loggged_user_id' => $userid, 'id' => $case_id);
		$query = $this->db->get_where('case_editor', array(
            'loggged_user_id' => $userid
        ));
		$count = $query->num_rows();
		$data = array(
			'case_body'=>$body,
			'case_heading'=>$case_heading,
			'case_category' => $category,
		 );
		if($count===0){
			$res = $this->db->insert('case_editor',$data);
		}
		else{
		$data = array(
			'case_body'=>$body,
			'case_heading'=>$case_heading,
			'case_category' => $category,
			'last_updated'=>$current_timestamp,
		 );
			$res = $this->db->where($wherearray)->update('case_editor',$data);
		}
		// $this->db->update('case_editor',$data)->where('logged_user_id',$id);
		if($res==true)
		{
			$this->session->set_flashdata('success', "<span class='alert alert-success'>successfuly saved</span>"); 
			
			redirect(site_url('case_listings'));
		}
		else{
			$this->session->set_flashdata('error', "<span class='alert alert-danger'>Save Failed</span>");
			redirect(site_url('case_listings'));
		}
		
}



	public function savecase_new() {
		// echo "nmghj";die;
		// echo json_encode($_POST);die;
	    $userid = $this->user->info->ID;
	    $db = get_instance()->db->conn_id;
	    $body = htmlentities($_POST["casecontent"]);
	    $status = $_POST['save_as'];
	    //$value=$_REQUEST['editor1'];
	    $case_heading = $_POST['casetitle'];
	    $case_id = $_POST['caseid'];
	    $category = $_POST['catagory'];
	    $current_timestamp = new \Datetime('now');
	    $current_timestamp = (array)$current_timestamp;
	    $current_timestamp = $current_timestamp['date'];
	    $wherearray = array('loggged_user_id' => $userid, 'id' => $case_id);
	    $query = $this->db->get_where('case_editor', array('loggged_user_id' => $userid));
	    $count = $query->num_rows();
	    if ($count === 0) {
	    	$data = array('case_body' => $body, 'case_heading' => $case_heading, 'case_category' => $category,'status' => $status, 'loggged_user_id' => $userid);
	        $res = $this->db->insert('case_editor', $data);
	    } else {
	        $data = array('case_body' => $body, 'case_heading' => $case_heading, 'case_category' => $category, 'last_updated' => $current_timestamp,'status' => $status, 'loggged_user_id' => $userid);
	        $res = $this->db->where($wherearray)->update('case_editor', $data);
	    }
	    // $this->db->update('case_editor',$data)->where('logged_user_id',$id);
	    if ($res) {
	    	echo json_encode("success");
	        // $this->session->set_flashdata('success', "<span class='alert alert-success'>successfuly saved</span>");
	        // redirect(site_url('case_listings'));
	    } else {
	    	echo json_encode("failed");
	        // $this->session->set_flashdata('error', "<span class='alert alert-danger'>Save Failed</span>");
	        // redirect(site_url('case_listings'));
	    }
	}


public function savereasech()
{

		$userid=$this->user->info->ID;
		$db = get_instance()->db->conn_id;
		$body = htmlentities($_POST["editor1"]);
			//$value=$_REQUEST['editor1'];	
		$case_heading=$_POST['case_heading'];
		$case_id=$_POST['caseid'];
		$current_timestamp=new \Datetime('now');
		$current_timestamp=(array)$current_timestamp;
		$current_timestamp=$current_timestamp['date'];
		$wherearray = array('userid' => $userid, 'ID' => $case_id);
		$query = $this->db->get_where('research', array(
            'userid' => $userid
        ));
		$count = $query->num_rows();
		// print_r($count);
		// die("hello");
		$data = array(
			'research_body'=>$body,
			'reasearch_heading'=>$case_heading,
		 );
		if($count===0){
			$res = $this->db->insert('research',$data);
		}
		else{
		$data = array(
			'research_body'=>$body,
			'reasearch_heading'=>$case_heading,
			'updated_at'=>$current_timestamp,
		 );
			$res = $this->db->where($wherearray)->update('research',$data);
		}
		// $this->db->update('case_editor',$data)->where('logged_user_id',$id);
		$url = "research";
		if($res==true)
		{
			$this->session->set_flashdata('success', "<span class='alert alert-success'>successfuly saved</span>"); 
			
			echo json_encode(["url"=>site_url($url),"message"=>"success"]);
		}
		else{
			$this->session->set_flashdata('error', "<span class='alert alert-danger'>Save Failed</span>");
			echo json_encode(["message"=>"failed"]);
		}
}

public function information() {
$id=$this->user->info->ID;
$q = $this->db->select('*')->from('case_editor')->where('loggged_user_id',$id)->get();
//return $q->result();
echo $q->result;
}

public function viewfile($id){
		$this->load->library('Ckeditor');
		$userid=$this->user->info->ID;
		$saveddata = $this->db->get_where('user_saved_data', array('userid' => $userid))->result();
		$this->template->loadContent("case_editor/case.php",array(
			"caseid" => $id,
			"saveddata"=>$saveddata,
			));
	}

public function viewresearch($id)
{
		$this->load->library('Ckeditor');
		$userid=$this->user->info->ID;
		$saveddata = $this->db->get_where('user_saved_data', array('userid' => $userid))->result();
		//echo $caseid;
		$query=$this->user_model->get_reasearch_by_user($userid,$id);
		$qry=$query->result();
		$qry[0]->subcat;
		$qrydd=$this->user_model->get_catagories_by_id($qry[0]->subcat);
		$qry33=$qrydd->result();
		$this->template->loadContent("reasearch_editor/case.php",array(
			"caseid" => $id,
			"saveddata"=>$saveddata,
			"query"=>$qry,
			"subcat"=>$qry33,
			));
}

public function savecase($id,$key){
	$userid=$this->user->info->ID;
	$query = $this->db->get_where('user_saved_data', array('caseid' => $id, 'userid'=>$userid));
	$getcase = $this->db->get_where('high_court_ap_yearly', array('ID' => $id))->result();
	//print_r($getcase);
	$count = $query->num_rows();
	$data = array(
			'userid'=>$userid,
			'caseid'=>$id,
			'case_heading'=>$getcase[0]->heading,
			'datakey'=>$key,
		 );
	if($count===0){
			$res = $this->db->insert('user_saved_data',$data);
			$this->session->set_flashdata('success', "<span class='alert alert-success'>successfuly saved</span>"); 
			$this->template->loadContent("case_listings/search.php",array());
		}
	else{
		$this->session->set_flashdata('error', "<span class='alert alert-danger'>already in saved list</span>"); 
		$this->template->loadContent("case_listings/search.php",array());
		}
	}

public function saveActs($id,$key){
	$userid=$this->user->info->ID;
	$query = $this->db->get_where('user_saved_data', array('caseid' => $id, 'userid'=>$userid));
	$getcase = $this->db->get_where('wp_posts', array('ID' => $id))->result();
	//print_r($getcase);
	$count = $query->num_rows();
	$data = array(
			'userid'=>$userid,
			'caseid'=>$id,
			'case_heading'=>$getcase[0]->post_title,
			'datakey'=>$key
		 );
	if($count===0){
			$res = $this->db->insert('user_saved_data',$data);
			$this->session->set_flashdata('success', "<span class='alert alert-success'>successfuly saved</span>"); 
			$this->template->loadContent("case_listings/search.php",array());
		}
	else{
		$this->session->set_flashdata('error', "<span class='alert alert-danger'>already in saved list</span>"); 
		$this->template->loadContent("case_listings/search.php",array());
		}
	}
	
}
?>
