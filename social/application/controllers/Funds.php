<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Funds extends CI_Controller 

{



	public function __construct() 

	{

		parent::__construct();

		$this->load->model("user_model");

		$this->load->model("funds_model");

		$this->load->model("home_model");

		$this->load->model("image_model");

		$this->load->model("feed_model");

		$this->load->model("blog_model");

		$this->load->model("page_model");

		$this->load->model("calendar_model");

		

		$this->load->model("login_model");

		$this->load->model("register_model");

		$this->load->model("article_model");

		if(!$this->user->loggedin) $this->template->error(lang("error_1"));

		

		$this->template->set_error_view("error/login_error.php");

		$this->template->set_layout("client/themes/titan.php");

	}



	public function index($type = 0, $hashtag = "") 

	{

		$this->template->loadData("activeLink", 

			array("funds" => array("general" => 1)));

		if(!$this->settings->info->payment_enabled) {

			$this->template->error(lang("error_60"));

		}

		

		$blog = $this->blog_model->get_user_blog($this->user->info->ID);

		$type = intval($type);

		$hashtag = $this->common->nohtml($hashtag);

		$pages = $this->page_model->get_recent_pages();

		$hashtags = $this->feed_model->get_trending_hashtags(10);

		$users = $this->user_model->get_newest_users($this->user->info->ID);

		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);

		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);

		$postid = intval($this->input->get("postid"));

		$commentid = intval($this->input->get("commentid"));

		$replyid = intval($this->input->get("replyid"));

		$location=$this->user->info->city;

		$location=explode(",",$location);

		$city=$location[0];

		$leads=$this->home_model->get_leads($city);

		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);

		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);

		$getarticlesforreview=$getarticlesforreview->result_array();



		if(!empty($this->settings->info->stripe_secret_key) && !empty($this->settings->info->stripe_publish_key)) {

			// Stripe

			require_once(APPPATH . 'third_party/stripe/init.php');



			$stripe = array(

			  "secret_key"      => $this->settings->info->stripe_secret_key,

			  "publishable_key" => $this->settings->info->stripe_publish_key

			);



			\Stripe\Stripe::setApiKey($stripe['secret_key']);

		} else {

			$stripe = null;

		}



		$this->template->loadContent("funds/index.php", array(

			"stripe" => $stripe,

			"blog" => $blog,

			"pages" => $pages,

			"users" => $users,

			"hashtags" => $hashtags,

			"type" => $type,

			"hashtag" => $hashtag,

			"postid" => $postid,

			"commentid" => $commentid,

			"replyid" => $replyid,

			"topviewd"=>$topviewd,

			"getprofile"=>$getprofile,

			"leads"=>$leads,

			"countmessage"=>$countmessage,

			"getarticlesforreview"=>$getarticlesforreview,

			)

		);

	}



	public function spend() 

	{

		$this->template->loadContent("funds/spend.php", array(

			)

		);

	}



	public function verified() 

	{

		if(!$this->settings->info->enable_verified_buy) {

			$this->template->error(lang("error_166"));

		}

		$this->template->loadContent("funds/verified.php", array(

			)

		);

	}





	public function bonus_deduction_log(){



		$this->template->loadContent("funds/deduction_log.php", array(

			)

		);





	}

	public function verified_pro() 

	{

		if(!$this->settings->info->enable_verified_buy) {

			$this->template->error(lang("error_166"));

		}



		if($this->user->info->verified) {

			$this->template->error(lang("error_167"));

		}



		$cost = $this->settings->info->verified_cost;



		if($this->user->info->points < $cost) {

			$this->template->error(lang("error_161") . $cost);

		}



		// Add

		$this->user_model->update_user($this->user->info->ID, array("verified" => 1,

			"points" => $this->user->info->points - $cost));



		$this->session->set_flashdata("globalmsg", lang("success_99"));

		redirect(site_url("funds/spend"));

	}



	public function submit_ad() 

	{

		$this->template->loadContent("funds/submit_ad.php", array(

			)

		);

	}



	public function submit_ad_pro() 

	{

		$name = $this->common->nohtml($this->input->post("name"));

		$advert = $this->lib_filter->go($this->input->post("advert"));

		$pageviews = intval($this->input->post("pageviews"));



		if(empty($name)) {

			$this->template->error(lang("error_168"));

		}



		if($pageviews < 1000) {

			$this->template->error(lang("error_160"));

		}



		// Cost

		$amount = floatval($pageviews/1000);

		$cost = $amount * $this->settings->info->credit_price_pageviews;



		if($this->user->info->points < $cost) {

			$this->template->error(lang("error_161") . $cost);

		}



		$this->home_model->add_rotation_ad(array(

			"name" => $name,

			"advert" => $advert,

			"pageviews" => $pageviews,

			"userid" => $this->user->info->ID,

			"timestamp" => time()

			)

		);



		$this->user_model->update_user($this->user->info->ID, array(

			"points" => $this->user->info->points - $cost

			)

		);



		// Alert admin

		if($this->settings->info->rotation_ad_alert_user > 0) {

			$user = $this->user_model->get_user_by_id($this->settings->info->rotation_ad_alert_user);

			if($user->num_rows() > 0) {

				$user = $user->row(); 



				$this->user_model->increment_field($user->ID, "noti_count", 1);

				$this->user_model->add_notification(array(

					"userid" => $user->ID,

					"url" => "admin/rotation_ads",

					"timestamp" => time(),

					"message" => lang("ctn_755") . " " . $this->user->info->username,

					"status" => 0,

					"fromid" => $this->user->info->ID,

					"username" => $user->username,

					"email" => $user->email,

					"email_notification" => $user->email_notification

					)

				);

			}

		}



		$this->session->set_flashdata("globalmsg", lang("success_100"));

		redirect(site_url("funds/submit_ad"));

	}



	public function payment_log() 

	{

		$this->template->loadContent("funds/payment_log.php", array(

			)

		);

	}

	

	public function deduction_log()

	{

	$this->template->loadContent("funds/deduction_log.php", array(

			)

		);

	

     }

	public function accountStatement()

	{
		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
	$this->template->loadContent("funds/account_statement.php", array(
'getprofile'=>$getprofile
			)

		);

	

     }

	public function subscriptionsView()

	{$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
	$this->template->loadContent("funds/subscriptions.php", array(
'getprofile'=>$getprofile
			)

		);

		// $this->template->loadContent("funds/subscriptions.php", array(

		// 	)

		// );

	

     }







	public function earnhistory(){

		

		

		$this->template->loadContent("funds/earn_points_history.php", array());

		

		}









	public function earn_points_history(){

		

		$this->load->library("datatables");



		$this->datatables->set_default_order("rewards_points_earn.points", "desc");



		// Set page ordering options that can be used

		

		$this->datatables->ordering(

			array(

				 0 => array(

				 	"rewards_points_earn.points" => 0

				 ),

				 1 => array(

				 	"rewards_points_earn.notes" => 0

				 ),

				 2 => array(

				 	"rewards_points_earn.created_at" => 0

				 )

			)

		);



			$this->datatables->set_total_rows(

				$this->user_model

					->get_total_earn_logs_count($this->user->info->ID)

			);

		

		$logs = $this->user_model->get_total_earn_logs($this->user->info->ID, $this->datatables);

		

		

		

		

	

		foreach($logs->result() as $r) {

			

			$this->datatables->data[] = array(

				//$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

				$r->points,

				$r->notes,

				$r->created_at,

				

				

			);

		}

		

	

		

		

		echo json_encode($this->datatables->process());

		

		

		}





	public function payment_deduction_page()

	{

		

		$this->load->library("datatables");



		$this->datatables->set_default_order("users.joined", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 1 => array(

				 	"legal_leads.name" => 0

				 ),

				 2 => array(

				 	"legal_leads.phone" => 0

				 )

				 ,

				 3 => array(

				 	"leadslogs.date" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->user_model

				->get_total_deduction_logs_count($this->user->info->ID)

		);

		$logs = $this->user_model->get_deduction_logs($this->user->info->ID, $this->datatables);

		

	

		

		foreach($logs->result() as $r) {

			

			

			

			$ded=explode("_",$r->issue);

			$deduction=$ded[2];

			

			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

				$r->name,

				$r->email,

				$r->phone,

				$r->date,

				$deduction." Point",

				

				

			);

		}

		echo json_encode($this->datatables->process());

	

     }

	private function paymentDeduction()

	{

		

		$this->load->library("datatables");



		$this->datatables->set_default_order("users.joined", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				 1 => array(

				 	"legal_leads.name" => 0

				 ),

				 2 => array(

				 	"legal_leads.phone" => 0

				 )

				 ,

				 3 => array(

				 	"leadslogs.date" => 0

				 )

			)

		);



		$this->datatables->set_total_rows(

			$this->user_model

				->get_total_deduction_logs_count($this->user->info->ID)

		);

		$logs = $this->user_model->get_deduction_logs_2($this->user->info->ID, $this->datatables);

		

		return $logs;

	

		

		// foreach($logs->result() as $r) {

			

			

			

		// 	$ded=explode("_",$r->issue);

		// 	$deduction=$ded[2];

			

		// 	$this->datatables->data[] = array(

		// 		$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

		// 		$r->name,

		// 		$r->email,

		// 		$r->phone,

		// 		$r->date,

		// 		$deduction." Point",

				

				

		// 	);

		// }

		// echo json_encode($this->datatables->process());

	

     }

	public function get_subscription_history($id)

	{



		$this->load->library("datatables");



		// $this->datatables->set_default_order("city_subscription_plan.created_at", "asc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				0 => array(

					"payment_plans.name" => 0

				),

				 1 => array(

				 	"city_subscription_plan.city_name" => 0

				 ),

				 2 => array(

				 	"city_subscription_plan.category_name" => 0

				 )

				 ,

				 3 => array(

				 	"city_subscription_plan.price" => 0

				 ),

				 4 => array(

				 	"city_subscription_plan.start_date" => 0

				 ),

				 5 => array(

				 	"city_subscription_plan.end_date" => 0

				 ),

				 6 => array(

				 	"city_subscription_plan.plan_renew" => 0

				 )

			)

		);

		

		$this->datatables->set_total_rows(

			$this->user_model

				->get_total_lawyer_plan_count($this->user->info->ID)

		);

		$subscription_history = $this->user_model->get_subscription_history($this->user->info->ID, $this->datatables);

		

		foreach($subscription_history->result() as $r) {

			

			

			

			// $ded=explode("_",$r->issue);

			// $deduction=$ded[2];

			

			$this->datatables->data[] = array(

			// 	$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

				

				// $deduction." Point",

				$r->name,

				$r->city_name,

				$r->category_name,

				$r->price,

				date("d-m-Y",$r->start_date),

				date("d-m-Y",$r->end_date),

				($r->plan_renew == 1 ? "Plan Renew" : "New Plan")		

			);

				

		}

		echo json_encode($this->datatables->process());

	

     }





	 public function payment_logs_page() 

	 {

		 

	

		 $this->load->library("datatables");

 

	

		$merger_data = $this->getAccountStatementData();

		$final_data = $this->filterAccountStatement($merger_data,$_GET);

		

		$this->datatables->set_total_rows(

			count($final_data)

		);

		

		$final_data = array_slice($final_data,$_GET['start'],$_GET['length']);



		 

		 foreach($final_data as $r) {

		

			$this->datatables->data[] = array(



				 $r['username'],

				 $r['tranction_type'],

				 $r['detail'],

				 $r['online_timestamp'],

				 $r['pay_method'],

				 round($r['point']),

				 

			);

 

			 

		 }

	

		 echo  json_encode($this->datatables->process());

		 

		 

	 }

	 public function getAccountStatementCSV(){



        $filename = 'account_statement_'.date('Ymd').'.csv'; 

		header("Content-Description: File Transfer"); 

		header("Content-Disposition: attachment; filename=$filename"); 

		header("Content-Type: application/csv; ");

		

		

		$file = fopen('php://output', 'w');

		

		$header_csv = array(

			"name",

			"Transaction Debit/Credit",

			"Detail",

			"Date",

			"Processor",

			"Points"

		);

		fputcsv($file, $header_csv);



		$this->load->library("datatables");

		$merger_data = $this->getAccountStatementData();

		$final_data = $this->filterAccountStatement($merger_data,$_GET);





		foreach ($final_data as $r){ 

			$tmp_array = array(

				$r['username'],

				$r['tranction_type'],

				$r['detail'],

				$r['online_timestamp'],

				$r['pay_method'],

				round($r['point'])

			);

            fputcsv($file, $tmp_array);

		}

		fclose($file); 

		exit; 

	 }



	 public function getAccountStatementPDF(){



		$this->load->library("datatables");

		$merger_data = $this->getAccountStatementData();

		$final_data = $this->filterAccountStatement($merger_data,$_GET);







		$html = "<h2 style='text-align:center;margin-top:40px;'>Lawyer IND (Account statement)</h2>";

		// $html .= "<h3 style='text-align:center;margin-top:40px;'>Account statement</h3>";



		$html .= "<table style='margin-left:auto;margin-right:auto;'>";

		$html .= "<tr><th>name</th><th>Transaction Debit/Credit</th><th>Detail</th><th>Date</th><th>Processor</th><th>Points</th></tr>";



		foreach ($final_data as $r){ 



			$html .= "<tr>";

			$html .= "<td style='text-align:center;'>".$r['username']."</td>";

			$html .= "<td style='text-align:center;'>".$r['tranction_type']."</td>";

			$html .= "<td style='text-align:center;'>".$r['detail']."</td>";

			$html .= "<td style='text-align:center;'>".$r['online_timestamp']."</td>";

			$html .= "<td style='text-align:center;'>".$r['pay_method']."</td>";

			$html .= "<td style='text-align:center;'>".round($r['point'])."</td>";

			$html .= "</tr>";

		}



		$html .= "</table>";

		

		// echo $html;

		// die();

		$mpdf = new \Mpdf\Mpdf();

		// $html = "<h2>Lawyer IND</h2>";

		$mpdf->WriteHTML($html);

		$mpdf->Output();

	 }



	 public function getAccountStatementData(){

		$log_data = [];

		 $deduction_data = [];

		 $leads_data = [];

		 $plan_data = []; 



		

		 $logs = $this->user_model->get_payment_logs($this->user->info->ID, $this->datatables);

			

		$deduction =  $this->paymentDeduction();

		// echo "<pre>";

		// print_r($deduction);

		// die;

		

		//get point reward

		$reward_point = $this->user_model->get_reward_point($this->user->info->ID);

		$reward_point = $reward_point->result();

		$user_id = $reward_point[0]->userid;

		

			$user_detail =  $this->user_model->get_user_by_id($user_id);

			$username = $user_detail->result();

			$username = $username[0]->first_name." ".$username[0]->last_name;

			if(!empty($reward_point)){

				foreach($reward_point as $points){

			

					$data['username'] = $username;

					$data['tranction_type'] = "Credit";

					$data['tranction_type_status'] = "1";

					// $data['online_timestamp'] = date("d/m/Y", strtotime(explode(" ",$points->created_at)[0])); 

					$data['online_timestamp'] =  strtotime($points->created_at); 

					$data['detail'] = $points->notes;

					$data['detail_type'] = "1";

					$data['pay_method'] = "N/A";

					$data['point'] = $points->points;

					

					array_push($log_data,$data);

				}	

			}

		

			



		if(!empty($logs->result())){

			foreach($logs->result() as $r){

				$data['username'] = $r->first_name." ".$r->last_name;

				$data['tranction_type'] = "Credit";

				$data['tranction_type_status'] = "1";

				// $data['online_timestamp'] =  date($this->settings->info->date_format, $r->online_timestamp);

				$data['online_timestamp'] =  $r->timestamp + (5.5*60*60);

				$data['detail'] = "Points added to wallet";

				$data['detail_type'] = "2";

				$data['pay_method'] = $r->processor;

				$data['point'] = number_format($r->amount, 2);

				

				array_push($log_data,$data);

				

			}

		}

		if(!empty($deduction)){

			if(!empty($deduction['leads'])){

				foreach($deduction['leads'] as $d){

					

					$data['username'] = $d->first_name." ".$d->last_name;

					$data['tranction_type'] = "Debit";

					$data['tranction_type_status'] = "2";

					// $data['online_timestamp'] =  date($this->settings->info->date_format, $d->online_timestamp); 

					// $data['online_timestamp'] =  $d->online_timestamp; 

					$data['online_timestamp'] =  strtotime($d->date) + (5.5*60*60); 

					$lead = explode("_",$d->issue);

					$data['detail'] = "View lead :- " .$lead[1];

					$data['detail_type'] = "3";

					$data['point'] = number_format($lead[2], 2);

					$data['pay_method'] = "N/A";

					

					array_push($leads_data,$data);

					

				 }

			}

			

			if(!empty($deduction['subscription'])){

			 	foreach($deduction['subscription'] as $s){

				 $data['username'] = $username;

				 $data['tranction_type'] = "Debit";

				 $data['tranction_type_status'] = "2";

				//  $data['online_timestamp'] = date("d/m/Y", strtotime(explode(" ",$s->created_at)[0]));; 

				 $data['online_timestamp'] = strtotime($s->created_at); 

				if($s->type == 1 ){

					$data['detail'] = "Buy :- " .$s->name ." subscription";	 

					$data['point'] = $s->points;

				}

				$data['detail_type'] = "4";

				$data['pay_method'] = "N/A";

				//  $ded = explode("_",$d->issue);

				//  $data['point'] = number_format($ded[2], 2);

				array_push($plan_data,$data);

				// print_r($s->ref_id);

			 }

			}

		}	

		

		

		$deduction_data = array_merge($leads_data,$plan_data);

	

		$merger_data = $this->glueArrays($log_data,$deduction_data);



		return $merger_data;

	 }



	 public function filterAccountStatement($merger_data,$filter_data){

		/* Filter Data Start */



		$merger_data_1 = [];

		

		if(!empty($_GET['detailType']) || !empty($_GET['transactionType']) || !empty($_GET['startDate']) || !empty($_GET['endDate'])) {

			foreach($merger_data as $res){

				$record_match = 0;



				if(!empty($_GET['startDate'])){

					$startDate = strtotime($_GET['startDate']);

					if($startDate <= $res['online_timestamp']){

						$record_match = 1;

					}else{

						$record_match = 0;

						continue;

					}

				}

				if(!empty($_GET['endDate'])){

					$endDate = strtotime($_GET['endDate'] . "23:59:59");

					if($endDate >= $res['online_timestamp']){

						$record_match = 1;

					}else{

						$record_match = 0;

						continue;

					}

				}



				if(!empty($_GET['detailType'])){

					if($_GET['detailType'] == $res['detail_type']){

						$record_match = 1;

					}else{

						$record_match = 0;

						continue;

					}

				}

				if(!empty($_GET['transactionType'])){

					if($_GET['transactionType'] == $res['tranction_type_status']){

						$record_match = 1;

					}else{

						$record_match = 0;

						continue;

					}

				}



				if($record_match == 1){

					$merger_data_1[] = $res;

				}

			}

		}else{

			$merger_data_1 = $merger_data;

		}

		

		$sortType = SORT_DESC;



		if(!empty($_GET['sortType']) && !empty($_GET['sortType'] == 1)){

			$sortType = SORT_ASC;

		}else{

			$sortType = SORT_DESC;

		}



		$date_array = array_column($merger_data_1,'online_timestamp');

		array_multisort($date_array,$sortType,$merger_data_1);

		

		$final_data = array_map(function($merger_data_1){

				$merger_data_1['online_timestamp'] = date('d/m/Y H:i:s',$merger_data_1['online_timestamp']);

				return $merger_data_1;			

		},$merger_data_1);





		

		

		return $final_data;





		// /* Filter Data End */

	 }

 

	 public static function glueArrays($log_data, $deduction_data) {

		 // merges TWO (2) arrays without adding indexing. 

		 $myArr = $log_data;

		 foreach ($deduction_data as $arrayItem) {

			 $myArr[] = $arrayItem;

		 }

		 return $myArr;

	 }



	public function plans() 

	{

		$this->template->loadData("activeLink", 

			array("funds" => array("plans" => 1)));

		if(!$this->settings->info->payment_enabled) {

			$this->template->error(lang("error_60"));

		}



		$plans = $this->funds_model->get_plans();

		$this->template->loadContent("funds/plans.php", array(

			"plans" => $plans

			)

		);

		

	}

	public function plan_buy($id,$type,$hashtag = "") 

	{

		

	



		$this->template->loadData("activeLink", 

			array("funds" => array("plans" => 1)));

		if(!$this->settings->info->payment_enabled) {

			$this->template->error(lang("error_60"));

		}

		

		$blog = $this->blog_model->get_user_blog($this->user->info->ID);

		$type = intval($type);

		$hashtag = $this->common->nohtml($hashtag);

		$pages = $this->page_model->get_recent_pages();

		$hashtags = $this->feed_model->get_trending_hashtags(10);

		$users = $this->user_model->get_newest_users($this->user->info->ID);

		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);

		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);

		$postid = intval($this->input->get("postid"));

		$commentid = intval($this->input->get("commentid"));

		$replyid = intval($this->input->get("replyid"));

		$location=$this->user->info->city;

		$location=explode(",",$location);

		$city=$location[0];

		$leads=$this->home_model->get_leads($city);

		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);

		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);

		$getarticlesforreview=$getarticlesforreview->result_array();



		$plan_detail = $this->funds_model->get_plan_price_detail($id,$type); 

		

		$all_city = $this->funds_model->get_all_city(); 

		

		$all_category = $this->user_model->get_all_catagories();

		$all_category = $all_category->result();

		

	

		

		$startdt = new DateTime('now');

		$start_format = $startdt->format('m/d/Y');

		$recently_expire_date = "";

		$min_date = "";



		$plans = $this->funds_model->get_plans();

		if($type == 0){

			$this->template->loadContent("funds/plan_buy.php", array(

			

				"blog" => $blog,

				"pages" => $pages,

				"users" => $users,

				"hashtags" => $hashtags,

				"type" => $type,

				"hashtag" => $hashtag,

				"postid" => $postid,

				"commentid" => $commentid,

				"replyid" => $replyid,

				"topviewd"=>$topviewd,

				"getprofile"=>$getprofile,

				"leads"=>$leads,

				"countmessage"=>$countmessage,

				"getarticlesforreview"=>$getarticlesforreview,

				"plans" => $plans,

				"plan_detail" => $plan_detail,

				"id"=> $id,

				"type" => $type,

				"recently_expire_date" =>$recently_expire_date,

				"min_date" =>$min_date,

				'category_date' => $all_category,

				'all_city' => $all_city

				)

			);

	

		}

		else if($type == 1){

			$this->template->loadContent("funds/searchplan_buy.php", array(

			

				"blog" => $blog,

				"pages" => $pages,

				"users" => $users,

				"hashtags" => $hashtags,

				"type" => $type,

				"hashtag" => $hashtag,

				"postid" => $postid,

				"commentid" => $commentid,

				"replyid" => $replyid,

				"topviewd"=>$topviewd,

				"getprofile"=>$getprofile,

				"leads"=>$leads,

				"countmessage"=>$countmessage,

				"getarticlesforreview"=>$getarticlesforreview,

				"plans" => $plans,

				"plan_detail" => $plan_detail,

				"id"=> $id,

				"type" => $type,

				"recently_expire_date" =>$recently_expire_date,

				"min_date" =>$min_date,

				'category_date' => $all_category,

				'all_city' => $all_city

				)

			);

	

		}



		// $this->template->loadContent("funds/plan_buy.php", array(

			

		// 	"blog" => $blog,

		// 	"pages" => $pages,

		// 	"users" => $users,

		// 	"hashtags" => $hashtags,

		// 	"type" => $type,

		// 	"hashtag" => $hashtag,

		// 	"postid" => $postid,

		// 	"commentid" => $commentid,

		// 	"replyid" => $replyid,

		// 	"topviewd"=>$topviewd,

		// 	"getprofile"=>$getprofile,

		// 	"leads"=>$leads,

		// 	"countmessage"=>$countmessage,

		// 	"getarticlesforreview"=>$getarticlesforreview,

		// 	"plans" => $plans,

		// 	"plan_detail" => $plan_detail,

		// 	"id"=> $id,

		// 	"type" => $type,

		// 	// "buy_all_city_plan"=>$all_city_plan,

		// 	"recently_expire_date" =>$recently_expire_date,

		// 	"min_date" =>$min_date

		// 	)

		// );







	

		

		// $this->template->loadContent("funds/plan_buy.php", array(

		// 	"plans" => $plans,

		// 	"plan_detail" => $plan_detail

		// 	)

		// );

		

	}



	public function buy_plan($id,$type, $hash) 

	{

		

	

		$plan_type = $type;

		$start_date = $_POST['start_date'];

		$end_date = $_POST['end_date'];

		$city_id  = $_POST['city_id'];

		$plan_renew  = $_POST['plan_renew'];

		

		if($type == 1){

			$category_id = $_POST['cat_id'];

		}

		

		if($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));

		}

		$id = intval($id);



		//only 10 plan can buy in same city



		$plan_purchased = $this->user_model->plan_purchased($city_id); 

		// if($plan_purchased->num_rows() > 0 ){

		// 	if(count($plan_purchased->result()) >= 1){

		// 		if($hash != $this->security->get_csrf_hash()) {

		// 			$this->template->error(lang("error_6"));

		// 		}

		// 	}

			

			

			

		// }

		$price = "";

		$city_name = "";

		$category_name = "";



		$plan = $this->funds_model->get_plan($id);

		

		

		if($plan_type==0){

			$plan_detail = $this->user_model->get_city_plan_detail($city_id); 



			if(!empty($plan_detail)){

				$price = $plan_detail[0]->price;

				$city = $plan_detail[0]->name;

			}

			

		}	

		else if($plan_type == 1){

			$plan_detail = $this->user_model->get_search_plan_detail($category_id,$city_id); 

		

			if(!empty($plan_detail)){

				$price = $plan_detail[0]->price;

				$city = $plan_detail[0]->city;

				$category_name = $plan_detail[0]->catagory_name;



			}

		}	

 



		

		if($plan->num_rows() == 0) $this->template->error(lang("error_61"));

		$plan = $plan->row();

		

		// if(!empty($plan_price)){

		// 	$price = $plan_price[0]->price;

		// 	$city_name = $plan_price[0]->name;

		// }

	

		// Check user has dolla

		// if($this->user->info->points < $plan->cost) {

		// 	$this->template->error(lang("error_62"));

		// }



		if($this->user->info->points < $price) {

			$this->template->error(lang("error_62"));

		}

	

		// if($this->user->info->premium_time == -1) {

		// 	$this->template->error(lang("error_63"));

		// }

		



		

		// if($plan->days > 0) {

		// 	$premium_time = $this->user->info->premium_time;

		// 	$time_added = (24*3600) * $plan->days;

			

		// 	// Check to see if user currently has time.

		// 	if($premium_time > time()) {

		// 		// If plan does not equal current one, then we reset 

		// 		// the timer 

		// 		$premiun_plan_id = $this->user_model->get_premium_plan_id($this->user->info->ID);

		// 		$premium_planid  = $premiun_plan_id[0]->premium_planid;

				

		// 		if($premium_planid != $plan->ID) {

		// 			$premium_time = time() + $time_added;

		// 		} else {

		// 			$premium_time = $premium_time + $time_added;

		// 		}

		// 	} else {

		// 		$premium_time = time() + $time_added;

		// 	}

		// } else {

		// 	// Unlimited Time modifier

		// 	$premium_time = -1;

		// }

			



		// $this->user->info->points = $this->user->info->points - $plan->cost;

		$this->user->info->points = $this->user->info->points - $price;

		

		// $this->user_model->update_user($this->user->info->ID, array(

		// 	"premium_time" => $premium_time,

		// 	"points" => $this->user->info->points,

		// 	"premium_planid" => $plan->ID

		// 	)

		// );

		$this->user_model->update_user($this->user->info->ID, array(

			"premium_time" => "",

			"points" => $this->user->info->points,

			"premium_planid" => $id

			)

		);

		// $this->user_model->update_point_deduction($this->user->info->ID, array(

		// 	// "premium_time" => $premium_time,

		// 	"user_id" => $this->user->info->ID,

		// 	"ref_id" => $plan->ID,

		// 	"points" => $plan->cost,

		// 	"type" => 1 

		// 	)

		// );

		$this->user_model->update_point_deduction($this->user->info->ID, array(

			// "premium_time" => $premium_time,

			"user_id" => $this->user->info->ID,

			"ref_id" => $id,

			"points" => $price,

			"type" => 1 

			)

		);



		$this->funds_model->update_plan($id, array(

			"sales" => $plan->sales + 1

			)

		);

		$plan_data = array(

			'user_id'  => $this->user->info->ID,

			'plan_id'  => $id,

			'city_id' => $city_id,

			'city_name'=>$city,

			'plan_type' => 0,

			'category_id'=>"N/A",

			'category_name'=>"N/A",

			'price'  => $price,

			'start_date' => strtotime($start_date),

			'end_date' => strtotime($end_date),

			'status' => 1,

			'plan_renew'=>$plan_renew

		);

		if($type == 1){

			$plan_data['category_id'] = $category_id;

			$plan_data['category_name'] = $category_name;

			$plan_data['plan_type'] = 1;

		}

		

		

		$this->user_model->update_plan($plan_data,$type);



		// $aaa  = [

		// 	"userid" => $this->user->info->ID,

		// 	"IP" => $_SERVER['REMOTE_ADDR'],

		// 	"user_agent" => $_SERVER['HTTP_USER_AGENT'],

		// 	"timestamp" => time(),

		// 	"message" => lang("ctn_442") . $plan->name

		// 	];

		// 	echo "<pre>";

		// 	print_r($aaa);



		// $this->user_model->add_log(array(

		// 	"userid" => $this->user->info->ID,

		// 	"IP" => $_SERVER['REMOTE_ADDR'],

		// 	"user_agent" => $_SERVER['HTTP_USER_AGENT'],

		// 	"timestamp" => time(),

		// 	"message" => lang("ctn_442") . $plan->name

		// 	)

		// );



		$this->session->set_flashdata("globalmsg", lang("success_28"));

		$response['status'] = true;

		echo json_encode($response);

		// redirect(site_url("funds/plans"));

		

	}







}

