<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Headnotes extends CI_Controller 
{
	
	public function __construct() 
	{
		parent::__construct();
		
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		$this->load->model("home_model");
		$this->load->model("login_model");
		$this->load->model("register_model");
		$this->load->model("headnotes_model");
	
		$this->load->library("form_validation");
		
		$this->load->model("article_model");
		
		$this->template->loadData("activeLink", 
			array("home" => array("general" => 1)));
		
		
		//if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		
		
		$this->template->set_layout("client/themes/titan.php");
		$this->template->set_error_view("error/login_error.php");
	}
	public function createheadnote($type = 0, $hashtag = "")
	{

		$this->load->library('Ckeditor');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		
		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location=$this->user->info->city;
		$location=explode(",",$location);
		$city=$location[0];
		$leads=$this->home_model->get_leads($city);
		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview=$getarticlesforreview->result_array();
		
		$categories = $this->user_model->get_case_catagories_data()->result();

	
		
		$this->template->loadContent("headnotes/index.php",array(
		
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd"=>$topviewd,
			"getprofile"=>$getprofile,
			"leads"=>$leads,
			"countmessage"=>$countmessage,
			"getarticlesforreview"=>$getarticlesforreview,
			"categories" => $categories
		));
	
	}
	public function index($type = 0, $hashtag = "")
	{

		$this->load->library('Ckeditor');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location=$this->user->info->city;
		$location=explode(",",$location);
		$city=$location[0];
		$leads=$this->home_model->get_leads($city);
		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview=$getarticlesforreview->result_array();
		
		$categories = $this->user_model->get_case_catagories_data()->result();
		
		$headnotes = $this->headnotes_model->getheadnotes($this->user->info->ID);
		$headnotes  = $headnotes->result();
		$this->template->loadContent("headnotes/headnotes_index.php",array(
		
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd"=>$topviewd,
			"getprofile"=>$getprofile,
			"leads"=>$leads,
			"countmessage"=>$countmessage,
			"getarticlesforreview"=>$getarticlesforreview,
			"categories" => $categories,
			"headnotes" => $headnotes
		));
	
	}
	public function researchIndex($type = 0, $hashtag = "")
	{
		
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		$this->load->library('Ckeditor');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$userid=$this->user->info->ID;
		$selectrearch=$this->user_model->get_reasearch_by_id($userid);
		
		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location=$this->user->info->city;
		$location=explode(",",$location);
		$city=$location[0];
		$leads=$this->home_model->get_leads($city);
		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview=$getarticlesforreview->result_array();
		
		//$this->load->view('sidebar/sidebar.php');
		//print_r($selectrearch);
		$result=$selectrearch->result();
		$this->template->loadContent("reasearch/index.php",array(
			"researchdata"=>$result,
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd"=>$topviewd,
			"getprofile"=>$getprofile,
			"leads"=>$leads,
			"countmessage"=>$countmessage,
			"getarticlesforreview"=>$getarticlesforreview,
			"getarticlesforreview"=>$getarticlesforreview,
		));
	}

	//edit haednotes 
	public function edit_headnotes($id,$type){
		$get_headnotes_data  =  $this->headnotes_model->get_single_headnote($this->user->info->ID,$id);
		$get_headnotes_data = $get_headnotes_data->result();

		// redirect(base_url().'headnotes/createheadnote',$get_headnotes_data);

		$this->template->loadContent("headnotes/index.php",array(
			"type" => $type, 
			"get_headnotes_data" => $get_headnotes_data,
		));
	
	}
	
	
	//deleet headnotes
	public function delete_headnotes($id){
		$delete_headnotes = $this->headnotes_model->delete_headnotes($this->user->info->ID,$id);
		$this->session->set_flashdata('success', "<span class='alert alert-success'>Deleted Success</span>"); 
		if($delete_headnotes){
			redirect(base_url().'headnotes');
		}
	}



	public function passtitleheading(){
		if($this->user->loggedin){
			$select_judgement = array(
				'product_id' => $_POST['product_id'],
				'heading'   => trim($_POST['heading'])	
			);

			if(!empty($select_judgement)){
				$this->template->loadContent("headnotes/index.php",array(
					"select_judgement" => $select_judgement,
				));
			}
				
			
		
		}
		
		
	}


	public function deleteresearch($id)
	{
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$del=$this->user_model->del_research_by_id($id);
		$this->session->set_flashdata('success', "<span class='alert alert-success'>Deleted Success</span>"); 
		redirect(site_url('case_listings/researchIndex'));
	}
	public function create(){
		
		if(!$this->user->loggedin) 
			$this->template->error(lang("error_1"));
	   		$this->load->library('Ckeditor');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$cats  = $this->user_model->get_catagories_data()->result();
		$year  = $this->user_model->get_high_court_unique_years()->result();
		$court= $this->user_model->get_high_court_unique_courts()->result();
		$maxims = $this->user_model->get_maxims()->result();
		$newmaxims = $this->user_model->get_maxims_new()->result();
		
		// echo "<pre>";
		// print_r($cats);
		// die();
		$data['cats'] = $cats;
		$data['year'] = $year;
		$data['courts'] = $court;
		$data['maxims'] = $maxims;
		$data['newmaxims'] = $newmaxims;
		$this->template->loadContent("case_listings/create.php",$data);
	}

	public function my_case_referance_iframe(){

		$cats  = $this->user_model->get_catagories_data()->result();
		$year  = $this->user_model->get_high_court_unique_years()->result();
		$court= $this->user_model->get_high_court_unique_courts()->result();
		$maxims = $this->user_model->get_maxims()->result();
		$newmaxims = $this->user_model->get_maxims_new()->result();
		
		$data['cats'] = $cats;
		$data['year'] = $year;
		$data['courts'] = $court;
		$data['maxims'] = $maxims;
		$data['newmaxims'] = $newmaxims;
		$this->template->loadContent("headnotes/my_case_referance_iframe.php",$data);
	}
	public function getPerformas(){
		$search = $this->input->get("search");
		$category = $this->input->get("category");
		$performas = $this->user_model->getActivePerformas($search,$category);
		$output = "";
		if(!empty($performas)){
			foreach($performas as $performa){
				$output.='<li><a href="javascript:void(0);" id="performa_'.$performa->ID.'" onclick="select_performa('.$performa->category_id.');">'.$performa->title.'</a></li>';
			}
			echo $output;
		}else{
			$output.='<p>No Result found....</p>';
			echo $output;
		}
	}
	public function getCategoryName(){
		$category_id = $this->input->get('category_id');
		$categoryInfo = $this->user_model->getCategoryInfo($category_id);
		echo $categoryInfo['c_name'];
	}
		
	public function createreasearch()
	{
		
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		$this->load->library('Ckeditor');
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$this->template->loadContent("reasearch/create.php");
	}
    public function delete($id){
		
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		$this->db->where('id', $id);
        $res=$this->db->delete('case_editor'); 
        if($res==true){
		$this->session->set_flashdata('success', "<span class='alert alert-success'>Deleted Success</span>"); 
		 redirect(site_url('case_listings'));
	    }
	    else{
			$this->session->set_flashdata('error', "<span class='alert alert-danger'>Delete not Success</span>");
			redirect(site_url('case_listings'));
			}
	}
	
   public function ajaxpro(){
	   
	   
		 $query = $this->input->get('query');
		 $year = $this->input->get('yearfilter');
		 $court = $this->input->get('court');
		 $this->db->like(array('heading' => $query, 'year' => $year,'statecode'=>$court));
		//$this->db->like('statecode', $query);
         $users = $this->db->get("high_court_ap_yearly")->result();
   		 $data = array();
		 foreach($users as $hsl){
		//	$slug = $hsl->ID;
		//	$data[]=$hsl->email;
					$s = new STDClass;
					$s->label = $hsl->heading;
					$s->type = "page";
					$s->value = $hsl->heading;
					//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;
					$s->url = site_url("case_listings/judgment/" . $hsl->ID);
					$data[] = $s;
			}
        echo json_encode($data);
        exit();
	   }
	 
	 
	 
	 
	   public function maxims(){
		  $query = $this->input->get('query');
		  $this->db->like('term', $query);
		//$this->db->like('statecode', $query);
          $users = $this->db->get("maxims")->result();
		  $data = array();
		  foreach($users as $hsl){
			//$slug = $hsl->ID;
			//$data[]=$hsl->email;
					$s = new STDClass;
					$s->label = $hsl->term;
					$s->type = "page";
					$s->value = $hsl->term;
					$s->definitin = $hsl->definition;
					//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;
					$s->url = site_url("case_listings/maximslist/" . $hsl->ID);
					$data[] = $s;
			}
        echo json_encode($data);
        exit();
		   }
		   
		public function viewmaximslaravel(){
		  $query = $this->input->get('query');
		  $this->db->like('term', $query);
		//$this->db->like('statecode', $query);
          $users = $this->db->get("maxims")->result();
		  $data = array();
		  foreach($users as $hsl){
			//$slug = $hsl->ID;
			//$data[]=$hsl->email;
					$s = new STDClass;
					$s->label = $hsl->term;
					$s->type = "page";
					$s->value = $hsl->term;
					//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;

					$maxims_tmp_url = str_replace("/social","",base_url());
					$maxims_url = $maxims_tmp_url."view_maxims/";
					
					$s->url = $maxims_url.$hsl->ID;
					$data[] = $s;
			}
        echo json_encode($data);
        exit();
		   }
	   public function bareacts(){
		   $query = $this->input->get('query');
		   $this->db->like('title', $query);
		   //$this->db->like('statecode', $query);
		   $users = $this->db->get("acts")->result();
		   //$this->db->like('statecode', $query);
		 
    	   $data = array();
		   foreach($users as $hsl){
				//$slug = $hsl->ID;
				//$data[]=$hsl->email;
				$s = new STDClass;
				$s->label = $hsl->post_title;
				$s->type = "page";
				$s->value = $hsl->post_title;
				//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;
				$s->url = site_url("case_listings/bareactslist/" . $hsl->ID);
				$data[] = $s;
			}
        echo json_encode($data);
        exit();
		   }
	   
	   
	   public function view_bare_acts(){
		   
		   
		   $query = $this->input->get('query');
		   $this->db->like('title', $query);
		   //$this->db->like('statecode', $query);
		   $users = $this->db->get("acts")->result();
    	   $data = array();
		   foreach($users as $hsl){
				//$slug = $hsl->ID;
				//$data[]=$hsl->email;
				$s = new STDClass;
				$s->label = $hsl->title;
				$s->type = "page";
				$s->value = $hsl->description;
				//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;
				$bareacts_tmp_url = str_replace("/social","",base_url());
				$bareacts_url = $bareacts_tmp_url."bareacts/";

				$s->url = $bareacts_url. $hsl->id;
				$data[] = $s;
			}
        echo json_encode($data);
        exit();
		   }
		   
		   
	public function view_judgements_laravel(){
	   
	   
		  $query = $this->input->get('query');
		
		 $this->db->like(array('heading' => $query));
		//$this->db->like('statecode', $query);
         $users = $this->db->get("high_court_ap_yearly")->result();
   		 $data = array();
		 foreach($users as $hsl){
		
				$s = new STDClass;
				$s->label = $hsl->heading;
				$s->type = "page";
				$s->value = $hsl->heading;
				//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;
				
				$viewjudgements_tmp_url = str_replace("/social","",base_url());
				$viewjudgements_url = $viewjudgements_tmp_url."bareacts/";

				$s->url = $viewjudgements_url.$hsl->ID;
				$data[] = $s;
			}
    	echo json_encode($data);
        exit();
	}
		   public function view_landmark_judgements_laravel(){
	   
	   
		  $query = $this->input->get('query');
		
		 $this->db->like(array('heading' => $query));
		//$this->db->like('statecode', $query);
         $users = $this->db
					   ->where("status",1)
					   ->get("high_court_ap_yearly")->result();
   		 $data = array();
		 foreach($users as $hsl){
		//	$slug = $hsl->ID;
		//	$data[]=$hsl->email;
					$s = new STDClass;
					$s->label = $hsl->heading;
					$s->type = "page";
					$s->value = $hsl->heading;
					//$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" .  $hsl->profile_avatar;
					$landmarkjudgements_tmp_url = str_replace("/social","",base_url());
					$landmarkjudgements_url = $landmarkjudgements_tmp_url."bareacts/";

					$s->url = $landmarkjudgements_url.$hsl->ID;

					$data[] = $s;
			}
        echo json_encode($data);
        exit();
	   }
		   
		   
		   
		   
		   
		   
	 public function maximslist($id){
		  if(!$this->user->loggedin) {
			redirect(site_url("login"));
			}
		  $maxims=$this->db->where("ID", $id)->get("maxims")->result();
		  $this->template->loadContent("case_listings/maxims_view.php",array(
				"maxims" =>  $maxims
			));
		 }
	  public function bareactslist($id){
		    if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		  $bareacts=$this->db->where("ID", $id)->get("wp_posts")->result();
		  $this->template->loadContent("case_listings/bareacts_view.php",array(
			"bareacts" =>  $bareacts
			));
		  }
	   public function judgment($id,$key='judgment'){
		 
		 
		   if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		if($key=='judgment'){
		$userid=$this->user->info->ID;
		$judgment=$this->db->where("ID", $id)->get("high_court_ap_yearly")->result();
		$jid=$judgment[0]->ID;
		$query = $this->db->get_where('view_judgments_by_user', array(
            'judgmentid' => $jid,
            'userid' =>$userid,
        ));
		$count = $query->num_rows();
		if($count===0){
		$data = array(
			'userid'=>$userid,
			'judgmentid'=>$jid,
		 );
			$res = $this->db->insert('view_judgments_by_user',$data);
		}
	    $this->template->loadContent("case_listings/judgments_view.php",array(
			"judgment" => $judgment
			));
		  }
		  else if($key=='bareact'){
		$userid=$this->user->info->ID;
		$judgment=$this->db->where("ID", $id)->get("wp_posts")->result();
		$jid=$judgment[0]->ID;
		$query = $this->db->get_where('view_judgments_by_user', array(
            'judgmentid' => $jid,
            'userid' =>$userid,
        ));
		$count = $query->num_rows();
			if($count===0){
			$data = array(
			'userid'=>$userid,
			'judgmentid'=>$jid,
		 );
		$res = $this->db->insert('view_judgments_by_user',$data);
		}
		$this->template->loadContent("case_listings/bareacts_view.php",array(
				"bareacts" => $judgment,
			));
		}
		}
	  public function ajaxpro1(){
		$query = $this->input->get('query');
        $this->db->like('email', $query);
        $users = $this->db->get("users")->result();
		$data = array();
		foreach($users as $hsl){
			$data[]=$hsl->email;
			}
        echo json_encode($data);
	   }
	 public function casebuild(){
		 
		 if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		$this->load->library('Ckeditor');
		$this->template->loadContent("case_listings/casebuild.php",array());
		 }
	public function researchbuild()
	{
		
		if(!$this->user->loggedin) $this->template->error(lang("error_1"));
		$this->load->library('Ckeditor');
		$s = $this->user_model->get_catagories();
		$result=$s->result();
		$this->template->loadContent("reasearch/casebuild.php",array(
			"cat"=>$result,
		));
	}
	public function subcatg(){
		 $id=$_GET['categoryId'];
		 $subcat=$this->user_model->get_sub_catag($id);
		 $resultsub=$subcat->result();
		 foreach($resultsub as $subcat)
		 {
			 echo "<option value='".$subcat->cid."'>$subcat->c_name</option>";
		 }
	}
    public function searchResearch()
    {
		$userid=$this->user->info->ID;
		$saveddata = $this->db->get_where('user_saved_data', array('userid' => $userid))->result();
		$uniqueyears=$this->user_model->get_high_court_unique_years();
		$uniquecourts=$this->user_model->get_high_court_unique_courts();
		$s = $this->user_model->get_catagories();
		$result=$s->result();
		$year=$uniqueyears->result();
		$uniquecourts=$uniquecourts->result();
		$this->template->loadContent("reasearch/search.php",array(
			"saveddata"=>$saveddata,
			"year"=>$year,
			"courts"=>$uniquecourts,
			"cats"=>$result,
			));
		}
	public function searchcase(){
		$userid=$this->user->info->ID;
		$saveddata = $this->db->get_where('user_saved_data', array('userid' => $userid))->result();
		$uniqueyears=$this->user_model->get_high_court_unique_years();
		$uniquecourts=$this->user_model->get_high_court_unique_courts();
		$s = $this->user_model->get_catagories();
		$result=$s->result();
		$year=$uniqueyears->result();
		$uniquecourts=$uniquecourts->result();
		$this->template->loadContent("case_listings/search.php",array(
			"saveddata"=>$saveddata,
			"year"=>$year,
			"courts"=>$uniquecourts,
			"cats"=>$result,
			));
		}
		public function mycases($id){
			$category_id = $id;
			$cases = $this->user_model->getCasesByCategory($category_id);
			$category_info = $this->user_model->getCategoryInfo($category_id);


			$type = 0;
			$hashtag = 0;
			$hashtag = $this->common->nohtml($hashtag);
			$pages = $this->page_model->get_recent_pages();
			$hashtags = $this->feed_model->get_trending_hashtags(10);
			$users = $this->user_model->get_newest_users($this->user->info->ID);
			$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
			$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
			$postid = intval($this->input->get("postid"));
			$commentid = intval($this->input->get("commentid"));
			$replyid = intval($this->input->get("replyid"));
			$location=$this->user->info->city;
			$location=explode(",",$location);
			$city=$location[0];
			$leads=$this->home_model->get_leads($city);
			$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);
			$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);
			$getarticlesforreview=$getarticlesforreview->result_array();
			
			$categories = $this->user_model->get_catagories_data()->result();


			$data["cases"] = $cases;
			$data["category_info"] = $category_info;

			$data["getprofile"] = $getprofile;
			$data["type"] = $type;
			$data["countmessage"] = $countmessage;
			$data["hashtag"] = $hashtag;
			$data["getprofile"] = $getprofile;
			
			$this->template->loadContent("case_listings/my_case_listing.php",$data);
		}
}
?>
