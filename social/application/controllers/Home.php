<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{


	public function __construct()

	{

		parent::__construct();





		$this->template->loadData(
			"activeLink",

			array("home" => array("general" => 1))
		);

		$this->load->model("user_model");

		$this->load->model("home_model");

		$this->load->model("page_model");

		$this->load->model("feed_model");

		$this->load->model("funds_model");

		$this->load->model("article_model");

		// $query = $this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");

		// 

		if (!$this->user->loggedin) {





			redirect(site_url("login"));
		}

		$this->template->set_error_view("error/login_error.php");

		$this->template->set_layout("client/themes/titan.php");
	}

	public function index($type = 0, $hashtag = "")

	{



		$type = intval($type);

		$hashtag = $this->common->nohtml($hashtag);

		$pages = $this->page_model->get_recent_pages();

		$hashtags = $this->feed_model->get_trending_hashtags(10);

		$users = $this->user_model->get_newest_users($this->user->info->ID);

		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);

		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);

		$postid = intval($this->input->get("postid"));

		$commentid = intval($this->input->get("commentid"));

		$replyid = intval($this->input->get("replyid"));

		$location = $this->user->info->city;

		$location = explode(",", $location);

		$city = $location[0];

		$leads = $this->home_model->get_leads($city);





		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);

		$getarticlesforreview = $getarticlesforreview->result_array();







		$this->template->loadContent(
			"home/index.php",
			array(

				"pages" => $pages,

				"users" => $users,

				"hashtags" => $hashtags,

				"type" => $type,

				"hashtag" => $hashtag,

				"postid" => $postid,

				"commentid" => $commentid,

				"replyid" => $replyid,

				"topviewd" => $topviewd,

				"getprofile" => $getprofile,

				"leads" => $leads,

				"countmessage" => $countmessage,

				"getarticlesforreview" => $getarticlesforreview,



			)

		);
	}









	public function enterlocation()
	{

		echo $_REQUEST['locationselect'];
	}

	public function comppleteprofile()
	{

		$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
		$user_detail = $user_detail->row_array();

		if ($user_detail['complete_profile'] == 1) {
			return redirect(site_url());
		}
		if ($user_detail['profile_identification'] == 0) {
			$languages = $this->get_languages();
			$courts = $this->get_courts();
			$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();
			$getprofile = $getprofile->row_array();
			$user_info = $this->user_model->get_user_info($this->user->info->ID);
			$catagories = $this->user_model->get_catagories()->result_array();
			if (empty($getprofile)) {
				$verification_level = 0;
				$getprofile = [];
			} else {
				$verification_level = $getprofile['verification_level'];
			}
			$array =[];
			$consult_data = $this->home_model->get_consultation_timing($this->user->info->ID);
			foreach ($consult_data as  $cons) {
				$array[$cons['day_name']] = $cons;
			}

			//print_r($getprofile);die;
			$this->template->loadContent("home/completeprofile.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'core_profile' => $getprofile, 'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '1','consult_data' => $array));
		} elseif ($user_detail['profile_identification'] == 1) {
			$languages = $this->get_languages();
			$courts = $this->get_courts();
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();

			$catagories = $this->user_model->get_catagories()->result_array();
			$verification_level = 0;
			$this->template->loadContent("home/completeprofileseeker.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '1'));
		} elseif ($user_detail['profile_identification'] == 2) {
			$languages = $this->get_languages();
			$courts = $this->get_courts();
			$getprofile = $this->home_model->get_coreprofile_student($this->user->info->ID);
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();
			$getprofile = $getprofile->row_array();
			$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
			$user_detail = $user_detail->row_array();
			$catagories = $this->user_model->get_catagories()->result_array();
			if (empty($getprofile)) {
				$verification_level = 0;
			} else {
				$verification_level = $getprofile['verification_level'];
			}
			$this->template->loadContent("home/completeprofilestudent.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'core_profile' => $getprofile, 'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '1'));
		} elseif ($user_detail['profile_identification'] == 3) {
			$languages = $this->get_languages();
			$courts = $this->get_courts();
			$getprofile = $this->home_model->get_coreprofile_professor($this->user->info->ID);
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();
			$getprofile = $getprofile->row_array();
			$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
			$user_detail = $user_detail->row_array();
			$catagories = $this->user_model->get_catagories()->result_array();
			if (empty($getprofile)) {
				$verification_level = 0;
			} else {
				$verification_level = $getprofile['verification_level'];
			}

			$this->template->loadContent("home/completeprofileprofessor.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'core_profile' => $getprofile, 'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '1'));
		}
	}



	public function get_user_friends()

	{

		$query2 = $this->common->nohtml($this->input->get("term"));



		$char = substr($query2, 0, 1);

		if ($char == "@") {

			$query2 = substr($query2, 1, strlen($query2));

			$users = $this->user_model->get_names($query2);



			$usersArr = array();

			foreach ($users->result() as $user) {

				$u = new STDClass;

				$u->uid = $user->username;

				$u->value = $user->first_name . " " . $user->last_name;

				$usersArr[] = $u;
			}

			header('Content-Type: application/json');

			echo json_encode($usersArr);

			exit();
		} elseif ($char == "#") {
		}
	}

	public function appointment($hostid)
	{



		$userid = $this->user->info->ID;

		$getprofile = $this->home_model->get_coreprofile($hostid)->result();

		$gettimeschedule = "";

		$getseekerdetail = $this->user_model->get_user_by_id($userid)->result();

		$getProviderDetail = $this->user_model->get_user_by_id($hostid)->result();

		$catagories = $this->user_model->get_catagories()->result();

		if ($userid != $hostid) {

			$this->template->loadContent("home/bookappointment.php", array(

				"coreprofile" => $getprofile,

				"timeschedule" => $gettimeschedule,

				"userdetail" => $getseekerdetail,

				"providerprofile" => $getProviderDetail,

				"catss" => $catagories,

			));
		}
	}





	public function bookappointment()
	{



		$seekerid = $this->input->post('seekerid');

		$seekername = $this->input->post('seekername');

		$seekeremail = $this->input->post('seekeremail');

		$seekerphone = $this->input->post('seekerphone');

		$providername = $this->input->post('providername');

		$providerid = $this->input->post('providerid');

		$provideremail = $this->input->post('provideremail');

		$providerphone = $this->input->post('providerphone');

		$dateofappointment = $this->input->post('dateofappointment');

		$catagory = $this->input->post('catagory');

		$subjectmatter = $this->input->post('subjectmatter');

		$time = $this->input->post('minute');

		$timeformat = $this->input->post('timeformat');

		$timeofappoint =  $time . " " . $timeformat;

		$data = array(

			"service_provider_id" => $providerid,

			"service_seeker_id" => $seekerid,

			"seeker_name" => $seekername,

			"seeker_contact" => $seekerphone,

			"dateofappointment" => $dateofappointment,

			"timeofappoint" => $timeofappoint,

			"cat_id" => $catagory,

			"matter" => $subjectmatter,

		);

		$insert = $this->home_model->appointmentbooking($data);

		if ($insert) {

			$this->session->set_flashdata("globalmsg", "Your appointment details has been sent to $providername, You will be notified once he approve your appointment...");
		} else {

			$this->session->set_flashdata("globalmsg", "Appointment with same lawyer already booked on date" . $dateofappointment . "at" . $timeofappoint);
		}



		return redirect(site_url());
	}







	public function uploadproof()

	{

		$this->load->library("upload");

		$userid = $this->user->info->ID;

		if ($_FILES['userfile']['size'] > 0) {

			if (!$this->settings->info->resize_avatar) {

				$settings = array(

					"upload_path" => "uploads",

					"overwrite" => FALSE,

					"max_filename" => 300,

					"encrypt_name" => TRUE,

					"remove_spaces" => TRUE,

					"allowed_types" => 'pdf',

					"max_size" => $this->settings->info->file_size,

				);

				if ($this->settings->info->avatar_width > 0) {

					$settings['max_width'] = $this->settings->info->avatar_width;
				}

				if ($this->settings->info->avatar_height > 0) {

					$settings['max_height'] = $this->settings->info->avatar_height;
				}

				$this->upload->initialize($settings);



				if (!$this->upload->do_upload()) {

					$this->template->error(lang("error_21")

						. $this->upload->display_errors());
				}



				$data = $this->upload->data();



				$image = $data['file_name'];
			} else {

				$this->upload->initialize(array(

					"upload_path" => "uploads",

					"overwrite" => FALSE,

					"max_filename" => 300,

					"encrypt_name" => TRUE,

					"remove_spaces" => TRUE,

					"allowed_types" => "pdf",

					"max_size" => $this->settings->info->file_size,

				));

				if (!$this->upload->do_upload()) {

					$this->template->error(lang("error_21")

						. $this->upload->display_errors());
				}

				$data = $this->upload->data();

				$image = $data['file_name'];

				$config['image_library'] = 'gd2';

				$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;

				$config['create_thumb'] = FALSE;

				$config['maintain_ratio'] = FALSE;

				$config['width']         = $this->settings->info->avatar_width;

				$config['height']       = $this->settings->info->avatar_height;

				//$this->load->library('image_lib', $config);

				$this->load->library('image_lib');

				// Set your config up

				$this->image_lib->initialize($config);

				// Do your manipulation

				$this->image_lib->clear();

				if (!$this->image_lib->resize()) {

					$this->template->error(lang("error_21") .

						$this->image_lib->display_errors());
				}
			}
		} else {

			$image = $this->user->info->avatar;
		}

		$dis = $this->common->nohtml($this->input->post("disclain"));

		$data = array(

			"userid" => $userid,

			"proof" => $image,

			"disclaimer" => $dis,

		);

		$q = $this->home_model->upload_proof_for_upgrade($data);

		if ($q) {

			$this->session->set_flashdata("globalmsg", "Document Has been uploaded successfully. You will be notified soon once administrator approve your profile for upgradation.");
		} else {

			$this->session->set_flashdata("globalmsg", "Your Document has already been send to administrator for approval. You Will be notified when admin approve your document.");
		}

		return redirect(site_url('upgrade/advocate'));
	}



	public function studentform()

	{

		$userid = $this->user->info->ID;

		$institute = $this->input->post("inst");

		$degree = $this->input->post("degree");

		$university = $this->input->post("university");

		$grade = $this->input->post("grade");

		$yearstart = $this->input->post("yearstart");

		$yearend = $this->input->post("yearend");

		$data = array(

			"institute" => $institute,

			"degree" => $degree,

			"university" => $university,

			"grade" => $grade,

			"yearstart" => $yearstart,

			"yearend" => $yearend,

		);

		$orginst = $this->input->post("orginst");

		$yrtt = $this->input->post("yrtt");

		$from = $this->input->post("from");

		$to = $this->input->post("to");

		$interndata = array(

			"orgnaisation" => $orginst,

			"year" => $yrtt,

			"from" => $from,

			"to" => $to,

		);

		$educationhistory =  json_encode($data);

		$internhistory =  json_encode($interndata);

		$data3 = array(

			"USERID" => $userid,

			"education_history" => $educationhistory,

			"previous_internship" => $internhistory,



		);

		$insertprofile = $this->home_model->insertstudentprofile($data3);

		$this->session->set_flashdata("globalmsg", "Your profile has been updated");

		return redirect(site_url());
	}

	// public function completeformprofile()
	// {

	// 	$userid = $this->user->info->ID;

	// 	$category_id = $this->input->post("category_id");

	// 	$language = $this->input->post("language");

	// 	$language = json_encode($language);

	// 	$registration_no = $this->input->post("registration_no");

	// 	$councel_registration_no = $this->input->post("councel_registration_no");

	// 	$year_reg = $this->input->post("year_reg");

	// 	$practice_city = $this->input->post("practice_city");

	// 	$field_court = $this->input->post("field_court");

	// 	$courtnamearray = json_encode($field_court);

	// 	$field_court_number = $this->input->post("field_court_number");

	// 	// $field_court_number_array=json_encode($field_court_number);

	// 	$field_court_number_array = "";

	// 	$degree = $this->input->post("degree");

	// 	$degree_array = json_encode($degree);

	// 	$yearstart = $this->input->post("yearstart");

	// 	$yearstart_array = json_encode($yearstart);

	// 	$yearend = $this->input->post("yearend");

	// 	$yearend_array = json_encode($yearend);

	// 	$college = $this->input->post("college");

	// 	$college_array = json_encode($college);

	// 	$university = $this->input->post("university");

	// 	$university_array = json_encode($university);

	// 	$division = $this->input->post("division");

	// 	$division_array = json_encode($division);

	// 	$hour_charges = $this->input->post("hour_charges");

	// 	$telephone_charges = $this->input->post("telephone_charges");

	// 	$email_charges = $this->input->post("email_charges");

	// 	$apperance_charges = $this->input->post("apperance_charges");

	// 	$haur_draft_charges = $this->input->post("haur_draft_charges");

	// 	$category_id_array = "";

	// 	$data = array(

	// 		'userid' => $userid,

	// 		'category_id_array' => $category_id_array,

	// 		'language' => $language,

	// 		'registration_no' => $registration_no,

	// 		'councel_registration_no' => $councel_registration_no,

	// 		'year_reg' => $year_reg,

	// 		'practice_city' => $practice_city,

	// 		'courtnamearray' => $courtnamearray,

	// 		'field_court_number_array' => $field_court_number_array,

	// 		'degree_array' => $degree_array,

	// 		'yearstart_array' => $yearstart_array,

	// 		'yearend_array' => $yearend_array,

	// 		'college_array' => $college_array,

	// 		'university_array' => $university_array,

	// 		'division_array' => $division_array,

	// 		'hour_charges' => $hour_charges,

	// 		'telephone_charges' => $telephone_charges,

	// 		'email_charges' => $email_charges,

	// 		'apperance_charges' => $apperance_charges,

	// 		'haur_draft_charges' => $haur_draft_charges,

	// 	);







	// 	$location_country = $this->common->nohtml($this->input->post("location_country"));

	// 	$location_state = $this->common->nohtml($this->input->post("location_state"));

	// 	$location_city = $this->common->nohtml($this->input->post("location_city"));

	// 	$permannentadr = $this->common->nohtml($this->input->post("permannentadr"));

	// 	$location_address = $this->common->nohtml($this->input->post("location_address"));

	// 	$zipcode = $this->common->nohtml($this->input->post("zipcode"));



	// 	$address_data = array(

	// 		'address_1' => $location_address,

	// 		'permanentAddr' => $permannentadr,

	// 		'city' => $location_city,

	// 		'state' => $location_state,

	// 		'country' => $location_country,

	// 		'zipcode' => $zipcode,

	// 	);



	// 	$points = $this->user->info->points;

	// 	$bonus_points = $this->user->info->bonus_points;

	// 	$total_points = $this->user->info->total_points;

	// 	$cal_points = $points + 100;

	// 	$cal_bonus_points = $bonus_points + 100;

	// 	$cal_total_points = $total_points + 100;

	// 	$coreprofile = $this->home_model->insertcoreprofile($data);

	// 	$updatepoints = $this->home_model->updatepoints($userid, array(

	// 		"points" => $cal_points,

	// 		"bonus_points" => $cal_bonus_points,

	// 		"total_points" => $cal_total_points,

	// 	));



	// 	$this->page_model->add_history(
	// 		array(

	// 			"userid" => $userid,

	// 			"notes" => "Profile Completion",

	// 			"points" => 100

	// 		)

	// 	);



	// 	//update user address in social db table(user)



	// 	$update_address = $this->user_model->update_user($this->user->info->ID, $address_data);



	// 	//update user address in lawyer db table(user)



	// 	$ch = curl_init();



	// 	$data2 = 'id=' . $this->user->info->ID . '&zipcode=' . $zipcode . '&country=' . $location_country . '&state=' . $location_state . '&city=' . $location_city . '&address=' . $location_address;



	// 	$profile_tmp_url = str_replace("/social", "", base_url());

	// 	$profile_url = $profile_tmp_url . "api/update_laravel_profile_address.php";





	// 	curl_setopt($ch, CURLOPT_URL, $profile_url);

	// 	curl_setopt($ch, CURLOPT_HEADER, 0);

	// 	curl_setopt($ch, CURLOPT_POST, 1);

	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);







	// 	curl_exec($ch);







	// 	curl_close($ch);





	// 	$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.100 free points credited to your account.");

	// 	return redirect(site_url());
	// }



	public function step1()
	{
		$userid = $this->user->info->ID;
		$lawyer_practitioner = $this->input->post("lawyer_practitioner");
		$date_of_birth = $this->input->post("date_of_birth");
		$firm_name = $this->input->post("firm_name");
		$firm_email = $this->input->post("firm_email");
		$firm_name = (!empty($firm_name)) ? $firm_name : null;
		$firm_email = (!empty($firm_email)) ? $firm_email : null;
		$data = array(
			'userid' => $userid,
			'lawyer_practitioner' => $lawyer_practitioner,
			'date_of_birth' => $date_of_birth,
			'firm_name' => $firm_name,
			'firm_email' => $firm_email,
			'verification_level' => 1
		);
		$verification_level = array(
			'verification_level' => 1
		);
		$check_coreprofile = $this->home_model->get_coreprofile($userid);
		$check_coreprofile = $check_coreprofile->row_array();
		if (!empty($check_coreprofile)) {
			unset($data['userid']);
			$coreprofile = $this->home_model->updatecoreprofile($data, $userid);
		} else {
			$coreprofile = $this->home_model->step1coreprofile($data);
		}

		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if (!empty($firm_email)) {
			$body = $this->user->info->first_name . " " . $this->user->info->last_name . " has added your email as a firm email while registration on Lawsmos.";
			$this->load->library('email');
			$this->email->from("no-reply@lawsmos.com");
			$this->email->to($firm_email);
			$this->email->subject("Lawyer Registration");
			$this->email->message($body);
			$this->email->send();

			$check_firm_user = $this->home_model->getUsersDetailByEmail($firm_email);
			if (!empty($check_firm_user)) {
				$this->home_model->add_notification(
					array(
						"userid" => $check_firm_user['ID'],
						"url" => "",
						"timestamp" => time(),
						"message" => $body = $this->user->info->first_name . " " . $this->user->info->last_name . " has added your email as a firm email while registration on Lawsmos.",
						"status" => 0,
						"fromid" => $this->user->info->ID
					)
				);
			}
		}
		if ($coreprofile) {
			$update_verification_level = $this->input->post("update_verification_level");
			if ($update_verification_level != 'true') {
				$this->session->set_flashdata("globalmsg", "Profile updated successfully");
			}
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		}
	}

	public function step2()
	{

		$state_bar_form = "";

		$bar_association_form = "";




		$userid = $this->user->info->ID;

		$practicing_lawyer = $this->input->post("practicing_lawyer");
		$unpracticing_city = $this->input->post("unpracticing_city");
		$unpracticing_fields_of_interest = $this->input->post("unpracticing_fields_of_interest");
		$unpracticing_area_of_specialization = $this->input->post("unpracticing_area_of_specialization");
		$unpracticing_working_as = $this->input->post("unpracticing_working_as");
		$organization_name = $this->input->post("organization_name");
		$category_id_array = $this->input->post("cat_type_id");
		$language = $this->input->post("lang");
		// $language = json_encode($language);
		$registration_no = $this->input->post("regno");
		$councel_registration_no = $this->input->post("councelregno");
		$year_reg = $this->input->post("year_reg");
		$practice_city = $this->input->post("practice_city");
		$total_exp = $this->input->post("total_exp");
		$court_name = $this->input->post("court_name");
		$court_names = explode(',', $court_name);
		$add_court_field = $this->input->post("add_court_field");
		$add_court_fields = "";
		if (!empty($add_court_field)) {
			$add_court_fields = explode(',', $add_court_field);
		}

		$add_court = $this->input->post("add_court");
		$bar_association_member = $this->input->post("bar_association_member");
		$bar_association_name = $this->input->post("bar_association_name");
		$bar_association_name = json_encode($bar_association_name);
		$add_court = (!empty($add_court)) ? $add_court : null;
		$registration_no = (!empty($registration_no)) ? $registration_no : null;
		//check state bar council no is exist 
		$state_bar_no_exist = $this->home_model->state_bar_no($councel_registration_no, $userid);
		//check bar association registration no is exist 

		$bar_registration_no_exist = $this->home_model->bar_registration_no($registration_no, $userid);
		if (!empty($state_bar_no_exist)) {
			$response['message'] = "state_bar_no_already_exist";
			$response['status_code'] = 0;
			echo json_encode($response);
			exit();
		}

		if (!empty($bar_registration_no_exist)) {
			$response['message'] = "bar_reg_no_already_exist";
			$response['status_code'] = 2;
			echo json_encode($response);
			exit();
		} else {

			//new court name add to court table

			
			$court_ids = [];
			if (!empty($add_court)) {
				$new_courts_id = $this->home_model->add_new_court($add_court);
				// array_push($court_names,$new_court_id);
				$court_names = array_merge($court_names, $new_courts_id);
			}

			if (!empty($add_court_fields)) {
				$new_courts_id = $this->home_model->add_new_courts($add_court_fields);
				$court_names = array_merge($court_names, $new_courts_id);
			} 
			$arr_key = array_search('others', $court_ids);

			if ($arr_key != '' || $arr_key == 0) {
				unset($court_ids[$arr_key]);
			}

			$court_name = implode(',', $court_names);
			// $filename= $_FILES["councelregform"]["name"];
			// $file_ext = pathinfo($filename,PATHINFO_EXTENSION);
			// $array_name  = ['jpg' , 'jpeg', 'png' ];
			// if(!in_array ( $file_ext, $array_name )){
			// 		$response['message'] = "Please upload correct format image";
			// 		$response['status_code'] = 3;
			// 		echo json_encode($response);die;
			// 		exit();
			// }
			
			
			if (isset($_FILES['councelregform']) && ($_FILES['councelregform']['size'] > 0)) {
				$upload_counsil_form = $this->uploadstatebarform();
				if ($upload_counsil_form['status'] == 1) {
					$state_bar_form = $upload_counsil_form['message'];
				}
			} else {
				$state_bar_form = $this->input->post("councelregform_url");
			}

			$upload_bar_ref_form = [];
			$new_bar_form = false;
			if ($bar_association_member == 1) {
				if (isset($_FILES['barregform_0'])) {
					if ($_FILES['barregform_0']['size'] > 0) {
						$upload_bar_association_form_0 = $this->uploadbarassoform(0);
						if ($upload_bar_association_form_0['status'] == 1) {
							$new_bar_form = true;
							array_push($upload_bar_ref_form, $upload_bar_association_form_0['message']);
						}
					}
				}
				if (isset($_FILES['barregform_1'])) {
					if ($_FILES['barregform_1']['size'] > 0) {
						$upload_bar_association_form_1 = $this->uploadbarassoform(1);
						if ($upload_bar_association_form_1['status'] == 1) {
							$new_bar_form = true;
							array_push($upload_bar_ref_form, $upload_bar_association_form_1['message']);
						}
					}
				}
			}

			// print_r($court_names);
			// echo "<pre>";
			// print_r($this->input->post());
			// die();

			$data = array(
				'practicing_lawyer' => $practicing_lawyer,
				'unpracticing_city' => $unpracticing_city,
				'unpracticing_fields_of_interest' => $unpracticing_fields_of_interest,
				'unpracticing_area_of_specialization' => $unpracticing_area_of_specialization,
				'unpracticing_working_as' => $unpracticing_working_as,
				'organization_name' => $organization_name,
				'category_id_array' => $category_id_array,
				'language' => $language,
				'councel_registration_no' => $councel_registration_no,
				'state_bar_form' => $state_bar_form,
				'registration_no' => json_encode($registration_no),
				'year_reg' => $year_reg,
				'total_exp' => $total_exp,
				'practice_city' => $practice_city,
				'field_court_number_array' => $court_name,
				'bar_association_member' => $bar_association_member,
				'bar_association_name' => $bar_association_name,
				'verification_level' => 2
			);

			if ($new_bar_form == true) {
				$data['bar_association_form'] = json_encode($upload_bar_ref_form);
			}

			$verification_level = array(
				'verification_level' => 2
			);

			$coreprofile = $this->home_model->step2coreprofile($data, $userid);
			$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);

			if ($coreprofile) {
				$update_verification_level = $this->input->post("update_verification_level");
				if ($update_verification_level != 'true') {
					$this->session->set_flashdata("globalmsg", "Profile updated successfully");
				}
				$response['status_code'] = true;
				$response['message'] = "ok";
				echo json_encode($response);
			}
		}
	}

	public function step3()
	{
		$userid = $this->user->info->ID;
		$school_10    = $this->input->post("school_10");
		$board_10     = $this->input->post("board_10");
		$year_pass_10 = $this->input->post("year_pass_10");
		$x_marks_type      = $this->input->post("x_marks_type");
		$marks_x_percentage      = $this->input->post("marks_x_percentage");
		$x_division      = $this->input->post("x_division");
		$marks_x_cgpa      = $this->input->post("marks_x_cgpa");
		$school_12    = $this->input->post("school_12");
		$board_12     = $this->input->post("board_12");
		$year_pass_12 = $this->input->post("year_pass_12");
		$xii_marks_type      = $this->input->post("xii_marks_type");
		$marks_xii_percentage      = $this->input->post("marks_xii_percentage");
		$xii_division      = $this->input->post("xii_division");
		$marks_xii_cgpa      = $this->input->post("marks_xii_cgpa");
		$degree       = $this->input->post("degree");
		$degree_array = json_encode($degree);
		$yearstart    = $this->input->post("yearstart");
		$yearstart_array = json_encode($yearstart);
		$yearend         = $this->input->post("yearend");
		$yearend_array   = json_encode($yearend);
		$college         = $this->input->post("college");
		$college_array   = json_encode($college);
		$university      = $this->input->post("university");
		$university_array = json_encode($university);
		$division         = $this->input->post("division");
		$division_array   = json_encode($division);
		$marksheet        = $this->input->post("marksheetform_url");
		$upload_marksheet_array  = json_encode($marksheet);

		$data = array(
			'school_10' => $school_10,
			'board_10' => $board_10,
			'year_pass_10' => $year_pass_10,
			'x_marks_type' => $x_marks_type,
			'marks_x_percentage' => $marks_x_percentage,
			'x_division' => $x_division,
			'marks_x_cgpa' => $marks_x_cgpa,
			'school_12' => $school_12,
			'board_12' => $board_12,
			'year_pass_12' => $year_pass_12,
			'xii_marks_type' => $xii_marks_type,
			'marks_xii_percentage' => $marks_xii_percentage,
			'xii_division' => $xii_division,
			'marks_xii_cgpa' => $marks_xii_cgpa,
			'degree_array' => $degree_array,
			'yearstart_array' => $yearstart_array,
			'yearend_array' => $yearend_array,
			'college_array' => $college_array,
			'university_array' => $university_array,
			'division_array' => $division_array,
			'upload_marksheet_array' => $upload_marksheet_array,
			'verification_level' => 3
		);

		$verification_level = array(
			'verification_level' => 3
		);
		$coreprofile = $this->home_model->step3coreprofile($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$update_verification_level = $this->input->post("update_verification_level");
			if ($update_verification_level != 'true') {
				$this->session->set_flashdata("globalmsg", "Profile updated successfully");
			}
			$result['status'] = true;
			echo json_encode($result);
		}
	}

	public function step4()
	{

		$userid = $this->user->info->ID;
		$experience_organization_name       = $this->input->post("experience_organization_name");
		$experience_organization_name = json_encode($experience_organization_name);
		$experience_city    = $this->input->post("experience_city");
		$experience_city = json_encode($experience_city);
		$experience_court         = $this->input->post("experience_court");
		$experience_court   = json_encode($experience_court);
		$experience_designation         = $this->input->post("experience_designation");
		$experience_designation   = json_encode($experience_designation);
		$experience_start_date      = $this->input->post("experience_start_date");
		$experience_start_date = json_encode($experience_start_date);
		$experience_end_date         = $this->input->post("experience_end_date");
		$experience_end_date   = json_encode($experience_end_date);
		$update_verification_level = $this->input->post("update_verification_level");

		$data = array(
			'experience_organization_name' => $experience_organization_name,
			'experience_city' => $experience_city,
			'experience_court' => $experience_court,
			'experience_designation' => $experience_designation,
			'experience_start_date' => $experience_start_date,
			'experience_end_date' => $experience_end_date,
			'verification_level' => 4
		);

		$verification_level = array(
			'verification_level' => 4
		);

		$coreprofile = $this->home_model->updatecoreprofile($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
			$getprofile = $getprofile->row_array();
			// print_r($getprofile);
			$result['practicing_lawyer'] = $getprofile['practicing_lawyer'];
			if ($update_verification_level != 'true') {
				$this->session->set_flashdata("globalmsg", "Profile updated successfully");
			}
			$result['status'] = true;
			echo json_encode($result);
		}

		// echo "<pre>";

		// print_r($_POST);die();

	}

	public function getPracticingLawyer()
	{

		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);

		$getprofile = $getprofile->row_array();

		$result['practicing_lawyer'] = $getprofile['practicing_lawyer'];

		$result['lawyer_practitioner'] = $getprofile['lawyer_practitioner'];

		$result['status'] = true;

		echo json_encode($result);
	}

	public function getFirmAddress()
	{

		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);

		$getprofile = $getprofile->row_array();



		if (!empty($getprofile)) {

			if (!empty($getprofile["firm_email"])) {

				$firm_data = $this->home_model->getUsersDetailByEmail($getprofile["firm_email"]);

				$result = $firm_data;

				echo json_encode($result);
			} else {

				$result['status'] = false;

				echo json_encode($result);
			}
		} else {

			$result['status'] = false;

			echo json_encode($result);
		}
	}

	public function testEmail()
	{

		echo "hello";
	}

	public function step5()
	{



		// echo "<pre>";

		// print_r($_POST);

		// die();

		$userid = $this->user->info->ID;

		$office_consultation_type = $this->input->post("office_consultation_type");
		$office_charges_checkbox_30_min = $this->input->post("office_charges_checkbox_30_min");
		$office_charges_checkbox_1hour = $this->input->post("office_charges_checkbox_1hour");
		$office_charges_30min = $this->input->post("office_charges_30min");
		$office_charges_1hour = $this->input->post("office_charges_1hour");

		$site_consultation_type = $this->input->post("site_consultation_type");
		$site_charges_checkbox_30_min = $this->input->post("site_charges_checkbox_30_min");
		$site_charges_checkbox_1hour = $this->input->post("site_charges_checkbox_1hour");
		$site_charges_30min = $this->input->post("site_charges_30min");
		$site_charges_1hour = $this->input->post("site_charges_1hour");


		$telvoice_consultation_type = $this->input->post("telvoice_consultation_type");
		$telvoice_charges_checkbox_30_min = $this->input->post("telvoice_charges_checkbox_30_min");
		$telvoice_charges_checkbox_1hour = $this->input->post("telvoice_charges_checkbox_1hour");
		$telvoice_charges_30min = $this->input->post("telvoice_charges_30min");
		$telvoice_charges_1hour = $this->input->post("telvoice_charges_1hour");



		$telvideo_consultation_type = $this->input->post("telvideo_consultation_type");
		$telvideo_charges_checkbox_30_min = $this->input->post("telvideo_charges_checkbox_30_min");
		$telvideo_charges_checkbox_1hour = $this->input->post("telvideo_charges_checkbox_1hour");
		$telvideo_charges_30min = $this->input->post("telvideo_charges_30min");
		$telvideo_charges_1hour = $this->input->post("telvideo_charges_1hour");



		$email_consultation_type = $this->input->post("email_consultation_type");
		$email_consultation_charges = $this->input->post("email_consultation_charges");
		$travel_consultation = $this->input->post("travel_consultation");
		$apperance_charges = $this->input->post("apperance_charges");
		$haur_draft_charges = $this->input->post("haur_draft_charges");

		$update_verification_level = $this->input->post("update_verification_level");


		$data = [
			'office_consultation_type' => $office_consultation_type,
			'office_charges_checkbox_30_min' => $office_charges_checkbox_30_min,
			'office_charges_checkbox_1hour' => $office_charges_checkbox_1hour,
			'office_charges_30min' => $office_charges_30min,
			'office_charges_1hour' => $office_charges_1hour,

			'site_consultation_type' => $site_consultation_type,
			'site_charges_checkbox_30_min' => $site_charges_checkbox_30_min,
			'site_charges_checkbox_1hour' => $site_charges_checkbox_1hour,
			'site_charges_30min' => $site_charges_30min,
			'site_charges_1hour' => $site_charges_1hour,

			'telvoice_consultation_type' => $telvoice_consultation_type,
			'telvoice_charges_checkbox_30_min' => $telvoice_charges_checkbox_30_min,
			'telvoice_charges_checkbox_1hour' => $telvoice_charges_checkbox_1hour,
			'telvoice_charges_30min' => $telvoice_charges_30min,
			'telvoice_charges_1hour' => $telvoice_charges_1hour,

			'telvideo_consultation_type' => $telvideo_consultation_type,
			'telvideo_charges_checkbox_30_min' => $telvideo_charges_checkbox_30_min,
			'telvideo_charges_checkbox_1hour' => $telvideo_charges_checkbox_1hour,
			'telvideo_charges_30min' => $telvideo_charges_30min,
			'telvideo_charges_1hour' => $telvideo_charges_1hour,

			'email_consultation_type' => $email_consultation_type,
			'email_consultation_charges' => $email_consultation_charges,
			'travel_consultation' => $travel_consultation,
			'apperance_charges' => $apperance_charges,
			'haur_draft_charges' => $haur_draft_charges,
			'verification_level' => 5

		];


		$address1 = $this->common->nohtml($this->input->post("address1"));
		$address2 = $this->common->nohtml($this->input->post("address2"));
		$address3 = $this->common->nohtml($this->input->post("address3"));
		$location_city = $this->common->nohtml($this->input->post("location_city"));
		$location_state = $this->common->nohtml($this->input->post("location_state"));
		$location_country = $this->common->nohtml($this->input->post("location_country"));
		$lat = $this->common->nohtml($this->input->post("lat"));
		$lng = $this->common->nohtml($this->input->post("lng"));



		$address_data = array(

			'address_1' => $address1,
			'address_2' => $address2,
			'address3' => $address3,
			'city' => $location_city,
			'state' => $location_state,
			'country' => $location_country,
			'lat' => $lat,
			'longitude' => $lng,
			'LatitudeLongitude' => $lat . "," . $lng,
			'verification_level' => 5
		);


		$coreprofile = $this->home_model->step4coreprofile($data, $userid);

		$update_address = $this->user_model->update_user($this->user->info->ID, $address_data);

		$days_value = $this->input->post("days_value");
		$start_time_hour = $this->input->post("start_time_hour");
		$start_time_min = $this->input->post("start_time_min");
		$end_time_hour = $this->input->post("end_time_hour");
		$end_time_min = $this->input->post("end_time_min");
		$total_selected_days = count($days_value);


		$this->home_model->delete_consultation_timing($userid);

		foreach ($days_value as $key => $days) {
			$days_array = array(
				"user_id" => $userid,
				"day_name" => $days,
				"start_hour" => $start_time_hour[$key],
				"start_min" => $start_time_min[$key],
				"end_hour" => $end_time_hour[$key],
				"end_min" => $end_time_min[$key],
				"created_at" => date("Y-m-d H:i:s")
			);
			$this->home_model->insert_consultation_timing($days_array);
		}

		if ($coreprofile) {
			if ($update_verification_level != 'true') {
				$this->session->set_flashdata("globalmsg", "Profile updated successfully");
			}
			$result['status'] = true;
			echo json_encode($result);
		}
	}



	public function step6()
	{

		$userid = $this->user->info->ID;

		$firm_address_1 = "";
		$firm_address_2 = "";
		$firm_address_3 = "";
		$firm_city = "";
		$firm_state = "";
		$firm_country = "";
		$firm_lat = "";
		$firm_lng = "";
		$firm_pincode = "";

		$tmp_address1_name = "";
		$tmp_address2_contact_no = "";
		$tmp_address1 = "";
		$tmp_address2 = "";
		$tmp_address3 = "";
		$tmp_location_city = "";
		$tmp_location_state = "";
		$tmp_location_country = "";
		$tmp_lat = "";
		$tmp_lng = "";
		$tmp_pincode = "";

		$address1_name = $this->common->nohtml($this->input->post("address1_name"));
		$address1_contact_no = $this->common->nohtml($this->input->post("address1_contact_no"));
		$address1 = $this->common->nohtml($this->input->post("address1"));
		$address2 = $this->common->nohtml($this->input->post("address2"));
		$address3 = $this->common->nohtml($this->input->post("address3"));
		$location_city = $this->common->nohtml($this->input->post("location_city"));
		$location_state = $this->common->nohtml($this->input->post("location_state"));
		$location_country = $this->common->nohtml($this->input->post("location_country"));
		$lat = $this->common->nohtml($this->input->post("lat"));
		$lng = $this->common->nohtml($this->input->post("lng"));
		$pincode = $this->input->post("pincode");

		$tmp_address1_name = $this->common->nohtml($this->input->post("tmp_address1_name"));
		$tmp_address1_contact_no = $this->common->nohtml($this->input->post("tmp_address1_contact_no"));
		$tmp_address1 = $this->common->nohtml($this->input->post("tmp_address1"));
		$tmp_address2 = $this->common->nohtml($this->input->post("tmp_address2"));
		$tmp_address3 = $this->common->nohtml($this->input->post("tmp_address3"));
		$tmp_location_city = $this->common->nohtml($this->input->post("tmp_location_city"));
		$tmp_location_state = $this->common->nohtml($this->input->post("tmp_location_state"));
		$tmp_location_country = $this->common->nohtml($this->input->post("tmp_location_country"));
		$tmp_lat = $this->common->nohtml($this->input->post("tmp_lat"));
		$tmp_lng = $this->common->nohtml($this->input->post("tmp_lng"));
		$tmp_pincode = $this->input->post("tmp_pincode");

		$firm_address_1 = $this->common->nohtml($this->input->post("firm_address_1"));
		$firm_address_2 = $this->common->nohtml($this->input->post("firm_address_2"));
		$firm_address_3 = $this->common->nohtml($this->input->post("firm_address_3"));
		$firm_city = $this->common->nohtml($this->input->post("firm_city"));
		$firm_state = $this->common->nohtml($this->input->post("firm_state"));
		$firm_country = $this->common->nohtml($this->input->post("firm_country"));
		$firm_lat = $this->common->nohtml($this->input->post("firm_lat"));
		$firm_lng = $this->common->nohtml($this->input->post("firm_lng"));
		$firm_pincode = $this->input->post("firm_pincode");

		$update_verification_level = $this->input->post("update_verification_level");

		$address_data = array(

			'address1_name' => $address1_name,
			'address1_contact_no' => $address1_contact_no,
			'address_1' => $address1,
			'address_2' => $address2,
			'address3' => $address3,
			'city' => $location_city,
			'state' => $location_state,
			'country' => $location_country,
			'lat' => $lat,
			'longitude' => $lng,
			'LatitudeLongitude' => $lat . "," . $lng,
			'zipcode' => $pincode,

			'firm_address_1' => $firm_address_1,
			'firm_address_2' => $firm_address_2,
			'firm_address_3'  => $firm_address_3,
			'firm_city'    => $firm_city,
			'firm_state'     => $firm_state,
			'firm_country'   => $firm_country,
			'firm_lat'       => $firm_lat,
			'firm_lng' => $firm_lng,
			'firm_pincode' => $firm_pincode,

			'tmp_address1_name' => $tmp_address1_name,
			'tmp_address1_contact_no' => $tmp_address1_contact_no,
			'tmp_address1' => $tmp_address1,
			'tmp_address2' => $tmp_address2,
			'tmp_address3' => $tmp_address3,
			'tmp_location_city' => $tmp_location_city,
			'tmp_location_state' => $tmp_location_state,
			'tmp_location_country' => $tmp_location_country,
			'tmp_lat' => $tmp_lat,
			'tmp_lng' => $tmp_lng,
			'tmp_pincode' => $tmp_pincode,

		);

		if ($update_verification_level == 'true') {
			$address_data['verification_level'] = 6;
			$address_data['complete_profile'] = 1;
			$data = array(
				'verification_level' => 6,
			);
			$coreprofile = $this->home_model->updatecoreprofile($data, $userid);
		}		

		$update_address = $this->user_model->update_user($this->user->info->ID, $address_data);

		
			if ($update_verification_level == 'true') {
				$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.3000 free points credited to your account once admin verified your profile.");
			}else{
				$this->session->set_flashdata("globalmsg", "Profile updated successfully");
			}
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		
	}

	private function uploadstatebarform()
	{

		if ($_FILES['councelregform']['size'] > 0) {

			$settings = array(

				"upload_path" => "uploads",

				"overwrite" => FALSE,

				"max_filename" => 300,

				"encrypt_name" => TRUE,

				"remove_spaces" => TRUE,

				"allowed_types" => $this->settings->info->file_types,

				"max_size" => $this->settings->info->file_size,

			);

			$this->load->library("upload");

			$this->upload->initialize($settings);

			$this->load->library('upload', $settings);



			$res = [];

			if (!$this->upload->do_upload('councelregform')) {

				$error = array('error' => $this->upload->display_errors());

				$res['status'] = 0;

				$res['message'] = "The upload path does not appear to be valid";

				// $this->load->view('upload_form', $error);

			} else {

				$data = array('upload_data' => $this->upload->data());

				$res['status'] = 1;

				$res['message'] = $data['upload_data']['file_name'];

				// $this->load->view('upload_success', $data);

			}

			return $res;
		}
	}

	public function uploadmarksheetform()
	{

		// var_dump($_FILES);die;

		if ($_FILES['marksheetform']['size'] > 0) {

			$settings = array(

				"upload_path" => "uploads",

				"overwrite" => FALSE,

				"max_filename" => 300,

				"encrypt_name" => TRUE,

				"remove_spaces" => TRUE,

				"allowed_types" => $this->settings->info->file_types,

				"max_size" => $this->settings->info->file_size,

			);

			$this->load->library("upload");

			$this->upload->initialize($settings);

			$this->load->library('upload', $settings);



			$res = [];

			if (!$this->upload->do_upload('marksheetform')) {

				$error = array('error' => $this->upload->display_errors());

				$res['status'] = 0;

				$res['message'] = "The upload path does not appear to be valid";

				// $this->load->view('upload_form', $error);

			} else {

				$data = array('upload_data' => $this->upload->data());

				$res['status'] = 1;

				$res['message'] = $data['upload_data']['file_name'];

				// $this->load->view('upload_success', $data);

			}

			echo json_encode($res);
			die;
		}
	}



	// private function uploadbarassoform(){

	// 	if($_FILES['barregform']['size'] > 0){

	// 		$settings = array(

	// 			"upload_path" => $this->settings->info->upload_path,

	// 			"overwrite" => FALSE,

	// 			"max_filename" => 300,

	// 			"encrypt_name" => TRUE,

	// 			"remove_spaces" => TRUE,

	// 			"allowed_types" => $this->settings->info->file_types,

	// 			"max_size" => $this->settings->info->file_size,

	// 		);

	// 		$this->load->library("upload");

	// 		$this->upload->initialize($settings);

	// 		$this->load->library('upload', $settings);	



	// 		$res = [];

	// 		if ( ! $this->upload->do_upload('barregform'))

	// 		{

	// 			$error = array('error' => $this->upload->display_errors());

	// 			$res['status'] = 0;	

	// 			$res['message'] = $error;	

	// 		}else{

	// 			$data = array('upload_data' => $this->upload->data());

	// 			$res['status'] = 1;	

	// 			$res['message'] = $data['upload_data']['file_name'];	

	// 		}



	// 		return $res;

	// 	}	



	// }



	private function uploadbarassoform($no)
	{

		$pic_1 = "";

		if ($no == 0) {

			$pic_1 = $_FILES['barregform_0']['size'];
		}

		if ($no == 1) {

			$pic_1 = $_FILES['barregform_1']['size'];
		}



		if ($pic_1 > 0) {

			$settings = array(

				"upload_path" => "uploads",

				"overwrite" => FALSE,

				"max_filename" => 300,

				"encrypt_name" => TRUE,

				"remove_spaces" => TRUE,

				"allowed_types" => $this->settings->info->file_types,

				"max_size" => $this->settings->info->file_size,

			);

			$this->load->library("upload");

			$this->upload->initialize($settings);

			$this->load->library('upload', $settings);



			$res = [];

			if (!$this->upload->do_upload('barregform_' . $no)) {

				$error = array('error' => $this->upload->display_errors());

				$res['status'] = 0;

				$res['message'] = $error;
			} else {

				$data = array('upload_data' => $this->upload->data());

				$res['status'] = 1;

				$res['message'] = $data['upload_data']['file_name'];
			}



			return $res;
		}
	}

	public function get_user_friends_v2()
	{

		$query2 = $this->common->nohtml($this->input->get("term"));

		$users = $this->user_model->get_friend_names($query2, $this->user->info->ID);

		$usersArr = array();

		foreach ($users->result() as $user) {

			$u = new STDClass;

			$u->id = $user->username;

			$u->text = $user->first_name . " " . $user->last_name;

			$usersArr[] = $u;
		}

		header('Content-Type: application/json');

		echo json_encode(array("results" => $usersArr));

		exit();
	}



	private function get_fresh_results($stats)

	{

		$data = new STDclass;

		$data->google_members = $this->user_model->get_oauth_count("google");

		$data->facebook_members = $this->user_model->get_oauth_count("facebook");

		$data->twitter_members = $this->user_model->get_oauth_count("twitter");

		$data->total_members = $this->user_model->get_total_members_count();

		$data->new_members = $this->user_model->get_new_today_count();

		$data->active_today = $this->user_model->get_active_today_count();

		return $data;
	}



	public function change_language()

	{



		$languages = $this->config->item("available_languages");

		if (!isset($_COOKIE['language'])) {

			$lang = "";
		} else {

			$lang = $_COOKIE["language"];
		}

		$this->template->loadContent(
			"home/change_language.php",
			array(

				"languages" => $languages,

				"user_lang" => $lang

			)

		);
	}





	public function applymoderator()
	{

		if (!$this->user->loggedin) {

			redirect(site_url("login"));
		}







		if (empty($this->user->info->moderator_apply)) :

			$this->user_model->update_user(
				$this->user->info->ID,
				array(

					"moderator_apply" => 1,

				)

			);



			$this->session->set_flashdata("globalmsg", "You have successfully applied for Moderator..");



			return redirect(site_url());



		else :

			$this->session->set_flashdata("globalmsg", "You have Already applied for Moderator..Please Wait for approval usually it takes 48 hrs for approval.");



			return redirect(site_url());

		endif;
	}



	public function change_language_pro()

	{



		$lang = $this->common->nohtml($this->input->post("language"));

		$languages = $this->config->item("available_languages");



		if (!array_key_exists($lang, $languages)) {

			$this->template->error(lang("error_25"));
		}



		setcookie("language", $lang, time() + 3600 * 7, "/");

		$this->session->set_flashdata("globalmsg", lang("success_14"));

		redirect(site_url());
	}



	public function get_usernames()

	{

		$query = $this->common->nohtml($this->input->get("query"));



		if (!empty($query)) {

			$usernames = $this->user_model->get_usernames($query);

			if ($usernames->num_rows() == 0) {

				echo json_encode(array());
			} else {

				$array = array();

				foreach ($usernames->result() as $r) {





					$array[] = $r->username;
				}







				echo json_encode($array);

				exit();
			}
		} else {

			echo json_encode(array());

			exit();
		}
	}



	public function get_search_results()
	{
		$query = $this->common->nohtml($this->input->get("query"));

		$array = array();

		if (!empty($query)) {
			$usernames = $this->user_model->get_user_by_name($query);
			if ($usernames->num_rows() == 0) {
			} else {
				foreach ($usernames->result() as $r) {
					$s = new STDClass;
					$s->label = $r->first_name . " " . $r->last_name;
					$s->type = "user";
					$s->value = $r->username;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->avatar;
					$s->url = site_url("profile/" . $r->username);
					$array[] = $s;
				}
			}

			// Search pages

			$pages = $this->page_model->get_pages_by_name($query);

			if ($pages->num_rows() == 0) {
			} else {
				foreach ($pages->result() as $r) {
					if (!empty($r->slug)) {
						$slug = $r->slug;
					} else {
						$slug = $r->ID;
					}

					$s = new STDClass;
					$s->label = $r->name;
					$s->type = "page";
					$s->value = $r->name;
					$s->marker_id = $r->MarkerId;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->profile_avatar;
					$s->url = site_url("pages/view/" . $slug);
					$array[] = $s;
				}
			}
		}

		echo json_encode($array);
		exit();
	}

	public function get_law_firms()
	{
		$query = $this->common->nohtml($this->input->get("query"));

		$array = array();

		if (!empty($query)) {

			// Search pages
			$pages = $this->page_model->get_law_firms($query);

			if ($pages->num_rows() == 0) {
			} else {
				foreach ($pages->result() as $r) {
					if (!empty($r->slug)) {
						$slug = $r->slug;
					} else {
						$slug = $r->ID;
					}

					$s = new STDClass;
					$s->label = $r->name;
					$s->type = "Law Firm";
					$s->value = $r->name;
					$s->marker_id = $r->MarkerId;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->profile_avatar;
					$s->url = site_url("pages/view/" . $slug);
					$array[] = $s;
				}
			}
		}

		echo json_encode($array);
		exit();
	}

	public function get_universities()
	{
		$query = $this->common->nohtml($this->input->get("query"));

		$array = array();

		if (!empty($query)) {

			// Search pages
			$pages = $this->page_model->get_universities($query);

			if ($pages->num_rows() == 0) {
			} else {
				foreach ($pages->result() as $r) {
					if (!empty($r->slug)) {
						$slug = $r->slug;
					} else {
						$slug = $r->ID;
					}

					$s = new STDClass;
					$s->label = $r->name;
					$s->type = "page";
					$s->value = $r->name;
					$s->marker_id = $r->MarkerId;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->profile_avatar;
					$s->url = site_url("pages/view/" . $slug);
					$array[] = $s;
				}
			}
		}

		echo json_encode($array);
		exit();
	}







	public function get_search_resultssss()

	{

		$query = $this->common->nohtml($this->input->get("query"));



		$array = array();

		if (!empty($query)) {

			$usernames = $this->user_model->get_user_by_name($query);

			if ($usernames->num_rows() == 0) {
			} else {

				foreach ($usernames->result() as $r) {

					$s = new STDClass;

					$s->label = $r->first_name . " " . $r->last_name;

					$s->type = "user";

					$s->value = $r->username;

					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->avatar;

					$s->url = site_url("profile/" . $r->username);

					$array[] = $s;
				}
			}

			// Search pages



		}

		echo json_encode($array);

		exit();
	}



	public function get_names()

	{

		$query = $this->common->nohtml($this->input->get("query"));

		$CI = &get_instance();

		if (!empty($query)) {

			$usernames = $this->user_model->get_names($query);

			if ($usernames->num_rows() == 0) {

				echo json_encode(array());
			} else {

				$array = array();

				foreach ($usernames->result() as $r) {

					$u = new STDClass();

					$u->label = $r->first_name . " " . $r->last_name;

					$u->value = $r->ID;

					$u->url = base_url() . $CI->settings->info->upload_path_relative . '/' . $r->avatar;

					$array[] = $u;
				}



				echo json_encode($array);

				exit();
			}
		} else {

			echo json_encode(array());

			exit();
		}
	}



	public function load_notifications()

	{

		$notifications = $this->user_model

			->get_notifications($this->user->info->ID);

		$this->template->loadAjax(
			"home/ajax_notifications.php",
			array(

				"notifications" => $notifications

			),
			0

		);
	}



	public function load_notifications_unread()

	{

		$notifications = $this->user_model

			->get_notifications_unread($this->user->info->ID);

		$this->template->loadAjax(
			"home/ajax_notifications.php",
			array(

				"notifications" => $notifications

			),
			0

		);
	}



	public function read_all_noti($hash)

	{

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));
		}



		$this->user_model->update_user_notifications(
			$this->user->info->ID,
			array(

				"status" => 1

			)

		);



		$this->user_model->update_user(
			$this->user->info->ID,
			array(

				"noti_count" => 0

			)

		);



		$this->session->set_flashdata("globalmsg", lang("success_43"));

		redirect(site_url("home/notifications"));
	}





	public function clear_all_noti($hash)
	{

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));
		}







		$this->user_model->delete_user_notifications($this->user->info->ID);



		$this->session->set_flashdata("globalmsg", lang("success_118"));

		redirect(site_url("home/notifications"));
	}



	public function load_notification($id)

	{

		$notification = $this->user_model

			->get_notification($id, $this->user->info->ID);

		if ($notification->num_rows() == 0) {

			$this->template->error(lang("error_111"));
		}

		$noti = $notification->row();

		if (!$noti->status) {

			$this->user_model->update_notification(
				$id,
				array(

					"status" => 1

				)

			);

			$this->user_model->update_user(
				$this->user->info->ID,
				array(

					"noti_count" => $this->user->info->noti_count - 1

				)

			);
		}



		// redirect

		redirect(site_url($noti->url));
	}



	public function notifications()

	{

		$this->template->loadContent(
			"home/notifications.php",
			array()

		);
	}



	public function notification_read($id)

	{

		$notification = $this->user_model

			->get_notification($id, $this->user->info->ID);

		if ($notification->num_rows() == 0) {

			$this->template->error(lang("error_111"));
		}

		$noti = $notification->row();

		if (!$noti->status) {

			$this->user_model->update_notification(
				$id,
				array(

					"status" => 1

				)

			);

			$this->user_model->update_user(
				$this->user->info->ID,
				array(

					"noti_count" => $this->user->info->noti_count - 1

				)

			);
		}

		redirect(site_url("home/notifications"));
	}



	public function notification_unread($id)

	{

		$notification = $this->user_model

			->get_notification($id, $this->user->info->ID);

		if ($notification->num_rows() == 0) {

			$this->template->error(lang("error_111"));
		}

		$noti = $notification->row();

		if ($noti->status) {

			$this->user_model->update_notification(
				$id,
				array(

					"status" => 0

				)

			);

			$this->user_model->update_user(
				$this->user->info->ID,
				array(

					"noti_count" => $this->user->info->noti_count + 1

				)

			);
		}

		redirect(site_url("home/notifications"));
	}



	public function delete_notification($id, $hash)

	{

		if ($hash != $this->security->get_csrf_hash()) {

			$this->template->error(lang("error_6"));
		}

		$id = intval($id);

		$notification = $this->user_model

			->get_notification($id, $this->user->info->ID);

		if ($notification->num_rows() == 0) {

			$this->template->error(lang("error_113"));
		}

		$noti = $notification->row();

		if ($noti->status) {



			$this->user_model->update_user(
				$this->user->info->ID,
				array(

					"noti_count" => $this->user->info->noti_count + 1

				)

			);
		} else {

			$this->user_model->update_user(
				$this->user->info->ID,
				array(

					"noti_count" => $this->user->info->noti_count - 1

				)

			);
		}



		$this->user_model->delete_notification($id);

		$this->session->set_flashdata("globalmsg", lang("success_101"));

		redirect(site_url("home/notifications"));
	}



	public function notifications_page()

	{

		$this->load->library("datatables");



		$this->datatables->set_default_order("user_notifications.timestamp", "desc");



		// Set page ordering options that can be used

		$this->datatables->ordering(

			array(

				2 => array(

					"user_notifications.timestamp" => 0

				)

			)

		);

		$this->datatables->set_total_rows(

			$this->user_model

				->get_notifications_all_total($this->user->info->ID)

		);

		$notifications = $this->user_model

			->get_notifications_all($this->user->info->ID, $this->datatables);







		foreach ($notifications->result() as $r) {

			$msg = '<a href="' . site_url("profile/" . $r->username) . '">' . $r->username . '</a> ' . $r->message;

			$options = '<a href="' . site_url("home/notification_unread/" . $r->ID) . '" class="btn btn-default btn-xs">' . lang("ctn_655") . '</a>';

			if ($r->status != 1) {

				$msg .= ' <label class="label label-danger">' . lang("ctn_796") . '</label>';

				$options = '<a href="' . site_url("home/notification_read/" . $r->ID) . '" class="btn btn-info btn-xs">' . lang("ctn_656") . '</a>';
			}



			$this->datatables->data[] = array(

				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),

				$msg,

				date($this->settings->info->date_format, $r->timestamp),

				$options . ' <a href="' . site_url("home/delete_notification/" . $r->ID . "/" . $this->security->get_csrf_hash()) . '" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> <a href="' . site_url("home/load_notification/" . $r->ID) . '" class="btn btn-primary btn-xs">' . lang("ctn_657") . '</a>'

			);
		}

		echo json_encode($this->datatables->process());
	}





	private function get_languages()
	{

		$lang = $this->user_model->get_all_languages();

		return $lang;
	}

	private function get_courts()
	{

		$courts = $this->home_model->get_all_courts();

		return $courts;
	}



	public function getcitybystate()
	{

		$state_id = $this->input->post('state_id');

		$get_city = $this->home_model->get_city_by_state_id($state_id);

		$get_city  = $get_city->result();



		echo json_encode($get_city);
	}

	public function profile_law_student()
	{

		$languages = $this->get_languages();

		$courts = $this->get_courts();

		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();

		$all_city = $all_city->result();

		$states = $this->home_model->get_all_states();

		$states = $states->result();

		$getprofile = $getprofile->row_array();

		$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);

		$user_detail = $user_detail->row_array();

		if (empty($getprofile)) {

			$verification_level = 0;
		} else {

			$verification_level = $getprofile['verification_level'];
		}



		$this->template->loadContent("home/completeprofilestudent.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'core_profile' => $getprofile, 'states' => $states));
	}
	public function getTotalUsers()
	{

		// echo "<pre>";
		// print_r($this->input->get('usertype'));

		$usertype = $this->input->get('usertype');
		if (!empty($usertype)) {
			$profile_identification = [];
			foreach ($usertype as $type) {
				if ($type == "lawyer") {
					$profile_identification[] = 0;
				}
				if ($type == "seeker") {
					$profile_identification[] = 1;
				}
				if ($type == "student") {
					$profile_identification[] = 2;
				}
				if ($type == "professor") {
					$profile_identification[] = 3;
				}
			}
			$user_count = $this->home_model->getUsersCount($profile_identification);
			$response['user_count'] = $user_count;
		} else {
			$response['user_count'] = 0;
		}

		echo json_encode($response);
	}
	public function submit_seeker_coreprofile()
	{




		$userid = $this->user->info->ID;
		$seeker_type = $this->input->post("seeker_type");
		$date_of_birth = $this->input->post("date_of_birth");
		$address = $this->input->post("address");
		$state = $this->input->post("state");
		$city = $this->input->post("city");
		$pincode = $this->input->post("pincode");
		$education_level = $this->input->post("education_level");
		$degree = $this->input->post("degree");
		$occupation = $this->input->post("occupation");
		$language = $this->input->post("language");
		$marital_status = $this->input->post("marital_status");
		$annual_income = $this->input->post("annual_income");
		$categories = $this->input->post("categories");
		$organization_name = $this->input->post("organization_name");
		$office_location = $this->input->post("office_location");
		$office_address = $this->input->post("office_address");
		$office_state = $this->input->post("office_state");
		$office_city = $this->input->post("office_city");
		$office_pincode = $this->input->post("office_pincode");
		$organization_type = $this->input->post("organization_type");
		$inception_year = $this->input->post("inception_year");
		$category = $this->input->post("category");
		$no_of_employees = $this->input->post("no_of_employees");
		$annual_turnover = $this->input->post("annual_turnover");
		$organization_language = $this->input->post("organization_language");
		$age = $this->input->post("age");
		$organization_categories = $this->input->post("organization_categories");

		$data = array(
			'userid' => $userid,
			'age' => $age,
			'seeker_type' => $seeker_type,
			'date_of_birth' => $date_of_birth,
			'address' => $address,
			'state' => $state,
			'city' => $city,
			'pincode' => $pincode,
			'education_level' => $education_level,
			'degree' => $degree,
			'occupation' => $occupation,
			'language' => json_encode($language),
			'marital_status' => $marital_status,
			'annual_income' => $annual_income,
			'categories' => json_encode($categories),
			'organization_name' => $organization_name,
			'office_location' => $office_location,
			'office_address' => $office_address,
			'office_state' => $office_state,
			'office_city' => $office_city,
			'office_pincode' => $office_pincode,
			'organization_type' => $organization_type,
			'inception_year' => $inception_year,
			'category' => $category,
			'no_of_employees' => $no_of_employees,
			'annual_turnover' => $annual_turnover,
			'organization_language' => json_encode($organization_language),
			'organization_categories' => json_encode($organization_categories),
			'created_at' => date("Y-m-d H:i:s")
		);


		$verification_level = array(
			'complete_profile' => 1
		);
		$check_coreprofile_seeker = $this->home_model->get_coreprofile_seeker($userid);
		$check_coreprofile_seeker = $check_coreprofile_seeker->row_array();
		if (!empty($check_coreprofile_seeker)) {
			unset($data['userid']);
			$coreprofile_seeker = $this->home_model->updatecoreprofile_seeker($data, $userid);
		} else {
			$coreprofile_seeker = $this->home_model->insertcoreprofile_seeker($data);
		}
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile_seeker) {
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		}
	}
	public function student_step1()
	{
		$userid = $this->user->info->ID;
		$student_enrolled = $this->input->post("student_enrolled");
		$date_of_birth = $this->input->post("date_of_birth");
		$data = array(
			'userid' => $userid,
			'student_enrolled' => $student_enrolled,
			'date_of_birth' => $date_of_birth,
			'verification_level' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		);
		$verification_level = array(
			'verification_level' => 1
		);
		$check_coreprofile_student = $this->home_model->get_coreprofile_student($userid);
		$check_coreprofile_student = $check_coreprofile_student->row_array();
		if (!empty($check_coreprofile_student)) {
			unset($data['userid']);
			$coreprofile_student = $this->home_model->updatecoreprofile_student($data, $userid);
		} else {
			$coreprofile_student = $this->home_model->insertcoreprofile_student($data);
		}
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);

		if ($coreprofile_student) {
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		} else {
			$response['status_code'] = false;
			$response['message'] = "false";
			echo json_encode($response);
		}
	}
	public function student_step2()
	{
		$userid = $this->user->info->ID;
		$school_10    = $this->input->post("school_10");
		$board_10     = $this->input->post("board_10");
		$year_pass_10 = $this->input->post("year_pass_10");
		$x_marks_type      = $this->input->post("x_marks_type");
		$marks_x_percentage      = $this->input->post("marks_x_percentage");
		$x_division      = $this->input->post("x_division");
		$marks_x_cgpa      = $this->input->post("marks_x_cgpa");
		$school_12    = $this->input->post("school_12");
		$board_12     = $this->input->post("board_12");
		$year_pass_12 = $this->input->post("year_pass_12");
		$xii_marks_type      = $this->input->post("xii_marks_type");
		$marks_xii_percentage      = $this->input->post("marks_xii_percentage");
		$xii_division      = $this->input->post("xii_division");
		$marks_xii_cgpa      = $this->input->post("marks_xii_cgpa");
		$degree       = $this->input->post("degree");
		$degree_array = json_encode($degree);
		$yearstart    = $this->input->post("yearstart");
		$yearstart_array = json_encode($yearstart);
		$yearend         = $this->input->post("yearend");
		$yearend_array   = json_encode($yearend);
		$college         = $this->input->post("college");
		$college_array   = json_encode($college);
		$university      = $this->input->post("university");
		$university_array = json_encode($university);
		$division         = $this->input->post("division");
		$division_array   = json_encode($division);

		$data = array(
			'school_10' => $school_10,
			'board_10' => $board_10,
			'year_pass_10' => $year_pass_10,
			'x_marks_type' => $x_marks_type,
			'marks_x_percentage' => $marks_x_percentage,
			'x_division' => $x_division,
			'marks_x_cgpa' => $marks_x_cgpa,
			'school_12' => $school_12,
			'board_12' => $board_12,
			'year_pass_12' => $year_pass_12,
			'xii_marks_type' => $xii_marks_type,
			'marks_xii_percentage' => $marks_xii_percentage,
			'xii_division' => $xii_division,
			'marks_xii_cgpa' => $marks_xii_cgpa,
			'degree_array' => $degree_array,
			'yearstart_array' => $yearstart_array,
			'yearend_array' => $yearend_array,
			'college_array' => $college_array,
			'university_array' => $university_array,
			'division_array' => $division_array,
			'verification_level' => 2
		);

		$verification_level = array(
			'verification_level' => 2
		);
		$coreprofile = $this->home_model->updatecoreprofile_student($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$result['status'] = true;
			echo json_encode($result);
		}
	}

	public function student_step3()
	{
		$userid = $this->user->info->ID;
		$training_name_of_organization    = $this->input->post("training_name_of_organization");
		$training_state    = $this->input->post("training_state");
		$training_city     = $this->input->post("training_city");
		$training_court = $this->input->post("training_court");
		$training_start_date      = $this->input->post("training_start_date");
		$training_end_date      = $this->input->post("training_end_date");
		$role_learning      = $this->input->post("role_learning");
		$internship_validation      = $this->input->post("internship_validation");
		$contact_person_name    = $this->input->post("contact_person_name");
		$contact_person_designation     = $this->input->post("contact_person_designation");
		$contact_person_phone_no = $this->input->post("contact_person_phone_no");
		$contact_person_email_address      = $this->input->post("contact_person_email_address");
		$research_journal_name      = $this->input->post("research_journal_name");
		$research_issn_number      = $this->input->post("research_issn_number");
		$research_share_link      = $this->input->post("research_share_link");
		$course_college    = $this->input->post("course_college");
		$course_program_name     = $this->input->post("course_program_name");
		$course_location    = $this->input->post("course_location");
		$course_start_date     = $this->input->post("course_start_date");
		$course_end_date    = $this->input->post("course_end_date");
		$course_certificate_url     = $this->input->post("course_certificate_url");
		$course_validation    = $this->input->post("course_validation");
		$course_contact_person_name     = $this->input->post("course_contact_person_name");
		$course_contact_person_designation    = $this->input->post("course_contact_person_designation");
		$course_contact_person_email     = $this->input->post("course_contact_person_email");
		$project_title    = $this->input->post("project_title");
		$project_start_month    = $this->input->post("project_start_month");
		$project_end_month    = $this->input->post("project_end_month");
		$project_description    = $this->input->post("project_description");
		$project_link    = $this->input->post("project_link");
		$hobbies    = $this->input->post("hobbies");
		$awards     = $this->input->post("awards");
		$skills     = $this->input->post("skills");
		$academic_awards     = $this->input->post("academic_awards");
		$research_title = $this->input->post("research_title");
		
		$data = array(
			'training_name_of_organization' => json_encode($training_name_of_organization),
			'training_state' => json_encode($training_state),
			'training_city' => json_encode($training_city),
			'training_court' => json_encode($training_court),
			'training_start_date' => json_encode($training_start_date),
			'training_end_date' => json_encode($training_end_date),
			'role_learning' => json_encode($role_learning),
			'internship_validation' => json_encode($internship_validation),
			'contact_person_name' => json_encode($contact_person_name),
			'contact_person_designation' => json_encode($contact_person_designation),
			'contact_person_phone_no' => json_encode($contact_person_phone_no),
			'contact_person_email_address' => json_encode($contact_person_email_address),
			'research_journal_name' => json_encode($research_journal_name),
			'research_issn_number' => json_encode($research_issn_number),
			'research_share_link' => json_encode($research_share_link),
			'course_college' => json_encode($course_college),
			'course_program_name' => json_encode($course_program_name),
			'course_location' => json_encode($course_location),
			'course_start_date' => json_encode($course_start_date),
			'course_end_date' => json_encode($course_end_date),
			'course_certificate_url' => json_encode($course_certificate_url),
			'course_validation' => json_encode($course_validation),
			'course_contact_person_name' => json_encode($course_contact_person_name),
			'course_contact_person_designation' => json_encode($course_contact_person_designation),
			'course_contact_person_email' => json_encode($course_contact_person_email),
			'project_title' => $project_title,
			'project_start_month' => $project_start_month,
			'project_end_month' => $project_end_month,
			'project_description' => $project_description,
			'project_link' => $project_link,
			'hobbies' => $hobbies,
			'awards' => json_encode($awards),
			'skills' => $skills,
			'academic_awards' => $academic_awards,
			'verification_level' => 3,
			'research_title'=> json_encode($research_title)
		);

		$verification_level = array(
			'verification_level' => 3
		);
		$coreprofile = $this->home_model->updatecoreprofile_student($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$result['status'] = true;
			echo json_encode($result);
		}
	}
	public function student_step4()
	{
		$userid = $this->user->info->ID;
		$fields_of_interest    = $this->input->post("fields_of_interest");
		$career_interest     = $this->input->post("career_interest");


		$data = array(
			'fields_of_interest' => json_encode($fields_of_interest),
			'career_interest' => json_encode($career_interest),
			'verification_level' => 4
		);

		$verification_level = array(
			'verification_level' => 4
		);
		$coreprofile = $this->home_model->updatecoreprofile_student($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$result['status'] = true;
			echo json_encode($result);
		}
	}
	public function student_step5()
	{
		$userid = $this->user->info->ID;
		$profile_picture    = "";
		$cover_photo     = "";

		if (isset($_FILES['profile_picture']) && ($_FILES['profile_picture']['size'] > 0)) {
			$profile_picture_response = $this->uploadProfilePic();
			if ($profile_picture_response['status'] == 1) {
				$profile_picture = $profile_picture_response['message'];
			}
		}

		if (isset($_FILES['cover_photo']) && ($_FILES['cover_photo']['size'] > 0)) {
			$cover_photo_response = $this->uploadCoverPic();
			if ($cover_photo_response['status'] == 1) {
				$cover_photo = $cover_photo_response['message'];
			}
		}

		if ($profile_picture == "" || $cover_photo == "") {
			$result["status"] = false;
			$result["msg"] = "Something went wrong";
			echo json_encode($result);
		}

		$data = array(
			'profile_picture' => $profile_picture,
			'cover_photo' => $cover_photo,
			'verification_level' => 5
		);

		$verification_level = array(
			'avatar' => $profile_picture,
			'profile_header' => $cover_photo,
			'verification_level' => 5,
			'complete_profile' => 1
		);
		$coreprofile = $this->home_model->updatecoreprofile_student($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {

			$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.3000 free points credited to your account.");

			$result['status'] = true;
			echo json_encode($result);
		}
	}
	public function studentnew_step5()
	{
		$userid = $this->user->info->ID;
		$profile_picture    = "";
		$cover_photo     = "";

		if (isset($_FILES['profile_picture']) && ($_FILES['profile_picture']['size'] > 0)) {
			$profile_picture_response = $this->uploadProfilePic();
			if ($profile_picture_response['status'] == 1) {
				$profile_picture = $profile_picture_response['message'];
					$data = array(
					'profile_picture' => $profile_picture,
					
					);
					$coreprofile = $this->home_model->updatecoreprofile_student($data, $userid);
			}

		}

		if (isset($_FILES['cover_photo']) && ($_FILES['cover_photo']['size'] > 0)) {
			$cover_photo_response = $this->uploadCoverPic();
			if ($cover_photo_response['status'] == 1) {
				$cover_photo = $cover_photo_response['message'];
					$data = array(
					'cover_photo' => $cover_photo,
					
					);
					$coreprofile = $this->home_model->updatecoreprofile_student($data, $userid);
			}
		}

		

		

		
		
	

			$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.3000 free points credited to your account.");

			$result['status'] = true;
			echo json_encode($result);
		
	}
	private function uploadProfilePic()
	{
		if ($_FILES['profile_picture']['size'] > 0) {
			$settings = array(
				"upload_path" => "uploads",
				"overwrite" => FALSE,
				"max_filename" => 300,
				"encrypt_name" => TRUE,
				"remove_spaces" => TRUE,
				"allowed_types" => $this->settings->info->file_types,
				"max_size" => $this->settings->info->file_size,
			);

			$this->load->library("upload");
			$this->upload->initialize($settings);
			$this->load->library('upload', $settings);

			$res = [];
			if (!$this->upload->do_upload('profile_picture')) {
				$error = array('error' => $this->upload->display_errors());
				$res['status'] = 0;
				$res['message'] = "The upload path does not appear to be valid";
			} else {
				$data = array('upload_data' => $this->upload->data());
				$res['status'] = 1;
				$res['message'] = $data['upload_data']['file_name'];
			}

			return $res;
		}
	}
	private function uploadCoverPic()
	{
		if ($_FILES['cover_photo']['size'] > 0) {
			$settings = array(
				"upload_path" => "uploads",
				"overwrite" => FALSE,
				"max_filename" => 300,
				"encrypt_name" => TRUE,
				"remove_spaces" => TRUE,
				"allowed_types" => $this->settings->info->file_types,
				"max_size" => $this->settings->info->file_size,
			);

			$this->load->library("upload");
			$this->upload->initialize($settings);
			$this->load->library('upload', $settings);

			$res = [];
			if (!$this->upload->do_upload('cover_photo')) {
				$error = array('error' => $this->upload->display_errors());
				$res['status'] = 0;
				$res['message'] = "The upload path does not appear to be valid";
			} else {
				$data = array('upload_data' => $this->upload->data());
				$res['status'] = 1;
				$res['message'] = $data['upload_data']['file_name'];
			}

			return $res;
		}
	}

	private function uploadImage($name = "")
	{
		if ($_FILES[$name]['size'] > 0) {
			$settings = array(
				"upload_path" => "uploads",
				"overwrite" => FALSE,
				"max_filename" => 300,
				"encrypt_name" => TRUE,
				"remove_spaces" => TRUE,
				"allowed_types" => $this->settings->info->file_types,
				"max_size" => $this->settings->info->file_size,
			);

			$this->load->library("upload");
			$this->upload->initialize($settings);
			$this->load->library('upload', $settings);

			$res = [];
			if (!$this->upload->do_upload($name)) {
				$error = array('error' => $this->upload->display_errors());
				$res['status'] = 0;
				$res['message'] = "The upload path does not appear to be valid";
			} else {
				$data = array('upload_data' => $this->upload->data());
				$res['status'] = 1;
				$res['message'] = $data['upload_data']['file_name'];
			}

			return $res;
		}
	}

	public function professor_step1()
	{
		$userid = $this->user->info->ID;
		$professor_enrolled = $this->input->post("professor_enrolled");
		$date_of_birth = $this->input->post("date_of_birth");
		$employer_identification_document = "";

		if (isset($_FILES['employer_identification_document']) && ($_FILES['employer_identification_document']['size'] > 0)) {
			$employer_identification_document_response = $this->uploadImage('employer_identification_document');
			if ($employer_identification_document_response['status'] == 1) {
				$employer_identification_document = $employer_identification_document_response['message'];
			}
		}

		$data = array(
			'userid' => $userid,
			'professor_enrolled' => $professor_enrolled,
			'date_of_birth' => $date_of_birth,
			'employer_identification_document' => $employer_identification_document,
			'verification_level' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		);
		$verification_level = array(
			'verification_level' => 1
		);
		$check_coreprofile_professor = $this->home_model->get_coreprofile_professor($userid);
		$check_coreprofile_professor = $check_coreprofile_professor->row_array();
		if (!empty($check_coreprofile_professor)) {
			unset($data['userid']);
			$coreprofile_professor = $this->home_model->updatecoreprofile_professor($data, $userid);
		} else {
			$coreprofile_professor = $this->home_model->insertcoreprofile_professor($data);
		}
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);

		if ($coreprofile_professor) {
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		} else {
			$response['status_code'] = false;
			$response['message'] = "false";
			echo json_encode($response);
		}
	}
	public function professor_step2()
	{
		$userid = $this->user->info->ID;
		$school_10    = $this->input->post("school_10");
		$board_10     = $this->input->post("board_10");
		$year_pass_10 = $this->input->post("year_pass_10");
		$x_marks_type      = $this->input->post("x_marks_type");
		$marks_x_percentage      = $this->input->post("marks_x_percentage");
		$x_division      = $this->input->post("x_division");
		$marks_x_cgpa      = $this->input->post("marks_x_cgpa");
		$school_12    = $this->input->post("school_12");
		$board_12     = $this->input->post("board_12");
		$year_pass_12 = $this->input->post("year_pass_12");
		$xii_marks_type      = $this->input->post("xii_marks_type");
		$marks_xii_percentage      = $this->input->post("marks_xii_percentage");
		$xii_division      = $this->input->post("xii_division");
		$marks_xii_cgpa      = $this->input->post("marks_xii_cgpa");
		$degree       = $this->input->post("degree");
		$degree_array = json_encode($degree);
		$yearstart    = $this->input->post("yearstart");
		$yearstart_array = json_encode($yearstart);
		$yearend         = $this->input->post("yearend");
		$yearend_array   = json_encode($yearend);
		$college         = $this->input->post("college");
		$college_array   = json_encode($college);
		$university      = $this->input->post("university");
		$university_array = json_encode($university);
		$division         = $this->input->post("division");
		$division_array   = json_encode($division);

		$data = array(
			'school_10' => $school_10,
			'board_10' => $board_10,
			'year_pass_10' => $year_pass_10,
			'x_marks_type' => $x_marks_type,
			'marks_x_percentage' => $marks_x_percentage,
			'x_division' => $x_division,
			'marks_x_cgpa' => $marks_x_cgpa,
			'school_12' => $school_12,
			'board_12' => $board_12,
			'year_pass_12' => $year_pass_12,
			'xii_marks_type' => $xii_marks_type,
			'marks_xii_percentage' => $marks_xii_percentage,
			'xii_division' => $xii_division,
			'marks_xii_cgpa' => $marks_xii_cgpa,
			'degree_array' => $degree_array,
			'yearstart_array' => $yearstart_array,
			'yearend_array' => $yearend_array,
			'college_array' => $college_array,
			'university_array' => $university_array,
			'division_array' => $division_array,
			'verification_level' => 2
		);

		$verification_level = array(
			'verification_level' => 2
		);
		$coreprofile = $this->home_model->updatecoreprofile_professor($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$result['status'] = true;
			echo json_encode($result);
		}
	}
	public function professor_step3()
	{
		$userid = $this->user->info->ID;
		$college_name    = $this->input->post("college_name");
		$university_name    = $this->input->post("university_name");
		$ugc_affliation    = $this->input->post("ugc_affliation");
		$designation    = $this->input->post("designation");
		$training_state    = $this->input->post("training_state");
		$training_city     = $this->input->post("training_city");
		$training_start_date      = $this->input->post("training_start_date");
		$training_end_date      = $this->input->post("training_end_date");
		$role_learning      = $this->input->post("role_learning");
		$internship_validation      = $this->input->post("internship_validation");
		$contact_person_name    = $this->input->post("contact_person_name");
		$contact_person_designation     = $this->input->post("contact_person_designation");
		$contact_person_phone_no = $this->input->post("contact_person_phone_no");
		$contact_person_email_address      = $this->input->post("contact_person_email_address");
		$research_journal_name      = $this->input->post("research_journal_name");
		$research_issn_number      = $this->input->post("research_issn_number");
		$research_share_link      = $this->input->post("research_share_link");
		$course_college    = $this->input->post("course_college");
		$course_program_name     = $this->input->post("course_program_name");
		$course_location    = $this->input->post("course_location");
		$course_start_date     = $this->input->post("course_start_date");
		$course_end_date    = $this->input->post("course_end_date");
		$course_certificate_url     = $this->input->post("course_certificate_url");
		$course_validation    = $this->input->post("course_validation");
		$course_contact_person_name     = $this->input->post("course_contact_person_name");
		$course_contact_person_designation    = $this->input->post("course_contact_person_designation");
		$course_contact_person_email     = $this->input->post("course_contact_person_email");
		$project_title    = $this->input->post("project_title");
		$project_start_month    = $this->input->post("project_start_month");
		$project_end_month    = $this->input->post("project_end_month");
		$project_description    = $this->input->post("project_description");
		$project_link    = $this->input->post("project_link");
		$hobbies    = $this->input->post("hobbies");
		$awards     = $this->input->post("awards");
		$skills     = $this->input->post("skills");
		$academic_awards     = $this->input->post("academic_awards");
		$research_title = $this->input->post("research_title");


		$data = array(
			'college_name' => json_encode($college_name),
			'university_name' => json_encode($university_name),
			'ugc_affliation' => json_encode($ugc_affliation),
			'designation' => json_encode($designation),
			'training_state' => json_encode($training_state),
			'training_city' => json_encode($training_city),
			'training_start_date' => json_encode($training_start_date),
			'training_end_date' => json_encode($training_end_date),
			'role_learning' => json_encode($role_learning),
			'internship_validation' => json_encode($internship_validation),
			'contact_person_name' => json_encode($contact_person_name),
			'contact_person_designation' => json_encode($contact_person_designation),
			'contact_person_phone_no' => json_encode($contact_person_phone_no),
			'contact_person_email_address' => json_encode($contact_person_email_address),
			'research_journal_name' => json_encode($research_journal_name),
			'research_issn_number' => json_encode($research_issn_number),
			'research_share_link' => json_encode($research_share_link),
			'course_college' => json_encode($course_college),
			'course_program_name' => json_encode($course_program_name),
			'course_location' => json_encode($course_location),
			'course_start_date' => json_encode($course_start_date),
			'course_end_date' => json_encode($course_end_date),
			'course_certificate_url' => json_encode($course_certificate_url),
			'course_validation' => json_encode($course_validation),
			'course_contact_person_name' => json_encode($course_contact_person_name),
			'course_contact_person_designation' => json_encode($course_contact_person_designation),
			'course_contact_person_email' => json_encode($course_contact_person_email),
			'project_title' => $project_title,
			'project_start_month' => $project_start_month,
			'project_end_month' => $project_end_month,
			'project_description' => $project_description,
			'project_link' => $project_link,
			'hobbies' => $hobbies,
			'awards' => json_encode($awards),
			'skills' => $skills,
			'academic_awards' => $academic_awards,
			'verification_level' => 3,
			'research_title' =>json_encode($research_title)
		);

		$verification_level = array(
			'verification_level' => 3
		);
		$coreprofile = $this->home_model->updatecoreprofile_professor($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$result['status'] = true;
			echo json_encode($result);
		}
	}
	public function professor_step4()
	{
		$userid = $this->user->info->ID;
		$fields_of_interest    = $this->input->post("fields_of_interest");
		$career_interest     = $this->input->post("career_interest");


		$data = array(
			'fields_of_interest' => json_encode($fields_of_interest),
			'career_interest' => json_encode($career_interest),
			'verification_level' => 4
		);

		$verification_level = array(
			'verification_level' => 4
		);
		$coreprofile = $this->home_model->updatecoreprofile_professor($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {
			$result['status'] = true;
			echo json_encode($result);
		}
	}
	public function professor_step5()
	{
		$userid = $this->user->info->ID;
		$profile_picture    = "";
		$cover_photo     = "";

		if (isset($_FILES['profile_picture']) && ($_FILES['profile_picture']['size'] > 0)) {
			$profile_picture_response = $this->uploadProfilePic();
			if ($profile_picture_response['status'] == 1) {
				$profile_picture = $profile_picture_response['message'];
			}
		}

		if (isset($_FILES['cover_photo']) && ($_FILES['cover_photo']['size'] > 0)) {
			$cover_photo_response = $this->uploadCoverPic();
			if ($cover_photo_response['status'] == 1) {
				$cover_photo = $cover_photo_response['message'];
			}
		}

		if ($profile_picture == "" || $cover_photo == "") {
			$result["status"] = false;
			$result["msg"] = "Something went wrong";
			echo json_encode($result);
		}

		$data = array(
			'profile_picture' => $profile_picture,
			'cover_photo' => $cover_photo,
			'verification_level' => 5
		);

		$verification_level = array(
			'avatar' => $profile_picture,
			'profile_header' => $cover_photo,
			'verification_level' => 5,
			'complete_profile' => 1
		);
		$coreprofile = $this->home_model->updatecoreprofile_professor($data, $userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level, $userid);
		if ($coreprofile) {

			$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.3000 free points credited to your account.");

			$result['status'] = true;
			echo json_encode($result);
		}
	}

	public function professornew_step5()
	{
		$userid = $this->user->info->ID;
		$profile_picture    = "";
		$cover_photo     = "";

		if (isset($_FILES['profile_picture']) && ($_FILES['profile_picture']['size'] > 0)) {
			$profile_picture_response = $this->uploadProfilePic();
			if ($profile_picture_response['status'] == 1) {
				$profile_picture = $profile_picture_response['message'];
					$data = array(
					'profile_picture' => $profile_picture,
					
					);
					$coreprofile = $this->home_model->updatecoreprofile_professor($data, $userid);
			}

		}

		if (isset($_FILES['cover_photo']) && ($_FILES['cover_photo']['size'] > 0)) {
			$cover_photo_response = $this->uploadCoverPic();
			if ($cover_photo_response['status'] == 1) {
				$cover_photo = $cover_photo_response['message'];
					$data = array(
					'cover_photo' => $cover_photo,
					
					);
					$coreprofile = $this->home_model->updatecoreprofile_professor($data, $userid);
			}
		}

		

		

		
		
	

			$this->session->set_flashdata("globalmsg", "Profile updated successfully");

			$result['status'] = true;
			echo json_encode($result);
		
	}
}
