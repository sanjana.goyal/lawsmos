<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home1 extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	
		
		$this->template->loadData("activeLink", 
		array("home" => array("general" => 1)));
		$this->load->model("user_model");
		$this->load->model("home_model");
		$this->load->model("page_model");
		$this->load->model("feed_model");
		$this->load->model("funds_model");
		$this->load->model("article_model");
		// $query = $this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
		// 
		if(!$this->user->loggedin) {
				
			
		  redirect(site_url("login"));
		
		}
		$this->template->set_error_view("error/login_error.php");
		$this->template->set_layout("client/themes/titan.php");
	}
	public function index($type = 0, $hashtag = "")
	{
		
		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location=$this->user->info->city;
		$location=explode(",",$location);
		$city=$location[0];
		$leads=$this->home_model->get_leads($city);
	
		
		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview=$getarticlesforreview->result_array();
		
		
		
		$this->template->loadContent("home/index.php", array(
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd"=>$topviewd,
			"getprofile"=>$getprofile,
			"leads"=>$leads,
			"countmessage"=>$countmessage,
			"getarticlesforreview"=>$getarticlesforreview,

			)
		);
	}
	
	
	
	
	public function enterlocation(){
		echo $_REQUEST['locationselect'];
		
		}
	public function comppleteprofile(){
		$languages = $this->get_languages();
		$courts = $this->get_courts();
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();
		$states = $this->home_model->get_all_states();
		$states = $states->result();
		$getprofile = $getprofile->row_array();
		$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
		$user_detail = $user_detail->row_array();
		if(empty($getprofile)){
			$verification_level = 0;
		}else{
			$verification_level = $getprofile['verification_level'];
		}
		
		$this->template->loadContent("home/completeprofile1.php", array('languages'=>$languages,'courts'=>$courts,'verification_level'=> $verification_level,'all_city'=>$all_city,'user_detail'=>$user_detail,'core_profile' => $getprofile,'states'=>$states));
		
	 }

	public function get_user_friends() 
	{
		$query2 = $this->common->nohtml($this->input->get("term"));

		$char = substr($query2, 0, 1);
		if($char == "@") {
			$query2 = substr($query2, 1, strlen($query2));
			$users = $this->user_model->get_names($query2);

			$usersArr = array();
			foreach($users->result() as $user) {
				$u = new STDClass;
				$u->uid = $user->username;
				$u->value = $user->first_name . " " . $user->last_name;
				$usersArr[] = $u;
			}
			header('Content-Type: application/json');
			echo json_encode($usersArr);
			exit();
		} elseif($char == "#") {

		}
	}
	public function appointment($hostid){
		 
		$userid=$this->user->info->ID;
		$getprofile=$this->home_model->get_coreprofile($hostid)->result();
		$gettimeschedule="";
		$getseekerdetail=$this->user_model->get_user_by_id($userid)->result();
		$getProviderDetail=$this->user_model->get_user_by_id($hostid)->result();
		$catagories=$this->user_model->get_catagories()->result();
		if($userid!=$hostid){
		$this->template->loadContent("home/bookappointment.php", array(
				"coreprofile" => $getprofile,
				"timeschedule" => $gettimeschedule,
				"userdetail" =>$getseekerdetail,
				"providerprofile"=>$getProviderDetail,
				"catss"=>$catagories,
			));	
		}
	}
		
		
		public function bookappointment(){
		
				$seekerid= $this->input->post('seekerid');
				$seekername= $this->input->post('seekername');
				$seekeremail= $this->input->post('seekeremail');
				$seekerphone= $this->input->post('seekerphone');
				$providername= $this->input->post('providername');
				$providerid= $this->input->post('providerid');
				$provideremail= $this->input->post('provideremail');
				$providerphone= $this->input->post('providerphone');
				$dateofappointment= $this->input->post('dateofappointment');
				$catagory= $this->input->post('catagory');
			    $subjectmatter= $this->input->post('subjectmatter');
				$time= $this->input->post('minute');
				$timeformat= $this->input->post('timeformat');
				$timeofappoint=  $time." ".$timeformat;
				$data=array(
					"service_provider_id"=>$providerid,
					"service_seeker_id"=>$seekerid,
					"seeker_name"=>$seekername,
					"seeker_contact"=>$seekerphone,
					"dateofappointment"=>$dateofappointment,
					"timeofappoint"=>$timeofappoint,
					"cat_id"=>$catagory,
					"matter"=>$subjectmatter,
				);
			$insert = $this->home_model->appointmentbooking($data);
			if($insert){
			$this->session->set_flashdata("globalmsg", "Your appointment details has been sent to $providername, You will be notified once he approve your appointment...");
			}else
			{
				$this->session->set_flashdata("globalmsg","Appointment with same lawyer already booked on date".$dateofappointment."at".$timeofappoint);
			}
		
					return redirect(site_url());	
		}
	
	
	
	public function uploadproof()
	{
		$this->load->library("upload");
		$userid=$this->user->info->ID;
			if ($_FILES['userfile']['size'] > 0) {
				if(!$this->settings->info->resize_avatar) {
					$settings = array(
						"upload_path" => $this->settings->info->upload_path,
						"overwrite" => FALSE,
						"max_filename" => 300,
						"encrypt_name" => TRUE,
						"remove_spaces" => TRUE,
						"allowed_types" => 'pdf',
						"max_size" => $this->settings->info->file_size,
					);
					if($this->settings->info->avatar_width > 0) {
						$settings['max_width'] = $this->settings->info->avatar_width;
					}
					if($this->settings->info->avatar_height > 0) {
						$settings['max_height'] = $this->settings->info->avatar_height;
					}
					$this->upload->initialize($settings);

				    if (!$this->upload->do_upload()) {
				    	$this->template->error(lang("error_21")
				    	.$this->upload->display_errors());
				    }

				    $data = $this->upload->data();

				     $image = $data['file_name'];
				} else {
					$this->upload->initialize(array( 
				       "upload_path" => $this->settings->info->upload_path,
				       "overwrite" => FALSE,
				       "max_filename" => 300,
				       "encrypt_name" => TRUE,
				       "remove_spaces" => TRUE,
				       "allowed_types" => "pdf",
				       "max_size" => $this->settings->info->file_size,
				    ));
				    if (!$this->upload->do_upload()) {
				    	$this->template->error(lang("error_21")
				    	.$this->upload->display_errors());
				    }
				    $data = $this->upload->data();
				    $image = $data['file_name'];
					$config['image_library'] = 'gd2';
					$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = FALSE;
					$config['width']         = $this->settings->info->avatar_width;
					$config['height']       = $this->settings->info->avatar_height;
					//$this->load->library('image_lib', $config);
					$this->load->library('image_lib');
					// Set your config up
					$this->image_lib->initialize($config);
					// Do your manipulation
					$this->image_lib->clear();
					if ( ! $this->image_lib->resize()) {
					       $this->template->error(lang("error_21") . 
					       	$this->image_lib->display_errors());
					}
				}
			} else {
				$image= $this->user->info->avatar;
			}
		$dis = $this->common->nohtml($this->input->post("disclain"));
		$data=array(
			"userid" => $userid,
			"proof"=>$image,
			"disclaimer"=>$dis, 
			);
		$q=$this->home_model->upload_proof_for_upgrade($data);
		if($q){
		$this->session->set_flashdata("globalmsg", "Document Has been uploaded successfully. You will be notified soon once administrator approve your profile for upgradation.");
	     }else{
			 $this->session->set_flashdata("globalmsg", "Your Document has already been send to administrator for approval. You Will be notified when admin approve your document.");
			 }
		return redirect(site_url('upgrade/advocate'));
	}
	
	public function studentform()
		{
		$userid=$this->user->info->ID;
		$institute=$this->input->post("inst");
		$degree=$this->input->post("degree");
		$university=$this->input->post("university");
		$grade=$this->input->post("grade");
		$yearstart=$this->input->post("yearstart");
		$yearend=$this->input->post("yearend");
		$data=array(
				"institute"=>$institute,
				"degree"=>$degree,
				"university"=>$university,
				"grade"=>$grade,
				"yearstart"=>$yearstart,
				"yearend"=>$yearend,
			);
		$orginst=$this->input->post("orginst");
		$yrtt=$this->input->post("yrtt");
		$from=$this->input->post("from");
		$to=$this->input->post("to");
		$interndata=array(
				"orgnaisation"=>$orginst,
				"year"=>$yrtt,
				"from"=>$from,
				"to"=>$to,
			);
		 $educationhistory=  json_encode($data);
		 $internhistory=  json_encode($interndata);
		 $data3=array(
			"USERID"=>$userid,
			"education_history"=>$educationhistory,
			"previous_internship"=>$internhistory,
			
		);
		$insertprofile = $this->home_model->insertstudentprofile($data3);
		$this->session->set_flashdata("globalmsg", "Your profile has been updated");
		return redirect(site_url());
		
		}	
	public function completeformprofile(){
		$userid=$this->user->info->ID;
		$category_id=$this->input->post("category_id");
		$language=$this->input->post("language");
		$language=json_encode($language);
		$registration_no=$this->input->post("registration_no");
		$councel_registration_no=$this->input->post("councel_registration_no");
		$year_reg=$this->input->post("year_reg");
		$practice_city=$this->input->post("practice_city");
		$field_court=$this->input->post("field_court");
	    $courtnamearray=json_encode($field_court);
		$field_court_number=$this->input->post("field_court_number");
		// $field_court_number_array=json_encode($field_court_number);
		$field_court_number_array="";
		$degree=$this->input->post("degree");
		$degree_array=json_encode($degree);
		$yearstart=$this->input->post("yearstart");
		$yearstart_array=json_encode($yearstart);
		$yearend=$this->input->post("yearend");
		$yearend_array=json_encode($yearend);
		$college=$this->input->post("college");
		$college_array=json_encode($college);
		$university=$this->input->post("university");
		$university_array=json_encode($university);
		$division=$this->input->post("division");
		$division_array=json_encode($division);
		$hour_charges=$this->input->post("hour_charges");
		$telephone_charges=$this->input->post("telephone_charges");
		$email_charges=$this->input->post("email_charges");
		$apperance_charges=$this->input->post("apperance_charges");
		$haur_draft_charges=$this->input->post("haur_draft_charges");
		$data = array(
		  'userid'=>$userid,
          'category_id_array'=>$category_id_array,
          'language'=>$language,
          'registration_no'=>$registration_no,
          'councel_registration_no'=>$councel_registration_no,
          'year_reg'=>$year_reg,
          'practice_city'=>$practice_city,
          'courtnamearray'=>$courtnamearray,
          'field_court_number_array'=>$field_court_number_array,
          'degree_array'=>$degree_array,
          'yearstart_array'=>$yearstart_array,
          'yearend_array'=>$yearend_array,
          'college_array'=>$college_array,
          'university_array'=>$university_array,
          'division_array'=>$division_array,
          'hour_charges'=>$hour_charges,
          'telephone_charges'=>$telephone_charges,
          'email_charges'=>$email_charges,
          'apperance_charges'=>$apperance_charges,
          'haur_draft_charges'=>$haur_draft_charges,
         );
	

		
		$location_country = $this->common->nohtml($this->input->post("location_country"));
		$location_state = $this->common->nohtml($this->input->post("location_state"));
	   $location_city = $this->common->nohtml($this->input->post("location_city"));
	   $permannentadr = $this->common->nohtml($this->input->post("permannentadr"));
	   $location_address = $this->common->nohtml($this->input->post("location_address"));
	   $zipcode = $this->common->nohtml($this->input->post("zipcode"));
		
		$address_data = array(
			'address_1' => $location_address,
			'permanentAddr' => $permannentadr,
			'city' => $location_city,
			'state' => $location_state,
			'country' => $location_country,
			'zipcode' => $zipcode,
		);

		$points=$this->user->info->points;
       $bonus_points=$this->user->info->bonus_points;
       $total_points=$this->user->info->total_points;
       $cal_points=$points+100;
       $cal_bonus_points=$bonus_points+100;
       $cal_total_points=$total_points+100;
	   $coreprofile = $this->home_model->insertcoreprofile($data);
	   $updatepoints=$this->home_model->updatepoints($userid,array(
			"points"=>$cal_points,
			"bonus_points"=>$cal_bonus_points,
			"total_points"=>$cal_total_points,
		));
		
		$this->page_model->add_history(array(
			"userid" => $userid,
			"notes" => "Profile Completion",
			"points" => 100
			)
		);
		
		//update user address in social db table(user)
		
		$update_address = $this->user_model->update_user($this->user->info->ID, $address_data);
		
		//update user address in lawyer db table(user)

		$ch = curl_init();
		
        $data2 = 'id='.$this->user->info->ID.'&zipcode='.$zipcode.'&country='.$location_country.'&state='.$location_state.'&city='.$location_city.'&address='.$location_address;
		
		$profile_tmp_url = str_replace("/social","",base_url());
		$profile_url = $profile_tmp_url."api/update_laravel_profile_address.php";	
		

		curl_setopt($ch, CURLOPT_URL, $profile_url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);

		

		curl_exec($ch);
		  
       
		
		curl_close($ch);
	
		
		$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.100 free points credited to your account.");
		return redirect(site_url());
		}

	public function step1(){
		$userid = $this->user->info->ID;
		
		$lawyer_practitioner = $this->input->post("lawyer_practitioner");
		$firm_name = $this->input->post("firm_name");
		$firm_email = $this->input->post("firm_email");
		
		
		$firm_name = (!empty($firm_name)) ? $firm_name : null;	
		$firm_email = (!empty($firm_email)) ? $firm_email : null;	
		
		$data = array(
				'userid'=>$userid,
				'lawyer_practitioner'=>$lawyer_practitioner,
				'firm_name'=>$firm_name,
				'firm_email'=>$firm_email,
				'verification_level'=>1
			);
			
			$verification_level = array(
				'verification_level' => 1
			); 

			$check_coreprofile = $this->home_model->get_coreprofile($userid);
			$check_coreprofile = $check_coreprofile->row_array();
			if(!empty($check_coreprofile)){
				unset($data['userid']);
				$coreprofile = $this->home_model->updatecoreprofile($data,$userid);
			}else{
				$coreprofile = $this->home_model->step1coreprofile($data);
			}

			
			$upadte_verification_level = $this->home_model->update_user_level($verification_level,$userid);
			
			if($coreprofile){
				$response['status_code'] = true;
				$response['message'] = "ok";
				echo json_encode($response);
			}
		

	}
	public function step2(){
		$state_bar_form = "";
		$bar_association_form = "" ;
		
		$userid = $this->user->info->ID;
		$category_id_array = $this->input->post("cat_type_id");
		$language = $this->input->post("lang");
		// $language = json_encode($language);
		$registration_no = $this->input->post("regno");
		$councel_registration_no = $this->input->post("councelregno");
		$year_reg = $this->input->post("year_reg");
		$practice_city = $this->input->post("practice_city");
		
		$total_exp = $this->input->post("total_exp");
		$court_name = $this->input->post("court_name");
		// $court_name = json_encode($court_name);
		$add_court = $this->input->post("add_court");
		$bar_association_member = $this->input->post("bar_association_member");
		$bar_association_name = $this->input->post("bar_association_name");
		$bar_association_name = json_encode($bar_association_name);
		
		
		$add_court = (!empty($add_court)) ? $add_court : null;	
		$registration_no = (!empty($registration_no)) ? $registration_no : null;	
			


		//check state bar council no is exist 
		$state_bar_no_exist = $this->home_model->state_bar_no($councel_registration_no,$userid);
		//check bar association registration no is exist 
		$bar_registration_no_exist = $this->home_model->bar_registration_no($registration_no,$userid);
		
		

		if(!empty($state_bar_no_exist->result())){
			$response['message'] = "state_bar_no_already_exist";
			$response['status_code'] = 1;
			echo json_encode($response);
		exit();
		}
		if(!empty($bar_registration_no_exist->result())){
			$response['message'] = "bar_reg_no_already_exist";
			$response['status_code'] = 2;
			echo json_encode($response);
			exit();
		}
		else{
			//new court name add to court table
		
			
			if(!empty($add_court)){
				$new_court_id = $this->home_model->add_new_court($add_court);
				// array_push($court_name,$new_court_id);
				$court_name .= ",".$new_court_id;
			}

			if($_FILES['councelregform']['size'] > 0){
				$upload_counsil_form = $this->uploadstatebarform();
					if($upload_counsil_form['status'] == 1){
						$state_bar_form = $upload_counsil_form['message'];
					}	

			}
			$upload_bar_ref_form = [];
			if($bar_association_member == "yes"){
				if(isset($_FILES['barregform_0'])){
					if($_FILES['barregform_0']['size'] > 0){
						$upload_bar_association_form_0 = $this->uploadbarassoform(0);
							if($upload_bar_association_form_0['status'] == 1){
								array_push($upload_bar_ref_form,$upload_bar_association_form_0['message']);
							}	
		
					}
				}
				
				if(isset($_FILES['barregform_1'])){
					if($_FILES['barregform_1']['size'] > 0){
						$upload_bar_association_form_1 = $this->uploadbarassoform(1);
							if($upload_bar_association_form_1['status'] == 1){
								array_push($upload_bar_ref_form,$upload_bar_association_form_1['message']);
							}	
		
					}
				}
				
			}


			
			$data = array(
				'category_id_array'=>$category_id_array,
				'language'=>$language,
				'councel_registration_no'=>$councel_registration_no,
				'state_bar_form'=>$state_bar_form,
				'registration_no'=>$registration_no,
				'bar_association_form'=>json_encode($upload_bar_ref_form),
				'year_reg'=>$year_reg,
				'total_exp'=>$total_exp,
				'practice_city'=>$practice_city,
				'field_court_number_array'=>$court_name,
				'bar_association_member'=>$bar_association_member,
				'bar_association_name'=>$bar_association_name,
				'verification_level'=>2
			);
		
			

			$verification_level = array(
				'verification_level' => 2
			); 
			
			$coreprofile = $this->home_model->step2coreprofile($data,$userid);
			$upadte_verification_level = $this->home_model->update_user_level($verification_level,$userid);
			
			if($coreprofile){
				$response['status_code'] = true;
				$response['message'] = "ok";
				echo json_encode($response);
			}
		}

		


		
	}
	public function step3(){
		$userid = $this->user->info->ID;
		
		$school_10    = $this->input->post("school_10");
		$board_10     = $this->input->post("board_10");
		$year_pass_10 = $this->input->post("year_pass_10");
		$x_marks_type      = $this->input->post("x_marks_type");
		$marks_x_percentage      = $this->input->post("marks_x_percentage");
		$x_division      = $this->input->post("x_division");
		$marks_x_cgpa      = $this->input->post("marks_x_cgpa");
		
		
		$school_12    = $this->input->post("school_12");
		$board_12     = $this->input->post("board_12");
		$year_pass_12 = $this->input->post("year_pass_12");
		$xii_marks_type      = $this->input->post("xii_marks_mode");
		$marks_xii_percentage      = $this->input->post("marks_xii_percentage");
		$xii_division      = $this->input->post("xii_division");
		$marks_xii_cgpa      = $this->input->post("marks_xii_cgpa");
		
		
		$degree       = $this->input->post("degree");
		$degree_array = json_encode($degree);
		$yearstart    = $this->input->post("yearstart");
		$yearstart_array = json_encode($yearstart);
		$yearend         = $this->input->post("yearend");
		$yearend_array   = json_encode($yearend);
		$college         = $this->input->post("college");
		$college_array   = json_encode($college);
		$university      = $this->input->post("university");
		$university_array = json_encode($university);
		$division         = $this->input->post("division");
		$division_array   = json_encode($division);
		
		



		$data = array(
			'school_10' => $school_10,
			'board_10' => $board_10,
			'year_pass_10' => $year_pass_10,
			'x_marks_type' => $x_marks_type,
			'marks_x_percentage' => $marks_x_percentage,
			'x_division' => $x_division,
			'marks_x_cgpa' => $marks_x_cgpa,
			'school_12' => $school_12,
			'board_12' => $board_12,
			'year_pass_12' => $year_pass_12,
			'xii_marks_type' => $xii_marks_type,
			'marks_xii_percentage' => $marks_xii_percentage,
			'xii_division' => $xii_division,
			'marks_xii_cgpa' => $marks_xii_cgpa,
			'degree_array' => $degree_array,
			'yearstart_array' => $yearstart_array,
			'yearend_array' => $yearend_array,
			'college_array' => $college_array,
			'university_array' => $university_array,
			'division_array' => $division_array,
			'verification_level' => 3
		);
		$verification_level = array(
			'verification_level' => 3
		);
		
	

	

		$coreprofile = $this->home_model->step3coreprofile($data,$userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level,$userid);
		
		if($coreprofile){
			$result['status'] = true;
			echo json_encode($result);
		}


		
	}
	public function step33(){
		$userid = $this->user->info->ID;
		$hour_charges=$this->input->post("hour_charges");
		$telephone_charges=$this->input->post("telephone_charges");
		$email_charges=$this->input->post("email_charges");
		$apperance_charges=$this->input->post("apperance_charges");
		$haur_draft_charges=$this->input->post("haur_draft_charges");
		

		$data = array(
			'hour_charges'=>$hour_charges,
			'telephone_charges'=>$telephone_charges,
			'email_charges'=>$email_charges,
			'apperance_charges'=>$apperance_charges,
			'haur_draft_charges'=>$haur_draft_charges,
			'verification_level' => 3
		   );
		$verification_level = array(
			'verification_level' => 3
		); 

		$coreprofile = $this->home_model->step3coreprofile($data,$userid);
		$upadte_verification_level = $this->home_model->update_user_level($verification_level,$userid);
		
		if($coreprofile){
			$result['status'] = true;
			echo json_encode($result);
		}


		
	}
	public function step4(){
		$userid = $this->user->info->ID;
		
		$office_consultation_type = $this->input->post("office_consultation_type");
		$office_charges_checkbox_30_min = $this->input->post("office_charges_checkbox_30_min");
		$office_charges_checkbox_1hour = $this->input->post("office_charges_checkbox_1hour");
		$office_charges_30min = $this->input->post("office_charges_30min");
		$office_charges_1hour = $this->input->post("office_charges_1hour");
		
		$site_consultation_type = $this->input->post("site_consultation_type");
		$site_charges_checkbox_30_min = $this->input->post("site_charges_checkbox_30_min");
		$site_charges_checkbox_1hour = $this->input->post("site_charges_checkbox_1hour");
		$site_charges_30min = $this->input->post("site_charges_30min");
		$site_charges_1hour = $this->input->post("site_charges_1hour");
		
		$telvoice_consultation_type = $this->input->post("telvoice_consultation_type");
		$telvoice_charges_checkbox_30_min = $this->input->post("telvoice_charges_checkbox_30_min");
		$telvoice_charges_checkbox_1hour = $this->input->post("telvoice_charges_checkbox_1hour");
		$telvoice_charges_30min = $this->input->post("telvoice_charges_30min");
		$telvoice_charges_1hour = $this->input->post("telvoice_charges_1hour");
		
		$telvideo_consultation_type = $this->input->post("telvideo_consultation_type");
		$telvideo_charges_checkbox_30_min = $this->input->post("telvideo_charges_checkbox_30_min");
		$telvideo_charges_checkbox_1hour = $this->input->post("telvideo_charges_checkbox_1hour");
		$telvideo_charges_30min = $this->input->post("telvideo_charges_30min");
		$telvideo_charges_1hour = $this->input->post("telvideo_charges_1hour");
	
		$email_consultation_type = $this->input->post("email_consultation_type");
		$email_consultation_charges = $this->input->post("email_consultation_charges");
		$travel_consultation = $this->input->post("travel_consultation");
		$apperance_charges = $this->input->post("apperance_charges");
		$haur_draft_charges = $this->input->post("haur_draft_charges");
	
		$data = [
			'office_consultation_type' => $office_consultation_type,
			'office_charges_checkbox_30_min' => $office_charges_checkbox_30_min,
			'office_charges_checkbox_1hour' => $office_charges_checkbox_1hour,
			'office_charges_30min' => $office_charges_30min,
			'office_charges_1hour' => $office_charges_1hour,
			
			'site_consultation_type' => $site_consultation_type,
			'site_charges_checkbox_30_min' => $site_charges_checkbox_30_min,
			'site_charges_checkbox_1hour' => $site_charges_checkbox_1hour,
			'site_charges_30min' => $site_charges_30min,
			'site_charges_1hour' => $site_charges_1hour,
			
			'telvoice_consultation_type' => $telvoice_consultation_type,
			'telvoice_charges_checkbox_30_min' => $telvoice_charges_checkbox_30_min,
			'telvoice_charges_checkbox_1hour' => $telvoice_charges_checkbox_1hour,
			'telvoice_charges_30min' => $telvoice_charges_30min,
			'telvoice_charges_1hour' => $telvoice_charges_1hour,
			
			'telvideo_consultation_type' => $telvideo_consultation_type,
			'telvideo_charges_checkbox_30_min' => $telvideo_charges_checkbox_30_min,
			'telvideo_charges_checkbox_1hour' => $telvideo_charges_checkbox_1hour,
			'telvideo_charges_30min' => $telvideo_charges_30min,
			'telvideo_charges_1hour' => $telvideo_charges_1hour,
			
			'email_consultation_type' => $email_consultation_type,
			'email_consultation_charges' => $email_consultation_charges,
			'travel_consultation' => $travel_consultation,
			'apperance_charges' => $apperance_charges,
			'haur_draft_charges' => $haur_draft_charges,
			'verification_level' => 4
		];	
		
		
		
		

		
		// $verification_level = array(
		// 	'verification_level' => 4
		// ); 
		// $points = $this->user->info->points;
        // $bonus_points = $this->user->info->bonus_points;
        // $total_points = $this->user->info->total_points;
        // $cal_points = $points+100;
        // $cal_bonus_points = $bonus_points+100;
        // $cal_total_points = $total_points+100;
		
		$coreprofile = $this->home_model->step4coreprofile($data,$userid);
		
		// $updatepoints = $this->home_model->updatepoints($userid,array(
		// 	"points" => $cal_points,
		// 	"bonus_points" => $cal_bonus_points,
		// 	"total_points" => $cal_total_points,
		// ));
		
		// $this->page_model->add_history(array(
		// 	"userid" => $userid,
		// 	"notes" => "Profile Completion",
		// 	"points" => 100
		// 	)
		// );
		
		//update user address in social db table(user)
		
		$update_address = $this->user_model->update_user($this->user->info->ID, $address_data);
		
		//update user address in lawyer db table(user)

		// $ch = curl_init();
		
        // $data2 = 'id='.$this->user->info->ID.'&zipcode='.$zipcode.'&country='.$location_country.'&state='.$location_state.'&city='.$location_city.'&address='.$location_address;
		
		// $profile_tmp_url = str_replace("/social","",base_url());
		// $profile_url = $profile_tmp_url."api/update_laravel_profile_address.php";	
		

		// curl_setopt($ch, CURLOPT_URL, $profile_url);
		// curl_setopt($ch, CURLOPT_HEADER, 0);
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS,$data2);

		

		// curl_exec($ch);
		  
       
		
		// curl_close($ch);
	
		
		$this->session->set_flashdata("globalmsg", "Your profile has been updated successfully.100 free points credited to your account.");
		
		if($coreprofile){
			$result['status'] = true;
			echo json_encode($result);
		}


		
	}

	public function step5(){
	
		$userid = $this->user->info->ID;

		$res_address1 = "";
		$res_address2 = "";
	   	$res_address3 = "";
	    $res_location_city     = "";
	    $res_location_state    = "";
	    $res_location_country  = "";
	    $res_lat = "";
	    $res_lng = "";
	    $res_pincode = "";


		$address1 = $this->common->nohtml($this->input->post("address1"));
		$address2 = $this->common->nohtml($this->input->post("address2"));
	   	$address3 = $this->common->nohtml($this->input->post("address3"));
	    $location_city = $this->common->nohtml($this->input->post("location_city"));
	    $location_state = $this->common->nohtml($this->input->post("location_state"));
	    $location_country = $this->common->nohtml($this->input->post("location_country"));
	    $lat = $this->common->nohtml($this->input->post("lat"));
	    $lng = $this->common->nohtml($this->input->post("lng"));
	    $pincode = $this->input->post("pincode");
		
		$res_address1 = $this->common->nohtml($this->input->post("res_address1"));
		$res_address2 = $this->common->nohtml($this->input->post("res_address2"));
	   	$res_address3 = $this->common->nohtml($this->input->post("res_address3"));
	    $res_location_city = $this->common->nohtml($this->input->post("res_location_city"));
	    $res_location_state = $this->common->nohtml($this->input->post("res_location_state"));
	    $res_location_country = $this->common->nohtml($this->input->post("res_location_country"));
	    $res_lat = $this->common->nohtml($this->input->post("res_lat"));
	    $res_lng = $this->common->nohtml($this->input->post("res_lng"));
		$res_pincode = $this->input->post("res_pincode");
		
		
		$address_data = array(
			'address_1' => $address1,
			'address_2' => $address2,
			'address3' => $address3,
			'city' => $location_city,
			'state' => $location_state,
			'country' => $location_country,
			'lat' => $lat,
			'longitude' => $lng,
			'LatitudeLongitude'=>$lat.",".$lng,
			'zipcode' => $pincode,
			'res_address1' => $res_address1,
			'res_address2' => $res_address2,
			'res_address3'  => $res_address3,
			'res_city'      => $res_location_city,
			'res_state'     => $res_location_state,
			'res_country'   => $res_location_country,
			'res_lat'       => $res_lat,
			'res_lng' => $res_lng,
			'res_LatitudeLongitude'=>$res_lat.",".$res_lng,
			'res_zipcode' => $res_pincode,
			'verification_level' => 5
		);

		$data = array(
			'verification_level'=>5,
		);
		
		$coreprofile = $this->home_model->updatecoreprofile($data,$userid);
		$update_address = $this->user_model->update_user($this->user->info->ID, $address_data);
		
		
		
		if($coreprofile){
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		}
	}
	
	public function step55(){
		$upload_profile_pic = "";
		$upload_cover_pic = "" ;
		$upload_office_pic1 = "" ;
		$upload_office_pic2 = "" ;
		$upload_office_pic3 = "" ;
		$upload_office_pic4 = "" ;
		
		$userid = $this->user->info->ID;


		if(isset($_FILES['profile_pic']) && $_FILES['profile_pic']['size'] > 0){
			$upload_profile_pic_response = $this->upload_pic('profile_pic');
			if($upload_profile_pic_response['status'] == 1){
				$upload_profile_pic = $upload_profile_pic_response['message'];
			}	
		}
		if(isset($_FILES['cover_pic']) && $_FILES['cover_pic']['size'] > 0){
			$upload_cover_pic_response = $this->upload_pic('cover_pic');
			if($upload_cover_pic_response['status'] == 1){
				$upload_cover_pic = $upload_cover_pic_response['message'];
			}	
		}
		if(isset($_FILES['office_picture1']) && $_FILES['office_picture1']['size'] > 0){
			$upload_office_pic1_response = $this->upload_pic('office_picture1');
			if($upload_office_pic1_response['status'] == 1){
				$upload_office_pic1 = $upload_office_pic1_response['message'];
			}	
		}
		if(isset($_FILES['office_picture2']) && $_FILES['office_picture2']['size'] > 0){
			$upload_office_pic2_response = $this->upload_pic('office_picture2');
			if($upload_office_pic2_response['status'] == 1){
				$upload_office_pic2 = $upload_office_pic2_response['message'];
			}	
		}
		if( isset($_FILES['office_picture3']) && $_FILES['office_picture3']['size'] > 0){
			$upload_office_pic3_response = $this->upload_pic('office_picture3');
			if($upload_office_pic3_response['status'] == 1){
				$upload_office_pic3 = $upload_office_pic3_response['message'];
			}	
		}
		if( isset($_FILES['office_picture4']) && $_FILES['office_picture4']['size'] > 0){
			$upload_office_pic4_response = $this->upload_pic('office_picture4');
			if($upload_office_pic4_response['status'] == 1){
				$upload_office_pic4 = $upload_office_pic4_response['message'];
			}	
		}
			
		$data = array(
			'verification_level'=>5,
			'office_picture1' => $upload_office_pic1,
			'office_picture2' => $upload_office_pic2,
			'office_picture3' => $upload_office_pic3,
			'office_picture4' => $upload_office_pic4,
		);
		
		$user_data = array(
			'avatar'=>$upload_profile_pic,
			'profile_header'=>$upload_cover_pic,
			'verification_level' => 5
		); 
		
		$coreprofile = $this->home_model->updatecoreprofile($data,$userid);
		$upadte_verification_level = $this->home_model->update_user_level($user_data,$userid);
		
		if($coreprofile){
			$response['status_code'] = true;
			$response['message'] = "ok";
			echo json_encode($response);
		}
	}
	
	
	private function uploadstatebarform(){
		if($_FILES['councelregform']['size'] > 0){
			$settings = array(
				"upload_path" => $this->settings->info->upload_path, 
				"overwrite" => FALSE,
				"max_filename" => 300,
				"encrypt_name" => TRUE,
				"remove_spaces" => TRUE,
				"allowed_types" => $this->settings->info->file_types,
				"max_size" => $this->settings->info->file_size,
			);
			$this->load->library("upload");
			$this->upload->initialize($settings);
			$this->load->library('upload', $settings);	

			$res = [];
			if ( ! $this->upload->do_upload('councelregform'))
			{
				$error = array('error' => $this->upload->display_errors());
				$res['status'] = 0;	
				$res['message'] = "The upload path does not appear to be valid";	
				// $this->load->view('upload_form', $error);
			}else{
				$data = array('upload_data' => $this->upload->data());
				$res['status'] = 1;	
				$res['message'] = $data['upload_data']['file_name'];	
				// $this->load->view('upload_success', $data);
			}
			return $res;
		}
	}
	
	// private function uploadbarassoform(){
	// 	if($_FILES['barregform']['size'] > 0){
	// 		$settings = array(
	// 			"upload_path" => $this->settings->info->upload_path,
	// 			"overwrite" => FALSE,
	// 			"max_filename" => 300,
	// 			"encrypt_name" => TRUE,
	// 			"remove_spaces" => TRUE,
	// 			"allowed_types" => $this->settings->info->file_types,
	// 			"max_size" => $this->settings->info->file_size,
	// 		);
	// 		$this->load->library("upload");
	// 		$this->upload->initialize($settings);
	// 		$this->load->library('upload', $settings);	

	// 		$res = [];
	// 		if ( ! $this->upload->do_upload('barregform'))
	// 		{
	// 			$error = array('error' => $this->upload->display_errors());
	// 			$res['status'] = 0;	
	// 			$res['message'] = $error;	
	// 		}else{
	// 			$data = array('upload_data' => $this->upload->data());
	// 			$res['status'] = 1;	
	// 			$res['message'] = $data['upload_data']['file_name'];	
	// 		}

	// 		return $res;
	// 	}	

	// }
	
	private function uploadbarassoform($no){
		$pic_1 = "";
		if($no == 0){
			$pic_1 = $_FILES['barregform_0']['size'];	
		}
		if($no == 1){
			$pic_1 = $_FILES['barregform_1']['size'];	
		}
		
		if($pic_1 > 0){
			$settings = array(
				"upload_path" => $this->settings->info->upload_path,
				"overwrite" => FALSE,
				"max_filename" => 300,
				"encrypt_name" => TRUE,
				"remove_spaces" => TRUE,
				"allowed_types" => $this->settings->info->file_types,
				"max_size" => $this->settings->info->file_size,
			);
			$this->load->library("upload");
			$this->upload->initialize($settings);
			$this->load->library('upload', $settings);	

			$res = [];
			if ( ! $this->upload->do_upload('barregform_'.$no))
			{
				$error = array('error' => $this->upload->display_errors());
				$res['status'] = 0;	
				$res['message'] = $error;	
			}else{
				$data = array('upload_data' => $this->upload->data());
				$res['status'] = 1;	
				$res['message'] = $data['upload_data']['full_path'];	
			}

			return $res;
		}	

	}
	public function get_user_friends_v2(){
		$query2 = $this->common->nohtml($this->input->get("term"));
		$users = $this->user_model->get_friend_names($query2, $this->user->info->ID);
		$usersArr = array();
		foreach($users->result() as $user) {
			$u = new STDClass;
			$u->id = $user->username;
			$u->text = $user->first_name . " " . $user->last_name;
			$usersArr[] = $u;
		}
		header('Content-Type: application/json');
		echo json_encode(array("results" => $usersArr));
		exit();
	}

	private function get_fresh_results($stats) 
	{
		$data = new STDclass;
		$data->google_members = $this->user_model->get_oauth_count("google");
		$data->facebook_members = $this->user_model->get_oauth_count("facebook");
		$data->twitter_members = $this->user_model->get_oauth_count("twitter");
		$data->total_members = $this->user_model->get_total_members_count();
		$data->new_members = $this->user_model->get_new_today_count();
		$data->active_today = $this->user_model->get_active_today_count();
		return $data;
	}

	public function change_language() 
	{	

		$languages = $this->config->item("available_languages");
		if(!isset($_COOKIE['language'])) {
			$lang = "";
		} else {
			$lang = $_COOKIE["language"];
		}
		$this->template->loadContent("home/change_language.php", array(
			"languages" => $languages,
			"user_lang" => $lang
			)
		);
	}


	public function applymoderator(){
		if(!$this->user->loggedin) {
				redirect(site_url("login"));
		}
		
		

		if(empty($this->user->info->moderator_apply)):
		$this->user_model->update_user($this->user->info->ID, array(
				"moderator_apply" => 1,
				)
			);
	
		$this->session->set_flashdata("globalmsg", "You have successfully applied for Moderator..");

		return redirect(site_url());
		
		else:
		$this->session->set_flashdata("globalmsg", "You have Already applied for Moderator..Please Wait for approval usually it takes 48 hrs for approval.");

		return redirect(site_url());
		endif;
		
		
		}

	public function change_language_pro() 
	{

		$lang = $this->common->nohtml($this->input->post("language"));
		$languages = $this->config->item("available_languages");
		
		if(!array_key_exists($lang, $languages)) {
			$this->template->error(lang("error_25"));
		}

		setcookie("language", $lang, time()+3600*7, "/");
		$this->session->set_flashdata("globalmsg", lang("success_14"));
		redirect(site_url());
	}

	public function get_usernames() 
	{
		$query = $this->common->nohtml($this->input->get("query"));

		if(!empty($query)) {
			$usernames = $this->user_model->get_usernames($query);
			if($usernames->num_rows() == 0) {
				echo json_encode(array());
			} else {
				$array = array();
				foreach($usernames->result() as $r) {
					

					$array[] = $r->username;
					

				}

				

				echo json_encode($array);
				exit();
			}
		} else {
			echo json_encode(array());
			exit();
		}
	}

	public function get_search_results() 
	{
		$query = $this->common->nohtml($this->input->get("query"));

		$array = array();
		if(!empty($query)) {
			$usernames = $this->user_model->get_user_by_name($query);
			if($usernames->num_rows() == 0) {
			} else {
				foreach($usernames->result() as $r) {
					$s = new STDClass;
					$s->label = $r->first_name ." " . $r->last_name;
					$s->type = "user";
					$s->value = $r->username;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->avatar;
					$s->url = site_url("profile/" . $r->username);
					$array[] = $s;
				}
			}
			// Search pages
			$pages = $this->page_model->get_pages_by_name($query);
			
			if($pages->num_rows() == 0) {
			} else {
				foreach($pages->result() as $r) {
					if(!empty($r->slug)) {
						$slug = $r->slug;
					} else {
						$slug = $r->ID;
					}
					$s = new STDClass;
					$s->label = $r->name;
					$s->type = "page";
					$s->value = $r->name;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->profile_avatar;
					$s->url = site_url("pages/view/" . $slug);
					$array[] = $s;
				}
			}
		}
		echo json_encode($array);
		exit();
	}



	public function get_search_resultssss() 
	{
		$query = $this->common->nohtml($this->input->get("query"));

		$array = array();
		if(!empty($query)) {
			$usernames = $this->user_model->get_user_by_name($query);
			if($usernames->num_rows() == 0) {
			} else {
				foreach($usernames->result() as $r) {
					$s = new STDClass;
					$s->label = $r->first_name ." " . $r->last_name;
					$s->type = "user";
					$s->value = $r->username;
					$s->avatar = base_url() . $this->settings->info->upload_path_relative . "/" . $r->avatar;
					$s->url = site_url("profile/" . $r->username);
					$array[] = $s;
				}
			}
			// Search pages
			
		}
		echo json_encode($array);
		exit();
	}

	public function get_names() 
	{
		$query = $this->common->nohtml($this->input->get("query"));
		 $CI =& get_instance();
		if(!empty($query)) {
			$usernames = $this->user_model->get_names($query);
			if($usernames->num_rows() == 0) {
				echo json_encode(array());
			} else {
				$array = array();
				foreach($usernames->result() as $r) {
					$u = new STDClass();
					$u->label = $r->first_name ." ".$r->last_name;
					$u->value = $r->ID;
					$u->url=base_url().$CI->settings->info->upload_path_relative.'/'.$r->avatar;
					$array[] = $u;
				}
				
				echo json_encode($array);
				exit();
			}
		} else {
			echo json_encode(array());
			exit();
		}
	}

	public function load_notifications() 
	{
		$notifications = $this->user_model
			->get_notifications($this->user->info->ID);
		$this->template->loadAjax("home/ajax_notifications.php", array(
			"notifications" => $notifications
			),0
		);	
	}

	public function load_notifications_unread() 
	{
		$notifications = $this->user_model
			->get_notifications_unread($this->user->info->ID);
		$this->template->loadAjax("home/ajax_notifications.php", array(
			"notifications" => $notifications
			),0
		);	
	}

	public function read_all_noti($hash) 
	{
		if($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		
		$this->user_model->update_user_notifications($this->user->info->ID, array(
			"status" => 1
			)
		);

		$this->user_model->update_user($this->user->info->ID, array(
			"noti_count" => 0
			)
		);

		$this->session->set_flashdata("globalmsg", lang("success_43"));
		redirect(site_url("home/notifications"));
	}


	public function clear_all_noti($hash){
		if($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}



	$this->user_model->delete_user_notifications($this->user->info->ID);

	$this->session->set_flashdata("globalmsg", lang("success_118"));
		redirect(site_url("home/notifications"));

	}

	public function load_notification($id)
	{
		$notification = $this->user_model
			->get_notification($id, $this->user->info->ID);
		if($notification->num_rows() == 0) {
			$this->template->error(lang("error_111"));
		}
		$noti = $notification->row();
		if(!$noti->status) {
			$this->user_model->update_notification($id, array(
				"status" => 1
				)
			);
			$this->user_model->update_user($this->user->info->ID, array(
				"noti_count" => $this->user->info->noti_count - 1
				)
			);
		}

		// redirect
		redirect(site_url($noti->url));
	}

	public function notifications() 
	{
		$this->template->loadContent("home/notifications.php", array(
			)
		);	
	}

	public function notification_read($id) 
	{
		$notification = $this->user_model
			->get_notification($id, $this->user->info->ID);
		if($notification->num_rows() == 0) {
			$this->template->error(lang("error_111"));
		}
		$noti = $notification->row();
		if(!$noti->status) {
			$this->user_model->update_notification($id, array(
				"status" => 1
				)
			);
			$this->user_model->update_user($this->user->info->ID, array(
				"noti_count" => $this->user->info->noti_count - 1
				)
			);
		}
		redirect(site_url("home/notifications"));
	}

	public function notification_unread($id) 
	{
		$notification = $this->user_model
			->get_notification($id, $this->user->info->ID);
		if($notification->num_rows() == 0) {
			$this->template->error(lang("error_111"));
		}
		$noti = $notification->row();
		if($noti->status) {
			$this->user_model->update_notification($id, array(
				"status" => 0
				)
			);
			$this->user_model->update_user($this->user->info->ID, array(
				"noti_count" => $this->user->info->noti_count + 1
				)
			);
		}
		redirect(site_url("home/notifications"));
	}

	public function delete_notification($id, $hash) 
	{
		if($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$notification = $this->user_model
			->get_notification($id, $this->user->info->ID);
		if($notification->num_rows() == 0) {
			$this->template->error(lang("error_113"));
		}
		$noti = $notification->row();
		if($noti->status) {
			
			$this->user_model->update_user($this->user->info->ID, array(
				"noti_count" => $this->user->info->noti_count + 1
				)
			);
		} else {
			$this->user_model->update_user($this->user->info->ID, array(
				"noti_count" => $this->user->info->noti_count - 1
				)
			);
		}

		$this->user_model->delete_notification($id);
		$this->session->set_flashdata("globalmsg", lang("success_101"));
		redirect(site_url("home/notifications"));
	}

	public function notifications_page() 
	{
		$this->load->library("datatables");

		$this->datatables->set_default_order("user_notifications.timestamp", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				 2 => array(
				 	"user_notifications.timestamp" => 0
				 )
			)
		);
		$this->datatables->set_total_rows(
			$this->user_model
			->get_notifications_all_total($this->user->info->ID)
		);
		$notifications = $this->user_model
			->get_notifications_all($this->user->info->ID, $this->datatables);



		foreach($notifications->result() as $r) {
			$msg = '<a href="'.site_url("profile/" . $r->username).'">'.$r->username.'</a> ' . $r->message;
			$options = '<a href="'.site_url("home/notification_unread/" . $r->ID).'" class="btn btn-default btn-xs">'.lang("ctn_655").'</a>';
			if($r->status !=1) {
				$msg .=' <label class="label label-danger">'.lang("ctn_796").'</label>';
				$options = '<a href="'.site_url("home/notification_read/" . $r->ID).'" class="btn btn-info btn-xs">'.lang("ctn_656").'</a>';
			}

			$this->datatables->data[] = array(
				$this->common->get_user_display(array("username" => $r->username, "avatar" => $r->avatar, "online_timestamp" => $r->online_timestamp)),
				$msg,
				date($this->settings->info->date_format, $r->timestamp),
				$options . ' <a href="'.site_url("home/delete_notification/" . $r->ID . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> <a href="'.site_url("home/load_notification/" . $r->ID).'" class="btn btn-primary btn-xs">'.lang("ctn_657").'</a>'
			);
		}
		echo json_encode($this->datatables->process());
	}


	private function get_languages(){
		$lang = $this->user_model->get_all_languages();
		return $lang;
	}
	private function get_courts(){
		$courts = $this->home_model->get_all_courts();
		return $courts;
	}

	public function getcitybystate(){
		$state_id = $this->input->post('state_id');
		$get_city = $this->home_model->get_city_by_state_id($state_id);
		$get_city  = $get_city->result();
		
		echo json_encode($get_city);
	}

}

?>
