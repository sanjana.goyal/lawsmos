<?php defined('BASEPATH') OR exit('No direct script access allowed');
class PdfController extends CI_Controller {

	public function __construct() 
	{
		
		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("home_model");
		$this->load->model("blog_model");
		$this->load->model("page_model");
		
		$this->load->model("article_model");
		$this->load->model("calendar_model");
		
		$this->load->model("login_model");
		$this->load->model("register_model");
		$this->load->helper('custom_helper');

	}
	public function index(){
		$userid = $this->user->info->ID;
		// echo $userid;die;
		$userid = intval($userid);
		$user = $this->user_model->get_user_by_id($userid);
		$user = $user->row();

		$profile = $this->user_model->get_user_core_profile($userid,$userid->user_role);
		
		
        
		$this->load->library('pdf');
		$this->pdf->load_view('user_list',$user,$profile,1);

	}
	public function preview(){
		$userid = $this->user->info->ID;
		// echo $userid;die;
		$userid = intval($userid);
		$user = $this->user_model->get_user_by_id($userid);
		$user = $user->row();

		$profile = $this->user_model->get_user_core_profile($userid,$userid->user_role);
		
		
        
		$this->load->library('pdf');
		$this->pdf->load_view('user_list',$user,$profile,0);

	}
}