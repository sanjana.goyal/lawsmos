<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ratings extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		$this->load->model("home_model");
		$this->load->model("article_model");
		$this->load->model("login_model");
		$this->load->model("register_model");
		// If the user does not have premium. 
		// -1 means they have unlimited premium.
		$this->template->set_layout("client/themes/titan.php");
	}
	
	
	public function view_appointment_history()
	{
			
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
			}
			$appointments = $this->user_model->get_rating_profiles($this->user->info->ID);
			$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
			
			$this->template->loadContent("ratings/appointmenthistory.php", array(
				"appointments"=>$appointments,
				'getprofile'=>$getprofile
			));
	
	}
	
	
	
	 function fetch()
	 {
		echo $this->user_model->html_output($this->user->info->ID);
	 }
	
	
	function updateRating()
	{
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
			}
		$lawyerid=$_GET['lawyerid'];
		$seekerid=$_GET['seekerid'];
		$rating=$_GET['rating'];
		$lawyerdetail=$this->user_model->get_user_by_id($lawyerid);
		$detail=$lawyerdetail->row();
		
		if($rating==5)
		{

					$points=$detail->points;
					$bonus_points=$detail->bonus_points;
					$total_points=$detail->total_points;
					$cal_points=$points+100;
					$cal_bonus_points=$bonus_points+100;
					$cal_total_points=$total_points+100;

					$updatepoints=$this->home_model->updatepoints($lawyerid,array(
								"points"=>$cal_points,
								"bonus_points"=>$cal_bonus_points,
								"total_points"=>$cal_total_points,
							));

					$this->page_model->add_history(array(
								"userid" => $lawyerid,
								"notes" => "5 Star Rating",
								"points" => 100,
								)
							);	
		
			 $averageRating = $this->user_model->userRating($lawyerid,$seekerid,$rating);
			 echo $averageRating;
			 exit;
		}
		else{
				 // Update user rating and get Average rating of a post
			$averageRating = $this->user_model->userRating($lawyerid,$seekerid,$rating);
			echo $averageRating;
			exit;
		}
	}

	
	public function updateReview()
	{
		
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
			}
		$lawyerid=$_GET['lawyerid'];
		$seekerid=$_GET['seekerid'];
	 	$reviewtext=$_GET['reviewtext'];
	 	
	 	
		$lawyerratingvalue=$this->user_model->get_rating_value($lawyerid);
		$lawyerdetail=$this->user_model->get_user_by_id($lawyerid);
		$detail=$lawyerdetail->row();
		
		if($lawyerratingvalue==5.0){
					
					$points=$detail->points;
					$bonus_points=$detail->bonus_points;
					$total_points=$detail->total_points;
					$cal_points=$points+50;
					$cal_bonus_points=$bonus_points+50;
					$cal_total_points=$total_points+50;

					 $updatepoints=$this->home_model->updatepoints($lawyerid,array(
								"points"=>$cal_points,
								"bonus_points"=>$cal_bonus_points,
								"total_points"=>$cal_total_points,
							));

					$this->page_model->add_history(array(
								"userid" => $lawyerid,
								"notes" => "Review on Profile",
								"points" => 50
								)
							);	
			
				$updaterecord = $this->user_model->updatereview($lawyerid,$seekerid,$reviewtext);
					echo $updaterecord;
					exit;
			}else{
	
			$updaterecord = $this->user_model->updatereview($lawyerid,$seekerid,$reviewtext);
			echo $updaterecord;
			exit;
		
	}

}

}
?>
