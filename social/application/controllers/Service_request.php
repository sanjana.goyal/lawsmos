<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_request extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->helper('custom_helper');
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		$this->load->model("home_model");
		
		$this->load->model("login_model");
		
		
		$this->load->model("register_model");
		
		
		
		
		
		
		
		
		$this->load->model("home_model");
		$this->load->model("article_model");
		
		
		
		$this->template->loadData("activeLink", 
			array("home" => array("general" => 1)));
		// If the user does not have premium. 
		// -1 means they have unlimited premium
		

		$this->template->set_layout("client/themes/titan.php");
		$this->template->set_error_view("error/login_error.php");
	}

	public function index()
	{
		
		
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		$userid = $this->user->info->ID;
	$user = $this->user_model->get_user_by_id($userid);
		if($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}
	   
	 
		$this->template->loadContent("service_request/index.php", array(
			
				"user" => $user,
			
			)
			
		);
	}
	
	
	public function action(){
		
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		$catid=$_GET['categoryId'];
	
		$query = $this->db->get_where('catagories', array('parent_id' => $catid));
		$data1['categories'] = $query->result();
		
		if(!empty($query->result())){
		//print_r($data1['categories']);
		$i=1;
		
		foreach($query->result() as $rows){
		
		?> 
		
			<div class="step-1-content-danger">
				
				
					<?php
				if($i=='1')
				{
				?>
				<input type="radio"  value="<?php echo $rows->cid."_".$rows->c_name; ?>" name="subissue_id" id="<?php echo "child_cat.".$rows->cid ?>" checked  />
				<?php
			}
			else
			{
				?>
				<input type="radio"  value="<?php echo $rows->cid."_".$rows->c_name; ?>" name="subissue_id" id="<?php echo "child_cat.".$rows->cid ?>"  />
			<?php
			}
			?>
				
				
				<label for="<?php echo "child_cat.".$rows->cid ?>"><?php echo $rows->c_name; ?></label>
				
			</div>
			
			<?php
		
            $i++;
			}
		}
           
	
		
		
		}
		
	public function save(){
		
		
			if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
			
			$issue=$this->input->post('issue_id');
			//$issuearray=explode("_",$issueid);
			//$issue=$issuearray[1];
			$subissue=$this->input->post('subissue_id');
			//$subissuearray=explode("_",$subissueid);
			//$subissue=$subissuearray[1];
			$action=$this->input->post('action_list');
			$tellus=$this->input->post('tellus');
			$phone=$this->input->post('phone');
			$name=$this->input->post('name');
			$email=$this->input->post('email');
			$calltime=$this->input->post('calltime');
			$location=$this->input->post('location');
			
			$locationss=explode(",",$location);
			
			$locationsss=$locationss[0];
			
			$shortdescription=$this->input->post('shortdescription');
			$recieve_emails=$this->input->post('emailtime');
			
			
			
	 $data = array(
        'issue'=>$issue,
        'subissue'=>$subissue,
        'action'=>$action,
        'tellus'=>$tellus,
        'phone'=>$phone,
        'name'=>$name,
        'email'=>$email,
        'calltime'=>$calltime,
        'location'=>$locationsss,
        'shortdescription'=>$shortdescription,
        'recieve_emails'=>$recieve_emails,
    );
		
	$run=$this->db->insert('legal_leads',$data);
	
	
	if($run==true){
	
		$this->session->set_flashdata('globalmsg', 'Service Request Generated Successfully');
		redirect(site_url());	
	}
	else{
		
			$this->session->set_flashdata('globalmsg', 'Error , Service request could not generete at this moment ! Plesae try again later');
			redirect(site_url());	
		}
	
}
		
		
		

public function serviceleads($type = 0, $hashtag = "")
{
	 
	
	$userid = $this->user->info->ID;

	$user = $this->user_model->get_user_by_id($userid);
		if($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
	}
	
	
		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile=$this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location=$this->user->info->city;
		$location=explode(",",$location);
		$city=$location[0];
		$leads=$this->home_model->get_leads($city);
		$countmessage=$this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview=$this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview=$getarticlesforreview->result_array();
	
	
	
	$user = $user->row();

	$this->template->loadContent("service_request/leads.php", array(
			"user" => $user,
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd"=>$topviewd,
			"getprofile"=>$getprofile,
			"leads"=>$leads,
			"countmessage"=>$countmessage,
			"getarticlesforreview"=>$getarticlesforreview,
	));
	
}



public function unpublishlead($id){

	$leads = $this->user_model->unpublishlead($id);
	$this->session->set_flashdata('globalmsg', 'Lead unpublished successfully.');
	redirect(site_url('leads'));
	}
public function viewleadss($id,$lead_price){

 //$this->user->info->ID;

	$userid = $this->user->info->ID;
	// $leads = $this->user_model->view_leads($id);
	$leads = $this->user_model->view_leads_detail($id);
	$result=$leads->result();
	
	$lead_view_count = $this->user_model->countleadview($id);
	$lead_view_limit = explode("-",$result[0]->lawyer_max)[1];
	
	
	//if lead view is equal to lead view limit than lead status change 
	if($lead_view_count == $lead_view_limit  ){
		$data = array(
			'status' => 2
		);

		$update_lead_status = $this->user_model->update_lead_status($id,$data);
	}

	$cat=$result[0]->issue;
	$subcat=$result[0]->subissue;
	$catid=explode("_",$cat);
	$catid=$catid[0];

	$catprice=$this->user_model->view_cat_price($catid);

	if(!empty($lead_price)){
		$price = $lead_price;
	}
	// $price=$catprice[0]->price;

	$points=$this->user->info->points;
	

	$logcount=$this->user_model->countleadlogs($userid,$id);

  	if($logcount==0){

  		if($points>=$price){


			$balance=$points-$price;

			$deductbalance=$this->user_model->deductbalance($this->user->info->ID,$balance,$id,$price);
 
 
	
			$date=date("Y-m-d h:i:sa");
			$data=array(

				"leadid"=>$id,
				"userid"=>$userid,
				"date"=>$date,

			);

		 	$insertlog=$this->user_model->insertlogs($data);


    		$this->template->loadContent("service_request/viewleads.php", array(
			
				"leads" => $result,
				"price"=>$price,
				"balance"=>$balance,
			));

		}
		else{
			$this->session->set_flashdata('globalmsg', 'Insufficient funds , please recharge your account...');
			redirect(site_url());	
		}
	}
	else{
		

		$this->template->loadContent("service_request/viewfreeleads.php", array(
			
				"leads" => $result,
		));

}
//print_r($leads->result());

//echo "Points has been deducted , Remaining balance is".$this->user->info->points;

}

public function myleads($city)
{
	$userid = $this->user->info->ID;

	$c=explode(",",$city);
	
	 $cityy=$c[0];
	
	//echo $city;
	if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		

		$this->load->library("datatables");

		$this->datatables->set_default_order("legal_leads.id", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				 1 => array(
				 	"legal_leads.issue" => 0
				 ),
				 2 => array(
				 	"legal_leads.subissue" => 0
				 ),
				 3 => array(
				 	"legal_leads.action" => 0
				 ),
				 4 => array(
				 	"legal_leads.name" => 0
				 ),
				 5 => array(
				 	"legal_leads.email" => 0
				 ),
				 6 => array(
				 	"legal_leads.phone" => 0
				 ),
				 7 => array(
				 	"legal_leads.calltime" => 0
				 ),
				 8 => array(
				 	"legal_leads.location" => 0
				 ),
				 9 => array(
				 	"legal_leads.shortdescription" => 0
				 ),
				 10 => array(
				 	"legal_leads.create_time" => 0
				 )
			)
		);

		$this->datatables->set_total_rows(
			$this->user_model->leads_count($cityy)
		);
		
		
		
		$friends = $this->user_model->get_leads($cityy,$this->datatables);
		$friendsdata=$friends->result();
		
		
		$leadslogs=$this->user_model->countleadlogss($userid);
		
		foreach($leadslogs as $logs){

			$array[]=$logs->leadid;

		}
		//echo $logs->leadid;


		//$leadidarray[]=$leadslogs[0]->leadid;


		//$catprice=$this->user_model->view_cat_price(2);

		//print_r($catprice[0]->price);


		foreach($friends->result() as $r) {

			
			$lead_view_count = $this->user_model->countleadview($r->leadid);
			$lead_price = $this->calculateLeadPrice($lead_view_count,$r->lawyer_max,$r->lead_points,$r->point_slab);
		
			if(!empty($r->issue)){
				$category=explode("_",$r->issue);
				$catprice=$category[2];
				$category=$category[1];
				}
			if(!empty($r->subissue)){
				$subcategory=explode("_",$r->subissue);

				$subcategory=$subcategory[1];
			}
			else{

				$subcategory="";
			}	
			
			
			if(isset($array)){		
				if(!in_array($r->leadid, $array)){
			
					$options = '<a href="#" class="btn btn-success btn-xs" onclick="viewLead('.$r->leadid.','.$lead_price.')">View Lead <span>'.$lead_price.' Points</span></a>';
				
					// $options = '<a href="'.site_url("service_request/viewleadss/" . $r->leadid.'/'.$lead_price . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" " onclick="clickon">View Lead <span>'.$lead_price.' Points</span></a>';
				}
				else{

					$options = '<a href="'.site_url("service_request/viewleadss/" . $r->leadid . "/" . $this->security->get_csrf_hash()).'" class="btn btn-danger btn-xs" " id="clickon">Already View Lead </a>';
				}
			}
			else{
				$options = '<a href="#" class="btn btn-success btn-xs" " id="clickon" onclick="viewLead('.$r->leadid.','.$lead_price.')">View Lead <span>'.$lead_price.' Points</span></a>';
			}

			if (empty($subcategory)) {
				$subcategory='N/A';
			}
			$timeofcreate = date('Y-m-d H:i:s',strtotime('+12 hours  +30 minutes',strtotime($r->create_time)));
			$ct = getCityName($r->location);
			$this->datatables->data[] = array(
				
				$category,
				$subcategory,
				$r->shortdescription,
				$r->action,
				$ct,
				$r->lead_price_slot,
				$lead_view_count,
				$timeofcreate,
			    $options
				
			);
		}
		
		
		echo json_encode($this->datatables->process());


}



public function myleadsbyuserid($userid)
{
	$userid = $this->user->info->ID;


	
	 
	
	
	if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		

		$this->load->library("datatables");

		$this->datatables->set_default_order("legal_leads.id", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				 1 => array(
				 	"legal_leads.issue" => 0
				 ),
				 2 => array(
				 	"legal_leads.subissue" => 0
				 ),
				 3 => array(
				 	"legal_leads.action" => 0
				 ),
				 4 => array(
				 	"legal_leads.name" => 0
				 ),
				 5 => array(
				 	"legal_leads.email" => 0
				 ),
				 6 => array(
				 	"legal_leads.phone" => 0
				 ),
				 7 => array(
				 	"legal_leads.calltime" => 0
				 ),
				 8 => array(
				 	"legal_leads.location" => 0
				 ),
				 9 => array(
				 	"legal_leads.shortdescription" => 0
				 ),
				 10 => array(
				 	"legal_leads.create_time" => 0
				 )
			)
		);

		$this->datatables->set_total_rows(
			$this->user_model->leads_count_userid($userid)
		);
		
		
		
		$friends = $this->user_model->get_leads_userid($userid,$this->datatables);
		$friendsdata=$friends->result();
		
		
		$leadslogs=$this->user_model->countleadlogss($userid);
		
		foreach($leadslogs as $logs){

			$array[]=$logs->leadid;

		}

		//echo $logs->leadid;


		//$leadidarray[]=$leadslogs[0]->leadid;


		//$catprice=$this->user_model->view_cat_price(2);

		//print_r($catprice[0]->price);


		foreach($friends->result() as $r) {

			
			$lead_view_count = $this->user_model->countleadview($r->leadid);
			$lead_price = $this->calculateLeadPrice($lead_view_count,$r->lawyer_max,$r->lead_points,$r->point_slab);
		
			if(!empty($r->issue)){
				$category=explode("_",$r->issue);
				$catprice=$category[2];
				$category=$category[1];
				}
			if(!empty($r->subissue)){
				$subcategory=explode("_",$r->subissue);

				$subcategory=$subcategory[1];
			}
			else{

				$subcategory="";
			}	
			
			
			
			
					$options = '<a href="#" class="btn btn-success btn-xs" onclick="unpublishlead('.$r->leadid.','.$lead_price.')">Unpublish lead</span></a>';
				
					// $options = '<a href="'.site_url("service_request/viewleadss/" . $r->leadid.'/'.$lead_price . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" " 

			if (empty($subcategory)) {
				$subcategory='N/A';
			}
			$timeofcreate = date('Y-m-d H:i:s',strtotime('+12 hours  +30 minutes',strtotime($r->create_time)));
			$ct = getCityName($r->location);
			$this->datatables->data[] = array(
				
				$category,
				$subcategory,
				$r->shortdescription,
				$r->action,
				$ct,
				$r->lead_price_slot,
				$lead_view_count,
				$timeofcreate,
			    $options
				
			);
		}
		
		
		echo json_encode($this->datatables->process());


}


private function  calculateLeadPrice($lead_current_count,$lawyer_max,$lead_points,$point_slab){
	$deduct_amount = 0;
	if( !empty($lawyer_max) && !empty($lead_points) && !empty($point_slab)){
		$lawyer_max = explode("-",$lawyer_max);
		
		$lawyer_max = $lawyer_max[1];
	   //  $max_lawyer = $lawyer_max;
	   //  echo $lawyer_max;
	   //  die;
		$point_slab = unserialize($point_slab);
	   
	
	   //if current count is less than max lawyer count limit
	   if($lead_current_count < $lawyer_max ){
		   $deduct_amount = $lead_points * $point_slab[$lead_current_count]/100;
	   }
	}
	

	return $deduct_amount;
	
}

}

?>
