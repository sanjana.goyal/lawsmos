<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_request extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		
		$this->load->model("user_model");
		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("page_model");
		$this->load->model("calendar_model");
		$this->load->model("home_model");
		
		$this->load->model("login_model");
		
		
		$this->load->model("register_model");
		// If the user does not have premium. 
		// -1 means they have unlimited premium
		

		$this->template->set_layout("client/themes/titan.php");
	}

	public function index()
	{
		
		
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		$userid = $this->user->info->ID;
	$user = $this->user_model->get_user_by_id($userid);
		if($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}
	   
	 
		$this->template->loadContent("service_request/index.php", array(
			
				"user" => $user,
			
			)
			
		);
	}
	
	
	public function action(){
		
		if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		$catid=$_GET['categoryId'];
	
		$query = $this->db->get_where('catagories', array('parent_id' => $catid));
		$data1['categories'] = $query->result();
		
		if(!empty($query->result())){
		//print_r($data1['categories']);
		$i=1;
		
		foreach($query->result() as $rows){
		
		?> 
		
			<div class="step-1-content-danger">
				
				
					<?php
				if($i=='1')
				{
				?>
				<input type="radio"  value="<?php echo $rows->cid."_".$rows->c_name; ?>" name="subissue_id" id="<?php echo "child_cat.".$rows->cid ?>" checked  />
				<?php
			}
			else
			{
				?>
				<input type="radio"  value="<?php echo $rows->cid."_".$rows->c_name; ?>" name="subissue_id" id="<?php echo "child_cat.".$rows->cid ?>"  />
			<?php
			}
			?>
				
				
				<label for="<?php echo "child_cat.".$rows->cid ?>"><?php echo $rows->c_name; ?></label>
				
			</div>
			
			<?php
		
            $i++;
			}
		}
           
	
		
		
		}
		
	public function save(){
		
		
			if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
			
			$issueid=$this->input->post('issue_id');
			$issuearray=explode("_",$issueid);
			$issue=$issuearray[1];
			$subissueid=$this->input->post('subissue_id');
			$subissuearray=explode("_",$subissueid);
			$subissue=$subissuearray[1];
			$action=$this->input->post('action_list');
			$tellus=$this->input->post('tellus');
			$phone=$this->input->post('phone');
			$name=$this->input->post('name');
			$email=$this->input->post('email');
			$calltime=$this->input->post('calltime');
			$location=$this->input->post('location');
			
			$locationss=explode(",",$location);
			
			$locationsss=$locationss[0];
			
			$shortdescription=$this->input->post('shortdescription');
			$recieve_emails=$this->input->post('emailtime');
			
			
			
	 $data = array(
        'issue'=>$issue,
        'subissue'=>$subissue,
        'action'=>$action,
        'tellus'=>$tellus,
        'phone'=>$phone,
        'name'=>$name,
        'email'=>$email,
        'calltime'=>$calltime,
        'location'=>$locationsss,
        'shortdescription'=>$shortdescription,
        'recieve_emails'=>$recieve_emails,
    );
		
	$run=$this->db->insert('legal_leads',$data);
	
	
	if($run==true){
	
		$this->session->set_flashdata('globalmsg', 'Service Request Generated Successfully');
		redirect(site_url());	
	}
	else{
		
			$this->session->set_flashdata('globalmsg', 'Error , Service request could not generete at this moment ! Plesae try again later');
			redirect(site_url());	
		}
		



}
		
		
		

public function serviceleads()
{
	
	
	$userid = $this->user->info->ID;
	$user = $this->user_model->get_user_by_id($userid);
		if($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}
		
		
		$user = $user->row();
	
	$this->template->loadContent("service_request/leads.php", array(
			
				"user" => $user,
			
	 ));
	
	
}


public function viewleadss($id){

 //$this->user->info->ID;

 echo $points=$this->user->info->points;

 $points=$points-10;


$leads = $this->user_model->view_leads($id);




$deductbalance=$this->user_model->deductbalance($this->user->info->ID,$points);


print_r($leads->result());

echo "Points has been deducted , Remaining balance is".$this->user->info->points;


}

public function myleads($city)
{
	
	
	 $city;
	
	$c=explode(",",$city);
	
	 $cityy=$c[0];
	
	//echo $city;
	if(!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		

		

		$this->load->library("datatables");

		$this->datatables->set_default_order("legal_leads.id", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				 1 => array(
				 	"legal_leads.issue" => 0
				 ),
				 2 => array(
				 	"legal_leads.subissue" => 0
				 ),
				 3 => array(
				 	"legal_leads.action" => 0
				 ),
				 4 => array(
				 	"legal_leads.name" => 0
				 ),
				 5 => array(
				 	"legal_leads.email" => 0
				 ),
				 6 => array(
				 	"legal_leads.phone" => 0
				 ),
				 7 => array(
				 	"legal_leads.calltime" => 0
				 ),
				 8 => array(
				 	"legal_leads.location" => 0
				 ),
				 9 => array(
				 	"legal_leads.shortdescription" => 0
				 ),
				 10 => array(
				 	"legal_leads.create_time" => 0
				 )
			)
		);

		$this->datatables->set_total_rows(
			$this->user_model->leads_count($cityy)
		);
		
		
		
		$friends = $this->user_model->get_leads($cityy,$this->datatables);
	
		
		foreach($friends->result() as $r) {
			
			
			$options = '<a href="'.site_url("service_request/viewleadss/" . $r->leadid . "/" . $this->security->get_csrf_hash()).'" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'You are about to download this lead, points will be deducted according to lead price\')">Download Lead</a>';
			
			$this->datatables->data[] = array(
				
				$r->issue,
				$r->subissue,
				$r->shortdescription,
				$r->action,
				$r->location,
			    $r->create_time,
			    $options
				
			);
		}
		

		echo json_encode($this->datatables->process());


}	

}

?>
