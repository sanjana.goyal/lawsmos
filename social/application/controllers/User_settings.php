<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_Settings extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		$this->load->model("user_model");
		$this->load->model("page_model");
		$this->load->library("form_validation");



		$this->load->model("image_model");
		$this->load->model("feed_model");
		$this->load->model("home_model");


		$this->load->config('email');
		$this->load->library('email');
		$this->load->helper('custom_helper');


		$this->load->model("article_model");
		$this->load->model("funds_model");

		$this->template->loadData(
			"activeLink",
			array("home" => array("general" => 1))
		);

		if (!$this->user->loggedin) $this->template->error(lang("error_1"));


		$this->template->set_layout("client/themes/titan.php");
		$this->template->set_error_view("error/login_error.php");

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}



	public function index()
	{
		$fields = $this->user_model->get_custom_fields_answers(array(
			"edit" => 1
		), $this->user->info->ID);


		$relationship_user = "";


		if ($this->user->info->relationship_userid > 0) {
			$user = $this->user_model->get_user_by_id($this->user->info->relationship_userid);
			if ($user->num_rows() > 0) {
				$user = $user->row();
				$relationship_user = $user->first_name . " " . $user->last_name;
			}
		}

		// Look for request
		$request = $this->user_model->get_relationship_request($this->user->info->ID);

		// Get invite requests
		$requests = $this->user_model->get_relationship_request_invites($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/index.php",
			array(
				"fields" => $fields,
				"relationship_user" => $relationship_user,
				"request" => $request,
				"requests" => $requests,
				'getprofile' => $getprofile
			)
		);
	}


	public function enterlocation()
	{


		echo $_REQUEST['locationselect'];
	}


	public function bonus($id, $type = 0, $hashtag = "")
	{


		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location = $this->user->info->city;
		$location = explode(",", $location);
		$city = $location[0];
		$leads = $this->home_model->get_leads($city);
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview = $getarticlesforreview->result_array();


		$getuser = $this->user_model->get_user_by_id($id);

		$user = $getuser->result();

		if ($user[0]->bonus_points == 0.00 && $user[0]->source == "invite") {

			//500 points rewards
			$updatepoint = $this->user_model->update_user_reward($id, array(

				"bonus_points" => 500,
			));


			$updaterewardtable = $this->user_model->insert_reward_table(array(

				"userid" => $id,
				"points" => 500,
				"notes" => "registration Bonus",

			));
		}

		$this->template->loadContent("bonus/bonus.php", array(


			"user" => $user,
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd" => $topviewd,
			"getprofile" => $getprofile,
			"leads" => $leads,
			"countmessage" => $countmessage,
			"getarticlesforreview" => $getarticlesforreview,
		));
	}

	public function sendmail()
	{


		$from = $this->config->item('smtp_user');
		$to = "sunielsethy@gmail.com";
		$subject = "test email";
		$message = "test message";

		$this->email->set_newline("\r\n");
		$this->email->from($from);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);

		if ($this->email->send()) {
			echo 'Your Email has successfully been sent.';
		} else {
			show_error($this->email->print_debugger());
		}
	}



	public function send_email_to_gmail_list()
	{
		$this->load->config('email');


		$this->load->library('email');




		// $from = $this->config->item('smtp_user');
		$from = "lawyers@lawsmos.com";
		$message = "You are cordially Invited to join LawsMos Created By Suniel Sethy" . base_url();
		$subject = "Lawsmos Invitation";


		$to = array();


		$incominglist = $_GET['myCheckboxes'];

		foreach ($incominglist as $user) :

			$to[] = $user;

		endforeach;

		//print_r($to);

		$emails = implode(',', $to);

		$this->email->set_newline("\r\n");
		$this->email->from($from);
		$this->email->to(implode(',', $to));
		$this->email->subject($subject);
		$this->email->message($message);



		if ($this->email->send()) {
			echo json_encode(array("success" => "Email Send Successfully to $emails "));
		} else {
			show_error($this->email->print_debugger());
			echo json_encode(array("failed" => "Email not Send Successfuly to $emails"));
		}
	}

	public function leadership($type = 0, $hashtag = "")
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$top10rank = $this->user_model->get_leaders_by_points();
		$allusersrank = $this->user_model->get_all_leaders();

		$count = count($top10rank->result());

		$userid = $this->user->info->ID;
		$user = $this->user_model->get_user_by_id($userid);

		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location = $this->user->info->city;
		$location = explode(",", $location);
		$city = $location[0];
		$leads = $this->home_model->get_leads($city);
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview = $getarticlesforreview->result_array();

		$this->template->loadContent("leader/leadership_board.php", array(
			"leaders" => $top10rank->result(),
			"count" => $count,
			"user" => $user,
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd" => $topviewd,
			"getprofile" => $getprofile,
			"leads" => $leads,
			"countmessage" => $countmessage,
			"getarticlesforreview" => $getarticlesforreview,
		));
	}

	public function approveappointment($id)
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$update = $this->user_model->update_approve_sttaus($id);
		return redirect("user_settings/myappointment");
	}

	public function rejectappointment($id)
	{


		$this->template->loadContent("user_settings/rejectappointment.php", array(
			"id" => $id,
		));

		//$update = $this->user_model->update_reject_sttaus($id);
		//return redirect("user_settings/myappointment");
	}

	public function rejectss()
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$id = intval($this->input->post("appointmentid"));
		$reasontype = $this->input->post("reasontype");
		$rejectnote = $this->input->post("rejectnote");


		$data = array(

			"reject_reason" => $reasontype,
			"reject_note" => $rejectnote,
			"status" => 2,

		);


		$update = $this->user_model->update_reject_sttaus($id, $data);

		$getappointment = $this->user_model->get_appoint_by_id($id);

		$data = $getappointment->row();

		$userid = $this->user->info->ID;
		$user = $this->user_model->get_user_by_id($userid);



		// Notification
		$this->user_model->increment_field($data->service_seeker_id, "noti_count", 1);
		$this->user_model->add_notification(
			array(
				"userid" => $data->service_seeker_id,
				"url" => "user_settings#relationship_part",
				"timestamp" => time(),
				"message" => $data->seeker_name . ", Your Appointment request has been rejected by," . $this->user->info->first_name . " " . $this->user->info->last_name,
				"status" => 0,
				"fromid" => $data->service_provider_id,
				"username" => $data->seeker_name,
				"email" => $user->email,
				"email_notification" => $user->email_notification
			)
		);

		return redirect("user_settings/myappointment");
	}


	public function myappointments($type = 0, $hashtag = "")
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$userid = $this->user->info->ID;
		$user = $this->user_model->get_user_by_id($userid);


		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location = $this->user->info->city;
		$location = explode(",", $location);
		$city = $location[0];
		$leads = $this->home_model->get_leads($city);
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview = $getarticlesforreview->result_array();

		if ($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}



		$user = $user->row();
		//$request = $this->user_model->get_my_appointments($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/myappoints.php",
			array(
				"user" => $user,
				"pages" => $pages,
				"users" => $users,
				"hashtags" => $hashtags,
				"type" => $type,
				"hashtag" => $hashtag,
				"postid" => $postid,
				"commentid" => $commentid,
				"replyid" => $replyid,
				"topviewd" => $topviewd,
				"getprofile" => $getprofile,
				"leads" => $leads,
				"countmessage" => $countmessage,
				"getarticlesforreview" => $getarticlesforreview,
			)
		);
	}


	public function myservicerequests()
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$userid = $this->user->info->ID;
		$user = $this->user_model->get_user_by_id($userid);
		if ($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}



		$user = $user->row();
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		//$request = $this->user_model->get_my_appointments($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/myservicerequests.php",
			array(
				"user" => $user,
				'getprofile' => $getprofile
			)
		);
	}

	public function invitecontacts($type = 0, $hashtag = "")
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$userid = $this->user->info->ID;
		$user = $this->user_model->get_user_by_id($userid);
		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location = $this->user->info->city;
		$location = explode(",", $location);
		$city = $location[0];
		$leads = $this->home_model->get_leads($city);
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview = $getarticlesforreview->result_array();
		$this->template->loadContent("user_settings/invite_contacts.php", array(

			"user" => $user,
			"pages" => $pages,
			"users" => $users,
			"hashtags" => $hashtags,
			"type" => $type,
			"hashtag" => $hashtag,
			"postid" => $postid,
			"commentid" => $commentid,
			"replyid" => $replyid,
			"topviewd" => $topviewd,
			"getprofile" => $getprofile,
			"leads" => $leads,
			"countmessage" => $countmessage,
			"getarticlesforreview" => $getarticlesforreview,
		));
	}


	public function process()
	{

		$incominglist = $_GET['email'];
		$full = array();


		$results = $this->user_model->get_email_list();

		$result = $results->result();
		$full = array();
		$fulls = array();
		$fulln = array();
		foreach ($result as $r) :

			$full[] = $r->email;
			$fulls[] = $r->pic;
			$fulln[] = $r->name;

		endforeach;

		$friendlist = array_intersect($full, $incominglist);

		$piclist = array_intersect_key($fulls, $friendlist);
		$namelist = array_intersect_key($fulln, $friendlist);
		$friendlist = array_values($friendlist);
		$piclist = array_values($piclist);
		$namelist = array_values($namelist);

		$newlist = array_diff($incominglist, $friendlist);
		$newlist = array_values($newlist);

		// echo json_encode(array("newlist" =>$incominglist));
		echo json_encode(array("newlist" => $newlist, "users" => $friendlist, "pic" => $piclist, "name" => $namelist));
		//echo json_encode($this->datatables->process());

	}









	public function listservicereq($email)
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}



		$this->load->library("datatables");

		$this->datatables->set_default_order("legal_leads.id", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				1 => array(
					"legal_leads.issue" => 0
				),
				2 => array(
					"legal_leads.subissue" => 0
				),
				3 => array(
					"legal_leads.action" => 0
				),
				4 => array(
					"legal_leads.create_time" => 0
				),

				5 => array(
					"legal_leads.shortdescription" => 0
				),
				6 => array(
					"legal_leads.status" => 0
				)
			)
		);

		$this->datatables->set_total_rows(
			$this->user_model
				->get_total_requests_count($email)
		);


		$friends = $this->user_model->get_user_service_requests($email, $this->datatables);


		foreach ($friends->result() as $r) {



			if (!empty($r->issue)) {


				$category = explode("_", $r->issue);


				$catprice = $category[2];
				$category = $category[1];
			}
			echo json_encode($this->datatables->process());

			if (!empty($r->subissue)) {
				$subcategory = explode("_", $r->subissue);

				$subcategory = $subcategory[1];
			} else {

				$subcategory = "";
			}

			if ($r->status == 0) {
				$options = '<a href="' . site_url("user_settings/disblerequests/" . $r->id . "/" . $this->security->get_csrf_hash()) . '" class="btn btn-success btn-xs" onClick="javascript:return confirm(\'Are you sure to Disable this request, it never let lawyer(s) contact you anymore for this issue?\')">Disable request</a>';
			} else {

				$options = '<a href="' . site_url("user_settings/enablerequests/" . $r->id . "/" . $this->security->get_csrf_hash()) . '" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Enable Request..Once Enable Lawyers can start calling you...?\')">Enable request</a>';
			}


			$this->datatables->data[] = array(


				$category,
				$subcategory,
				$r->action,
				$r->shortdescription,
				$r->create_time,
				$options,


			);
		}
		echo json_encode($this->datatables->process());
	}


	public function disblerequests($id)
	{
		$update = $this->user_model->update_disable_req_status($id);
		return redirect("user_settings/myservicerequests");
	}

	public function enablerequests($id)
	{
		$update = $this->user_model->update_enable_req_status($id);
		return redirect("user_settings/myservicerequests");
	}

	public function myappointment($type = 0, $hashtag = "")
	{

		$userid = $this->user->info->ID;


		$type = intval($type);
		$hashtag = $this->common->nohtml($hashtag);
		$pages = $this->page_model->get_recent_pages();
		$hashtags = $this->feed_model->get_trending_hashtags(10);
		$users = $this->user_model->get_newest_users($this->user->info->ID);
		$topviewd = $this->user_model->get_top_viewd_users($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$postid = intval($this->input->get("postid"));
		$commentid = intval($this->input->get("commentid"));
		$replyid = intval($this->input->get("replyid"));
		$location = $this->user->info->city;
		$location = explode(",", $location);
		$city = $location[0];
		$leads = $this->home_model->get_leads($city);
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$getarticlesforreview = $this->article_model->get_user_article_for_review($this->user->info->catagoryid);
		$getarticlesforreview = $getarticlesforreview->result_array();

		$user = $this->user_model->get_user_by_id($userid);
		if ($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}


		$user = $user->row();
		//$request = $this->user_model->get_my_appointments($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/myappointments.php",
			array(
				"user" => $user,
				"pages" => $pages,
				"users" => $users,
				"hashtags" => $hashtags,
				"type" => $type,
				"hashtag" => $hashtag,
				"postid" => $postid,
				"commentid" => $commentid,
				"replyid" => $replyid,
				"topviewd" => $topviewd,
				"getprofile" => $getprofile,
				"leads" => $leads,
				"countmessage" => $countmessage,
				"getarticlesforreview" => $getarticlesforreview,
			)
		);
	}





	public function get_appointment_events_seekers()
	{




		// Our Start and End Dates
		$start = $this->input->get("start");


		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');


		$userid = $this->user->info->ID;


		$events = $this->user_model->get_appointmettss_view_seekers($start_format, $userid);

		$data_events = array();

		foreach ($events->result() as $r) {

			$data_events[] = array(
				"id" => $r->ID,
				"title" => $r->first_name . " " . $r->last_name . " " . $r->phone,
				"description" => $r->phone,
				"start" => $r->dateofappointment
			);
		}

		echo json_encode(array("events" => $data_events));
		exit();
	}




	public function get_appointment_events_providers()
	{
		// Our Start and End Dates
		$start = $this->input->get("start");


		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');


		$userid = $this->user->info->ID;


		$events = $this->user_model->get_appointmettss($start_format, $userid);

		$data_events = array();

		foreach ($events->result() as $r) {

			$data_events[] = array(
				"id" => $r->ID,
				"title" => $r->seeker_name . " " . $r->dateofappointment,
				"description" => $r->matter,
				"start" => $r->dateofappointment
			);
		}

		echo json_encode(array("events" => $data_events));
		exit();
	}



	public function mylistappointments($userid)
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$userid = intval($userid);
		$user = $this->user_model->get_user_by_id($userid);
		if ($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}
		$user = $user->row();



		$this->load->library("datatables");

		$this->datatables->set_default_order("appointmentbooking.ID", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				1 => array(
					"appointmentbooking.seeker_name" => 0
				),
				2 => array(
					"appointmentbooking.seeker_contact" => 0
				),
				3 => array(
					"appointmentbooking.dateofappointment" => 0
				),
				4 => array(
					"appointmentbooking.timeofappoint" => 0
				)
			)
		);

		$this->datatables->set_total_rows(
			$this->user_model
				->get_total_apointments_counts($user->ID)
		);


		$friends = $this->user_model->get_user_appointment($user->ID, $this->datatables);


		foreach ($friends->result() as $r) {


			$name =	$r->first_name . " " . $r->last_name;

			if ($r->status == 1) {

				$options = '<a href="#" class="btn btn-success btn-xs">Approved</a>';
			} else if ($r->status == 2) {

				$options = '<a href="#" class="btn btn-danger btn-xs" >Rejected</a>';
			} else {
				$options = '<a href="' . site_url("user_settings/approveappointment/" . $r->ID . "/" . $this->security->get_csrf_hash()) . '" class="btn btn-info btn-xs" onClick="javascript:return confirm(\'Are you sure to Approve Request?\')">Approve</a>  <a href="' . site_url("user_settings/rejectappointment/" . $r->ID . "/" . $this->security->get_csrf_hash()) . '" class="btn btn-danger btn-xs" onClick="javascript:return confirm(\'Are you sure to Reject Request?\')">Reject</a>';
			}

			$this->datatables->data[] = array(

				"<a href='" . site_url('profile/') . $r->username . "' title='view profile of $name'>" . $name . "</a>",
				$r->phone,
				$r->email,
				$r->dateofappointment,
				$r->timeofappoint,
				$options,
				$r->created_at,

			);
		}
		echo json_encode($this->datatables->process());
	}

	public function listappointments($userid)
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		$userid = intval($userid);
		$user = $this->user_model->get_user_by_id($userid);
		if ($user->num_rows() == 0) {
			$this->template->error(lang("error_85"));
		}
		$user = $user->row();



		$this->load->library("datatables");

		$this->datatables->set_default_order("appointmentbooking.ID", "desc");

		// Set page ordering options that can be used
		$this->datatables->ordering(
			array(
				1 => array(
					"appointmentbooking.dateofappointment" => 0
				),
				2 => array(
					"appointmentbooking.timeofappoint" => 0
				),
				3 => array(
					"appointmentbooking.status" => 0
				),
				4 => array(
					"appointmentbooking.created_at" => 0
				),
				5 => array(
					"users.email" => 0
				),
				6 => array(
					"users.phone" => 0
				),
				7 => array(
					"users.first_name" => 0
				),
				8 => array(
					"users.last_name" => 0
				)
			)
		);

		$this->datatables->set_total_rows(
			$this->user_model
				->get_total_apointment_counts($user->ID)
		);


		$friends = $this->user_model->get_user_appointments($user->ID, $this->datatables);




		foreach ($friends->result() as $r) {


			$name =	$r->first_name . " " . $r->last_name;
			if ($r->status == 0) {
				$options = "Pending";
			} else if ($r->status == 1) {
				$options = "Approved";
			} else {
				$options = "Status: Rejected<br>Reason: " . $r->reject_reason . "<br>Message:" . $r->reject_note;
			}
			$this->datatables->data[] = array(

				$name,
				$r->phone,
				$r->email,
				$r->dateofappointment,
				$r->timeofappoint,
				$options,
				$r->created_at,

			);
		}
		echo json_encode($this->datatables->process());
	}
	public function pro()
	{


		$this->form_validation->set_rules('location_country', 'Country', 'required');
		$this->form_validation->set_rules('location_state', 'State', 'required');
		//$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|alpha');
		$this->form_validation->set_rules('location_city', 'City', 'required');
		$this->form_validation->set_rules('permannentadr', 'Permananent Address', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone number', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('aboutme', 'About me ', 'required');
		$this->form_validation->set_rules('zipcode', 'zip code', 'required');


		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {



			$location_country = $this->common->nohtml($this->input->post("location_country"));
			$location_state = $this->common->nohtml($this->input->post("location_state"));
			$location_city = $this->common->nohtml($this->input->post("location_city"));
			$permannentadr = $this->common->nohtml($this->input->post("permannentadr"));



			$citylat = str_replace(',', '+', $location_city);

			$stripped = preg_replace('/\s/', '', $citylat);
			//$address='$location_city+$location_state+$location_country';

			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=$stripped&key=AIzaSyAp613NfAwqpgpzboBMgj1eoHTZOzglGAA";
			//echo 	$geocode_stats = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address='".$location_city."'&key=AIzaSyAp613NfAwqpgpzboBMgj1eoHTZOzglGAA");

			$ch = curl_init();



			//  $data2 = 'email='.$email.'&zipcode='.$zipcode.'&country='.$location_country.'&state='.$location_state.'&city='.$location_city.'&address_1='.$location_address.'&lat='.$lat.'&logitude='.$long.'&latlong='.$latlong;


			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$response = json_decode(curl_exec($ch), true);


			$latLng = $response['results'][0]['geometry']['location'];

			$lati = $latLng['lat'];
			$lngi = $latLng['lng'];
			$latlong = $lati . "," . $lngi;

			curl_exec($ch);



			curl_close($ch);


			$this->load->model("register_model");
			$fields = $this->user_model->get_custom_fields_answers(array(
				"edit" => 1
			), $this->user->info->ID);

			$this->load->helper('email');
			$this->load->library("upload");
			$email = $this->common->nohtml($this->input->post("email"));
			$first_name = $this->common->nohtml($this->input->post("first_name"));
			$last_name = $this->common->nohtml($this->input->post("last_name"));
			$aboutme = $this->common->nohtml($this->input->post("aboutme"));




			$phone = $this->common->nohtml($this->input->post("phone"));

			$lat = $this->common->nohtml($this->input->post("lat"));
			$long = $this->common->nohtml($this->input->post("long"));





			//$address_1 = $this->common->nohtml($this->input->post("address_1"));
			//$address_2 = $this->common->nohtml($this->input->post("address_2"));
			//$city = $this->common->nohtml($this->input->post("city"));
			//$state = $this->common->nohtml($this->input->post("state"));
			$zipcode = $this->common->nohtml($this->input->post("zipcode"));
			//$country = $this->common->nohtml($this->input->post("country"));


			$location_address	 = $this->common->nohtml($this->input->post("location_address"));



			$relationship_status = intval($this->input->post("relationship_status"));
			$relationship_user = intval($this->input->post("userid"));

			if ($relationship_status < 0 || $relationship_status > 3) {
				$this->template->error(lang("error_144"));
			}

			$profile_comments = intval($this->input->post("profile_comments"));

			$this->load->helper('email');

			if (empty($email)) $this->template->error(lang("error_18"));

			if (!valid_email($email)) {
				$this->template->error(lang("error_47"));
			}

			if ($email != $this->user->info->email) {
				if (!$this->register_model->checkEmailIsFree($email)) {
					$this->template->error(lang("error_20"));
				}
			}

			$enable_email_notification =
				intval($this->input->post("enable_email_notification"));
			if ($enable_email_notification > 1 || $enable_email_notification < 0)
				$enable_email_notification = 0;

			if ($this->settings->info->avatar_upload) {
				if ($_FILES['userfile']['size'] > 0) {
					if (!$this->settings->info->resize_avatar) {
						$settings = array(
							"upload_path" => $this->settings->info->upload_path,
							"overwrite" => FALSE,
							"max_filename" => 300,
							"encrypt_name" => TRUE,
							"remove_spaces" => TRUE,
							"allowed_types" => $this->settings->info->file_types,
							"max_size" => $this->settings->info->file_size,
						);
						if ($this->settings->info->avatar_width > 0) {
							$settings['max_width'] = $this->settings->info->avatar_width;
						}
						if ($this->settings->info->avatar_height > 0) {
							$settings['max_height'] = $this->settings->info->avatar_height;
						}
						$this->upload->initialize($settings);

						if (!$this->upload->do_upload()) {
							$this->template->error(lang("error_21")
								. $this->upload->display_errors());
						}

						$data = $this->upload->data();

						$image = $data['file_name'];
					} else {
						$this->upload->initialize(array(
							"upload_path" => $this->settings->info->upload_path,
							"overwrite" => FALSE,
							"max_filename" => 300,
							"encrypt_name" => TRUE,
							"remove_spaces" => TRUE,
							"allowed_types" => "gif|png|jpg|jpeg",
							"max_size" => $this->settings->info->file_size,
						));

						if (!$this->upload->do_upload()) {
							$this->template->error(lang("error_21")
								. $this->upload->display_errors());
						}

						$data = $this->upload->data();

						$image = $data['file_name'];

						$config['image_library'] = 'gd2';
						$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['width']         = $this->settings->info->avatar_width;
						$config['height']       = $this->settings->info->avatar_height;

						//$this->load->library('image_lib', $config);

						$this->load->library('image_lib');
						// Set your config up
						$this->image_lib->initialize($config);
						// Do your manipulation
						$this->image_lib->clear();

						if (!$this->image_lib->resize()) {
							$this->template->error(lang("error_21") .
								$this->image_lib->display_errors());
						}
					}
				} else {
					$image = $this->user->info->avatar;
				}

				if ($_FILES['userfile_profile']['size'] > 0) {
					$this->upload->initialize(array(
						"upload_path" => $this->settings->info->upload_path,
						"overwrite" => FALSE,
						"max_filename" => 300,
						"encrypt_name" => TRUE,
						"remove_spaces" => TRUE,
						"allowed_types" => "gif|png|jpg|jpeg",
						"max_size" => $this->settings->info->file_size
					));

					if (!$this->upload->do_upload("userfile_profile")) {
						$this->template->error(lang("error_21")
							. $this->upload->display_errors());
					}

					$data = $this->upload->data();

					$profile_header = $data['file_name'];
				} else {
					$profile_header = $this->user->info->profile_header;
				}
			} else {
				$profile_header = $this->user->info->profile_header;
				$image = $this->user->info->avatar;
			}

			// Custom Fields
			// Process fields
			$answers = array();
			foreach ($fields->result() as $r) {
				$answer = "";
				if ($r->type == 0) {
					// Look for simple text entry
					$answer = $this->common->nohtml($this->input->post("cf_" . $r->ID));

					if ($r->required && empty($answer)) {
						$this->template->error(lang("error_78") . $r->name);
					}
					// Add
					$answers[] = array(
						"fieldid" => $r->ID,
						"answer" => $answer
					);
				} elseif ($r->type == 1) {
					// HTML
					$answer = $this->common->nohtml($this->input->post("cf_" . $r->ID));

					if ($r->required && empty($answer)) {
						$this->template->error(lang("error_78") . $r->name);
					}
					// Add
					$answers[] = array(
						"fieldid" => $r->ID,
						"answer" => $answer
					);
				} elseif ($r->type == 2) {
					// Checkbox
					$options = explode(",", $r->options);
					foreach ($options as $k => $v) {
						// Look for checked checkbox and add it to the answer if it's value is 1
						$ans = $this->common->nohtml($this->input->post("cf_cb_" . $r->ID . "_" . $k));
						if ($ans) {
							if (empty($answer)) {
								$answer .= $v;
							} else {
								$answer .= ", " . $v;
							}
						}
					}

					if ($r->required && empty($answer)) {
						$this->template->error(lang("error_78") . $r->name);
					}
					$answers[] = array(
						"fieldid" => $r->ID,
						"answer" => $answer
					);
				} elseif ($r->type == 3) {
					// radio
					$options = explode(",", $r->options);
					if (isset($_POST['cf_radio_' . $r->ID])) {
						$answer = intval($this->common->nohtml($this->input->post("cf_radio_" . $r->ID)));

						$flag = false;
						foreach ($options as $k => $v) {
							if ($k == $answer) {
								$flag = true;
								$answer = $v;
							}
						}
						if ($r->required && !$flag) {
							$this->template->error(lang("error_78") . $r->name);
						}
						if ($flag) {
							$answers[] = array(
								"fieldid" => $r->ID,
								"answer" => $answer
							);
						}
					}
				} elseif ($r->type == 4) {
					// Dropdown menu
					$options = explode(",", $r->options);
					$answer = intval($this->common->nohtml($this->input->post("cf_" . $r->ID)));
					$flag = false;
					foreach ($options as $k => $v) {
						if ($k == $answer) {
							$flag = true;
							$answer = $v;
						}
					}
					if ($r->required && !$flag) {
						$this->template->error(lang("error_78") . $r->name);
					}
					if ($flag) {
						$answers[] = array(
							"fieldid" => $r->ID,
							"answer" => $answer
						);
					}
				}
			}

			if ($relationship_status > 1) {
				if ($relationship_user > 0 && $this->user->info->relationship_userid != $relationship_user) {
					$user = $this->user_model->get_user_by_id($relationship_user);
					if ($user->num_rows() == 0) {
						$this->template->error(lang("error_145"));
					}

					$user = $user->row();

					// They must be friends in order to send request
					$flags = $this->common->check_friend($this->user->info->ID, $user->ID);
					if (!$flags['friend_flag']) {
						$this->template->error(lang("error_146"));
					}

					// Check to see if user hasn't already sent this user a request
					$request = $this->user_model->check_relationship_request($user->ID, $this->user->info->ID);
					if ($request->num_rows() == 0) {

						// Now delete any other requests from this user
						$this->user_model->delete_relationship_request_from_user($this->user->info->ID);

						// Send request
						$this->user_model->add_relationship_request(
							array(
								"userid" => $user->ID,
								"friendid" => $this->user->info->ID,
								"status" => 0,
								"relationship_status" => $relationship_status
							)
						);

						// Notification
						$this->user_model->increment_field($user->ID, "noti_count", 1);
						$this->user_model->add_notification(
							array(
								"userid" => $user->ID,
								"url" => "user_settings#relationship_part",
								"timestamp" => time(),
								"message" => $this->user->info->first_name . " " . $this->user->info->last_name . " " . lang("ctn_662"),
								"status" => 0,
								"fromid" => $this->user->info->ID,
								"username" => $user->username,
								"email" => $user->email,
								"email_notification" => $user->email_notification
							)
						);
					}
				}
			} else {
				if ($this->user->info->relationship_userid > 0) {
					$this->user_model->update_user(
						$this->user->info->relationship_userid,
						array(
							"relationship_userid" => 0
						)
					);
				}
				$this->user->info->relationship_userid = 0;
			}


			$this->user_model->update_user(
				$this->user->info->ID,
				array(
					"email" => $email,
					"phone" => $phone,
					"first_name" => $first_name,
					"last_name" => $last_name,
					"email_notification" => $enable_email_notification,
					"avatar" => $image,
					"aboutme" => $aboutme,
					//"address_1" => $address_1,
					//"address_2" => $address_2,
					//"city" => $city,
					//"state" => $state,
					"zipcode" => $zipcode,
					//"country" => $country,
					"profile_comments" => $profile_comments,
					"profile_header" => $profile_header,
					"country" => $location_country,
					"state" => $location_state,
					"permanentAddr" => $permannentadr,
					"city" => $location_city,
					"address_1" => $location_address,
					"lat" => $lat,
					"longitude" => $long,
					"LatitudeLongitude" => $latlong,
					"relationship_status" => $relationship_status,
					"relationship_userid" => $this->user->info->relationship_userid
				)
			);

			//	$lat = $this->common->nohtml($this->input->post("lat"));
			//$long = $this->common->nohtml($this->input->post("long"));




			$ch = curl_init();


			$data2 = 'email=' . $email . '&zipcode=' . $zipcode . '&country=' . $location_country . '&state=' . $location_state . '&city=' . $location_city . '&address_1=' . $location_address . '&lat=' . $lat . '&logitude=' . $long . '&latlong=' . $latlong . '&photo=' . $image;

			$profile_tmp_url = str_replace("/social", "", base_url());
			$profile_url = $profile_tmp_url . "api/update_laravel_profile.php";

			curl_setopt($ch, CURLOPT_URL, $profile_url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data2);



			curl_exec($ch);



			curl_close($ch);



			// Update CF
			// Add Custom Fields data
			foreach ($answers as $answer) {
				// Check if field exists
				$field = $this->user_model->get_user_cf($answer['fieldid'], $this->user->info->ID);
				if ($field->num_rows() == 0) {
					$this->user_model->add_custom_field(
						array(
							"userid" => $this->user->info->ID,
							"fieldid" => $answer['fieldid'],
							"value" => $answer['answer']
						)
					);
				} else {
					$this->user_model->update_custom_field(
						$answer['fieldid'],
						$this->user->info->ID,
						$answer['answer']
					);
				}
			}



			$this->session->set_flashdata("globalmsg", lang("success_22"));
			redirect(site_url("user_settings"));
		}
	}



	public function create_agency()
	{

		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent(
			"agency/create_agency.php",
			array(
				'getprofile' => $getprofile
			)
		);


		// $this->template->loadContent("agency/create_agency.php", array());

	}
	public function create_university()
	{



		$this->template->loadContent("agency/create_university.php", array());
	}


	public function myjudgments()
	{

		$this->template->loadContent("user_settings/myjudgements.php", array());
	}




	function validate_name($str)
	{
		if (preg_match("/^[A-Z][a-zA-Z -]+$/", $str) !== 0) {
			return true;
		} else {
			$this->form_validation->set_message("validate_name", '%s is not valid. It allows only alphabets and spaces');
			return false;
		}
	}


	function validate_agency_name($str)
	{
		if (preg_match("/^[A-Z][a-zA-Z -]+$/", $str) !== 0) {
			return true;
		} else {
			$this->form_validation->set_message("validate_agency_name", '%s is not valid.  It allows only alphabets and spaces');
			return false;
		}
	}




	public function university()
	{

		$this->load->model("agency_model");


		$this->load->helper("email");
		$this->load->library("upload");

		$this->form_validation->set_rules('uni_name', 'University Name', 'required');
		$this->form_validation->set_rules('contact_name', 'Contact Name', 'trim|required|callback_validate_name');
		//$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone number', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('location_from', 'City', 'required');
		$this->form_validation->set_rules('map_location', 'Address', 'required');

		$this->form_validation->set_rules('p_address', 'Permanent Address', 'required');

		//$this->load->helper("email");

		/*
		if (empty($email)) $this->template->error(lang("error_18"));

		if (!valid_email($email)) {
			$this->template->error(lang("error_47"));
		}
*/
		$userid = $this->user->info->ID;

		$uniname = $this->common->nohtml($this->input->post("uni_name"));
		$contact_name = $this->common->nohtml($this->input->post("contact_name"));
		$email = $this->common->nohtml($this->input->post("email"));
		$phone = $this->common->nohtml($this->input->post("phone"));

		$city = $this->common->nohtml($this->input->post("location_from"));
		$map_location = $this->common->nohtml($this->input->post("map_location"));

		$lat = $this->common->nohtml($this->input->post("lat"));
		$logitude = $this->common->nohtml($this->input->post("long"));

		$lat_long = $lat . "," . $logitude;


		$p_address = $this->common->nohtml($this->input->post("p_address"));



		if ($this->form_validation->run() == FALSE) {
			$this->template->loadContent("agency/create_university.php", array());
		} else {


			//image upload 



			if ($_FILES['userfile']['size'] > 0) {
				if (!$this->settings->info->resize_avatar) {
					$settings = array(
						"upload_path" => $this->settings->info->upload_path,
						"overwrite" => FALSE,
						"max_filename" => 300,
						"encrypt_name" => TRUE,
						"remove_spaces" => TRUE,
						"allowed_types" => $this->settings->info->file_types,
						"max_size" => $this->settings->info->file_size,
					);
					if ($this->settings->info->avatar_width > 0) {
						$settings['max_width'] = $this->settings->info->avatar_width;
					}
					if ($this->settings->info->avatar_height > 0) {
						$settings['max_height'] = $this->settings->info->avatar_height;
					}
					$this->upload->initialize($settings);

					if (!$this->upload->do_upload()) {
						$this->template->error(lang("error_21")
							. $this->upload->display_errors());
					}

					$data = $this->upload->data();

					$image = $data['file_name'];
				} else {
					$this->upload->initialize(array(
						"upload_path" => $this->settings->info->upload_path,
						"overwrite" => FALSE,
						"max_filename" => 300,
						"encrypt_name" => TRUE,
						"remove_spaces" => TRUE,
						"allowed_types" => "gif|png|jpg|jpeg",
						"max_size" => $this->settings->info->file_size,
					));

					if (!$this->upload->do_upload()) {
						$this->template->error(lang("error_21")
							. $this->upload->display_errors());
					}

					$data = $this->upload->data();

					$image = $data['file_name'];

					$config['image_library'] = 'gd2';
					$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = FALSE;
					$config['width']         = $this->settings->info->avatar_width;
					$config['height']       = $this->settings->info->avatar_height;

					//$this->load->library('image_lib', $config);

					$this->load->library('image_lib');
					// Set your config up
					$this->image_lib->initialize($config);
					// Do your manipulation
					$this->image_lib->clear();

					if (!$this->image_lib->resize()) {
						$this->template->error(lang("error_21") .
							$this->image_lib->display_errors());
					}
				}
			} else {
				$image = $this->user->info->avatar;
			}
		}


		$userid = $this->user->info->ID;

		$uniname = $this->common->nohtml($this->input->post("uni_name"));
		$contact_name = $this->common->nohtml($this->input->post("contact_name"));
		$email = $this->common->nohtml($this->input->post("email"));
		$phone = $this->common->nohtml($this->input->post("phone"));

		$city = $this->common->nohtml($this->input->post("location_from"));
		$map_location = $this->common->nohtml($this->input->post("map_location"));

		$lat = $this->common->nohtml($this->input->post("lat"));
		$logitude = $this->common->nohtml($this->input->post("long"));

		$lat_long = $lat . "," . $logitude;


		$p_address = $this->common->nohtml($this->input->post("p_address"));

		$data = array(


			"created_by_user_id" => $userid,
			"universityid" => $uniname,
			"contact_name" => $contact_name,
			"uni_email" => $email,
			"uni_phone" => $phone,
			"proof" => $image,
			"city" => $city,
			"map_location" => $map_location,
			"p_address" => $p_address,
			"lat" => $lat,
			"logitude" => $logitude,
			"lat_long" => $lat_long,

		);


		$data1 = array(

			"lat" => $lat,
			"logitude" => $logitude,
			"LatitudeLongitude" => $lat_long,
		);
		//0 by default 1 under review 2 accepted 3 rejected 



		$this->agency_model->registeruniversity($data);
		$this->agency_model->update_page_by_email($email, $data1);



		$data1 = array(

			"agency_verified" => 1,

		);

		$this->user_model->update_user($userid, $data1);

		$this->session->set_flashdata("globalmsg", lang("success_111"));
		redirect(site_url("user_settings"));
	}



	public function agency()
	{





		$this->load->model("agency_model");


		$this->load->helper("email");
		$this->load->library("upload");

		$this->form_validation->set_rules('agency_name', 'Agency Name', 'trim|required|callback_validate_agency_name');
		$this->form_validation->set_rules('contact_name', 'Contact Name', 'trim|required|callback_validate_name');
		//$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone number', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('location_from', 'City', 'required');
		$this->form_validation->set_rules('address_1', 'Address', 'required');
		$this->form_validation->set_rules('reg_number', 'Registration Number', 'required');
		$this->form_validation->set_rules('p_address', 'Permanent Address', 'required');

		//$this->load->helper("email");

		/*
		if (empty($email)) $this->template->error(lang("error_18"));

		if (!valid_email($email)) {
			$this->template->error(lang("error_47"));
		}
*/




		if ($this->form_validation->run() == FALSE) {
			$this->template->loadContent("agency/create_agency.php", array());
		} else {


			//image upload 


			if ($this->settings->info->avatar_upload) {
				if ($_FILES['userfile']['size'] > 0) {
					if (!$this->settings->info->resize_avatar) {
						$settings = array(
							"upload_path" => $this->settings->info->upload_path,
							"overwrite" => FALSE,
							"max_filename" => 300,
							"encrypt_name" => TRUE,
							"remove_spaces" => TRUE,
							"allowed_types" => $this->settings->info->file_types,
							"max_size" => $this->settings->info->file_size,
						);
						if ($this->settings->info->avatar_width > 0) {
							$settings['max_width'] = $this->settings->info->avatar_width;
						}
						if ($this->settings->info->avatar_height > 0) {
							$settings['max_height'] = $this->settings->info->avatar_height;
						}
						$this->upload->initialize($settings);

						if (!$this->upload->do_upload()) {
							$this->template->error(lang("error_21")
								. $this->upload->display_errors());
						}

						$data = $this->upload->data();

						$image = $data['file_name'];
					} else {
						$this->upload->initialize(array(
							"upload_path" => $this->settings->info->upload_path,
							"overwrite" => FALSE,
							"max_filename" => 300,
							"encrypt_name" => TRUE,
							"remove_spaces" => TRUE,
							"allowed_types" => "gif|png|jpg|jpeg",
							"max_size" => $this->settings->info->file_size,
						));

						if (!$this->upload->do_upload()) {
							$this->template->error(lang("error_21")
								. $this->upload->display_errors());
						}

						$data = $this->upload->data();

						$image = $data['file_name'];

						$config['image_library'] = 'gd2';
						$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['width']         = $this->settings->info->avatar_width;
						$config['height']       = $this->settings->info->avatar_height;

						//$this->load->library('image_lib', $config);

						$this->load->library('image_lib');
						// Set your config up
						$this->image_lib->initialize($config);
						// Do your manipulation
						$this->image_lib->clear();

						if (!$this->image_lib->resize()) {
							$this->template->error(lang("error_21") .
								$this->image_lib->display_errors());
						}
					}
				} else {
					$image = $this->user->info->avatar;
				}
			} else {
				$profile_header = $this->user->info->profile_header;
				$image = $this->user->info->avatar;
			}


			$userid = $this->user->info->ID;

			$agency_name = $this->common->nohtml($this->input->post("agency_name"));
			$contact_name = $this->common->nohtml($this->input->post("contact_name"));
			$email = $this->common->nohtml($this->input->post("email"));
			$phone = $this->common->nohtml($this->input->post("phone"));

			$location_from = $this->common->nohtml($this->input->post("location_from"));
			$address_1 = $this->common->nohtml($this->input->post("address_1"));

			$lat = $this->common->nohtml($this->input->post("lat"));
			$logitu = $this->common->nohtml($this->input->post("long"));

			$lat_long = $lat . "," . $logitu;
			$reg_number = $this->common->nohtml($this->input->post("reg_number"));
			$p_address = $this->common->nohtml($this->input->post("p_address"));

			$data = array(
				"created_by_user_id" => $userid,

				"agency_name" => $agency_name,
				"contact_name" => $contact_name,
				"email" => $email,
				"phone" => $phone,
				"city" => $location_from,
				"address" => $address_1,
				"permanentAddr" => $p_address,
				"lat" => $lat,
				"logitude" => $logitu,
				"LatitudeLongitude" => $lat_long,
				"registration_number" => $reg_number,
				"registration_proof" => $image,
				"status" => 1,

			);


			$data1 = array(

				"lat" => $lat,
				"logitude" => $logitu,
				"LatitudeLongitude" => $lat_long,
			);
			//0 by default 1 under review 2 accepted 3 rejected 



			$this->agency_model->registerAgency($data);




			$this->agency_model->update_page_by_email($email, $data1);



			$data1 = array(

				"agency_verified" => 1,

			);

			$this->user_model->update_user($userid, $data1);

			$this->session->set_flashdata("globalmsg", lang("success_111"));
			redirect(site_url("user_settings"));
		}
	}

	public function addjudgment()
	{



		$this->load->model("agency_model");
		$this->load->helper("email");
		$this->load->library("upload");
		$this->form_validation->set_rules('case_heading', 'Case Heading', 'required|callback_validate_agency_name');
		$this->form_validation->set_rules('case_description', 'Case Description', 'required');
		//$this->form_validation->set_rules('contact_name', 'Contact Name', 'required|alpha');
		$this->form_validation->set_rules('courtname', 'Court Name', 'required');
		$this->form_validation->set_rules('case_number', 'Case Number', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('final_order_date', 'Final order Date', 'required');
		$this->form_validation->set_rules('nature_of_proceing', 'Nature of proceding', 'required');
		$this->form_validation->set_rules('resultcase', 'Result', 'required');




		//$this->load->helper("email");

		/*
		if (empty($email)) $this->template->error(lang("error_18"));

		if (!valid_email($email)) {
			$this->template->error(lang("error_47"));
		}
*/
		$userid = $this->user->info->ID;

		$caseheading = $this->common->nohtml($this->input->post("case_heading"));
		$casenumber = $this->common->nohtml($this->input->post("case_number"));
		$casedescription = $this->common->nohtml($this->input->post("case_description"));
		$courtname = $this->common->nohtml($this->input->post("courtname"));
		$windate = $this->common->nohtml($this->input->post("windate"));
		$final_order_date = $this->common->nohtml($this->input->post("final_order_date"));
		$nature_of_proceing = $this->common->nohtml($this->input->post("nature_of_proceing"));
		$resultcase = $this->common->nohtml($this->input->post("resultcase"));





		if ($this->form_validation->run() == FALSE) {
			$this->template->loadContent(
				"user_settings/myjudgements.php",
				array()
			);
		} else {


			//image upload 


			if ($this->settings->info->avatar_upload) {
				if ($_FILES['userfile']['size'] > 0) {
					if (!$this->settings->info->resize_avatar) {
						$settings = array(
							"upload_path" => $this->settings->info->upload_path,
							"overwrite" => FALSE,
							"max_filename" => 300,
							"encrypt_name" => TRUE,
							"remove_spaces" => TRUE,
							"allowed_types" => 'pdf',
							"max_size" => $this->settings->info->file_size,
						);


						if ($this->settings->info->avatar_width > 0) {
							$settings['max_width'] = $this->settings->info->avatar_width;
						}
						if ($this->settings->info->avatar_height > 0) {
							$settings['max_height'] = $this->settings->info->avatar_height;
						}
						$this->upload->initialize($settings);

						if (!$this->upload->do_upload()) {
							$this->template->error(lang("error_21")
								. $this->upload->display_errors());
						}

						$data = $this->upload->data();

						$image = $data['file_name'];
					} else {
						$this->upload->initialize(array(
							"upload_path" => $this->settings->info->upload_path,
							"overwrite" => FALSE,
							"max_filename" => 300,
							"encrypt_name" => TRUE,
							"remove_spaces" => TRUE,
							"allowed_types" => "pdf",
							"max_size" => $this->settings->info->file_size,
						));

						if (!$this->upload->do_upload()) {
							$this->template->error(lang("error_21")
								. $this->upload->display_errors());
						}

						$data = $this->upload->data();

						$image = $data['file_name'];


						$config['image_library'] = 'gd2';
						$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = FALSE;
						$config['width']         = $this->settings->info->avatar_width;
						$config['height']       = $this->settings->info->avatar_height;

						//$this->load->library('image_lib', $config);

						$this->load->library('image_lib');
						// Set your config up
						$this->image_lib->initialize($config);
						// Do your manipulation
						$this->image_lib->clear();

						if (!$this->image_lib->resize()) {
							$this->template->error(lang("error_21") .
								$this->image_lib->display_errors());
						}
					}
				} else {
					$image = $this->user->info->avatar;
				}
			} else {
				$profile_header = $this->user->info->profile_header;
				$image = $this->user->info->avatar;
			}


			$userid = $this->user->info->ID;

			$caseheading = $this->common->nohtml($this->input->post("case_heading"));
			$casedescription = $this->common->nohtml($this->input->post("case_description"));
			$courtname = $this->common->nohtml($this->input->post("courtname"));
			$windate = $this->common->nohtml($this->input->post("windate"));

			$final_order_date = $this->common->nohtml($this->input->post("final_order_date"));
			$nature_of_proceing = $this->common->nohtml($this->input->post("nature_of_proceing"));
			$resultcase = $this->common->nohtml($this->input->post("resultcase"));
			$casenumber = $this->common->nohtml($this->input->post("case_number"));


			$data = array(

				"userid" => $userid,
				"caseheading" => $caseheading,
				"casedescription" => $casedescription,
				"filename" => $image,
				"courtname	" => $courtname,
				"windate" => $windate,
				"finalorderdate" => $final_order_date,
				"natureofproceding" => $nature_of_proceing,
				"result" => $resultcase,
				"casenumber" => $casenumber,



			);

			$this->agency_model->registermycase($data);


			$this->session->set_flashdata("globalmsg", lang("success_114"));
			redirect(site_url("user_settings"));
		}
	}

	public function change_password()
	{
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/change_password.php",
			array(
				'getprofile' => $getprofile
			)
		);
	}




	public function change_password_pro()
	{
		$current_password = $this->common->nohtml($this->input->post("current_password"));
		$new_pass1 = $this->common->nohtml($this->input->post("new_pass1"));
		$new_pass2 = $this->common->nohtml($this->input->post("new_pass2"));

		if (empty($new_pass1)) $this->template->error(lang("error_45"));
		if ($new_pass1 != $new_pass2) $this->template->error(lang("error_22"));

		$phpass = new PasswordHash(12, false);
		if (!$phpass->CheckPassword($current_password, $this->user->getPassword())) {
			$this->template->error(lang("error_59"));
		}

		$pass = $this->common->encrypt($new_pass1);
		$this->user_model->update_user(
			$this->user->info->ID,
			array("password" => $pass)
		);

		$this->session->set_flashdata("globalmsg", lang("success_23"));
		redirect(site_url("user_settings/change_password"));
	}


	public function social_networks()
	{
		$user_data = $this->user_model->get_user_data($this->user->info->ID);
		if ($user_data->num_rows() == 0) {
			$this->user_model->add_user_data(
				array(
					"userid" => $this->user->info->ID
				)
			);
			$user_data = $this->user_model->get_user_data($this->user->info->ID);
		}
		$user_data = $user_data->row();
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/social_networks.php",
			array(
				"user_data" => $user_data,
				'getprofile' => $getprofile
			)
		);
		// $this->template->loadContent("user_settings/social_networks.php", array(
		// 	"user_data" => $user_data
		// 	)
		// );
	}

	public function social_networks_pro()
	{
		$twitter = $this->common->nohtml($this->input->post("twitter"));
		$google = $this->common->nohtml($this->input->post("google"));
		$facebook = $this->common->nohtml($this->input->post("facebook"));
		$linkedin = $this->common->nohtml($this->input->post("linkedin"));
		$website = $this->common->nohtml($this->input->post("website"));

		$user_data = $this->user_model->get_user_data($this->user->info->ID);
		if ($user_data->num_rows() == 0) {
			$this->user_model->add_user_data(
				array(
					"userid" => $this->user->info->ID
				)
			);
			$user_data = $this->user_model->get_user_data($this->user->info->ID);
		}
		$user_data = $user_data->row();

		$this->user_model->update_user_data(
			$user_data->ID,
			array(
				"twitter" => $twitter,
				"facebook" => $facebook,
				"google" => $google,
				"linkedin" => $linkedin,
				"website" => $website
			)
		);

		$this->session->set_flashdata("globalmsg", lang("success_47"));
		redirect(site_url("user_settings/social_networks"));
	}


	public function friend_requests()
	{



		$requests = $this->user_model->get_friend_requests($this->user->info->ID);
		if (empty($requests)) {


			echo "No requests found...";
		} else {

			$requestcount = $this->user_model->get_friend_requests_count($this->user->info->ID);
			$userid =   $this->user->info->ID;
			$requestresult = $requests->result();
			$requestkeys = array_keys($requestresult);

			foreach ($requestkeys as $rkrys) {

				// $rkrys;

				$requestvalues = $requestresult[$rkrys]->userid;


				$mutualfriends = $this->user_model->get_mutual_friends($userid, $requestvalues);

				$mutualresult = $mutualfriends->result();
				$mutualkeys = array_keys($mutualresult);



				foreach ($mutualkeys as $mkeys) {


					$mutualfriendsids = $mutualresult[$mkeys]->friendid;

					$mtutalfriend = $this->user_model->get_user_by_id($mutualfriendsids);

					$mutualfriendsarray[] = $mtutalfriend->result();
				}
			}
		}


		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		if (!empty($mutualfriendsarray)) {

			$this->template->loadContent(
				"user_settings/requests.php",
				array(
					"requests" => $requests,
					"requestcount" => $requestcount,
					"mutualfriends" => $mutualfriendsarray,
					'getprofile' => $getprofile
				)
			);
		} else {


			$this->template->loadContent(
				"user_settings/requests.php",
				array(
					"requests" => $requests,
					"requestcount" => $requestcount,
					'getprofile' => $getprofile

				)
			);
		}
	}

	public function friend_request($type, $id, $hash)
	{
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$type = intval($type);
		$id = intval($id);
		$request = $this->user_model->get_friend_request($id, $this->user->info->ID);
		if ($request->num_rows() == 0) {
			$this->template->error(lang("error_147"));
		}
		$request = $request->row();

		if ($type == 0) {
			// Reject
			$this->user_model->delete_friend_request($id);
			// Notification
			$this->user_model->increment_field($request->userid, "noti_count", 1);
			$this->user_model->add_notification(
				array(
					"userid" => $request->userid,
					"url" => "home/index",
					"timestamp" => time(),
					"message" => $this->user->info->first_name . " " . $this->user->info->last_name . " " . lang("ctn_663"),
					"status" => 0,
					"fromid" => $this->user->info->ID,
					"username" => $request->username,
					"email" => $request->email,
					"email_notification" => $request->email_notification
				)
			);
			$this->session->set_flashdata("globalmsg", lang("success_82"));
		} elseif ($type == 1) {
			// Accept
			$this->user_model->delete_friend_request($id);
			// Notification
			$this->user_model->increment_field($request->userid, "noti_count", 1);
			$this->user_model->add_notification(
				array(
					"userid" => $request->userid,
					"url" => "home/index",
					"timestamp" => time(),
					"message" => $this->user->info->first_name . " " . $this->user->info->last_name . " " . lang("ctn_664"),
					"status" => 0,
					"fromid" => $this->user->info->ID,
					"username" => $request->username,
					"email" => $request->email,
					"email_notification" => $request->email_notification
				)
			);

			$this->user_model->add_friend(
				array(
					"userid" => $request->userid,
					"friendid" => $this->user->info->ID,
					"timestamp" => time()
				)
			);

			$this->user_model->add_friend(
				array(
					"friendid" => $request->userid,
					"userid" => $this->user->info->ID,
					"timestamp" => time()
				)
			);

			// Now update the user's serialized friends list so we can get
			// the wall posts of all friends
			$friends = unserialize($this->user->info->friends);

			$friends[] = $request->userid;

			$this->user_model->update_user(
				$this->user->info->ID,
				array(
					"friends" => serialize($friends)
				)
			);

			// Update other user
			$user = $this->user_model->get_user_by_id($request->userid);
			if ($user->num_rows() > 0) {
				$user = $user->row();
				$friends = unserialize($user->friends);

				$friends[] = $this->user->info->ID;

				$this->user_model->update_user(
					$user->ID,
					array(
						"friends" => serialize($friends)
					)
				);
			}


			$this->session->set_flashdata("globalmsg", lang("success_83"));
		}

		redirect(site_url("user_settings/friend_requests"));
	}

	public function page_invites()
	{
		$invites = $this->page_model->get_page_invites($this->user->info->ID);
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/invites.php",
			array(
				"invites" => $invites,
				'getprofile' => $getprofile
			)
		);
	}

	public function delete_page_invite($id, $hash)
	{
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$invite = $this->page_model->get_page_invite_id($id);
		if ($invite->num_rows() == 0) {
			$this->template->error(lang("error_148"));
		}

		$invite = $invite->row();
		if ($invite->userid != $this->user->info->ID) {
			$this->template->error(lang("error_149"));
		}

		$this->page_model->delete_page_invite($id);
		$this->session->set_flashdata("globalmsg", lang("success_84"));
		redirect(site_url("user_settings/page_invites"));
	}

	public function privacy()
	{
		$getprofile = $this->home_model->get_coreprofile($this->user->info->ID);
		$this->template->loadContent(
			"user_settings/privacy.php",
			array(
				'getprofile' => $getprofile
			)
		);
	}

	public function privacy_pro()
	{
		$profile_view = intval($this->input->post("profile_view"));
		$posts_view = intval($this->input->post("posts_view"));
		$post_profile = intval($this->input->post("post_profile"));
		$allow_friends = intval($this->input->post("allow_friends"));
		$allow_pages = intval($this->input->post("allow_pages"));
		$chat_option = intval($this->input->post("chat_option"));
		$tag_user = intval($this->input->post("tag_user"));

		$this->user_model->update_user($this->user->info->ID, array(
			"profile_view" => $profile_view,
			"posts_view" => $posts_view,
			"post_profile" => $post_profile,
			"allow_friends" => $allow_friends,
			"allow_pages" => $allow_pages,
			"chat_option" => $chat_option,
			"tag_user" => $tag_user
		));

		$this->session->set_flashdata("globalmsg", lang("success_85"));
		// redirect(site_url("user_settings/privacy"));
		redirect(site_url("privacy/settings"));
	}

	public function cancel_request($id, $hash)
	{
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$request = $this->user_model->get_relationship_request_id($id);
		if ($request->num_rows() == 0) {
			$this->template->error(lang("error_147"));
		}
		$request = $request->row();

		if ($request->friendid != $this->user->info->ID) {
			$this->template->error(lang("error_150"));
		}

		$this->user_model->delete_relationship_request($id);
		$this->session->set_flashdata("globalmsg", lang("success_86"));
		redirect(site_url("user_settings"));
	}

	public function relationship_request($id, $type, $hash)
	{
		if ($hash != $this->security->get_csrf_hash()) {
			$this->template->error(lang("error_6"));
		}
		$id = intval($id);
		$request = $this->user_model->get_relationship_request_id($id);
		if ($request->num_rows() == 0) {
			$this->template->error(lang("error_147"));
		}
		$request = $request->row();

		if ($request->userid != $this->user->info->ID) {
			$this->template->error(lang("error_151"));
		}

		if ($type == 1) {
			// Accept
			$this->user_model->delete_relationship_request($id);

			$this->user_model->update_user(
				$this->user->info->ID,
				array(
					"relationship_status" => $request->relationship_status,
					"relationship_userid" => $request->friendid
				)
			);

			$this->user_model->update_user(
				$request->friendid,
				array(
					"relationship_status" => $request->relationship_status,
					"relationship_userid" => $this->user->info->ID
				)
			);
			$this->session->set_flashdata("globalmsg", lang("success_87"));
		} else {
			// Reject
			$this->user_model->delete_relationship_request($id);
			$this->session->set_flashdata("globalmsg", lang("success_88"));
		}

		redirect(site_url("user_settings"));
	}

	public function verified()
	{
		if (!$this->settings->info->enable_verified_requests) {
			$this->template->error(lang("error_166"));
		}
		$this->template->loadContent(
			"user_settings/verified.php",
			array()
		);
	}

	public function verified_pro()
	{


		if (!$this->settings->info->enable_verified_requests) {
			$this->template->error(lang("error_166"));
		}
		$us = $this->user_model->get_verified_request($this->user->info->ID);
		if ($us->num_rows() > 0) {
			$this->template->error(lang("error_169"));
		}

		if ($this->user->info->verified) {
			$this->template->error(lang("error_167"));
		}

		$this->load->model("agency_model");


		$this->load->helper('email');
		$this->load->library("upload");





		$this->load->helper('email');







		//image upload 


		if ($this->settings->info->avatar_upload) {
			if ($_FILES['userfile']['size'] > 0) {
				if (!$this->settings->info->resize_avatar) {
					$settings = array(
						"upload_path" => $this->settings->info->upload_path,
						"overwrite" => FALSE,
						"max_filename" => 300,
						"encrypt_name" => TRUE,
						"remove_spaces" => TRUE,
						"allowed_types" => $this->settings->info->file_types,
						"max_size" => $this->settings->info->file_size,
					);
					if ($this->settings->info->avatar_width > 0) {
						$settings['max_width'] = $this->settings->info->avatar_width;
					}
					if ($this->settings->info->avatar_height > 0) {
						$settings['max_height'] = $this->settings->info->avatar_height;
					}
					$this->upload->initialize($settings);

					if (!$this->upload->do_upload()) {
						$this->template->error(lang("error_21")
							. $this->upload->display_errors());
					}

					$data = $this->upload->data();

					$image = $data['file_name'];
				} else {
					$this->upload->initialize(array(
						"upload_path" => $this->settings->info->upload_path,
						"overwrite" => FALSE,
						"max_filename" => 300,
						"encrypt_name" => TRUE,
						"remove_spaces" => TRUE,
						"allowed_types" => "gif|png|jpg|jpeg",
						"max_size" => $this->settings->info->file_size,
					));

					if (!$this->upload->do_upload()) {
						$this->template->error(lang("error_21")
							. $this->upload->display_errors());
					}

					$data = $this->upload->data();

					$image = $data['file_name'];

					$config['image_library'] = 'gd2';
					$config['source_image'] =  $this->settings->info->upload_path . "/" . $image;
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = FALSE;
					$config['width']         = $this->settings->info->avatar_width;
					$config['height']       = $this->settings->info->avatar_height;

					//$this->load->library('image_lib', $config);

					$this->load->library('image_lib');
					// Set your config up
					$this->image_lib->initialize($config);
					// Do your manipulation
					$this->image_lib->clear();

					if (!$this->image_lib->resize()) {
						$this->template->error(lang("error_21") .
							$this->image_lib->display_errors());
					}
				}
			} else {
				$image = $this->user->info->avatar;
			}
		} else {
			$profile_header = $this->user->info->profile_header;
			$image = $this->user->info->avatar;
		}








		$agency_name = $this->common->nohtml($this->input->post("agency_name"));
		$contact_name = $this->common->nohtml($this->input->post("contact_name"));
		$email = $this->common->nohtml($this->input->post("email"));
		$phone = $this->common->nohtml($this->input->post("phone"));

		$location_from = $this->common->nohtml($this->input->post("location_from"));
		$address_1 = $this->common->nohtml($this->input->post("address_1"));
		$reg_number = $this->common->nohtml($this->input->post("reg_number"));


		if (empty($email)) $this->template->error(lang("error_18"));

		if (!valid_email($email)) {
			$this->template->error(lang("error_47"));
		}

		$this->user_model->add_verified_request(
			array(
				"userid" => $this->user->info->ID,
				"agency_name" => $agency_name,
				"contact_name" => $contact_name,
				"email" =>  $email,
				"phone" =>  $phone,
				"city" =>  $location_from,
				"address" => $address_1,
				"registration_number" => $reg_number,
				"registration_proof" =>  $image,
				"timestamp" => time()
			)
		);
		$this->session->set_flashdata("globalmsg", lang("success_111"));
		redirect(site_url("user_settings/verified"));
	}
	// get price of city plan based on city 
	public function get_city_plan_price($id)
	{

		if (!$this->settings->info->enable_verified_requests) {
			$this->template->error(lang("error_166"));
		}
		$get_price = $this->user_model->get_city_plan_detail($id);
		if (!empty($get_pricec)) {
			$get_price = $get_price[0]->price;
			echo json_encode($get_price);
		}
		echo json_encode($get_price);
	}
	//get price of search plan  based on city and category
	public function get_city_cat_plan_price($cat_id, $city_id)
	{

		if (!$this->settings->info->enable_verified_requests) {
			$this->template->error(lang("error_166"));
		}
		$get_price = $this->user_model->get_search_plan_detail($cat_id, $city_id);
		if (!empty($get_pricec)) {
			$get_price = $get_price[0]->price;
			echo json_encode($get_price);
		}
		echo json_encode($get_price);
	}

	// active city plan for calander view
	public function get_buy_city_plan_list($city_id, $plan_id)
	{

		// Our Start and End Dates
		$start = $this->input->get("start");
		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');
		$start_format = strtotime($start_format);
		$userid = $this->user->info->ID;

		$events = $this->user_model->get_buy_city_plan($start_format, $city_id, $plan_id);

		$data_events = array();

		foreach ($events->result() as $r) {

			//  $data_events[] = array(
			// 	 "id" => $r->id,
			// 	 "title" => $r->first_name." ".$r->last_name." ".$r->phone,
			// 	 "description" => $r->phone,
			// 	 "start" => date('m/d/Y',$r->end_date)

			//  );

			$all_date = $this->displayDates($r->start_date, $r->end_date);


			for ($i = 0; $i < count($all_date); $i++) {
				$data_events[] = array(
					"id" => $r->id,
					"title" => $r->first_name . " " . $r->last_name . " " . $r->phone,
					"description" => $r->phone,
					"start" => $all_date[$i],
					"end" => $all_date[$i]

				);
			}
		}

		echo json_encode(array("events" => $data_events));
		exit();
	}
	// get all active members of search plan for calander view
	public function get_buy_search_plan($plan_id, $cat_id, $city_id)
	{
		// Our Start and End Dates
		$start = $this->input->get("start");


		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');
		$start_format = strtotime($start_format);
		$userid = $this->user->info->ID;

		$events = $this->user_model->get_all_buy_search_plan($start_format, $plan_id, $cat_id, $city_id);

		$data_events = array();

		foreach ($events->result() as $r) {

			$all_date = $this->displayDates($r->start_date, $r->end_date);

			for ($i = 0; $i < count($all_date); $i++) {
				$data_events[] = array(
					"id" => $r->id,
					"title" => $r->first_name . " " . $r->last_name . " " . $r->phone,
					"description" => $r->phone,
					"start" => $all_date[$i],
					"end" => $all_date[$i]

				);
			}
		}





		echo json_encode(array("events" => $data_events));
		exit();
	}

	// get all active members of city plan having same city for show member left 
	public function get_all_active_member($city_id, $plan_id)
	{

		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');

		$start_format = strtotime($start_format);


		$userid = $this->user->info->ID;

		$active_members = $this->user_model->get_buy_city_plan($start_format, $city_id, $plan_id);
		$check_existing_plan = $this->user_model->get_existing_city_plan($this->user->info->ID, $start_format, $city_id, $plan_id);




		if ($check_existing_plan->num_rows() > 0) {
			$existing_plan = $check_existing_plan->result();

			$end_dates = array_column($existing_plan, 'end_date');
			$upcoming_expire_date = max($end_dates);

			$upcoming_date = $upcoming_expire_date + 24 * 60 * 60; //add 1 days for start date range for buy plan
			// $min_expire_date = date('Y/m/d',$upcoming_date);
			$min_expire_date = $upcoming_date;

			$max_last_date = $upcoming_expire_date + 384 * 60 * 60; //add 15 days for minimum date range

			// $max_last_date = date('Y/m/d',$max_last_date);
			$max_last_date = $max_last_date;

			$members_count = count($existing_plan);
			if ($members_count >= 0 && $members_count < 10) {
				$members_left = 10 - $members_count;
			} else if ($members_count >= 10) {
				$members_left = 0;
			}
		} else if ($active_members->num_rows() > 0) {
			$members_data = $active_members->result();
			$members_count = count($members_data);
			if ($members_count >= 10) {

				$end_dates = array_column($members_data, 'end_date');
				$upcoming_expire_date = min($end_dates);

				$upcoming_date = $upcoming_expire_date + 24 * 60 * 60; //add 1 days for start date range for buy plan
				// $min_expire_date = date('Y/m/d',$upcoming_date);
				$min_expire_date = $upcoming_date;

				$max_last_date = $upcoming_expire_date + 384 * 60 * 60; //add 15 days for minimum date range

				// $max_last_date = date('Y/m/d',$max_last_date);
				$max_last_date = $max_last_date;
				$members_left = 0;
			} else if ($members_count >= 0 && $members_count < 10) {
				$members_left = 10 - $members_count;

				$min_expire_date = strtotime(date('Y/m/d'));
				$max_last_date = $min_expire_date + 384 * 60 * 60;
			}
		} else {
			$members_left = 10;

			$min_expire_date = strtotime(date('Y/m/d'));
			$max_last_date = $min_expire_date + 384 * 60 * 60;
		}

		echo json_encode(array('min_expire_date' => $min_expire_date, 'max_last_date' => $max_last_date, 'members_left' => $members_left));
		exit();
	}

	// get all active members of search plan same city or same category for show member left  
	public function get_all_search_members($plan_id, $cat_id, $city_id)
	{

		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');
		$start_format = strtotime($start_format);
		$start_format = strtotime($start_format);
		$userid = $this->user->info->ID;

		$all_active_members = $this->user_model->get_all_buy_search_plan($start_format, $plan_id, $cat_id, $city_id);

		$existing_current_member = $this->user_model->get_existing_buy_search_plan($this->user->info->ID, $start_format, $plan_id, $cat_id, $city_id);


		if ($existing_current_member->num_rows() > 0) {
			$existing_member = $existing_current_member->result();

			$end_dates = array_column($existing_member, 'end_date');
			$upcoming_expire_date = max($end_dates);

			$upcoming_date = $upcoming_expire_date + 24 * 60 * 60; //add 1 days for start date range for buy plan
			// $min_expire_date = date('Y/m/d',$upcoming_date);
			$min_expire_date = $upcoming_date;

			$max_last_date = $upcoming_expire_date + 384 * 60 * 60; //add 15 days for minimum date range

			// $max_last_date = date('Y/m/d',$max_last_date);
			$max_last_date = $max_last_date;

			$members_count = count($existing_member);
			if ($members_count >= 0 && $members_count < 10) {
				$members_left = 10 - $members_count;
			} else if ($members_count >= 10) {
				$members_left = 0;
			}
		} else {
			if ($all_active_members->num_rows() > 0) {

				$members_data = $all_active_members->result();

				$members_count = count($members_data);


				if ($members_count >= 10) {

					$end_dates = array_column($members_data, 'end_date');
					$upcoming_expire_date = min($end_dates);
					$upcoming_date = $upcoming_expire_date + 24 * 60 * 60; //add 1 days for start date range for buy plan

					// $min_expire_date = date('Y/m/d',$upcoming_date);
					$min_expire_date = $upcoming_date;

					$max_last_date = $upcoming_expire_date + 384 * 60 * 60; //add 15 days for minimum date range
					// $max_last_date = date('Y/m/d',$max_last_date);
					$max_last_date = $max_last_date;

					$members_left = 0;
				} else if ($members_count >= 0 && $members_count < 10) {
					$members_left = 10 - $members_count;

					$min_expire_date = strtotime(date('Y/m/d'));
					$max_last_date = $min_expire_date + 384 * 60 * 60;
				}
			} else {
				$members_left = 10;
				$min_expire_date = strtotime(date('Y/m/d'));
				$max_last_date = $min_expire_date + 384 * 60 * 60;
			}
		}

		echo json_encode(array('min_expire_date' => $min_expire_date, 'max_last_date' => $max_last_date, 'members_left' => $members_left));
		exit();
	}


	// active lawyer city plan 
	public function get_lawyer_city_plan($user_id)
	{


		$city_id = $_GET['city_id'];
		$plan_id = $_GET['plan_id'];

		// Our Start and End Dates
		$start = $this->input->get("start");
		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');
		$start_format = strtotime($start_format);


		$lawyer_plan = $this->user_model->get_lawyer_city_plan($start_format, $user_id, $city_id, $plan_id);

		$last_expire_date = "";
		if ($lawyer_plan->num_rows() > 0) {
			$lawyer_plan = $lawyer_plan->result();

			$last_expire_date = max(array_column($lawyer_plan, 'end_date'));
			$last_expire_date = date('d-m-Y', $last_expire_date);
		}
		echo json_encode(array("last_expire_date" => $last_expire_date));
		exit();
	}
	// active lawyer search plan 
	public function get_lawyer_search_plan($user_id)
	{


		$cat_id = $_GET['cat_id'];
		$city_id = $_GET['city_id'];
		$plan_id = $_GET['plan_id'];

		// Our Start and End Dates
		$start = $this->input->get("start");
		$startdt = new DateTime('now'); // setup a local datetime
		// $startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('m/d/Y');
		$start_format = strtotime($start_format);


		$lawyer_plan = $this->user_model->get_lawyer_search_plan($start_format, $user_id, $cat_id, $city_id, $plan_id);

		$last_expire_date = "";
		if ($lawyer_plan->num_rows() > 0) {
			$lawyer_plan = $lawyer_plan->result();

			$last_expire_date = max(array_column($lawyer_plan, 'end_date'));
			$last_expire_date = date('d-m-Y', $last_expire_date);
		}
		echo json_encode(array("last_expire_date" => $last_expire_date));
		exit();
	}


	//get all date from start & end
	private function displayDates($date1, $date2, $format = 'm/d/Y')
	{
		$dates = array();
		$current = $date1;
		$date2 = $date2;
		$stepVal = '+1 day';
		while ($current <= $date2) {
			$dates[] = date($format, $current);
			$current = strtotime($stepVal, $current);
		}
		return $dates;
	}


	public function get_lawyer_plan_active($id, $type)
	{

		$subscription_data = $this->user_model->get_lawyer_active_plan($id, $type);
		$plan_data = $subscription_data->result();
		$final_data = [];

		foreach ($plan_data as $subsription_data) {


			$data['id'] = $subsription_data->id;
			$data['plan_name'] = $subsription_data->name;
			$data['city_id'] = $subsription_data->city_id;
			$data['city_name'] = $subsription_data->city_name;
			$data['category_id'] = $subsription_data->category_id;
			$data['category_name'] = $subsription_data->category_name;
			$data['price'] = $subsription_data->price;
			$data['start_date'] = date('d-m-Y', $subsription_data->start_date);
			$data['end_date'] = date('d-m-Y', $subsription_data->end_date);;


			array_push($final_data, $data);
		}

		echo json_encode($final_data, true);
	}

	
	private function get_languages()
	{

		$lang = $this->user_model->get_all_languages();

		return $lang;
	}

	private function get_courts()
	{

		$courts = $this->home_model->get_all_courts();

		return $courts;
	}
	public function my_resume(){
		
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		
		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$this->template->loadContent("user_settings/my_resume.php", array("user_info" => $user_info, "getprofile" => $getprofile,"type" => $type, "countmessage" => $countmessage));
	}
	public function edit_profile_lawyer()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		if (empty($user_info)) {
			redirect(site_url("admin/lawyer_list"));
		}

		$coreprofile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/lawyer_profile/lawyer_detail.php", array("user_info" => $user_info, "coreprofile" => $coreprofile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage));
	}
	public function edit_profile_lawyer_step_1()
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		$core_profile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();

		$catagories = $this->user_model->get_catagories()->result_array();

		$languages = $this->get_languages();
		$courts = $this->get_courts();
		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/lawyer_profile/edit_step_1.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage, 'all_city' => $all_city, 'catagories' => $catagories,'languages' => $languages, 'courts' => $courts));
	}
	public function edit_profile_lawyer_step_2()
	{
		
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		$core_profile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();

		$catagories = $this->user_model->get_catagories()->result_array();

		$languages = $this->get_languages();
		$courts = $this->get_courts();
		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		// echo "<pre>";
		// print_r(json_decode($core_profile['bar_association_name']));
		// die();

		$this->template->loadContent("user_settings/lawyer_profile/edit_step_2.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage, 'all_city' => $all_city, 'catagories' => $catagories,'languages' => $languages, 'courts' => $courts));
	}
	public function edit_profile_lawyer_step_3()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		$core_profile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();

		$catagories = $this->user_model->get_catagories()->result_array();

		$languages = $this->get_languages();
		$courts = $this->get_courts();
		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/lawyer_profile/edit_step_3.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage, 'all_city' => $all_city, 'catagories' => $catagories,'languages' => $languages, 'courts' => $courts));
	}
	public function edit_profile_lawyer_step_4()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		$core_profile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();

		$catagories = $this->user_model->get_catagories()->result_array();

		$languages = $this->get_languages();
		$courts = $this->get_courts();
		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/lawyer_profile/edit_step_4.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage, 'all_city' => $all_city, 'catagories' => $catagories,'languages' => $languages, 'courts' => $courts));
	}
	public function edit_profile_lawyer_step_5()
	{
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		$core_profile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		// echo "<pre>";
		// print_r($consultation_timing);
		// die();

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();

		$catagories = $this->user_model->get_catagories()->result_array();

		$languages = $this->get_languages();
		$courts = $this->get_courts();
		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/lawyer_profile/edit_step_5.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage, 'all_city' => $all_city, 'catagories' => $catagories,'languages' => $languages, 'courts' => $courts));
	}
	public function edit_profile_lawyer_step_6()
	{

		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}
		
		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		
		$core_profile = $this->user_model->get_coreprofile($user_id);
		$getprofile = $this->home_model->get_coreprofile($user_id);

		$consultation_timing = $this->user_model->get_consultation_timing($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$all_city = $this->funds_model->get_all_city();
		$all_city = $all_city->result();

		$catagories = $this->user_model->get_catagories()->result_array();

		$languages = $this->get_languages();
		$courts = $this->get_courts();
		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$states = $this->home_model->get_all_states();
		$states = $states->result();

		$this->template->loadContent("user_settings/lawyer_profile/edit_step_6.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs, "consultation_timing" => $consultation_timing, "getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage, 'all_city' => $all_city, 'catagories' => $catagories,'languages' => $languages, 'courts' => $courts,'states' => $states));
	}

	public function edit_profile_seeker(){
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;
		$user_info = $this->user_model->get_user_info($user_id);
		if (empty($user_info)) {
			redirect(site_url("admin/lawyer_list"));
		}

		$core_profile = $this->home_model->get_coreprofile_seeker($user_id)->row_array();
		$getprofile = $this->home_model->get_coreprofile_seeker($user_id);

		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		$this->template->loadContent("user_settings/seeker_profile/seeker_detail.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs,"getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage));
	}
	public function update_profile_seeker(){
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}

		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;
		$user_info = $this->user_model->get_user_info($user_id);
		if (empty($user_info)) {
			redirect(site_url("admin/lawyer_list"));
		}

		$core_profile = $this->home_model->get_coreprofile_seeker($user_id)->row_array();
		$getprofile = $this->home_model->get_coreprofile_seeker($user_id);
		$cities = $this->home_model->get_cities($core_profile['state'])->result();
		$citiesoffice = $this->home_model->get_cities($core_profile['office_state'])->result();
		
		$verification_logs = $this->user_model->get_verification_log($user_id);

		$countmessage = $this->user_model->get_total_message_count($this->user->info->ID);
		$states = $this->home_model->get_all_states();
			$states = $states->result();
			$languages = $this->get_languages();
			$courts = $this->get_courts();
		$catagories = $this->user_model->get_catagories()->result_array();
		$this->template->loadContent("user_settings/seeker_profile/edit_seeker_detail.php", array("user_info" => $user_info, "core_profile" => $core_profile, "verification_logs" => $verification_logs,"getprofile" => $getprofile, "type" => $type, "countmessage" => $countmessage,'states' => $states,'cities'=>$cities,'languages' => $languages,'catagories' =>$catagories,'courts'=>$courts,'citiesoffice'=>$citiesoffice));
	
}
public function update_profile_seeker2(){
	$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
		$user_detail = $user_detail->row_array();

		$languages = $this->get_languages();
			$courts = $this->get_courts();
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();

			$catagories = $this->user_model->get_catagories()->result_array();
			$verification_level = 0;
			$this->template->loadContent("home/completeprofileseeker.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '1'));
	
}
	public function edit_profile_student(){
			if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		if (empty($user_info)) {
			redirect(site_url("admin/lawyer_list"));
		}

		 $coreprofile = $this->user_model->get_coreprofile_student($user_id);
		$getprofile = $this->home_model->get_coreprofile_student($user_id);
		if(isset($_GET['print'])){
			 print_r($getprofile->row_array());die;
		}
		// print_r($getprofile->row_array());die;
		// $consultation_timing = $this->user_model->get_consultation_timing($user_id);

		// $verification_logs = $this->user_model->get_verification_log($user_id);

		// $countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/student_profile/student_detail.php", array( "coreprofile" => $coreprofile,"getprofile" => $getprofile));
	}

	public function edit_profile_professor(){
		if (!$this->user->loggedin) {
			redirect(site_url("login"));
		}


		$type = 0;
		$type = intval($type);

		$user_id = $this->user->info->ID;

		$user_info = $this->user_model->get_user_info($user_id);

		if (empty($user_info)) {
			redirect(site_url("admin/lawyer_list"));
		}

		 $coreprofile = $this->user_model->get_coreprofile_professor($user_id);
		$getprofile = $this->home_model->get_coreprofile_professor($user_id);
		if(isset($_GET['print'])){
			 print_r($getprofile->row_array());die;
		}
		// print_r($getprofile->row_array());die;
		// $consultation_timing = $this->user_model->get_consultation_timing($user_id);

		// $verification_logs = $this->user_model->get_verification_log($user_id);

		// $countmessage = $this->user_model->get_total_message_count($this->user->info->ID);

		// $this->template->loadData("activeLink", 

		// 	array("admin" => array("lawyer_list" => 1)));

		$this->template->loadContent("user_settings/professor_profile/professor_detail.php", array( "coreprofile" => $coreprofile,"getprofile" => $getprofile));
	}
	public function edit_student_profile($value='')
	{
		$languages = $this->get_languages();
			$courts = $this->get_courts();
			$getprofile1 = $this->home_model->get_coreprofile_student($this->user->info->ID);
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();
			$getprofile = $getprofile1->row_array();
			$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
			$user_detail = $user_detail->row_array();
			$catagories = $this->user_model->get_catagories()->result_array();
			$step = $_GET['step'];
			if (empty($getprofile)) {
				$verification_level = 0;
			} else {
				$verification_level = $step-1;
			}
			if ($step==0) {
				print_r($getprofile);die;
			}
			$this->template->loadContent("user_settings/student_profile/edit_profile_student.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'core_profile' => $getprofile,  'getprofile' => $getprofile1,'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '0'));
	}
	public function edit_professor_profile($value='')
	{
		$languages = $this->get_languages();
			$courts = $this->get_courts();
			$getprofile1 = $this->home_model->get_coreprofile_professor($this->user->info->ID);
			$all_city = $this->funds_model->get_all_city();
			$all_city = $all_city->result();
			$states = $this->home_model->get_all_states();
			$states = $states->result();
			$getprofile = $getprofile1->row_array();
			$user_detail = $this->home_model->getUsersDetail($this->user->info->ID);
			$user_detail = $user_detail->row_array();
			$catagories = $this->user_model->get_catagories()->result_array();

			$step = $_GET['step'];
			if (empty($getprofile)) {
				$verification_level = 0;
			} else {
				$verification_level = $step-1;
			}
			if ($step==0) {
				print_r($getprofile);die;
			}
			//print_r($all_city);die;
			$this->template->loadContent("user_settings/professor_profile/edit_professor.php", array('languages' => $languages, 'courts' => $courts, 'verification_level' => $verification_level, 'all_city' => $all_city, 'user_detail' => $user_detail, 'core_profile' => $getprofile,  'getprofile' => $getprofile1,'states' => $states, 'catagories' => $catagories, 'is_hide_navbar' => '0'));
	}
	public function edit_profile_student_step_2($value='')
	{
		echo "edit_profile_student_step_2";
	}
	public function edit_profile_student_step_3($value='')
	{
		echo "edit_profile_student_step_3";
	}
	public function edit_profile_student_step_4($value='')
	{
		echo "edit_profile_student_step_4";
	}
	public function edit_profile_student_step_5($value='')
	{
		echo "edit_profile_student_step_5";
	}
}
