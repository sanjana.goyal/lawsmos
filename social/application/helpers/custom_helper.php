<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    if ( ! function_exists('getCityName'))
    {
        function getCityName($city_id){
            $CI =& get_instance();
            $CI->db->from('city');
            $CI->db->where('id',$city_id);
            $result = $CI->db->get()->row_array();
            if (isset($result['name'])) {
               return $result['name'];
            }
            
        }
    }
    if ( ! function_exists('getCategoryName'))
    {
        function getCategoryName($category_id){
            $CI =& get_instance();
            $CI->db->from('catagories');
            $CI->db->where('cid',$category_id);
            $result = $CI->db->get()->row_array();
            return $result['c_name'];
        }
    } 
    if ( ! function_exists('getCourtName'))
    {
        function getCourtName($court_id){
            $CI =& get_instance();
            $CI->db->from('courts');
            $CI->db->where('id',$court_id);
            $result = $CI->db->get()->row_array();
            return $result['court_name'];
        }
    }
    if ( ! function_exists('isVerifiedCourt'))
    {
        function isVerifiedCourt($court_id){
            $CI =& get_instance();
            $CI->db->from('courts');
            $CI->db->where('id',$court_id);
            $CI->db->where('status',0);
            $result = $CI->db->get()->row_array();
            return $result;
        }
    }
    if ( ! function_exists('getLanguageName'))
    {
        function getLanguageName($language_id){
            $CI =& get_instance();
            $CI->db->from('languages');
            $CI->db->where('ID',$language_id);
            $result = $CI->db->get()->row_array();
            return $result['language'];
        }
    }
    if ( ! function_exists('getStateName'))
    {
        function getStateName($state_id){
            $CI =& get_instance();
            $CI->db->from('state');
            $CI->db->where('id',$state_id);
            $result = $CI->db->get()->row_array();
            return $result['name'];
        }
    }
      if ( ! function_exists('getuser_friends'))
    {
        function getuser_friends($user_id){
            $CI =& get_instance();
            $CI->db->from('user_friends');
            $CI->db->where('userid',$user_id);
            $result = $CI->db->get()->row();
            return $result;
        }
    }
    if ( ! function_exists('get_apointments'))
    {
        function get_apointments($user_id){
            $CI =& get_instance();
            $CI->db->from('appointmentbooking');
            $CI->db->where('service_provider_id',$user_id);
            $result = $CI->db->get()->row();
            return $result;
        }
    }
     if ( ! function_exists('get_user_score'))
    {
        function get_user_score($user_id){
            $CI =& get_instance();
             $CI->db->select_sum('score');
            $CI->db->from('scores');
            $CI->db->where('userid',$user_id);
            $result = $CI->db->get()->row()->score;
            return $result;
        }
    }
    if ( ! function_exists('get_user_rating'))
    {
        function get_user_rating($user_id){
            $CI =& get_instance();
             $CI->db->select_avg('rating');
            $CI->db->from('rating');
            $CI->db->where('lawyerid',$user_id);
            $result = $CI->db->get()->row()->rating;
            return $result;
        }
    }

    

?>