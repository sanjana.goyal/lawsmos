<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**


* CodeIgniter PDF Library
 *
 * Generate PDF's in your CodeIgniter applications.
 *
 * @package         CodeIgniter
 * @subpackage      Libraries
 * @category        Libraries
 * @author          Chris Harvey
 * @license         MIT License
 * @link            https://github.com/chrisnharvey/CodeIgniter-  PDF-Generator-Library



*/

require_once APPPATH.'libraries/dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;
class Pdf extends DOMPDF
{
/**
 * Get an instance of CodeIgniter
 *
 * @access  protected
 * @return  void
 */
protected function ci()
{
    return get_instance();
}

/**
 * Load a CodeIgniter view into domPDF
 *
 * @access  public
 * @param   string  $view The view to load
 * @param   array   $data The view data
 * @return  void
 */
public function load_view($view,$user,$profile,$type)
{
   
    $options = new Options();
     $options->set('isRemoteEnabled', true);
    $dompdf = new Dompdf($options);
    //$html = $this->ci()->load->view($view, $data, TRUE);
    $html = '<div class="row profile-mp" >
    <div class="col-md-12">

        <body style="margin: 0;font-family: Arial;">
            <section class="base-section" id="datafor" style="">
                <div id="content">
                    <section class="container" style="width:1170px;margin:0 auto;padding: 0 5px;">
                        <div class="main-section" style=" padding-top: 15px;">
                            <div class="row" style="display: flex; flex-wrap: wrap; margin: 0 -15px;">
                                <div class="col-md-2" style=" width: 16.66666667%;">
                                    <div class="intro-sec" style="">
                                        <img width="150px" src="'.base_url().'/uploads/'.$user->avatar.'">
                                    </div>
                                </div>
                                <div class="col-md-10" style="width: 60%;">
                                    <div class="d-flex flex-wrap" style="display: flex;flex-wrap: wrap;">
                                        <div class="col-md-5" style=" width: 30%; position: absolute; left: 140px;">
                                            <div class="info-sec" style=" padding-left: 32px;">
                                                <h2 class="main-text" style=" font-size: 16px;">Advocate</h2>
                                                <p style=" font-size: 14px;">Name:<span style="color: #919191; font-size: 14px; padding-left: 8px;">'.ucfirst($user->first_name).' '.ucfirst($user->last_name).'</span></p>
                                                <p style="font-size: 14px;">Year of practice:<span style="color: #919191;font-size: 14px;padding-left: 8px;">';
                                                if($profile['total_exp']) 
                                                    $html.= $profile['total_exp'].' Years'; 
                                                else 
                                                    $html.=  'N/A';
                                               $html.= ' </span></p>
                                                <p style="font-size: 14px;">City:<span style="color: #919191;font-size: 14px;padding-left: 8px;">';
                                                if($user->city) $html.= getCityName($user->city); else $html.= "N/A";
                                               $html.= ' </span></p>
                                                <p style="font-size: 14px;">Court:<span style="color: #919191;font-size: 14px;padding-left: 8px;">';
                                                if($profile['experience_court']) 
                                                    $html.= getCourtName(json_decode($profile['experience_court'])[0]); 
                                                else 
                                                    $html.=  'N/A';
                                               $html.= '</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-5" style="width: 41.66666667%; position: absolute; left: 390px; top:30px">
                                            <div class="specialization-sec" style="margin-top: 25px;">
                                                <p style="font-size: 14px;">Areas of Specialization:<span style="font-size: 14px;color: #919191;padding-left: 10px;">';
                                                  $spcl = $profile['unpracticing_area_of_specialization'];
                                                if (!$spcl) {
                                                $html.= 'N/A';
                                                }
                                                $array = explode(',', $spcl); 
                                                foreach ($array as $value) {
                                                $array2[] = getCategoryName($value);
                                                }
                                                $html.= implode(',', $array2);
                                               $html.= '</span></p>
                                                <p style="font-size: 14px;">Fields of Interest:<span style="font-size: 14px;color: #919191;padding-left: 10px;"> ';
                                                 $spcl = $profile['unpracticing_fields_of_interest'];
                                         if (!$spcl) {
                                            $html.= 'N/A';
                                        }
                                        $array = explode(',', $spcl); 
                                        foreach ($array as $value) {
                                             $array5[] = getCategoryName($value);
                                         }
                                         $html.= implode(',', $array5);
                                               $html.= '</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br>
                                    <hr>
                        <section class="experiecce-sec">
                            <div class="container" style="width: 1170px;margin: 0 auto;padding: 0 15px;">
                                <div class="row" style=" margin-right: -15px;margin-left: -15px;display: flex;flex-wrap: wrap;">
                                    <div class="col-md-6" style="width: 50%;">
                                        <div class="details" style="margin-top: 20px;">
                                            <div class="section__list">
                                                <div class="section__list-item">
                                                    <div class="left">
                                                        <h3 class="sub-title" style="font-size: 15px;color: #000;border-bottom: 1px solid;display: inline-block;">DISPLAY CURRENT AS:</h3>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Designation:<span style="color: #919191;padding-left: 8px;">';
                                                        if($profile['experience_designation']) 
                                            { if(json_decode($profile['experience_designation'])[0] == "1"){
                                                                        $html.=  "Designated Senior";
                                                                    }elseif(json_decode($profile['experience_designation'])[0] == "2"){
                                                                        $html.=  "Retired Justice";
                                                                    }else{
                                                                        $html.=  "N/A";
                                                                    }
                                                                    } else $html.=  "N/A";
                                               $html.= '</span></p>
                                                        <p class="des-name">Organization (internal link) :<span style="color: #919191;padding-left: 8px;">';
                                                       if($profile['experience_organization_name']) $html.=  json_decode($profile['experience_organization_name'])[0]; else $html.=  "N/A";
                                               $html.= '</span>
                                                        </p>
                                                        <p class="des-name">Display exp: <span style="color: #919191;padding-left: 8px;">'; if($profile['total_exp']) 
                                                    $html.= $profile['total_exp'].' Years'; 
                                                else 
                                                    $html.=  'N/A';

                                                 $html.=  '</span></p>
                                                        <p class="des-name">From: <span style="color: #919191;padding-left: 8px;">'; 
                                                        if($profile['experience_start_date']) $html.=  json_decode($profile['experience_start_date'])[0]; 
                                                        else $html.=  "N/A";

                                                 $html.=  '</span></p>
                                                        <p class="des-name">Till: <span style="color: #919191;padding-left: 8px;"> '; 
                                                        if($profile['experience_end_date']) $html.=  json_decode($profile['experience_end_date'])[0]; 
                                                        else $html.=  "N/A";

                                                 $html.=  '</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style=" width: 50%;position: absolute;left: 300px;">
                                        <br>
                                        <div class="details" style="margin-top: 20px;">
                                            <div class="section__title" style="font-size: 20px;font-weight: 700;color: black;text-transform: uppercase;margin-bottom: 25px;">CONTACT DETAILS</div>
                                            <div class="section__list">
                                                <div class="section__list-item">
                                                    <div class="left">
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Offiice Address:<span style="color: #919191;padding-left: 8px;">'; 
                                                        
                                                        if($user->address_1) 
                                                            $html.=  $user->address_1.' '.$user->address_2.' '.$user->address3.', '.getCityName($user->city).', '.getStateName($user->state).', '.$user->zipcode; 
                                                        else 
                                                            $html.=  "N/A";

                                                 $html.=  '</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Phone no:<span style="color: #919191;padding-left: 8px;">+91-'; 

                                                        $html.=  $user->phone;
                                                        

                                                 $html.=  '</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details" style="margin-top: 20px;">
                                            <div class="section__title" style="font-size: 20px;font-weight: 700;color: black;text-transform: uppercase;margin-bottom: 25px;">LAWSMOS PROFILE DETAILS</div>
                                            <div class="section__list">
                                                <div class="section__list-item">
                                                    <div class="left">
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">User Since: <span style="color: #919191;padding-left: 8px;">'; 
                                                        
                                                        if($profile['created_at']) 
                                                            $html.= date('F, Y', strtotime($profile['created_at'])); 
                                                        else 
                                                            $html.= 'N/A';

                                                 $html.=  ' </span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">User Rating:<span style="color: #919191;padding-left: 8px;">'; 
                                                            if(get_user_rating($user->ID)>0) 
                                                             $html.= round(get_user_rating($user->ID),1); 
                                                            else 
                                                                $html.= 'N/A';
                                                        

                                                 $html.=  '</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Connections:<span style="color: #919191;padding-left: 8px;">'; 
                                                        
                                                         $html.= count(getuser_friends($user->ID));

                                                 $html.=  '</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">City Rank:<span style="color: #919191;padding-left: 8px;">10'; 
                                                        
                                                        

                                                 $html.=  '</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Total Consultations: <span style="color: #919191;padding-left: 8px">'; 
                                                        
                                                         $html.=  count(get_apointments($user->ID));

                                                 $html.=  '</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Lawsmos Score:<span style="color: #919191;padding-left: 8px;">'; 
                                                        if(get_user_score($user->ID)) 
                                                        $html.= get_user_score($user->ID); 
                                                        else 
                                                        $html.= 0;
                                                        

                                                 $html.=  '</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </section>

                                </div>

                            </div>
                        </div>
                    </section>
                   </div>
               </section>
        </body>
    </div>
</div>
</div>
';

  
    $dompdf->loadHtml($html);

    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'portrait');

    // Render the HTML as PDF
    $dompdf->render();
    $time = time();

    // Output the generated PDF to Browser
    $dompdf->stream("my_resume-". $time,array("Attachment"=>$type));
}
}