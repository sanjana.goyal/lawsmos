<?php



class Admin_Model extends CI_Model 

{



	public function updateSettings($data) 

	{

		$this->db->where("ID", 1)->update("site_settings", $data);

	}

	



	public function add_ipblock($ip, $reason) 

	{

		

		$this->db->insert("ip_block", array(

			"IP" => $ip,

			"reason" => $reason,

			"timestamp" => time()

			)

		);

		

	}

	

	

	public function get_ip_blocks() 

	{

		return $this->db->get("ip_block");

	}

	

	

	public function get_all_blog()

	{

	

	return $this->db->get("user_blogs");

	}

	public function get_all_news()

	{

	

	return $this->db->get("user_news");

	}

	

	public function view_section_details($id) 

	{

		return $this->db->where('sections.id',$id)

		->select('sections.description,acts.title,sections.section')

		->join("acts", "acts.id = sections.act_id")

		->get("sections");

	}



	public function get_ip_block($id) 

	{

		return $this->db->where("ID", $id)->get("ip_block");

	}

	public function get_added_chapters($title,$actid) 

	{

		return $this->db->where("chapter_title", $title)->where("act_id",$actid)->get("act_chapters");

	}

	public function get_all_performas() 

	{

		return $this->db->get("performas");

	}

	public function get_category_name($id){

		$info = $this->db->from("catagories")->where("cid",$id)->get()->row_array();

		if(!empty($info)){

			return $info['c_name'];

		}else{

			return "N/A";

		}

	}

	public function get_performa_data($id){

		return $this->db->where('id',$id)->get("performas")->row_array();

	}







	public function get_subcat_by_cat_id($id)

	{

	return $this->db->where("parent_id", $id)->get("catagories");

	

	}

	public function delete_ipblock($id) {

		$this->db->where("ID", $id)->delete("ip_block");

	}





	public function delete_sectionwithoutchapter($id)

	{

	

	$this->db->where("ID", $id)->delete("sections");

	}

	public function delete_chaptersss($id)

	{

	

	$this->db->where("ID", $id)->delete("act_chapters");

	}

	public function delete_performa($id)

	{

	

	$this->db->where("ID", $id)->delete("performas");

	}



	public function get_email_template($id) 

	{

		return $this->db->where("ID", $id)->get("email_templates");

	}





	public function getpage_details($id)

	{

	 

		return $this->db->where("ID", $id)->get("manage_pages");



	}

	public function add_email_template($data) 

	{

		$this->db->insert("email_templates", $data);

	}

	public function add_performa($data) 

	{

		$this->db->insert("performas", $data);

		// echo $this->db->last_query();

		// die();

	}

	public function update_performa($id,$data) 

	{

		$this->db->where('id',$id)->update("performas", $data);

		// echo $this->db->last_query();

		// die();

	}



    public function addbareconnects($data)

    {

		$this->db->insert("category_connecttions", $data);

	}



	public function add_pages_to_pro($data)

	{

		

		$this->db->insert("manage_pages", $data);

	}

	public function update_email_template($id, $data) 

	{

		$this->db->where("ID", $id)->update("email_templates", $data);

	}

	public function mark_as_landmark_judgement($id, $data) 

	{

		$this->db->where("ID", $id)->update("high_court_ap_yearly", $data);

	}
	public function mark_as_landmark_supreme_judgement($id, $data) 

	{

		$this->db->where("ID", $id)->update("supreme", $data);

	}

	public function updatebareconnects($cid, $data) 

	{

		$this->db->where("cat_id", $cid)->update("category_connecttions", $data);

	}



	public function delete_email_template($id) 

	{

		$this->db->where("ID", $id)->delete("email_templates");

	}

	

	

	

	public function get_user_groups() 

	{

		return $this->db->get("user_groups");

	}

	public function get_bareacts($datatable) 

	{

		

		// $datatable->db_order();



		$datatable->db_search(array(

			"acts.title"

			)

		);



		return $this->db->limit($datatable->length, $datatable->start)->get("acts");

	

	}

	public function get_chapters_by_act_id($id) 

	{

		return $this->db->where("act_id", $id)->get("act_chapters");

	}



	public function add_group($data) 

	{

		$this->db->insert("user_groups", $data);

	}

	public function save_chapters($data) 

	{

		$this->db->insert("act_chapters", $data);

	}

	

	public function save_sections($data) 

	{

		$this->db->insert("sections", $data);

	}



	public function get_user_group($id) 

	{

		 $this->db->where("ID", $id)->get("user_groups");

		 

	}

	

	

	public function selectbareactconect($id) 

	{

		 $query=$this->db->where("cat_id", $id)->get("category_connecttions");

		return $query->num_rows();

	}



	public function get_bare_act_by_id($id) 

	{

		return $this->db->where("ID", $id)->get("bareacts");

	}



	public function delete_group($id) {

		

		$this->db->where("ID", $id)->delete("user_groups");

	}



	public function delete_users_from_group($id) 

	{

		

		$this->db->where("groupid", $id)->delete("user_group_users");

	

	}



	public function update_group($id, $data) 

	{

		$this->db->where("ID", $id)->update("user_groups", $data);

	}



	public function get_users_from_groups($id, $page) 

	{

		return $this->db->where("user_group_users.groupid", $id)

			->select("users.ID as userid, users.username, user_groups.name, 

				user_groups.ID as groupid, user_groups.default")

			->join("users", "users.ID = user_group_users.userid")

			->join("user_groups", "user_groups.ID = user_group_users.groupid")

			->limit(20, $page)

			->get("user_group_users");

	}





	public function view_bare_acts_chapters_by_id($id){



		return $this->db->where("act_details.act_id", $id)



			->select("act_details.id as chapterid, act_details.act_id, act_details.chapter, act_details.heading")

			->get("act_details");



	}



	public function view_bare_acts_sectionss_by_id($id){



		return $this->db->where("act_details.act_id", $id)



			->select("act_details.id as chapterid,sections.id,sections.act_id,sections.chapter_id,sections.section,sections.description")



			->join("sections","sections.act_id=act_details.act_id AND sections.chapter_id=act_details.id")

			

			->get("act_details");



	}



	public function actssectionwithoutchapters($id){



		return $this->db->where("sections.act_id", $id)



						->where("sections.chapter_id", NULL)

			

						->get("sections");



	}

	public function view_bare_acts_by_id($id)

	{



		return $this->db->where("acts.id", $id)->get("acts");

	}



	public function view_bare_acts_sections_by_id($id,$chtperid)

	{



		return $this->db->where("sections.act_id", $id)->where_in("sections.chapter_id",implode(",",$chtperid))->get("sections");

	}



	public function get_all_group_users($id) 

	{

		return $this->db->where("user_group_users.groupid", $id)

			->select("users.ID as userid, users.email, users.username, 

				user_groups.name, user_groups.ID as groupid, 

				user_groups.default")

			->join("users", "users.ID = user_group_users.userid")

			->join("user_groups", "user_groups.ID = user_group_users.groupid")

			->get("user_group_users");

	}

	public function get_all_special_words($id) 

	{

		return $this->db->get("special_words");

	}

	public function get_all_special_wordss() 

	{

		return $this->db->get("special_words");

	}



	public function get_total_user_group_members_count($groupid) 

	{

		$s= $this->db->where("groupid", $groupid)

			->select("COUNT(*) as num")

			->join("users", "users.ID = user_group_users.userid")

			->join("user_groups", "user_groups.ID = user_group_users.groupid")

			->get("user_group_users");

		$r = $s->row();

		if(isset($r->num)) return $r->num;

		return 0;

	}



	public function get_total_performas()

	{

	

		$s= $this->db

			->select("COUNT(*) as num")

			->get("performas");

		 $r = $s->row();

		 

		

		 

		if(isset($r->num)) return $r->num;

		return 0;

	}



	public function get_total_pages_count()

	{

		

		$s= $this->db

			->select("COUNT(*) as num")

			->get("manage_pages");

		 $r = $s->row();

		 

		

		 

		if(isset($r->num)) return $r->num;

		return 0;

		

		

	}

	public function get_total_bareacts_count()

	{

		

		$s= $this->db

			->select("COUNT(*) as num")

			->get("acts");

		 $r = $s->row();

		 

		

		 

		if(isset($r->num)) return $r->num;

		return 0;

		

		

	}

	

	public function get_total_modrat_req()

	{

		 $s= $this->db

			->where("moderator_apply",1)

			->select("COUNT(*) as num")

			->get("users");

		 $r = $s->row();

		 

		

		 

		if(isset($r->num)) return $r->num;

		return 0;

	

	}

	public function get_total_words()

	{

		 $s= $this->db->get("special_words");

		 $r = $s->row();

		 

		

		 

		if(isset($r->num)) return $r->num;

		return 0;

	

	}

	public function get_total_words_by_id($id)

	{

		 return $this->db

		 ->where("ID",$id)

		 ->get("special_words");

	

	

	}

	public function get_total_words_by_link($link)

	{

		 $s= $this->db

			->where("link",$link)

			->select("COUNT(*) as num")

			->get("special_words");

		 

		

		  $r = $s->row();

			

	

		 

		if(isset($r->num)) return $r->num;

		return 0;

	

	}

	

	



	public function get_user_from_group($userid, $id) 

	{

		return $this->db->where("userid", $userid)

			->where("groupid", $id)->get("user_group_users");

	}



	public function delete_user_from_group($userid, $id) 

	{

		$this->db->where("userid", $userid)

			->where("groupid", $id)->delete("user_group_users");

	}

	public function delete_special_word($id) 

	{

		$this->db->where("ID", $id)

			     ->delete("special_words");

	}



	public function delete_page_admin($id)

	{

		$this->db->where("ID", $id)->delete("manage_pages");

	

	}



	public function delete_user_from_all_groups($userid) 

	{

		$this->db->where("userid", $userid)->delete("user_group_users");

	}



	public function add_user_to_group($userid, $id) 

	{

		$this->db->insert("user_group_users", 

			array(

			"userid" => $userid, 

			"groupid" => $id

			)

		);

	}



	public function get_all_users() 

	{

		return $this->db->select("users.email, users.ID as userid")

			->get("users");

	}



	public function add_payment_plan($data) 

	{

		$this->db->insert("payment_plans", $data);

	}

	public function add_new_special_word($data) 

	{

		$this->db->insert("special_words", $data);

	}



public function insert_pages_history($data){

	

	

		$this->db->insert("pagehistory_logs", $data);

		}

	public function get_payment_plans() 

	{

		return $this->db->where('status',1)->get("payment_plans");

	}



	public function get_payment_plan($id) 

	{

		return $this->db->where("ID", $id)->get("payment_plans");

	}



	public function delete_payment_plan($id) 

	{

		$this->db->where("ID", $id)->delete("payment_plans");

	}



	public function update_special_word($id, $data)

	{

		$this->db->where("ID", $id)->update("special_words", $data);

	}

	public function update_payment_plan($id, $data)

	{

		$this->db->where("ID", $id)->update("payment_plans", $data);

	}



	public function update_pages_to_pro($id,$data){

		

		$this->db->where("ID", $id)->update("manage_pages", $data);

		}

	

	public function get_payment_logs($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			"payment_logs.email"

			)

		);

		return $this->db->select("users.ID as userid, users.username, users.email,

			users.avatar, users.online_timestamp,

			payment_logs.email, payment_logs.amount, payment_logs.timestamp, 

			payment_logs.ID, payment_logs.processor")

			->join("users", "users.ID = payment_logs.userid")

			->limit($datatable->length, $datatable->start)

			->get("payment_logs");

	}

	public function get_mod_requests($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			

			)

		);

		return $this->db

			->where("moderator_apply",1)

			->select("users.ID as userid, users.username, users.email,

			users.avatar, users.online_timestamp,users.total_points,users.Is_moderator,users.moderator_apply,users.first_name,users.last_name")

			

			->limit($datatable->length, $datatable->start)

			->get("users");

	}

	

	public function get_history_notes_pages($id, $datatable)

	{

	

	$datatable->db_order();



		$datatable->db_search(array(

			"manage_pages.meta_title",

			"pagehistory_logs.history_notes",

			"pagehistory_logs.last_updated"

			)

		);

		return $this->db->select("manage_pages.ID as pageid, manage_pages.meta_title, pagehistory_logs.history_notes,

			pagehistory_logs.last_updated,pagehistory_logs.version")

			->where("pagehistory_logs.pageid",$id)

			->join("manage_pages", "manage_pages.ID = pagehistory_logs.pageid")

			->limit($datatable->length, $datatable->start)

			->get("pagehistory_logs");	

		

	}



	public function get_site_pages($datatable)

	{

		

		$datatable->db_order();



		$datatable->db_search(array(

			"manage_pages.meta_title",

			"manage_pages.meta_description",

			)

		);

		

		

		return $this->db->limit($datatable->length, $datatable->start)->get("manage_pages");

	}



	public function get_total_logs_count($id){

		

		

		$s= $this->db

			->select("COUNT(*) as num")->where("pagehistory_logs.pageid",$id)->get("pagehistory_logs");

		$r = $s->row();

		if(isset($r->num)) return $r->num;

		return 0;

		}



	public function get_total_payment_logs_count() 

	{

		$s= $this->db

			->select("COUNT(*) as num")->get("payment_logs");

		$r = $s->row();

		if(isset($r->num)) return $r->num;

		return 0;

	}



	public function get_user_roles() 

	{

		return $this->db->get("user_roles");

	}



	public function add_user_role($data) 

	{

		$this->db->insert("user_roles", $data);

	}



	public function get_user_role($id) 

	{

		return $this->db->where("ID", $id)->get("user_roles");

	}



	public function update_user_role($id, $data) 

	{

		$this->db->where("ID", $id)->update("user_roles", $data);

	}



	public function delete_user_role($id) 

	{

		$this->db->where("ID", $id)->delete("user_roles");

	}



	public function get_premium_users($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			"users.email",

			"payment_plans.name"

			)

		);

		

		return $this->db->select("users.username, users.email, users.avatar,

		users.online_timestamp, users.first_name, 

			users.last_name, users.ID, users.joined, users.oauth_provider, 

			payment_plans.name, users.premium_time")

		->join("payment_plans", "payment_plans.ID = users.premium_planid")

		->where("users.premium_time >", time())

		->or_where("users.premium_time", -1)

		->limit($datatable->length, $datatable->start)

		->get("users");

	}



	public function get_total_premium_users_count() 

	{

		$s= $this->db

			->select("COUNT(*) as num")->where("premium_time >", time())

			->or_where("users.premium_time", -1)

			->join("payment_plans", "payment_plans.ID = users.premium_planid")

			->get("users");

		$r = $s->row();

		if(isset($r->num)) return $r->num;

		return 0;

	}



	public function add_custom_field($data) 

	{

		$this->db->insert("custom_fields", $data);

	}



	public function get_custom_fields($data) 

	{

		if(isset($data['register'])) {

			$this->db->where("register", 1);

		}

		return $this->db->get("custom_fields");

	}



	public function get_custom_field($id) 

	{

		return $this->db->where("ID", $id)->get("custom_fields");

	}



	public function update_custom_field($id, $data) 

	{

		return $this->db->where("ID", $id)->update("custom_fields", $data);

	}



	public function delete_custom_field($id) 

	{

		$this->db->where("ID", $id)->delete("custom_fields");

	}



	public function get_layouts() 

	{

		return $this->db->get("site_layouts");

	}



	public function get_layout($id) 

	{

		return $this->db->where("ID", $id)->get("site_layouts");

	}



	public function get_user_role_permissions() 

	{

		return $this->db->get("user_role_permissions");

	}



	public function get_total_email_templates() 

	{

		$s = $this->db->select("COUNT(*) as num")->get("email_templates");

		$r = $s->row();

		if(isset($r->num)) return $r->num;

		return 0;

	}



	public function get_email_templates($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"email_templates.title",

			"email_templates.language"

			)

		);

		

		return $this->db

		->limit($datatable->length, $datatable->start)

		->get("email_templates");

	}



	public function add_page_category($data) 

	{

		$this->db->insert("page_categories", $data);

	}

	public function add_category($data) 

	{

		$this->db->insert("catagories", $data);

	}





	public function add_sub_category($data){



		$this->db->insert("catagories", $data);

		

	}

	

	public function add_category_price_location($data)

	{

	

	$this->db->insert("locationprice", $data);

	

     }



	public function get_page_category($id) 

	{

		return $this->db->where("ID", $id)->get("page_categories");

	}

	public function get_category($id) 

	{

		return $this->db->where("cid", $id)->get("catagories");

	}



	public function delete_page_category($id) 

	{

		$this->db->where("ID", $id)->delete("page_categories");

	}

	public function delete_category($id) 

	{

		$this->db->where("cid", $id)->delete("catagories");

	}



	public function update_page_category($id, $data) 

	{

		$this->db->where("ID", $id)->update("page_categories", $data);

	}

	

	public function deactive_page_status($id,$data){

		

		$this->db->where("ID", $id)->update("manage_pages", $data);

		}

	

	public function active_page_status($id,$data){

		

		$this->db->where("ID", $id)->update("manage_pages", $data);

		}

		

		

		

		

	public function update_category($id, $data) 

	{

		$this->db->where("cid", $id)->update("catagories", $data);

	}



	public function get_total_page_categories() 

	{

		return $this->db->from("page_categories")->count_all_results();

	}

	public function get_total_categories() 

	{

		return $this->db->from("catagories")->count_all_results();

	}

	public function get_categories($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"catagories.c_name",

			"catagories.parent_id",

			),

			true // Cache query

		);

		//$this->db->where('catagories.parent_id','0');

		//$datatable->db_where('catagories.parent_id','0');

		//$datatable->db->where('parent_id','0');

		

		

		

		

		

		return $datatable->get("catagories");

	}



	public function get_judgements($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"high_court_ap_yearly.state",

			"high_court_ap_yearly.heading",

			"high_court_ap_yearly.year",

		

			),

			true // Cache query

		);

	

		$this->db->select("catagories.c_name,catagories.price,catagories.cat_slug,high_court_ap_yearly.id,high_court_ap_yearly.state,high_court_ap_yearly.heading,

			high_court_ap_yearly.content,high_court_ap_yearly.year,high_court_ap_yearly.dateofhearing,high_court_ap_yearly.status")

			->join("catagories", "high_court_ap_yearly.category = catagories.cid", "left outer");

			

	

		return $datatable->get("high_court_ap_yearly");

	}



	public function get_total_judgements(){



		 return $this->db->from("high_court_ap_yearly")->count_all_results();

         

		

	}

	

	public function get_judgement_cat($id) 

	{

		return $this->db->where("ID", $id)->get("high_court_ap_yearly");

	}



	public function update_judgement_category($id, $data) 

	{

		$this->db->where('ID', $id);

		return $this->db->update('high_court_ap_yearly', $data);

	}



	public function get_supreme_judgements($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"supreme.heading",

			"supreme.year",

		

			),

			true // Cache query

		);

		return $datatable->get("supreme");

	}

	public function get_total_supreme_judgements(){

		 return $this->db->from("supreme")->count_all_results();

	}

	

	public function get_supreme_judgement_cat($id) 

	{

		return $this->db->where("ID", $id)->get("supreme");

	}
	public function update_supreme_judgement_category($id, $data) 

	{

		$this->db->where('ID', $id);

		return $this->db->update('supreme', $data);

	}




	public function get_page_categories($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"page_categories.name",

			),

			true // Cache query

		);

		return $datatable->get("page_categories");

	}



	public function delete_report($id) 

	{

		$this->db->where("ID", $id)->delete("reports");

	}



	public function get_total_reports() 

	{

		return $this->db->from("reports")->count_all_results();

	}



	public function get_report($id) 

	{

		return $this->db->where("ID", $id)->get("reports");

	}



	public function get_reports($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"reports.reason",

			"users.username",

			"pages.name",

			"g.username"

			),

			true // Cache query

		);

		$this->db

			->select("users.username as reported_username,

				users.first_name as reported_first_name, users.last_name as reported_last_name,

				users.avatar as reported_avatar, 

				users.online_timestamp as reported_online_timestamp,

				pages.ID as pageid, pages.name as page_name, pages.slug,

				g.username, g.avatar, g.first_name, g.last_name, g.online_timestamp,

				reports.ID, reports.timestamp, reports.reason")

			->join("users", "users.ID = reports.userid", "left outer")

			->join("pages", "pages.ID = reports.pageid", "left outer")

			->join("users as g", "g.ID = reports.fromid");



		return $datatable->get("reports");

	}



	public function add_rotation_ad($data) 

	{

		$this->db->insert("rotation_ads", $data);

	}



	public function get_rotation_ad($id) 

	{

		return $this->db->where("ID", $id)->get("rotation_ads");

	}



	public function update_rotation_ad($id, $data) 

	{

		$this->db->where("ID", $id)->update("rotation_ads", $data);

	}



	public function delete_rotation_ad($id) 

	{

		$this->db->where("ID", $id)->delete("rotation_ads");

	}



	public function get_total_rotation_ads() 

	{

		return $this->db->from("rotation_ads")->count_all_results();

	}



	public function get_rotation_ads($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"rotation_ads.name",

			"rotation_ads.status",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,

			rotation_ads.name, rotation_ads.timestamp, rotation_ads.pageviews,

			rotation_ads.status, rotation_ads.cost, rotation_ads.ID")

			->join("users", "users.ID = rotation_ads.userid", "left outer");

		

		return $datatable->get("rotation_ads");

	}



	public function get_total_promoted_posts() 

	{

		return $this->db->from("promoted_posts")->count_all_results();

	}



	public function get_promoted_posts($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"promoted_posts.status",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,

			promoted_posts.timestamp, promoted_posts.pageviews, 

			promoted_posts.postid,

			promoted_posts.status,promoted_posts.location,promoted_posts.user_type, promoted_posts.cost, promoted_posts.ID")

			->join("users", "users.ID = promoted_posts.userid", "left outer");

		

		return $datatable->get("promoted_posts");

	}



	public function get_promoted_post($id) 

	{

		return $this->db->where("ID", $id)->get("promoted_posts");

	}



	public function delete_promoted_post($id) 

	{

		$this->db->where("ID", $id)->delete("promoted_posts");

	}



	public function update_promoted_post($id, $data) 

	{

		$this->db->where("ID", $id)->update("promoted_posts", $data);

	}

	

	public function update_upgrade_reqests_table($id,$data){

		

		return $this->db->where("userid", $id)->update("verification_proof", $data);

		}



	public function get_total_verified_requests()

	{

		return $this->db->from("verified_requests")->count_all_results();

	}

	

	public function get_total_agencies(){

		

		return $this->db->from("agency")->count_all_results();

		}

public function get_total_universities(){

		

		return $this->db->from("university")->count_all_results();

		}



	public function get_total_upgrade_req()

	{

	

		$s = $this->db->select("COUNT(*) as num")->get("verification_proof");

		$r = $s->row();

		

		

		if(isset($r->num)) return $r->num;

		return 0;

	}





	public function get_upgrade_requests($datatable)

	{

	$datatable->db_order();

	

	$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);

		

	

	

	$this->db->select("users.username, users.ID as userid,users.online_timestamp, users.first_name, users.last_name,users.avatar,verification_proof.ID, verification_proof.proof,verification_proof.disclaimer,verification_proof.status")

			->join("users", "users.ID = verification_proof.userid", "left outer");

			

		

		return $datatable->get("verification_proof");

	

	}

	

	

	

	public function get_verify_universities_requests($datatable)

	{

	

	$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,users.online_timestamp, users.first_name, users.last_name,users.avatar,university.ID, universities.universityname,university.contact_name,university.uni_email,university.uni_phone,university.proof,university.city,university.map_location,university.p_address,university.lat,university.logitude,university.lat_long,university.created_at,university.status")

			->join("users", "users.ID = university.created_by_user_id", "left outer")

			->join("universities", "universities.ID = university.universityid", "left outer");

			

		

		return $datatable->get("university");

	

	}

	public function get_verify_agency_requests($datatable){

		

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,users.online_timestamp, users.first_name, users.last_name,users.avatar,agency.id, agency.agency_name,agency.contact_name,agency.email,agency.phone,agency.city,agency.address,agency.registration_number,agency.registration_proof,agency.status,agency.created_at")

			->join("users", "users.ID = agency.created_by_user_id", "left outer");

			

		

		return $datatable->get("agency");

		

		

		

		}

	public function get_verified_requests($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,verified_requests.ID, verified_requests.about, verified_requests.timestamp")

			->join("users", "users.ID = verified_requests.userid", "left outer");

		

		return $datatable->get("verified_requests");

	}





	public function get_verify_agency_request($id){

		

		return $this->db->where("id", $id)->get("agency");

		}

	

	

	public function get_verify_universityyy_requests($id){

		

		return $this->db->where("id", $id)->get("university");

		}

	

	

	public function get_verified_request($id) 

	{

		return $this->db->where("ID", $id)->get("verified_requests");

	}





	public function delete_verified_agency_request($id) 

	{

		$this->db->where("id", $id)->delete("agency");

	}

	

	

	

	public function delete_verified_request($id) 

	{

		$this->db->where("ID", $id)->delete("verified_requests");

	}



	public function get_total_blogs() 

	{

		return $this->db->from("user_blogs")->count_all_results();

	}

	public function get_total_news() 

	{

		return $this->db->from("user_news")->count_all_results();

	}

	public function get_total_articles() 

	{

		return $this->db->from("user_articles")->where("status",1)->count_all_results();

	}



	public function get_blogs($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,user_blogs.ID, user_blogs.title")

			->join("users", "users.ID = user_blogs.userid", "left outer");

		

		return $datatable->get("user_blogs");

	}

	public function get_news($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,user_news.ID, user_news.title")

			->join("users", "users.ID = user_news.userid", "left outer");

		

		return $datatable->get("user_news");

	}

	public function get_articles($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->where("user_articles.status",1)->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,user_articles.ID, user_articles.title,catagories.c_name")

			->join("users", "users.ID = user_articles.userid", "left outer")

			->join("catagories","catagories.cid = user_articles.catagory");

		

		return $datatable->get("user_articles");

	}

	public function get_articles_cat($id) 

	{

		return $this->db->where("ID", $id)->get("user_articles");

	}
	public function update_article_category($id, $data) 

	{

		$this->db->where('ID', $id);

		return $this->db->update('user_articles', $data);

	}



	public function get_total_blog_posts() 

	{

		return $this->db->from("user_blog_posts")->count_all_results();

	}

	public function get_total_news_posts() 

	{

		return $this->db->from("user_news_posts")->count_all_results();

	}



	public function get_blog_posts($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,

			user_blog_posts.ID, user_blog_posts.title, user_blog_posts.status,

			user_blog_posts.timestamp, user_blog_posts.views, user_blog_posts.image")

			->join("user_blogs", "user_blogs.ID = user_blog_posts.blogid")

			->join("users", "users.ID = user_blogs.userid", "left outer");

		

		return $datatable->get("user_blog_posts");

	}

	public function get_news_posts($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"users.username",

			),

			true // Cache query

		);



		$this->db->select("users.username, users.ID as userid,

			users.online_timestamp, users.first_name, users.last_name,

			users.avatar,

			user_news_posts.ID, user_news_posts.title, user_news_posts.status,

			user_news_posts.timestamp, user_news_posts.views, user_news_posts.image")

			->join("user_news", "user_news.ID = user_news_posts.blogid")

			->join("users", "users.ID = user_news.userid", "left outer");

		

		return $datatable->get("user_news_posts");

	}

	public function check_category_name($faq_category_name){

		return $this->db->from('faq_category')->where('faq_category_name',$faq_category_name)->get()->row_array();

	}

	public function add_faq_category($data){

		$this->db->insert('faq_category',$data);

	}

	public function get_total_faq_category() 

	{

		return $this->db->from("faq_category")->count_all_results();

	}

	public function get_faq_category($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"faq_category.category_name",

			),

			true // Cache query

		);



		$this->db->select("*");

		

		return $datatable->get("faq_category");

	}

	public function get_faq_category_by_id($id){

		return $this->db->from("faq_category")->where("id",$id)->get()->row_array();

	} 

	public function delete_faq_category($id){

		$this->db->where('id',$id)->delete("faq_category");

	}

	public function check_category_name_by_id($id,$faq_category_name){

		return $this->db->from('faq_category')->where('id !='.$id)->where('faq_category_name',$faq_category_name)->get()->row_array();

	}

	public function update_faq_category($id,$data){

		$this->db->where("id",$id)->update('faq_category',$data);

	}

	public function get_all_faq_category(){

		return $this->db->from("faq_category")->get()->result_array();

	}

	public function add_faq($data){

		$this->db->insert('faq',$data);

	}

	public function get_total_faq() 

	{

		return $this->db->from("faq")->count_all_results();

	}

	public function get_faq($datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"faq.title",

			),

			true // Cache query

		);



		$this->db->select("*");

		

		return $datatable->get("faq");

	}

	public function get_faq_by_id($id){

		return $this->db->from("faq")->where("id",$id)->get()->row_array();

	}

	public function delete_faq($id){

		$this->db->where('id',$id)->delete("faq");

	}

	public function update_faq($id,$data){

		$this->db->where('id',$id)->update('faq',$data);

	}

	public function get_faq_category_name($id){

		$info = $this->db->from("faq_category")->where("id",$id)->get()->row_array();

		if(!empty($info)){

			return $info['faq_category_name'];

		}else{

			return "";

		}

	}



	public function get_all_cities($datatable){

		// return $this->db->from('city')->order_by('name','asc')->get();



	

		$datatable->db_order();



		$datatable->db_search(array(

			"city_plan_pricelist.name",

			),

			true // Cache query

		);



		// $this->db->select("users.username, users.ID as userid,

		// 	users.online_timestamp, users.first_name, users.last_name,

		// 	users.avatar,

		// 	promoted_posts.timestamp, promoted_posts.pageviews, 

		// 	promoted_posts.postid,

		// 	promoted_posts.status,promoted_posts.location,promoted_posts.user_type, promoted_posts.cost, promoted_posts.ID")

		// 	->join("users", "users.ID = promoted_posts.userid", "left outer");



		$this->db->select("*");

	

		return $datatable->get("city_plan_pricelist");	







	}



	public function get_all_cities_cat($datatable){

		// return $this->db->from('search_plan_pricelist')->order_by('name','asc')->get();



		$datatable->db_order();



		$datatable->db_search(array(

			"search_plan_pricelist.name",

			"search_plan_pricelist.catagory_name",

			),

			true // Cache query

		);



		// // $this->db->select("users.username, users.ID as userid,

		// // 	users.online_timestamp, users.first_name, users.last_name,

		// // 	users.avatar,

		// // 	promoted_posts.timestamp, promoted_posts.pageviews, 

		// // 	promoted_posts.postid,

		// // 	promoted_posts.status,promoted_posts.location,promoted_posts.user_type, promoted_posts.cost, promoted_posts.ID")

		// // 	->join("users", "users.ID = promoted_posts.userid", "left outer");



	 	//   $this->db->from('search_plan_pricelist')->order_by('name','asc')->get();

	

		// return $datatable->get("search_plan_pricelist");	



		$this->db->select("*");

		return $datatable->get("search_plan_pricelist");



	}



	public function get_total_city_records() 

	{

		return $this->db->from("city_plan_pricelist")->count_all_results();

	}



	public function get_total_city__cat_records() 

	{

		return $this->db->from("search_plan_pricelist")->count_all_results();

	}

	

	public function city_plan_detail($id) 

	{

		return $this->db->from("city_plan_pricelist")->where('id',$id)->get();

	}

	

	public function city_plan_price_change($data) 

	{

	

		$data1['price'] = $data['city_price']; 

		return $this->db->where('id',$data['id'])->update("city_plan_pricelist",$data1);

		

	}

	public function city__cat_plan_detail($id) 

	{

		return $this->db->from("search_plan_pricelist")->where('id',$id)->get();

	}

	

	public function city_cat_plan_price_change($data) 

	{

	

		$data1['price'] = $data['city_price']; 

		return $this->db->where('id',$data['id'])->update("search_plan_pricelist",$data1);

		

	}



	public function city_subscription_plan($id) 

	{

		return $this->db->from("search_plan_pricelist")->count_all_results();

	}





	public function get_total_active_city_plan_count($plan_id,$plan_type) 

	{

		// return $this->db->from("city_subscription_plan")->count_all_results();



		$current_date = date('d-m-Y');

		$current_date = strtotime($current_date);

		

		

			$data =  $this->db->where('city_subscription_plan.plan_id',$plan_id)

						->where('city_subscription_plan.plan_type',$plan_type)

						->get("city_subscription_plan")->result();

					return count($data);	

						

		

		

	}



	public function get_total_active_search_plan_count($plan_id,$plan_type) 

	{	

		$current_date = date('d-m-Y');

		$current_date = strtotime($current_date);

		

		

			$data =  $this->db->where('city_subscription_plan.plan_id',$plan_id)

						->where('city_subscription_plan.plan_type',$plan_type)

						->get("city_subscription_plan")->result();

					return count($data);	

						

		

		



		// return $this->db->where('city_subscription_plan.plan_id',$plan_id)

		// 				->where('city_subscription_plan.plan_type',$plan_type)

		// 				->where('city_subscription_plan.end_date >=',$plan_type)

		// from("city_subscription_plan")->count_all_results();

	}

	



	public function get_lawyer_all_plans($id,$datatable) 

	{

		$datatable->db_order();



		$datatable->db_search(array(

			"city_subscription_plan.city_name",

			"city_subscription_plan.category_name",

			"users.first_name",

			"city_subscription_plan.status",

			),

			true // Cache query

		);



		  $this->db->where('city_subscription_plan.plan_id',$id)

				->select("city_subscription_plan.*,payment_plans.name,

						users.username,users.first_name, users.last_name,users.avatar,users.online_timestamp")

				->join("users", "users.ID = city_subscription_plan.user_id", "left outer")

				->join("payment_plans", "city_subscription_plan.plan_id = payment_plans.ID", "left");

				// ->get('city_subscription_plan');

		

		// $this->db->where('city_subscription_plan.id',$id)->get('city_subscription_plan');

		

		return $datatable->get("city_subscription_plan");

	}

	public function get_lawyer_list($datatable,$list_type){

		$datatable->db_order();



		$datatable->db_search(array(

			"users.first_name",

			),

			true // Cache query

		);



		$this->db->select("users.*,coreprofile.*")

				->join("coreprofile", "coreprofile.userid = users.ID")

				->where("users.verification_level",6)

				->where("users.admin_verified",$list_type);



		return $datatable->get("users");

	}

	public function get_total_lawyers($list_type){

		

		return $this->db->select("*")

				->join("coreprofile", "coreprofile.userid = users.ID")

				->where("users.verification_level","6")

				->where("users.admin_verified",$list_type)

				->get("users")->num_rows();

	}

	public function get_user_info($id){

		return $this->db->select("*")

				->where("users.ID",$id)

				->get("users")->row_Array();

	}

	public function get_coreprofile($userid){

		return $this->db->where("userid", $userid)->get("coreprofile")->row_Array();	

	}

	public function update_user($data,$id) 

	{

		return $this->db->where("ID",$id)->update("users",$data);

	}

	public function insert_user_verification_log($data){

		return $this->db->insert("user_verification_logs",$data);

	}

	public function get_verification_log($userid){

		return $this->db->select("user_verification_logs.*")

				->join("user_verification_logs", "user_verification_logs.user_id = users.ID")

				->where("users.ID",$userid)

				->order_by('user_verification_logs.id','DESC')

				->get("users")->result_array();

	}

	public function get_consultation_timing($userid){

		return $this->db->select("*")->from("lawyers_consultation_timing")->where("user_id",$userid)->get()->result_array();

	}

	public function update_court_data($data,$courtid){

		return $this->db->where("id",$courtid)->update("courts",$data);

	}

	public function add_rewards_points($data){

		return $this->db->insert("rewards_points_earn", $data);	

	}

	public function add_score($data){

		return $this->db->insert("scores", $data);	

	}

	public function updatepoints($userid,$data){

		return $this->db->where("ID", $userid)->update("users",$data);	

	}







	//Court

	public function get_total_courts() {

		return $this->db->from("courts")->count_all_results();

	}

	public function get_courts($datatable) {

		$datatable->db_order();



		$datatable->db_search(array(

			"courts.court_name",

			),

			true // Cache query

		);

		return $datatable->get("courts");

	}



	public function get_court($id)  {

		return $this->db->where("id", $id)->get("courts");

	}



	public function add_court($data) {

		$this->db->insert("courts", $data);

	}



	public function update_court($id, $data) {

		$this->db->where("id", $id)->update("courts", $data);

	}



	public function delete_court($id) {

		$this->db->where("id", $id)->delete("courts");

	}





}





?>

