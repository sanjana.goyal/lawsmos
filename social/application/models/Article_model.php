<?php

class Article_Model extends CI_Model 
{

	public function get_user_article($userid) 
	{
		return $this->db->where("userid", $userid)->get("user_articles");
	}
	public function get_user_article_for_review($catid) 
	{
	return $this->db->where("catagory", $catid)
					->where("Is_reviewed",1)
					->where("status",0)
					->select("user_articles.ID as articleid, user_articles.title as articletitle, user_articles.userid,
					user_articles.description, user_articles.keywords,user_articles.on_hold,user_articles.reviewby,
					users.ID as useridss, users.username,users.first_name, users.last_name, users.avatar,
					users.online_timestamp, users.username")
					->join("users", "users.ID = user_articles.userid")
		            ->get("user_articles");
	}

	public function get_user_articless($id) 
	{
		return $this->db->where("ID", $id)->get("user_articles");
	}

	public function get_user_article_by_id($id) 
	{
		return $this->db->where("ID", $id)->get("user_articles")->row_array();
	}
	

	public function get_user_articless_preview($id) 
	{
		return $this->db->where("user_articles.ID", $id)
						->select("user_articles.ID as articleid, user_articles.title as articletitle, user_articles.userid,
					user_articles.description, user_articles.keywords,
					users.ID as useridss, users.first_name, users.last_name, users.avatar,
					users.online_timestamp, users.username,users.points,users.bonus_points,users.total_points")
					->join("users", "users.ID = user_articles.userid")
					->get("user_articles");
	}				

	public function get_user_article_sections($id) 
	{
		return $this->db->where("articleid", $id)->get("user_articles_posts");
	}
	
	public function get_user_article_sections_parent($id) 
	{
		return $this->db->where("articleid", $id)->where("parentid",0)->get("user_articles_posts");
	}
	
	public function get_user_article_sectionssss($id) 
	{
		return $this->db->where("ID", $id)->get("user_articles_posts");
	}
	
	public function get_preview_article_sections($id){
		
		return $this->db->where("user_articles_posts.articleid", $id)
			->select("user_articles.ID as articleid, user_articles.title as articletitle, user_articles.userid,
				user_articles.description, user_articles.keywords, user_articles_posts.articleid as artid,user_articles_posts.title as sectiontitle,
				user_articles_posts.ID as sectionid, user_articles_posts.body,user_articles_posts.image,user_articles_posts.views,
				users.ID as userids, users.first_name, users.last_name, users.avatar,
				users.online_timestamp, users.username")
			->join("user_articles", "user_articles.ID = user_articles_posts.articleid")
			->join("users", "users.ID = user_articles.userid")
			->get("user_articles_posts");
		}


	public function get_tree_menu($id){
		return  $this->db->where("user_articles_posts.articleid",$id)
					->select('*')
				    ->order_by('parentid')
					->get("user_articles_posts");
					
 		}
	public function generateTree($items = array(), $parent_id = 0)
	{
		
	$tree = '<ul>';
		for($i=0, $ni=count($items); $i < $ni; $i++){
			if($items[$i]['parentid'] == $parent_id){
				//$data=implode(' ', $items[$i]['ID']);
				$tree .= '<li>';
				$tree .= "<a href='#".$items[$i]['ID']."section'>".$items[$i]['title']."</a>";
				$tree .= $this->generateTree($items, $items[$i]['ID']);
				$tree .= '</li>';
			}
		}
    $tree .= '</ul>';
    return $tree;
	
	}
	public function add_article($data) 
	{
		$this->db->insert("user_articles", $data);
	}

	public function delete_blog($id) 
	{
		$this->db->where("ID", $id)->delete("user_blogs");
	}

	public function update_article($id,$data){
		
		$this->db->where("ID", $id)->update("user_articles", $data);
		
		}
		
	public function update_review_status($id,$data){
		
		$this->db->where("ID", $id)->update("user_articles", $data);
		}
		
		
		
	public function update_article_sections($id,$data){
		
		$this->db->where("ID", $id)->update("user_articles_posts", $data);
		
		}

	public function edit_heading($id,$data)
	{
	
		$this->db->where("ID", $id)->update("user_articles", $data);
	
	}
	public function edit_section_heading($id,$data)
	{
	
		$this->db->where("ID", $id)->update("user_articles_posts", $data);
	
	}
	public function edit_article_content($id,$data)
	{
	
		$this->db->where("ID", $id)->update("user_articles", $data);
	
	}

	public function edit_section_content($id,$data)
	{
	
		$this->db->where("ID", $id)->update("user_articles_posts", $data);
	
	}

	public function get_blog($id) 
	{
		return $this->db->where("ID", $id)->get("user_blogs");
	}

	
	public function add_article_section($data) 
	{
		$this->db->insert("user_articles_posts", $data);
		return $this->db->insert_ID();
	}

	public function update_post($id, $data) 
	{
		$this->db->where("ID", $id)->update("user_blog_posts", $data);
	}

	public function delete_post($id) 
	{
		$this->db->where("ID", $id)->delete("user_blog_posts");
	}

	public function delete_post_article_main($id) 
	{
		$this->db->where("ID", $id)->delete("user_articles");
	}
	
	
	
	public function delete_post_article_sectionss($id) 
	{
		$this->db->where("articleid", $id)->delete("user_articles_posts");
	}

	public function get_post($id) 
	{
		return $this->db->where("ID", $id)->get("user_blog_posts");
	}
	public function get_post_article_main($id) 
	{
		return $this->db->where("ID", $id)->get("user_articles");
	}


	public function get_total_articles($id){
		
		 $count =$this->db->where("userid", $id)->from("user_articles")->count_all_results();
		
			if($count==0){
					return 0;
				}
			else{
				
					return $count;
				}
		}
	



	public function get_total_article_posts($id) 
	{
		return $this->db->where("articleid", $id)->from("user_articles_posts")->count_all_results();
	}

	public function get_article_posts($blogid, $datatable) 
	{
		$datatable->db_order();

		$datatable->db_search(array(
			"user_articles_posts.title",
			),
			true // Cache query
		);
		$this->db->where("user_articles_posts.articleid", $blogid);
		$this->db->select("user_articles_posts.title, user_articles_posts.ID, user_articles_posts.body ,user_articles_posts.position_order,user_articles_posts.timestamp,
			user_articles_posts.last_updated, user_articles_posts.status, user_articles_posts.image, user_articles_posts.views,user_articles_posts.parentid");

		return $datatable->get("user_articles_posts");
	}
	public function get_articles($userid, $datatable) 
	{
		$datatable->db_order();

		$datatable->db_search(array(
			"user_articles.title",
			),
			true // Cache query
		);
		$this->db->where("user_articles.userid", $userid);
		$this->db->select("user_articles.title,user_articles.catagory,user_articles.subcatagory, user_articles.ID, user_articles.timestamp,
			, user_articles.private,user_articles.description,user_articles.keywords,user_articles.userid,user_articles.Is_reviewed,user_articles.status,catagories.c_name");
		$this->db->join("catagories","catagories.cid = user_articles.catagory");
		
		return $datatable->get("user_articles");
	}

	public function delete_all_blog_posts($blogid) 
	{
		$this->db->where("blogid", $blogid)->delete("user_blog_posts");
	}

	public function get_new_public_posts($page) 
	{
		return $this->db->where("user_blog_posts.status", 1)->where("user_blogs.private", 0)
			->select("user_blog_posts.ID, user_blog_posts.title, user_blog_posts.body,
				user_blog_posts.status, user_blog_posts.last_updated, user_blog_posts.image,
				user_blogs.ID as blogid, user_blogs.userid,
				users.ID as userid, users.first_name, users.last_name, users.avatar,
				users.online_timestamp, users.username")
			->join("user_blogs", "user_blogs.ID = user_blog_posts.blogid")
			->join("users", "users.ID = user_blogs.userid")
			->limit(10, $page)
			->order_by("user_blog_posts.timestamp", "desc")
			->get("user_blog_posts");
	}

	public function get_total_public_posts() 
	{
		return $this->db->where("user_blog_posts.status", 1)->where("user_blogs.private", 0)
			->join("user_blogs", "user_blogs.ID = user_blog_posts.blogid")
			->join("users", "users.ID = user_blogs.userid")
			->from("user_blog_posts")->count_all_results();
	}

	public function get_total_blog_posts_published($blogid) 
	{
		return $this->db
			->where("user_blog_posts.blogid", $blogid)
			->where("user_blog_posts.status", 1)->from("user_blog_posts")
			->count_all_results();
	}

	public function get_blog_posts_published($blogid, $page) 
	{
		return $this->db
			->where("user_blog_posts.status", 1)->where("user_blog_posts.blogid", $blogid)
			->select("user_blog_posts.ID, user_blog_posts.title, user_blog_posts.body,
				user_blog_posts.status, user_blog_posts.last_updated, user_blog_posts.image,
				user_blogs.ID as blogid, user_blogs.userid,
				users.ID as userid, users.first_name, users.last_name, users.avatar,
				users.online_timestamp, users.username")
			->join("user_blogs", "user_blogs.ID = user_blog_posts.blogid")
			->join("users", "users.ID = user_blogs.userid")
			->limit(10, $page)
			->order_by("user_blog_posts.timestamp", "desc")
			->get("user_blog_posts");
	}

}


?>
