<?php

class Funds_Model extends CI_Model 
{

	public function get_plans() 
	{
		// return $this->db->get("payment_plans");
		return $this->db->where('status',1)->get("payment_plans");
	}

	public function get_plan($id) 
	{
		return $this->db->where("ID", $id)->get("payment_plans");
	}

	public function update_plan($id, $data) 
	{	
		$this->db->where("ID", $id)->update("payment_plans", $data);
	}
	
	public function get_plan_price_detail($id,$type) 
	{	
		if($type==0){
			//city plan
			return $plan = $this->db->from('city_plan_pricelist')->order_by('name','asc')->get();
		}
		elseif($type==1){
			//search plan 
			return  $plan = $this->db->from('search_plan_pricelist')->order_by('name','asc')->get();
			
		}
		
	}

	public function get_all_city_plan() 
	{	
		return $plan = $this->db->get('search_plan_pricelist');
		
	}
	
	public function get_all_city() 
	{	
		return $this->db->from('city')->order_by('name','asc')->get();
		

	}


	
}

?>
