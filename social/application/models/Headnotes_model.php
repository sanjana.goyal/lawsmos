<?php

class Headnotes_Model extends CI_Model 
{

	//get headnotes by user_id
	public function getheadnotes($id) 
	{
		return $this->db->where('created_by',$id)->order_by("created_at", "desc")->get("judgement_headnotes");
	}
	
	//get specfic headnote 
	
	public function get_single_headnote($user_id,$id) 
	{
		return $this->db->where('created_by',$user_id)->where('id',$id)->get("judgement_headnotes");
	}


	public function delete_headnotes($user_id,$id) 
	{
	
		return $this->db->where('created_by',$user_id)->where('id',$id)->delete("judgement_headnotes");
	}

}

?>
