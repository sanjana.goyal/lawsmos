<?php



class Home_Model extends CI_Model

{



	public function get_home_stats()

	{

		return $this->db->get("home_stats");
	}



	public function update_home_stats($stats)

	{

		$this->db->where("ID", 1)->update(
			"home_stats",
			array(

				"google_members" => $stats->google_members,

				"facebook_members" => $stats->facebook_members,

				"twitter_members" => $stats->twitter_members,

				"total_members" => $stats->total_members,

				"new_members" => $stats->new_members,

				"active_today" => $stats->active_today,

				"timestamp" => time()

			)

		);
	}



	public function appointmentbooking($data)
	{





		$q = $this->db->where("service_provider_id =", $data['service_provider_id'])->where("service_seeker_id=",  $data['service_seeker_id'])->where("dateofappointment =", $data['dateofappointment'])->get("appointmentbooking");



		$count = $q->num_rows();



		if ($count === 0) {



			return $this->db->insert("appointmentbooking", $data);
		}
	}



	public function updatepoints($userid, $data)
	{



		return $this->db->where("ID", $userid)->update("users", $data);
	}



	public function get_email_template($id)

	{

		return $this->db->where("ID", $id)->get("email_templates");
	}







	public function get_leads($city)
	{

		//  $city = "Mohali";



		return  $this->db->select('legal_leads.*,lead_price_slots.lead_price_slot,lead_price_slots.lead_points,

									lead_max_lawyer.lawyer_max,lead_max_lawyer.point_slab')

			->where("legal_leads.location", $city)

			->where("legal_leads.status", 0)

			->join("lead_price_slots", "lead_price_slots.id = legal_leads.lead_budget", "left")

			->join("lead_max_lawyer", "lead_max_lawyer.id = legal_leads.max_lawyer", "left")

			->limit(5)->order_by("legal_leads.create_time", "DESC")

			->get("legal_leads");





		// $data =  $this->db->limit(5)->order_by("legal_leads.create_time","DESC")

		// ->where("legal_leads.location", $city)

		// ->where("legal_leads.status", 0)

		// ->get("legal_leads");









	}

	public function get_email_template_hook($hook, $language)

	{

		return $this->db->where("hook", $hook)

			->where("language", $language)->get("email_templates");
	}



	public function get_random_ad()
	{
		return $this->db->where("pageviews >", 0)->where("status", 2)->order_by("RAND()")->get("rotation_ads");
	}
	public function get_coreprofile($userid)
	{
		return $this->db->where("userid", $userid)->get("coreprofile");
	}
	public function get_coreprofile_seeker($userid)
	{
		return $this->db->where("userid", $userid)->get("coreprofile_seeker");
	}
	public function get_cities($state)
	{
		return $this->db->where("state_id", $state)->get("city");
	}
	public function get_coreprofile_student($userid)
	{
		return $this->db->where("userid", $userid)->get("coreprofile_student");
	}
	
	public function get_coreprofile_professor($userid)
	{
		return $this->db->where("userid", $userid)->get("coreprofile_professor");
	}

	



	public function decrease_ad_pageviews($id)

	{

		$this->db->where("ID", $id)->set("pageviews", "pageviews-1", FALSE)->update("rotation_ads");
	}



	public function add_rotation_ad($data)

	{

		$this->db->insert("rotation_ads", $data);
	}



	public function insertcoreprofile($data)
	{



		$this->db->insert("coreprofile", $data);
	}



	public function insertstudentprofile($data)
	{



		$this->db->insert("student_profile", $data);
	}





	public function upload_proof_for_upgrade($data)

	{



		$q = $this->db->where("userid =", $data['userid'])->get("verification_proof");

		$count = $q->num_rows();



		if ($count === 0) {



			return $q = $this->db->insert("verification_proof", $data);
		}
	}

	public function add_promoted_post($data)

	{

		$this->db->insert("promoted_posts", $data);
	}



	public function get_promoted_post_by_postid($postid)

	{

		return $this->db->where("postid", $postid)->get("promoted_posts");
	}



	public function get_all_courts()

	{

		return $this->db->where('status', 1)->group_by('court_name')->order_by('court_name', 'asc')->get("courts");
	}

	public function updatecoreprofile($date, $userid)
	{
		return $this->db->where('userid', $userid)->update("coreprofile", $date);
	}
	public function updatecoreprofile_seeker($date, $userid)
	{
		return $this->db->where('userid', $userid)->update("coreprofile_seeker", $date);
	}
	public function updatecoreprofile_student($date, $userid)
	{
		return $this->db->where('userid', $userid)->update("coreprofile_student", $date);
	}
	public function updatecoreprofile_professor($date, $userid)
	{
		return $this->db->where('userid', $userid)->update("coreprofile_professor", $date);
	}

	public function step1coreprofile($date)
	{
		return $this->db->insert("coreprofile", $date);
	}
	public function insertcoreprofile_seeker($date)

	{
		return $this->db->insert("coreprofile_seeker", $date);
	}
	public function insertcoreprofile_student($date)
	{
		return $this->db->insert("coreprofile_student", $date);
	}
	public function insertcoreprofile_professor($date)
	{
		return $this->db->insert("coreprofile_professor", $date);
	}


	public function step2coreprofile($date, $userid)

	{

		return $this->db->where('userid', $userid)->update("coreprofile", $date);
	}

	public function step3coreprofile($date, $userid)

	{

		return $this->db->where('userid', $userid)->update("coreprofile", $date);
	}

	public function step4coreprofile($date, $userid)

	{

		return $this->db->where('userid', $userid)->update("coreprofile", $date);
	}



	public function update_user_level($data, $id)

	{

		return $this->db->where("ID", $id)->update("users", $data);
	}

	public function update_user($data, $id)

	{

		return $this->db->where("ID", $id)->update("users", $data);
	}

	public function state_bar_no($councel_registration_no, $userid)

	{

		if (empty($councel_registration_no)) {

			return "";
		}

		return $this->db->where("councel_registration_no", $councel_registration_no)->where('userid !=', $userid)->get("coreprofile")->result();
	}



	public function bar_registration_no($registration_no, $userid)

	{

		if (empty($registration_no)) {

			return "";
		}

		return $this->db->where("registration_no", $registration_no)->where('userid !=', $userid)->get("coreprofile")->result();
	}

	public function add_new_court($add_court)

	{

		$data['court_name'] = $add_court;

		$data['status'] = 0;

		$this->db->insert("courts", $data);

		return $this->db->insert_id();
	}



	public function add_new_courts($add_court)

	{

		$affected_ids = [];

		foreach ($add_court as $key => $value) {

			$if_exist = $this->home_model->get_court_by_name($value);

			$data = ['court_name' => $value, 'status' => 0];

			if ($if_exist) {

				$affected_ids[] = $if_exist;
			} else {

				$this->db->insert("courts", $data);

				$affected_ids[] = $this->db->insert_id();
			}
		}

		return $affected_ids;

		// foreach ($add_court as $key => $value) {

		// 	$data[] = ['court_name' => $value, 'status' => 0];

		// }

		// // $data['court_name'] = $add_court;

		// // $data['status'] = 0;

		// $test = $this->db->insert_batch("courts",$data);

		// var_dump($test);die;

		// return $this->db->insert_id();



		// $first_id = $this->db->insert_id();

		// $last_id = $first_id + ($count-1);

	}



	public function getUsersDetail($id)

	{

		return $this->db->where("ID", $id)->get("users");
	}

	public function get_all_states()

	{

		return $this->db->order_by('name', 'asc')->get("state");
	}

	public function get_city_by_state_id($state_id)

	{

		return $this->db->select('id,name,state_id')->where('state_id', $state_id)->get("city");
	}

	public function delete_consultation_timing($id)
	{

		return $this->db->where("user_id", $id)->delete("lawyers_consultation_timing");
	}

	public function insert_consultation_timing($data)
	{

		return $this->db->insert('lawyers_consultation_timing', $data);
	}
	public function get_consultation_timing($id)
	{

		return $this->db->where("user_id", $id)->get("lawyers_consultation_timing")->result_array();
	}

	public function add_notification($data)
	{

		$this->db->insert("user_notifications", $data);
	}

	public function getUsersDetailByEmail($email)

	{

		return $this->db->where("email", $email)->get("users")->row_array();
	}



	public function get_court_by_name($name)
	{

		$court = $this->db->where("court_name", $name)->get("courts");

		if ($court->num_rows() > 0) {

			$court_result = $court->row_array();

			return $court_result['id'];
		}

		return false;
	}

	public function getUsersCount($profile_identification)
	{
		$user_count = $this->db->select("*")->from("users")->where_in("profile_identification", $profile_identification)->get()->num_rows();
		return $user_count;
	}
}
