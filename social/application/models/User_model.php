<?php

class User_Model extends CI_Model
{

	public function getUser($email, $pass)
	{
		return $this->db->select("ID")
			->where("email", $email)->where("password", $pass)->get("users");
	}

	public function get_user_by_id($userid)
	{
		return $this->db->where("ID", $userid)->get("users");
	}



	public function get_user_by_id_and_token($userid, $token)
	{
		return $this->db->where("ID", $userid)->where("verification_token", $token)->get("users");
	}


	public function get_email_list()
	{

		return $this->db->get("email");
	}

	public function update_agency_status($userid, $data)
	{

		$this->db->where("created_by_user_id", $userid)->update("agency", $data);
	}
	public function update_pages_status($pageid, $data)
	{

		$this->db->where("ID", $pageid)->update("pages", $data);
	}

	public function get_high_court_unique_years()
	{
		return $this->db->distinct()
			->select('year')
			->order_by('year', 'ASC')
			->get("high_court_ap_yearly");
		// ->result_array();
	}

	public function get_supreme_court_unique_years()
	{
		return $this->db->distinct()
			->select('year')
			->order_by('year', 'ASC')
			->get("supreme");
		// ->result_array();
	}

	public function get_high_court_unique_courts()
	{
		return $this->db->distinct()
			->select('state,statecode')

			->get("high_court_ap_yearly");
		// ->result_array();

	}


	public function get_reasearch_by_id($userid)
	{


		return $this->db
			->where("research.userid", $userid)
			->select("research.ID,research.userid,research.reasearch_heading,research.research_body,research.created_at,research.updated_at,catagories.c_name,catagories.cat_slug")
			->join("catagories", "catagories.cid = research.catagory ")
			->get("research");
	}

	public function get_reasearch_by_rowid($id)
	{


		return $this->db->where("research.ID", $id)->get("research")->row_array();
	}

	public function update_reasearch_by_rowid($id, $data)
	{
		$this->db->where("ID", $id)->update("research", $data);
	}


	public function get_reasearch_by_user($userid, $caseid)
	{

		return $this->db
			->where("research.userid", $userid)
			->where("research.ID", $caseid)
			->select("research.ID,research.userid,research.reasearch_heading,research.research_body,research.created_at,research.updated_at,catagories.c_name,catagories.cat_slug,research.subcat")
			->join("catagories", "catagories.cid = research.catagory")
			->get("research");
	}


	public function get_catagories()
	{


		return $this->db->where("parent_id", 0)->order_By('c_name','ASC')->get("catagories");
	}
	public function get_catagories_data()
	{
		return $this->db->where("parent_id", 0)->order_By('c_name', 'ASC')->get("catagories");
	}
	public function get_all_catagories()
	{
		return $this->db->where("parent_id", 0)
			->where_not_in("cid", [114, 118, 119])
			->order_By('c_name', 'ASC')
			->get("catagories");
	}
	public function get_case_catagories_data()
	{
		return $this->db->join('case_editor', 'case_editor.case_category = catagories.cid')->where("parent_id", 0)->group_by('catagories.cid')->order_By('c_name', 'ASC')->get("catagories");
	}

	public function get_catagories_by_id($id)
	{

		return $this->db->where("cid", $id)->get("catagories");
	}
	public function get_sub_catag($id)
	{

		return $this->db->where("parent_id", $id)->get("catagories");
	}
	public function view_leads($id)
	{

		return $this->db->where("id", $id)->where("status", 0)->get("legal_leads");
	}

	public function view_leads_detail($id)
	{

		// return $this->db->where("id", $id)->where("status",0)->get("legal_leads");

		return $this->db->where("legal_leads.id", $id)
			->where("legal_leads.status", 0)
			->select('legal_leads.id,legal_leads.issue, legal_leads.subissue,legal_leads.action,
							          legal_leads.name,legal_leads.email,legal_leads.phone,legal_leads.calltime,legal_leads.location,legal_leads.shortdescription,legal_leads.create_time,legal_leads.recieve_emails,lead_price_slots.lead_price_slot,lead_price_slots.lead_points,lead_max_lawyer.lawyer_max,lead_max_lawyer.point_slab')
			->join("lead_price_slots", "lead_price_slots.id = legal_leads.lead_budget", "left")
			->join("lead_max_lawyer", "lead_max_lawyer.id = legal_leads.max_lawyer", "left")
			->get('legal_leads');
	}

	public function view_cat_price($id)
	{


		return $this->db->select("price")
			->where("cid", $id)->get("catagories")->result();
	}

	public function deductbalance($userid, $points, $id, $price)
	{

		$data = [
			"user_id" => $userid,
			"ref_id" => $id,
			"points" => $price,
			"type" => 2
		];

		$update_deduction_point = $this->db->insert('point_deduction_logs', $data);

		return $this->db
			->set("points", $points)
			->where("ID", $userid)
			->update("users");
	}
	public function get_my_win_cases_list($userid)
	{


		$s = $this->db->where("userid", $userid)->get("case_upload");
		return $r = $s->num_rows();
	}

	public function get_my_win_cases($userid)
	{

		$s = $this->db
			->where("case_upload.userid", $userid)

			->select("COUNT(*) as num")

			->get("case_upload");

		$r = $s->row();


		if (isset($r->num)) return $r->num;
		return 0;
	}
	public function get_cases_win($userid, $datatable)
	{



		$datatable->db_order();


		$datatable->db_search(
			array(
				"case_upload.caseheading",
				"case_upload.courtname",
			)
		);


		return $this->db
			->where("case_upload.userid", $userid)
			->order_By("case_upload.id", "DESC")
			->get("case_upload");

		//return $this->db->where("userid", $userid)->get("case_upload");


		return $datatable->get("case_upload");
	}

	public function get_agency_by_id($userid)
	{

		return $this->db->where("created_by_user_id", $userid)->get("agency");
	}

	public function get_university_by_id($userid)
	{

		return $this->db->where("created_by_user_id", $userid)
			->join("universities", "universities.ID = university.universityid")
			->get("university");
	}

	public function get_appoint_by_id($id)
	{

		return $this->db->where("ID", $id)->get("appointmentbooking");
	}


	public function get_appointments_by_seekerid($id)
	{


		return $this->db->where("service_seeker_id", $id)
			->where("status", 1)
			->join("users", "users.ID = appointmentbooking.service_provider_id")
			->get("appointmentbooking");
	}


	public function get_rating_profiles($userid)
	{


		//Fetch records
		// Posts
		$this->db->select('*');
		$this->db->from('appointmentbooking');
		$this->db->where('service_seeker_id', $userid);
		$this->db->where('appointmentbooking.status', "1");

		$this->db->order_by("users.ID", "asc");
		$this->db->join("users", "users.ID = appointmentbooking.service_provider_id");
		$this->db->group_by("appointmentbooking.service_provider_id");

		$postsquery = $this->db->get();

		$postResult = $postsquery->result_array();

		$posts_arr = array();


		foreach ($postResult as $post) {


			$id = $post['ID'];
			$service_provider_id = $post['service_provider_id'];
			$service_seeker_id = $post['service_seeker_id'];
			$seeker_name = $post['seeker_name'];
			$seeker_contact = $post['seeker_contact'];
			$dateofappointment = $post['dateofappointment'];
			$timeofappoint = $post['timeofappoint'];
			$matter = $post['matter'];
			$reject_reason = $post['reject_reason'];
			$reject_note = $post['reject_note'];

			// User rating
			$this->db->select('rating,comment');
			$this->db->from('rating');
			$this->db->where("lawyerid", $service_provider_id);
			$this->db->where("seekerid", $service_seeker_id);

			$userRatingquery = $this->db->get();

			$userpostResult = $userRatingquery->result_array();

			$userRating = 0;
			$userComment = 0;
			if (count($userpostResult) > 0) {
				$userRating = $userpostResult[0]['rating'];
				$userComment = $userpostResult[0]['comment'];
			}

			// Average rating
			$this->db->select('ROUND(AVG(rating),1) as averageRating');
			$this->db->from('rating');
			$this->db->where("lawyerid", $service_provider_id);
			$this->db->where("seekerid", $service_seeker_id);

			$ratingquery = $this->db->get();

			$postResult = $ratingquery->result_array();

			$rating = $postResult[0]['averageRating'];


			if ($rating == '') {
				$rating = 0;
			}

			$posts_arr[] = array("userdata" => $post, "averageRating" => $rating, "userrating" => $userRating, "userComment" => $userComment);
		}

		return $posts_arr;
	}

	public function get_city_rank($id)
	{
		return $this->db->where("ID", $id)->get("cityrankss");
	}
	public function get_state_rank($id)
	{
		return $this->db->where("ID", $id)->get("staterankss");
	}
	public function get_country_rank($id)
	{
		return $this->db->where("ID", $id)->get("countryrankss");
	}
	public function create_city_rank_view($city)
	{
		$sql = "create or replace view staterankss as SELECT * , FIND_IN_SET( `total_points`, (
SELECT GROUP_CONCAT( `total_points`


ORDER BY `total_points` DESC 

) 
FROM users where MarkerId='Lawyer' and city='" . $city . "')
) AS rank 

FROM users where MarkerId='Lawyer' and city='" . $city . "' order by total_points desc";

		return $this->db->query($sql);
	}
	public function create_country_rank_view($country)
	{
		$sql = "create or replace view countryrankss as SELECT * , FIND_IN_SET( `total_points`, (
SELECT GROUP_CONCAT( `total_points`


ORDER BY `total_points` DESC 

) 
FROM users where MarkerId='Lawyer' and country='" . $country . "')
) AS rank 

FROM users where MarkerId='Lawyer' and country='" . $country . "' order by total_points desc";

		return $this->db->query($sql);
	}
	public function create_state_rank_view($state)
	{
		$sql = "create or replace view cityrankss as SELECT * , FIND_IN_SET( `total_points`, (
SELECT GROUP_CONCAT( `total_points`


ORDER BY `total_points` DESC 

) 
FROM users where MarkerId='Lawyer' and state='" . $state . "')
) AS rank 

FROM users where MarkerId='Lawyer' and state='" . $state . "' order by total_points desc";

		return $this->db->query($sql);
	}

	public function get_leaders_by_points()
	{

		$sql = "SELECT * , FIND_IN_SET( `total_points`, (
SELECT GROUP_CONCAT( `total_points`


ORDER BY `total_points` DESC 

) 
FROM users where MarkerId='Lawyer')
) AS rank 

FROM users where MarkerId='Lawyer' order by total_points desc limit 10";

		return $this->db->query($sql);
	}
	public function get_all_leaders()
	{

		$sql = "SELECT * , FIND_IN_SET( `total_points`, (
SELECT GROUP_CONCAT( `total_points`


ORDER BY `total_points` DESC 

) 
FROM users )
) AS rank 

FROM users  order by total_points desc";

		return $this->db->query($sql);
	}

	public function get_rating_value($id)
	{
		$this->db->select('ROUND(AVG(rating),1) as averageRating');
		$this->db->from('rating');
		$this->db->where("lawyerid", $id);
		$ratingquery = $this->db->get();

		$postResult = $ratingquery->result_array();

		return $rating = $postResult[0]['averageRating'];
	}
	public function get_total_rating_count($id)
	{
		return $this->db->select('*')->from('rating')->where('lawyerid', $id)->get()->num_rows();
	}
	public function get_recent_best_review($id)
	{
		$info = $this->db->select('*')->from('rating')->where('lawyerid', $id)->where('comment !=', "")->order_by('rating', 'DESC')->order_by('id', 'DESC')->get()->row_array();
		return $info;
	}
	public function get_user_core_profile($id, $roleid)
	{
		if ($roleid == 0) {
			return $this->db->select('*')->from('coreprofile')->where('userid', $id)->get()->row_array();
		} elseif ($roleid == 2) {
			return $this->db->select('*')->from('coreprofile_student')->where('userid', $id)->get()->row_array();
		} elseif ($roleid == 1) {
			return $this->db->select('*')->from('coreprofile_seeker')->where('userid', $id)->get()->row_array();
		} elseif ($roleid == 3) {
			return $this->db->select('*')->from('coreprofile_professor')->where('userid', $id)->get()->row_array();
		}
	}

	public function userRating($lawyerid, $seekerid, $rating)
	{

		$this->db->select('*');
		$this->db->from('rating');
		$this->db->where("lawyerid", $lawyerid);
		$this->db->where("seekerid", $seekerid);
		$userRatingquery = $this->db->get();
		$userRatingResult = $userRatingquery->result_array();

		if (count($userRatingResult) > 0) {

			$postRating_id = $userRatingResult[0]['ID'];
			// Update
			$value = array('rating' => $rating);
			$this->db->where('ID', $postRating_id);
			$this->db->update('rating', $value);
		} else {
			$userRating = array(
				"lawyerid" => $lawyerid,
				"seekerid" => $seekerid,
				"rating" => $rating
			);

			$this->db->insert('rating', $userRating);
		}

		//Average rating
		$this->db->select('ROUND(AVG(rating),1) as averageRating');
		$this->db->from('rating');
		$this->db->where("lawyerid", $lawyerid);
		$ratingquery = $this->db->get();

		$postResult = $ratingquery->result_array();

		$rating = $postResult[0]['averageRating'];

		if ($rating == '') {
			$rating = 0;
		}

		return $rating;
	}

	public function updatereview($lawyerid, $seekerid, $reviewtext)
	{

		$this->db->select('*');
		$this->db->from('rating');
		$this->db->where("lawyerid", $lawyerid);
		$this->db->where("seekerid", $seekerid);
		$userRatingquery = $this->db->get();

		$userRatingResult = $userRatingquery->result_array();
		if (count($userRatingResult) > 0) {

			$postRating_id = $userRatingResult[0]['ID'];
			// Update
			$value = array('comment' => $reviewtext);
			$this->db->where('ID', $postRating_id);
			$this->db->update('rating', $value);
		}

		$this->db->select('*');
		$this->db->from('rating');
		$this->db->where("lawyerid", $lawyerid);
		$ratingquery = $this->db->get();

		$postResult = $ratingquery->result_array();

		$comment = $postResult[0]['comment'];

		if ($comment == '') {
			$comment = 0;
		}

		return $comment;
	}

	function html_output($userid)
	{
		$data = $this->get_appointments_by_seekerid($userid);
		$output = '';
		foreach ($data->result_array() as $row) {
			$color = '';
			$rating = $this->get_business_rating($row["service_provider_id"]);
			$output .= '
   
   <h3 class="text-primary">' . $row["first_name"] . " " . $row["last_name"] . '</h3>
   <ul class="list-inline" data-rating="' . $rating . '" title="Average Rating - ' . $rating . '">
   ';
			for ($count = 1; $count <= 5; $count++) {
				if ($count <= $rating) {
					$color = 'color:#ffcc00;';
				} else {
					$color = 'color:#ccc;';
				}

				$output .= '<li title="' . $count . '" id="' . $row['ID'] . '-' . $count . '" data-indexid="' . $row['ID'] . '" data-index="' . $count . '" data-seeker_id="' . $row["service_seeker_id"] . '" data-user_id="' . $row["service_provider_id"] . '" data-rating="' . $rating . '" class="rating" style="cursor:pointer; ' . $color . ' font-size:24px;">&#9733;</li>';
			}
			$output .= '</ul>
   <p>' . $row["city"] . '</p>
   <label style="text-danger">Date of Appointment: ' . $row["dateofappointment"] . '</label>
   <br>
   <textarea id="review-content" class="form-control"></textarea>
   <button class="btn btn-danger" id="submit-danger-review">post review</button>
   <hr />
   ';
		}
		echo $output;
	}



	function get_business_rating($user_id)
	{
		$this->db->select('AVG(rating) as rating');
		$this->db->from('rating');
		$this->db->where('lawyerid', $user_id);
		$data = $this->db->get();
		foreach ($data->result_array() as $row) {
			return $row["rating"];
		}
	}

	public function get_agency_detail($pageid)
	{

		return $this->db->where("ID", $pageid)->get("pages");
	}

	public function get_law_firm_users($userid)
	{

		return $this->db->where("userid", $userid)->get("page_users");
	}

	public function get_user_by_username($username)
	{
		return $this->db->where("username", $username)->get("users");
	}

	public function delete_user($id)
	{
		$this->db->where("ID", $id)->delete("users");
	}

	public function del_research_by_id($id)
	{

		$this->db->where("ID", $id)->delete("research");
	}

	public function get_new_members($limit)
	{
		return $this->db->select("email, username, joined, oauth_provider, 
			avatar")
			->order_by("ID", "DESC")->limit($limit)->get("users");
	}

	public function get_registered_users_date($month, $year)
	{
		$s = $this->db->where("joined_date", $month . "-" . $year)->select("COUNT(*) as num")->get("users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}


	public function countleadlogs($userid, $id)
	{

		$s = $this->db->where("leadid", $id)->where("userid", $userid)->select("COUNT(*) as num")->get("leadslogs");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function countleadlogss($userid)
	{

		return $s = $this->db->where("userid", $userid)->get("leadslogs")->result();
	}

	public function countleadview($id)
	{

		$s = $this->db->where("leadid", $id)->get("leadslogs")->num_rows();
		return $s;
	}

	public function get_oauth_count($provider)
	{
		$s = $this->db->where("oauth_provider", $provider)->select("COUNT(*) as num")->get("users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}



	public function leads_count($city)
	{



		$s = $this->db
			->where("legal_leads.location LIKE", $city)
			->where("legal_leads.status", 0)

			->select("COUNT(*) as num")

			->get("legal_leads");


		$r = $s->row();



		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function leads_count_userid($userid)
	{



		$s = $this->db
			->where("legal_leads.user_id LIKE", $userid)
				->where("legal_leads.status", 0)
			->select("COUNT(*) as num")
				->get("legal_leads");


		$r = $s->row();



		if (isset($r->num)) return $r->num;else
		return 0;
	}



	public function get_total_requests_count($email)
	{

		$s = $this->db
			->where("legal_leads.email", $email)
			->select("*")
			->get("legal_leads");
		$r = $s->row();


		if (isset($r->num)) return $r->num;
		return 0;
	}
	public function get_total_apointments_counts($userid)
	{

		$s = $this->db
			->where("appointmentbooking.service_provider_id", $userid)
			->select("COUNT(*) as num")
			->join("users", "users.ID = appointmentbooking.service_seeker_id")
			->get("appointmentbooking");
		$r = $s->row();


		if (isset($r->num)) return $r->num;
		return 0;
	}


	public function get_total_earn_logs_count($userid)
	{

		$s = $this->db
			->where("rewards_points_earn.userid", $userid)
			->select("COUNT(*) as num")
			->join("users", "users.ID = rewards_points_earn.userid")
			->get("rewards_points_earn");
		$r = $s->row();




		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function get_total_apointment_counts($userid)
	{

		$s = $this->db
			->where("appointmentbooking.service_seeker_id", $userid)
			->select("COUNT(*) as num")
			->join("users", "users.ID = appointmentbooking.service_provider_id")
			->get("appointmentbooking");


		$r = $s->row();


		if (isset($r->num)) return $r->num;
		return 0;
	}



	public function get_leads($city, $datatable)
	{


		$datatable->db_order();

		$datatable->db_search(
			array(
				"legal_leads.shortdescription",
				"legal_leads.location",
				"legal_leads.action",
				"lead_price_slots.lead_price_slot"

			),
			true // Cache query
		);
		// $this->db
		// 	->where("legal_leads.location  LIKE",$city)
		// 	->where("legal_leads.status",0)
		// 	->order_by("legal_leads.create_time", "desc")
		// 	->select("legal_leads.id as leadid, legal_leads.issue, legal_leads.subissue,legal_leads.action,legal_leads.name,legal_leads.email, legal_leads.phone,legal_leads.calltime,legal_leads.location,legal_leads.shortdescription,legal_leads.create_time");

		$this->db->select('legal_leads.id as leadid,legal_leads.issue, legal_leads.subissue,legal_leads.action,
									legal_leads.name,legal_leads.email,legal_leads.phone,legal_leads.calltime,legal_leads.location,legal_leads.shortdescription,legal_leads.create_time,lead_price_slots.lead_price_slot,lead_price_slots.lead_points,lead_max_lawyer.lawyer_max,lead_max_lawyer.point_slab')
			->where("legal_leads.location", $city)
			->where("legal_leads.status", 0)
			->join("lead_price_slots", "lead_price_slots.id = legal_leads.lead_budget", "left")
			->join("lead_max_lawyer", "lead_max_lawyer.id = legal_leads.max_lawyer", "left")
			->order_by("legal_leads.create_time", "DESC")->limit(5);
		// ->get("legal_leads");	

		return $datatable->get("legal_leads");
	}

public function get_leads_userid($userid, $datatable)
	{


		$datatable->db_order();

		$datatable->db_search(
			array(
				"legal_leads.shortdescription",
				"legal_leads.location",
				"legal_leads.action",
				"lead_price_slots.lead_price_slot"

			),
			true // Cache query
		);
		// $this->db
		// 	->where("legal_leads.location  LIKE",$city)
		// 	->where("legal_leads.status",0)
		// 	->order_by("legal_leads.create_time", "desc")
		// 	->select("legal_leads.id as leadid, legal_leads.issue, legal_leads.subissue,legal_leads.action,legal_leads.name,legal_leads.email, legal_leads.phone,legal_leads.calltime,legal_leads.location,legal_leads.shortdescription,legal_leads.create_time");

		$this->db->select('legal_leads.id as leadid,legal_leads.issue, legal_leads.subissue,legal_leads.action,
									legal_leads.name,legal_leads.email,legal_leads.phone,legal_leads.calltime,legal_leads.location,legal_leads.shortdescription,legal_leads.create_time,lead_price_slots.lead_price_slot,lead_price_slots.lead_points,lead_max_lawyer.lawyer_max,lead_max_lawyer.point_slab')
			->where("legal_leads.user_id", $userid)
			->where("legal_leads.status", 0)
			->join("lead_price_slots", "lead_price_slots.id = legal_leads.lead_budget", "left")
			->join("lead_max_lawyer", "lead_max_lawyer.id = legal_leads.max_lawyer", "left")
			->order_by("legal_leads.create_time", "DESC")->limit(5);
		// ->get("legal_leads");	

		return $datatable->get("legal_leads");
	}





	public function get_user_service_requests($email, $datatable)
	{

		$datatable->db_order();

		$datatable->db_search(
			array(
				"legal_leads.issue",
				"legal_leads.subissue",
				"legal_leads.action",
				"legal_leads.create_time",


			),
			true // Cache query
		);
		$this->db
			->where("legal_leads.email", $email)
			->select("legal_leads.id, legal_leads.issue, legal_leads.subissue,
			legal_leads.action,legal_leads.phone,legal_leads.name,legal_leads.email,
			legal_leads.calltime,legal_leads.create_time,legal_leads.shortdescription,legal_leads.status");

		return $datatable->get("legal_leads");
	}

	public function get_appointmettss($start, $userid)
	{

		//WHERE date_field>=(CURDATE()-INTERVAL 1 MONTH);
		return $this->db->where("service_provider_id", $userid)->where("status", 1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}

	public function get_appointmettss_view_seekers($start, $userid)
	{

		return $this->db->where("appointmentbooking.service_seeker_id", $userid)
			->where("appointmentbooking.status", 1)
			->select("users.ID as userid, users.username, users.email,
							users.phone,users.first_name,users.last_name,appointmentbooking.ID,
							appointmentbooking.dateofappointment,appointmentbooking.timeofappoint,
							appointmentbooking.status, appointmentbooking.created_at")
			->join("users", "users.ID = appointmentbooking.service_provider_id")
			->order_by("appointmentbooking.dateofappointment", "ASC")
			->get("appointmentbooking");



		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}

	public function get_user_appointment($userid, $datatable)
	{

		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"users.first_name",
				"users.last_name",
				"users.email",
				"appointmentbooking.dateofappointment"

			),
			true // Cache query
		);
		$this->db
			->where("appointmentbooking.service_provider_id", $userid)
			->where("appointmentbooking.dateofappointment >=", date('m/d/Y')) //For current month
			->select("users.ID as userid, users.username, users.email,
			users.phone,users.first_name,users.last_name,appointmentbooking.ID,
			appointmentbooking.dateofappointment,appointmentbooking.timeofappoint,
			appointmentbooking.status, appointmentbooking.created_at")
			->join("users", "users.ID = appointmentbooking.service_seeker_id")
			->order_by("dateofappointment", "ASC")
			->limit(20);
		return $datatable->get("appointmentbooking");
	}

	public function get_user_appointments($userid, $datatable)
	{


		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"users.first_name",
				"users.last_name",
				"users.email",
				"appointmentbooking.dateofappointment"

			),
			true // Cache query
		);
		$this->db
			->where("appointmentbooking.service_seeker_id", $userid)
			->where("appointmentbooking.dateofappointment >=", date('m/d/Y')) //For current month
			->select("users.ID as userid, users.username, users.email,
			users.phone,users.first_name,users.last_name,
			appointmentbooking.dateofappointment,appointmentbooking.timeofappoint,
			appointmentbooking.status, appointmentbooking.created_at,appointmentbooking.reject_reason,appointmentbooking.reject_note")
			->join("users", "users.ID = appointmentbooking.service_provider_id");
		return $datatable->get("appointmentbooking");


		/*
		
		$datatable->db_order();

		$datatable->db_search(array(
			"appointmentbooking.dateofappointment",
			),
			true // Cache query
		);
		
		
		$this->db->select("dateofappointment, timeofappoint,status,created_at")->where("service_seeker_id",$userid);
			
		return $datatable->get("appointmentbooking");
		*/
	}


	public function get_total_earn_logs($userid, $datatable)
	{





		$datatable->db_order();

		$datatable->db_search(
			array(
				"rewards_points_earn.points",
				"rewards_points_earn.notes",

			)
		);

		return $this->db
			->where("rewards_points_earn.userid", $userid)
			->select("rewards_points_earn.userid, rewards_points_earn.points, rewards_points_earn.notes ,rewards_points_earn.created_at")

			//->limit($datatable->length, $datatable->start)
			->get("rewards_points_earn");
	}







	public function get_total_members_count()
	{
		$s = $this->db->select("COUNT(*) as num")->get("users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function get_active_today_count()
	{
		$s = $this->db->where("online_timestamp >", time() - 3600 * 24)->select("COUNT(*) as num")->get("users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function get_new_today_count()
	{
		$s = $this->db->where("joined >", time() - 3600 * 24)->select("COUNT(*) as num")->get("users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function get_online_count()
	{
		$s = $this->db->where("online_timestamp >", time() - 60 * 15)->select("COUNT(*) as num")->get("users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function get_members($datatable)
	{
		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"users.first_name",
				"users.last_name",
				"user_roles.name"
			)
		);

		return $this->db->select("users.username,users.profile_identification,users.email, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, users.online_timestamp, users.avatar,
			user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit($datatable->length, $datatable->start)
			->get("users");
	}





	public function get_members_admin($datatable)
	{
		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"users.first_name",
				"users.last_name",
				"user_roles.name",
				"users.email"
			)
		);

		return $this->db->select("users.username, users.email, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, users.online_timestamp, users.avatar,
			user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit($datatable->length, $datatable->start)
			->get("users");
	}

	public function get_members_by_search($search)
	{
		return $this->db->select("users.username, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit(20)
			->like("users.username", $search)
			->get("users");
	}

	public function search_by_username($search)
	{
		return $this->db->select("users.username, users.email, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit(20)
			->like("users.username", $search)
			->get("users");
	}

	public function search_by_email($search)
	{
		return $this->db->select("users.username, users.email, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit(20)
			->like("users.email", $search)
			->get("users");
	}

	public function search_by_first_name($search)
	{
		return $this->db->select("users.username, users.email, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit(20)
			->like("users.first_name", $search)
			->get("users");
	}

	public function search_by_last_name($search)
	{
		return $this->db->select("users.username, users.email, users.first_name, 
			users.last_name, users.ID, users.joined, users.oauth_provider,
			users.user_role, user_roles.name as user_role_name")
			->join(
				"user_roles",
				"user_roles.ID = users.user_role",
				"left outer"
			)
			->limit(20)
			->like("users.last_name", $search)
			->get("users");
	}

	public function get_notifications($userid)
	{
		return $this->db
			->where("user_notifications.userid", $userid)
			->select("users.ID as userid, users.username, users.avatar,
    			user_notifications.timestamp, user_notifications.message,
    			user_notifications.url, user_notifications.ID, 
    			user_notifications.status")
			->join("users", "users.ID = user_notifications.fromid")
			->limit(5)
			->order_By("user_notifications.ID", "DESC")
			->get("user_notifications");
	}

	public function get_notifications_unread($userid)
	{
		return $this->db
			->where("user_notifications.userid", $userid)
			->select("users.ID as userid, users.username, users.avatar,
    			user_notifications.timestamp, user_notifications.message,
    			user_notifications.url, user_notifications.ID, 
    			user_notifications.status")
			->join("users", "users.ID = user_notifications.fromid")
			->limit(5)
			->where("user_notifications.status", 0)
			->order_By("user_notifications.ID", "DESC")
			->get("user_notifications");
	}

	public function get_notification($id, $userid)
	{
		return $this->db
			->where("user_notifications.userid", $userid)
			->where("user_notifications.ID", $id)
			->select("users.ID as userid, users.username, users.avatar,
    			user_notifications.timestamp, user_notifications.message,
    			user_notifications.url, user_notifications.ID, 
    			user_notifications.status")
			->join("users", "users.ID = user_notifications.fromid")
			->order_By("user_notifications.ID", "DESC")
			->get("user_notifications");
	}

	public function get_notifications_all($userid, $datatable)
	{
		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"user_notifications.message",
			)
		);

		return $this->db
			->where("user_notifications.userid", $userid)
			->select("users.ID as userid, users.username, users.avatar,
    			users.online_timestamp,
    			user_notifications.timestamp, user_notifications.message,
    			user_notifications.url, user_notifications.ID, 
    			user_notifications.status")
			->join("users", "users.ID = user_notifications.fromid")
			->limit($datatable->length, $datatable->start)
			->order_By("user_notifications.ID", "DESC")
			->get("user_notifications");
	}

	public function get_notifications_all_fp($userid, $page, $max = 10)
	{
		return $this->db
			->where("user_notifications.userid", $userid)
			->select("users.ID as userid, users.username, users.avatar,
    			users.online_timestamp,
    			user_notifications.timestamp, user_notifications.message,
    			user_notifications.url, user_notifications.ID, 
    			user_notifications.status")
			->join("users", "users.ID = user_notifications.fromid")
			->limit($max, $page)
			->order_By("user_notifications.ID", "DESC")
			->get("user_notifications");
	}

	public function get_notifications_all_total($userid)
	{
		$s = $this->db
			->where("user_notifications.userid", $userid)
			->select("COUNT(*) as num")
			->get("user_notifications");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function add_notification($data)
	{
		if (
			isset($data['email']) && isset($data['email_notification'])
			&& $data['email_notification']
		) {
			// Send Email
			$subject = $this->settings->info->site_name . lang("ctn_670");

			if (isset($data['username'])) {
				$username = $data['username'] . ",";
			} else {
				$username = lang("ctn_339");
			}

			if (!isset($_COOKIE['language'])) {
				// Get first language in list as default
				$lang = $this->config->item("language");
			} else {
				$lang = $this->common->nohtml($_COOKIE["language"]);
			}

			// Send Email
			$this->load->model("home_model");
			$email_template = $this->home_model->get_email_template_hook("new_notification", $lang);
			if ($email_template->num_rows() == 0) {
				$this->template->error(lang("error_48"));
			}
			$email_template = $email_template->row();

			$email_template->message = $this->common->replace_keywords(
				array(
					"[NAME]" => $username,
					"[SITE_URL]" => site_url(),
					"[SITE_NAME]" =>  $this->settings->info->site_name
				),
				$email_template->message
			);

			$this->common->send_email(
				$subject,
				$email_template->message,
				$data['email']
			);
		}
		unset($data['email']);
		unset($data['email_notification']);
		unset($data['username']);
		$this->db->insert("user_notifications", $data);
	}

	public function update_notification($id, $data)
	{
		$this->db->where("ID", $id)->update("user_notifications", $data);
	}

	public function unpublishlead($id)
	{
		$this->db->where("id", $id)->update("legal_leads", ['status' =>2]);
	}


	public function update_user_reward($id, $data)
	{


		$this->db->where("ID", $id)->update("users", $data);
	}

	public function update_approve_sttaus($id)
	{

		$this->db->set("status", 1)->where("ID", $id)->update("appointmentbooking");
	}

	public function update_reject_sttaus($id, $data)
	{

		$this->db->where("ID", $id)->update("appointmentbooking", $data);
	}
	public function update_user_notifications($userid, $data)
	{
		$this->db->where("userid", $userid)
			->update("user_notifications", $data);
	}



	public function increment_field($userid, $field, $amount)
	{
		$this->db->where("ID", $userid)
			->set($field, $field . '+' . $amount, FALSE)->update("users");
	}

	public function decrement_field($userid, $field, $amount)
	{
		$this->db->where("ID", $userid)
			->set($field, $field . '-' . $amount, FALSE)->update("users");
	}

	public function update_user($userid, $data)
	{
		$this->db->where("ID", $userid)->update("users", $data);
	}



	public function get_reward_point($userid)
	{

		return  $this->db->where("userid", $userid)
			->get("rewards_points_earn");
	}


	public function update_agency($userid, $data)
	{
		$this->db->where("created_by_user_id", $userid)->update("agency", $data);
	}

	public function check_block_ip()
	{
		$s = $this->db->where("IP", $_SERVER['REMOTE_ADDR'])->get("ip_block");
		if ($s->num_rows() == 0) return false;
		return true;
	}

	public function get_user_groups($userid)
	{
		return $this->db->where("user_group_users.userid", $userid)
			->select("user_groups.name,user_groups.ID as groupid,
				user_group_users.userid")
			->join("user_groups", "user_groups.ID = user_group_users.groupid")
			->get("user_group_users");
	}

	public function check_user_in_group($userid, $groupid)
	{
		$s = $this->db->where("userid", $userid)->where("groupid", $groupid)
			->get("user_group_users");
		if ($s->num_rows() == 0) return 0;
		return 1;
	}

	public function get_default_groups()
	{
		return $this->db->where("default", 1)->get("user_groups");
	}

	public function add_user_to_group($userid, $groupid)
	{
		$this->db->insert(
			"user_group_users",
			array(
				"userid" => $userid,
				"groupid" => $groupid
			)
		);
	}



	public function insert_research($data)
	{
		$this->db->insert("research", $data);
	}

	public function add_points($userid, $points)
	{
		$this->db->where("ID", $userid)
			->set("points", "points+$points", FALSE)->update("users");
	}

	public function get_verify_user($code, $username)
	{
		return $this->db
			->where("activate_code", $code)
			->where("username", $username)
			->get("users");
	}

	public function get_user_event($request)
	{
		return $this->db->where("IP", $_SERVER['REMOTE_ADDR'])
			->where("event", $request)
			->order_by("ID", "DESC")
			->get("user_events");
	}

	public function add_user_event($data)
	{
		$this->db->insert("user_events", $data);
	}

	public function insert_reward_table($data)
	{
		$this->db->insert("rewards_points_earn", $data);
	}

	public function get_custom_fields($data)
	{
		if (isset($data['register'])) {
			$this->db->where("register", 1);
		}
		return $this->db->get("custom_fields");
	}

	public function add_custom_field($data)
	{
		$this->db->insert("user_custom_fields", $data);
	}



	public function insertlogs($data)
	{

		$this->db->insert("leadslogs", $data);
	}

	public function get_custom_fields_answers($data, $userid)
	{
		if (isset($data['edit'])) {
			$this->db->where("custom_fields.edit", 1);
		}
		return $this->db
			->select("custom_fields.ID, custom_fields.name, custom_fields.type,
				custom_fields.required, custom_fields.help_text,
				custom_fields.options,
				user_custom_fields.value")
			->join("user_custom_fields", "user_custom_fields.fieldid = custom_fields.ID
			 AND user_custom_fields.userid = " . $userid, "LEFT OUTER")
			->get("custom_fields");
	}

	public function get_user_cf($fieldid, $userid)
	{
		return $this->db
			->where("fieldid", $fieldid)
			->where("userid", $userid)
			->get("user_custom_fields");
	}



	public function update_disable_req_status($id)
	{


		$this->db->where("id", $id)
			->update("legal_leads", array("status" => 1, "create_time" => date('Y-m-d H:i:s')));
	}


	public function update_enable_req_status($id)
	{

		$this->db->where("id", $id)
			->update("legal_leads", array("status" => 0, "create_time" => date('Y-m-d H:i:s')));
	}

	public function update_custom_field($fieldid, $userid, $value)
	{
		$this->db->where("fieldid", $fieldid)
			->where("userid", $userid)
			->update("user_custom_fields", array("value" => $value));
	}


	public function get_deduction_logs($userid, $datatable)
	{

		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"leadslogs.date",
			)
		);

		return  $this->db
			->where("leadslogs.userid", $userid)
			->select("users.ID as userid, users.username,users.avatar,users.online_timestamp,leadslogs.leadid,
			leadslogs.date,legal_leads.name,legal_leads.email,legal_leads.phone,legal_leads.issue")
			->join("users", "users.ID = leadslogs.userid")
			->join("legal_leads", "legal_leads.id = leadslogs.leadid")
			->limit($datatable->length, $datatable->start)
			->get("leadslogs");
	}
	public function get_deduction_logs_2($userid, $datatable)
	{
		$subscription_buy = $this->db->where("point_deduction_logs.user_id", $userid)
			->where("point_deduction_logs.type", 1)
			->select("point_deduction_logs.ref_id,point_deduction_logs.points,point_deduction_logs.	user_id,point_deduction_logs.type,point_deduction_logs.created_at,payment_plans.name")
			->join("payment_plans", "payment_plans.ID = point_deduction_logs.ref_id", "left")->get("point_deduction_logs")->result();



		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"leadslogs.date",
			)
		);

		$leads_seen =  $this->db
			->where("leadslogs.userid", $userid)
			->select("users.ID as userid,users.first_name,users.last_name,users.username,users.avatar,users.online_timestamp,leadslogs.leadid,
			leadslogs.date,legal_leads.name,legal_leads.email,legal_leads.phone,legal_leads.issue")
			->join("users", "users.ID = leadslogs.userid")
			->join("legal_leads", "legal_leads.id = leadslogs.leadid")
			->limit($datatable->length, $datatable->start)
			->get("leadslogs")->result();

		return $data = [
			'leads' => $leads_seen,
			'subscription' => $subscription_buy
		];
	}

	public function get_total_payment_logs_count($userid)
	{
		$s = $this->db
			->where("userid", $userid)
			->select("COUNT(*) as num")->get("payment_logs");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}


	public function get_payment_logs($userid, $datatable)
	{
		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"payment_logs.email"
			)
		);
		return  $this->db
			->where("payment_logs.userid", $userid)
			// ->where("point_deduction_logs.ref_id",1)
			->select("users.ID as userid, users.username,users.first_name,users.last_name,users.email,
			users.avatar, users.online_timestamp,
			payment_logs.email, payment_logs.amount, payment_logs.timestamp, 
			payment_logs.ID, payment_logs.processor")
			->join("users", "users.ID = payment_logs.userid")
			// ->join("point_deduction_logs", "users.ID = point_deduction_logs.user_id")
			// ->join("payment_plans", "payment_plans.ID = point_deduction_logs.ref_id")
			// ->limit($datatable->length, $datatable->start)
			->get("payment_logs");
	}

	public function get_total_lawyer_plan_count($userid)
	{

		$subscription_buy = $this->db->where("city_subscription_plan.user_id", $userid)
			->where("city_subscription_plan.status", 1)
			->select("city_subscription_plan.*,payment_plans.name")
			->join("payment_plans", "payment_plans.ID = city_subscription_plan.plan_id", "left")
			->order_by('city_subscription_plan.created_at', "desc")
			->get("city_subscription_plan")->result();

		if (count($subscription_buy) > 0) {
			$plan_count =  count($subscription_buy);
			return $plan_count;
		} else {
			return 0;
		}
	}


	public function get_subscription_history($userid, $datatable)
	{


		$datatable->db_order();

		$datatable->db_search(
			array(
				"city_subscription_plan.city_name",
				"city_subscription_plan.category_name",
				// "users.first_name"

			),
			true
		);

		$current_time = strtotime('now');

		$this->db->where('city_subscription_plan.user_id', $userid)
			->where('city_subscription_plan.status =', 1)
			->select(
				"city_subscription_plan.*,payment_plans.name"
			)
			->join("payment_plans", "city_subscription_plan.plan_id = payment_plans.ID", "left")->order_by('city_subscription_plan.created_at', 'desc');

		return $datatable->get("city_subscription_plan");
	}
	public function get_lawyer_active_plan($userid, $type)
	{


		$current_time = strtotime('now');

		return   $this->db->where('city_subscription_plan.user_id', $userid)
			->where('city_subscription_plan.status =', 1)
			->where('city_subscription_plan.plan_type', $type)
			->where('city_subscription_plan.end_date >=', $current_time)
			->select('max(city_subscription_plan.id ) as id ,city_subscription_plan.user_id,city_subscription_plan.plan_id,city_subscription_plan.plan_type,city_subscription_plan.city_id,city_subscription_plan.city_name,city_subscription_plan.category_id,city_subscription_plan.category_name`,city_subscription_plan.price,city_subscription_plan.status,min(city_subscription_plan.start_date) as start_date,max(city_subscription_plan.end_date) as end_date,`city_subscription_plan.created_at`,`city_subscription_plan.updated_at`,payment_plans.name')
			->join("payment_plans", "city_subscription_plan.plan_id = payment_plans.ID", "left")
			->group_by('city_subscription_plan.user_id,city_subscription_plan.city_id,city_subscription_plan.category_id')
			->get('city_subscription_plan');


		//  return  $this->db->where('city_subscription_plan.user_id',$userid)
		// 		->where('city_subscription_plan.status =', 1)
		// 		->where('city_subscription_plan.end_date >=', $current_time)
		// 		->select(
		// 				"city_subscription_plan.*,payment_plans.name")
		// 		->join("payment_plans", "city_subscription_plan.plan_id = payment_plans.IDs", "left")->get('city_subscription_plan');



	}

	public function update_point_deduction($userid, $data)
	{
		$this->db->insert("point_deduction_logs", $data);
	}

	public function get_total_deduction_logs_count($userid)
	{

		$s = $this->db
			->where("userid", $userid)
			->select("COUNT(*) as num")->get("leadslogs");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}



	public function get_profile_comments($userid, $page)
	{
		return $this->db
			->where("profile_comments.profileid", $userid)
			->select("profile_comments.ID, profile_comments.comment,
				profile_comments.userid, profile_comments.timestamp,
				profile_comments.profileid, profile_comments.userid,
				users.username, users.avatar, users.online_timestamp")
			->join("users", "users.ID = profile_comments.userid")
			->limit(5, $page)
			->order_by("profile_comments.ID", "DESC")
			->get("profile_comments");
	}




	public function add_profile_comment($data)
	{
		$this->db->insert("profile_comments", $data);
	}

	public function get_profile_comment($id)
	{
		return $this->db->where("ID", $id)->get("profile_comments");
	}

	public function delete_profile_comment($id)
	{
		$this->db->where("ID", $id)->delete("profile_comments");
	}

	public function get_total_profile_comments($userid)
	{
		$s = $this->db
			->where("profile_comments.profileid", $userid)
			->select("COUNT(*) as num")
			->get("profile_comments");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}

	public function increase_profile_views($userid)
	{
		$this->db->where("ID", $userid)->set("profile_views", "profile_views+1", FALSE)->update("users");
	}

	public function add_user_data($data)
	{
		$this->db->insert("user_data", $data);
	}

	public function update_user_data($id, $data)
	{
		$this->db->where("ID", $id)->update("user_data", $data);
	}

	public function get_user_data($userid)
	{
		return $this->db->where("userid", $userid)->get("user_data");
	}

	public function get_user_role($roleid)
	{
		return $this->db->where("ID", $roleid)->get("user_roles");
	}

	public function get_users_with_permissions($permissions)
	{

		foreach ($permissions as $p) {
			$this->db->or_where("user_roles." . $p, 1);
		}

		return $this->db
			->select("users.ID as userid, users.username, users.email, users.first_name,
				users.last_name, users.online_timestamp")
			->join("user_roles", "user_roles.ID = users.user_role")
			->get("users");
	}

	public function get_all_user_groups()
	{
		return $this->db->get("user_groups");
	}

	public function get_user_group($id)
	{
		return $this->db->where("ID", $id)->get("user_groups");
	}

	public function get_user_friends($userid, $limit)
	{
		return $this->db
			->select("users.username, users.first_name, users.last_name,
				users.avatar, users.online_timestamp, users.ID as friendid")
			->where("user_friends.userid", $userid)
			->join("users", "users.ID = user_friends.friendid")
			->order_by("users.online_timestamp", "DESC")
			->limit($limit)
			->get("user_friends");
	}


	public function get_total_mail_count($userid)
	{
		$s = $this->db
			->where("live_chat_users.userid", $userid)
			->select("COUNT(*) as num")
			->join("live_chat", "live_chat.ID = live_chat_users.chatid")
			->join("users", "users.ID = live_chat_users.userid")
			->join("live_chat_messages", "live_chat_messages.ID = live_chat.last_replyid")
			->group_by("live_chat.ID")
			->get("live_chat_users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}


	public function get_total_message_count($userid)
	{

		$s = $this->db
			->where("live_chat_users.userid", $userid)
			->select("COUNT(*) as num")

			->get("live_chat_users");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}
	public function get_usernames($username)
	{
		return $this->db->like("username", $username)->limit(10)->get("users");
	}

	public function get_user_by_name($query)
	{
		return $this->db->like("first_name", $query)->or_like("last_name", $query)->limit(10)->get("users");
	}

	public function get_names($name)
	{
		return $this->db->like("first_name", $name)->or_like("last_name", $name)->limit(10)->get("users");
	}

	public function get_friend_names($name, $userid)
	{
		return $this->db
			->where("user_friends.userid", $userid)
			->group_start()
			->like("users.first_name", $name)
			->or_like("users.last_name", $name)
			->group_end()
			->select("users.ID, users.username, users.first_name, users.last_name,
    			users.email, users.online_timestamp, users.avatar")
			->join("users", "users.ID = user_friends.friendid")
			->limit(10)
			->get("user_friends");
	}

	public function get_user_friend($userid, $friendid)
	{
		return $this->db
			->where("userid", $userid)
			->where("friendid", $friendid)
			->get("user_friends");
	}

	public function check_friend_request($userid, $friendid)
	{
		return $this->db
			->where("userid", $userid)
			->where("friendid", $friendid)
			->get("user_friend_requests");
	}

	public function add_friend_request($data)
	{
		$this->db->insert("user_friend_requests", $data);
	}

	public function get_friend_requests($userid)
	{
		return $this->db
			->select("user_friend_requests.ID, user_friend_requests.timestamp,
    			users.ID as userid, users.avatar, users.first_name, users.last_name,users.city,users.state,users.profile_identification,
    			users.online_timestamp, users.username")
			->join("users", "users.ID = user_friend_requests.userid")
			->where("user_friend_requests.friendid", $userid)
			->get("user_friend_requests");
	}


	public function get_friend_requests_count($userid)
	{


		$s = $this->db
			->select("COUNT(*) as num")
			->join("users", "users.ID = user_friend_requests.userid")
			->where("user_friend_requests.friendid", $userid)
			->get("user_friend_requests");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}
	public function get_friend_request($id, $userid)
	{
		return $this->db->where("user_friend_requests.ID", $id)
			->where("user_friend_requests.friendid", $userid)
			->select("user_friend_requests.ID, users.ID as userid, users.first_name,
    			users.last_name, users.avatar, users.online_timestamp,
    			users.email, users.email_notification, users.username")
			->join("users", "users.ID = user_friend_requests.userid")
			->get("user_friend_requests");
	}

	public function delete_friend_request($id)
	{
		$this->db->where("ID", $id)->delete("user_friend_requests");
	}

	public function add_friend($data)
	{
		$this->db->insert("user_friends", $data);
	}

	public function get_user_friends_sample($userid)
	{
		return $this->db->where("user_friends.userid", $userid)
			->select("users.username, users.first_name, users.last_name, users.avatar,
    			users.online_timestamp, users.ID as userid,
    			user_friends.ID")
			->join("users", "users.ID = user_friends.friendid")
			->limit(6)
			->get("user_friends");
	}

	public function get_total_friends_count($userid)
	{
		$s = $this->db
			->where("user_friends.userid", $userid)
			->select("COUNT(*) as num")
			->join("users", "users.ID = user_friends.friendid")
			->get("user_friends");
		$r = $s->row();
		if (isset($r->num)) return $r->num;
		return 0;
	}



	public function get_user_friends_dt1($userid)
	{

		return $this->db
			->where("user_friends.userid", $userid)
			->select("users.ID as userid, users.username, users.email,
			users.avatar,users.profile_identification, users.online_timestamp,
			users.first_name,users.city, users.last_name,
			user_friends.timestamp, user_friends.ID")
			->join("users", "users.ID = user_friends.friendid")
			->get("user_friends");
	}

	public function get_mutual_friends($id, $userid)
	{




		return $this->db->where('a.userid', $id)
			->where('b.userid', $userid)

			->join('user_friends b', 'a.friendid = b.friendid', 'left outer')
			->get('user_friends a');



		/*
    	SELECT *
FROM user_friends f1 INNER JOIN user_friends f2
  ON f1.`friendid` = f2.`friendid`
WHERE f1.`userid` = 89
  AND f2.`userid` = 41

  */
	}

	public function get_user_friends_dt($userid, $datatable)
	{
		$datatable->db_order();

		$datatable->db_search(
			array(
				"users.username",
				"users.first_name",
				"users.last_name"
			),
			true // Cache query
		);
		$this->db
			->where("user_friends.userid", $userid)
			->select("users.ID as userid, users.username, users.email,
			users.avatar, users.online_timestamp,
			users.first_name, users.last_name,
			user_friends.timestamp, user_friends.ID")
			->join("users", "users.ID = user_friends.friendid");
		return $datatable->get("user_friends");
	}

	public function get_user_friend_id($id, $userid)
	{
		return $this->db->where("ID", $id)->where("userid", $userid)->get("user_friends");
	}

	public function delete_friend($userid, $friendid)
	{
		$this->db->where("userid", $userid)->where("friendid", $friendid)->delete("user_friends");
		$this->db->where("userid", $friendid)->where("friendid", $userid)->delete("user_friends");
	}

	public function add_report($data)
	{
		$this->db->insert("reports", $data);
	}

	public function add_relationship_request($data)
	{
		$this->db->insert("relationship_requests", $data);
	}

	public function get_relationship_request($userid)
	{
		return $this->db
			->where("relationship_requests.friendid", $userid)
			->select("relationship_requests.ID, users.first_name,
    		users.last_name, users.username")
			->join("users", "users.ID = relationship_requests.userid")
			->get("relationship_requests");
	}

	public function get_relationship_request_invites($userid)
	{
		return $this->db
			->where("relationship_requests.userid", $userid)
			->select("relationship_requests.ID, relationship_requests.relationship_status,
    			users.first_name,
    		users.last_name, users.username")
			->join("users", "users.ID = relationship_requests.friendid")
			->get("relationship_requests");
	}

	public function get_relationship_request_id($id)
	{
		return $this->db->where("ID", $id)->get("relationship_requests");
	}

	public function delete_relationship_request($id)
	{
		$this->db->where("ID", $id)->delete("relationship_requests");
	}

	public function check_relationship_request($userid, $friendid)
	{
		return $this->db->where("userid", $userid)->where("friendid", $friendid)->get("relationship_requests");
	}

	public function delete_relationship_request_from_user($userid)
	{
		$this->db->where("userid", $userid)->delete("relationship_requests");
	}



	public function delete_user_notifications($userid)
	{


		$this->db->where("userid", $userid)->delete("user_notifications");
	}
	public function get_newest_users($userid)
	{
		return $this->db->where("ID !=", $userid)->limit(5)->order_by("ID", "DESC")->get("users");
	}
	public function get_top_viewd_users($userid)
	{
		return $this->db->where("ID !=", $userid)->limit(5)->order_by("profile_view", "DESC")->get("users");
	}

	public function increase_posts($userid)
	{
		$this->db->where("ID", $userid)->set("post_count", "post_count+1", FALSE)->update("users");
	}

	public function decrease_posts($userid)
	{
		$this->db->where("ID", $userid)->set("post_count", "post_count-1", FALSE)->update("users");
	}

	public function delete_notification($id)
	{
		$this->db->where("ID", $id)->delete("user_notifications");
	}

	public function add_verified_request($data)
	{
		$this->db->insert("verified_requests", $data);
	}

	public function get_verified_request($userid)
	{
		return $this->db->where("userid", $userid)->get("verified_requests");
	}
	public function get_maxims()
	{
		return $this->db->get('maxims');
	}
	public function get_maxims_new()
	{
		return $this->db->get('maxims-new');
	}
	public function getActivePerformas($search, $category)
	{
		if (!empty($category)) {
			$this->db->where('category_id', $category);
		}
		return $this->db->where('status', '1')->like('title', $search)->get('performas')->result();
	}
	public function getCategoryInfo($id)
	{
		return $this->db->where('cid', $id)->get('catagories')->row_array();
	}
	public function getCaseInfo($id)
	{
		return $this->db->where('id', $id)->get('case_editor')->row_array();
	}
	public function getCasesByCategory($category_id)
	{
		return $this->db->where('case_category', $category_id)->get('case_editor')->result();
	}
	public function get_premium_plan_id($id)
	{
		return $this->db->select('premium_planid,premium_time')->where('ID', $id)->get('users')->result();
	}
	public function isPlanBy($id)
	{
		return $this->db->where('premium_planid', $id)->limit(5)->get('users')->result();
	}

	public function getSubscriptionPlan($id)
	{
		return $this->db->where('ID', $id)->get('payment_plans')->result();
	}

	public function get_all_languages()
	{
		return $this->db->select('*')->get('languages')->result();
	}

	public function check_balance($id)
	{
		return $this->db->select('points')->where('ID', $id)->get('users')->result();
	}

	public function get_city_plan_detail($id)
	{
		return  $this->db->select('name,price')->where('id', $id)->get('city_plan_pricelist')->result();
	}

	public function get_search_plan_detail($cat_id, $city_id)
	{
		return   $this->db->select('name as city,catagory_name,price')
			->where('cat_id', $cat_id)
			->where('city_id', $city_id)
			->get('search_plan_pricelist')->result();
	}

	public function get_city_cat_detail($city_id, $cat_id)
	{

		return $this->db->select('name,price')
			->where('city_id', $city_id)
			->where('cat_id', $cat_id)
			->get('search_plan_pricelist')->result();
	}
	public function update_plan($data)
	{
		return  $this->db->insert('city_subscription_plan', $data);
	}


	public function get_buy_city_plan($start, $city_id, $plan_id)
	{

		// return   $this->db->where("city_subscription_plan.end_date >=", $start)
		// 			->where("city_subscription_plan.city_id",$city_id)
		// 			->where("city_subscription_plan.plan_id",$plan_id)
		// 			->select("users.ID as userid, users.username, users.email,
		// 					users.phone,users.first_name,users.last_name,city_subscription_plan.*")
		// 			->join("users", "users.ID = city_subscription_plan.user_id")
		// 			->order_by("city_subscription_plan.end_date","ASC")
		// 			->get("city_subscription_plan");

		return  $this->db->where('city_subscription_plan.status =', 1)
			->where('city_subscription_plan.end_date >=', $start)
			->where('city_subscription_plan.plan_id', $plan_id)
			->where('city_subscription_plan.city_id', $city_id)
			->select('max(city_subscription_plan.id ) as id ,city_subscription_plan.user_id,city_subscription_plan.plan_id,city_subscription_plan.plan_type,city_subscription_plan.city_id,city_subscription_plan.city_name,city_subscription_plan.category_id,city_subscription_plan.category_name`,city_subscription_plan.price,city_subscription_plan.status,min(city_subscription_plan.start_date) as start_date,max(city_subscription_plan.end_date) as end_date,`city_subscription_plan.created_at`,`city_subscription_plan.updated_at`,payment_plans.name,users.ID as userid, users.username, users.email,users.phone,users.first_name,users.last_name')
			->join("payment_plans", "city_subscription_plan.plan_id = payment_plans.ID", "left")
			->join("users", "users.ID = city_subscription_plan.user_id")
			->group_by('city_subscription_plan.user_id,city_subscription_plan.city_id,city_subscription_plan.category_id')
			->get('city_subscription_plan');





		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}
	public function get_existing_city_plan($user_id, $start, $city_id, $plan_id)
	{

		return  $this->db->where("city_subscription_plan.user_id", $user_id)
			->where("city_subscription_plan.end_date >=", $start)
			->where("city_subscription_plan.city_id", $city_id)
			->where("city_subscription_plan.plan_id", $plan_id)
			->select("users.ID as userid, users.username, users.email,
							users.phone,users.first_name,users.last_name,city_subscription_plan.*")
			->join("users", "users.ID = city_subscription_plan.user_id")
			->order_by("city_subscription_plan.end_date", "ASC")
			->get("city_subscription_plan");


		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}

	public function get_all_buy_search_plan($start, $plan_id, $cat_id, $city_id)
	{


		return  $this->db->where("city_subscription_plan.end_date >=", $start)
			->where("city_subscription_plan.city_id", $city_id)
			->where("city_subscription_plan.category_id", $cat_id)
			->where("city_subscription_plan.plan_id", $plan_id)
			->select("users.ID as userid, users.username, users.email,
							users.phone,users.first_name,users.last_name,city_subscription_plan.*")
			->join("users", "users.ID = city_subscription_plan.user_id")
			->order_by("city_subscription_plan.end_date", "ASC")
			->get("city_subscription_plan");

		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}


	public function get_existing_buy_search_plan($user_id, $start, $plan_id, $cat_id, $city_id)
	{

		return  $this->db->where("city_subscription_plan.user_id", $user_id)
			->where("city_subscription_plan.end_date >=", $start)
			->where("city_subscription_plan.city_id", $city_id)
			->where("city_subscription_plan.plan_id", $plan_id)
			->where("city_subscription_plan.category_id", $cat_id)
			->select("users.ID as userid, users.username, users.email,
								users.phone,users.first_name,users.last_name,city_subscription_plan.*")
			->join("users", "users.ID = city_subscription_plan.user_id")
			->order_by("city_subscription_plan.end_date", "ASC")
			->get("city_subscription_plan");


		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}

	public function plan_purchased($city_id)
	{
		$start_format = strtotime(date("Y/m/d"));
		return $this->db->where("city_id", $city_id)
			->select("id")
			->order_by("end_date", "ASC")
			->get("city_subscription_plan");


		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}


	public function get_lawyer_city_plan($start_format, $user_id, $city_id, $plan_id)
	{

		return  $this->db->where("city_subscription_plan.user_id", $user_id)
			->where("city_subscription_plan.end_date >=", $start_format)
			->where("city_subscription_plan.city_id", $city_id)
			->where("city_subscription_plan.plan_id", $plan_id)
			->select("users.ID as userid, users.username, users.email,
								users.phone,users.first_name,users.last_name,city_subscription_plan.*")
			->join("users", "users.ID = city_subscription_plan.user_id")
			->order_by("city_subscription_plan.end_date", "ASC")
			->get("city_subscription_plan");


		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}
	public function get_lawyer_search_plan($start_format, $user_id, $cat_id, $city_id, $plan_id)
	{
		return   $this->db->where("city_subscription_plan.user_id", $user_id)
			->where("city_subscription_plan.end_date >=", $start_format)
			->where("city_subscription_plan.category_id", $cat_id)
			->where("city_subscription_plan.city_id", $city_id)
			->where("city_subscription_plan.plan_id", $plan_id)
			->select("users.ID as userid, users.username, users.email,
								users.phone,users.first_name,users.last_name,city_subscription_plan.*")
			->join("users", "users.ID = city_subscription_plan.user_id")
			->order_by("city_subscription_plan.end_date", "ASC")
			->get("city_subscription_plan");


		//return $this->db->where("service_seeker_id",$userid)->where("status",1)->where("dateofappointment >=", "(CURDATE()-INTERVAL 1 MONTH)")->get("appointmentbooking");
	}

	public function update_lead_status($id, $data)
	{
		$this->db->where("id", $id)->update("legal_leads", $data);
	}
	public function get_user_info($id){

		return $this->db->select("*")

				->where("users.ID",$id)

				->get("users")->row_Array();

	}

	public function get_coreprofile($userid){

		return $this->db->where("userid", $userid)->get("coreprofile")->row_Array();	

	}
	public function get_coreprofile_student($userid){

		return $this->db->where("userid", $userid)->get("coreprofile_student")->row_Array();	

	}
	public function get_coreprofile_professor($userid){

		return $this->db->where("userid", $userid)->get("coreprofile_professor")->row_Array();	

	}
	public function get_consultation_timing($userid){

		return $this->db->select("*")->from("lawyers_consultation_timing")->where("user_id",$userid)->get()->result_array();

	}
	public function get_verification_log($userid){

		return $this->db->select("user_verification_logs.*")

				->join("user_verification_logs", "user_verification_logs.user_id = users.ID")

				->where("users.ID",$userid)

				->order_by('user_verification_logs.id','DESC')

				->get("users")->result_array();

	}
}
