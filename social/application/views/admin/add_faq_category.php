<style>

.modal-footer{
    margin-top:"20px !important";
}
</style>

<div class="white-area-content">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
        <div class="db-header-extra"> 
            
            <input type="button" class="btn btn-primary btn-sm" value="Add New FAQ Category" data-toggle="modal" data-target="#myModal">
        </div>
    </div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">FAQ Category</li>
</ol>


<div class="table-responsive">
    <table id="faq-category-table" class="table table-striped table-hover table-bordered">
        <thead>
            <tr class="table-header">
                <td>FAQ Category Name</td>
                <td><?php echo lang("ctn_52") ?></td></tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top:100px !important">
  <div class="modal-dialog">
    <div class="modal-content">
    <form method="post" action="<?php echo base_url();?>admin/submit_faq_category">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New FAQ Category</h4>
      </div>
      <div class="modal-body">
            <div class="form-group">
              <div class="row">
                <label for="inputEmail3" class="col-sm-3 control-label">Category name</label>
                <div class="col-sm-9">
                    <input type="text" name="faq_category_name" class="form-control" required>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                </div>
              </div>  
            </div>
      </div>
      <div class="modal-footer" >
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
         <input type="submit" class="btn btn-primary" value="Submit">
      </div>
      </form>
    </div>
   </div> 
</div>

<script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#faq-category-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        null,
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("admin/faq_category_page") ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
        "search-like", 
        "search-exact",
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>
<script type="text/javascript">
$(document).ready(function() {
CKEDITOR.replace('blogs_desc', { height: '150'});
CKEDITOR.replace('post', { height: '150'});
});
</script>
