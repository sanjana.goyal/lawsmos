<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
    <div class="db-header-extra"> 
		
		<input type="button" class="btn btn-primary btn-sm" value="Add New Blog" data-toggle="modal" data-target="#myModal">
		<input type="button" class="btn btn-primary btn-sm" value="Add New Blog Post" data-toggle="modal" data-target="#myModal1">
    </div>
    <div class="db-header-extra">
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active"><?php echo lang("ctn_772") ?></li>
</ol>


<div class="table-responsive">
<table id="blog-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header"><td><?php echo lang("ctn_11") ?></td><td><?php echo lang("ctn_339") ?></td><td><?php echo lang("ctn_52") ?></td></tr>
</thead>
<tbody>
</tbody>
</table>
</div>


</div>






<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Post</h4>
      </div>
      <div class="modal-body">
       <?php echo form_open_multipart(site_url("admin/add_post_pro"), array("class" => "form-horizontal")) ?>
             <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_767") ?></label>
                <div class="col-sm-10">
                    <input type="text" name="title" class="form-control">
                </div>
            </div>
            
             <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Select Blog</label>
                <div class="col-sm-10">
                    <select name="blogid" class="form-control">
						<?php 
						
						
						foreach($allblogs as $blogd): ?>
						
						
							<option value="<?php echo $blogd->ID; ?>"><?php echo $blogd->title; ?></option>
								
						<?php	endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_770") ?></label>
                <div class="col-sm-10">
                    <input type="file" name="userfile" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_506") ?></label>
                <div class="col-sm-10">
                    <textarea name="blog_post" id="post"></textarea>
                </div>
            </div>
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_606") ?></label>
                <div class="col-sm-10">
                    <select name="status" class="form-control">
                      <option value="0"><?php echo lang("ctn_768") ?></option>
                      <option value="1"><?php echo lang("ctn_769") ?></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_784") ?></label>
                <div class="col-sm-10">
                    <input type="checkbox" name="post_to_timeline" value="1" checked> <?php echo lang("ctn_53") ?>
                </div>
            </div>
               
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
         <input type="submit" class="btn profile-set-btn" value="<?php echo lang("ctn_506") ?>">
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>








<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Blog</h4>
      </div>
      <div class="modal-body">
       <?php echo form_open_multipart(site_url("admin/create_pro"), array("class" => "form-horizontal")) ?>
             <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_773") ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="title" class="form-control">
                    </div>
                </div>
            <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_774") ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="blogs_desc" name="description" value="">
                    </div>
                </div>
            <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_775") ?></label>
                    <div class="col-sm-10">
                        <select name="private" class="form-control">
                          <option value="0"><?php echo lang("ctn_539") ?></option>
                          <option value="1"><?php echo lang("ctn_633") ?></option>
                        </select>
                    </div>
               </div>
            
               
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
         <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_785") ?>">
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#blog-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        null,
        null,
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("admin/blog_page") ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
        "search-like", 
        "search-exact",
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>
<script type="text/javascript">
$(document).ready(function() {
CKEDITOR.replace('blogs_desc', { height: '150'});
CKEDITOR.replace('post', { height: '150'});
});
</script>
