<div>
  <div class="">
    <div class="">
      <div class="r">
     
        <h4 class="modal-title" id="myModalLabel"><?php echo lang("ctn_883") ?></h4>
      </div>
      <div class="modal-body">
      <?php echo form_open(site_url("admin/change_city_plan_pro"), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_844") ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="city_name" name="city_name" readonly  value="<?php echo $plan_detail[0]->name;?>">
                        <p id="city_error_tag" class="text-danger"></p>
                        <input type="hidden" class="form-control" id="city_id" name="city_id" value="<?php echo $plan_detail[0]->id;?>">
                    </div>
            </div>
            <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_869") ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="city_price" name="city_price" value="<?php echo $plan_detail[0]->price;?>">
                        <p id="price_error_tag" class="text-danger"></p>
                    </div>
            </div>
          
         
      </div>
      <div class="modal-footer">
        
        <input type="button" class="btn btn-primary" onclick="chnagePrice();" value="<?php echo lang("ctn_61") ?>" />
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>




<script>
    function chnagePrice(){
      
        var city_id  = $('#city_id').val();
        var city  = $('#city_name').val();
        var price  = $('#city_price').val();
        var error = 0;

        if(city.length == 0 || city == "" || city ==  null ){
            $('#city_error_tag').html('City Column Can Not Be Empty.');
            error = 1;
            return false;
        }
        if(price.length == 0 || price == "" || price ==  null){
            $('#price_error_tag').html('Price Column Can Not Be Empty.');
            error = 1;
            return false;
        }

  

        $.ajax({
            url : "<?php echo site_url("admin/change_city_plan_pro") ?>",
            type: 'post',
            data: {
                'city_id':city_id,
                'city':city,
                'price':price, 
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'},

            success: function(response){
                // console.log(response);
                  var res = JSON.parse(response);
                if(res.status == "true"){
                       window.location.href = "<?php echo site_url('admin/set_city_price') ?>";
                }
            }
        });    
    }



</script>