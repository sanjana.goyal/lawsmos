<div class="row">
	<div class="col-md-12">
		<form action="<?php echo site_url('admin/addbareacts'); ?>" method="post">
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
			<div class="form-group">
				<input type="hidden" name="cat_id" value="<?php echo $category->cid; ?>" />
			</div>
			<div class="form-group">
				<label for="catagory">Catagory Name</label>
				<input type="text" name="cat_name" value="<?php echo $category->c_name; ?>" class="form-control" readonly />
			</div>
			<div class="form-group">
				<label for="catagory">Bare acts</label><br>
				<?php foreach($bareact as $bareacts): ?>
				
						<input type="hidden" name="bareactid" value="<?php echo $bareacts->id; ?>"   /> 
						<input type="checkbox" name="bareact[]" value="<?php echo $bareacts->id; ?>" <?php echo set_checkbox('bareact', $bareacts->id); ?>   /> <?php echo $bareacts->title; ?><br>
				
				<?php endforeach; ?>
			
			</div>
			<button class="btn btn-danger" type="submit">Update record</button>
		</form>
	</div>
</div>
