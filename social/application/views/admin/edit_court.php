<div class="white-area-content">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
        <div class="db-header-extra form-inline"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
        <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
        <li class="active"><?php echo lang("ctn_454") ?></li>
    </ol>
    <div class="panel panel-default">
        <div class="panel-body">
            <?php echo form_open(site_url("admin/edit_court/" . $court->id), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                <label for="court-name" class="col-md-3 label-heading"><?php echo lang("ctn_81") ?></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="court-name" name="court-name" value="<?php echo $court->court_name ?>">
                </div>
            </div>            
            <div class="form-group">
                <label for="court-status" class="col-md-3 label-heading"><?php echo lang("ctn_606") ?></label>
                <div class="col-md-9">
                    <select name="status" id="court-status" class="form-control">
                        <option value="0"><?php echo lang("ctn_702") ?></option>
                        <option value="1" <?php if($court->status == 1) echo "selected" ?>><?php echo lang("ctn_703") ?></option>
                    </select>
                </div>
            </div>
            <div class="block_btn bf">
                <input type="submit" class="btn btn-primary btn-sm " value="<?php echo lang("ctn_13") ?>" />
            </div>
            <?php echo form_close() ?>
        </div>
    </div>
</div>