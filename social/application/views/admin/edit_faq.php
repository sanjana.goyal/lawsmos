<div class="white-area-content blog_edit">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
        <div class="db-header-extra">
    </div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">Edit FAQ </li>
</ol>


<form method="post" action="<?php echo base_url();?>admin/submit_edit_faq">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
            <div class="row">
                  <label for="inputEmail3" class="col-sm-3 control-label">FAQ Title</label>
                  <div class="col-sm-9">
                      <input type="text" name="faq_title" class="form-control" value="<?php echo $faq['title']; ?>" required>
                      <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                      <input type="hidden" name="id" value="<?php echo $faq['id'];?>">
                  </div>
                </div><br>
                <div class="row">
                  <label for="inputEmail3" class="col-sm-3 control-label">FAQ Text</label>
                  <div class="col-sm-9">
                      <textarea name="faq_text" id="" cols="50" rows="5"><?php echo $faq['text']; ?></textarea>
                  </div>
                </div><br>
                <div class="row">
                  <label for="inputEmail3" class="col-sm-3 control-label">FAQ Category</label>
                  <div class="col-sm-9">
                    <select name="faq_category_id" class="form-control">
                        <option value="">Select Category</option>
                        <?php foreach($faq_category as $category) {
                            $selected = "";
                            if($category['id'] == $faq['category_id']){
                                $selected = "selected";
                            }    
                        ?>
                            <option <?php echo $selected; ?> value="<?php echo $category['id']; ?>"><?php echo $category['faq_category_name'] ?></option>
                        <?php
                        }   
                        ?>
                    </select>
                  </div>
                </div>
            </div> 
               
            <div class="block_btn" style="margin-top:15px">
                <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_13") ?>">
            </div>
        </div>
    </div>

</div>
