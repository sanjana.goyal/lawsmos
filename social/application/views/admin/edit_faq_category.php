<div class="white-area-content blog_edit">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
        <div class="db-header-extra">
    </div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">Edit FAQ Category</li>
</ol>


<form method="post" action="<?php echo base_url();?>admin/submit_edit_faq_category">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Category Name</label>
                <div class="col-sm-10">
                    <input type="text" name="faq_category_name" class="form-control" value="<?php echo $faq_category['faq_category_name'] ?>" required>
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <input type="hidden" name="id" value="<?php echo $faq_category['id'];?>">
                </div>
            </div> 
               
            <div class="block_btn" style="margin-top:50px">
                <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_13") ?>">
            </div>
        </div>
    </div>

</div>
