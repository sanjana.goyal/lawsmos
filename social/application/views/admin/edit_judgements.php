<style type="text/css">.form-group {
    display: flow-root;
}</style><?php  echo form_open('admin/update_judg_cat/'.$judgements->id); ?>
<div class="row">
        <div class="col-md-12">
        	<div class="white-area-content margin_left">
        		
        		<!-- <div class="db-header clearfix">
				    <div class="page-header-title"> <span class="glyphicon glyphicon-pencil"></span> <?php //echo lang("ctn_780") ?>: <?php //echo $blog->title ?></div>
				    <div class="db-header-extra form-inline"> 

                         <div class="form-group has-feedback no-margin">
                            <div class="input-group">
                            <input type="text" class="form-control input-sm" placeholder="<?php //echo lang("ctn_336") ?>" id="form-search-input" />
                            <div class="input-group-btn">
                                <input type="hidden" id="search_type" value="0">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
                                      <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> <?php //echo lang("ctn_337") ?></a></li>
                                      <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok nodisplay" id="search-exact"></span> <?php //echo lang("ctn_338") ?></a></li>
                                      <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok nodisplay" id="name-exact"></span> <?php //echo lang("ctn_81") ?></a></li>
                                    </ul>
                                  </div>
                            </div>
                            </div>

                       
                            
				</div>
				</div> -->


               
            <div class="panel panel-default edit-judgment-wrap">
            <div class="panel-body">
            <div class="form-group">


                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo "Year"; ?></label>
                <div class="col-sm-10">
                  <!--   <input type="text" name="title" class="form-control" value="<?php// echo $judgements->year; ?>"> -->
                    <label><?php echo $judgements->year; ?></label>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo "Date Of Hearing"; ?></label>
                <div class="col-sm-10">
                  <label><?php echo $judgements->dateofhearing; ?></label>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo "Heading"; ?></label>
                <div class="col-sm-10">
                     <label><?php echo $judgements->heading; ?></label>
                </div>
            </div>
             
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label"><?php echo "State";?></label>
                <label><?php echo $judgements->state; ?></label>
            </div>
             <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Content</label>
                <label><?php echo base64_decode($judgements->content); ?></label>
            </div>
        
            <div class="form-group">
    <label for="name-in" class="col-sm-2 control-label">Select Category</label>
    <div class="col-sm-10">
        <select name="cat_option" class="form-control">
        
        <option value="0">Select Judgement Category</option>
            <?php  foreach ($parentcat as $r) { ?>
        <option value="<?php echo $r->cid; ?>" <?php echo ($judgements->category==$r->cid) ? "selected" : ""; ?>><?php echo $r->c_name; ?></option>
            <?php  }  ?>
        
        </select>

       <span class="help-block">Select any category for this judgement.</span>
    </div>
    <?php //print_r($parentcat); die;?>
</div>

                    <div class="form-group">
                        <input type="hidden" class="form-control" id="metaid" name="metaid" value="<?php echo $judgements->ID; ?>">
                        <label for="metatitle" class="col-md-2 label-heading">Meta Title</label>
                        <div class="col-md-10">  
                            <input type="text" class="form-control" required="" id="metatitle" name="metatitle" value="<?php echo $judgements->meta_title; ?>">
                            
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="username-in" class="col-md-2 label-heading">Meta description</label>
                        <div class="col-md-10">                             
                            <textarea class="form-control" required="" id="metadescription" name="metadescription" rows="4" value="<?php echo $judgements->meta_description; ?>"><?php echo $judgements->meta_description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="password-in" class="col-md-2 label-heading">Meta keywords</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" required="" id="metakey" name="metakey" value="<?php echo $judgements->metakeywords; ?>">
                            <span class="help-block">Note: Add keywords (separated by comma)</span>
                            
                        </div>
                    </div>

                    <div class="block_btn">
                        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_13"); ?>">
                    </div>
            </div>
            </div>
            <?php echo form_close() ?>




        	</div>
        </div>
    </div>

        <script type="text/javascript">
CKEDITOR.replace('post', { height: '300'});

</script>
