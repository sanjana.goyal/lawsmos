
	<div class="row">
		
				<div class="col-md-12">
	<h1 style="font-size:21px; text-align:center;">Edit Page <?php echo $fields[0]->meta_title; ?></h1><hr>
		 <?php echo validation_errors(); ?>

		  
      <?php echo form_open(site_url("admin/edit_pages_pro"), array("class" => "form-horizontal")) ?>
				<div class="form-group">
						<label for="email-in" class="col-md-3 label-heading">Meta Title</label>
						<div class="col-md-9">
							
							
							<input type="hidden" class="form-control" id="email-in" name="metaid" value="<?php echo $fields[0]->ID; ?>">
							<input type="text" class="form-control" id="email-in" name="metatitle" value="<?php echo $fields[0]->meta_title; ?>">
							
						</div>
				</div>
				<div class="form-group">

                        <label for="username-in" class="col-md-3 label-heading">Meta description</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="metadescription" name="metadescription" value="<?php echo $fields[0]->meta_description; ?>">
                             
                            
                        </div>
				</div>
				<div class="form-group">

                        <label for="password-in" class="col-md-3 label-heading">Meta keyword</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="metakey" name="metakey" value="<?php echo $fields[0]->metakeywords; ?>">
                        </div>
                </div>

                <div class="form-group">

                        <label for="cpassword-in" class="col-md-3 label-heading">Content</label>
                        <div class="col-md-9">
                             <textarea name="pagecontent" rows="10" id="pagecontent" style="width: 100%"><?php echo html_entity_decode($fields[0]->content); ?></textarea>
                        </div>
                </div>

                <div class="form-group">

                        <label for="name-in" class="col-md-3 label-heading">Version Control</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="versionc" name="versionc" value="<?php echo $fields[0]->versioncontrol; ?>">
                        </div>
                </div>
				 <div class="form-group">

                        <label for="name-in" class="col-md-3 label-heading">History Notes</label>
                        <div class="col-md-9">
							<textarea name="hotnotes" rows="10" id="honotes" style="resize:none;width: 100%"><?php  echo $fields[0]->historynotes; ?></textarea>
                        </div>
                </div>
                
				<div class="profile-set-wrap">
						<input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_61") ?>" />
						<?php echo form_close() ?>
                </div>
			</div>
			
			 
            
      </div>

    
<script type="text/javascript">
$(document).ready(function() {
CKEDITOR.replace('pagecontent', { height: '500'});


});
</script>
