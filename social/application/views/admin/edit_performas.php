<div class="white-area-content blog_edit">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
        <div class="db-header-extra">
    </div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">Edit Performa </li>
</ol>


<form method="post" action="<?php echo base_url();?>admin/submit_edit_performa">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group">
            <div class="row">
                  <label for="inputEmail3" class="col-sm-3 control-label">Title</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" value="<?php echo $info['title'];?>" id="email-in" name="title">
                    <input type="hidden" name="performa_id" value="<?php echo $info['ID']; ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                  </div>
                </div><br>
                <div class="row">
                  <label for="inputEmail3" class="col-sm-3 control-label">Category</label>
                  <div class="col-sm-9">
                    <select name="category_id" class="form-control">
                        <?php
                          foreach($cats as $cat){
                        ?>
                          <option <?php if($info['category_id'] == $cat['cid']){ echo "selected"; } ?> value="<?php echo $cat['cid'] ?>"><?php echo $cat['c_name'] ?></option>
                        <?php
                          }
                        ?>
                        </select>
                  </div>
                </div><br>
                <div class="row">
                  <label for="status" class="col-sm-3 control-label"><?php echo lang("ctn_606") ?></label>
                  <div class="col-sm-9">
                  <select name="status" class="form-control">
                    
                    <option <?php if($info['status'] == "0") { echo "selected"; } ?> value="0"><?php echo lang("ctn_702") ?></option>
                    <option <?php if($info['status'] == "1") { echo "selected"; } ?> value="1"><?php echo lang("ctn_703") ?></option>
                    </select>
                  </div>
                </div><br>
                <div class="row">
                  <label for="featured" class="col-sm-3 control-label">Featured</label>
                  <div class="col-sm-9">
                  <select name="featured" class="form-control">
                    
                    <option <?php if($info['featured'] == "0") { echo "selected"; } ?> value="0">Off</option>
                    <option <?php if($info['featured'] == "1") { echo "selected"; } ?> value="1">On</option>
                    </select>
                  </div>
                </div><br>
                <div class="row">
                  <label for="inputEmail3" class="col-sm-3 control-label">Content</label>
                  <div class="col-sm-9">
                    <textarea rows="5" cols="90" name="content" id="content"><?php echo $info['content'] ?></textarea>
                  </div>
                </div>
            </div> 
               
            <div class="block_btn" style="margin-top:15px">
                <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_13") ?>">
            </div>
        </div>
    </div>

</div>
