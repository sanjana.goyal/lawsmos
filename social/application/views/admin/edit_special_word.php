<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
    <div class="db-header-extra form-inline">
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">Edit this word</li>
</ol>

<div class="panel panel-default">
<div class="panel-body">
<?php echo form_open(site_url("admin/edit_special_word_pro/" . $words->ID), array("class" => "form-horizontal")) ?>
<div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading">Word</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="word-in" name="word" value="<?php echo $words->word ?>">
                    </div>
            </div>
             <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading">Link</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="link-in" name="link" value="<?php echo $words->link ?>">
                    </div>
            </div>
       <div class="block_btn bj">    
<input type="submit" class="btn btn-primary btn-sm " value="<?php echo lang("ctn_13") ?>" />
    </div>
<?php echo form_close() ?>
</div>
</div>

</div>
