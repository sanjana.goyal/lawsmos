<div class="white-area-content">

<div class="db-header clearfix">

    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>

    <div class="db-header-extra"> 

</div>

</div>



<ol class="breadcrumb">

  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>

  <li class="active"><?php echo lang("ctn_1") ?></li>

</ol>



<p><?php echo lang("ctn_64") ?></p>



<hr>

</div>

<br>

<div class="white-area-content">

	<div class="row">

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

             <a class="feature-list-admin" href="<?php echo base_url('admin/settings');?>">

             <figure>

                 <i class="fa fa-globe" aria-hidden="true"></i>

             </figure>

             <span>Global Settings</span>

            </a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin" href="<?php echo base_url('admin/add_bareacts');?>">

             <figure>

             <i class="fa fa-bookmark" aria-hidden="true"></i>

             </figure>

             <span>Bare Acts</span></a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin" href="<?php echo base_url('admin/verify_university');?>">

             <figure>

             <i class="fa fa-university" aria-hidden="true"></i>

             </figure>

             <span>University Requests</span></a>

		</div>

		

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		  <a class="feature-list-admin" href="<?php echo base_url('admin/promoted_post');?>"> 

             <figure>

             <i class="fa fa-clipboard" aria-hidden="true"></i>

             </figure>

             <span>Promoted Posts</span>

            </a>

		</div>

        

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		  <a class="feature-list-admin" href="<?php echo base_url('admin/judgements');?>"> 

             <figure>

             <i class="fa fa-balance-scale" aria-hidden="true"></i>

             </figure>

             <span>Judgements</span>

            </a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		  <a class="feature-list-admin" href="<?php echo base_url('admin/verify_university');?>"> 

             <figure>

            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

             </figure>

             <span>Students Upgrade Requests</span>

            </a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		<a class="feature-list-admin" href="<?php echo base_url('admin/verify_agency');?>"> 

             <figure>

            <i class="fa fa-gavel" aria-hidden="true"></i>

             </figure>

             <span>Law Firm</span>

            </a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin"  href="<?php echo base_url('admin/categories');?>"> 

             <figure>

            <i class="fa fa-folder" aria-hidden="true"></i>

             </figure>

             <span> Categories</span>

            </a>

		</div>

        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin" href="<?php echo base_url('admin/manage_pages');?>"> 

             <figure>

             <i class="fa fa-file-text" aria-hidden="true"></i>

             </figure>

             <span>Manage Pages</span>

            </a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin" href="<?php echo base_url('admin/members');?>"> 

             <figure>

            <i class="fa fa-users" aria-hidden="true"></i>

             </figure>

             <span> Manage Members</span>

            </a>

		</div>

        	<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin" href="<?php echo base_url('admin/ipbloc');?>"> 

             <figure>

           <i class="fa fa-ban" aria-hidden="true"></i>

             </figure>

             <span>IP Blocking</span>

            </a>

		</div>

		<div class="col-lg-2 col-md-3 col-sm-6 col-xs-6">

		 <a class="feature-list-admin"  href="<?php echo base_url('admin/payment_logs');?>"> 

             <figure>

           <i class="fa fa-google-wallet" aria-hidden="true"></i>

             </figure>

             <span> Payment Logs</span>

            </a>

		</div>

	</div>

</div>

