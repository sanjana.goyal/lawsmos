<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err{
        color: red !important;
    }
    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top:1px;
    }
</style>
<div class="white-area-content">
    <div class="db-header clearfix">
        <div class="page-header-title">
            <span class="glyphicon glyphicon-user"></span> Admin Panel
        </div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li><a href="http://lawsmos.com/social/admin">Admin Panel</a></li>
        <li class="active">Lawyer Detail</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5>HELP OUR SYSTEMS IDENTIFY:</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <thead>
                    <tr>
                        <th>You Work as an Individual Practitioner or with a Law Firm:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['lawyer_practitioner'] == "1"){
                                    echo "Individual";
                                }elseif($coreprofile['lawyer_practitioner'] == "2"){
                                    echo "Firm";
                                }elseif($coreprofile['lawyer_practitioner'] == "3"){
                                    echo "Individual & Firm Both";
                                }else{
                                    echo "N/A";
                                }

                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php if($coreprofile['lawyer_practitioner'] == "2" || $coreprofile['lawyer_practitioner'] == "3"){ ?>
            <table class="table borderless-table">
                <thead>
                    <tr>
                        <th>Firm Name:</th>
                        <th>Firm's Email Address:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['firm_name'])){
                                    echo $coreprofile['firm_name'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                        <?php
                            if(!empty($coreprofile['firm_email'])){
                                echo $coreprofile['firm_email'];
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
                }
            ?>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5>Professional profile information</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <thead>
                    <tr>
                        <th>Are you a practicing lawyer?:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['practicing_lawyer'] == "1"){
                                    echo "Yes";
                                }elseif($coreprofile['practicing_lawyer'] == "2"){
                                    echo "No";
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php if($coreprofile['practicing_lawyer'] == "2"){ ?>
                <table class="table borderless-table">
                <tbody>
                    <tr>
                        <th>City of Practice:</th>
                        <th>City of Practice:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php
                            if(!empty($coreprofile['unpracticing_city'])){
                                echo getCityName($coreprofile['unpracticing_city']);
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                        <td>
                        <?php
                            if(!empty($coreprofile['unpracticing_fields_of_interest'])){
                                echo getCategoryName($coreprofile['unpracticing_fields_of_interest']);
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Court Name:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php
                            if(!empty($coreprofile['unpracticing_area_of_specialization'])){
                                echo getCourtName($coreprofile['unpracticing_area_of_specialization']);
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
            }
            ?>
            <?php if($coreprofile['practicing_lawyer'] == "1"){ ?>
            <table class="table borderless-table">
                <tbody>
                    <tr>
                        <th>City of Practice:</th>
                        <th>Area of practice:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php 
                            if(!empty($coreprofile['practice_city'])){
                                $practice_city = explode(",",$coreprofile['practice_city']);
                                foreach($practice_city as $city){
                                    echo getCityName($city) . ",";
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['category_id_array'])){
                                    $category_id = explode(",",$coreprofile['category_id_array']);
                                    foreach($category_id as $category){
                                        echo getCategoryName($category) . ",";
                                    }
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Court Name:</th>
                        <th>Language:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['field_court_number_array'])){
                                    $courts = explode(",",$coreprofile['field_court_number_array']);
                                    foreach($courts as $court){
                                        echo getCourtName($court);
                                        $isVerifiedCourt = isVerifiedCourt($court);
                                        if(!empty($isVerifiedCourt)){
                                        ?>
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Verify court</button>
                                        <?php    
                                        }
                                        echo ",";
                                    }
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['language'])){
                                    $language = explode(",",$coreprofile['language']);
                                    foreach($language as $lang){
                                        echo getLanguageName($lang) . ",";
                                    }
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>State Bar Council Registration no:</th>
                        <th>Year of registration:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['councel_registration_no'])){
                                    echo $coreprofile['councel_registration_no'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['year_reg'])){
                                    echo $coreprofile['year_reg'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Upload State Bar Council Registration ID:</th>
                        <th>Total Experience:</th>
                    </tr>
                    <tr>
                        <td>
                            <div class="photo-upload">
                                <?php
                                if(!empty($coreprofile['state_bar_form'])){
                                    $state_bar_form = "uploads/".$coreprofile['state_bar_form'];
                                    echo "<img width='150px' src='".base_url($state_bar_form)."'>";
                                }else{
                                    echo "N/A";
                                }
                                ?>
                            </div>
                        </td>
                        <td>
                        <?php
                            if(is_numeric($coreprofile['total_exp'])){
                                echo $coreprofile['total_exp'] ." Years";
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Are you a member of any Bar Association:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php
                            if(!empty($coreprofile['bar_association_member'])){
                                if($coreprofile['bar_association_member'] == 1){
                                    echo "Yes";
                                }elseif($coreprofile['bar_association_member'] == 2){
                                    echo "No";
                                }else{
                                    echo "N/A";
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php if($coreprofile['bar_association_member'] == "1"){ ?>
                <table class="table borderless-table">
                <tbody>
                    <tr>
                        <th>Name of Bar Association:</th>
                        <th>Bar Association Registration no:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php
                            if(!empty($coreprofile['bar_association_name'])){
                                $bar_association_name = json_decode($coreprofile['bar_association_name']);
                                // print_r($bar_association_name);
                                // die();
                                // foreach($bar_association_name as $name){
                                    echo $bar_association_name;
                                // }
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                        <td>
                        <?php
                            if(!empty($coreprofile['registration_no'])){
                                $registration_no = json_decode($coreprofile['registration_no']);
                                echo $registration_no;
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Bar Association Registration ID:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php
                            if(!empty($coreprofile['bar_association_form'])){
                                $bar_association_form = json_decode($coreprofile['bar_association_form']);
                                foreach($bar_association_form as $bar_association_id){
                                    $bar_association_id = "uploads/".$bar_association_id;
                                    echo "<img width='150px' src='".base_url($bar_association_id)."'>";
                                    // echo $bar_association_id;
                                }
                                
                                // echo $coreprofile['bar_association_form'];
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
            }
            ?>
            <?php } ?>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5>Education Information</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <tbody>
                    <tr class="table-sub-title">
                        <td colspan="2">School & High School</td>
                    </tr>
                    <tr>
                        <th>School:10</th>
                        <th>School:12</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['school_10'])){
                                    echo $coreprofile['school_10'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['school_12'])){
                                    echo $coreprofile['school_12'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <th>University:</th>
                        <th>University:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['board_10'])){
                                    echo $coreprofile['board_10'] ;
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['board_12'])){
                                    echo $coreprofile['board_12'] ;
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Year of completion:</th>
                        <th>Year of completion:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['year_pass_10'])){
                                    echo $coreprofile['year_pass_10'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['year_pass_12'])){
                                    echo $coreprofile['year_pass_12'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Marks:</th>
                        <th>Marks:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['x_marks_type'] == 1){
                                    echo "Percentage : ". $coreprofile['marks_x_percentage'];
                                }elseif($coreprofile['x_marks_type'] == 2){
                                    echo "Division : ";
                                    if($coreprofile['x_division'] == 1){
                                        echo "1st";
                                    }elseif($coreprofile['x_division'] == 2){
                                        echo "2nd";
                                    }elseif($coreprofile['x_division'] == 3){
                                        echo "3rd";
                                    }elseif($coreprofile['x_division'] == 4){
                                        echo "Gold Medalist";
                                    }else{
                                        echo "N/A";
                                    }
                                }elseif($coreprofile['x_marks_type'] == 2){
                                    echo "CGPA : ". $coreprofile['marks_x_cgpa'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if($coreprofile['xii_marks_type'] == 1){
                                    echo "Percentage : ". $coreprofile['marks_xii_percentage'];
                                }elseif($coreprofile['xii_marks_type'] == 2){
                                    echo "Division : ";
                                    if($coreprofile['xii_division'] == 1){
                                        echo "1st";
                                    }elseif($coreprofile['xii_division'] == 2){
                                        echo "2nd";
                                    }elseif($coreprofile['xii_division'] == 3){
                                        echo "3rd";
                                    }elseif($coreprofile['xii_division'] == 4){
                                        echo "Gold Medalist";
                                    }else{
                                        echo "N/A";
                                    }
                                }elseif($coreprofile['xii_marks_type'] == 2){
                                    echo "CGPA : ". $coreprofile['marks_xii_cgpa'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    
                    <?php
                        if(!empty($coreprofile['degree_array'])){
                            $degree_array = json_decode($coreprofile['degree_array']);
                            $yearstart_array = json_decode($coreprofile['yearstart_array']);
                            $yearend_array = json_decode($coreprofile['yearend_array']);
                            $college_array = json_decode($coreprofile['college_array']);
                            $university_array = json_decode($coreprofile['university_array']);
                            $division_array = json_decode($coreprofile['division_array']);
                            $upload_marksheet_array = json_decode($coreprofile['upload_marksheet_array']);
                            $professional_qualification_count = count($degree_array);
                            // print_r($degree_array);
                            // die();
                        }
                        for($i = 0; $i < $professional_qualification_count; $i++){
                    ?>

                    <tr class="table-sub-title">
                        <td colspan="2">Professional Qualification <?php echo $i + 1; ?></td>
                    </tr>
                    <tr>
                        <th>Degree/ Course Name:</th>
                        <th>Start Year:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($degree_array[$i])){
                                    echo $degree_array[$i];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($yearstart_array[$i])){
                                    echo $yearstart_array[$i];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Date:</th>
                        <th>End Year:</th>
                    </tr>
                    <tr>
                        <td>
                        <?php
                            if(!empty($yearend_array[$i])){
                                echo $yearend_array[$i];
                            }else{
                                echo "N/A";
                            }
                        ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($college_array[$i])){
                                    echo $college_array[$i];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>University Name:</th>
                        <th>Division:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($university_array[$i])){
                                    echo $university_array[$i];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($division_array[$i])){
                                    // echo "Division : ";
                                    if($division_array[$i] == 1){
                                        echo "1st";
                                    }elseif($division_array[$i] == 2){
                                        echo "2nd";
                                    }elseif($division_array[$i] == 3){
                                        echo "3rd";
                                    }elseif($division_array[$i] == 4){
                                        echo "Gold Medalist";
                                    }else{
                                        echo "N/A";
                                    }
                                    // echo $division_array[$i];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <?php if ($upload_marksheet_array[$i]) { ?>
                    <tr>
                        <td>
                            <img src="<?=base_url('uploads/'.$upload_marksheet_array[$i])?>" width="200px">
                        </td>
                    </tr>
                    <?php
                        } }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5>Professional Experience</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <tr>
                    <th>Name of organization</th>
                    <th>City</th>
                </tr>
                <tr>
                    <td>
                        <?php
                            if(!empty($coreprofile['experience_organization_name'])){
                                $experience_organization_name = json_decode($coreprofile['experience_organization_name']);
                                foreach($experience_organization_name as $org_name){
                                    echo $org_name. ",";
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if(!empty($coreprofile['experience_city'])){
                                $experience_city = json_decode($coreprofile['experience_city']);
                                foreach($experience_city as $city_name){
                                    echo getCityName($city_name). ",";
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>Court</th>
                    <th>Designation</th>
                </tr>
                <tr>
                    <td>
                        <?php
                            if(!empty($coreprofile['experience_court'])){
                                $experience_court = json_decode($coreprofile['experience_court']);
                                foreach($experience_court as $court_name){
                                    echo getCourtName($court_name). ",";
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if(!empty($coreprofile['experience_designation'])){
                                $experience_designation = json_decode($coreprofile['experience_designation']);
                                foreach($experience_designation as $desig_name){
                                    echo $desig_name. ",";
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
                <tr>
                    <td>
                        <?php
                            if(!empty($coreprofile['experience_start_date']) && $coreprofile['experience_start_date'] != "null"){
                                $experience_start_date = json_decode($coreprofile['experience_start_date']);
                                foreach($experience_start_date as $start_date){
                                    if(!empty($start_date)){
                                        echo $start_date. ",";
                                    }
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if(!empty($coreprofile['experience_end_date']) && $coreprofile['experience_end_date'] != "null"){
                                $experience_end_date = json_decode($coreprofile['experience_end_date']);
                                foreach($experience_end_date as $end_date){
                                    if(!empty($end_date)){
                                        echo $end_date. ",";
                                    }
                                }
                            }else{
                                echo "N/A";
                            }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5>Consultation Information</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <tbody>
                    <tr class="table-sub-title">
                        <td colspan="3">Consultation Timings</td>
                    </tr>
                    <tr>
                        <th>Days</th>
                        <th>From - To (24 hours)</th>
                    </tr>
                    <?php
                        if(!empty($consultation_timing)){
                            foreach($consultation_timing as $timing){
                                ?>
                                    <tr>
                                        <td><?php echo $timing['day_name']; ?></td>
                                        <td>
                                            0<?php echo $timing['start_hour'] . ":0" .$timing['start_min'] ?>
                                             - 0<?php echo $timing['end_hour'] . ":0" .$timing['end_min'] ?>
                                            
                                        </td>
                                        <!-- <td><?php echo $timing['end_hour'] . ":" .$timing['end_min'] ?></td> -->
                                    </tr>    
                                <?php
                            }
                        }else{
                            ?>
                            <tr>
                                <td colspan="3">No result found</td>
                            </tr>
                            <?php
                        }
                        
                    ?>
                    <!-- <tr>
                        <td>Monday</td>
                        <td>2Hour 15min</td>
                        <td>2Hour 15min</td>
                    </tr>
                    <tr>
                        <td>Tuesday</td>
                        <td>2Hour 15min</td>
                        <td>2Hour 15min</td>
                    </tr>
                    <tr>
                        <td>Friday</td>
                        <td>2Hour 15min</td>
                        <td>2Hour 15min</td>
                    </tr> -->
                    <tr>
                        <th>In Office Consultation</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['office_consultation_type'] == 1){
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                        if($coreprofile['office_consultation_type'] == 1){
                    ?>
                        <?php
                            if(!empty($coreprofile['office_charges_checkbox_30_min'])){
                        ?>
                            <tr>
                                <th>Consultation time(Office)</th>
                                <th>Consultation Charges(Office)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['office_charges_30min'])){
                                            echo $coreprofile['office_charges_30min'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        <?php
                            if(!empty($coreprofile['office_charges_checkbox_1_hour'])){
                        ?>
                            <tr>
                                <th>Consultation time(Office)</th>
                                <th>Consultation Charges(Office)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['office_charges_1hour'])){
                                            echo $coreprofile['office_charges_1hour'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    <?php
                        }
                    ?>
                    <tr>
                        <th>Client Site Consultation</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['site_consultation_type'] == 1){
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                        if($coreprofile['office_consultation_type'] == 1){
                    ?>
                        <?php
                            if(!empty($coreprofile['site_charges_checkbox_30_min'])){
                        ?>
                            <tr>
                                <th>Consultation time(Client Site)</th>
                                <th>Consultation Charges(Client Site)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['site_charges_30min'])){
                                            echo $coreprofile['site_charges_30min'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        <?php
                            if(!empty($coreprofile['site_charges_checkbox_1hour'])){
                        ?>
                            <tr>
                                <th>Consultation time(Client Site)</th>
                                <th>Consultation Charges(Client Site)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['site_charges_1hour'])){
                                            echo $coreprofile['site_charges_1hour'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    <?php
                        }
                    ?>
                    <tr>
                        <th>Telephonic Consultation (Voice)</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['telvoice_consultation_type'] == 1){
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                        if($coreprofile['telvoice_consultation_type'] == 1){
                    ?>
                        <?php
                            if(!empty($coreprofile['telvoice_charges_checkbox_30_min'])){
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Voice)</th>
                                <th>Consultation Charges(Telephonic Voice)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['telvoice_charges_30min'])){
                                            echo $coreprofile['telvoice_charges_30min'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        <?php
                            if(!empty($coreprofile['telvoice_charges_checkbox_1hour'])){
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Voice)</th>
                                <th>Consultation Charges(Telephonic Voice)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['telvoice_charges_1hour'])){
                                            echo $coreprofile['telvoice_charges_1hour'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    <?php
                        }
                    ?>
                    <tr>
                        <th>Telephonic Consultation(Video)</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['telvideo_consultation_type'] == 1){
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                        if($coreprofile['telvideo_consultation_type'] == 1){
                    ?>
                        <?php
                            if(!empty($coreprofile['telvideo_charges_checkbox_30_min'])){
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Video)</th>
                                <th>Consultation Charges(Telephonic Video)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['telvideo_charges_1hour'])){
                                            echo $coreprofile['telvideo_charges_1hour'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                        <?php
                            if(!empty($coreprofile['telvideo_charges_checkbox_1hour'])){
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Video)</th>
                                <th>Consultation Charges(Telephonic Video)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                        if(!empty($coreprofile['office_charges_1hour'])){
                                            echo $coreprofile['office_charges_1hour'] . " Rs.";
                                        }else{
                                            echo "N/A";
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    <?php
                        }
                    ?>
                    <tr>
                        <th>Email Consultation</th>
                            <?php
                                if($coreprofile['email_consultation_type'] == 1){
                            ?>
                                <th>Email Consultation Charges</th>
                            <?php
                                }
                            ?>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if($coreprofile['email_consultation_type'] == 1){
                                    echo "Yes";
                                }else{
                                    echo "No";
                                }
                            ?>
                        </td>
                        <?php
                            if($coreprofile['email_consultation_type'] == 1){
                        ?>
                            <td>
                                <?php
                                    if(!empty($coreprofile['email_consultation_charges'])){
                                        echo $coreprofile['email_consultation_charges'] . " Rs.";
                                    }else{
                                        echo "N/A";
                                    }
                                ?>
                            </td>
                        <?php
                            }
                        ?>
                    </tr>
                    <tr>
                        <th>Appearance charges</th>
                        <th>Hourly Drafting charges</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($coreprofile['apperance_charges'])){
                                    echo $coreprofile['apperance_charges'] . " Rs.";
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($coreprofile['haur_draft_charges'])){
                                    echo $coreprofile['haur_draft_charges'] . " Rs.";
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5>Address Details</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <tbody>
                    <tr>
                        <th>Address</th>
                        <th>City</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($user_info['address_1'])){
                                    echo $user_info['address_1'];
                                    if(!empty($user_info['address_2'])){
                                        echo ", ". $user_info['address_2'];
                                    }
                                    if(!empty($user_info['address3'])){
                                        echo ", ". $user_info['address3'];
                                    }
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($user_info['city'])){
                                    echo getCityNAme($user_info['city']);
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>State/Region/Province</th>
                        <th>Country</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($user_info['state'])){
                                    echo getStateName($user_info['state']);
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            India
                        </td>
                    </tr>
                    <tr>
                        <th>Latitude</th>
                        <th>Longitude</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($user_info['lat'])){
                                    echo $user_info['lat'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($user_info['longitude'])){
                                    echo $user_info['longitude'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Pincode</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                                if(!empty($user_info['zipcode'])){
                                    echo $user_info['zipcode'];
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                    </tr>
                   
                    
                    
                </tbody>
            </table>
        </div>
    </div>
    <?php
        if($user_info['admin_verified'] == 0){
    ?>
    <div class="card">
        <div class="card-head">
            <h5>Verification</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <tbody>
                    <tr>
                        <th>
                            Enter Message (Optional for Approved)
                        </th>
                    </tr>
                    <tr>
                        <td>   
                            <textarea style="width:100%" name="message" id="message"></textarea>
                            <p id="message_err" class="err" style="display:none"></p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="javascript:void(0);" class="btn btn-primary" onclick="approve_lawyer(<?php echo $user_info['ID']; ?>);">
                                <span class="glyphicon glyphicon-ok"></span> Approve
                            </a>
                            <a href="javascript:void(0);" class="btn btn-primary" onclick="reject_lawyer(<?php echo $user_info['ID']; ?>);">
                                <span class="glyphicon glyphicon-remove" ></span> Reject
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>    
    </div>   
    <?php  } ?> 
    <div class="card">
        <div class="card-head">
            <h5>Admin Logs</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table">
                <tbody>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Status</th>
                        <th>Message</th>
                        <th>Created at</th>
                    </tr>
                    <?php
                        if(empty($verification_logs)){
                            ?>
                            <tr>
                                <td colspan="4" style="text-align:center;">No results found</td>
                            </tr>
                            <?php
                        }else{
                            $count = 0;
                            foreach($verification_logs as $verification_log){
                                $count++;
                                ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo ($verification_log['status'] == 1) ? "Approved" : ( $verification_log['status'] == 2 ? "Rejected" : "N/A"); ?></td>
                                    <td><?php echo !empty($verification_log['message']) ? $verification_log['message'] : "N/A";?></td>
                                    <td><?php echo $verification_log['created_at'];?></td>
                                </tr>
                                <?php
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="top:100px !important">
  <div class="modal-dialog">
    <div class="modal-content">
    <form method="post" action="<?php echo base_url();?>admin/verify_court">

    <?php
    $courts = explode(",",$coreprofile['field_court_number_array']);
    $court_name = "";
    $court_id = "";
    foreach($courts as $court){
        $isVerifiedCourt = isVerifiedCourt($court);
        if(!empty($isVerifiedCourt)){
            $court_id  = $court;
            $court_name = getCourtName($court);
        }
    }
    ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Verify Court</h4>
      </div>
      <div class="modal-body">
            <div class="form-group">
              <div class="row">
                <label for="court_name" class="col-sm-3 control-label">Court name</label>
                <div class="col-sm-9">
                    <input type="text" id="court_name" name="court_name" class="form-control" value="<?php echo $court_name; ?>" required>
                    <input type="hidden" id="court_id" name="court_id" value="<?php echo $court_id; ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <p class="err" id="court_name_err"></p>
                </div>
              </div>  
            </div>
      </div>
      <div class="modal-footer" >
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="button" class="btn btn-primary" value="Submit" onclick="verify_court();">
      </div>
      </form>
    </div>
   </div> 
</div>

<script>
    function approve_lawyer(lawyer_id){
        var message = $("#message").val();
        if(lawyer_id != ""){
            $.ajax({
                url : "<?php echo site_url("admin/approveLawyer") ?>",
                type: "POST",
                dataType:"json",
                data:{
                    'lawyer_id':lawyer_id,
                    'message':message,
                    'status':1,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success:function(data){
                    window.location.reload();   
                },
                error:function(error){
                    console.log(`Error ${error}`);
                }
            }); 
        } 
    }
    function reject_lawyer(lawyer_id){
        var message = $("#message").val();
        $("#message_err").html("");
        if(message == ""){
            $("#message_err").html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg>Please enter your message');
            $("#message_err").show();
            return false;
        }
        if(lawyer_id != ""){
            $.ajax({
                url : "<?php echo site_url("admin/approveLawyer") ?>",
                type: "POST",
                dataType:"json",
                data:{
                    'lawyer_id':lawyer_id,
                    'message':message,
                    'status':2,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success:function(data){
                    window.location.reload();   
                },
                error:function(error){
                    console.log(`Error ${error}`);
                }
            }); 
        } 
    }
    function verify_court(){

        var court_id = $("#court_id").val();
        var court_name = $("#court_name").val();
        if(court_name == ""){
            $("#court_name_err").html("Please enter court name");
            return false;
        }
        // if(!empty(court_id,court_name )){
            $.ajax({
                url : "<?php echo site_url("admin/verify_court") ?>",
                type: "POST",
                dataType:"json",
                data:{
                    'court_id':court_id,
                    'court_name':court_name,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success:function(data){
                    window.location.reload();   
                },
                error:function(error){
                    console.log(`Error ${error}`);
                }
            }); 
        // }
    }
</script>