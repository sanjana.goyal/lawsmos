<div class="white-area-content">
	 <?php echo validation_errors(); ?>
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
    <div class="db-header-extra form-inline">


<div class="form-group has-feedback no-margin">
<div class="input-group">

<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        
        
        <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
			  <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
			  <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
			  <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="user-exact"></span> <?php echo lang("ctn_339") ?></a></li>
			  <li><a href="#" onclick="change_search(3)"><span class="glyphicon glyphicon-ok no-display" id="fn-exact"></span> <?php echo lang("ctn_340") ?></a></li>
			  <li><a href="#" onclick="change_search(4)"><span class="glyphicon glyphicon-ok no-display" id="ln-exact"></span> <?php echo lang("ctn_341") ?></a></li>
			  <li><a href="#" onclick="change_search(5)"><span class="glyphicon glyphicon-ok no-display" id="role-exact"></span> <?php echo lang("ctn_342") ?></a></li>
			  <li><a href="#" onclick="change_search(6)"><span class="glyphicon glyphicon-ok no-display" id="email-exact"></span> <?php echo lang("ctn_78") ?></a></li>
        </ul>
     
     </div><!-- /btn-group -->
</div>
</div>

  <!--  <input type="button" class="btn btn-primary btn-sm" value="Add Pages" data-toggle="modal" data-target="#memberModal" /> -->
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">Manage Pages</li>
</ol>



<div class="table-responsive">
<table id="member-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header"><td>Page Name</td><td>Page description</td><td>Last Updated</td><td>Options</td></tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#member-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12'i><'col-sm-12'p>>",
      "processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "columns": [
		null, 
		null, 
		null,
		
		
		
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("admin/members_page_settings") ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
        "search-like", 
        "search-exact",
        "user-exact",
        "fn-exact",
        "ln-exact",
        "role-exact",
        "email-exact"
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>




    <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang("ctn_73") ?></h4>
      </div>
      <div class="modal-body">
		  
		  
		 <?php echo validation_errors(); ?>

		  
      <?php echo form_open(site_url("admin/add_pages_pro"), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading">Meta Title</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="email-in" name="metatitle">
                        <?php echo form_error('metatitle'); ?>
                    </div>
            </div>
            <div class="form-group">

                        <label for="username-in" class="col-md-3 label-heading">Meta description</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="metadescription" name="metadescription">
                              <?php echo form_error('metadescription'); ?>
                            
                        </div>
            </div>
            <div class="form-group">

                        <label for="password-in" class="col-md-3 label-heading">Meta keyword</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="metakey" name="metakey" value="">
                        </div>
                </div>

                <div class="form-group">

                        <label for="cpassword-in" class="col-md-3 label-heading">Content</label>
                        <div class="col-md-9">
                             <textarea name="pagecontent" id="pagecontent"></textarea>
                        </div>
                </div>

                <div class="form-group">

                        <label for="name-in" class="col-md-3 label-heading">Version Control</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="versionc" name="versionc">
                        </div>
                </div>
              
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_61") ?>" />
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
CKEDITOR.replace('pagecontent', { height: '150'});

CKEDITOR.replace('historynotes', { height: '150'});
});
</script>
