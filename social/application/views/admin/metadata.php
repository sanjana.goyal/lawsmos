<div class="white-area-content margin_left">
	<div class="db-header clearfix">
		<div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>

	</div>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
		<li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  		<li class="active"><?php echo lang("ctn_996") ?></li>
  	</ol>
  	<hr>
  	<div class="panel panel-default">
  		<div class="panel-body">
  			<?php echo form_open(site_url("admin/metadata"), array("class" => "form-horizontal")) ?>
  				<div class="form-group">
					<label for="select_page" class="col-sm-3 control-label"><?php echo lang("ctn_997") ?></label>
					<div class="col-md-8">
						<select class="select_page form-control" id="select_page">
							<option value="">Select</option>
							<option value="home">Home Page</option>
							<option value="privacy">Privacy policy</option>
							<option value="terms">Terms and Conditions</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="meta_title" class="col-sm-3 control-label"><?php echo lang("ctn_998") ?></label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="title" id="meta_title">
					</div>
				</div>
				<div class="form-group">
					<label for="meta_description" class="col-sm-3 control-label"><?php echo lang("ctn_999") ?></label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="description" id="meta_description">
					</div>
				</div>
				<div class="form-group">
					<label for="meta_keywords" class="col-sm-3 control-label"><?php echo lang("ctn_1000") ?></label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="keywords" id="meta_keywords">
					</div>
				</div>
				<div class="form-group">
					<label for="meta_content" class="col-sm-3 control-label"><?php echo lang("ctn_1001") ?></label>
					<div class="col-md-8">
						<input class="form-control" type="text" name="content" id="meta_content">
					</div>
				</div>
				<div class="form-group block_btn">
					<input class="btn btn-primary" type="submit" name="metasubmit" value="<?=lang('ctn_61')?>">
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>