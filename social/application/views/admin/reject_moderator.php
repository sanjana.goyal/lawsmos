<div class="row">

	<div class="col-md-12">
		<h1 style="font-size:18px;">Reject Moderator Request.</h1>
		
			<?php echo form_open(site_url('admin/rejectmodrerators/'.$users->ID), array("id" => "register_form")) ?>
			<div class="form-group">
				<label for="reject-reason">Name of advocate:</label>
				<input type="text" class="form-control" value="<?php echo $users->first_name." ".$users->last_name; ?>" readonly />
			</div>
			<div class="form-group">
				<label for="reject-reason">Total Points Earned:</label>
				<input type="text" class="form-control" value="<?php echo $users->total_points; ?>" readonly />
			</div>
			<div class="form-group">
				<label for="reject-reason">Reason for Rejection</label>
				<textarea name="reason" class="form-control"></textarea>
			</div>
			
			<button type="submit" class="btn btn-danger">Submit</button>
		
			<?php echo form_close(); ?>
	</div>

</div>
