<div class="white-area-content">
	<div class="db-header clearfix">
		<div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_1") ?></div>
		<div class="db-header-extra form-inline">

					<div class="form-group has-feedback no-margin">
	<div class="input-group">
	<input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
		<div class="input-group-btn">
		<input type="hidden" id="search_type" value="0">
			<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	<span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
			
			<ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
			  <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
			  <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
			  <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> <?php echo lang("ctn_81") ?></a></li>
			</ul>
		  
		  </div><!-- /btn-group -->
	</div>
</div>

    <input type="button" class="btn btn-primary btn-sm" value="Add New Word" data-toggle="modal" data-target="#addnewModal" />
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("admin") ?>"><?php echo lang("ctn_1") ?></a></li>
  <li class="active">All Special Words</li>
</ol>


<div class="table-responsive">
<table id="member-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header">
	
	<td>Word</td>
	<td>Link</td>
	<td>Options</td>
	
</tr>
</thead>
<tbody>
	
</tbody>
</table>


</div>


</div>




<!-- Add new model -->


  <div class="modal fade" id="addnewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang("ctn_457") ?></h4>
      </div>
      <div class="modal-body">
      <?php echo form_open(site_url("admin/add_new_word"), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading">Word</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="word-in" name="word">
                    </div>
            </div>
             <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading">Link</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="link-in" name="link">
                    </div>
            </div>

            
            

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_61") ?>" />
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>









<script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
   
   var table = $('#member-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
		"processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
       
       
        
       
        { "orderable" : false },
        null,
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url('admin/view_special_words_page') ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
                
                
                
            }
        },
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
        "search-like", 
        "search-exact",
        "reason-exact",
        "user-exact",
        "page-exact",
        "from-exact",
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon,options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>

