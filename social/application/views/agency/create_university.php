<style>
.row.fdgfdgfd {
    margin-top: 36px;
    margin-bottom: 3px;
}
</style>
<div class="row">

<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
	<?php include(APPPATH . "views/user_settings/sidebar.php"); ?>
</div>


<?php 

	//print_r($agency);
	
	$agency=$this->user_model->get_user_by_id($this->user->info->ID);
				$agency = $agency->row();
	
	 $agency_status = $agency->agency_verified;
	
	if($agency_status==0){

?>

 <div class="col-md-9">

<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-lock"></span> Create University Page</div>
    <div class="db-header-extra"> 
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li><a href="<?php echo site_url("user_settings/create_agency") ?>"><?php echo lang("ctn_224") ?></a></li>
  <li class="active"><?php echo lang("ctn_829") ?></li>
</ol>

<p><?php echo lang("ctn_872") ?></p>
<hr>


<div class="panel panel-default">
<div class="panel-body">
 <div class="form_error">
          <?php echo validation_errors(); ?>
  </div>	
	
<p class="panel-subheading"><?php echo lang("ctn_227") ?></p>
<?php echo form_open_multipart(site_url("user_settings/university"), array("class" => "form-horizontal")) ?>
		
		
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">Select University</label>
	    <div class="col-sm-10">
			
			<select class="form-control" name="uni_name" >
				<option>select</option>
				<option value="1">Guru Kashi University</option>
				<option value="2">Punjabi University Patiala</option>
				<option value="3">Punjab University Chandigarh</option>
				<option value="4">Kurukshetar University</option>
				
				
				
			</select>
	     
	    </div>
	</div>
		
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_832") ?></label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="contact_name"  value="<?php //echo $this->user->info->first_name ?>">
	    </div>
	</div>
	
	
    <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">University Email</label>
	    <div class="col-sm-10">
	      <input type="email" class="form-control" name="email"   value="<?php //echo $this->user->info->email ?>">
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">University Contact Number</label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control" name="phone"  value="<?php //echo $this->user->info->email ?>">
	    </div>
	</div>
	
	<!--
	
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php //echo lang("ctn_835") ?></label>
	    <div class="col-sm-10">
	      <input type="text" name="address_1" class="form-control"  value="<?php //echo $this->user->info->address_1 ?>">
	    </div>
	</div>
	
	
	-->
	
	
	
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label">Upload Document(PDF format)</label>
	    <div class="col-sm-10">
	    
	    <?php if($this->settings->info->avatar_upload) : ?>
	     	<input type="file" name="userfile" required /> 
	     <?php endif; ?>
	    </div>
	</div>
	
	
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_833") ?></label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control map_name"  name="location_from" value="<?php //echo $this->user->info->location_from ?>">
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_865") ?></label>
	    <div class="col-sm-10">
	     
	      <input type="text" class="form-control"  name="p_address" placeholder="Enter address" value="<?php //echo $this->user->info->address_1 ?>">
		 
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_866") ?></label>
	    <div class="col-sm-10">
	     
	      <input type="text" class="form-control map_name" id="us3-address" name="map_location" value="<?php echo $this->user->info->address_1 ?>">
	    
	    
	    
			<input type="hidden" class="form-control" name="lat"  value=""  id="us3-lat" />
			<input type="hidden" class="form-control" name="long"  value="" id="us3-lon" />
	    </div>
	</div>
	
	
	<p id="map_view"></p>
	<div class="container">
<div class="row fdgfdgfd">
	<div class="col-md-12">
	<div class="form-group">
	
		
		<div id="us3"  style="position: relative;overflow: hidden;width: 625px;height: 248px;" ></div>
                  
		
					
		</div>
	
	</div>


</div>
</div>
	

	 <input type="submit" name="s" value="<?php echo lang("ctn_838") ?>" class="btn btn-primary form-control" />
<?php echo form_close() ?>
</div>

<?php }else{
	
	
	?> 
	
<h3>Your request to create university page has been under review ! You will be notified once administrator approve your request</h3>

	<?php 
	
	}

	?>
</div>





 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
  
  
  
  <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"></script>

				<script>
			
			
			
					var $j=jQuery.noConflict();
					
			$( "#map_view" ).html( "Drag and drop map marker to get exact location for more accuracy" );	
            $j("#us3").locationpicker({
                location: {
					
					
					//latitude:30.64079866685497,
					//longitude:76.8260851832307
                    latitude: <?php echo $this->user->info->lat; ?>,
                    longitude: <?php echo $this->user->info->longitude; ?>
					
                },
                radius: 500,
                inputBinding: {
                    latitudeInput:  $j("#us3-lat") ,
                    longitudeInput:$j("#us3-lon"),
                    radiusInput: $j("#us3-radius"),
                    locationNameInput: $j("#us3-address")
                },
                enableAutocomplete: true,
                onchanged: function (currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                  //  alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
            
	

        </script>
