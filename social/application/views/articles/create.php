
<?php
$tem_base_url = str_replace('/social','',base_url());
?>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="content-right-area case-builder-area">
    <div class="area-bar-top">
        
            <div class="col-md-8">
                <ul class="left-top-editor list-unstyled">
                    <li>
                        <a href="#" class="active"><span><img src="<?=$tem_base_url.'/assets/images/case-icon.png'?>"
                                    alt=""></span>Case
                            Builder</a>
                    </li>
                    <li>
                        <a href="#"><span><img src="<?=$tem_base_url.'/assets/images/articles-icon.png'?>"
                                    alt=""></span>Write an Articles</a>
                    </li>
                    <li>
                        <a href="#"><span><img src="<?=$tem_base_url.'/assets/images/research-icon.png'?>"
                                    alt=""></span> My
                            Research</a>
                    </li>
                    <li>
                        <select name="" id="">
                            <option value="">Select Catagory</option>
                            <option value="">Select Catagory</option>
                            <option value="">Select Catagory</option>
                        </select>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 ">
                <ul class="right-editor-sec text-right list-unstyled">
                    <li>
                        <a href="#" class="save-sec">Save</a>
                    </li>
                    <li>
                        <a href="#" class="hide-sec">Hide reference<span><img
                                    src="<?=$tem_base_url.'/assets/images/cross-01.png'?>" alt=""></span></a>
                    </li>
                </ul>
            </div>
        <div class="clearfix"></div>
    </div>
    <section class="performance-sec">
        <div class="mid-bar-sec">
            <!-- <div class="row"> -->
                <div class="col-md-12">
                    <h5>References</h5>
                    <div class="bar-sec">
                        <ul class="list-unstyled">
                            <li><a href="#" class="active">Performas</a></li>
                            <li><a href="#">Judgments</a></li>
                            <li><a href="#">Supreme Judgments</a></li>
                            <li><a href="#">Bare Acts</a></li>
                            <li><a href="#">Legal Terms</a></li>
                            <li><a href="#">Articles</a></li>
                            <li><a href="#">Maxims</a></li>
                        </ul>
                    </div>
                </div>
            <!-- </div>-->
        
        <div class="clearfix"></div> 
        </div>
    </section>

    <section class="draft-sec">
        <div class="description-sec">
            <!-- <div class="row"> -->
                <div class="col-lg-6 des-border-right">
                    <h5>Following is the sample draft for reference which may further be modified as per requirement
                    </h5>

                    <div class="content-sec">
                    <textarea class="form-control" name="description" id="description" placeholder="Content">
                            <?php echo (!empty($article->description)) ? base64_decode($article->description) : ''; ?>
                    </textarea>
                    <span><b>Note</b>: Articles content having words between 300-1000 are generally read more by users.</span>
                    <div class="error" id="error_description"></div>
                </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-search-performa">
                        <input type="text" placeholder="Search performa" class="search_data">
                        <ul class="list-sec">
                            <li>Legal notice/ reply to legal notice</li>
                            <li>Write petition</li>
                            <li>Divorce petition/reply</li>
                            <li>Business contract</li>
                            <li>Criminal complaint/defense</li>
                            <li>Plaint/ reply</li>
                            <li>Lease deed</li>
                            <li>Employment contract</li>
                            <li>Maintenance application/ reply</li>
                            <li>Consumer case</li>
                            <li>Domestic violence application/ reply</li>
                        </ul>

                    </div>

                </div>


            <!-- </div> -->
        <div class="clearfix"></div> 

        </div>
    </section>





    <div class="container" style="display:none">
        <div class="row">
            <div class="col-md-6">
                <div class="db-header clearfix">
                    <div class="page-header-title"> <span class="glyphicon glyphicon-pencil"></span> Write an article
                    </div>
                    <div class="db-header-extra form-inline">
                    </div>
                </div>
                <form method="POST" action="<?php echo  site_url("create/article"); ?>"
                    onsubmit="return validateForm()">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                        value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Title</label> -->
                                <div class="col-sm-12">
                                    <input type="text" id="title" name="title" class="form-control"
                                        placeholder="Enter Title">
                                    <div class="error" id="error_title"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Select Catagory</label> -->
                                <div class="col-sm-12">
                                    <select name="cat" id="cat" class="form-control">
                                        <option value="">Select catagory</option>
                                        <?php foreach ($catagory as $catg) { ?>
                                        <option value="<?php echo $catg->cid; ?>"><?php echo $catg->c_name; ?></option>
                                        <?php
                                                                                                                        } ?>
                                    </select>
                                    <div class="error" id="error_cat"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Select Subcatagory</label> -->
                                <div class="col-sm-12">
                                    <select name="subcat" id="subcat" class="form-control">
                                        <option value="">Select Subcatagory</option>
                                    </select>
                                    <div class="error" id="error_subcat"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Article Description</label> -->
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="description" id="description"
                                        placeholder="Content"></textarea>
                                    <span><b>Note</b>: Articles content having words between 300-1000 are generally read
                                        more by users.</span>
                                    <div class="error" id="error_description"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Article Keywords</label> -->
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="keywords" id="keywords" value=""
                                        placeholder="Keywords">
                                    <div class="error" id="error_keywords">
                                        <?php echo $this->session->flashdata('error_key') ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label> -->
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="metadescription" maxlength="50"
                                        id="metadescription" placeholder="Meta description"></textarea>
                                    <span><b>Note</b>: In 50 words mention what this article is about and what all does
                                        it cover!</span>
                                    <div class="error" id="error_metadescription"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_775") ?></label> -->
                                <div class="col-sm-12">
                                    <select name="private" class="form-control" id="private">
                                        <option value="2"><?php echo lang("ctn_539") ?></option>
                                        <option value="1"><?php echo lang("ctn_633") ?></option>
                                    </select>
                                    <div class="error_private"></div>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">
                            </div>
                        </div> -->
                            <div class="block_btn">
                                <input type="submit" class="btn btn-primary" value="Create Article"
                                    style="margin-top: 20px;">
                            </div>
                        </div>
                    </div>
                    <!-- <?php echo form_close() ?> -->
                </form>
            </div>
            <div class="col-md-6">
                <!-- <iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe'); ?>"
                    width="100%" height="1300px" style="border:none;"></iframe> -->
            </div>
        </div>
    </div>
</div>
<!-- </div> -->
<script type="text/javascript">
    CKEDITOR.replace('description');
</script>