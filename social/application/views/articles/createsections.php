
<div class="row">
        <div class="col-md-12">
        	<div class="white-area-content margin_left ">
        		
        		<div class="db-header clearfix">
				    <div class="page-header-title"> <span class="glyphicon glyphicon-pencil"></span>Create Section</div>
				   
				</div>


        <?php echo form_open_multipart(site_url("articles/add_section_pro"), array("class" => "form-horizontal")) ?>
            <div class="panel panel-default">
            <div class="panel-body">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Section title</label>
                <div class="col-sm-10">
                    <input type="text" name="title" class="form-control">
                </div>
                
                 <div class="col-sm-10">
                    <input type="hidden" name="artcleid" class="form-control" value="<?php   echo $this->uri->segment('3'); ?>">
                </div>
            </div>
          
            
			
            
            <?php if(!empty($sections)){  ?>
				
				
            <div class="form-group">
				
				<label for="inputEmail3" class="col-sm-2 control-label">Parent Section</label>
                <div class="col-sm-10">
               <select name="parentid" class="form-control">
						<option>Select Parent Section</option>
						<option value="0">None</option>
						<?php foreach($sections as $sections): ?>
						<option value="<?php echo $sections['ID']; ?>"><?php echo $sections['title']; ?></option>
						<?php endforeach; ?>
               </select>
                </div>
            </div>
             <?php }else{ ?>
				  <div class="form-group">
					    <div class="col-sm-10">
						<input type="hidden" value="0" name="parentid" class="form-control" />
						</div>
				 </div>
			<?php } ?>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Section Content</label>
                <div class="col-sm-10">
                    <textarea name="section_content" id="post"></textarea>
                </div>
            </div>
          
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Section <?php echo lang("ctn_770") ?> (optional)</label>
                <div class="col-sm-10">
                    <input type="file" name="userfile" class="form-control">
                </div>
            </div>
                <div class="block_btn">
            <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_506") ?>">
                </div>
            </div>
            </div>
            <?php echo form_close() ?>

                


             




        	</div>
        </div>
    </div>
    <script type="text/javascript">
CKEDITOR.replace('post', { height: '300'});

</script>
