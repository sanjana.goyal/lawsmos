<style>
    .error {
        color: red;
        padding: 5px 2px;
    }

    textarea.form-control {
        height: auto;
        margin-bottom: 0px;
        resize: vertical;
    }

    .cke_top,
    .cke_contents,
    .cke_bottom {
        height: auto !important;
    }
</style>

<?php $this->load->view('sidebar/sidebar.php'); ?>
<!-- <div class="white-area-content margin_left margin_repo separator page-right"> -->
<div class="white-area-content margin_left margin_repo separator page-right">
    <div class="containerr">
        <div class="row">
            <div class="col-md-6">
                <div class="db-header clearfix">
                    <div class="page-header-title"> <span class="glyphicon glyphicon-pencil"></span> Write an article</div>
                    <div class="db-header-extra form-inline">


                    </div>
                </div>

                <form method="POST" action="<?php echo  site_url("editarticle"); ?>" onsubmit="return validateForm()">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Title</label> -->
                                <div class="col-sm-12">
                                    <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title" value="<?php if (!empty($article->title)) {
                                                                                                                                            echo $article->title;
                                                                                                                                        } ?>">
                                    <input type="hidden" name="articleid" class="form-control" value="<?php if (!empty($article->ID)) {
                                                                                                            echo $article->ID;
                                                                                                        } ?>">
                                    <div class="error" id="error_title"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Select Catagory</label> -->
                                <div class="col-sm-12">
                                    <select name="cat" id="cat" class="form-control">
                                        <option value="">Select catagory</option>
                                        <?php

                                        foreach ($catagory as $catg) {
                                            $selected = "";
                                            if ($catg->cid == $article->catagory) {
                                                $selected = "selected";
                                            }
                                        ?>
                                            <option value="<?php echo $catg->cid; ?>" <?php echo $selected; ?>><?php echo $catg->c_name; ?></option>
                                        <?php
                                        } ?>
                                    </select>
                                    <div class="error" id="error_cat"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Select Subcatagory</label> -->
                                <div class="col-sm-12">
                                    <select name="subcat" id="subcat" class="form-control">
                                        <option value="">Select Subcatagory</option>
                                    </select>
                                    <input type="hidden" name="hidden_sub_cat_id" id="hidden_sub_cat_id" value="<?php echo $article->subcatagory  ?>">
                                    <div class="error" id="error_subcat"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Article Description</label> -->
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="description" id="description" placeholder="Content"><?php if (!empty($article->description)) {
                                                                                                                                    echo base64_decode($article->description);
                                                                                                                                } ?></textarea>
                                    <span><b>Note</b>: Articles content having words between 300-1000 are generally read more by users.</span>
                                    <div class="error" id="error_description"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Article Keywords</label> -->
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="keywords" id="keywords" value="<?php if (!empty($article->keywords)) {
                                                                                                                        echo $article->keywords;
                                                                                                                    } ?>" placeholder="Keywords">
                                    <div class="error" id="error_keywords"><?php echo $this->session->flashdata('error_key') ?></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label">Meta Description</label> -->
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="metadescription" maxlength="50" id="metadescription" placeholder="Meta description"><?php if (!empty($article->meta_description)) {
                                                                                                                                                                    echo $article->meta_description;
                                                                                                                                                                } ?></textarea>
                                    <span><b>Note</b>: In 50 words mention what this article is about and what all does it cover!</span>
                                    <div class="error" id="error_metadescription"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_775") ?></label> -->
                                <div class="col-sm-12">
                                    <select name="private" class="form-control" id="private">
                                        <option value="2" <?php if ($article->private == 0) {
                                                                echo "selected";
                                                            } ?>><?php echo lang("ctn_539") ?></option>
                                        <option value="1" <?php if ($article->private == 1) {
                                                                echo "selected";
                                                            } ?>><?php echo lang("ctn_633") ?></option>
                                    </select>
                                    <div class="error_private"></div>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10">

                            </div>
                        </div> -->
                            <div class="block_btn">
                                <input type="submit" class="btn btn-primary" value="Update Article" style="margin-top: 20px;">
                            </div>
                        </div>
                    </div>
                    <!-- <?php echo form_close() ?> -->
                </form>
            </div>
            <div class="col-md-6">
                <iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe'); ?>" width="100%" height="1300px" style="border:none;"></iframe>
            </div>
        </div>
    </div>
</div>


<!-- </div> -->


<script type="text/javascript">
    $(document).ready(function() {

        $('#cat').on("change", function() {
            var categoryId = $(this).find('option:selected').val();
            loadSubCategory(categoryId, "");
        });

        CKEDITOR.replace('description');

        var cat_id = $("#cat").val();
        var hidden_sub_cat_id = $("#hidden_sub_cat_id").val();
        if (cat_id != "") {
            loadSubCategory(cat_id, hidden_sub_cat_id);
        }

    });

    function loadSubCategory(categoryId = "", subCatId = "") {

        $.ajax({
            url: '<?php echo site_url('articles/subcatg') ?>',
            type: "GET",
            data: "category=" + categoryId,
            success: function(response) {
                $("#subcat").html(response);
                if (subCatId != "") {
                    $("#subcat").val(subCatId);
                }
            },
        });
    }

    function validateForm() {
        var title = $("#title").val();
        var cat = $("#cat").val();
        var subcat = $("#subcat").val();
        // var description = $("#description").val();
        var description = $("#cke_description iframe").contents().find("body").text();
        var metadescription = $("#metadescription").val();
        var keywords = $("#keywords").val();
        var private = $("#private").val();
        console.log(description);

        var error = false;

        $("#error_title").html("");
        $("#error_cat").html("");
        $("#error_subcat").html("");
        $("#error_description").html("");
        $("#error_keywords").html("");
        $("#error_private").html("");

        if (title.trim() == "") {
            error = true;
            $("#error_title").html("Title cannot be empty.");
        }
        if (cat.trim() == "") {
            error = true;
            $("#error_cat").html("Catagory cannot be empty.");
        }
        if (subcat.trim() == "") {
            error = true;
            $("#error_subcat").html("Subcatagory cannot be empty.");
        }
        if (description.trim() == "") {
            error = true;
            $("#error_description").html("Content cannot be empty.");
        } else {
            //exclude  start and end white-space
            description = description.replace(/(^\s*)|(\s*$)/gi, "");
            //convert 2 or more spaces to 1  
            description = description.replace(/[ ]{2,}/gi, " ");
            // exclude newline with a start spacing  
            description = description.replace(/\n /, "\n");
            var description_length = description.split(' ').length;
            if (description_length < 300) {
                error = true;
                $("#error_description").html("Content must have min. 300 words.");
            }
        }
        if (metadescription.trim() == "") {
            error = true;
            $("#error_metadescription").html("Meta Description cannot be empty.");
        }
        if (keywords.trim() == "") {
            error = true;
            $("#error_keywords").html("Keywords cannot be empty.");
        }
        if (private.trim() == "") {
            error = true;
            $("#error_private").html("Private cannot be empty.");
        }
        if (error == true) {
            return false;
        } else {
            return true;
        }
    }
</script>