
<div class="row">
        <div class="col-md-12">
        	<div class="white-area-content margin_left">
        		
        		<div class="db-header clearfix">
				    <div class="page-header-title"> <span class="glyphicon glyphicon-pencil"></span>Edit Section</div>
				   
				</div>

        <?php echo form_open_multipart(site_url("articles/edit_section_posts_pro"), array("class" => "form-horizontal")) ?>
            <div class="panel panel-default">
            <div class="panel-body">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Section title</label>
                <div class="col-sm-10">
                    <input type="text" name="title" class="form-control" value="<?php   echo $sections->title; ?>">
                       <input type="hidden" name="sectionid" class="form-control" value="<?php   echo $sections->ID; ?>">
                       <input type="hidden" name="articleid" class="form-control" value="<?php   echo $sections->articleid; ?>">
                </div>
                
                
            </div>
          
           
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Section Content</label>
                <div class="col-sm-10">
                    <textarea name="section_content" id="post" ><?php echo base64_decode($sections->body); ?></textarea>
                </div>
            </div>
          
           <!--
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Section <?php //echo lang("ctn_770") ?> (optional)</label>
                <div class="col-sm-10">
                    <input type="file" name="userfile" class="form-control">
                </div>
            </div>
		-->
                 <div class="block_btn">
            <input type="submit" class="btn btn-primary" value="Update">
                </div>
            </div>
            </div>
            <?php echo form_close() ?>

                


             




        	</div>
        </div>
    </div>
    <script type="text/javascript">
CKEDITOR.replace('post', { height: '300'});

</script>
