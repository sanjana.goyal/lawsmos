<div class="container">
<div class="well back-well">

	
	<div class="back-well-img">
                        <?php echo $this->common->get_user_display(array("username" => $aboutarticle->username, "avatar" => $aboutarticle->avatar, "online_timestamp" => $aboutarticle->online_timestamp)) ?>
                        <?php if(!empty($aboutarticle->image)) : ?>
								<img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $aboutarticle->image ?>" class="blog-post-thumb-main">
                        <?php endif; ?>
    </div>
    <div class="well-content">
						<h1><?php echo $aboutarticle->articletitle; ?></h1>
						<p><?php echo base64_decode($aboutarticle->description);  ?></p>
				<div class="section-content">	
					<?php  if(!empty($menu)): ?>
					<div class="section-tree">
						<h5>Contents: </h5>
						<?php  echo $menu; ?>
					</div>
					<?php  endif; ?>
					<div style="float:none;">
					<?php 
					 foreach($sections as $section):
					  ?>
						<div class="submne" id="<?php echo $section['sectionid']."section"; ?>">
							<h4><?php	echo $section['sectiontitle']; ?> </h4>
						</div>
							<p><?php echo base64_decode($section['body']);  ?></p>
						
					<?php 	endforeach;   ?>
						
					</div>
					</div>
		  
    </div>
</div>
</div>
