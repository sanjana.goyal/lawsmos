<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="row white-area-content page-right">
     

        		<?php foreach($posts->result() as $post) : ?>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
        		<div class="blog-post-wrapper blog-content-wrap clearfix">

        			<div class="blog-post-image-p">
                        <div class="blog-post-user-icon">
                            <?php echo $this->common->get_user_display(array("username" => $post->username, "avatar" => $post->avatar, "online_timestamp" => $post->online_timestamp)) ?>
                        </div>
                        <?php if(!empty($post->image)) : ?>
                        <figure>
            				<img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $post->image ?>" class="blog-post-thumb-main"></figure>
                        <?php endif; ?>
        			</div>
        			<div class="blog-post-info-p">
        				<a href="<?php echo site_url("blog/view/". $post->ID) ?>" class="b-content-title"><?php echo $post->title ?></a>
                        <?php
                           $summary = strip_tags($post->body);
                           $summary = substr($summary, 0, 180);
                        ?>
        				<p><?php echo $summary ?> ...</p>

                        <p><a href="<?php echo site_url("blog/view/". $post->ID) ?>" class="blog-read btn"><?php echo lang("ctn_414") ?></a></p>
        			</div>

        		</div>
            </div>
        	<?php endforeach; ?>


            <?php if($total_posts >= 10) : ?>
                <div class="white-area-content top-margin align-center">
                    <?php echo $this->pagination->create_links() ?>
                </div>
            <?php endif; ?>

     
    </div>
