 <?php use \application\Controllers\Pages;  ?> 
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content page-right bonus-wrapper">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-piggy-bank"></span> My Bonus Points <?php //echo lang("ctn_250") ?></div>
    <div class="db-header-extra">
		
		
		 <a href="<?php echo site_url("pages/updateq2apoints") ?>" class="btn btn-info btn-sm">Synchronise Q2A Points</a>
		
		 <a href="<?php echo site_url("funds/bonus_deduction_log") ?>" class="btn btn-danger btn-sm">Deduction History</a>
		
		 <a href="<?php echo site_url("funds/earnhistory") ?>" class="btn btn-success btn-sm"> Bonus Points Earned History </a>

</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li class="active"><?php echo lang("ctn_250") ?></li>
</ol>

 

<p>Bonus Points Left: <strong><?php echo number_format($this->user->info->bonus_points,2) ?></strong></p>
<p>Total Earned: <strong><?php echo number_format($this->user->info->total_points,2) ?></strong></p>

<hr>






</div>
