

<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<style>
.error{
	color:red;
}
.result-draft
{

height:1100px;
overflow:scroll;
}

/* width */
::-webkit-scrollbar {
  width: 20px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: black; 
 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #black; 
}

div#judgements-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

}
div#bareacts-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#article-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
ul.bareacts-list li, div#result li, .backend-text ul li, .result-draft li {
    list-style: none;
    margin-bottom: 14px;
    line-height: 28px;
    color: #666;
    margin-right: 30px;
    padding-left: 16px;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}

</style>
<div class="drafttt-page">
    <div class="container">
        <div class="row">
			<div class="col-md-6">
				<form method="post" action="<?php echo site_url('Case_editor/save'); ?>" target="_top">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
					<input type="hidden" name="caseid" value="<?php echo $case_info['id']; ?>">
					<div class="section-title">
						<h2>Case Builder</h2>
					</div>
					<div class="form-group ">
						<label for="title">Case Name</label>
						<input type="text" name="case_heading" class="form-control" id="casetitle" value="<?php echo $case_info['case_heading']; ?>" />
						<p class="title-error-info error"></p>
					</div>
					<div class="form-group">
						<label for="catagory">Category</label>
						<select name="catagory" id="catagory" class="form-control">
							<option value="">Select Catagory</option>
							<?php foreach($cats as $dtd){ 
								$selected = "";
								if($dtd->cid == $case_info['case_category']){
									$selected = "selected";
								}
							?>
								<option <?php echo $selected; ?> value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
							<?php } ?>
						</select>
						<p class="catagory-error-info error"></p>
					</div>
					<div class="form-group">
						<label for="title">Case Description</label>
						<textarea rows="4" cols="66" name="editor1" class="" id="casecontent" style="visibility: visible !important; "> <?php echo $case_info['case_body'] ?></textarea>
						<p class="description-error-info error"></p>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-danger" id="submitBtn">Save Case</button> 
					</div>
					<div id="content-container"></div>
				</form>
            </div>
			
			<div class="col-md-6">
				<div class="section-title">
				<h2>References</h2>
			</div>
			
			<iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe'); ?>"  width="100%" height="1300px" style="border:none;"></iframe>
			
           

  			</div>     
	
		</div>		
	</div>
</div>
                
<?php
	$tmp_url = str_replace("/social","",base_url());
?>				
              
			


 <script>
	
	$(document).ready(function(){
		
		jQuery('#submitBtn').click(function(e){
		 
			$(".title-error-info").html("");
			$(".description-error-info").html("");
			$(".catagory-error-info").html("");
			var casetitle = $('#casetitle').val();
			var casecontent = $('#casecontent').val();
			var catagory = $("#catagory").children("option:selected").val();

			var error = false;
			if(casetitle == ""){
				error = true;
				$(".title-error-info").html("Case name is required");
			}
			if(casecontent == ""){
				error = true;
				$(".description-error-info").html("Case description is required");
			}
			if(catagory == ""){
				error = true;
				$(".catagory-error-info").html("Category field is required");
			}
			if(error == true){
				return false;
			}
			return true;
        });

	});
 
 </script>

 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>


<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

<script>
$('.reference').load(function(){
	$('.reference').contents().find('.header-area-wrapper').hide();
    $('.reference').contents().find('.footer-area-wrapper').hide();
});
</script>


<script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>

