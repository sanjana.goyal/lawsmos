<style>

div#responsive-menu-links {
    display: none;
}

div#footer {
    display: none;
}

nav.navbar.navbar-inverse.navbar-header2 {
    display: none;
}


ul#ui-id-1 {
    width: 292px;
    border-right: 2px solid dodgerblue;
}



div#cke_1_contents {
    height: 1500px;
}

#main-content {
   
    border-right: none !important;
}

</style>
 
 
 
<form action="">
		
			<div class="form-group">
			
				
				<a href="javascript:void(0);" class="btn  profile-set-btn btn-xs" id="article">Judgments  ihuhughu</a>
				<a href="javascript:void(0);" class="btn  profile-set-btn btn-xs" id="whereacts">BareActs</a>
				<a href="javascript:void(0);" class="btn  profile-set-btn btn-xs" id="maxims">Maxims</a>
				<a href="javascript:void(0);" class="btn  profile-set-btn btn-xs" id="judgment">Articles</a>
			
			
			</div>
		
			<div class="">
			
				<p> Type here and press enter</p>
			</div>
		
			<div class="form-group">
			
				<input type="text" id="search-article"  placeholder="search judgments" class="typeahead form-control"/>
			</div>
			<div class="form-group">
			
				<input type="text" id="search-BareActs"  placeholder="search BareActs" class="typeahead form-control"/>
			</div>
			
			<div class="form-group">
			
				<input type="text" id="search-maxims"  placeholder="search maxims" class="typeahead form-control"/>
			</div>
			<div class="form-group">
			
				<input type="text" id="search-judgments" name="search_links" placeholder="search Articles" class="typeahead form-control"/>
			</div>
		</form>
 
 
 
 
 
 
 <iframe name="iframe_a" src="" id="iframe2ef" class="result-draft-soc" style="border:none;" scrolling="no"></iframe>
 
 <iframe name="iframe_b" src="" id="iframe3ef" class="result-draft-soc" style="border:none;" scrolling="no"></iframe>
 
 <iframe name="iframe_c" src="" id="iframe4ef"  class="result-draft-soc" style="border:none;" scrolling="no"></iframe>
 
 

<?php echo $this->session->flashdata('success'); ?>
 
 

<?php echo $this->session->flashdata('error'); ?>	
 
 <script>
	$(document).ready(function(){

		$("#search-article").show();
		$("#search-BareActs").hide();
		$("#search-maxims").hide();
		$("#search-judgments").hide();
		
		
		$("#article").click(function(){
			
		$("#search-article").show();
		$("#search-BareActs").hide();
		$("#search-maxims").hide();
		$("#search-judgments").hide();
			
			
			});
		$("#whereacts").click(function(){
			
		$("#search-article").hide();
		$("#search-BareActs").show();
		$("#search-maxims").hide();
		$("#search-judgments").hide();
			
			
			});
		$("#maxims").click(function(){
			
		$("#search-article").hide();
		$("#search-BareActs").hide();
		$("#search-maxims").show();
		$("#search-judgments").hide();
			
			
			});


	$("#judgment").click(function(){
			
		$("#search-article").hide();
		$("#search-BareActs").hide();
		$("#search-maxims").hide();
		$("#search-judgments").show();
			
			
			});

	});
 
 </script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
        
   
 
<script type="text/javascript">
   
   /*
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get('case_listings/ajaxpro', { query: query }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
        }
    });
   */ 
    $('#search-article').autocomplete({
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/ajaxpro",
	             data: {
	             		query : request.term
	             },
	             dataType: 'JSON',
	             success: function (msg) {
	                 response(msg);
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_a">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_a">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });
	  
	  
	  
	   $('#search-BareActs').autocomplete({
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/bareacts",
	             data: {
	             		query : request.term
	             },
	             dataType: 'JSON',
	             success: function (msg) {
	                 response(msg);
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_b">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_b">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });
	  
	  
	  
	    $('#search-maxims').autocomplete({
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/maxims",
	             data: {
	             		query : request.term
	             },
	             dataType: 'JSON',
	             success: function (msg) {
	                 response(msg);
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } 
	                else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });
	  
	  
	   $(document).ready(function(){
		  
		  
		
		  $("#editor1").css("height","600");
		  $("#iframe2ef").css("display","none");
		  
		  $("#iframe3ef").css("display","none");
		   $("#iframe4ef").css("display","none");
		  
		  
		  $("#search-article").on('change',function(){
			  
			  
			  $("#iframe2ef").css("display","block").css("height","800").css("scrolling","no").css("border","none");
			   $("#iframe3ef").css("display","none");
			   $("#iframe4ef").css("display","none");
			  });
			  
			$("#search-BareActs").on('change',function(){
			  
			  
			  $("#iframe3ef").css("display","block").css("height","800").css("scrolling","no").css("border","none");
			   $("#iframe2ef").css("display","none");
			   $("#iframe4ef").css("display","none");
			  });
		  
		  $("#search-maxims").on('change',function(){
			  
			  
			  $("#iframe4ef").css("display","block").css("height","800").css("scrolling","no").css("border","none");
			   $("#iframe2ef").css("display","none");
			   $("#iframe3ef").css("display","none");
			  });
		  
		  });
</script>
