<?php
    $tem_base_url = str_replace('/social','',base_url());
    $this->load->view('sidebar/sidebar.php'); 
?>
<style>
    .collapse-width{
        width: 100%;
        border-right: none;
    }.error{
        color: red;
    }.alert {
        position: absolute;
        right: 16px;
        top: 0;
        z-index: 9;
        width: 300px;
    }.save-opt{
        display: block !important;
    }button{
        border: none;
    }.case-builder-area .description-sec .content-sec .note{
        color: #fff;
        font-size: 14px;
    }.article-sec,.research-sec{
        display: none;
    }#category{
        margin-left: 10px;
    }.case-builder-area .search_data{
        margin-left: 0;
    }.case-builder-area .article_input.search_data{
        width: 100%;
    }.modal-header, .modal-content, .modal-dialog {
        background: #222222 !important;
        border: none;
    }.save-sec {
        background-color: #176bea;
        border-radius: 4px;
        color: #fff;
        padding: 10px 30px;
    }#save{
        margin-top: 15px;
    }.modal-header .close{
        color: #fff;
        opacity: 0.9;
    }.case-builder-area h5 {
        font-weight: 600;
    }.padding-left-cat{
        padding-left: 10px;
    }.sub-cat-li{
        display: none;
    }.scope-field label,.scope-btn{
        color: #fff;
    }.jist-content{
        width: 100%;
        display: none;
    }button:not(.disabled):before {
        content: '';
        position: absolute;
        right: 10px;
        top: 50%;
        margin-top: -11px;
        width: 20px;
        height: 20px;
        border: 4px solid;
        border-left-color: transparent;
        border-radius: 50%;
        filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
        opacity: 0;
        -moz-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
        -webkit-transition-duration: 0.5s;
        transition-duration: 0.5s;
        -moz-transition-property: opacity;
        -o-transition-property: opacity;
        -webkit-transition-property: opacity;
        transition-property: opacity;
        -moz-animation-duration: 1s;
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -moz-animation-iteration-count: infinite;
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
        -moz-animation-name: rotate;
        -webkit-animation-name: rotate;
        animation-name: rotate;
        -moz-animation-timing-function: linear;
        -webkit-animation-timing-function: linear;
        animation-timing-function: linear;
    }button:not(.disabled).loading:after {
        -moz-transition-delay: 0s;
        -o-transition-delay: 0s;
        -webkit-transition-delay: 0s;
        transition-delay: 0s;
        width: 20px;
    }button:not(.disabled).loading:before {
    -moz-transition-delay: 0.5s;
    -o-transition-delay: 0.5s;
    -webkit-transition-delay: 0.5s;
    transition-delay: 0.5s;
    -moz-transition-duration: 1s;
    -o-transition-duration: 1s;
    -webkit-transition-duration: 1s;
    transition-duration: 1s;
    filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
    opacity: 1;
}button:not(.disabled).loading:not(.expand) {
    text-align: left;
}
button:not(.disabled).loading {
    pointer-events: none;
    cursor: not-allowed;
}
    button:not(.disabled):after {
        content: '';
        display: inline-block;
        height: 100%;
        width: 0px;
        -moz-transition-delay: 0.5s;
        -o-transition-delay: 0.5s;
        -webkit-transition-delay: 0.5s;
        transition-delay: 0.5s;
        -moz-transition-duration: 0.75s;
        -o-transition-duration: 0.75s;
        -webkit-transition-duration: 0.75s;
        transition-duration: 0.75s;
        -moz-transition-property: width;
        -o-transition-property: width;
        -webkit-transition-property: width;
        transition-property: width;
    }
    @keyframes rotate {
        from {
            transform: rotate(0deg);
        } to {
            transform: rotate(360deg);
        }
    }.position-relative{
        position: relative;
    }.nopadding{
        padding: 0 !important;
    }.scope-label{
        line-height: 45px;
    }.scope-container{
        margin: 30px 0;        
    }.scope-field{
        margin-top: 10px;        
    }.scope-btn,.scope-btn:hover,.scope-btn:focus {
        color: #176bea;
        text-decoration: underline;
    }.add_jist,.add_jist:hover,.add_jist:focus,.remove_jist,.remove_jist:hover,.remove_jist:focus{
        color: #fff;
        text-decoration: underline;
        margin: 20px 0;
    }
    .reference-list-ul {
      background-color: #333;
      overflow: hidden;
    }

    .reference-list-ul li.reference-list {
      float: left;
    }

    .reference-list-ul li.reference-list a.reference-links {
      display: block;
      color: white;
      text-align: center;
      padding: 10px 14px;
      text-decoration: none;
    }

    .reference-list-ul li.reference-list a.reference-links:hover:not(.active) {
      background-color: #111;
    }

    .reference-list-ul a.reference-links.active {
      background-color: #176bea;
    }section.performance-sec ul {
         padding:  0; 
    }.mt-0{
        margin-top: 0;
    }a.hide-sec {
        padding: 5px 5px;
        background-color: #000;
        border: 1px solid #5c5b5b;
        color: #fff;
        font-size: 12px;
        position: absolute;
        right: 0;
        top: 0;
    }.hide-ref-sec {
        position: relative;
        display: list-item;
        clear: both;
        float: none;
        margin-bottom: 20px;
    }.case-builder-area .dropdown {
        margin-bottom: 70px;
        text-align: right;
        clear: both;
    }.case-builder-area .dropdown-menu>li>a{
        color: #fff;
    }.case-builder-area .dropdown-menu{
        background-color: #302f2f;
    }
</style>
<div class="content-right-area case-builder-area">  
    <div class="area-bar-top">  
        <div class="col-md-12 hide-ref-sec">
            <a href="#" class="hide-sec">
                <span>Hide reference</span>
                <i class="fa fa-angle-double-right"></i>
            </a>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <ul class="left-top-editor list-unstyled">
                <li>
                    <a href="#" class="active editor-links" id="case" data-target="case-builder-sec"><span><img src="<?=$tem_base_url.'/assets/images/case-icon.png'?>" alt=""></span>Case
                    Builder</a>
                </li>
                <li>
                    <a href="#" class="editor-links" id="article" data-target="article-sec"><span><img src="<?=$tem_base_url.'/assets/images/articles-icon.png'?>" alt=""></span>Write an Articles</a>
                </li>
                <li>
                    <a href="#" class="editor-links" id="research" data-target="research-sec"><span><img src="<?=$tem_base_url.'/assets/images/research-icon.png'?>" alt=""></span> My
                    Research</a>
                </li>
                <li class="padding-left-cat">
                    <?php //var_dump($research_info); ?>

                    <?php                        
                        $casecat = (isset($case_info)) ? $case_info['case_category'] : '';
                        $articlecat = (isset($article_info)) ? $article_info['catagory'] : '';
                        $researchcat = (isset($research_info)) ? $research_info['catagory'] : '';
                    ?>
                    <input type="hidden" name="cat" id="cat" value="<?php echo (($casecat) ? $casecat : (($articlecat) ? $articlecat : (($researchcat) ? $researchcat : ""))); ?>">
                    <select name="catagory" id="catagory">
                        <option value="">Select Catagory</option>
                        <?php foreach($cats as $dtd){ 
                            $case_cat = (isset($case_info)) ? ($case_info['case_category']==$dtd->cid ? "selected" : "") : '';
                            $article_cat = (isset($article_info)) ? ($article_info['catagory']==$dtd->cid ? "selected" : "") : '';
                            $research_cat = (isset($research_info)) ? ($research_info['catagory']==$dtd->cid ? "selected" : "") : '';
                        ?>
                        <option value="<?php echo $dtd->cid; ?>" <?php echo (($case_cat) ? $case_cat : (($article_cat) ? $article_cat : (($research_cat) ? $research_cat : ""))); ?> ><?php echo $dtd->c_name; ?></option>
                        <?php } ?>
                    </select>
                    <p class="catagory-error-info error"></p>
                </li>
                <li class="padding-left-cat sub-cat-li">
                    <?php                        
                        // $case_subcat = (isset($case_info)) ? $case_info['case_category'] : '';
                        $article_subcat = (isset($article_info)) ? $article_info['subcatagory'] : '';
                        $research_subcat = (isset($research_info)) ? $research_info['subcat'] : '';
                    ?>
                    <input type="hidden" name="sub_cat" id="sub_cat" value="<?php echo (($article_subcat) ? $article_subcat : (($research_subcat) ? $research_subcat : "")); ?>">
                    <select name="subcategory" id="subcategory">
                        <option value="">Select Subcatagory</option>
                    </select>
                    <p class="catagory-error-info error"></p>
                </li>
            </ul>
        </div>
        <div class="col-md-6">   <!--                         
            <h5 class="mt-0">References</h5> -->
            <ul class="list-unstyled reference-list-ul">                        
                <li class="reference-list"><a href="#" class="active reference-links" data-placeholder="Search performa" data-href="performa-filter" data-search="search_performas">Performas</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search judgments" data-href="judgements-filter" data-search="search_judgements">Judgments</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search Supreme judgments" data-href="supreme-judgements-filter" data-search="search_supreme_judgements">Supreme Judgments</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search Bare Acts" data-href="bareacts-filter" data-search="search_bareacts">Bare Acts</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search legal terms" data-href="legal-terms-filter" data-search="set_alphabet_legal_term">Legal Terms</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search Articles" data-href="article-filter" data-search="search_articles">Articles</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search Maxims" data-href="maxims-filter" data-search="search_maxims">Maxims</a></li>
                <li class="reference-list"><a href="#" class="reference-links" data-placeholder="Search Research" data-href="research-filter" data-search="search_research">Research</a></li>
            </ul>
            <div id="content-container"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <section class="draft-sec">
        <div class="description-sec case-builder-sec">
            <div class="col-md-6 des-border-right">
                <input type="hidden" id="caseid" value="<?php echo isset($_GET['caseid']) ? $_GET['caseid'] : ''; ?>">
                <input type="hidden" id="caseurl" value="<?php echo site_url("case_editor/savecase_new") ?>">
                <h5>Following is the sample draft for reference which may further be modified as per requirement</h5>
                <div class="content-sec">
                    <p class="description-error-info error"></p>
                    <textarea class="form-control" name="editor1" id="description" placeholder="Content"><?php echo (isset($case_info)) ? base64_decode($case_info['case_body'])  : "" ; ?></textarea>
                    <p class="note"><b>Note</b>: Case Builder content having words between 300-1000 are generally read more by users.</p>
                </div>
                
                <div class="dropdown">
                    <button class="dropdown-toggle save-sec" type="button" data-toggle="dropdown">Save<span class="caret"></span></button><ul class="dropdown-menu"><li class="save-opt"><a href="#" data-target="#savemodal" data-toggle="modal" class="saveopt">Save as draft</a></li><li class="save-opt"><a href="#" data-target="#savemodal" data-toggle="modal" class="saveopt">Publish</a></li></ul>
                </div>
            </div>
            <div class="col-md-6 iframe-sec">
                <div class="right-search-performa">
                    <iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe?performa-filter&test'); ?>"  width="100%" style="border:none;min-height:600px"></iframe>                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="description-sec article-sec">
            <div class="col-md-6 des-border-right">
                <input type="hidden" id="articleid" value="<?php echo isset($_GET['articleid']) ? $_GET['articleid'] : ''; ?>">
                <input type="hidden" id="articleurl" value="<?php echo site_url("articles/save_article") ?>">
                <h5>Following is the sample draft for reference which may further be modified as per requirement</h5>
                <div class="content-sec">
                    <div>
                        <p class="articledescription-error-info error"></p>
                        <textarea class="form-control" name="editor2" id="articledescription" placeholder="Content"><?php echo (isset($article_info)) ? base64_decode($article_info['description'])  : "" ; ?></textarea>
                        <p class="note"><b>Note</b>: Articles content having words between 300-1000 are generally read more by users.</p>
                    </div>
                    <div>
                        <h5>Keywords</h5>
                        <input type="text" class="search_data article_input" name="keywords" id="keywords" placeholder="Keywords" value="<?php echo (isset($article_info)) ? $article_info['metakeywords']  : "" ; ?>">
                        <p class="keywords-error-info error"></p>
                    </div>
                    <div>
                        <h5>Meta Description</h5>
                        <textarea class="search_data article_input" name="meta-description" id="meta-description" placeholder="enter your meta description"><?php echo (isset($article_info)) ? $article_info['meta_description']  : "" ; ?></textarea>
                        <div class="metadescription-error-info error"></div>
                        <p class="note"><b>Note</b>: In 50 words mention what this article is about and what all does it cover.</p>
                    </div>
                </div>
                <div class="dropdown"></div>
            </div>
            <div class="col-md-6 iframe-sec">
                <div class="right-search-performa">
                    <h5>References</h5>
                    <iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe?performa-filter&test'); ?>"  width="100%" style="border:none;min-height:600px"></iframe>                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="description-sec research-sec">
            <div class="col-md-6 des-border-right">
                <input type="hidden" id="researchid" value="<?php echo isset($_GET['researchid']) ? $_GET['researchid'] : '' ?>">
                <input type="hidden" id="researchurl" value="<?=base_url()?>Case_editor/saveresearch">
                <h5>Following is the sample draft for reference which may further be modified as per requirement</h5>
                <div class="content-sec">
                    <div>
                        <p class="researchdescription-error-info error"></p>
                        <textarea class="form-control" name="editor2" id="researchdescription" placeholder="Content"><?php echo (isset($research_info)) ? base64_decode($research_info['research_body'])  : "" ; ?></textarea>
                        <!-- <p class="note"><b>Note</b>: Research content having words between 300-1000 are generally read more by users.</p> -->
                    </div>
                    <div class="scope-container">
                        <?php
                            echo $research_info['scope'];
                            if ($research_info['scope']) {
                                $r_count=1;
                                foreach(json_decode($research_info['scope']) as $rkey => $r_scope){
                        ?>                        
                        <div class="scope-field">
                            <label class="col-md-1 nopadding scope-label">Scope <?=$r_count?></label>
                            <div class="col-md-9">
                                <input type="text" name="scope" class="scope article_input search_data" value="<?=$r_scope?>">
                            </div>
                            <a href="#" class="<?php echo ($r_count==1) ? "add_btn" : "remove_btn"; ?> scope-btn col-md-2 nopadding scope-label text-right">
                                <?php if ($r_count==1) { ?>
                                    <i class="fa fa-plus"></i> Add
                                <?php }else{ ?>
                                    <i class="fa fa-minus"></i> Remove
                                <?php } ?>
                            </a>
                            <div class="clearfix"></div>
                        </div>
                        <?php       $r_count++;
                                }
                            }else{
                        ?>
                        <div class="scope-field">
                            <label class="col-md-1 nopadding scope-label">Scope 1</label>
                            <div class="col-md-9">
                                <input type="text" name="scope" class="scope article_input search_data" value="">
                            </div>
                            <a href="#" class="add_btn scope-btn col-md-2 nopadding scope-label text-right"><i class="fa fa-plus"></i> Add</a>
                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                    </div>
                    <div>
                        <h5>References</h5>
                        <p class="researchdescription2-error-info error"></p>
                        <textarea class="form-control" name="editor2" id="researchdescription2" placeholder="Content"><?php echo (isset($research_info)) ? base64_decode($research_info['research_body2'])  : "" ; ?></textarea>
                    </div>
                    <div>
                        <a href="#" class="add_jist jist-btn col-md-2">
                            <?php if (isset($research_info) && $research_info['research_jist']) { ?>
                                <i class="fa fa-minus"></i> Remove Jist
                            <?php }else{ ?>
                                <i class="fa fa-plus"></i> Add Jist
                            <?php } ?>
                        </a>
                        <textarea rows="4" class="jist-content" <?php echo (isset($research_info)) ? ($research_info['research_jist'] ? "style='display:block'" : "")  : "" ; ?>><?php echo (isset($research_info)) ? $research_info['research_jist']  : "" ; ?></textarea>
                    </div>
                    <div class="dropdown"></div>
                </div>
            </div>
            <div class="col-md-6 iframe-sec">
                <div class="right-search-performa">
                    <h5>References</h5>
                    <iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe?performa-filter&test'); ?>"  width="100%" style="border:none;min-height:600px"></iframe>                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <div class="modal fade dark-modal-side in" id="savemodal" role="dialog" style="overflow: scroll;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h5 class="modal-title">Give a Title to your Case</h5>
                </div>
                <div class="modal-body">
                    <input type="text" name="title" id="title" value="<?php echo ((isset($case_info)) ? $case_info['case_heading']  : ((isset($article_info)) ? $article_info['title']  : ((isset($research_info)) ? $research_info['reasearch_heading']  : ""))) ; ?>" placeholder="Enter the title here" class="search_data article_input">
                    <div class="title-error-info"></div>
                    <input type="hidden" name="save_as" id="save_as" value="Save as draft">
                    <div class="text-right">
                        <button type="button" name="save" id="save" data-btn="case-builder-sec" class="save-sec position-relative"> Save as draft </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="csrf_token_name" value="<?=$this->security->get_csrf_token_name()?>">
<input type="hidden" id="csrf_hash" value="<?=$this->security->get_csrf_hash()?>">
<script type="text/javascript" src="<?=$tem_base_url?>/assets/front/js/social-custom.js"></script>