
<style>
	.category-name{
		height: 50px;
	}
</style>
<?php 
	$userid=$this->user->info->ID;
	$sql ="SELECT * FROM case_editor where loggged_user_id=$userid";
	$query = $this->db->query($sql);
?>

<?php $this->load->view('sidebar/sidebar.php'); ?>

<div class="row page-right view-height">
	<div class="col-xs-12 case-list-outer">
	
	
 		<h3>My Cases</h3>
 		
    <div class="rebtn">
        <a href="<?php echo site_url('case_listings/create');  ?>" class="btn profile-set-btn builder-button create_cases_new">Create New Case</a>
        <input type="hidden" name="page_name" id="page_name" value="case">

    </div>
	
	<div class="diff-law-categories">
		<div class="row">
		<?php
			foreach($categories as $cat){
			?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="differ-category">
						<a href="<?php echo base_url("case_listings/mycases/$cat->cid"); ?>">
							<figure >
								<span class="icon-folder"></span>
							</figure>
						</a>	
						<figcaption class="category-name">
							<h2><?php echo $cat->c_name ?></h2>
						</figcaption>
					</div>
				</div>
			<?php
			}
		?>
		</div>
	</div>
 
 </div>

 </div>
 
 
 <?php 
 
 
 if(isset($_POST['editor'])){
	echo $_POST['editor'];
 }
 ?>


