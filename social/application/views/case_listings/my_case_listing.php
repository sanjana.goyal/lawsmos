<?php $this->load->view('sidebar/sidebar.php'); ?>

<div class="row page-right view-height">
	<div class="col-xs-12 case-list-outer">

        <?php echo $this->session->flashdata('success'); ?>
 		<?php echo $this->session->flashdata('error'); ?>	
	
 		<h3>My Cases : <?php echo $category_info['c_name']; ?></h3>
        <?php if (count($cases) > 0) { ?>
         <table class="table case_table table-striped table-hover table-bordered dataTable no-footer" role="grid" aria-describedby="friends-table_info">
            
			<tr>
				<th>Case Heading</th>
				<th>Last Updated</th>
				<th colspan="2">Action</th>
			</tr>
                <?php 
                    

                        foreach ($cases as $row) {
                    
                        $heading=$row->case_heading;
                        $body=html_entity_decode($row->case_body);
                        $last_update=$row->last_updated;
                        $row_id=$row->id;
                ?>
                    <tr>
                        <td><a href="<?php echo site_url(); ?>case_editor?id=<?php echo $row_id; ?>"><?php echo $heading; ?></a></td>
                        <td><a href="#"><?php echo $last_update; ?></a></td>
                        <td><a href="<?php echo site_url(); ?>case_editor?id=<?php echo $row_id; ?>">Edit Case</a></td>
                        <td><a href="<?php echo site_url(); ?>case_listings/delete/<?php echo $row_id; ?>">Remove</a></td>
                    </tr>
            <?php
                    }
               
                
        ?>
        
        </table>
        <?php
            }
            else{
                echo "<h3 style='text-align:center;'>No Case Available</h3>";
            }
        ?>