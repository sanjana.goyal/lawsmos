<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<style>
.error{
	color:red;
}
.result-draft
{

height:500px;
overflow:auto;
}

/ width /
::-webkit-scrollbar {
  width: 10px !important;
}

/ Track /
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/ Handle /
::-webkit-scrollbar-thumb {
  background: black; 
 
}

/ Handle on hover /
::-webkit-scrollbar-thumb:hover {
  background: black; 
}

div#judgements-filter select,div#supreme-judgements-filter select {

    width: 22%;
    display: inline-block;
    margin: 4px 4px;

}
div#bareacts-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#article-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#performa-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
ul.bareacts-list li, div#result li, .backend-text ul li, .result-draft li {
    list-style: none;
    margin-bottom: 14px;
    line-height: 28px;
    color: #666;
    margin-right: 30px;
    padding-left: 16px;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}
.navbar.navbar-inverse.navbar-header2 {
    display: none;
}.result-draft{
	display: none;
}
	.result-draft  li a{
		position:relative;
	}
	.result-draft  li a:before{
		    position: absolute;
    content: "\f111";
    left: -13px;
    top: -5px;
    font-family: fontawesome;
    font-size: 8px;
	}
#main-content {
    padding-top: 20px;
}
.btn {
    padding: 6px 12px !important;
}
</style>

<form id="draftcasess">
				<div class="form-group reference-tabs">
					<a href="javascript:void(0);" class="btn btn-success  btn-xs" onclick="search_performas();" id="performas">Performas</a>
					<a href="javascript:void(0);" class="btn btn-danger btn-xs" onclick="search_judgements();" id="judgements">Judgments</a>
					<a href="javascript:void(0);" class="btn btn-danger btn-xs" onclick="search_supreme_judgements();" id="supreme_judgements">Supreme Judgments</a>
					<a href="javascript:void(0);" class="btn btn-primary  btn-xs" onclick="search_bareacts();" id="bareacts">Bare Acts</a>
					<a href="javascript:void(0);" class="btn btn-info  btn-xs" id="legalterms">Legal Terms</a>
					<a href="javascript:void(0);" class="btn btn-success  btn-xs" onclick="search_articles();" id="articles">Articles</a>
					<a href="javascript:void(0);" class="btn btn-success  btn-xs" id="maxims">Maxims</a>
				</div>
                <div>
                    <p><b>Selected Performa</b> : <span id="selected_performa"></span></p> 
                    <p><b>Category name</b> :<span id="selected_category"></span> </p>
                </div>
				<div class="form-group" id="judgements-filter">
					<!-- <p>Search Judgements </p> -->
					<select name="courtname" id="courtname" class="form-control" onchange="search_judgements()">
						<option value="">Select Court</option>
						<?php foreach($courts as $dtd){ ?>
						<option value="<?php echo $dtd->state; ?>"><?php echo $dtd->state; ?></option>
						<?php } ?>
					</select>
					<select name="yearcity" id="year" class="form-control" onchange="search_judgements()">
						<option value="">Select Year</option>
						<?php foreach($year as $dtd){ ?>
						<option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
						<?php } ?>
					</select>
					<select name="catagory" id="catagory" class="form-control" onchange="search_judgements()">
						<option value="">Select Catagory</option>
						<?php foreach($cats as $dtd){ 
                        ?>
						<option  value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
						<?php } ?>
					</select>
					<select name="sort_by" id="sort_by" class="form-control" onchange="search_judgements()">
						<option value="">Sort By</option>	
						<option value="asc">Ascending</option>
						<option value="desc">Descending</option>	
						<option value="recent">Recent</option>
					</select>
						
				</div>				
				<div class="form-group" id="supreme-judgements-filter">
					<!-- <p>Search Judgements </p> -->
					<select name="yearcity" id="supreme_year" class="form-control" onchange="search_supreme_judgements()">
						<option value="">Select Year</option>
						<?php foreach($supremeyear as $dtd){ ?>
						<option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
						<?php } ?>
					</select>
					<select name="sort_by" id="supreme_sort_by" class="form-control" onchange="search_supreme_judgements()">
						<option value="">Sort By</option>	
						<option value="asc">Ascending</option>
						<option value="desc">Descending</option>	
						<option value="recent">Recent</option>
					</select>
						
				</div>
				<div class="form-group" id="bareacts-filter">
					<!-- <p>Search Bareacts</p> -->
					<select name="sort_by_bareacts" id="sort_by_bareacts" class="form-control" onchange="search_bareacts()">
						<option value="">Sort By</option>	
						<option value="asc">Ascending</option>
						<option value="desc">Descending</option>	
						<option value="recent">Recent</option>
					</select>
				</div>
				<div class="form-group" id="legal-terms-filter">
					<!-- <p>Search Legal Terms</p> -->
					<?php  
						$character = range('A', 'Z');  
						echo '<ul class="pagination">';  
						echo "<li><a href='javascript:void(0);' 
						id='selected_alphabet_all'
						class='selected_alphabet' onclick=set_alphabet_legal_term('')>All</a></li>";
						foreach($character as $alphabet)  
						{  
							// echo "<li><a href=".url('searchterms/'.$alphabet).">$alphabet</a></li>"; 
							echo "<li><a href='javascript:void(0);' 
							id='selected_alphabet_".$alphabet."' class='selected_alphabet' onclick=set_alphabet_legal_term('".$alphabet."')>$alphabet</a></li>";
						}  
						
						echo "</ul>"; 
					?>
					<input type="hidden" name="alphabet_value" id="alphabet_value" value="" >
				</div>
				<div class="form-group" id="article-filter">
					<!-- <p>Search Articles</p> -->
					<select  name="catagory_article" id="catagory_article" class="form-control" onchange="search_articles()">
						<option value="">Select Catagory</option>
						<?php foreach($cats as $dtd){ 
						?>
						<option  value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
						<?php } ?>
					</select>
					<select name="sub_catagory_article" id="sub_catagory_article" class="form-control" onchange="search_articles()">
						<option value="">Select Subcatagory</option>
					<select>
					<select name="sort_by_article" id="sort_by_article" class="form-control" onchange="search_articles()">
						<option value="">Sort By</option>	
						<!-- <option value="asc">Ascending</option>
						<option value="desc">Descending</option>	 -->
						<option value="recent">Recent</option>
						<option value="popular">Popular</option>
						<option value="popular">Most Viewed</option>
					</select>
				</div>
				<div class="form-group" id="maxims-filter">
					<!-- <p>Search Maxims</p> -->
					<?php  
						$character = range('A', 'Z');  
						echo '<ul class="pagination">';  
						echo "<li><a href='javascript:void(0);' id='selected_alphabet_maxims_all'
						class='selected_alphabet_maxims' onclick=set_alphabet_maxims('')>All</a></li>";
						foreach($character as $alphabet)  
						{  
							// echo "<li><a href=".url('searchterms/'.$alphabet).">$alphabet</a></li>"; 
							echo "<li><a href='javascript:void(0);' id='selected_alphabet_maxims_".$alphabet."' class='selected_alphabet_maxims' onclick=set_alphabet_maxims('".$alphabet."')>$alphabet</a></li>";
							
						}  
						
						echo "</ul>"; 
					?> 
					<!--<div>Showing result for: <span id="selected_alphabet_maxims">All</span></div>-->
					<input type="hidden" name="alphabet_value_maxims" id="alphabet_value_maxims" value="" >
				</div>
                <div class="form-group" id="performa-filter">
					<select  name="catagory_performa" id="catagory_performa" class="form-control" onchange="search_performas()">
						<option value="">Select Catagory</option>
						<?php foreach($cats as $dtd){ 
						?>
						<option  value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
						<?php } ?>
					</select>
                </div>
                
						
						
				<div class="form-group">
					<input type="text" id="search-judgements"  placeholder="Search judgments" class="form-control"/>
				</div>
				<div class="form-group">
					<input type="text" id="search-supreme-judgements"  placeholder="Search Supreme judgments" class="form-control"/>
				</div>
				<div class="form-group">
					<input type="text" id="search-bareacts"  placeholder="Search Bare Acts" class="form-control"/>
				</div>
				
				<div class="form-group">
					<input type="text" id="search-articles" name="Search_links" placeholder="Search Articles" class="form-control"/>
				</div>
				<div class="form-group" >
					<input type="text" class="form-controller form-control" id="search" name="search" placeholder="Search legal terms">
				</div>
				<div class="form-group" >
					<input type="text" class="form-controller form-control" id="searchmaxims" name="search" placeholder="Search Maxims">
				</div>
                <div class="form-group" >
					<input type="text" class="form-controller form-control" id="search-performa" name="search-performa" placeholder="Search performa">
				</div>
				
            </form>

            <div id="notification-loaders">
				<div id="loading_spinner_notification" style="text-align:center;">
					<span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
				</div>
			</div>
			<div name="iframe_a" id="iframe2ef" class="result-draft"></div>	
			<div name="iframe_a" id="iframe8ef" class="result-draft"></div>	
			<div name="iframe_b" id="iframe3ef" class="result-draft"></div>	
			<div name="iframe_c" id="iframe4ef" class="result-draft">
				<div id="resultt"></div>
				<div class="list">
					<?php foreach($maxims as $maxims){ ?>							
						<div class="list_blockk">
							<h4><?php echo $maxims->term?></h4>
							<p><?php echo strip_tags($maxims->definition) ;?></p>
						</div>
					<?php } ?>
				</div>
			</div>
			<div name="iframe_e" id="iframe6ef" class="result-draft">
				<div id="resulttss"></div>
				<div class="list">
					<?php foreach($newmaxims as $maxims){ ?>	
					<div class="list_blockk">
						<h4><?php echo $maxims->term ?></h4>
						<p><?php echo strip_tags($maxims->def) ?></p>
					</div>
					<?php } ?>
				</div>
			</div>
			<div name="iframe_d" id="iframe5ef" class="result-draft"></div>
            <div name="iframe_f" id="iframe7ef" class="result-draft"></div>

<?php
	$tmp_url = str_replace("/social","",base_url());
?>				
              
		

<script>
	function search_judgements(){

		$(".btn").removeClass("active-reference-tabs");
		$("#judgements").addClass("active-reference-tabs");

		var catagory = $("#catagory").children("option:selected").val();
		var year = $("#year").children("option:selected").val();
		var courtname = $("#courtname").children("option:selected").val();
		var sort_by = $("#sort_by").children("option:selected").val();
		var value = $("#search-judgements").val();
		
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchhighcourt"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'court': courtname,'year':year,'catagory':catagory,'search':value,'sort_by':sort_by},
			success:function(data){
				if(data!="" || data!=null){
					$("#iframe2ef").html(data);
					$('#iframe2ef').show();
				}else
				{
					$("#iframe2ef").html("No records found...");
					$('#iframe2ef').show();
				}
			}
		});
	}
	function search_supreme_judgements(){

		$(".btn").removeClass("active-reference-tabs");
		$("#supreme_judgements").addClass("active-reference-tabs");

		var year = $("#supreme_year").children("option:selected").val();
		var sort_by = $("#supreme_sort_by").children("option:selected").val();
		var value = $("#search-supreme-judgements").val();
		// alert(sort_by);
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchsupremecourt"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'year':year,'search':value,'sort_by':sort_by},
			success:function(data){
				if(data!="" || data!=null){
					$("#iframe8ef").html(data);
					$('#iframe8ef').show();
				}else
				{
					$("#iframe8ef").html("No records found...");
					$('#iframe8ef').show();
				}
			}
		});
	}
	function search_bareacts(){

		$(".btn").removeClass("active-reference-tabs");
		$("#bareacts").addClass("active-reference-tabs");

		var value = $("#search-bareacts").val();
		var sort_by_bareacts = $("#sort_by_bareacts").children("option:selected").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchacts"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,'sort_by':sort_by_bareacts},
			success:function(data){
				$("#iframe3ef").html(data);
				$('#iframe3ef').show();
			}
		});
	}
	function set_alphabet_legal_term(alphabet_value){
		$("#search").val('');
		$("#alphabet_value").val(alphabet_value);
		$(".selected_alphabet").css("background-color", "");
		if(alphabet_value != ""){
			$("#selected_alphabet_"+alphabet_value).css("background-color", "gray");
		}else{
			$("#selected_alphabet_all"+alphabet_value).css("background-color", "gray");
		}
		search_legal_terms(alphabet_value)
	}
	function search_legal_terms(alphabet_value){
		var value = $("#search").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchmaxims"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"alphabet":alphabet_value},
			success:function(data){
				$("#resultt").html(data);
				$('#resultt').show();
				$('.list').hide();

			}
		});
	}
	function search_articles(){

		$(".btn").removeClass("active-reference-tabs");
		$("#articles").addClass("active-reference-tabs");

		var catagory_article = $("#catagory_article").children("option:selected").val();
		var sub_catagory_article = $("#sub_catagory_article").children("option:selected").val();
		var sort_by_article = $("#sort_by_article").children("option:selected").val();

		
		var value = $("#search-articles").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searcharticles"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"category": catagory_article,"sub_category":sub_catagory_article,"sort_by":sort_by_article},
			success:function(data){
				$("#iframe5ef").html(data);
				$('#iframe5ef').show();
			}
		});
	}
	function set_alphabet_maxims(alphabet_value_maxims){
		$("#searchmaxims").val('');
		$("#alphabet_value_maxims").val(alphabet_value_maxims);
		$(".selected_alphabet_maxims").css("background-color", "");
		if(alphabet_value_maxims != ""){
			$("#selected_alphabet_maxims_"+alphabet_value_maxims).css("background-color", "gray");
		}else{
			$("#selected_alphabet_maxims_all"+alphabet_value_maxims).css("background-color", "gray");
		}
		search_maxims(alphabet_value_maxims)
	}
	function search_maxims(alphabet_value_maxims){
		var value = $("#searchmaxims").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchmax"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"alphabet":alphabet_value_maxims},
			success:function(data){
				$("#resulttss").html(data);
				$('#resulttss').show();
				$('.list').hide();
			}
		});
	}
    function search_performas(){

		$(".btn").removeClass("active-reference-tabs");
		$("#performas").addClass("active-reference-tabs");

		var catagory_performa = $("#catagory_performa").children("option:selected").val();

        var value = $("#search-performa").val();
        $.ajax({
            type: 'get',
            async: true,
            url: "<?php echo base_url().'/case_listings/getPerformas' ?>", 
            beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
            data:{'search': value,"category": catagory_performa},
            success:function(data){
				$("#iframe7ef").html(data);
				$('#iframe7ef').show();
			}
        });
    }
    function select_performa(id){
        var performa_name = event.target.innerText;
        if(id){
            $.ajax({
                type: 'get',
                async: true,
                url: "<?php echo base_url().'/case_listings/getCategoryName' ?>", 
                data:{'category_id': id},
                success:function(data){
                    $("#selected_performa").html(performa_name);
                    $("#selected_category").html(data);
                    $('#catagory option[value="'+id+'"]').attr('selected', true)
                    $('#catagory_article option[value="'+id+'"]').attr('selected', true)
                }
            });
        }
    }
</script>

 <script>
	
	$(document).ready(function(){


		
			$("#search-judgements").hide();
			$("#search-supreme-judgements").hide();
		    $("#search-bareacts").hide();
			$("#search-articles").hide();
			$("#search").hide();
			$("#searchmaxims").hide();
            $("#search-performa").show();
			
			$("#judgements-filter").hide();
			$("#supreme-judgements-filter").hide();
			$("#bareacts-filter").hide();
			$("#legal-terms-filter").hide();
			$("#article-filter").hide();
			$("#maxims-filter").hide();
            $("#performa-filter").show();
            

			$("#iframe2ef").hide();
			$("#iframe3ef").hide();
			$("#iframe4ef").hide();
			$("#iframe5ef").hide();
			$("#iframe6ef").hide();
			
			var catagory = $("#catagory").children("option:selected").val();
			var year = $("#year").children("option:selected").val();
			var courtname = $("#courtname").children("option:selected").val();
			var sort_by = $("#sort_by").children("option:selected").val();

			search_performas();
			
		$("select#catagory").change(function(){
			catagory=$(this).children("option:selected").val();
		
		});			
		$("select#year").change(function(){
			year=$(this).children("option:selected").val();
		});			
		$("select#courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});		
		$("select#courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});	
		$("select#sort_by").change(function(){
			sort_by=$(this).children("option:selected").val();
		});		

		$('#catagory_article').on("change",function () {
			var category_id_article = $(this).find('option:selected').val();
			$.ajax({
				url: "<?php echo $tmp_url."getSubCategory"; ?>",
				type: "GET",
				data: "category_id="+category_id_article,
				success: function (response) {
					$("#sub_catagory_article").html(response);
				},
			});
		}); 
			
			
	$('#search-judgements').on('keyup',function(){
		search_judgements();		
	});
	$('#search-supreme-judgements').on('keyup',function(){
		search_supreme_judgements();		
	});
	
	 $("#judgements").click(function(){
		
		$("#search-judgements").show();
		$("#search-supreme-judgements").hide();
		$("#search-bareacts").hide();
		$("#search-articles").hide();
		$("#search").hide();
		$("#searchmaxims").hide();
        $("#search-performa").hide();

		$("#judgements-filter").show();
		$("#supreme-judgements-filter").hide();
		$("#bareacts-filter").hide();
		$("#legal-terms-filter").hide();
		$("#article-filter").hide();
		$("#maxims-filter").hide();
        $("#performa-filter").hide();
		
		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').hide();
        $('#iframe5ef').hide();
		$('#iframe6ef').hide();
        $('#iframe7ef').hide();
			
			
		$("select#catagory").change(function(){
			catagory=$(this).children("option:selected").val();
		});			
		$("select#year").change(function(){
			year=$(this).children("option:selected").val();
		});			
		$("select#courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});			
		
	});


	 $("#supreme_judgements").click(function(){
		
		$("#search-judgements").hide();
		$("#search-supreme-judgements").show();
		$("#search-bareacts").hide();
		$("#search-articles").hide();
		$("#search").hide();
		$("#searchmaxims").hide();
        $("#search-performa").hide();

		$("#judgements-filter").hide();
		$("#supreme-judgements-filter").show();
		$("#bareacts-filter").hide();
		$("#legal-terms-filter").hide();
		$("#article-filter").hide();
		$("#maxims-filter").hide();
        $("#performa-filter").hide();
		
		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').hide();
        $('#iframe5ef').hide();
		$('#iframe6ef').hide();
        $('#iframe7ef').hide();
		$("select#year").change(function(){
			year=$(this).children("option:selected").val();
		});			
		
	});

//search bareacts

	$("#bareacts").click(function(){
		
		$("#search-bareacts").show();
		$("#search-articles").hide();
		$("#search-judgements").hide();
			$("#search-supreme-judgements").hide();
		$("#search").hide();
		$("#searchmaxims").hide();
        $("#search-performa").hide();

		$("#judgements-filter").hide();
		$("#supreme-judgements-filter").hide();
		$("#bareacts-filter").show();
		$("#legal-terms-filter").hide();
		$("#article-filter").hide();
		$("#maxims-filter").hide();
        $("#performa-filter").hide();

		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').hide();
        $('#iframe5ef').hide();
		$('#iframe6ef').hide();
        $('#iframe7ef').hide();
		
		$('#search-bareacts').on('keyup',function(){
			search_bareacts();	
		});
	});

//search maxims
	$("#legalterms").click(function(){
		
		$(".btn").removeClass("active-reference-tabs");
		$("#legalterms").addClass("active-reference-tabs");

		$("#search-bareacts").hide();
		$("#search-articles").hide();
		$("#search-judgements").hide();
			$("#search-supreme-judgements").hide();
		$("#search").show();
		$("#searchmaxims").hide();
        $("#search-performa").hide();
		
		$("#judgements-filter").hide();
		$("#supreme-judgements-filter").hide();
		$("#bareacts-filter").hide();
		$("#legal-terms-filter").show();
		$("#article-filter").hide();
		$("#maxims-filter").hide();
        $("#performa-filter").hide();
		
		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').show();
        $('#iframe5ef').hide();
		$('#iframe6ef').hide();
        $('#iframe7ef').hide();

		var alphabet_value = $("#alphabet_value").val();
		if(alphabet_value != ""){
			$("#selected_alphabet_"+alphabet_value).css("background-color", "gray");
		}else{
			$("#selected_alphabet_all").css("background-color", "gray");
		}
		
	});


//artcles

	$("#articles").click(function(){
		$("#search-bareacts").hide();
		$("#search-judgements").hide();
			$("#search-supreme-judgements").hide();
		$("#search-articles").show();
		$("#search").hide();
		$("#searchmaxims").hide();
        $("#search-performa").hide();

		$("#judgements-filter").hide();
		$("#supreme-judgements-filter").hide();
		$("#bareacts-filter").hide();
		$("#legal-terms-filter").hide();
		$("#article-filter").show();
		$("#maxims-filter").hide();
        $("#performa-filter").hide();

		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').hide();
		$('#iframe5ef').show();
		$('#iframe6ef').hide();
        $('#iframe7ef').hide();
		
		$('#search-articles').on('keyup',function(){
			search_articles();
		});
	
	});
	$("#maxims").click(function(){
		
		$(".btn").removeClass("active-reference-tabs");
		$("#maxims").addClass("active-reference-tabs");

		$("#search-bareacts").hide();
		$("#search-judgements").hide();
			$("#search-supreme-judgements").hide();
		$("#search-articles").hide();
		$("#search").hide();
		$("#searchmaxims").show();
        $("#search-performa").hide();
		
		$("#judgements-filter").hide();
		$("#supreme-judgements-filter").hide();
		$("#bareacts-filter").hide();
		$("#legal-terms-filter").hide();
		$("#article-filter").hide();
		$("#maxims-filter").show();
        $("#performa-filter").hide();

		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').hide();
		$('#iframe5ef').hide();
		$('#iframe6ef').show();
        $('#iframe7ef').hide();

		var alphabet_value_maxims = $("#alphabet_value_maxims").val();
			if(alphabet_value_maxims != ""){
				$("#selected_alphabet_maxims_"+alphabet_value_maxims).css("background-color", "gray");
			}else{
				$("#selected_alphabet_maxims_all"+alphabet_value_maxims).css("background-color", "gray");
			}

	});

    $("#performas").click(function(){
        $("#search-bareacts").hide();
		$("#search-judgements").hide();
		$("#search-supreme-judgements").hide();
		$("#search-articles").hide();
		$("#search").hide();
		$("#searchmaxims").hide();
        $("#search-performa").show();
		
		$("#judgements-filter").hide();
		$("#supreme-judgements-filter").hide();
		$("#bareacts-filter").hide();
		$("#legal-terms-filter").hide();
		$("#article-filter").hide();
		$("#maxims-filter").hide();
        $("#performa-filter").show();

		$('#iframe2ef').hide();
		$('#iframe8ef').hide();
		$('#iframe3ef').hide();
		$('#iframe4ef').hide();
		$('#iframe5ef').hide();
		$('#iframe6ef').hide();
        $('#iframe7ef').show();

		$('#search-performa').on('keyup',function(){
			search_performas();
		});
    });
});
 
 </script>

 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>


<script type="text/javascript">

	$('#search').on('keyup',function(){
		var alphabet_value = $("#alphabet_value").val();
		search_legal_terms(alphabet_value);
	});

	$('#searchmaxims').on('keyup',function(){
		var alphabet_value_maxims = $("#alphabet_value_maxims").val();
		search_maxims(alphabet_value_maxims);
	});

	
</script>

<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>



