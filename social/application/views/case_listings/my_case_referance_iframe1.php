<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<style type="text/css">
	.navbar.navbar-inverse.navbar-header2, #resultt, #resulttss {
	    display: none;
	}#main-content {
	    padding-top: 0;
	}#draftcasess li{
	    max-width: 22%;
	    display: inline-block;
	    margin: 4px 4px;
	}#draftcasess .performa, .list{
		display: none;
	}body{
		background: transparent;
	}#iframedata div#resulttss, #iframedata div#resultt {
	    color: #fff;
	    margin-top: 30px;
	}.search_data{
		max-width: 100%;
	}#draftcasess .pagination{
		margin-top:0;
	}.search_data:focus-visible {
	    outline: none;
	    border: 1px solid #fff;
	}
</style>
<?php $tmp_url = str_replace("/social","",base_url()); ?>

<div id="notification-loaders">
	<div id="loading_spinner_notification" style="text-align:center;">
		<span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
	</div>
</div>

<script type="text/javascript">
	function search_judgements(){
		$(".btn").removeClass("active");
		$("#judgements").addClass("active");
		var catagory = $("#catagory").children("option:selected").val();
		var year = $("#year").children("option:selected").val();
		var courtname = $("#courtname").children("option:selected").val();
		var sort_by = $("#sort_by").children("option:selected").val();
		var value = $("#search-judgements").val();		
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchhighcourt"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'court': courtname,'year':year,'catagory':catagory,'search':value,'sort_by':sort_by},
			success:function(data){
				putdata(data);
			}
		});
	}
	function search_researches(){
		$(".btn").removeClass("active");
		$("#researches").addClass("active");
		var catagory = $("#catagory").children("option:selected").val();
		var sub_catagory = $("#subcategory").children("option:selected").val();
		var value = $("#search-researches").val();		
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchmyresearches"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"category": catagory,"sub_category":sub_catagory},
			success:function(data){
				console.log(data);
				putdata(data);
			}
		});
	}
	function putdata(data) {
		if(data!="" || data!=null){
			$("#iframedata").html(data);
		}else {
			$("#iframedata").html("No records found...");
		}
	}
	function search_performas(){
		$(".btn").removeClass("active");
		$("#performas").addClass("active");
		var catagory_performa = $("#catagory").children("option:selected").val();
        var value = $("#search-performa").val();
        $.ajax({
            type: 'get',
            async: true,
            url: "<?php echo base_url().'/case_listings/getPerformas' ?>", 
            beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
            data:{'search': value,"category": catagory_performa},
            success:function(data){
				putdata(data);
			}
        });
    }
    function search_supreme_judgements(){

		$(".btn").removeClass("active");
		$("#supreme_judgements").addClass("active");

		var year = $("#supreme_year").children("option:selected").val();
		var sort_by = $("#supreme_sort_by").children("option:selected").val();
		var value = $("#search-supreme-judgements").val();
		// alert(sort_by);
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchsupremecourt"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'year':year,'search':value,'sort_by':sort_by},
			success:function(data){
				putdata(data);
			}
		});
	}function search_bareacts(){

		$(".btn").removeClass("active");
		$("#bareacts").addClass("active");

		var value = $("#search-bareacts").val();
		var sort_by_bareacts = $("#sort_by_bareacts").children("option:selected").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchacts"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,'sort_by':sort_by_bareacts},
			success:function(data){
				putdata(data);
			}
		});
	}
	function set_alphabet_legal_term(alphabet_value){
		$("#search").val('');
		$("#alphabet_value").val(alphabet_value);
		$(".selected_alphabet").css("background-color", "");
		if(alphabet_value != ""){
			$("#selected_alphabet_"+alphabet_value).css("background-color", "gray");
		}else{
			$("#selected_alphabet_all"+alphabet_value).css("background-color", "gray");
		}
		search_legal_terms(alphabet_value)
	}
	function search_legal_terms(alphabet_value){
		var value = $("#search").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchmaxims"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"alphabet":alphabet_value},
			success:function(data){
				console.log(data);
				$("#resultt").html(data);
				$('#resultt').show();
				$('.list').hide();

			}
		});
	}
	function search_articles(){

		$(".btn").removeClass("active");
		$("#articles").addClass("active");

		var catagory_article = $("#catagory").children("option:selected").val();
		var sub_catagory_article = $("#sub_catagory_article").children("option:selected").val();
		var sort_by_article = $("#sort_by_article").children("option:selected").val();
		
		var value = $("#search-articles").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searcharticles"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"category": catagory_article,"sub_category":sub_catagory_article,"sort_by":sort_by_article},
			success:function(data){
				console.log(data);
				putdata(data);
			},error:function(res){				
				putdata(res);

			}
		});
	}
	function set_alphabet_maxims(alphabet_value_maxims){
		$("#searchmaxims").val('');
		$("#alphabet_value_maxims").val(alphabet_value_maxims);
		$(".selected_alphabet_maxims").css("background-color", "");
		if(alphabet_value_maxims != ""){
			$("#selected_alphabet_maxims_"+alphabet_value_maxims).css("background-color", "gray");
		}else{
			$("#selected_alphabet_maxims_all"+alphabet_value_maxims).css("background-color", "gray");
		}
		search_maxims(alphabet_value_maxims)
	}
	function search_maxims(alphabet_value_maxims){
		var value = $("#searchmaxims").val();
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchmax"; ?>",
			beforeSend: function () { 
				$('#notification-loader').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'search': value,"alphabet":alphabet_value_maxims},
			success:function(data){
				$("#resulttss").html(data);
				$('#resulttss').show();
				$('.list').hide();
			}
		});
	}
	function select_performa(id){
        var performa_name = event.target.innerText;
        if(id){
            $.ajax({
                type: 'get',
                async: true,
                url: "<?php echo base_url().'/case_listings/getCategoryName' ?>", 
                data:{'category_id': id},
                success:function(data){
                    $("#selected_performa").html(performa_name);
                    $(".performa").show();
                    $("#selected_category").html(data);
                    $('#catagory option[value="'+id+'"]').attr('selected', true)
                }
            });
        }
    }
</script>
<select name="catagory" id="catagory" style="display: none;">
    <option value="">Select Catagory</option>
    <?php foreach($cats as $dtd){ 
        ?>
    <option  value="<?php echo $dtd->cid; ?>" <?php echo (isset($_GET['category'])) ? ($_GET['category']==$dtd->cid ? "selected" : "") : ''; ?> ><?php echo $dtd->c_name; ?></option>
    <?php } ?>
</select>
<div id="draftcasess">
	<?php if (isset($_GET['judgements-filter'])) { ?>
	<ul class="form-group list-unstyled" id="judgements-filter">
        <li>
        	<select name="courtname" id="courtname" class="search_data" onchange="search_judgements()">
	            <option value="">Select Court</option>
	            <?php foreach($courts as $dtd){ ?>
	            <option value="<?php echo $dtd->state; ?>"><?php echo $dtd->state; ?></option>
	            <?php } ?>
	        </select>
	    </li>
	    <li>
	        <select name="yearcity" id="year" class="search_data" onchange="search_judgements()">
	            <option value="">Select Year</option>
	            <?php foreach($year as $dtd){ ?>
	            <option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
	            <?php } ?>
	        </select>
	    </li>
	    <li>
	        <select name="sort_by" id="sort_by" class="search_data" onchange="search_judgements()">
	            <option value="">Sort By</option>
	            <option value="asc">Ascending</option>
	            <option value="desc">Descending</option>
	            <option value="recent">Recent</option>
	        </select>
	    </li>
    </ul>
    <script type="text/javascript">
    	search_judgements();
    </script>
    <input type="text" id="search-judgements"  placeholder="Search judgments" class="search_data"/>
	<?php } elseif (isset($_GET['supreme-judgements-filter'])) { ?>
	<ul class="form-group list-unstyled" id="supreme-judgements-filter">
        <li>
        	<select name="yearcity" id="supreme_year" class="search_data" onchange="search_supreme_judgements()">
				<option value="">Select Year</option>
				<?php foreach($supremeyear as $dtd){ ?>
				<option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
				<?php } ?>
			</select>		
	    </li>
	    <li>
	        <select name="sort_by" id="supreme_sort_by" class="search_data" onchange="search_supreme_judgements()">
				<option value="">Sort By</option>	
				<option value="asc">Ascending</option>
				<option value="desc">Descending</option>	
				<option value="recent">Recent</option>
			</select>
	    </li>
    </ul>
    <script type="text/javascript">
    	search_supreme_judgements();
    </script>
    <input type="text" id="search-supreme-judgements"  placeholder="Search Supreme judgments" class="search_data"/>
	<?php } elseif (isset($_GET['bareacts-filter'])) { ?>
	<ul class="form-group list-unstyled" id="bareacts-filter">
        <li>
        	<select name="sort_by_bareacts" id="sort_by_bareacts" class="search_data" onchange="search_bareacts()">
				<option value="">Sort By</option>	
				<option value="asc">Ascending</option>
				<option value="desc">Descending</option>	
				<option value="recent">Recent</option>
			</select>	
	    </li>
    </ul>
    <script type="text/javascript">
    	search_bareacts();
    </script>
    <input type="text" id="search-bareacts"  placeholder="Search Bare Acts" class="search_data"/>
	<?php } elseif (isset($_GET['legal-terms-filter'])) { ?>
	<?php  
		$character = range('A', 'Z');  
	?>
	<ul class="pagination" id="legal-terms-filter">  
		<li><a href='javascript:void(0);' id='selected_alphabet_all' class='selected_alphabet' onclick="set_alphabet_legal_term('')">All</a></li>
		<?php  foreach($character as $alphabet) {  ?>
		<li><a href='javascript:void(0);' id='selected_alphabet_<?=$alphabet?>' class='selected_alphabet' onclick="set_alphabet_legal_term('<?=$alphabet?>')"><?=$alphabet?></a></li>
		<?php } ?>
	</ul>
    <script type="text/javascript">
    	set_alphabet_legal_term('');
    </script>

	<input type="text" class="form-controller search_data" id="search" name="search" placeholder="Search legal terms">
	<input type="hidden" name="alphabet_value" id="alphabet_value" value="">	
	<?php } elseif (isset($_GET['article-filter'])) { ?>
	<ul class="form-group list-unstyled" id="article-filter">
	    <li>
	        <select name="sub_catagory_article" id="sub_catagory_article" class="search_data" onchange="search_articles()">
				<option value="">Select Subcatagory</option>
			<select>
	    </li>
	    <li>
	    	<select name="sort_by_article" id="sort_by_article" class="search_data" onchange="search_articles()">
				<option value="">Sort By</option>	
				<option value="recent">Recent</option>
				<option value="popular">Popular</option>
				<option value="popular">Most Viewed</option>
			</select>
    </ul>
    <script type="text/javascript">
    	search_articles();
    </script>
	<input type="text" id="search-articles" name="Search_links" placeholder="Search Articles" class="search_data"/>
	<?php } elseif (isset($_GET['maxims-filter'])) { ?>
	<?php  
		$character = range('A', 'Z');  
		echo '<ul class="pagination">';  
		echo "<li><a href='javascript:void(0);' id='selected_alphabe
		</li>t_maxims_all'
		class='selected_alphabet_maxims' onclick=set_alphabet_maxims('')>All</a></li>";
		foreach($character as $alphabet)  
		{  
			// echo "<li><a href=".url('searchterms/'.$alphabet).">$alphabet</a></li>"; 
			echo "<li><a href='javascript:void(0);' id='selected_alphabet_maxims_".$alphabet."' class='selected_alphabet_maxims' onclick=set_alphabet_maxims('".$alphabet."')>$alphabet</a></li>";
			
		}  
		
		echo "</ul>"; 
	?> 
    <script type="text/javascript">
    	search_maxims();
		var alphabet_value_maxims = $("#alphabet_value_maxims").val();
		if(alphabet_value_maxims != ""){
			$("#selected_alphabet_maxims_"+alphabet_value_maxims).css("background-color", "gray");
		}else{
			$("#selected_alphabet_maxims_all"+alphabet_value_maxims).css("background-color", "gray");
		}
    </script>
	<input type="text" class="form-controller search_data" id="searchmaxims" name="search" placeholder="Search Maxims">
	<input type="hidden" name="alphabet_value_maxims" id="alphabet_value_maxims" value="" >
	<?php } elseif (isset($_GET['performa-filter'])) { ?>
	<input type="text" class="form-controller search_data" id="search-performa" name="search-performa" placeholder="Search performa">
	<div class="performa">
        <p><b>Selected Performa</b> : <span id="selected_performa"></span></p> 
        <p><b>Category name</b> :<span id="selected_category"></span> </p>
    </div>
	<script type="text/javascript">
		search_performas();
	</script>
	<?php }elseif (isset($_GET['research-filter'])) { ?>
    <script type="text/javascript">
    	search_researches();
    </script>
    <input type="text" id="search-researches"  placeholder="Search Researches" class="search_data"/>
	<?php } ?>
</div>
<div name="iframe_data" id="iframedata" class="result-draft">	
	<div id="resultt"></div>
	<div class="list">
		<?php foreach($maxims as $maxims){ ?>							
			<div class="list_blockk">
				<h4><?php echo $maxims->term?></h4>
				<p><?php echo strip_tags($maxims->definition) ;?></p>
			</div>
		<?php } ?>
	</div>
	<div id="resulttss"></div>
	<div class="list">
		<?php foreach($newmaxims as $maxims){ ?>	
		<div class="list_blockk">
			<h4><?php echo $maxims->term ?></h4>
			<p><?php echo strip_tags($maxims->def) ?></p>
		</div>
		<?php } ?>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>
<script type="text/javascript">	
	$('#search').on('keyup',function(){
		var alphabet_value = $("#alphabet_value").val();
		search_legal_terms(alphabet_value);
	});
	$('#searchmaxims').on('keyup',function(){
		var alphabet_value_maxims = $("#alphabet_value_maxims").val();
		search_maxims(alphabet_value_maxims);
	});		
	// $('#catagory').on("change",function () {
	// 	alert('hello');
		var category_id_article = $('#catagory').val();
		// alert(category_id_article);
		$.ajax({
			url: "<?php echo $tmp_url."getSubCategory"; ?>",
			type: "GET",
			data: "category_id="+category_id_article,
			success: function (response) {
				$("#sub_catagory_article").html(response);
			},
		});
	// }); 	
	$('#search-judgements').on('keyup',function(){
		search_judgements();		
	});
	$('#search-supreme-judgements').on('keyup',function(){
		search_supreme_judgements();		
	});
	$('#search-bareacts').on('keyup',function(){
		search_bareacts();	
	});$('#search-articles').on('keyup',function(){
		search_articles();
	});$('#search-performa').on('keyup',function(){
		search_performas();
	});$('#search-researches').on('keyup',function(){
		search_researches();
	});
</script>