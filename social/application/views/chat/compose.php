<script src="<?php echo base_url();?>scripts/custom/get_usernames.js"></script>


<style type="text/css">
  
  .fake-input { position: relative; width:100%; }
.fake-input input { border:none: background:#fff; display:block; width: 100%; box-sizing: border-box; padding-left: 38px; }
.fake-input img { position: absolute; top: 2px; left: 5px; width:30px; height: 30px; }
div#active_chats {
    display: none !important;
}

</style>
<link rel="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css" />
<link rel="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.themes.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>


<div id="loading_spinner_mail">
      <span class="glyphicon glyphicon-refresh" id="ajspinner_mail"></span>
</div>

<div class="mail-header">
<?php echo lang("ctn_461") ?>
</div>
<?php echo form_open(site_url("chat/compose_pro/")) ?>
<div class="mail-reply clearfix">

<div class="row">
  <div class="col-md-12">

    <div class="fake-input">
  		<?php if($this->settings->info->user_display_type) : ?>
  			<input type="text" class="form-control" placeholder="<?php echo lang("ctn_463") ?>" name="name" id="name-search">
  			<input type="text" name="userid" id="userid-search">
	<?php else : ?>
      <input type="text" class="form-control" placeholder="<?php echo lang("ctn_464") ?>" name="username" id="name-search">
      <input type="hidden" name="userid" id="userid-search">
    </div>
<!--
      name-search
      username-search
      username-search2
      image-search
-->




  <?php endif; ?>
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->
</div>

<div class="mail-reply-textbox">
	<!-- id="mail-reply-textarea" -->
<textarea name="reply" rows="5" style="width:100%; resize:none;" class="form-control"></textarea>
<p class="mail-reply-button"><input type="submit" name="s" value="<?php echo lang("ctn_465") ?>" class="btn btn-primary"></p>
<?php echo form_close() ?>
</div>


<script>

var options = {
	data: ["blue", "green", "pink", "red", "yellow"]
};

$("#susername-search").easyAutocomplete(options);

</script>
