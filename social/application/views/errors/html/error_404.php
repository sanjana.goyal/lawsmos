<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <title>404 Page Not Found</title>
<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
}
    .notfound-404 {
    text-align: center;
    margin: 100px 0;
}
    h3{
        position: relative;
    font-size: 16px;
    font-weight: 700;
    text-transform: uppercase;
    color: #000;
    margin: 0px;
    letter-spacing: 3px;
    padding-left: 6px;
    }
    .notfound-404 h1 {
    font-size: 252px;
    font-weight: 900;
    margin-top: -25px;
    color: #030303;
    letter-spacing: -63px;
    margin-left: -117px;
}
    .notfound-404 h2 {
    font-size: 20px;
    font-weight: 400;
    text-transform: uppercase;
    color: #000;
    margin-top: -12px;
    margin-bottom: 25px;
}
    .notfound-404 h1 span {
    text-shadow: -8px 0px 0px #fff;
}
   
@media(max-width:1800px){
   .notfound-404 h1 {
    letter-spacing: -55px;
    }
}

 @media(max-width:767px){
    .notfound-404 h1 {
    font-size: 191px !important;
    letter-spacing: -48px !important;
    margin-left: -50px !important;
    }
     .notfound-404 h2 {
    font-size: 14px !important;
    margin-top: -20px !important;
    line-height: 23px !important;
    }
}
@media(max-width:480px){
    h3 {
    font-size: 11px !important; 
    }
    .notfound-404 h1 {
    font-size: 154px !important;
    letter-spacing: -37px !important;
    margin-left: -50px !important;
    }
}






</style>
</head>
<body>
	<div class="container">
		<div class="notfound-404">
            <h3 >Oops! Page not found</h3>
            <h1>
                <span>4</span>
                <span>0</span>
                <span>4</span>
            </h1>
            <h2>we are sorry, but the page you requested was not found</h2>
        </div>
	</div>
</body>
</html>