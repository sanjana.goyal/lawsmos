<?php echo form_open(site_url("feed/promote_post_pro/" . $post->ID), array("id" => "social-form-edit")) ?>

<div class="modal-header">

    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> <?php echo lang("ctn_719") ?></h4>

</div>

<div class="modal-body ui-front form-horizontal" id="promotePost">



    <div class="form-group">

        <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_719") ?></label>

        <div class="col-md-9">

            <p><?php echo lang("ctn_724") ?></p>

            <p><?php echo lang("ctn_725") ?> <strong><?php echo lang("ctn_726") ?></strong> <?php echo lang("ctn_727") ?></p>

        </div>

    </div>

    <div class="form-group">

        <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_561") ?></label>

        <div class="col-md-9">

            <input type="text" class="form-control" name="pageviews" id="pageviews" value="1000">

            <span class="help-block"><?php echo lang("ctn_728") ?><br /><br /><?php echo lang("ctn_729") ?>: <strong><?php echo $this->settings->info->credit_price_pageviews ?> <?php echo lang("ctn_350") ?></strong><br /><br /><?php echo lang("ctn_730") ?>: <strong><span id="pageviews_cost"><?php echo $this->settings->info->credit_price_pageviews ?></span> <?php echo lang("ctn_350") ?></strong> </span>

        </div>

    </div>



    <div class="form-group">

        <label for="email-in" class="col-md-3 label-heading">Enter Location</label>

        <div class="col-md-9">

            <input id="locationselect" name="locationselect" placeholder="Enter Your Location" type="text" class="form-control" value="" required />

            <input type="hidden" class="form-control" name="lat" value="" id="us3-lat" />

            <input type="hidden" class="form-control" name="long" value="" id="us3-lon" />

            <span class="help-block">Enter a location to promote this post for a specific location </span>

        </div>

    </div>





    <div class="form-group">

        <label for="email-in" class="col-md-3 label-heading">Select User Type</label>

        <div class="col-md-9">
            <input type="checkbox" name="usertype" value="seeker" id="seeker" onChange="countUniqueUsers()"> <label for="seeker"> Service seeker</label>
            <input type="checkbox" name="usertype" value="student" id="student" onChange="countUniqueUsers()"> <label for="student" > Students/Researcher</label> <br>
            <input type="checkbox" name="usertype" value="professor" id="professor" onChange="countUniqueUsers()"> <label for="professor" > Professors/Universities </label> 
            <input type="checkbox" name="usertype" value="lawyer" id="lawyer" onChange="countUniqueUsers()"> <label for="lawyer">  Lawyers/Law firms</label>
            <span class="help-block">Select User Type to whom you want to promote this post!</span>
            Total Unique Users : <strong><span id="users_count"> 0</span></strong>
        </div>
    </div>
    <!-- <div class="form-group">
        <label class="col-md-3 label-heading">Total Users</label>
        <div class="col-md-9">
            <label id="users_count">N/A</label>
        </div>
    </div> -->

</div>

<div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
    <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_719") ?>" />

</div>
<?php echo form_close() ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"></script>

<script>
    google.maps.event.addDomListener(window, 'load', function() {

        var places = new google.maps.places.Autocomplete(document.getElementById('locationselect'));

        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            // Do watever wif teh value!
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            document.getElementById("us3-lat").value = lat;
            document.getElementById("us3-lon").value = lon;

        });

    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#pageviews').on("change", function() {

            var cost = <?php echo $this->settings->info->credit_price_pageviews ?>;

            var val = $('#pageviews').val();

            var total = parseFloat(val / 1000);

            var total_cost = parseFloat(total * cost);



            $('#pageviews_cost').text(total_cost);

        });    
    });
    function countUniqueUsers(){

        var usertype = [];
        $("input[name='usertype']:checked").each(function() {
            usertype.push($(this).val());
        });

        if(usertype.length == 0){
            $("#users_count").html("0");
        }else{
            $.ajax({
                url: "<?php echo site_url("home/getTotalUsers/") ?>",
                type: "get",
                dataType: "json",
                data: {usertype},
                success: function(data) {
                    console.log("data",data.user_count);
                    $("#users_count").html(data.user_count);
                },
                error: function(error) {
                    $("#users_count").html("0");
                }
            });
        }
        console.log("usertype",usertype);
    }
</script>