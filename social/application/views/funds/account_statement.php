<style>
  .header__outer {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
</style>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css" />


<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.0/css/smoothness/jquery-ui-1.10.0.custom.min.css" />

<script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js "></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.24/filtering/row-based/range_dates.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.19/filtering/row-based/range_numbers.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.24/filtering/row-based/range_dates.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js"></script>








 <?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content margin_left margin_repo separator page-right ">

  <div class="db-header clearfix">
    <div class="header__outer">
      <div class="page-header-title"> <span class="glyphicon glyphicon-piggy-bank"></span> <?php echo lang("ctn_881") ?></div>
      <div>
        <p>Current Points: <strong><?php echo number_format($this->user->info->points, 2) ?></strong></p>
      </div>
    </div>

  </div>


  <ol class="breadcrumb">
    <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
    <li><a href="<?php echo site_url("funds") ?>"><?php echo lang("ctn_250") ?></a></li>
    <li class="active"><?php echo lang("ctn_388") ?></li>
  </ol>

  <hr>


  <div class="form-group col-md-12">
    <div class="form-controll col-md-2">
      <label>Start Date</label>
      <input class="form-control" id="startDate"  readonly name="startDate" type="text"  />
    </div>
    <div class="form-controll col-md-2">
      <label>End Date</label>
      <input class="form-control" id="endDate"  readonly name="endDate" type="text"  />
    </div>
    <div class="form-controll col-md-2">
      <label>Transaction Type</label>
      <select  class="form-control" name="transactionType" id="transactionType">
        <option value="">Select Transaction Type</option>
        <option  value="1">Credit</option>
        <option value="2">Debit</option>
      </select>
    </div>
    <div class="form-controll col-md-2">
      <label>Detail Type</label>
      <select  class="form-control" name="detailType" id="detailType">
        <option value="">Select Detail Type</option>
        <option  value="1">Rewards Point</option>
        <option value="2">Add points to wallet</option>
        <option value="3">View lead</option>
        <option value="4">Subscription</option>
      </select>
    </div>
    <div class="form-controll col-md-2">
      <label>Sort by date</label>
      <select  class="form-control" name="sortType" id="sortType">
        <option value="">Select Order</option>
        <option  value="1">Ascending</option>
        <option value="2">Descending</option>
      </select>

      <!-- <input class="form-control" id="search"  name="search" type="text"  /> -->
    </div>
    <div class="form-controll col-md-2">
      <label>&nbsp;</label>
      <input class="form-control btn" id="filter" value="Filter" name="filter" type="button"  onclick="filter_statement();"/>
      <!-- <input class="form-control btn" id="csv" value="csv" name="csv" type="button"  onclick="csv();"/> -->
    </div>
  </div>
  <div class="form-group col-md-12">

    <!-- <div class="form-controll col-md-2"> -->
      <button class="btn btn-primary" onClick="download_csv();">Download as CSV</button>
    <!-- </div> -->
    <!-- <div class="form-controll col-md-2"> -->
      <button class="btn btn-primary" onClick="download_pdf();">Download as PDF</button>
    <!-- </div> -->
  </div>
 

  <div>



  <table id="payment_table" class="table table-bordered table-hover table-striped">
    <thead>
      <tr class="table-header">
        <td><?php echo lang("ctn_873") ?>
        </td>
        <td><?php echo lang("ctn_874") ?></td>
        <td><?php echo lang("ctn_875") ?></td>
        <td><?php echo lang("ctn_876") ?></td>
        <td><?php echo lang("ctn_878") ?></td>
        <td><?php echo lang("ctn_877") ?></td>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>

</div>
<script type="text/javascript">
  $(document).ready(function() {

    var sd =  (d => new Date(d.getFullYear(), d.getMonth() -1, d.getDate())) (new Date);
    var ed = new Date();

    $('#startDate').datetimepicker({
      pickTime: false,
      format: "MM/DD/YYYY",
      defaultDate:sd,
      // minDate:sd,
      // maxDate:ed
    });

    $('#endDate').datetimepicker({
      pickTime: false,
      format: "MM/DD/YYYY",
      defaultDate:ed,
      // minDate:sd,
      // maxDate:ed
    });

    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var detailType = $("#detailType").val();
    var sortType = $("#sortType").val();
    var transactionType = $("#transactionType").val();
    
    var filter_data = {
      startDate: startDate,
      endDate: endDate,
      detailType: detailType,
      sortType:sortType,
      transactionType:transactionType
    };
    show_account_statement(filter_data);

  });

  function download_csv(){
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var detailType = $("#detailType").val();
    var sortType = $("#sortType").val();
    var transactionType = $("#transactionType").val();

    var query = "?startDate="+startDate+"&endDate="+endDate+"&detailType="+detailType+"&sortType="+sortType+"&transactionType="+transactionType
    window.open('<?php echo site_url("funds/getAccountStatementCSV") ?>'+query, '_blank');
  }

  function download_pdf(){
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var detailType = $("#detailType").val();
    var sortType = $("#sortType").val();
    var transactionType = $("#transactionType").val();

    var query = "?startDate="+startDate+"&endDate="+endDate+"&detailType="+detailType+"&sortType="+sortType+"&transactionType="+transactionType
    window.open('<?php echo site_url("funds/getAccountStatementPDF") ?>'+query, '_blank');
  }

  function filter_statement(){

    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var detailType = $("#detailType").val();
    var sortType = $("#sortType").val();
    var transactionType = $("#transactionType").val();

    var filter_data = {
      startDate: startDate,
      endDate: endDate,
      detailType: detailType,
      sortType:sortType,
      transactionType:transactionType
    };
    show_account_statement(filter_data);
  }

  function show_account_statement(filter_data){
    console.log("new",filter_data);
    var st = $('#search_type').val();
    
    var table = $('#payment_table').DataTable({
      "dom": "Bfrt" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",

     
      destroy: true,
      "processing": false,
      "pagingType" : "full_numbers",
      "pageLength" : 50,
      "serverSide": true,
      "orderMulti": false,
      "orderable" : false,
      searching:false,
     
     
      buttons: [

      ],

      
      
      
      columns: [
        { "orderable" : false },
        { "orderable" : false },
        { "orderable" : false },
        { "orderable" : false },
        { "orderable" : false },
        { "orderable" : false },
      ],
      "ajax": {
        url: "<?php echo site_url("funds/payment_logs_page") ?>",
        type: 'GET',
        data: filter_data
      },
      "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function() {
      table.search(this.value).draw();
    });
  } 

  function change_search(search) {
    var options = [
      "search-like",
      "search-exact",
      // "search-like",
      // "search-exact",
      // "user-exact",
      // "email-exact"

    ];
    set_search_icon(options[search], options);
    $('#search_type').val(search);
    $("#form-search-input").trigger("change");
  }

  function set_search_icon(icon, options) {
    for (var i = 0; i < options.length; i++) {
      if (options[i] == icon) {
        $('#' + icon).fadeIn(10);
      } else {
        $('#' + options[i]).fadeOut(10);
      }
    }
  }
  // function csv(){
  //   $.ajax({
  //       url: "<?php echo site_url("funds/getAccountStatementCSV") ?>",
  //       dataType: 'json', 
  //       type: 'get',
  //       success: function(data) {
  //           response = jQuery.parseJSON(data);
  //           console.log(response);
  //       }             
  //   });
  // }
</script>