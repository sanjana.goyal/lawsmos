<style>
    #responsive-menu-links{
        display:none;
    }
    #buy_form{
      margin-top: 7px;
      margin-right: 55px;
      margin-left: -30px;
    }

</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content pay-gateway page-right">

<div class="db-header clearfix">
    <!-- <div class="page-header-title"> <span class="glyphicon glyphicon-piggy-bank"></span> <?php echo lang("ctn_250") ?></div> -->
    <div class="page-header-title"> <span class="glyphicon glyphicon-piggy-bank"></span> Buy Lawsmos Points For Services</div>
    <div class="db-header-extra">
		
		 <a href="<?php echo site_url("funds/account_statement") ?>" class="btn btn-danger btn-sm">Account Statement</a>
		 <!-- <a href="<?php echo site_url("funds/deduction_log") ?>" class="btn btn-danger btn-sm">Your Deduction Logs</a>
		 <a href="<?php echo site_url("funds/payment_log") ?>" class="btn btn-info btn-sm"><?php echo lang("ctn_388") ?></a> 
		 <a href="funds/spend" class="btn btn-success btn-sm"><?php echo lang("ctn_731") ?></a> -->

</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <!-- <li class="active"><?php echo lang("ctn_250") ?></li> -->
</ol>
<p><?php echo lang("ctn_247") ?></p>
<p>Points: <strong><?php echo number_format($this->user->info->points,2) ?></strong></p>
<hr>
<!-- <p class="align-center"><img src="<?php echo base_url() ?>/images/paypal.png"></p>
		<center>
		<form method="post" action="https://www.paypal.com/cgi-bin/webscr" accept-charset="UTF-8" class="form-inline">
				<input type="hidden" name="charset" value="utf-8" />
				<input type="hidden" name="cmd" value="_xclick" />
				<input type="hidden" name="item_number" value="funds01" />
				<input type="hidden" name="item_name" value="<?php // echo $this->settings->info->site_name ?> <?php // echo lang("ctn_250") ?>" />
				<input type="hidden" name="quantity" value="1" />
				<input type="hidden" name="custom" value="<?php //echo $this->user->info->ID ?>" />
				<input type="hidden" name="receiver_email" value="<?php // echo $this->settings->info->paypal_email ?>" />
				<input type="hidden" name="business" value="<?php // echo $this->settings->info->paypal_email ?>" />
				<input type="hidden" name="notify_url" value="<?php // echo site_url("IPN/process2") ?>" />
				<input type="hidden" name="return" value="<?php // echo site_url("funds") ?>" />
				<input type="hidden" name="cancel_return" value="<?php// echo site_url("funds") ?>" />
				<input type="hidden" name="no_shipping" value="1" />
				<input type="hidden" name="currency_code" value="<?php //echo $this->settings->info->paypal_currency ?>"> 
				<input type="hidden" name="no_note" value="1" />
		<div class="col-md-4 col-sm-4 col-xs-9 input-group funt-p">
		    <select name="amount" class="form-control">
		    <option value="50.00"><?php // echo $this->settings->info->payment_symbol ?>50.00 - 50 <?php //echo lang("ctn_350") ?></option>
		    <option value="100.00"><?php // echo $this->settings->info->payment_symbol ?>100.00 - 100 <?php // echo lang("ctn_350") ?></option>
		    <option value="500.00"><?php // echo $this->settings->info->payment_symbol ?>500.00 - 500 <?php // echo lang("ctn_350") ?></option>
		    <option value="100.00"><?php  // echo $this->settings->info->payment_symbol ?>1000.00 - 1000 <?php  // echo lang("ctn_350") ?></option>
		    </select>
		  </div>
		  <button type="submit" class="btn btn-primary"><?php // echo lang("ctn_249") ?></button>
		</form>
		</center>

<?php if($stripe !==null)  : ?> -->
<!-- <hr> -->

<h3>Pay With Stripe</h3>
<p>You can also buy funds with Stripe, which accepts any major credit/debit card. None of your information is stored on this site.</p>

<ul class="fund-list">
  <li class="gatway-btn" id="jerry">
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <form action="<?php echo site_url("IPN/stripe/1") ?>" method="POST" id="stripe_checkout_form">

      <b><?php echo lang("ctn_350"); ?></b> (Min : 100)
        <div class="col-md-12" id="buy_form">
            <div class="col-md-6">
              <input class="form-control" type="text" name="amount" id="amount" onkeypress="return isNumber(event);" onkeyup="amountEnter();" maxlength="8">
              <input type="hidden" name="stripeToken" id="stripeToken" value="">
            </div>
            <div class="col-md-6"><button class="btn" id="buy" type="button">Buy</button></div>
      </div>
      
      

       
    </form>
  </li>
  <!-- <li class="gatway-btn">
    <form action="<?php echo site_url("IPN/stripe/2") ?>" method="POST">
      <script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="<?php echo $stripe['publishable_key']; ?>"
        data-amount="10000"
        data-label="Buy <?php echo $this->settings->info->payment_symbol ?>100.00 - 100 <?php echo lang("ctn_877") ?>"
        data-name="<?php echo $this->settings->info->payment_symbol ?>100.00 for <?php echo $this->settings->info->site_name ?>"
        data-description="<?php echo lang("ctn_877") ?>"
        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
        data-locale="auto"
        data-zip-code="true"
        data-currency="<?php echo $this->settings->info->paypal_currency ?>">
      </script>
    </form>
  </li>
  <li class="gatway-btn">
    <form action="<?php echo site_url("IPN/stripe/3") ?>" method="POST">
      <script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="<?php echo $stripe['publishable_key']; ?>"
        data-amount="50000"
        data-label="Buy <?php echo $this->settings->info->payment_symbol ?>500.00 - 500 <?php echo lang("ctn_877") ?>"
        data-name="<?php echo $this->settings->info->payment_symbol ?>500.00 for <?php echo $this->settings->info->site_name ?>"
        data-description="<?php echo lang("ctn_877") ?>"
        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
        data-locale="auto"
        data-zip-code="true"
        data-currency="<?php echo $this->settings->info->paypal_currency ?>">
      </script>
    </form>
  </li>


  <li class="gatway-btn">
    <form action="<?php echo site_url("IPN/stripe/4") ?>" method="POST">
      <script
        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
        data-key="<?php echo $stripe['publishable_key']; ?>"
        data-amount="100000"
        data-label="Buy <?php echo $this->settings->info->payment_symbol ?>1000.00 - 1000 <?php echo lang("ctn_877") ?>"
        data-name="<?php echo $this->settings->info->payment_symbol ?>1000.00 for <?php echo $this->settings->info->site_name ?>"
        data-description="<?php echo lang("ctn_877") ?>"
        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
        data-locale="auto"
        data-zip-code="true"
        data-currency="<?php echo $this->settings->info->paypal_currency ?>">
      </script>
    </form>
  </li> -->
</ul>

<?php endif; ?>

<?php if( !empty($this->settings->info->checkout2_accountno) && !empty($this->settings->info->checkout2_secret)) : ?>
<h3>2CHECKOUT</h3>
<img src="https://www.2checkout.com/upload/images/paymentlogoshorizontal.png" alt="2Checkout.com is a worldwide leader in online payment services" />
<div class="row">

    <div class="col-md-3 col-sm-6 col-xs-12">
        <form action='https://www.2checkout.com/checkout/purchase' method='post'>
                <input type='hidden' name='sid' value='<?php echo $this->settings->info->checkout2_accountno ?>' />
                <input type='hidden' name='mode' value='2CO' />
                <input type='hidden' name='li_0_type' value='product' />
                <input type='hidden' name='li_0_name' value='<?php echo $this->settings->info->payment_symbol ?>50.00 for <?php echo $this->settings->info->site_name ?>' />
                <input type='hidden' name='li_0_price' value='50.00' />
                <input type='hidden' name='x_receipt_link_url' value="<?php echo site_url("IPN/checkout2/1") ?>">
                <input type="hidden" name="currency_code" value="<?php echo $this->settings->info->paypal_currency ?>" />
                <input name='submit' type='submit' value='<?php echo lang("ctn_249") ?> <?php echo $this->settings->info->payment_symbol ?>50.00 - 50 <?php echo lang("ctn_350") ?>' />
                </form>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <form action='https://www.2checkout.com/checkout/purchase' method='post'>
                <input type='hidden' name='sid' value='<?php echo $this->settings->info->checkout2_accountno ?>' />
                <input type='hidden' name='mode' value='2CO' />
                <input type='hidden' name='li_0_type' value='product' />
                <input type='hidden' name='li_0_name' value='<?php echo $this->settings->info->payment_symbol ?>100.00 for <?php echo $this->settings->info->site_name ?>' />
                <input type='hidden' name='li_0_price' value='100.00' />
                <input type='hidden' name='x_receipt_link_url' value="<?php echo site_url("IPN/checkout2/2") ?>">
                <input type="hidden" name="currency_code" value="<?php echo $this->settings->info->paypal_currency ?>" />
                <input name='submit' type='submit' value='<?php echo lang("ctn_249") ?> <?php echo $this->settings->info->payment_symbol ?>10.00 - 100 <?php echo lang("ctn_350") ?>' />
                </form>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">
        <form action='https://www.2checkout.com/checkout/purchase' method='post'>
                <input type='hidden' name='sid' value='<?php echo $this->settings->info->checkout2_accountno ?>' />
                <input type='hidden' name='mode' value='2CO' />
                <input type='hidden' name='li_0_type' value='product' />
                <input type='hidden' name='li_0_name' value='<?php echo $this->settings->info->payment_symbol ?>500.00 for <?php echo $this->settings->info->site_name ?>' />
                <input type='hidden' name='li_0_price' value='500.00' />
                <input type='hidden' name='x_receipt_link_url' value="<?php echo site_url("IPN/checkout2/3") ?>">
                <input type="hidden" name="currency_code" value="<?php echo $this->settings->info->paypal_currency ?>" />
                <input name='submit' type='submit' value='<?php echo lang("ctn_249") ?> <?php echo $this->settings->info->payment_symbol ?>500.00 - 500 <?php echo lang("ctn_350") ?>' />
                </form>
    </div>
</div>
<?php endif; ?>
</div>


<script>
$(document).ready(function(){
 
  $('button').attr("disabled",true);
});
function isNumber(evt) {
      console.log("event",evt);
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
        
    }
function amountEnter(){
  
  var amt = $('#amount').val();
  amountt = amt;
  
  if(amt.length > 0){
    // console.log(amt);
    if(amt >= 100 ){
      $('button').attr("disabled",false);
    }else{
      $('button').attr("disabled",true);
    }
  }
  else{
    console.log("test");
    $('button').attr("disabled",true);
  }
  
}

// $('#buy').click(function(){
//   alert('hii');
//   var amt = $('#amount').val();
//   if(amt.length == 0 || amt == null ){
//     alert('Please enter valid amount');
//   }
  

// });


var handler = StripeCheckout.configure({
  key: '<?php echo $stripe['publishable_key']; ?>',
  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
  locale: 'auto',
  token: function(token) {
    console.log(token.id);
    $('#stripeToken').val(token.id);
    $('#stripe_checkout_form').submit();

    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
  }
});

document.getElementById('buy').addEventListener('click', function(e) {
  // Open Checkout with further options:
  var buy_amt=$('#amount').val();
  buy_amt=parseFloat(buy_amt).toFixed(2);
  handler.open({
    name: "<?php echo $this->settings->info->payment_symbol ?> "+buy_amt+" for <?php echo $this->settings->info->site_name ?>",
    description: '<?php echo lang("ctn_877") ?>',
    amount: buy_amt*100,
    currency:'INR',
    zipcode:true 
  });
  e.preventDefault();
})
</script>