

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css" />

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css">

<style>



.fc-day-grid-event .fc-time {
    font-weight: 700;
    display: none;
}

.swal2-html-container{
    font-size:14px;
}



</style>


  <?php $this->load->view('sidebar/sidebar.php');?>

<div class="white-area-content separator page-right ">
    <div class="appointment-outer">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span>Plan</div>
    <div class="member_left"><span><p id="total_member"></p></span></div>
    <!-- <div class="db-header-extra form-inline">

<div class="form-group has-feedback no-margin">
<div class="input-group">
<input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
<div class="input-group-btn">
<input type="hidden" id="search_type" value="0">
<button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>

<ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
<li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
<li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok no-display" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
<li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok no-display" id="name-exact"></span> <?php echo lang("ctn_81") ?></a></li>
</ul>

</div>
</div>
</div>
</div> -->
</div>



<div class="table-responsive city-plan-book">
    <?php echo form_open(site_url()); ?>

    <ul>
    <li><p id="plan_detail" style="text-align: center;"></p></li>
        <li>
            <label for="">Select City</label>
            <select  class="form-control" name="" id="city" value="Select City" onchange="getPrice(this)">
            <?php foreach ($all_city->result() as $plan_detail): ?>
                <option  value="<?php echo $plan_detail->id; ?>"><?php echo $plan_detail->name; ?></option>
            <?php endforeach;?>
            </select>
            
        </li>
        <li><label>Price</label><input class="form-control" type="text" readonly id="price" value=""></li>
        <li><label>Start Date (yyyy/mm/dd)</label> <input class="form-control" id="startDate"  readonly name="startDate" type="text"  /></li>
        <p class="text-danger start_date"></p>
        <li><label>End Date (yyyy/mm/dd) </label><input style="background: #fff" class="form-control" id="endDate" name="endDate" readonly type="text"  /><input type="hidden" id="plan_renew" value="0"></li>
        <p class="text-danger end_date"></p>    
    </ul>
                <div class="city-btn__p">
                    <button type="button" class="btn btn-primary" id="buy_plan">Buy Plan</button>
                </div>
                <?php echo form_close() ?>
</div>


</div>
</div>




<!-- <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js
"></script>
 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.1/sweetalert.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.all.min.js"></script>




<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/gcal.js"></script>







<div class="container-fluid">
<div class="row">

	<div class="col-md-12 month-appoint">
		<h1 style="font-size:22px;">This Month Plan(s) </h1>
		<div id="calendar111">
		</div>

	</div>
</div>
</div>




<script type="text/javascript">
$(document).ready(function() {

      var  min_expire_date =" ";
      var max_last_date = " ";


    var city_id  = $('#city').val();
    var _url = '<?php echo base_url() ?>user_settings/get_buy_city_plan_list/'; //// active city plan for calander view
     var id = "<?php echo $id ?>";
    _url = _url+city_id+'/'+id;

    getPrice(city_id);

    $('#calendar111').fullCalendar({
        color: '#e0a211',
        textColor: '#000000',
        dayClick: function (date, allDay, jsEvent, view) {
        $("#dialog").dialog('open');
    },
		eventSources: [
         {
             events: function(start, end, timezone, callback) {
                 $.ajax({
                    url: _url,
                 dataType: 'json',
                 data: {
                 // our hypothetical feed requires UNIX timestamps
                 start: start.unix(),

                 },
                 success: function(msg) {
                     var events = msg.events;
                     console.log("member")
                    console.log(events)
                    console.log(events.length)
                   
                     callback(events);
                 }
                 });
             }
         },
     ]


    });

});

var recent_expire_date = "<?php echo $recently_expire_date; ?>";
var min_date = "<?php echo $min_date; ?>";

function calenderView(_url2){
    $('#calendar111').fullCalendar('destroy');
    var _url2 = _url2
    $('#calendar111').fullCalendar({
        color: '#e0a211',
        textColor: '#000000',
        dayClick: function (date, allDay, jsEvent, view) {
        $("#dialog").dialog('open');
    },
		eventSources: [
         {
             events: function(start, end, timezone, callback) {
                 $.ajax({
                    url: _url2,
                 dataType: 'json',
                 data: {
                 // our hypothetical feed requires UNIX timestamps
                 start: start.unix(),

                 },
                 success: function(msg) {
                     var events = msg.events;
                    //  console.log("total_");
                    //  console.log(events.length);
               
                     callback(events);
                 }
                 });
             }
         },
     ]


    });


}


</script>

<script type="text/javascript">

//on city change get price of city

function getPrice(sel){

       var city_id = "";
      if(typeof(sel) == "string"){
          city_id = sel;
      }else{
        city_id = sel.value;
      }


    var _calander_view = '<?php echo base_url() ?>user_settings/get_buy_city_plan_list/';
    //on date change show active plan in calander view

    var id3 = "<?php echo $id ?>";
    _calander_view = _calander_view+city_id+'/'+id3;

    var _member_url = '<?php echo base_url() ?>user_settings/get_all_active_member/';
    

    var id4 = "<?php echo $id ?>";
    _member_url = _member_url+city_id+'/'+id4;

    var _user_plan = '<?php echo base_url() ?>user_settings/get_lawyer_city_plan/';
    
    var user_id = '<?php echo $this->user->info->ID; ?>';  
    _user_plan = _user_plan+user_id;


    calenderView(_calander_view);
    totalMember(_member_url);
    userPlan(_user_plan,city_id,id4);  
    
    
    var _city_price = "<?php echo site_url("user_settings/get_city_plan_price/"); ?>";
    _city_price = _city_price+city_id;

     $.ajax({
        url : _city_price,
        type:"GET",
        dataType:"Json",
        success:function(data){

            var obj = data;

            if((obj.length != 0) ){
                $('#price').val(data[0].price);
            }



        },
        error:function(error){
            console.log(`Error ${error}`);
        }
    });

}
function  totalMember(_member_url){
    $.ajax({
        url : _member_url,
        type:"GET",
        dataType:"Json",
        success:function(data){

            var obj = data;
            // console.log(obj.members_count)
            // console.log('min date', new Date(obj.min_expire_date*1000))
            // console.log('max date', new Date(obj.max_last_date*1000))
            
            
            var min_start_date = new Date(obj.min_expire_date*1000);
            var max_last_date=new Date(min_start_date.getFullYear()+2,min_start_date.getMonth(),min_start_date.getDate() - 1);
            
            // $('#startDate').val(obj.min_expire_date)
            
            $('#startDate').data("DateTimePicker").setDate(new Date(obj.min_expire_date*1000));
            $('#startDate').data("DateTimePicker").setMinDate(new Date(obj.min_expire_date*1000));
            $('#startDate').data("DateTimePicker").setMaxDate(new Date(obj.min_expire_date*1000));

            $('#endDate').data("DateTimePicker").setDate(new Date(obj.max_last_date*1000));
            $('#endDate').data("DateTimePicker").setMinDate(new Date(obj.max_last_date*1000));
            $('#endDate').data("DateTimePicker").setMaxDate(max_last_date);


            
         

            if(obj != " " && obj != null) {
                $('#total_member').html(obj.members_left + " members left");
            }



        },
        error:function(error){
            console.log(`Error ${error}`);
        }
    });

}
function  userPlan(_user_plan,city_id,id4){
    var city_id = city_id;
    var plan_id = id4;
    
    $.ajax({
        url : _user_plan,
        type:"GET",
        dataType:"Json",
        data:{
            'city_id':city_id,
            'plan_id':plan_id,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'},
        success:function(data){

            var obj = data;
            var last_expire_date = obj.last_expire_date;
            
            if(obj.last_expire_date == "") {
                $('#plan_detail').html("");
                $('#plan_renew').val(0);
            }
            else if(obj.length != "") {
                $('#plan_detail').html("You have already buy plan for this city &amp; this plan expire on "+ last_expire_date);
                $('#plan_renew').val(1);
            }



        },
        error:function(error){
            console.log(`Error ${error}`);
        }
    });

}

$('#buy_plan').click(function(){
    
    var city_id = $('#city').val();
    var price = $('#price').val();
    var start_date = $('#startDate').val();
    var end_date = $('#endDate').val();
    var plan_renew = $('#plan_renew').val();
    var error = 0;
    
    Swal.fire({
            title: 'Are you sure?',
            text: "INR "+price +" will deduct from your account.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Buy Plan'
        }).then((result) => {
          if (result.isConfirmed) {
            if(start_date.length == 0 || start_date == ""){
                $('.start_date').html('Please Select Start Date');
                error = 1;
                return false;

            }
            if(end_date.length == 0 || end_date == ""){
                $('.end_date').html('Please Select End Date');
                error = 1;
                return false;

            }
            var date1 = new Date(start_date);
            var date2 = new Date(end_date);
            var Difference_In_Time = date2.getTime() - date1.getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            Difference_In_Days = Difference_In_Days+parseInt(1);
            if(Difference_In_Days < 15){
                $('.end_date').html('Days must be above 15 days');
                return false;
            }
            else if(Difference_In_Days > 730){
                $('.end_date').html('Days must be less than 2 years ');
                return false;
            }

            $.ajax({
                url : "<?php echo site_url("funds/buy_plan/" . $id . "/" . $type . "/" . $this->security->get_csrf_hash()) ?>",
                type: "POST",
                dataType:"json",
                data:{
                    'city_id':city_id,
                    'start_date':start_date,
                    'end_date':end_date,
                    'plan_renew':plan_renew,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'},
                        success:function(data){
                    var obj = data;

                    console.log(obj);
                    if(obj.status == true){
                        window.location.href = "<?php echo site_url("funds/subscriptionsView/") ?>";
                    }

            },
            error:function(error){
                console.log(`Error ${error}`);
            }
            })  
          }
        });
    
    });

</script>

<script>
 var bindDateRangeValidation = function (f, s, e) {
    if(!(f instanceof jQuery)){
      console.log("Not passing a jQuery object");
    }

    var jqForm = f,
        startDateId = s,
        endDateId = e;

    var checkDateRange = function (startDate, endDate) {
        var isValid = (startDate != "" && endDate != "") ? startDate <= endDate : true;
        return isValid;
    }

    var bindValidator = function () {
        var bstpValidate = jqForm.data('bootstrapValidator');
        var validateFields = {
            startDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    callback: {
                        message: 'Start Date must less than or equal to End Date.',
                        callback: function (startDate, validator, $field) {
                            return checkDateRange(startDate, $('#' + endDateId).val())
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: { message: 'This field is required.' },
                    beforeShowDay: disableDates,
                    callback: {
                        message: 'End Date must greater than or equal to Start Date.',
                        callback: function (endDate, validator, $field) {
                            return checkDateRange($('#' + startDateId).val(), endDate);
                        }
                    }
                }
            },
            customize: {
                validators: {
                    customize: { message: 'customize.' }
                }
            }
        }
        if (!bstpValidate) {
            jqForm.bootstrapValidator({
                excluded: [':disabled'],
            })
        }

        jqForm.bootstrapValidator('addField', startDateId, validateFields.startDate);
        jqForm.bootstrapValidator('addField', endDateId, validateFields.endDate);

    };

    var hookValidatorEvt = function () {
        var dateBlur = function (e, bundleDateId, action) {
            jqForm.bootstrapValidator('revalidateField', e.target.id);
        }

        $('#' + startDateId).on("dp.change dp.update blur", function (e) {
            $('#' + endDateId).data("DateTimePicker").setMinDate(e.date);
            dateBlur(e, endDateId);
        });

        $('#' + endDateId).on("dp.change dp.update blur", function (e) {
            $('#' + startDateId).data("DateTimePicker").setMaxDate(e.date);
            dateBlur(e, startDateId);
        });
    }

    bindValidator();
    hookValidatorEvt();
};

$(function () {

    var sd = new Date(), ed = new Date();
    // var  sd = new Date(), ed = new Date();
    var max_end_date = (d => new Date(d.getFullYear() + 2, d.getMonth(), d.getDate() -1 )) (new Date);
    var min_end_date = (d => new Date(d.getFullYear(), d.getMonth(), d.getDate()+15)) (new Date);

    

    $('#startDate').datetimepicker({
      pickTime: false,
      format: "YYYY/MM/DD",
      defaultDate: sd,
      minDate:sd,
      maxDate:sd
    });

    $('#endDate').datetimepicker({
      pickTime: false,
      format: "YYYY/MM/DD",
      defaultDate: min_end_date,
        maxDate: max_end_date,
        minDate: min_end_date
    });

    //passing 1.jquery form object, 2.start date dom Id, 3.end date dom Id
    bindDateRangeValidation($("#form"), 'startDate', 'endDate');
});

function change_search(search)
    {
      var options = [
        "search-like",
        "search-exact",
        "reason-exact",
        "user-exact",
        "page-exact",
        "from-exact",
      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options)
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }







</script>


