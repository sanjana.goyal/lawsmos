<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.all.min.js"></script>
<div class=" white-area-content margin_left margin_repo ">
   <div class="db-header clearfix">
      <div class="page-header-title"> <span class="glyphicon glyphicon-piggy-bank"></span> <?php echo lang("ctn_273") ?></div>
      <div class="db-header-extra"> <a href="<?php echo site_url("funds/account_statement") ?>" class="btn btn-info btn-sm"><?php echo lang("ctn_388") ?></a> <a href="<?php echo site_url("funds") ?>" class="btn btn-primary btn-sm"><?php echo lang("ctn_245") ?></a></div>
   </div>
   <ol class="breadcrumb">
      <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
      <li class="active"><?php echo lang("ctn_273") ?></li>
   </ol>
   <div class="container-fluid">
       <div class="row">
          <?php foreach($plans->result() as $r) : ?>
          <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
             <div class="planarea" style="background: #<?php echo $r->hexcolor ?>; color: #<?php echo $r->fontcolor ?>;">
                <h4 class="plan-title"><?php echo $r->name ?></h4>
                <center>
                   <div class="plan-icon">
                      <span class="<?php echo $r->icon ?>" style="font-size: 28pt; color: #<?php echo $r->hexcolor ?>; "></span>
                   </div>
                </center>
                <p class="align-center"><?php echo $r->description ?></p>
                <hr>
                <?php if($r->days >0) : ?>
                <p class="plan-days"><?php// echo $r->days ?> <?php// echo lang("ctn_277") ?></p>
                <?php else : ?>
                <p class="plan-days"><?php //echo lang("ctn_283") ?></p>
                <?php endif; ?>
                <p class="plan-cost">Plan start from <?php echo $this->settings->info->payment_symbol ?> <?php echo number_format($r->cost,2) ?></p>
                <hr>
                <!-- <a href="<?php echo site_url("funds/buy_plan/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-default form-control"><?php echo lang("ctn_284") ?></a> -->
                <a href="<?php echo site_url('funds/plan_buy/'.$r->ID.'/'.$r->type) ?>" data-rId=<?php echo $r->ID ?> class="btn btn-default form-control buy-subscription"  ><?php echo lang("ctn_284") ?></a>
             </div>
          </div>

          <?php endforeach; ?>
      </div>
   </div>
   <hr>
   <p><?php echo lang("ctn_248") ?>: <?php echo number_format($this->user->info->points,2) ?></p>
   <?php if($this->user->info->premium_time > 0) : ?>
   <?php $time = $this->common->convert_time($this->user->info->premium_time) ?>
   <p><?php echo lang("ctn_276") ?> <?php echo $this->common->get_time_string($time) ?> <?php echo lang("ctn_281") ?></p>
   <?php elseif($this->user->info->premium_time == -1) : ?>
   <p><?php echo lang("ctn_282") ?></p>
   <?php endif; ?>
</div>
<script>
   // $('select').selectpicker();
   
   // $('.buy-subscription').click(function(e) {
   //   e.preventDefault();
   //     let cl = console.log;
   //     let id = $(this).attr('data-rId');
   //     let token = '<?php echo $this->security->get_csrf_hash();?>';
   //     let __url = `funds/buy_plan/${id}/${token}`;
   //     let siteUrl = '<?php echo site_url() ?>';
   //     let url =  siteUrl +__url;
       
   //     Swal.fire({
   //         title: 'Are you sure?',
   //         // text: "You won't be able to revert this!",
   //         icon: 'warning',
   //         showCancelButton: true,
   //         confirmButtonColor: '#3085d6',
   //         cancelButtonColor: '#d33',
   //         confirmButtonText: 'Buy Plan'
   //     }).then((result) => {
   //       if (result.isConfirmed) {
   //       window.location.href = url;
   //       }
   //     });
   // });
</script>