<style>


table.table.research_table tbody tr td p {
    white-space: normal;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
}

</style>


<?php $this->load->view('sidebar/sidebar.php'); ?>

<div class="row page-right view-height">
<div class="col-xs-12">
 <?php echo $this->session->flashdata('success'); ?>
 <?php echo $this->session->flashdata('error'); ?>	
 <h3>My Haednotes</h3>
 <table class="table research_table table-striped table-hover table-bordered dataTable no-footer" role="grid" aria-describedby="friends-table_info">
	<tr class="text-center">
		<th>Title</th>
	    <th>Headnotes</th>
	    <th>Date</th>
	    <th>Action</th>
	    
	</tr>
    <?php
        if(!empty($headnotes)){
            foreach($headnotes as $notes){
             
        ?>
        <tr>
		    <td><p><?php echo $notes->title; ?></p></td>
		    <td> <p><?php echo html_entity_decode($notes->note); ?></p></td>
		    <td style="text-align:left"><?php echo explode(" ",$notes->created_at)[0]; ?></td>
            <td>		
                <a href="<?php echo site_url(); ?>headnotes/edit_headnotes/<?php echo $notes->id; ?>/2" title="Edit Headnotes"><span class="glyphicon glyphicon-edit"></span></a>
                <a href="<?php echo site_url(); ?>headnotes/delete_headnotes/<?php echo $notes->id; ?>" title="Delete Headnotes"><span class="glyphicon glyphicon-remove-circle"></span></a>
            </td>   
	    </tr>
        <?php
            }
        }
        
        else{
           ?>
           <tr><td>No Headnotes Found</td></tr>
           <?php
        }
        
    ?>
  		
</table>
    <div class="rebtn">
    <a href="<?php echo site_url('headnotes/createheadnote');  ?>" class="btn btn-primary re-btn">Create Headnotes</a>
    </div>
    
</div>

 </div>
 <?php 
 if(isset($_POST['editor'])){
	echo $_POST['editor'];
 }
 ?>


<script>
  $(document).ready(function() {
    // alert('goooo');
    setTimeout(function(){ 
        $('.alert-success').fadeOut(1000);        
       }, 5000);
});
  </script>