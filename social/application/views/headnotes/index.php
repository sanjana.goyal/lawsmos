

<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<style>
.error{
	color:red;
}
.result-draft
{

height:1100px;
overflow:scroll;
}

/* width */
::-webkit-scrollbar {
  width: 20px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: black; 
 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #black; 
}

div#judgements-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

}
div#bareacts-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#article-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
ul.bareacts-list li, div#result li, .backend-text ul li, .result-draft li {
    list-style: none;
    margin-bottom: 14px;
    line-height: 28px;
    color: #666;
    margin-right: 30px;
    padding-left: 16px;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}

</style>

 <?php  
	if($type==2){
		if(!empty($get_headnotes_data)){
			$id = $get_headnotes_data[0]->id;
			$title = $get_headnotes_data[0]->title;
			$note = $get_headnotes_data[0]->note;
		}
	}
 
;?> 
<div class="drafttt-page">
    <div class="container">
        <div class="row">
			<div class="col-md-6">
				<div class="section-title">
					<h2>Headnotes Builder</h2>
				</div>
			
				<div class="form-group">
					<label for="catagory">Tittle</label>
				
                    <input type="text" class="form-control" name="headnote_title" id="headnote_title" value="<?php if($type==2)echo $title; ?>">
					<p class="title-error-info error"></p>
                    <input type="hidden" class="form-control" name="headnote_heading" id="headnote_heading" value="<?php if($type==2)echo html_entity_decode($get_headnotes_data[0]->title); ?>">
                    <input type="hidden" class="form-control" name="headnote_id" id="headnote_id" value="<?php if($type==2)echo $get_headnotes_data[0]->id; ?>">
                    <input type="hidden" class="form-control" name="headnote_type" id="headnote_type" value="<?php if($type==2)echo $get_headnotes_data[0]->judgement_type; ?>">
                    <input type="hidden" class="form-control" name="row_id" id="row_id" value="<?php if($type==2)echo $get_headnotes_data[0]->id; ?>">
					
				</div>
                <!-- <div class="form-group ">
					<label for="title">Judgement</label>
					<input type="text" name="title" class="form-control" id="judgement_select" value="">
					<p class="catagory-error-info error"></p>
				</div> -->
				<div class="form-group">
					<!-- <label for="title">Following is the sample draft for reference which may further be modified as per requirement</label> -->
					<label for="title">Headnotes description</label>
					<textarea rows="4" cols="66" name="editor1" class="" id="headnote_content" style="visibility: visible !important; "><?php if($type==2)echo $note; ?></textarea>
					<p class="description-error-info error"></p>
				</div>
				<!-- <div class="form-group ">
					<label for="title">Save as</label>
					<input type="text" name="title" class="form-control" id="casetitle" value="" />
					<p class="title-error-info error"></p>
				</div> -->
				<div class="form-group">
					<button type="submit" class="btn btn-danger" data-attr ="<?php if($type==2) echo "2" ;else  echo "1" ;?>" id="submitBtn"><?php if($type==2) echo "Update Headnote" ;else  echo "Save Headnotes" ;?></button> 
				</div>
				<div id="content-container"></div>
            </div>
			<div class="col-md-6">
				<div class="section-title">
				<h2>References</h2>
			</div>
			
			<iframe class="reference" src="<?php echo base_url('headnotes/my_case_referance_iframe'); ?>"  width="100%" height="1300px" style="border:none;" id="headmoet_iframe"></iframe>
			
           

  			</div>     
	
		</div>		
	</div>
</div>
                
<?php
	$tmp_url = str_replace("/social","",base_url());
?>				
              
			


 <script>
	
	$(document).ready(function(){	
		
		jQuery('#submitBtn').click(function(e){	
			var type = $(this).attr('data-attr');
			var judgememnt_type ="";
			var judgement_id ="";
			var judgement_heading ="";
			var judgement_title ="";
			var casecontent ="";
			var row_id ="";

			if(type==1){
				 judgememnt_type = $('.reference').contents().find('#judgement_type')[0].value;
						if(judgememnt_type == 0){
							alert('please select judgement first...');
							return false;
						}
						else{
							judgement_type = $('.reference').contents().find('#judgement_type')[0].value;
							judgement_id = $('.reference').contents().find('#judgement_id')[0].value;
							judgement_heading = $('.reference').contents().find('#judgement_heading')[0].value;
							
							judgement_title = $('#headnote_title').val();
							casecontent = $('#headnote_content').val();
					
						
						}
			}
			else{
				 judgement_title = $('#headnote_title').val();
				 casecontent = $('#headnote_content').val();
				 judgement_id = $('#headnote_id').val();
				 judgement_heading = $('#headnote_heading').val();
				 judgement_type = $('#headnote_type').val();
				 row_id = $('#row_id').val();
				 type = 2;

			}
		 
				$(".title-error-info").html("");
				$(".description-error-info").html("");
				$(".catagory-error-info").html("");
			
			
				// var judgement_title = $('#headnote_title').val();
				// var casecontent = $('#headnote_content').val();
			
			

				// var catagory = $("#catagory").children("option:selected").val();
        
				var error = false;
				if(judgement_title == ""){
					error = true;
					$(".title-error-info").html("This field is required");
				}
				if(casecontent == ""){
					error = true;
					$(".description-error-info").html("This field is required");
				}
		
				if(error == true){
					return false;
				}
               	e.preventDefault();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				});
						jQuery.ajax({
							url: "<?php echo $tmp_url."headnots/save"; ?>",
							method: 'get',
							data: {
								title:judgement_title,
								casecontent: casecontent,
								judgement_id:judgement_id,
								judgement_type:judgement_type,
								judgement_heading:judgement_heading,
								row_id:row_id,
								type:type
							},
							dataType: "json",
							success: function(result){
								if(result=='failed')
								{
									$('#content-container').html("Please <a href='{{url('lawyer/login')}}'>login</a> or <a href='{{url('lawyer/register')}}'>register</a> to save this case!");
								}
								else
								{
									$('#content-container').html("Headnotes Saved Successfully...");
											$('#headnote_title').val("");
											$('#headnote_content').val("");
											window.location.href = "<?php echo base_url('headnotes'); ?>";
								}
							}
						});	  
       		});
					
		});
 
 </script>

 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>


<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

<script>
$('.reference').load(function(){
	$('.reference').contents().find('.header-area-wrapper').hide();
    $('.reference').contents().find('.footer-area-wrapper').hide();
});
</script>


<script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>

<script>






</script>