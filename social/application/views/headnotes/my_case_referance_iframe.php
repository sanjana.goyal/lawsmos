<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

<style>
.error{
	color:red;
}
.result-draft
{

height:500px;
overflow:auto;
}

/ width /
::-webkit-scrollbar {
  width: 10px !important;
}

/ Track /
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/ Handle /
::-webkit-scrollbar-thumb {
  background: black; 
 
}

/ Handle on hover /
::-webkit-scrollbar-thumb:hover {
  background: black; 
}

div#judgements-filter select {

    width: 22%;
    display: inline-block;
    margin: 4px 4px;

}

div#supreme_judgements_filter select {

width: 22%;
display: inline-block;
margin: 4px 4px;

}
div#bareacts-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#article-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
div#performa-filter select {

width: 23%;
display: inline-block;
margin: 4px 4px;

}
ul.bareacts-list li, div#result li, .backend-text ul li, .result-draft li {
    list-style: none;
    margin-bottom: 14px;
    line-height: 28px;
    color: #666;
    margin-right: 30px;
    padding-left: 16px;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}
.navbar.navbar-inverse.navbar-header2 {
    display: none;
}
	.result-draft  li a{
		position:relative;
	}
	.result-draft  li a:before{
		    position: absolute;
    content: "\f111";
    left: -13px;
    top: -5px;
    font-family: fontawesome;
    font-size: 8px;
	}
#main-content {
    padding-top: 20px;
}
.btn {
    padding: 6px 12px !important;
}
</style>

<form id="draftcasess">
<input type="hidden" name="judgement_type" id="judgement_type" value="0">	
<input type="hidden" name="judgement_id" id="judgement_id" value="">	
				<div class="form-group reference-tabs">
				
					<a href="javascript:void(0);" class="btn btn-danger btn-xs" onclick="search_judgements();" id="judgements">High Court Judgments</a>
					<a href="javascript:void(0);" class="btn btn-danger btn-xs" onclick="search_supreme_judgements();" id="supreme_judgements">Supreme Court Judgments</a>
				
				</div>
                <div>
                    <p><b>Selected Judgement</b> : <span id="selected_performa"></span></p> 
                    <p><b>Category name</b> :<span id="selected_category"></span> </p>
                </div>
				<div class="form-group" id="judgements-filter">
					<!-- <p>Search Judgements </p> -->
					<select name="yearcity" id="year" class="form-control" onchange="search_judgements()">
						<option value="">Select Year</option>
						<?php foreach($year as $dtd){ ?>
						<option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
						<?php } ?>
					</select>
					<select name="courtname" id="courtname" class="form-control" onchange="search_judgements()">
						<option value="">Select Court</option>
						<?php foreach($courts as $dtd){ ?>
						<option value="<?php echo $dtd->state; ?>"><?php echo $dtd->state; ?></option>
						<?php } ?>
					</select>
					<select name="catagory" id="catagory" class="form-control" onchange="search_judgements()">
						<option value="">Select Catagory</option>
						<?php foreach($cats as $dtd){ 
                        ?>
						<option  value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
						<?php } ?>
					</select>
					<select name="sort_by" id="sort_by" class="form-control" onchange="search_judgements()">
						<option value="">Sort By</option>	
						<option value="asc">Ascending</option>
						<option value="desc">Descending</option>	
						<option value="recent">Recent</option>
					</select>
						
				</div>
				<div class="form-group">
					<input type="text" id="search-judgements"  placeholder="Search judgments" class="form-control"/>
				</div>
				<div class="form-group" id="supreme_judgements_filter">
					<!-- <p>Search Judgements </p> -->
					<select name="yearcity" id="supreme_year" class="form-control" onchange="search_supreme_judgements()">
						<option value="">Select Year</option>
						<?php foreach($year as $dtd){ ?>
						<option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
						<?php } ?>
					</select>
					<select name="courtname" id="supreme_courtname" class="form-control" onchange="search_supreme_judgements()">
						<option value="">Select Court</option>
						<?php foreach($courts as $dtd){ ?>
						<option value="<?php echo $dtd->state; ?>"><?php echo $dtd->state; ?></option>
						<?php } ?>
					</select>
					<select name="catagory" id="supreme_catagory" class="form-control" onchange="search_supreme_judgements()">
						<option value="">Select Catagory</option>
						<?php foreach($cats as $dtd){ 
                        ?>
						<option  value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
						<?php } ?>
					</select>
					<select name="sort_by" id="supreme_sort_by" class="form-control" onchange="search_supreme_judgements()">
						<option value="">Sort By</option>	
						<option value="asc">Ascending</option>
						<option value="desc">Descending</option>	
						<option value="recent">Recent</option>
					</select>
						
				</div>
				<div class="form-group">
					<input type="text" id="search_supreme_judgement"  placeholder="Search judgments" class="form-control"/>
				</div>
			
				
				</form>

            <div id="notification-loaders">
				<div id="loading_spinner_notification" style="text-align:center;">
					<span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
				</div>
			</div>
			<div name="iframe_a" id="iframe2ef" class="result-draft"></div>	
			<div name="iframe_b" id="iframe3ef" class="result-draft"></div>	
		
		


<?php
	$tmp_url = str_replace("/social","",base_url());
?>				
              
		

<script>
	function search_judgements(){

		// $('#iframe3ef').hide();

		$(".btn").removeClass("active-reference-tabs");
		$("#judgements").addClass("active-reference-tabs");

		var catagory = $("#catagory").children("option:selected").val();
		var year = $("#year").children("option:selected").val();
		var courtname = $("#courtname").children("option:selected").val();
		var sort_by = $("#sort_by").children("option:selected").val();
		var value = $("#search-judgements").val();
		
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchhighcourt"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'court': courtname,'year':year,'catagory':catagory,'search':value,'sort_by':sort_by},
			success:function(data){
				if(data!="" || data!=null){
					$("#iframe2ef").html(data);
					
					$('#iframe2ef').show();

				}else
				{
					$("#iframe2ef").html("No records found...");
					$('#iframe2ef').show();
				}
			}
		});
	}
	function search_supreme_judgements(){

		// $('#iframe2ef').hide();
		
		$(".btn").removeClass("active-reference-tabs");
		$("#supreme_judgements").addClass("active-reference-tabs");

		var catagory = $("#supreme_catagory").children("option:selected").val();
		var year = $("#supreme_year").children("option:selected").val();
		var courtname = $("#supreme_courtname").children("option:selected").val();
		var sort_by = $("#supreme_sort_by").children("option:selected").val();
		var value = $("#search_supreme_judgement").val();
	
		$.ajax({
			type : 'get',
			async: true,
			url : "<?php echo $tmp_url."searchsupremecourt"; ?>",
			beforeSend: function () { 
				$('#notification-loaders').fadeIn(100);
				$("#ajspinner_notification").addClass("spin");
			},
			complete: function () { 
				$('#notification-loaders').css("display","none");
				$("#ajspinner_notification").removeClass("spin");
			},
			data:{'court': courtname,'year':year,'catagory':catagory,'search':value,'sort_by':sort_by},
			success:function(data){
				if(data!="" || data!=null){
					$("#iframe3ef").html(data);
					$('#iframe3ef').show();
				}else
				{
					$("#iframe3ef").html("No records found...");
					$('#iframe3ef').show();
				}
			}
		});
	}
	
	function select_performa(id){
        var performa_name = event.target.innerText;
        if(id){
            $.ajax({
                type: 'get',
                async: true,
                url: "<?php echo base_url().'/case_listings/getCategoryName' ?>", 
                data:{'category_id': id},
                success:function(data){
					$("#selected_performa").html(performa_name);
                    $("#selected_category").html(data);
                    $('#catagory option[value="'+id+'"]').attr('selected', true)
                    $('#catagory_article option[value="'+id+'"]').attr('selected', true)
                }
            });
        }
    }
</script>

 <script>
	
	$(document).ready(function(){
		search_judgements();

			$("#search-judgements").show();
			$("#judgements-filter").show();
			$("#search_supreme_judgement").hide();
			$("#supreme_judgements_filter").hide();
	
			$("#iframe2ef").show();
			$("#iframe3ef").hide();
		
			
			var catagory = $("#catagory").children("option:selected").val();
			var year = $("#year").children("option:selected").val();
			var courtname = $("#courtname").children("option:selected").val();
			var sort_by = $("#sort_by").children("option:selected").val();

			// search_performas();
			
		$("select#catagory").change(function(){
			catagory=$(this).children("option:selected").val();
		
		});			
		$("select#year").change(function(){
			year=$(this).children("option:selected").val();
		});			
		$("select#courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});		
		$("select#courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});	
		$("select#sort_by").change(function(){
			sort_by=$(this).children("option:selected").val();
		});		


			
			
	$('#search-judgements').on('keyup',function(){
		search_judgements();		
	});

	$('#search_supreme_judgement').on('keyup',function(){
		search_supreme_judgements();		
	});
	
	 $("#judgements").click(function(){
		
		$("#search-judgements").show();
		$("#search_supreme_judgement").hide();
		

		$("#judgements-filter").show();
		$("#supreme_judgements_filter").hide();
		
		
		$('#iframe2ef').show();
		$('#iframe3ef').hide();
		
			
			
		$("select#catagory").change(function(){
			catagory=$(this).children("option:selected").val();
		});			
		$("select#year").change(function(){
			year=$(this).children("option:selected").val();
		});			
		$("select#courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});			
		
	});

	$("#supreme_judgements").click(function(){
		
		$("#search-judgements").hide();
		$("#search_supreme_judgement").show();
		

		$("#judgements-filter").hide();
		$("#supreme_judgements_filter").show();
		
		
		$('#iframe2ef').hide();
		$('#iframe3ef').show();
		
			
			
		$("select#supreme_catagory").change(function(){
			catagory=$(this).children("option:selected").val();
		});			
		$("select#supreme_year").change(function(){
			year=$(this).children("option:selected").val();
		});			
		$("select#supreme_courtname").change(function(){
			courtname=$(this).children("option:selected").val();
		});			
		
	});


});
 
 </script>

 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>


<script type="text/javascript">

	$('#search').on('keyup',function(){
		var alphabet_value = $("#alphabet_value").val();
		search_legal_terms(alphabet_value);
	});

	$('#searchmaxims').on('keyup',function(){
		var alphabet_value_maxims = $("#alphabet_value_maxims").val();
		search_maxims(alphabet_value_maxims);
	});

	
</script>

<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });


function checkjudgementHeadnote(id,judgement_type){
	$('#judgement_id').val(id);
	$('#judgement_type').val(judgement_type);

}



</script>



