
 <style>
 
 .row.styleapppoint {
    background-color: gold;
    padding: 30px;
	margin-top: 71px;	
    box-shadow: 0px 2px 16px 3px black;
}


.section {
	position: relative;
	height: 100vh;
}

.section .section-center {
	position: absolute;
	top: 50%;
	left: 0;
	right: 0;
	-webkit-transform: translateY(-50%);
	transform: translateY(-50%);
}

#booking {
	font-family: 'Montserrat', sans-serif;
	
	background-size: cover;
	background-position: center;
}



.booking-form {
	max-width: 642px;
	width: 100%;
	margin: 15%;
	box-shadow: 0px 1px 27px 2px black;
}

.booking-form .form-header {
	text-align: center;
	margin-bottom: 25px;
}

.booking-form .form-header h1 {
	font-size: 58px;
	text-transform: uppercase;
	font-weight: 700;
	color: #ffc001;
	margin: 0px;
}

.booking-form>form {
	background-color: #101113;
	padding: 30px 20px;
	border-radius: 3px;
}

.booking-form .form-group {
	position: relative;
	margin-bottom: 15px;
}

.booking-form .form-control {
	background-color: #f5f5f5;
	border: none;
	height: 45px;
	border-radius: 3px;
	-webkit-box-shadow: none;
	box-shadow: none;
	font-weight: 400;
	color: #101113;
}

.booking-form .form-control::-webkit-input-placeholder {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form .form-control:-ms-input-placeholder {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form .form-control::placeholder {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form input[type="date"].form-control:invalid {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form select.form-control {
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
}

.booking-form select.form-control+.select-arrow {
	position: absolute;
	right: 0px;
	bottom: 6px;
	width: 32px;
	line-height: 32px;
	height: 32px;
	text-align: center;
	pointer-events: none;
	color: #101113;
	font-size: 14px;
}

.booking-form select.form-control+.select-arrow:after {
	content: '\279C';
	display: block;
	-webkit-transform: rotate(90deg);
	transform: rotate(90deg);
}

.booking-form .form-label {
	color: #fff;
	font-size: 12px;
	font-weight: 400;
	margin-bottom: 5px;
	display: block;
	text-transform: uppercase;
}

.booking-form .submit-btn {
	color: #101113;
	background-color: #ffc001;
	font-weight: 700;
	height: 50px;
	border: none;
	width: 100%;
	display: block;
	border-radius: 3px;
	text-transform: uppercase;
}
 </style>



<div id="booking" class="section">
		<div class="section-center">
			<div class="container">
				<div class="row">
					<div class="booking-form">
						<div class="form-header">
							
						</div>
						<form action="<?php echo site_url('home/bookappointment'); ?>" method="post">
						
						 <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
		
		
		
		<input type="hidden" name="seekerid" value="<?php echo $userdetail[0]->ID; ?>">
		
		
							<input type="hidden" name="providername" value="<?php echo $providerprofile[0]->first_name; ?>">
							<input type="hidden" name="providerid" value="<?php echo $providerprofile[0]->ID; ?>">
							<input type="hidden" name="provideremail" value="<?php echo $providerprofile[0]->email; ?>">
							<input type="hidden" name="providerphone" value="<?php echo $providerprofile[0]->phone; ?>">
						
						
						
						
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<span class="form-label">Name</span>
										<input class="form-control" type="text"  name="seekername" value="<?php echo $userdetail[0]->first_name." ".$userdetail[0]->last_name; ?>" readonly>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<span class="form-label">Email</span>
										<input class="form-control"   name="seekeremail" type="email" value="<?php echo $userdetail[0]->email; ?>" readonly>
									</div>
								</div>
							</div>
							<div class="form-group">
								<span class="form-label">Phone</span>
								<input class="form-control" name="seekerphone" type="tel" value="<?php echo $userdetail[0]->phone; ?>" readonly>
							</div>
							
							<div class="form-group">
								<span class="form-label">Select Catagory</span>
								<select class="form-control" name="catagory" required>
									<option>Select catagory</option>
									<?php foreach($catss as $cats): ?>
									
										<option value="<?php echo $cats->cid; ?>"><?php echo $cats->c_name; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<span class="form-label">Subject Matter</span>
								<textarea name="subjectmatter" class="form-control" required></textarea>
							</div>
							
							
							<div class="row">
								<div class="col-sm-7">
									<div class="form-group">
										<span class="form-label">Appointment Date </span>
										<input type="text" name="dateofappointment" class="form-control" id="from" required />
									</div>
								</div>
								<div class="col-sm-5">
									<div class="row">
										
										<div class="col-sm-8">
											<div class="form-group">
												<span class="form-label">Select Time</span>
												<select class="form-control" name="minute" required>
													<option value="9:00">9:00</option>
													<option value="9:30">9:30</option>
													<option value="10:00">10:00</option>
													<option value="10:30">10:30</option>
													<option value="11:00">11:00</option>
													<option value="11:30">11:30</option>
													<option value="12:00">12:00</option>
													<option value="12:30">12:30</option>
													<option value="1:00">1:00</option>
													<option value="1:30">1:30</option>
													<option value="2:00">2:00</option>
													<option value="2:30">2:30</option>
													<option value="3:00">3:00</option>
													<option value="3:30">3:30</option>
													<option value="4:00">4:00</option>
													<option value="4:30">4:30</option>
													<option value="5:00">5:00</option>
													<option value="5:30">5:30</option>
													<option value="6:00">6:00</option>
													<option value="6:30">6:30</option>
													<option value="7:00">7:00</option>
													<option value="7:30">7:30</option>
													<option value="8:00">8:00</option>
													<option value="8:30">8:30</option>
													
													
												</select>
												<span class="select-arrow"></span>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<span class="form-label">AM/PM</span>
												<select class="form-control" name="timeformat">
													<option value="am">AM</option>
													<option value="pm">PM</option>
												</select>
												<span class="select-arrow"></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
							
								<div class="col-md-12">
									<span class="form-label">Lawyers Info: </span>
									
									<div class="row">
									
										<div class="col-md-8">
											<span class="form-label">Approx Appointment Fee:  <?php if(!empty($coreprofile)){ echo $coreprofile[0]->apperance_charges; }else{ echo "No fee Detail Available";} ?></span>
											<span class="form-label">Name: <?php echo $providerprofile[0]->first_name; ?> </span>
											<span class="form-label">Email: <?php echo $providerprofile[0]->email; ?> </span>
											<span class="form-label">Phone: <?php echo $providerprofile[0]->phone; ?> </span>
											
											
										</div>
										
										<div class="col-md-4">
										
											<span class="form-label">Availability: <?php if(!empty($timeschedule)){ echo $timeschedule; } else { echo "9AM to 6:PM available"; } ?> </span>
											
											
											<?php  if(!empty($coreprofile)){?>
											<span class="form-label"><?php echo $providerprofile[0]->first_name; ?> Can Speak: <?php echo $coreprofile[0]->language; ?></span>
		
											<span class="form-label"><?php echo $providerprofile[0]->first_name; ?> Has done Practice in : <?php  $arraycourt=json_decode($coreprofile[0]->courtnamearray,true);  foreach($arraycourt as $court){ echo $court.",";  } ?></span>
											<?php } ?>
											
											
										</div>
									</div>
								</div>
							
							</div>
							<div class="form-btn">
								<button type="submit" class="submit-btn">Request Appointment</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



<script>


	var dateToday = new Date();
var dates = $("#from").datepicker({
    //defaultDate: "-1w",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    maxDate: '15',
    onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
    }
});
  </script>







