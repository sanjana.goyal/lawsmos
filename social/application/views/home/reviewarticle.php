
<?php 
	
	


?>

<style>
	
	.edit {
	padding-top: 7px;	
	padding-right: 7px;
	position: absolute;
	right: 0;
	top: 0;
	display: none;
}

.article_content h1:hover .edit  {
	display: block;
}

</style>
<div class="container">
<div class="well">
<div class="row">

	<div class="col-md-12">
		
					<div class="user-detail">
                        <?php echo $this->common->get_user_display(array("username" => $aboutarticle->username, "avatar" => $aboutarticle->avatar, "online_timestamp" => $aboutarticle->online_timestamp)) ?>
                    </div>
                    
                    <div class="options" style="float:right;">
                    
                    
						<?php 
						
							echo $options='<a href="' . site_url("articles/approvearticle/" . $aboutarticle->articleid. "/" . $this->security->get_csrf_hash()) .'" class="btn btn-success btn-xs" title="Approve"><span class="glyphicon glyphicon-thumbs-up"></span></a> <a href="' . site_url("articles/rejectarticle/" . $aboutarticle->articleid . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'' . lang("ctn_86") . '\')" class="btn btn-danger btn-xs" title="Reject"><span class="glyphicon glyphicon-thumbs-down"></span></a> <a href="' . site_url("articles/close_article/" . $aboutarticle->articleid . "/" . $this->security->get_csrf_hash()) .'" onclick="return confirm(\'You are about to quit review this article!.\')" class="btn btn-info btn-xs" title="I dont want to review! "><span class="glyphicon glyphicon-remove"></span></a>';
						?>
                    </div>    
                        
                      
                     <div class="article_content">  
                        <?php if(!empty($aboutarticle->image)) : ?>
								<img src="<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative ?>/<?php echo $aboutarticle->image ?>" class="blog-post-thumb-main">
                        <?php endif; ?>
        			
		
						<h1  contenteditable="true"  onBlur="saveToDatabase(this,'heading','<?php echo $aboutarticle->articleid; ?>')" onClick="showEdit(this);"><?php echo $aboutarticle->articletitle; ?></h1>
						
						<p contenteditable="true"  onBlur="saveToDatabasearticlecontent(this,'heading','<?php echo $aboutarticle->articleid; ?>')" onClick="showEdit(this);"><?php echo base64_decode($aboutarticle->description);  ?></p>
						<div class="edit"><i class="fa fa-pencil fa-lg"></i></div>
					</div>
				<div class="section-content">	
					
					<?php  if(!empty($menu)): ?>
					
					<div class="section-tree" style="float:left; width:27%; background-color:#cac6c6;padding: 19px; ">
						<h5>Contents: </h5>
						<?php  echo $menu; ?>
					</div>
					<?php  endif; ?>
					<div style="float:none;">
					
					<?php foreach($sections as $section): ?>
					
						<div class="submne" id="<?php echo $section['ID']."section"; ?>">
							<h4 contenteditable="true" title="click to edit" onBlur="saveSectionTitle(this,'heading','<?php echo $section['ID']; ?>')" onClick="showEdit(this);"><?php	echo $section['title']; ?> </h4>
						</div>
						
						
						
						
						<div class="section_content">
							<section contenteditable="true"  onBlur="saveSectioncontent(this,'heading','<?php echo $section['ID']; ?>')" onClick="showEdit(this);"><?php echo base64_decode($section['body']);  ?></section>
						
							
						</div>
						
						
					<?php 	endforeach;   ?>
						
					</div>
					</div>
	</div>


</div>
</div>
</div>


<script>
function showEdit(editableObj) {
	
		
			$(editableObj).css("background","#f5f5f5");
			$(editableObj).css("width","100%");
			
			
		} 
function saveToDatabase(editableObj,column,id) {
	$(editableObj).css("background","#f5f5f5 url(<?php echo base_url('images/tenor.gif'); ?>) no-repeat right");
	$.ajax({
		url: '<?php echo base_url('articles/saveedit');  ?>',
		type: "GET",
		data:{heading:column, editval:editableObj.innerHTML,id:id},
		error: function(){
              alert('Something is wrong');
           },
		success: function(data){
			
			//console.log(data);
			
			$(editableObj).css("background","#f5f5f5");
			
		}        
   });
}



function saveSectionTitle(editableObj,column,id) {
	$(editableObj).css("background","#f5f5f5 url(<?php echo base_url('images/tenor.gif'); ?>) no-repeat right");
	$.ajax({
		url: '<?php echo base_url('articles/savesectiontitle');  ?>',
		type: "GET",
		data:{heading:column, editval:editableObj.innerHTML,id:id},
		error: function(){
              alert('Something is wrong');
           },
		success: function(data){
			
			//console.log(data);
			
			$(editableObj).css("background","#f5f5f5");
			
		}        
   });
}


function saveSectioncontent(editableObj,column,id) {
	
	$(editableObj).css("background","#f5f5f5 url(<?php echo base_url('images/tenor.gif'); ?>) no-repeat right");
	$.ajax({
		url: '<?php echo base_url('articles/savesectioncontent');  ?>',
		type: "GET",
		data:{heading:column, editval:editableObj.innerHTML,id:id},
		error: function(){
              alert('Something is wrong');
           },
		success: function(data){
			
			//console.log(data);
			
			$(editableObj).css("background","#f5f5f5");
			
		}        
   });
}

 function saveToDatabasearticlecontent(editableObj,column,id) {
	
	
	$(editableObj).css("background","#f5f5f5 url(<?php echo base_url('images/tenor.gif'); ?>) no-repeat right");
	$.ajax({
		url: '<?php echo base_url('articles/saveArticleContent');  ?>',
		type: "GET",
		data:{heading:column, editval:editableObj.innerHTML,id:id},
		error: function(){
              alert('Something is wrong');
           },
		success: function(data){
			
			//console.log(data);
			
			$(editableObj).css("background","#f5f5f5");
			
		}        
   });
}

</script>
