<ul class="newnav nav nav-sidebar">
           <?php if($this->user->loggedin && isset($this->user->info->user_role_id) && 
           ($this->user->info->admin || $this->user->info->admin_settings || $this->user->info->admin_members || $this->user->info->admin_payment)

           ) : ?>
              <li id="admin_sb">
                <a data-toggle="collapse" data-parent="#admin_sb" href="#admin_sb_c" class="collapsed <?php if(isset($activeLink['admin'])) echo "active" ?>" >
                  <span class="glyphicon glyphicon-wrench sidebar-icon sidebar-icon-red"></span> <?php echo lang("ctn_157") ?>
                  <span class="plus-sidebar"><span class="glyphicon <?php if(isset($activeLink['admin'])) : ?>glyphicon-menu-down<?php else : ?>glyphicon-menu-right<?php endif; ?>"></span></span>
                </a>
                <div id="admin_sb_c" class="panel-collapse collapse sidebar-links-inner <?php if(isset($activeLink['admin'])) echo "in" ?>">
                  <ul class="inner-sidebar-links">
                    <?php if($this->user->info->admin || $this->user->info->admin_settings) : ?>
                      <li class="<?php if(isset($activeLink['admin']['settings'])) echo "active" ?>"><a href="<?php echo site_url("admin/settings") ?>"><?php echo lang("ctn_158") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['social_settings'])) echo "active" ?>"><a href="<?php echo site_url("admin/social_settings") ?>"> <?php echo lang("ctn_159") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['ad_settings'])) echo "active" ?>"><a href="<?php echo site_url("admin/ad_settings") ?>"> <?php echo lang("ctn_671") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['rotation_ads'])) echo "active" ?>"><a href="<?php echo site_url("admin/rotation_ads") ?>"> <?php echo lang("ctn_706") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['promoted_posts'])) echo "active" ?>"><a href="<?php echo site_url("admin/promoted_posts") ?>"> <?php echo lang("ctn_705") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['verified_requests'])) echo "active" ?>"><a href="<?php echo site_url("admin/verified_requests") ?>"> <?php echo lang("ctn_694") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['payment_plans'])) echo "active" ?>"><a href="<?php echo site_url("admin/payment_plans") ?>"> <?php echo lang("ctn_879") ?></a></li>

                       <li class="<?php if(isset($activeLink['admin']['add_bare_acts_with_cat'])) echo "active" ?>"><a href="<?php echo site_url("admin/add_bare_acts_with_cat") ?>"> Add Bare Acts with catagory</a></li>
                      <li class="<?php if(isset($activeLink['admin']['upgrade_requests'])) echo "active" ?>"><a href="<?php echo site_url("admin/upgrade_requests") ?>">Students Upgrade Requests</a></li>
					  <li class="<?php if(isset($activeLink['admin']['verify_agency'])) echo "active" ?>"><a href="<?php echo site_url("admin/verify_agency") ?>"><?php echo lang("ctn_848") ?></a></li>
					  <li class="<?php if(isset($activeLink['admin']['verify_university'])) echo "active" ?>"><a href="<?php echo site_url("admin/verify_university") ?>">University Requests</a></li>
                      <li class="<?php if(isset($activeLink['admin']['page_categories'])) echo "active" ?>"><a href="<?php echo site_url("admin/page_categories") ?>"> <?php echo lang("ctn_529") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['moderator_requests'])) echo "active" ?>"><a href="<?php echo site_url("admin/moderator_requests") ?>">Moderator Requests</a></li>
                      <li class="<?php if(isset($activeLink['admin']['special_words'])) echo "active" ?>"><a href="<?php echo site_url("admin/special_words") ?>">Special Words</a></li>
					  <li class="<?php if(isset($activeLink['admin']['articles'])) echo "active" ?>"><a href="<?php echo site_url("admin/articles") ?>">All Articles</a></li>
					   <!-- <li class="<?php //if(isset($activeLink['admin']['article_posts'])) echo "active" ?>"><a href="<?php //echo site_url("admin/article_posts") ?>">All Articles Posts</a></li> -->
					  <li class="<?php if(isset($activeLink['admin']['judgements'])) echo "active" ?>"><a href="<?php echo site_url("admin/judgements") ?>">High Court Judgements</a></li>
            <li class="<?php if(isset($activeLink['admin']['supreme'])) echo "active" ?>"><a href="<?php echo site_url("admin/supreme") ?>">Supreme Court Judgements</a></li>
					  <li class="<?php if(isset($activeLink['admin']['performas'])) echo "active" ?>"><a href="<?php echo site_url("admin/performas") ?>">Performas</a></li>
					  <li class="<?php if(isset($activeLink['admin']['add_bareacts'])) echo "active" ?>"><a href="<?php echo site_url("admin/add_bareacts") ?>">Bare Acts</a></li>
					  
                      <li class="<?php if(isset($activeLink['admin']['categories'])) echo "active" ?>"><a href="<?php echo site_url("admin/categories") ?>"> <?php echo lang("ctn_870") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['blogs'])) echo "active" ?>"><a href="<?php echo site_url("admin/blogs") ?>"> <?php echo lang("ctn_792") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['blog_posts'])) echo "active" ?>"><a href="<?php echo site_url("admin/blog_posts") ?>"> <?php echo lang("ctn_793") ?></a></li>

                      <li class="<?php if(isset($activeLink['admin']['faq_category'])) echo "active" ?>"><a href="<?php echo site_url("admin/faq_category") ?>"> FAQ Category</a></li>

                      <li class="<?php if(isset($activeLink['admin']['faq'])) echo "active" ?>"><a href="<?php echo site_url("admin/faq") ?>"> FAQ</a></li>
                     


                      <li class="<?php if(isset($activeLink['admin']['news'])) echo "active" ?>"><a href="<?php echo site_url("admin/news") ?>"> News</a></li>
                      <li class="<?php if(isset($activeLink['admin']['news_posts'])) echo "active" ?>"><a href="<?php echo site_url("admin/news_posts") ?>"> News Posts</a></li>
                      <li class="<?php if(isset($activeLink['admin']['manage_pages'])) echo "active" ?>"><a href="<?php echo site_url("admin/manage_pages") ?>">Manage Pages</a></li>
                    <?php endif; ?>
                    <?php if($this->user->info->admin || $this->user->info->admin_members) : ?>
                    <li class="<?php if(isset($activeLink['admin']['members'])) echo "active" ?>"><a href="<?php echo site_url("admin/members") ?>"> <?php echo lang("ctn_160") ?></a></li>
                    <li class="<?php if(isset($activeLink['admin']['custom_fields'])) echo "active" ?>"><a href="<?php echo site_url("admin/custom_fields") ?>"> <?php echo lang("ctn_346") ?></a></li>
                    <li class="<?php if(isset($activeLink['admin']['reports'])) echo "active" ?>"><a href="<?php echo site_url("admin/reports") ?>"> <?php echo lang("ctn_530") ?></a></li>
                    <?php endif; ?>
                    <?php if($this->user->info->admin) : ?>
                    <li class="<?php if(isset($activeLink['admin']['user_roles'])) echo "active" ?>"><a href="<?php echo site_url("admin/user_roles") ?>"> <?php echo lang("ctn_316") ?></a></li>
                    <?php endif; ?>
                    <?php if($this->user->info->admin || $this->user->info->admin_members) : ?>
                    <li class="<?php if(isset($activeLink['admin']['user_groups'])) echo "active" ?>"><a href="<?php echo site_url("admin/user_groups") ?>"> <?php echo lang("ctn_161") ?></a></li>
                    <li class="<?php if(isset($activeLink['admin']['ipblock'])) echo "active" ?>"><a href="<?php echo site_url("admin/ipblock") ?>"> <?php echo lang("ctn_162") ?></a></li>
                    <?php endif; ?>
                    <?php if($this->user->info->admin) : ?>
                      <li class="<?php if(isset($activeLink['admin']['email_templates'])) echo "active" ?>"><a href="<?php echo site_url("admin/email_templates") ?>"> <?php echo lang("ctn_163") ?></a></li>
                    <?php endif; ?>
                    <?php if($this->user->info->admin || $this->user->info->admin_members) : ?>
                      <li class="<?php if(isset($activeLink['admin']['email_members'])) echo "active" ?>"><a href="<?php echo site_url("admin/email_members") ?>"> <?php echo lang("ctn_164") ?></a></li>
                    <?php endif; ?>
                    <?php if($this->user->info->admin || $this->user->info->admin_payment) : ?>
                      <li class="<?php if(isset($activeLink['admin']['payment_settings'])) echo "active" ?>"><a href="<?php echo site_url("admin/payment_settings") ?>"> <?php echo lang("ctn_246") ?></a></li>
                      <li class="<?php if(isset($activeLink['admin']['payment_logs'])) echo "active" ?>"><a href="<?php echo site_url("admin/payment_logs") ?>"> <?php echo lang("ctn_288") ?></a></li>
                    <?php endif; ?>
                    <ul class="inner-sidebar-links">
                      <li class="<?php if(isset($activeLink['admin']['lawyer_list']["list_type"]) && ($activeLink['admin']['lawyer_list']["list_type"] == "pending")) echo "active" ?>"><a href="<?php echo site_url("admin/lawyer_list/pending") ?>">Lawyers list (Pending)</a></li>
                      <li class="<?php if(isset($activeLink['admin']['lawyer_list']["list_type"]) && ($activeLink['admin']['lawyer_list']["list_type"] == "verified")) echo "active" ?>"><a href="<?php echo site_url("admin/lawyer_list/verified") ?>">Lawyers list (Approved)</a></li>
                      <li class="<?php if(isset($activeLink['admin']['lawyer_list']["list_type"]) && ($activeLink['admin']['lawyer_list']["list_type"] == "rejected")) echo "active" ?>"><a href="<?php echo site_url("admin/lawyer_list/rejected") ?>">Lawyers list (Rejected)</a></li>
                    </ul>
                    <li class="<?php if(isset($activeLink['admin']['courts'])) echo "active" ?>"><a href="<?php echo site_url("admin/court_list") ?>"> <?php echo lang("ctn_810") ?></a></li>
                  </ul>
                </div>
              </li>
            <?php endif; ?>
            <li class="<?php if(isset($activeLink['home']['general'])) echo "active" ?>"><a href="<?php echo site_url() ?>"><span class="glyphicon glyphicon-home sidebar-icon sidebar-icon-blue"></span> <?php echo lang("ctn_154") ?> <span class="sr-only">(current)</span></a></li>
            <li class="<?php if(isset($activeLink['members']['general'])) echo "active" ?>"><a href="<?php echo site_url("members") ?>"><span class="glyphicon glyphicon-user sidebar-icon sidebar-icon-green"></span> <?php echo lang("ctn_155") ?></a></li>
            <li class="<?php if(isset($activeLink['settings']['general'])) echo "active" ?>"><a href="<?php echo site_url("user_settings") ?>"><span class="glyphicon glyphicon-cog sidebar-icon sidebar-icon-pink"></span> <?php echo lang("ctn_156") ?></a></li>
            
      
          </ul>
