<?php 
	
				$univ=$this->user_model->get_university_by_id($this->user->info->ID);
				$university = $univ->row();
?>

<div class="row margin_left k">
        <div class="col-md-12">
        	<div class="white-area-content">
        		
        		<div class="db-header clearfix">
				    <div class="page-header-title"> <span class="glyphicon glyphicon-file"></span> Add University</div>
				    <div class="db-header-extra"> <!-- <a href="<?php //echo site_url("pages/add") ?>" class="btn btn-primary btn-sm"><?php //echo lang("ctn_531") ?></a> -->
				</div>
				</div>

                <?php echo form_open_multipart(site_url("pages/add_pro_university"), array("class" => "form-horizontal")) ?>
                <div class="panel panel-default">
                <div class="panel-body">
                <p class="panel-subheading">Create New University Page</p>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Name</label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" value="<?php if(!empty($university)){ echo $university->universityname; } ?>" readonly >
                    </div>
                </div>
                <div class="form-group">
                    
                    <div class="col-sm-10">
                        <input type="hidden" name="lati" class="form-control" value="<?php if(!empty($university)){ echo $university->lat; } ?>" readonly >
                    </div>
                </div>
                 <div class="form-group">
                   
                    <div class="col-sm-10">
                        <input type="hidden" name="logi" class="form-control" value="<?php if(!empty($university)){ echo $university->logitude; } ?>" readonly >
                    </div>
                </div>
                
                 <div class="form-group">
                   
                    <div class="col-sm-10">
                        <input type="hidden" name="latilogi" class="form-control" value="<?php if(!empty($university)){ echo $university->lat_long; } ?>" readonly >
                    </div>
                </div>
                
                  <div class="form-group">
                   <label for="inputEmail3" class="col-sm-2 control-label">University Location</label>
                    <div class="col-sm-10">
                        <input type="text" name="address" class="form-control" value="<?php if(!empty($university)){ echo $university->map_location; } ?>" readonly >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">About University</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="description" value="">
                    </div>
                </div>
                <?php if(!$this->settings->info->page_slugs) : ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_535") ?></label>
                        <div class="col-sm-7">
                            <input type="text" name="slug" class="form-control" id="slug-check">
                            <span class="help-block"><?php echo lang("ctn_536") ?>: <?php echo site_url("pages/view/") ?><strong>my-unique-slug</strong></span>
                        </div>
                        <div class="col-sm-3" id="slug-msg">

                        </div>
                    </div>
                <?php endif; ?>
                
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Visibility</label>
                    <div class="col-sm-10">
                        <select name="type" class="form-control">
                            <option value="0"><?php echo lang("ctn_539") ?></option>
                            <option value="1"><?php echo lang("ctn_540") ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Logo</label>
                    <div class="col-sm-10">
                    
                    
                    <input type="file" name="userfile" /> 
                    
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Header Background</label>
                    <div class="col-sm-10">
                   
                        <input type="file" name="userfile_profile" /> 
                    </div>
                </div>
                <h4>University details</h4>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">City</label>
                    <div class="col-sm-10">
                        <input type="text" name="location" class="form-control" value="<?php if(!empty($university)){ echo $university->city; } ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Email</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" value="<?php if(!empty($university)){ echo $university->uni_email; } ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Phone</label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control" value="<?php if(!empty($university)){ echo $university->uni_phone; } ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">University Website</label>
                    <div class="col-sm-10">
                        <input type="text" name="website" class="form-control">
                    </div>
                </div>
                <h4><?php echo lang("ctn_156") ?></h4>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Who Can Post on University Page?</label>
                    <div class="col-sm-10">
                    <select name="posting_status" class="form-control">
                        <option value="0"><?php echo lang("ctn_547") ?></option>
                        <option value="1"><?php echo lang("ctn_548") ?></option>
                        <option value="2"><?php echo lang("ctn_549") ?></option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_550") ?></label>
                    <div class="col-sm-10">
                    <select name="nonmembers_view" class="form-control">
                        <option value="0"><?php echo lang("ctn_53") ?></option>
                        <option value="1"><?php echo lang("ctn_54") ?></option>
                    </select>
                    </div>
                </div>
                     <div class="block_btn">
                <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_531") ?>">
                    </div>
                </div>
                </div>
                <?php echo form_close() ?>


        	</div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#slug-check').on("change", function() {
                var slug = $('#slug-check').val();
                $.ajax({
                    url: global_base_url + 'pages/check_slug',
                    type: 'GET',
                    data: {
                        slug : slug
                    },
                    dataType : 'json',
                    success: function(msg) {
                        if(msg.error) {
                            $('#slug-msg').html(msg.error_msg);
                            return;
                        }
                        if(msg.status == 0) {
                            $('#slug-msg').html(msg.status_msg);
                        } else if(msg.status == 1) {
                            $('#slug-msg').html(msg.status_msg);
                        }
                        return;
                    }
                })
            });
        });
    </script>
