<style>
.row.fdgfdgfd {
    margin-top: 36px;
    margin-bottom: 3px;
}
</style>


<?php 

$userInfo=array();

$userInfo=$page;


?>
<div class="row">
        <div class="col-md-12">
        	<div class="white-area-content margin_left all_pagess">
        		
        		<div class="db-header clearfix">
				    <div class="page-header-title"> <span class="glyphicon glyphicon-file"></span> <?php echo lang("ctn_563") ?> <?php echo $page->name ?></div>
				    <div class="db-header-extra"> 
				</div>
				</div>

                <?php echo form_open_multipart(site_url("pages/edit_page_pro/" . $page->ID), array("class" => "form-horizontal")) ?>
                <div class="panel panel-default">
                <div class="panel-body">
                <p class="panel-subheading"><?php echo lang("ctn_564") ?></p>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_447") ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="name" class="form-control" value="<?php echo $page->name ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_534") ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="description" value="<?php echo $page->description ?>">
                    </div>
                </div>
                <?php if(!$this->settings->info->page_slugs) : ?>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_535") ?></label>
                        <div class="col-sm-7">
                            <input type="text" name="slug" class="form-control" id="slug-check" value="<?php echo $page->slug ?>">
                            <span class="help-block"><?php echo lang("ctn_536") ?> <?php echo site_url("pages/view/") ?><strong>my-unique-slug</strong></span>
                        </div>
                        <div class="col-sm-3" id="slug-msg">

                        </div>
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_537") ?></label>
                    <div class="col-sm-10">
                        <select name="categoryid" class="form-control">
                            <?php foreach($categories->result() as $r) : ?>
                                <option value="<?php echo $r->ID ?>" <?php if($page->categoryid == $r->ID) echo "selected" ?>><?php echo $r->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_538") ?></label>
                    <div class="col-sm-10">
                        <select name="type" class="form-control">
                            <option value="0"><?php echo lang("ctn_539") ?></option>
                            <option value="1" <?php if($page->type == 1) echo "selected" ?>><?php echo lang("ctn_540") ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_541") ?></label>
                    <div class="col-sm-10">
                    <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>" />
                    
                    <!-- <input type="file" name="userfile" /> -->
                    
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_542") ?></label>
                    <div class="col-sm-10">
                    <img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_header ?>" width="100%" />
                        <input type="file" name="userfile_profile" /> 
                    </div>
                </div>
                <p class="panel-subheading"><?php echo lang("ctn_543") ?></p>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_497") ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="location" class="form-control map_name" value="<?php echo $page->location ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_24") ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="email" class="form-control" value="<?php echo $page->email ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_544") ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="phone" class="form-control" value="<?php echo $page->phone ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_545") ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="website" class="form-control" value="<?php echo $page->website ?>">
                    </div>
                </div>
                   <p class="panel-subheading"><?php echo lang("ctn_156") ?></p>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_546") ?></label>
                    <div class="col-sm-10">
                    <select name="posting_status" class="form-control">
                        <option value="0"><?php echo lang("ctn_547") ?></option>
                        <option value="1" <?php if($page->posting_status == 1) echo "selected" ?>><?php echo lang("ctn_548") ?></option>
                        <option value="2" <?php if($page->posting_status == 2) echo "selected" ?>><?php echo lang("ctn_549") ?></option>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_550") ?></label>
                    <div class="col-sm-10">
                    <select name="nonmembers_view" class="form-control">
                        <option value="0"><?php echo lang("ctn_53") ?></option>
                        <option value="1" <?php if($page->nonmembers_view) echo "selected" ?>><?php echo lang("ctn_54") ?></option>
                    </select>
                    </div>
                </div>
                
                <div class="form-group">
	    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_390") ?></label>
	    <div class="col-sm-10">
	      <input type="text" class="form-control map_name" id="us3-address" name="address_1" value="<?php echo  $page->address;  ?>" readonly >
	    
		
	    </div>
	</div>
	
	
	
	<div class="container-fluid map-fullw">
		
			<div id="map-canvas" style="position:relative; width: 70%; height: 400px; display:block;"></div>
	

	</div>

              <div class="block_btn">
					<input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_565") ?>">
                    </div>
	</div>
	
          
               

                </div>
            
                </div>
                
                
                <?php echo form_close() ?>


        	</div>
        </div>
    </div>
    
    

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
  
  
  
  <script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"></script>


    
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAp613NfAwqpgpzboBMgj1eoHTZOzglGAA&libraries=places"></script> -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDL5Pfz_N5zaYOeDIp6cCA4hNNMzDVzoIM&libraries=places"></script> -->

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgriKllmz3R9e7ORquSKDyASj_DP4csuo&libraries=places"></script>
   		    
<script>
	
		var map;
        var geocoder;
        var marker;
        var people = new Array();
        var latlng;
        var infowindow;
		
        $(document).ready(function() {
			
            ViewCustInGoogleMap();
        });
       
       
        function ViewCustInGoogleMap() {

            var mapOptions = {
                center: new google.maps.LatLng(<?php echo $page->lat;  ?>,<?php  echo $page->logitude; ?>),   // Coimbatore = (11.0168445, 76.9558321)
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

			
			var data='[<?php echo json_encode($userInfo); ?>]';
          
            //var data = '[{ "name": "sunielsethy", "addr": "New Delhi", "LatitudeLongitude": "28.5603,77.1617", "MarkerId": "Customer" },{ "DisplayText": "abcd", "ADDRESS": "Coimbatore-641042", "LatitudeLongitude": "11.0168445,76.9558321", "MarkerId": "Customer"}]';

            people = JSON.parse(data); 

            for (var i = 0; i < people.length; i++) {
                setMarker(people[i]);
            }



		
        
        
        
        }

        function setMarker(people) {
            geocoder = new google.maps.Geocoder();
            infowindow = new google.maps.InfoWindow();
            if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
                geocoder.geocode({ 'address': people["addr"] }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                        marker = new google.maps.Marker({
                            
                            position: latlng,
                            map: map,
                            draggable: false,
                            html: people["name"],
                            icon: "images/marker/" + people["MarkerId"] + ".png"
                        
                        
                        });
                        //marker.setPosition(latlng);
                        //map.setCenter(latlng);
                        google.maps.event.addListener(marker, 'click', function(event) {
                            infowindow.setContent(this.html);
                            infowindow.setPosition(event.latLng);
                            infowindow.open(map, this);
                        });
                    }
                    else {
                        alert(people["name"] + " -- " + people["addr"] + ". This address couldn't be found");
                    }
                });
            }
            else {
				
				
                var latlngStr = people["LatitudeLongitude"].split(",");
                var lat = parseFloat(latlngStr[0]);
                var lng = parseFloat(latlngStr[1]);
                latlng = new google.maps.LatLng(lat, lng);
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: false,               // cant drag it
                    html: '<div jstcache="2"> <div jstcache="3" class="title full-width" jsan="7.title,7.full-width"></div>'+people["name"]+' <div class="address"> <div jstcache="4" jsinstance="0" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="1" class="address-line full-width" jsan="7.address-line,7.full-width"></div><div jstcache="4" jsinstance="*2" class="address-line full-width" jsan="7.address-line,7.full-width">'+people["address"]+' </div> </div> </div>',
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                    animation: google.maps.Animation.DROP,
                    
                    
                    
                       // Content display on marker click
                    //icon: "images/marker.png"       // Give ur own image
                });
                //marker.setPosition(latlng);
                //map.setCenter(latlng);
                google.maps.event.addListener(marker, 'click', function(event) {
                    infowindow.setContent(this.html);
                    infowindow.setPosition(event.latLng);
                    infowindow.open(map, this);
                });
            }
        }
        
       </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#slug-check').on("change", function() {
                var slug = $('#slug-check').val();
                $.ajax({
                    url: global_base_url + 'pages/check_slug',
                    type: 'GET',
                    data: {
                        slug : slug
                    },
                    dataType : 'json',
                    success: function(msg) {
                        if(msg.error) {
                            $('#slug-msg').html(msg.error_msg);
                            return;
                        }
                        if(msg.status == 0) {
                            $('#slug-msg').html(msg.status_msg);
                        } else if(msg.status == 1) {
                            $('#slug-msg').html(msg.status_msg);
                        }
                        return;
                    }
                })
            });
        });
    </script>
