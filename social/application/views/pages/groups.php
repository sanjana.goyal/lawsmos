 <div class="row margin_left f">
 <div class="col-md-12">
 <div class="profile-header" style="background: url(<?php echo base_url() ?><?php echo $this->settings->info->upload_path_relative . "/" . $page->profile_header ?>) center center; background-size: cover;">
 <div class="profile-header-avatar">
	<img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $page->profile_avatar ?>">
 </div>
 <div class="profile-header-options">
  <?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
<a href="<?php echo site_url("pages/edit_page/" . $page->ID) ?>" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-cog"></span></a> <!-- <a href="<?php //echo site_url("pages/delete_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" onclick="return confirm('<?php //echo lang("ctn_551") ?>')" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a> -->
<?php endif; ?>
 </div>
 <div class="profile-header-name">
<?php echo $page->name ?>
 </div>
 </div>
 <div class="profile-header-bar clearfix">
 <ul>
  
  <li><a href="<?php echo site_url("pages/view/" . $slug) ?>"><?php echo lang("ctn_552") ?></a></li>
  <li><a href="<?php echo site_url("pages/members/" . $slug) ?>"><?php echo lang("ctn_21") ?></a></li>
  <li><a href="<?php echo site_url("pages/albums/" . $slug) ?>"><?php echo lang("ctn_483") ?></a></li>
  <li class="active"><a href="<?php echo site_url("pages/events/" . $slug) ?>"><?php echo lang("ctn_553") ?></a></li>
 
 </ul>

 <div class="pull-right profile-friend-box">

 <?php if($member == null) : ?>
 
	<a href="#" class="btn btn-primary btn-sm"><?php echo lang("ctn_554") ?></a>
    <!-- <a href="<?php //echo site_url("pages/join_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-primary btn-sm"><?php //echo lang("ctn_554") ?></a> -->
  <?php else : ?>
   <a href="#" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span> <?php echo lang("ctn_34") ?> </a>
  
	 <!-- <a href="<?php //echo site_url("pages/leave_page/" . $page->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span> <?php// echo lang("ctn_34") ?></a> -->
  
  <?php endif; ?>
  
 </div>
 </div>

 <div class="row separator">

 <div class="col-md-12">

<div class="white-area-content">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-calendar"></span>Add Groups</div>
     <div class="db-header-extra"><input type="button" value="Add group" class="btn btn-info"  data-toggle="modal" data-target="#addGrouptModal"/></div>
    <div class="db-header-extra form-inline"> </div>
</div>


<table class="table table-bordered">
<tr class="table-header"><td><?php echo lang("ctn_18") ?></td><td><?php echo lang("ctn_52") ?></td></tr>
<?php foreach($groups->result() as $r) : ?>


<tr><td><?php echo $r->groupname; ?></td><td><a href="<?php echo site_url("pages/edit_group/" . $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-warning btn-xs" title="<?php echo lang("ctn_55") ?>" ><span class="glyphicon glyphicon-cog"></span></a> <a href="<?php echo site_url("pages/delete_group/" . $r->pageid . "/". $r->ID . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-danger btn-xs" onclick="return confirm('<?php echo lang("ctn_56") ?>')" title="<?php echo lang("ctn_57") ?>"><span class="glyphicon glyphicon-trash"></span></a> <a href="<?php echo site_url("pages/view_group/" . $r->ID) ?>" class="btn btn-primary btn-xs"><?php echo lang("ctn_58") ?></a></td></tr>

<?php endforeach; ?>
</table>


</div>

</div>
 </div>

 </div>
</div>

<?php if( (isset($member) && $member != null && $member->roleid == 1) || ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) ) : ?> 
<!-- Modal -->
<div class="modal fade" id="addGrouptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span> <?php echo lang("ctn_569") ?></h4>
      </div>
      <div class="modal-body">
         <?php echo form_open(site_url("pages/add_group/" . $page->ID), array("class" => "form-horizontal")) ?>
          <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_18") ?></label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="email-in" name="name">
                    </div>
            </div>
            
            
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="Add Group">
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>


<!-- Modal -->
<div class="modal fade" id="editEventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-folder-open"></span> <?php echo lang("ctn_575") ?></h4>
      </div>
      <?php if($member != null && $member->roleid == 1) : ?>
      <div class="modal-body">
         <?php echo form_open(site_url("pages/edit_event_pro/"), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_570") ?></label>
                    <div class="col-md-8 ui-front">
                        <input type="text" class="form-control" name="name" value="" id="event_name">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_575") ?></label>
                    <div class="col-md-8 ui-front">
                       <a href="<?php echo site_url("pages/view_event/") ?>" id="event_url"><?php echo lang("ctn_576") ?></a>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_271") ?></label>
                    <div class="col-md-8 ui-front">
                        <input type="text" class="form-control" name="description" id="event_desc">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_497") ?></label>
                    <div class="col-md-8">
                      <input type="text" name="location" id="map_name_d" class="form-control map_name">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_572") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control datetimepicker" name="start_date" id="event_start_date">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_573") ?></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control datetimepicker" name="end_date" id="event_end_date">
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_57") ?></label>
                    <div class="col-md-8">
                        <input type="checkbox" name="delete" value="1">
                    </div>
            </div>
            <input type="hidden" name="eventid" id="event_id" value="0" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_13") ?>">
        <?php echo form_close() ?>
      </div>
  <?php else : ?>
  <div class="modal-body form-horizontal">
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_570") ?></label>
                    <div class="col-md-8 ui-front">
                        <span id="event_name"></span>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_575") ?></label>
                    <div class="col-md-8 ui-front">
                       <a href="<?php echo site_url("pages/view_event/") ?>" id="event_url"><?php echo lang("ctn_576") ?></a>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_271") ?></label>
                    <div class="col-md-8 ui-front">
                        <span id="event_desc"></span>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_497") ?></label>
                    <div class="col-md-8">
                     <span id="map_name_d"></span>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_572") ?></label>
                    <div class="col-md-8">
                        <span id="event_start_date"></span>
                    </div>
            </div>
            <div class="form-group">
                    <label for="p-in" class="col-md-4 label-heading"><?php echo lang("ctn_573") ?></label>
                    <div class="col-md-8">
                        <span id="event_end_date"></span>
                    </div>
            </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
      </div>
  <?php endif; ?>
    </div>
  </div>
</div>
