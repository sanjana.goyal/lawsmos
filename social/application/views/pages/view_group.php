<?php 

if(!empty($pageuserss)){

$pageuserss;

}

?>

<div class="white-area-content margin_left margin_new">
<div class="db-header clearfix">
    
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span>Group Members </div>
    <div class="db-header-extra"><input type="button" class="btn btn-primary btn-sm" style="text-transform:capitalize;" value="<?php echo lang("ctn_129") ?> <?php echo $group->groupname ?>" data-toggle="modal" data-target="#memberModal" />
    <input type="button" class="btn btn-primary btn-sm" style="text-transform:capitalize;" value="Add this group to events" data-toggle="modal" data-target="#AddmemberModal" />
    

</div>
</div>	

<ol class="breadcrumb">
 
  <li class="active"><?php echo lang("ctn_125") ?></li>
</ol>

<p><?php echo lang("ctn_126") ?> <b><?php echo $group->groupname ?></b> - <?php echo lang("ctn_127") ?> <b><?php echo number_format($total_members) ?></b></p>

<hr>
<table class="table table-bordered">
<tr class="table-header"><td><?php echo lang("ctn_25") ?></td><td><?php echo lang("ctn_128") ?></td><td><?php echo lang("ctn_52") ?></td></tr>
<?php foreach($users->result() as $r) :

	$useridss[]=$r->userid;
 ?>
<tr><td><?php echo $r->first_name." ".$r->last_name; ?></td><td><?php echo $r->groupname; ?></td><td><a href="<?php echo site_url("pages/remove_user_from_group/" . $r->userid . "/" . $r->groupid . "/" . $this->security->get_csrf_hash()) ?>" class="btn btn-danger btn-xs"><?php echo lang("ctn_130") ?></a></td></tr>
<?php endforeach; ?>
</table>

<div class="align-center">
<?php echo $this->pagination->create_links() ?>
</div>

<div class="modal fade" id="AddmemberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Events to this group</h4>
      </div>
      <div class="modal-body">
      <?php echo form_open(site_url("pages/add_group_to_events/" . $group->ID), array("class" => "form-horizontal")) ?>
           <table class="table table-bordered">
			   <thead>
				   <tr>
					<th>Event Name</th>
					<th>Event Start Date</th>
					<th>Options</th>
					</tr>
				</thead>
				<tbody>
					
						<?php 
						
						if(!empty($eventss)){
							foreach($eventss as $events){
										//print_r($users);
										
									?>
									<tr>
										
										<td><?php echo $events->title; ?></td>
										<td><?php echo $events->start; ?></td>
										<td><input type="checkbox" name="eventidvalues[]" value="<?php echo $events->ID; ?>"></td>
										<input type="hidden" name="pageid[]" value="<?php echo $events->pageid; ?>">
									</tr>
									 <?php 
									
								
								
								
							}
						}
						?>
					
					

				
				</tbody>
           </table>
           
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_61") ?>" />
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>
</div>





















<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang("ctn_129") ?></h4>
      </div>
      <div class="modal-body">
      <?php echo form_open(site_url("pages/add_user_to_group_pro/" . $group->ID), array("class" => "form-horizontal")) ?>
            <div class="form-group">
                    <label for="email-in" class="col-md-3 label-heading"><?php echo lang("ctn_132") ?></label>
                    <div class="col-md-9">	
                        <input type="hidden" class="form-control" id="email-in" name="usernames">
                        <span class="help-text"><?php //echo lang("ctn_131") ?></span>
                    </div>
            </div>
           
           <table class="table table-bordered">
			   <thead>
				   <tr>
					<th>Profileimage</th>
					<th>Name</th>
					<th>Options</th>
					</tr>
				</thead>
				<tbody>
					
						<?php 
							foreach($pageuserss as $users){
								 	
								 	
										//print_r($users);
										
										
									?>
									<tr>
										<td><img src="<?php echo base_url('uploads'); ?>/<?php echo $users->avatar; ?>" alt="<?php echo $users->first_name; ?>" width="50" height="50" class="img img-circle"/></td>
										<td><?php echo $users->first_name; ?></td>
										<td><input type="checkbox" name="idvalues[]" value="<?php echo $users->userid; ?>"></td>
										<input type="hidden" name="pageid[]" value="<?php echo $users->pageid; ?>">
									</tr>
									 <?php 
									
								
								
								
							}
						
						?>
					
					

				
				</tbody>
           </table>
           
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang("ctn_60") ?></button>
        <input type="submit" class="btn btn-primary" value="<?php echo lang("ctn_61") ?>" />
        <?php echo form_close() ?>
      </div>
    </div>
  </div>
</div>
</div>
