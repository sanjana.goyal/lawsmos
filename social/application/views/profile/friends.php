
 <div class="row profile-mp" style="margin-left: 1%;">
 <div class="col-md-12">
   <?php include 'toplayout.php'; ?>
<!-- Add icon library -->
<div class="white-area-content separator row">
<h4>Total <?php echo $countfriend; ?> Connection(s)</h4>
<?php foreach($friendslist->result() as $user): 
  ?>
<div class="  col-lg-3 col-md-6 col-sm-6 col-xs-12">
	<div class="connection_card">
		  <?php 
			echo $this->common->get_user_display(array("username" => $user->username, "avatar" => $user->avatar, "online_timestamp" => $user->online_timestamp))
		  ?>
		  <h1><a style="color:#000 !important;" href="<?php echo site_url('profile'); ?>/<?php echo $user->username; ?>"><?php echo $user->first_name." ".$user->last_name  ?></a></h1>
		  <p class="title"><?php if($user->profile_identification==0){echo"Lawyer";}else if($user->profile_identification==1){ echo "Service Seeker";}else if($user->profile_identification==2){echo "Student";} ?> At <?php echo $user->city ?></p>
		  <p>Connection sincedate <?php echo date($this->settings->info->date_format, $user->timestamp); ?></p>
		  <p><a class="btn btn-xs btn-danger" href="<?php echo base_url('profile'); ?>/<?php echo $user->username ?>" title="<?php echo $user->first_name." ".$user->last_name ?>">View Profile</a> <a class="btn btn-xs btn-warning" href=<?php echo site_url("profile/deadd/" . $user->ID . "/" . $this->security->get_csrf_hash()); ?>  onClick="javascript:return confirm(\'Are you sure to Remove Connection?\')"><?php echo lang("ctn_470"); ?> </a>  
		  </p>
	</div>
    </div>
<?php endforeach; ?>
 </div>
 
