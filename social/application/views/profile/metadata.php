<div class="container">
 	<div class="row">
		<div class="col-md-12">
			<form action="" method="post" class="metaform form-horizontal">
				<div class="form-group">
					<label for="select_page" class="col-sm-3 control-label">Select page to add meta</label>
					<div class="col-md-8">
						<select class="select_page form-control" id="select_page">
							<option value="">Select</option>
							<option value="home">Home Page</option>
							<option value="privacy">Privacy policy</option>
							<option value="terms">Terms and Conditions</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<input class="form-control" type="text" name="title">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" name="description">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" name="keywords">
				</div>
				<div class="form-group">
					<input class="form-control" type="text" name="content">
				</div>
				<div class="form-group">
					<input class="form-control" type="submit" name="metasubmit" value="Submit">
				</div>
			</form>
		</div>
	</div>
</div>