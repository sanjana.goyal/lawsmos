<?php include 'toplayout.php'; $tmp_base_url = str_replace("/social", "", base_url()) ?>

<link rel="stylesheet" type="text/css" href="<?=$tmp_base_url?>/qa/qa-plugin/q2a-social-share-master/css/social-share.css?1.6.1">
    <div class="row profile-mp" style="">
    <div class="col-md-12">


        

        <section class="base-section" id="datafor">
            <div id="content">
            <section class="container">
                <div class="main-section">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="intro-sec">
                                <img style="  width: 100%;
    height: 100%;" src="<?php echo base_url().'/uploads/'.$user->avatar; ?>">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="d-flex flex-wrap">
                                <div class="col-md-5">
                                    <div class="info-sec">
                                        <h2 class="main-text">Advocate</h2>
                                        <p>Name:<span> <?php echo ucfirst($user->first_name).' '.ucfirst($user->last_name); ?></span></p>
                                        <p>Year of practice:<span><?php if($profile['total_exp']) echo $profile['total_exp'].' Years'; else echo 'N/A'; ?> </span></p>
                                        <p>City:<span><?php if($user->city) echo getCityName($user->city); else echo "N/A"; ?></span></p>
                                        <p>Court:<span><?php if($profile['experience_court']) echo getCourtName(json_decode($profile['experience_court'])[0]); else echo "N/A"; ?></span></p>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="specialization-sec">
                                        <p>Areas of Specialization:<span><?php 
                                        $spcl = $profile['unpracticing_area_of_specialization'];
                                        if (!$spcl) {
                                            echo 'N/A';
                                        }
                                        $array = explode(',', $spcl); 
                                        foreach ($array as $value) {
                                             $array2[] = getCategoryName($value);
                                         }
                                         echo implode(',', $array2); ?></span></p>
                                        <p>Fields of Interest:<span> <?php 
                                        $spcl = $profile['unpracticing_fields_of_interest'];
                                         if (!$spcl) {
                                            echo 'N/A';
                                        }
                                        $array = explode(',', $spcl); 
                                        foreach ($array as $value) {
                                             $array5[] = getCategoryName($value);
                                         }
                                         echo implode(',', $array5); ?></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="first-description">
                <div class="container">
                    <div class="description">
                        <h3>(To be filled by the candidate)</h3>
                        <p class="sub-title">Give your resume a powerful start. Describe your professional orientation/
                            objectives as
                            professionals. Keep it brief under 200 words. </p>
                    </div>
                </div>
            </section>
            <section class="experiecce-sec">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                           
                            <div class="details">
                                <div class="section__list">
                                    <div class="section__list-item">
                                        <div class="left">
                                            <h3 class="sub-title">DISPLAY CURRENT AS:</h3>
                                            <p class="des-name">Designation:<span><?php if($profile['experience_designation']) 
                                            { if(json_decode($profile['experience_designation'])[0] == "1"){
                                                                        echo "Designated Senior";
                                                                    }elseif(json_decode($profile['experience_designation'])[0] == "2"){
                                                                        echo "Retired Justice";
                                                                    }else{
                                                                        echo "N/A";
                                                                    }
                                                                    } else echo "N/A"; ?></span></p>
                                            <p class="des-name">Organization (internal link) :<span><?php if($profile['experience_organization_name']) echo json_decode($profile['experience_organization_name'])[0]; else echo "N/A"; ?></span>
                                            </p>
                                            <p class="des-name">Display exp: <span><?php echo $profile['total_exp'] ?> Years</span></p>
                                            <p class="des-name">From: <span><?php if($profile['experience_start_date']) echo json_decode($profile['experience_start_date'])[0]; else echo "N/A"; ?></span></p>
                                            <p class="des-name">Till: <span> <?php if($profile['experience_end_date']) echo json_decode($profile['experience_end_date'])[0]; else echo "N/A"; ?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="details">
                                <div class="section__title">CONTACT DETAILS</div>
                                <div class="section__list">
                                    <div class="section__list-item">
                                        <div class="left">
                                            <p class="des-name">Offiice Address:<span><?php if($user->address_1) echo $user->address_1.' '.$user->address_2.' '.$user->address3.', '.getCityName($user->city).', '.getStateName($user->state).', '.$user->zipcode; else echo "N/A"; ?></span></p>
                                            <p class="des-name">Phone no:<span>+91-<?php echo $user->phone; ?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="details">
                                <div class="section__title">LAWSMOS PROFILE DETAILS</div>
                                <div class="section__list">
                                    <div class="section__list-item">
                                        <div class="left">
                                            <p class="des-name">User Since: <span><?php if($profile['created_at']) echo date('F, Y', strtotime($profile['created_at'])); else echo 'N/A'; ?> </span></p>
                                            <p class="des-name">User Rating:<span><?php if(get_user_rating($user->ID)>0) echo round(get_user_rating($user->ID),1); else echo 'N/A'; ?></span></p>
                                            <p class="des-name">Connections:<span><?php echo count(getuser_friends($user->ID)); ?></span></p>
                                            <p class="des-name">City Rank:<span>10</span></p>
                                            <p class="des-name">Total Consultations: <span><?php echo count(get_apointments($user->ID)); ?></span></p>
                                            <p class="des-name">Lawsmos Score:<span><?php if(get_user_score($user->ID)) echo get_user_score($user->ID); else echo 0; ?></span></p>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- <section class="first-description">
                <div class="container">
                    <div class="description">
                        <h3>(To be filled by the candidate)</h3>
                        <p>Give your resume a powerful start. Describe your professional
                            orientation/
                            objectives as
                            professionals. Keep it brief under 200 words. </p>
                    </div>
                </div>
            </section> -->
            </div>
            <section class="container">
                <div class="button-sec">
                    <a class="button-sec" href="#" data-toggle="modal" data-target="#myModal">Share</a>
                    <a class="button-sec" href="<?php echo base_url('PdfController') ?>">Download as PDF </a>
                   <p id="p1" style="display: none;"><?php echo base_url('my_resume_public/'.$user->ID); ?></p>


                    <a type="button" class="button-sec" id="copy_btn" style="cursor: pointer;">Copy Link</a>
                    <a class="button-sec" target="_blank" href="<?php echo base_url('PdfController/preview') ?>">Print</a>

   
                  
            </section>
        </section>

    

      
    </div>
</div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Share your resume</h4>
        </div>
        <div class="modal-body">
          <div class="social-wrapper">
    <div class="social-share-text">
         <a class="button-sec" style="
    width: max-content;
" href="<?php echo base_url('?url='.base_url('my_resume_public/'.$user->ID)); ?>">Share on your timeline</a>OR <br>
    </div>
    <br>
    <span class="btn-share with-icon"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('my_resume_public/'.$user->ID); ?>" target="_blank" rel="external nofollow" class="social-share-btn btn-social btn-fb" title="Facebook"><i class="social-icon-facebook"></i> Facebook</a></span>
    <span class="btn-share with-icon"><a href="https://plus.google.com/share?url=<?php echo base_url('my_resume_public/'.$user->ID); ?>" target="_blank" rel="external nofollow" class="social-share-btn btn-social btn-gp" title="Google+"><i class="social-icon-google-plus"></i> Google+</a></span>
    <span class="btn-share with-icon"><a href="https://twitter.com/intent/tweet?text=<?php echo ucfirst($user->first_name).' '.ucfirst($user->last_name); ?>&amp;url=<?php echo base_url('my_resume_public/'.$user->ID); ?>" target="_blank" rel="external nofollow" class="social-share-btn btn-social btn-tw" title="Twitter"><i class="social-icon-twitter"></i> Twitter</a></span>
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>

<!--the script-->
<script>

    $('#download_pdf').click(function() {
    var options = {
        //'width': 800,
    };
    var pdf = new jsPDF('l', 'pt', 'a4');
    pdf.addHTML($("#content"), 0, 50, options, function() {

        pdf.save('my_resume.pdf');
    });
});


 $('#copy_btn').click(function() {
 var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($('#p1').text()).select();
  document.execCommand("copy");
  $temp.remove();
  $('#copy_btn').css('background-color','grey');
  $('#copy_btn').text('Copied');
});
// var doc = new jsPDF();
// var specialElementHandlers = {
//     '#editor': function (element, renderer) {
//         return true;
//     }
// };

// $('#download_pdf').click(function () {
//     doc.fromHTML($('#content').html(), 15, 15, {
//         'width': 170,
//             'elementHandlers': specialElementHandlers
//     });
//     doc.save('sample-file.pdf');
// });
</script>