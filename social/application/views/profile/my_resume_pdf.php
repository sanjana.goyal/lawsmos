

<div class="row profile-mp" style="margin-left: 20%;">
    <div class="col-md-8">

        <body style="margin: 0;font-family: Arial;">
            <section class="base-section" id="datafor" style="">
                <div id="content">
                    <section class="container" style="width:1170px;margin:0 auto;padding: 0 15px;">
                        <div class="main-section" style=" padding-top: 15px;">
                            <div class="row" style="display: flex; flex-wrap: wrap; margin: 0 -15px;">
                                <div class="col-md-2" style=" width: 16.66666667%;">
                                    <div class="intro-sec" style="">
                                        <img style=" width: 100%; height: 100%;" src="<?=base_url()?>/uploads/default.png">
                                    </div>
                                </div>
                                <div class="col-md-10" style="width: 83.33333333%;">
                                    <div class="d-flex flex-wrap" style="display: flex;flex-wrap: wrap;">
                                        <div class="col-md-5" style=" width: 41.66666667%;">
                                            <div class="info-sec" style=" padding-left: 32px;">
                                                <h2 class="main-text" style=" font-size: 16px;">Advocate</h2>
                                                <p style=" font-size: 14px;">Name:<span style="color: #919191; font-size: 14px; padding-left: 8px;"> Test Test</span></p>
                                                <p style="font-size: 14px;">Year of practice:<span style="color: #919191;font-size: 14px;padding-left: 8px;">5 Years </span></p>
                                                <p style="font-size: 14px;">City:<span style="color: #919191;font-size: 14px;padding-left: 8px;">Chandigarh</span></p>
                                                <p style="font-size: 14px;">Court:<span style="color: #919191;font-size: 14px;padding-left: 8px;">A.D.J Kotputali</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-5" style="width: 41.66666667%;">
                                            <div class="specialization-sec" style="margin-top: 25px;">
                                                <p style="font-size: 14px;">Areas of Specialization:<span style="font-size: 14px;color: #919191;padding-left: 10px;">Court marriage lawyers</span></p>
                                                <p style="font-size: 14px;">Fields of Interest:<span style="font-size: 14px;color: #919191;padding-left: 10px;"> Court marriage lawyers</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="first-description">
                        <div class="container" style="width: 1170px;margin: 0 auto; padding: 0 15px;">
                            <div class="description" style="margin-top: 25px;">
                               
                                <p class="sub-title" style="border-bottom: 1px solid;padding-bottom: 20px;">Give your resume a powerful start. Describe your professional orientation/
                                    objectives as
                                    professionals. Keep it brief under 200 words. </p>
                            </div>
                        </div>
                    </section>
                    <section class="experiecce-sec">
                        <div class="container" style="width: 1170px;margin: 0 auto;padding: 0 15px;">
                            <div class="row" style=" margin-right: -15px;margin-left: -15px;display: flex;flex-wrap: wrap;">
                                <div class="col-md-6" style="width: 50%;">
                                    <div class="details" style="margin-top: 20px;">
                                        <div class="section__list">
                                            <div class="section__list-item">
                                                <div class="left">
                                                    <h3 class="sub-title" style="font-size: 15px;color: #000;border-bottom: 1px solid;display: inline-block;">DISPLAY CURRENT AS:</h3>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">Designation:<span style="color: #919191;padding-left: 8px;">Designated Senior</span></p>
                                                    <p class="des-name">Organization (internal link) :<span style="color: #919191;padding-left: 8px;">test</span>
                                                    </p>
                                                    <p class="des-name">Display exp: <span style="color: #919191;padding-left: 8px;">5 Years</span></p>
                                                    <p class="des-name">From: <span style="color: #919191;padding-left: 8px;"></span></p>
                                                    <p class="des-name">Till: <span style="color: #919191;padding-left: 8px;"> 2021-05-01</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style=" width: 50%;">
                                    <div class="details" style="margin-top: 20px;">
                                        <div class="section__title" style="font-size: 20px;font-weight: 700;color: #176bea;text-transform: uppercase;margin-bottom: 25px;">CONTACT DETAILS</div>
                                        <div class="section__list">
                                            <div class="section__list-item">
                                                <div class="left">
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">Offiice Address:<span style="color: #919191;padding-left: 8px;">IT park Chandigarh rajiv gandhi, Chandigarh, Punjab, 160001</span></p>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">Phone no:<span style="color: #919191;padding-left: 8px;">+91-9068321122</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="details" style="margin-top: 20px;">
                                        <div class="section__title" style="font-size: 20px;font-weight: 700;color: #176bea;text-transform: uppercase;margin-bottom: 25px;">LAWSMOS PROFILE DETAILS</div>
                                        <div class="section__list">
                                            <div class="section__list-item">
                                                <div class="left">
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">User Since: <span style="color: #919191;padding-left: 8px;">August, 2021 </span></p>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">User Rating:<span style="color: #919191;padding-left: 8px;">*****</span></p>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">Connections:<span style="color: #919191;padding-left: 8px;">0</span></p>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">City Rank:<span style="color: #919191;padding-left: 8px;">10</span></p>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">Total Consultations: <span style="color: #919191;padding-left: 8px">0</span></p>
                                                    <p class="des-name" style="font-size: 14px;margin-top: 15px;">Lawsmos Score:<span style="color: #919191;padding-left: 8px;">0</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="first-description">
                        <div class="container" style="width: 1170px;margin: 0 auto;padding: 0 15px;">
                            <div class="description" style="margin-top: 25px;">
               
                                <p style="color: #919191;font-size: 14px;">Give your resume a powerful start. Describe your professional
                                    orientation/
                                    objectives as
                                    professionals. Keep it brief under 200 words. </p>
                            </div>
                        </div>
                    </section>
                </div>
                <section class="container" style="width: 1170px;margin: 0 auto;padding: 0 15px;">
                </section>
            </section>
        </body>
    </div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<!--the script-->
<script>
    $('#download_pdf').click(function () {
        var options = {
            //'width': 800,
        };
        var pdf = new jsPDF('l', 'pt', 'a4');
        pdf.addHTML($("#content"), 0, 50, options, function () {
            pdf.save('my_resume.pdf');
        });
    });
    // var doc = new jsPDF();
    // var specialElementHandlers = {
    //     '#editor': function (element, renderer) {
    //         return true;
    //     }
    // };
    // $('#download_pdf').click(function () {
    //     doc.fromHTML($('#content').html(), 15, 15, {
    //         'width': 170,
    //             'elementHandlers': specialElementHandlers
    //     });
    //     doc.save('sample-file.pdf');
    // });
</script>