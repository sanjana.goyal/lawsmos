

<style>
	
/* Style the form */
#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

/* Style the input fields */
input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none; 
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

/* Mark the active step: */
.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #4CAF50;
}

</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>

<form id="regForm" action="<?php  echo site_url('home/studentform'); ?>" method="post">

    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />


<!-- One "tab" for each step in the form: -->

<div class="tab">
	
	
	
	
	<div class="field_wrappersss">
   
   
   <div class="container">
    <div class="row">
		
		

		
		<div class="col-md-7">
			
					<h4>Edication History</h4>
			<div class="form-group">
					
					<input type="text" name="inst[]" class="form-control" placeholder="Institute/University"  />
			</div>
			
			<div class="form-group">
					
					<input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name"  />
			</div>
			<div class="form-group">
					
					<input type="text" name="university[]" class="form-control" placeholder="Enter university name" />
			</div>
			<div class="form-group">
					
					<input type="text" name="grade[]" class="form-control" placeholder="Enter Grade or Percent" />
			</div>
			<div class="form-group">
					
					<div class="row">
					<div class="col-md-6">
					Start Date<input type="date" class="form-control" name="yearstart[]" class="form-control"  />
					</div>
					<div class="col-md-6">
					End Date<input type="date" class="form-control" name="yearend[]"  placeholder="Enter College name"  />
					</div>
					</div>
			</div>
			
			
			
			
			
			   <a href="javascript:void(0);" class="add_qualification btn btn-info btn-xs"><span class="glyphicon glyphicon-plus"></span> Add More </a>
		</div>
		
       
      
   </div>
			
			
		
</div>



</div>
</div>

<div class="tab">
	
	
<div class="intern_wrappersss">

<div class="container">	
			
 <div class="row">
		
		

		
		<div class="col-md-7">
			
					<h4>Previous Internships</h4>
			<div class="form-group">
					
					<input type="text" name="orginst[]" class="form-control" placeholder="Organisation/Institute"  />
			</div>
			
			<div class="form-group">
					
					<input type="text" name="yrtt[]" class="form-control" placeholder="Enter Year"  />
			</div>
		
			
			<div class="form-group">
					<div class="row">
					<div class="col-md-6">
					Start Date<input type="date" class="form-control" name="from[]" class="form-control"  />
					</div>
					<div class="col-md-6">
					End Date<input type="date" class="form-control" name="to[]"    />
					</div>
					</div>
					
			</div>
			
			
			
			
			
			   <a href="javascript:void(0);" class="add_internships btn btn-info btn-xs"><span class="glyphicon glyphicon-plus"></span> Add More </a>
		</div>
		
       
      
   </div>
			
			
		
</div>

</div>

</div>



<div style="overflow:auto;">
  <div style="float:right;">
    <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
    <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
  </div>
</div>

<!-- Circles which indicates the steps of the form: -->
<div style="text-align:center;margin-top:40px;">
  <span class="step"></span>
  <span class="step"></span>
  
</div>

</form>




<script type="text/javascript">
$(document).ready(function(){
    var maxField = 4; //Input fields increment limitation
    var addButton = $('.add_qualification'); //Add button selector
    var wrapper = $('.field_wrappersss'); //Input field wrapper
    
    // <a href="javascript:void(0);" class="remove_qial_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Remove </a>
   
    var fieldHTML='<br><div class="container"><div class="row"><div class="col-md-7"><h4>Edication History</h4><div class="form-group"><input type="text" name="inst[]" class="form-control" placeholder="Institute/University"  /></div><div class="form-group"><input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name"  /></div><div class="form-group"><input type="text" name="university[]" class="form-control" placeholder="Enter university name" /></div><div class="form-group"><input type="text" name="grade[]" class="form-control" placeholder="Enter Grade or Percent" /></div><div class="form-group"><div class="row"><div class="col-md-6">Start Date<input type="date" class="form-control" name="yearstart[]" class="form-control"  /></div><div class="col-md-6">End Date<input type="date" class="form-control" name="yearend[]"  placeholder="Enter College name"  /></div></div></div><a href="javascript:void(0);" class="remove_qial_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Remove </a></div></div></div>';
   
  //  var fieldHTML='<div class="row"><div class="col-md-12"><input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name"  /></div><div class="col-md-12">Start<input type="date" class="form-control" name="yearstart[]" class="form-control"  />End<input type="date" class="form-control" name="yearend[]" class="form-control"   /></div><div class="col-md-12"><input type="text" name="college[]" class="form-control" placeholder="Enter College name"  /><input type="text" name="university[]" class="form-control" placeholder="enter university name"  /><select name="division[]" class="form-control"><option>Select Division</option><option value="goldmedal">Gold Medalist</option><option value="fst">Ist</option><option value="scnd">2nd</option><option value="3rd">3rd</option></select></div><div class="col-md-2"><a href="javascript:void(0);" class="remove_qial_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Remove </a></div>';
    //var fieldHTML='<div class="row"><div class="col-md-6"><input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name" oninput="this.className = ''" /></div><div class="col-md-12">Start<input type="date" class="form-control" name="yearstart[]" class="form-control" oninput="this.className = ''" />End<input type="date" class="form-control" name="yearend[]" class="form-control" oninput="this.className = ''"  /></div><div class="col-md-12"><input type="text" name="college[]" class="form-control" placeholder="Enter College name" oninput="this.className = ''" /><input type="text" name="university[]" class="form-control" placeholder="enter university name" oninput="this.className = ''" /><select name="division[]" class="form-control"><option>Select Division</option><option value="goldmedal">Gold Medalist</option><option value="fst">Ist</option><option value="scnd">2nd</option><option value="3rd">3rd</option></select></div><div class="col-md-2"><a href="javascript:void(0);" class="add_qualification" title="Add Courts"> <span class="glyphicon glyphicon-plus"></span></a></div>';
     var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_qial_button', function(e){
        e.preventDefault();
        //$(this).parent("div").remove(); //Remove field html
        
        $(this).parent("div").parent("div").remove();
        x--; //Decrement field counter
    });
});



$(document).ready(function(){
    var maxField = 4; //Input fields increment limitation
    var addButton = $('.add_internships'); //Add button selector
    var wrapper = $('.intern_wrappersss'); //Input field wrapper
    
    // <a href="javascript:void(0);" class="remove_qial_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Remove </a>
   
    var fieldHTML='<br><div class="container"><div class="row"><div class="col-md-7"><h4>Previous Internships</h4><div class="form-group"><input type="text" name="orginst[]" class="form-control" placeholder="Organisation/Institute"  /></div><div class="form-group"><input type="text" name="yrtt[]" class="form-control" placeholder="Enter Year"  /></div><div class="form-group"><div class="row"><div class="col-md-6">Start Date<input type="date" class="form-control" name="from[]" class="form-control"  /></div><div class="col-md-6">End Date<input type="date" class="form-control" name="to[]"    /></div></div></div><a href="javascript:void(0);" class="remove_intern_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Remove </a></div></div></div>';
   
  //  var fieldHTML='<div class="row"><div class="col-md-12"><input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name"  /></div><div class="col-md-12">Start<input type="date" class="form-control" name="yearstart[]" class="form-control"  />End<input type="date" class="form-control" name="yearend[]" class="form-control"   /></div><div class="col-md-12"><input type="text" name="college[]" class="form-control" placeholder="Enter College name"  /><input type="text" name="university[]" class="form-control" placeholder="enter university name"  /><select name="division[]" class="form-control"><option>Select Division</option><option value="goldmedal">Gold Medalist</option><option value="fst">Ist</option><option value="scnd">2nd</option><option value="3rd">3rd</option></select></div><div class="col-md-2"><a href="javascript:void(0);" class="remove_qial_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Remove </a></div>';
    //var fieldHTML='<div class="row"><div class="col-md-6"><input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name" oninput="this.className = ''" /></div><div class="col-md-12">Start<input type="date" class="form-control" name="yearstart[]" class="form-control" oninput="this.className = ''" />End<input type="date" class="form-control" name="yearend[]" class="form-control" oninput="this.className = ''"  /></div><div class="col-md-12"><input type="text" name="college[]" class="form-control" placeholder="Enter College name" oninput="this.className = ''" /><input type="text" name="university[]" class="form-control" placeholder="enter university name" oninput="this.className = ''" /><select name="division[]" class="form-control"><option>Select Division</option><option value="goldmedal">Gold Medalist</option><option value="fst">Ist</option><option value="scnd">2nd</option><option value="3rd">3rd</option></select></div><div class="col-md-2"><a href="javascript:void(0);" class="add_qualification" title="Add Courts"> <span class="glyphicon glyphicon-plus"></span></a></div>';
     var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_intern_button', function(e){
        e.preventDefault();
        //$(this).parent("div").remove(); //Remove field html
        
        $(this).parent("div").parent("div").remove();
        x--; //Decrement field counter
    });
});
</script>
<script>

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
</script>

