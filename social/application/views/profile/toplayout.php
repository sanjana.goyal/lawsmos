 <?php
    if (strpos($_SERVER['REQUEST_URI'], "friends") !== false){
        $flag = 'F';
    }elseif (strpos($_SERVER['REQUEST_URI'], "albums") !== false) {
       $flag = 'A';
    }elseif (strpos($_SERVER['REQUEST_URI'], "my_resume") !== false) {
       $flag = 'R';
    }else{
         $flag = 'P';
    }
 ?>

 <div class="profile-header profile-banner" style="background: url(<?php echo base_url() . $this->settings->info->upload_path_relative . "/" . $user->profile_header ?>) center center; background-size: cover;">
            <div class="profile-header-avatar">
                <img src="<?php echo base_url() . $this->settings->info->upload_path_relative . "/" . $user->avatar ?>">
            </div>
            <div class="profile-header-name">
                <?php
                echo $user->first_name . $user->last_name;
                if ($user->verified) :
                ?>
                    <img src="<?php echo base_url() ?>images/verified_badge.png" width="14" data-placement="top" data-toggle="tooltip" title="<?php echo lang("ctn_695") ?>">
                <?php endif; ?>
            </div>
        </div>
        <div class="profile-header-bar clearfix">
            <ul>
                <li class="<?php if ($flag== 'P'){echo "active";} ?>"><a href="<?php echo site_url("profile/" . $user->username) ?>"><?php echo lang("ctn_880") ?></a></li>
                 <?php
                 if ($user->profile_identification==0) :
                ?>
                <li class="<?php if ($flag== 'R'){echo "active";} ?>"><a href="<?php echo site_url("my_resume") ?>">Resume</a></li>
                 <?php
                 endif
                ?>
                <li class="<?php if ($flag== 'A'){echo "active";} ?>"><a href="<?php echo site_url("profile/albums/" . $user->ID) ?>"><?php echo lang("ctn_483") ?></a></li>
                <li class="<?php if ($flag== 'F'){echo "active";} ?>"><a  href="<?php echo site_url("profile/friends/" . $user->ID) ?>"><?php echo lang("ctn_493") ?></a></li>
                <li><a href="javascript:void(0)">Cases</a></li>
                <li><a href="javascript:void(0)">Research</a></li>
                <li><a href="javascript:void(0)">Articles</a></li>
                <li><a href="javascript:void(0)">Headnotes</a></li>
                <li><a href="javascript:void(0)">Q&A</a></li>
            </ul>
            <div class="pull-right profile-friend-box">
                <button type="button" class="btn btn-default btn-sm flag-profile" data-toggle="modal" data-target="#reportModal" title="<?php echo lang("ctn_578") ?>"><span class="glyphicon glyphicon-flag"></span></button>
            </div>
            <?php
            if ($this->user->loggedin && $this->user->info->ID == $user->ID && $page != "view_as_public") {
            ?>
                <div class="pull-right profile-friend-box">
                    <a type="button" class="btn btn-default btn-sm" href="<?php echo site_url("profile/" . $user->username . "?page=view_as_public") ?>" style="padding-bottom:6px;" target="_blank">View as Public</a>
                </div>
            <?php
            }
            ?>

        </div>

       

