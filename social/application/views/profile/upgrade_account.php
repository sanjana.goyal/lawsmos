



<?php $this->load->view('sidebar/sidebar.php'); ?>

<form id="regForm" action="<?php  echo site_url('home/uploadproof'); ?>" method="post" enctype="multipart/form-data">

    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />


<div class="container">
	<div class="row">
	
		<div class="col-md-12">
				<h4>Please Upload any verification proof in PDF format</h4>
				
				
				<br>
				
				<div class="form-group">
				
					<label for="uploadproof">Upload Verification Proof</label>
					
					<input type="file" name="userfile" class="form-control" required />
				
				
				</div>
			
			
				<div class="form-group">
				
					<h1>Disclaimer</h1>

<p>Last updated: July 16, 2019</p>
<?php
	$base_tmp_url = str_replace("/social","",base_url());
?>

<p>The information contained on <?php echo $base_tmp_url; ?> website (the "Service") is for general information purposes only.</p>

<p>LawyersIND assumes no responsibility for errors or omissions in the contents on the Service.</p>

<p>In no event shall LawyersIND be liable for any special, direct, indirect, consequential, or incidental damages or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in connection with the use of the Service or the contents of the Service. LawyersIND reserves the right to make additions, deletions, or modification to the contents on the Service at any time without prior notice. This Disclaimer  for LawyersIND has been created with the help of <a href="https://www.termsfeed.com/">TermsFeed</a>.</p>

<p>LawyersIND does not warrant that the website is free of viruses or other harmful components.</p>
					<input type="checkbox" name="disclain" value="1" required />
				
				
				</div>
			
			<button type="submit" class="btn btn-danger">Upload Proof</button>
		</div>


	</div>
</div>

</form>
