<style>
    .user-picture .offline-dot-user {
        background:none; 
        width: 0px; 
        height: 0px;
      
        border: 1px solid #FFF;
        position: absolute;
        bottom: 0px;
        left: 0px;
    }
    .user-picture .user-box-avatar img {
        display: inline-block;
        width: 200px;
        height: 224px;
        border-radius: 0px !important;
      
    }

    .previous_appointments {
        float: left;
        padding: 10px;
    }
    .fa {
      font-size: 25px;
    }

    .checked {
      color: orange;
    }

    .post-action{
      margin-top: 15px;
      margin-bottom: 15px;
    }
    h1{
        margin-top:100px;
    }
    @media(max-width:767px){
        h1{
            margin-top:50px;
            font-size:22px;
        }
        .container{
                margin-left: 15px;
                margin-right: -15px;
        }
    }
</style>
<!-- <link href='<?= base_url() ?>bootstrap-star-rating/css/star-rating.min.css' type='text/css' rel='stylesheet'>
<script src='<?= base_url() ?>bootstrap-star-rating/js/star-rating.min.js' type='text/javascript'></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/js/star-rating.min.js" ></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/css/star-rating.min.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
<div class="container text-center">
    <h1>Rate and Review lawyers you met!</h1>
    <?php 
			if(!empty($appointments)):
				foreach($appointments as $appo): ?>
					<div class="previous_appointments">
						<div class="user-picture">
							<?php echo $this->common->get_user_display(array("username" => $appo['userdata']['username'], "avatar" => $appo['userdata']['avatar'], "online_timestamp" => $appo['userdata']['online_timestamp'])); ?>
						</div>
						<div class="user-content">
								<b>Name</b> : <?php echo $appo['userdata']['username']; ?>		
						</div>
						<div class="post-action">
				  <!-- Rating Bar -->
				  
				    <input id="lawyer_<?= $appo['userdata']['service_provider_id'] ?>_<?= $appo['userdata']['service_seeker_id'] ?>" value='<?= $appo['userrating']; ?>' class="ratingbar" data-min="0" data-max="5" data-step="1">
				 <!-- Average Rating -->
				<!-- <div>Average Rating: <span id='averagerating_<?= $appo['userdata']['service_provider_id'] ?>'><?= $appo['averageRating']; ?></span></div> -->
				
				<?php if(!empty($appo['userComment'])): ?>
				<div>Your Review:  <span><?php echo $appo['userComment']; ?></span></div>
				<?php endif; ?>
				<div class="post_review">
					<textarea class="form-control" id="post_review_<?= $appo['userdata']['service_provider_id'] ?>" required ></textarea>
				</div>
				<button class="btn btn-danger" id="lawyer_<?= $appo['userdata']['service_provider_id'] ?>_<?= $appo['userdata']['service_seeker_id'] ?>" class="submitbutton">Post Review</button>
        </div>
			</div>		
				<?php endforeach;
					else:
					echo "No Records Available!";
					endif;
				 ?>
			
</div>
</div>
<script>
$(document).ready(function(){

$('.ratingbar').rating({
        showCaption:true,
        showClear: false,
        size: 'sm'
      });
 $("button").click(function(){
		var id = this.id;
        var splitid = id.split('_');
        var lawyerid = splitid[1];
        var seekerid = splitid[2];
        var reviewtext=$('textarea#post_review_'+lawyerid).val();
		if(!(reviewtext=="" || reviewtext==null)){
			$.ajax({
			  url: '<?= base_url() ?>ratings/updateReview',
			  type: 'GET',
			  data: {lawyerid:lawyerid,seekerid:seekerid,reviewtext:reviewtext},
			  success: function(response){
				  
				  location.reload();
				 
			  }
			});
      }
	 });
 // Rating change
  $('.ratingbar').on('rating:change', function(event, value, caption) {
		var id = this.id;
        var splitid = id.split('_');
        var lawyerid = splitid[1];
        var seekerid = splitid[2];
        $.ajax({
          url: '<?= base_url() ?>ratings/updateRating',
          type: 'GET',
          data: {lawyerid:lawyerid,seekerid:seekerid,rating:value},
          success: function(response){
             $('#averagerating_'+lawyerid).text(response);
          }
        });
  });
/*
 load_data();

 function load_data()
 {
  $.ajax({
   url:"<?php echo base_url(); ?>ratings/fetch",
   method:"GET",
   success:function(data)
   {
    $('#lawyers_list').html(data);
   }
  })
 }
 
 
 $(document).on('mouseenter', '.rating', function(){
  var index = $(this).data('index');
  var business_id = $(this).data('user_id');
  remove_background(business_id);
  for(var count = 1; count <= index; count++)
  {
   $('#'+business_id+'-'+count).css('color', '#ffcc00');
  }
 });
 
 function remove_background(business_id)
 {
  for(var count = 1; count <= 5; count++)
  {
   $('#'+business_id+'-'+count).css('color', '#ccc');
  }
 }
 
 
  $(document).on('click', '.rating', function(){
  var index = $(this).data('index');
  var lawyer_id = $(this).data('user_id');
  var seeker_id = $(this).data('seeker_id');
  var rating = $(this).data('rating');
  $.ajax({
   url:"<?php echo base_url(); ?>ratings/insert_rating/",
   method:"GET",
   data:{rating:index, lawyerid:lawyer_id,seeker_id:seeker_id},
   success:function(data)
   {
    load_data();
    alert("You have rate "+index +" out of 5");
   }
  })
  */
 });

</script>
