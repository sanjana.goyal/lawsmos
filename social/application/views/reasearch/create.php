<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
</link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<style>
  .error {
    color: red;
  }

  .result-draft {

    height: 1100px;
    overflow: scroll;
  }

  /* width */
  ::-webkit-scrollbar {
    width: 20px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: black;

  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #black;
  }

  div#judgements-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

  }

  div#bareacts-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

  }

  div#article-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

  }

  ul.bareacts-list li,
  div#result li,
  .backend-text ul li,
  .result-draft li {
    list-style: none;
    margin-bottom: 14px;
    line-height: 28px;
    color: #666;
    margin-right: 30px;
    padding-left: 16px;
  }

  li {
    display: list-item;
    text-align: -webkit-match-parent;
  }
</style>

<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content margin_left margin_repo separator page-right">
    <div class="containerr">
        <div class="row">
<div class="col-md-5 re-textarea">
  <?php echo $this->session->flashdata('success'); ?>
  <?php echo $this->session->flashdata('error'); ?>
  <h3>Reasearch Builder</h3>
  <!-- <iframe src="<?php echo site_url('case_listings/researchbuild'); ?>"  width="100%" height="1200px" style="border:none;"></iframe> -->
  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
  <div class="form-group">
    <label for="case_heading">Heading</label>
    <input type="text" name="case_heading" id="case_heading" placeholder="Research Heading" class="form-control" />
    <p class="case-heading-error-info error"></p>
  </div>
  <div class="form-group">
    <label for="catg">Catagory</label>
    <select name="catg" id="catg" class="form-control">
      <option value="">Select catagory</option>
      <?php foreach ($cat as $catg) { ?>
        <option value="<?php echo $catg->cid; ?>"><?php echo $catg->c_name; ?></option>
      <?php } ?>
    </select>
    <p class="cat-error-info error"></p>
  </div>
  <div class="form-group">
    <label for="subcatg">Sub category</label>
    <select name="subcat" id="subcatg" class="form-control">
      <option value="">Select Subcatagory</option>
    </select>
    <p class="subcat-error-info error"></p>
  </div>
  <div class="form-group">
   <label for="editor1">Description</label>
    <textarea name="editor1" id="editor1" class="form-control"></textarea>
    <!-- <textarea name="editor1" class="ckeditor" id="ck" style="height:1500px;"></textarea> -->
    <p class="editor1-error-info error"></p>
    <br>
  </div>
  <button type="button" class="btn  profile-set-btn" id="submitBtn">Create Research</button>
  <div id="content-container"></div>
</div>
<div class="col-md-7 re-short case-recol">
  <h3>Reference links</h3>
  <!-- <iframe src="<?php echo site_url('case_listings/searchResearch'); ?>"  width="100%" height="600px" style="border:none;"></iframe> -->
  <iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe'); ?>" width="100%" height="1300px" style="border:none;"></iframe>
</div>
</div>
</div>
</div>

<?php
$tmp_url = str_replace("/social", "", base_url());
?>







<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>



<script>
  $('.reference').load(function() {
    $('.reference').contents().find('.header-area-wrapper').hide();
    $('.reference').contents().find('.footer-area-wrapper').hide();

  });
</script>


<script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>


<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'csrftoken': '{{ csrf_token() }}'
    }
  });
</script>

<?php
$tmp_url = str_replace("/social", "", base_url());
?>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#catg').on("change", function() {
      var categoryId = $(this).find('option:selected').val();

      $.ajax({
        url: '<?php echo site_url('case_listings/subcatg') ?>',
        type: "GET",
        data: "categoryId=" + categoryId,
        success: function(response) {
          $("#subcatg").html(response);
        },
      });
    });

  });
</script>

<script>
  $(document).ready(function() {

    $('#submitBtn').click(function(e) {

      

      $(".case-heading-error-info").html("");
      $(".cat-error-info").html("");
      $(".subcat-error-info").html("");
      $(".editor1-error-info").html("");
      var case_heading = $('#case_heading').val();
      var catg = $('#catg').children("option:selected").val();
      var subcatg = $("#subcatg").children("option:selected").val();
      var editor1 = $('#editor1').val();

      var error = false;
      if (case_heading == "") {
        error = true;
        $(".case-heading-error-info").html("This field is required");
      }
      if (catg == "") {
        error = true;
        $(".cat-error-info").html("This field is required");
      }
      if (subcatg == "") {
        error = true;
        $(".subcat-error-info").html("This field is required");
      }
      if (editor1 == "") {
        error = true;
        $(".editor1-error-info").html("This field is required");
      }
      if (error == true) {
        return false;
      }
      e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({

        //   url: '<?php echo site_url('Case_editor/createreasearch') ?>',
        url: global_base_url + "Case_editor/createreasearch",
        type: 'POST',
        data: {
          case_heading: case_heading,
          catg: catg,
          subcatg: subcatg,
          editor1: editor1,
          '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
        },
        dataType: "json",
        success: function(response) {
          // console.log(response.message);
          // var result = json.stringify(response);
          if (response.message == 'success') {
            console.log("success", response);
            window.location.href = response.url;
          } else {
            console.log("failed", response);
            $('#content-container').html("Something went wrong");
          }
        }
      });
    });

  });
</script>
<script>
  $(document).ready(function() {
    // alert('goooo');
    setTimeout(function() {
      $('.alert-success').fadeOut(1000);
    }, 8000);
  });
  </script>