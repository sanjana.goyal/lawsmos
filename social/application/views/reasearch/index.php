
  <?php $this->load->view('sidebar/sidebar.php'); ?>

<div class="row page-right view-height">
<div class="col-xs-12">
 <?php echo $this->session->flashdata('success'); ?>
 <?php echo $this->session->flashdata('error'); ?>	
 <h3>My Research</h3>
 <table class="table research_table table-striped table-hover table-bordered dataTable no-footer" role="grid" aria-describedby="friends-table_info">
	<tr>
		<th>Reaserch Heading</th>
		<th>Reaserch Catagory</th>
		<th>Reaserch Created at</th>
	    <th>Last Updated</th>
	    <th>Options</th>
	</tr>
 <?php 
	 if (!empty($researchdata)) {
		foreach ($researchdata as $row) {
		 $heading=$row->reasearch_heading;
		 $body=html_entity_decode($row->research_body	);
		 $createdat=$row->created_at;
		 $last_update=$row->updated_at;
		 $row_id=$row->ID;
		?>
			<tr>
				<td><a href="<?php echo site_url(); ?>case_editor/research_editor?id=<?php echo $row_id; ?>"><?php echo $heading; ?></a></td>
				<td><a href=""><?php echo $row->c_name; ?></a></td>
				<td><a href=""><?php echo $row->created_at; ?></a></td>
				<td><a href="#"><?php echo $last_update; ?></a></td>
				<td>
					<a href="<?php echo site_url(); ?>case_editor/research_editor?id=<?php echo $row_id; ?>" title="Edit Research"><span class="glyphicon glyphicon-edit"></span></a>
					<a href="<?php echo site_url(); ?>case_listings/deleteresearch/<?php echo $row_id; ?>" title="Delete Research"><span class="glyphicon glyphicon-remove-circle"></span></a>
				</td>
			</tr>
	   <?php
		}
	}
	else{	
		?>
		<tr>
			<td><?php echo "No Reaseach Available";?></td>
		</tr>
		<?php
		}
	?>
</table>
    <div class="rebtn">
    <!-- <a href="<?php //echo site_url('case_listings/createreasearch');  ?>" class="btn btn-primary re-btn create_cases_new">Create New Research</a> -->
        <a href="<?php echo site_url('case_listings/create'); ?>" class="btn btn-primary btn-sm create_cases_new">Create New Research</a>
        <input type="hidden" name="page_name" id="page_name" value="research">


    </div>
    
</div>

 </div>
 <?php 
 if(isset($_POST['editor'])){
	echo $_POST['editor'];
 }
 ?>


<script>
  $(document).ready(function() {
    // alert('goooo');
    setTimeout(function(){ 
        $('.alert-success').fadeOut(1000);        
       }, 5000);
});
  </script>