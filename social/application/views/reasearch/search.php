<style>

div#responsive-menu-links {
    display: none;
}

div#footer {
    display: none;
}

nav.navbar.navbar-inverse.navbar-header2 {
    display: none;
}


ul#ui-id-1 {
    width: 292px;
    border-right: 2px solid dodgerblue;
}



div#cke_1_contents {
    height: 1500px;
}


#main-content {
   
    border-right: none !important;
}
   .header-area-wrapper {
 
    display: none;
}
</style>
 <div class="case-ifram-wraper">
			<div class="form-group">
			
				
											<a href="javascript:void(0);" class="btn btn-danger btn-xs" id="judgements">Judgments</a>
											<a href="javascript:void(0);" class="btn btn-primary  btn-xs" id="bareacts">BareActs</a>
											<a href="javascript:void(0);" class="btn btn-info  btn-xs" id="maxims">Maxims</a>
											<a href="javascript:void(0);" class="btn btn-success  btn-xs" id="articles">Articles</a>
			
			
			</div>
			<img src=<?php echo base_url('images/ajax-loading-icon-19.jpg'); ?> alt='loading..' title='loading..' id="loading-image" width="50px"  /><br>
			<?php //echo base_url(); die(); ?>
				<div class="form-group" id="common-db">
									<p>Refine Search Judgements by Year and Court Name</p>
									
									<select name="yearcity" id="year" class="form-control">
										<option value="">Select Year</option>
										<?php foreach($year as $dtd){ ?>
										<option value="<?php echo $dtd->year; ?>"><?php echo $dtd->year; ?></option>
										<?php } ?>
									</select>
									<select name="courtname" id="courtname" class="form-control">
										<option value="">Select Court</option>
										<?php foreach($courts as $dtd){ ?>
										<option value="<?php echo $dtd->state; ?>"><?php echo $dtd->state; ?></option>
										<?php } ?>
									</select>
                                    <select name="catagory" id="catagory" class="form-control">
										<option value="">Select Catagory</option>
										<?php foreach($cats as $dtd){ ?>
										<option value="<?php echo $dtd->cid; ?>"><?php echo $dtd->c_name; ?></option>
										<?php } ?>
									</select>
										
								</div>
			
								<div class="form-group">
										
											<input type="text" id="search-judgements"  placeholder="Search judgments" class="form-control"/>
										</div>
										<div class="form-group">
										
											<input type="text" id="search-bareacts"  placeholder="Search BareActs" class="form-control"/>
										</div>
										
										
										<div class="form-group">
										
											<input type="text" id="search-articles" name="Search_links" placeholder="Search Articles" class="form-control"/>
										</div>
	
 
<div name="iframe_a" id="iframe2ef" class="result-draft-soc"></div>	
<div name="iframe_b" id="iframe3ef" class="result-draft-soc"></div>			
<div id="maximsss"></div>
<div name="iframe_d" id="iframe5ef" class="result-draft-soc"></div>
<div name="iframe_c" id="iframe4ef" class="result-draft-soc">
		@foreach($maxims as $maxims)
                    <div class="list_blockk">
								<h4>{{$maxims->term}}</h4>
								<p>{{strip_tags($maxims->definition)}}</p>
                        </div>
					  @endforeach
	</div>
</div>
 <!--
 <iframe name="iframe_a" src="" id="iframe2ef"  style="border:none;" scrolling="no" ></iframe>
 
 <iframe name="iframe_b" src="" id="iframe3ef"  style="border:none;" scrolling="no"></iframe>
 
 <iframe name="iframe_c" src="" id="iframe4ef"  style="border:none;" scrolling="no"></iframe>
 -->
 

<?php echo $this->session->flashdata('success'); ?>
 
 

<?php echo $this->session->flashdata('error'); ?>	

<?php
	$tmp_url = str_replace("/social","",base_url());
?>
 
 
 
 <script>
	
	$(document).ready(function(){
		 $('#loading-image').hide();
			$("#search-bareacts").hide();
			$("#search-articles").hide();
			$("#search-judgements").show();
			$("#iframe2ef").hide();
			$("#iframe3ef").hide();
			$("#iframe4ef").hide();
			$("#iframe5ef").hide();
			
			var catagory;
			var year;
			var courtname;
			
			
	$("select#catagory").change(function(){
		
					  catagory=$(this).children("option:selected").val();
		
		});			
		$("select#year").change(function(){
		
					  year=$(this).children("option:selected").val();
		
		});			
		$("select#courtname").change(function(){
		
					  courtname=$(this).children("option:selected").val();
		
		});			
			
			
	  $('#search-judgements').on('keyup',function(){
				
					//alert(court);
					
					var value=$(this).val();
					
					//alert(value);
					
					if (!this.value) {
						   
						   $('#iframe2ef').hide();
						}
					else{
					$.ajax({
					type : 'get',
					async: true,
					url : "<?php echo tmp_url().'searchhighcourt' ?>",
				
					data:{'court': courtname,'year':year,'catagory':catagory,'search':value},
					beforeSend: function(){
							$('#loading-image').show();
					},
					success:function(data){
					
					if(data!="" || data!=null){
					$("#iframe2ef").html(data);
					$('#iframe2ef').show();
					}else
					{
					$("#iframe2ef").html("No records found...");
					$('#iframe2ef').show();
					}
	
					},
                     complete: function(){
							$('#loading-image').hide();
							
					}
					});

				}


			});
			
			$('#search-judgements').on('keydown',function(){
	
						var value=$(this).val();
						if(value=="" || value==null)
						{
							$('#iframe2ef').hide();
						}
			});
		
	 $("#judgements").click(function(){
		
			$("#search-bareacts").hide();
			$("#search-articles").hide();
			$("#search-judgements").show();
			$("#common-db").show();
			$('#iframe2ef').hide();
			$('#iframe3ef').hide();
			$('#iframe4ef').hide();
			$('#iframe5ef').hide();
			$("#maximsss").hide();
			
		$("select#catagory").change(function(){
		
					  catagory=$(this).children("option:selected").val();
		
		});			
		$("select#year").change(function(){
		
					  year=$(this).children("option:selected").val();
		
		});			
		$("select#courtname").change(function(){
		
					  courtname=$(this).children("option:selected").val();
		
		});			
		
		$('#search-judgements').on('keyup',function(){
	
					//alert(court);
					
					var value=$(this).val();
					
					//alert(value);
					
					if (!this.value) {
						   
						   $('#iframe2ef').hide();
						}
					else{
					$.ajax({
					type : 'get',
					async: true,
					url : "<?php echo tmp_url().'searchhighcourt' ?>",
				
					data:{'court': courtname,'year':year,'catagory':catagory,'search':value},
					beforeSend: function(){
							$('#loading-image').show();
					},
					success:function(data){
					
					if(data!="" || data!=null){
						$("#iframe2ef").html(data);
						$("#iframe2ef").show();
					}else
					{
						$("#iframe2ef").html("No records found...");
						$("#iframe2ef").show();
					}
					},
                     complete: function(){
							$('#loading-image').hide();
							
					}
					});

				}


			});
			
			$('#search-judgements').on('keydown',function(){
	
						var value=$(this).val();
						if(value=="" || value==null)
						{
							$('#iframe2ef').hide();
						}
			});
		
		});

//search bareacts

$("#bareacts").click(function(){
		
			$("#search-bareacts").show();
			$("#search-articles").hide();
			$("#search-judgements").hide();
			$("#common-db").hide();
			$('#iframe2ef').hide();
			$('#iframe3ef').hide();
			$('#iframe4ef').hide();
			$("#maximsss").hide();
			$('#iframe5ef').hide();
			
			$('#search-bareacts').on('keyup',function(){
	

					var value=$(this).val();
					
					//alert(value);
					
					if (!this.value) {
						   
						   $('#iframe2ef').hide();
						   $('#iframe3ef').hide();
						}
					else{
					$.ajax({
					type : 'get',
					async: true,
					url : "<?php echo tmp_url().'searchacts' ?>",,

					data:{'search': value},
					beforeSend: function(){
							$('#loading-image').show();
					},
					success:function(data){
					$("#iframe3ef").html(data);
					 
					$('#iframe3ef').show();

					},
                     complete: function(){
							$('#loading-image').hide();
							
					}
					});

				}


			});
			
			$('#search-bareacts').on('keydown',function(){
	
						var value=$(this).val();
						if(value=="" || value==null)
						{
							$('#iframe3ef').hide();
						}
			});
		
		});

//search maxims
 $('#maxims').click(function(){
		
			$("#search-bareacts").hide();
			$("#search-articles").hide();
			$("#search-judgements").hide();
			$("#common-db").hide();
			$('#iframe2ef').hide();
			$('#iframe3ef').hide();
			$('#iframe4ef').hide();
			$("#maximsss").show();
			$('#iframe5ef').hide();
		
		
		   $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/maxims",
	             dataType: 'JSON',
	             success: function (msg) {
	                // response(msg);
	                 
	                $.each(msg, function(k, v){
						
						//$("#maxmx").text(v.label);
						
					//	$('<li class="clearfix search-option-page">')
		                  //  .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>')
		                   // .appendTo(ul);
						
		                 $('#maximsss').append("<li>"+v.label+"<span>'"+v.definitin+"'</span></li>"); 
		               //  $('#maxims').append("<div class='search-user-info'><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>");
		                 
		                 
						 console.log(v.label);
						 
					
					});
	                 
	             }
	         });
		
		});	
		

//artcles

$("#articles").click(function(){
		
			$("#search-bareacts").hide();
			$("#search-articles").hide();
			$("#search-judgements").hide();
			$("#search-articles").show();
			$("#common-db").hide();
			$('#iframe2ef').hide();
			$('#iframe3ef').hide();
			$('#iframe4ef').hide();
			$('#iframe5ef').show();
			$("#maximsss").hide();
			
		
			$('#search-articles').on('keyup',function(){
	

					var value=$(this).val();
					
				
					
					if (!this.value) {
						   
						   $('#iframe2ef').hide();
						   $('#iframe3ef').hide();
						   $('#iframe4ef').hide();
						   $('#iframe5ef').hide();
						 
						}
					else{
					$.ajax({
					type : 'get',
					async: true,
					url : "<?php echo tmp_url().'searcharticles' ?>",

					data:{'search': value},
					success:function(data){
						
					
					$("#iframe5ef").html(data);
					 
					$('#iframe5ef').show();

					

					}
					});

				}


			});
		
		});
		
	});
	</script>
 
 
 
 <script>	
	 
	 
	 
	/*	
	$(document).ready(function(){
		
	
		
		
		
		
$("#anchor").trigger("click");

		$("#search-article").show();
		$("#search-BareActs").hide();
		$("#search-maxims").hide();
		$("#search-judgments").hide();
		$("#maximsss").hide();
		$("#common-db").show();
		
		
		$("#article").click(function(){
			
		$("#search-article").show();
		$("#search-BareActs").hide();
		$("#search-maxims").hide();
		$("#search-judgments").hide();
		$("#common-db").show();
		$("#maximsss").hide();
			
			});
		$("#whereacts").click(function(){
			
				$("#search-article").hide();
				$("#search-BareActs").show();
				$("#search-maxims").hide();
				$("#search-judgments").hide();
				$("#maximsss").hide();
				$("#common-db").hide();
			
			});
		
		$("#maxims").click(function(){
			
			$("#search-article").hide();
			$("#search-BareActs").hide();
			$("#search-maxims").show();
			$("#search-judgments").hide();
			$("#common-db").hide();
			$("#maximsss").show();
			
		});


	$("#judgment").click(function(){
			
			$("#search-article").hide();
			$("#search-BareActs").hide();
			$("#search-maxims").hide();
			$("#search-judgments").show();
			$("#common-db").hide();
			$("#maximsss").hide();
			
	});

	});
 */
 </script>



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
        
   
 
<script type="text/javascript">
   
   /*
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get('case_listings/ajaxpro', { query: query }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
        }
    });
   */ 
   
   /*
	   
	    $("#courtname").on('change',function(){
			  
			  
			  var year=$("#yearcity").val();
			  
			   var court=$("#courtname").val();
			  
			  
			 
	
			
	  
	   
	   
    $('#search-article').autocomplete({
		
		
		
		
		
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/ajaxpro",
	             data: {
	             		query : request.term,
	             		yearfilter: year,
	             		court: court,
	             },
	             dataType: 'JSON',
	             success: function (msg) {
					 if (msg.length > 0) {
						response(msg);
					 } else {
						 response([{ label: 'No results found.', val: -1}]);
					 }
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'"  target="iframe_a">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a  href="'+item.url+'" target="iframe_a">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });
	  
	  
	   });
	   $('#search-BareActs').autocomplete({
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/bareacts",
	             data: {
	             		query : request.term
	             },
	             dataType: 'JSON',
	             success: function (msg) {
	                 response(msg);
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_b">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_b">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });
	  
	  
	  
	    $('#search-maxims').autocomplete({
	  	delay : 300,
	  	minLength: 2,
	  	source: function (request, response) {
	         $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/maxims",
	             data: {
	             		query : request.term
	             },
	             dataType: 'JSON',
	             success: function (msg) {
	                 response(msg);
	             }
	         });
	      },
        	focus: function( event, ui ) {
		        $(this).val( ui.item.label );
		        return false;
		    },
		    create: function () {
	            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	            	if(item.type == "user") {
		                return $('<li class="clearfix search-option-user">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                } 
	                else if(item.type == "page") {
	                	return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>')
		                    .appendTo(ul);
	                }
	            };
	        }
	  });
	  
	  
	   $(document).ready(function(){
		  
		  $('#maxims').click(function(){
		
		
		
		
		   $.ajax({
	             type: "GET",
	             url: global_base_url + "case_listings/maxims",
	             dataType: 'JSON',
	             success: function (msg) {
	                // response(msg);
	                 
	                $.each(msg, function(k, v){
						
						//$("#maxmx").text(v.label);
						
					//	$('<li class="clearfix search-option-page">')
		                  //  .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>')
		                   // .appendTo(ul);
						
		                 $('#maximsss').append("<li>"+v.label+"<span>'"+v.definitin+"'</span></li>"); 
		               //  $('#maxims').append("<div class='search-user-info'><a href="'+item.url+'" target="iframe_c">' + item.label + '</a></div>");
		                 
		                 
						 console.log(v.label);
						 
					
					});
	                 
	             }
	         });
		
		});	
		
		  $("#editor1").css("height","600");
		  $("#iframe2ef").css("display","none");
		  
		  $("#iframe3ef").css("display","none");
		   $("#iframe4ef").css("display","none");
		  
		  
		  $("#search-article").on('change',function(){
			  
			  
			  $("#iframe2ef").css("display","block").css("height","800").css("border","none").css("scrolling","no");
			   $("#iframe3ef").css("display","none");
			   $("#iframe4ef").css("display","none");
			  });
			  
			$("#search-BareActs").on('change',function(){
			  
			  
			  $("#iframe3ef").css("display","block").css("height","800");
			   $("#iframe2ef").css("display","none");
			   $("#iframe4ef").css("display","none");
			  });
		  
		  $("#search-maxims").on('change',function(){
			  
			  
			  $("#iframe4ef").css("display","block").css("height","800");
			   $("#iframe2ef").css("display","none");
			   $("#iframe3ef").css("display","none");
			  });
			  
			  
			$("#year").on('change',function(){
				
					$("#form1").submit();
					
				
				});
		  
		  });
*/
</script>


<!-- return $('<li class="clearfix search-option-page">')
		                    .append('<div class="search-user-info"><a  href="'+item.url+'" target="iframe_a">' + item.label + '</a></div>')
		                    .appendTo(ul); -->
