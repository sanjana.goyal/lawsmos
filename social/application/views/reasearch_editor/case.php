<style>
	div#responsive-menu-links {
		display: none;
	}

	div#footer {
		display: none;
	}

	nav.navbar.navbar-inverse.navbar-header2 {
		display: none;
	}
</style>
<script type="text/javascript" src="<?php echo base_url('ckeditor/ckeditor.js'); ?>"></script>
<?php echo $this->session->flashdata('success'); ?>
<?php echo $this->session->flashdata('error'); ?>	
 <form method="post" action="<?php echo site_url('Case_editor/savereasech'); ?>" target="_top">
		 <?php 
		  if (!empty($query)) {
		  foreach ($query as $row) { 
			$heading=$row->reasearch_heading;
		 ?>
		 <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
		 <input type="hidden" name="caseid" value="<?php echo $row->ID;  ?>"/>
		 <div class="form-group">
		 <input type="text" name="case_heading" placeholder="Case Heading" class="form-control" value="<?php if(isset($heading)){ echo $heading; } ?>"/>
		</div>
		<div class="form-group">
		 <label for="catagory">Catagory</label>
		 <input type="text" name="catag" placeholder="Case Heading" class="form-control" value="<?php echo $row->c_name; ?>" readonly />
		</div>
		<?php if(!empty($subcat)){ ?>
		<div class="form-group">
			<label for="catagory">Sub Catagory</label>
			<input type="text" value="<?php echo $subcat[0]->c_name ?>" readonly class="form-control"/>
		</div>
		<?php }?>
		<div class="form-group">
		<?php
			$body=html_entity_decode($row->research_body);
		   ?> <textarea class="ckeditor" name="editor1"><?php  echo $body; ?></textarea> <?php
		}
		}
		else{
			  ?> <textarea class="ckeditor" name="editor1"></textarea><?php
			}	 
		 ?>
		 <br>
		 </div>
		 <button type="submit" class="btn btn-info">Save data</button>
 </form>


<script>
		CKEDITOR.replace( 'editor1',
            {
                extraPlugins: 'save-to-pdf',
                pdfHandler: '<?php  echo base_url("/php/savetopdf.php") ?>',
            } );
</script>
