<link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet">
</link>
<script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<style>
  .error {
    color: red;
  }

  .result-draft {

    height: 1100px;
    overflow: scroll;
  }

  /* width */
  ::-webkit-scrollbar {
    width: 20px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: black;

  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #black;
  }

  div#judgements-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

  }

  div#bareacts-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

  }

  div#article-filter select {

    width: 23%;
    display: inline-block;
    margin: 4px 4px;

  }

  ul.bareacts-list li,
  div#result li,
  .backend-text ul li,
  .result-draft li {
    list-style: none;
    margin-bottom: 14px;
    line-height: 28px;
    color: #666;
    margin-right: 30px;
    padding-left: 16px;
  }

  li {
    display: list-item;
    text-align: -webkit-match-parent;
  }
</style>
<?php
$userid = $this->user->info->ID;
$row_id = $_GET['id'];
?>
<div class="row margin_left">
	<div class="col-md-7">
		<?php echo $this->session->flashdata('success'); ?>
		<?php echo $this->session->flashdata('error'); ?>
		<h3>Research Editor</h3>
		<!-- <iframe src="<?php echo site_url('case_editor/viewresearch'); ?>/<?php echo $row_id;  ?>" width="100%" height="600px" style="border:none;"></iframe> -->
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
		<input type="hidden" name="caseid" id="caseid" value="<?php echo $query['ID'];  ?>" />
		<div class="form-group">
			<label for="case_heading">Heading</label>
			<input type="text" name="case_heading" id="case_heading" placeholder="Research Heading" class="form-control" value="<?php if (isset($query['reasearch_heading'])) {
																																	echo $query['reasearch_heading'];
																																} ?>" />
			<p class="case-heading-error-info error"></p>
		</div>
		<div class="form-group">
			<label for="catg">Catagory</label>
			<input type="text" name="catag" id="catg" placeholder="Case Heading" class="form-control" value="<?php echo $query['c_name']; ?>" readonly />
			<p class="cat-error-info error"></p>
		</div>
		<div class="form-group">
		<label for="subcatg">Sub category</label>
		<input type="text" id="subcatg" value="<?php echo $subcat['c_name'] ?>" readonly class="form-control"/>
			<p class="subcat-error-info error"></p>
		</div>
		<div class="form-group">
		<?php
			$body=html_entity_decode($query['research_body']);
		   ?> 
		    <label for="editor1">Description</label>
		   <textarea class="form-control" id="editor1" name="editor1"><?php  echo $body; ?></textarea> 
			<!-- <textarea name="editor1" class="ckeditor" id="ck" style="height:1500px;"></textarea> -->
			<p class="editor1-error-info error"></p>
			<br>
		</div>
		<button type="button" class="btn  profile-set-btn" id="submitBtn">Update Research</button>
		<div id="content-container"></div>
	</div>
	<div class="col-md-5">
		<h3>Reference Links</h3>
		<!-- <iframe src="<?php echo site_url('case_listings/searchcase'); ?>"  width="100%" height="600px" style="border:none;"></iframe> -->
		<iframe class="reference" src="<?php echo base_url('case_listings/my_case_referance_iframe'); ?>" width="100%" height="1300px" style="border:none;"></iframe>
	</div>
</div>
<?php
if (isset($_POST['editor'])) {
	echo $_POST['editor'];
}
?>
<!-- <script>
	$(document).ready(function(){
			$("#search-article").show();
			$("#search-Whereacts").hide();
			$("#search-maxims").hide();
			$("#search-judgments").hide();
	$("#article").click(function(){
			$("#search-article").show();
			$("#search-Whereacts").hide();
			$("#search-maxims").hide();
			$("#search-judgments").hide();
		});
	$("#whereacts").click(function(){
			$("#search-article").hide();
			$("#search-Whereacts").show();
			$("#search-maxims").hide();
			$("#search-judgments").hide();
		});
	$("#maxims").click(function(){
			$("#search-article").hide();
			$("#search-Whereacts").hide();
			$("#search-maxims").show();
			$("#search-judgments").hide();
		});
	$("#judgment").click(function(){
			$("#search-article").hide();
			$("#search-Whereacts").hide();
			$("#search-maxims").hide();
			$("#search-judgments").show();
		});
});
</script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script type="text/javascript">
	/*
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get('case_listings/ajaxpro', { query: query }, function (data) {
                console.log(data);
                data = $.parseJSON(data);
                return process(data);
            });
        }
    });
   */
	// $('#search-article').autocomplete({
	//   	delay : 300,
	//   	minLength: 2,
	//   	source: function (request, response) {
	//          $.ajax({
	//              type: "GET",
	//              url: global_base_url + "case_listings/ajaxpro",
	//              data: {
	//              		query : request.term
	//              },
	//              dataType: 'JSON',
	//              success: function (msg) {
	//                  response(msg);
	//              }
	//          });
	//       },
	//     	focus: function( event, ui ) {
	// 	        $(this).val( ui.item.label );
	// 	        return false;
	// 	    },
	// 	    create: function () {
	//             $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	//             	if(item.type == "user") {
	// 	                return $('<li class="clearfix search-option-user">')
	// 	                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_a" id="display_result">' + item.label + '</a></div>')
	// 	                    .appendTo(ul);
	//                 } else if(item.type == "page") {
	//                 	return $('<li class="clearfix search-option-page">')
	// 	                    .append('<div class="search-user-info"><a href="'+item.url+'" target="iframe_a" id="display_result">' + item.label + '</a></div>')
	// 	                    .appendTo(ul);
	//                 }
	//             };
	//         }
	//   });
	//   $(document).ready(function(){
	// 	  $("#editor1").css("height","600");
	// 	  $("#iframe2ef").css("display","none");
	// 	  $("#search-article").on('change',function(){
	// 			$("#iframe2ef").css("display","block").css("height","800").css("scrolling","no");
	// 	     });
	// 	  });
</script>

<?php
$tmp_url = str_replace("/social", "", base_url());
?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>



<script>
  $('.reference').load(function() {
    $('.reference').contents().find('.header-area-wrapper').hide();
    $('.reference').contents().find('.footer-area-wrapper').hide();
  });
</script>


<script src="{{asset('assets/plugins/validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('assets/plugins/validation/jquery.validate-init.js')}}"></script>


<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'csrftoken': '{{ csrf_token() }}'
    }
  });
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>


<script>
  $(document).ready(function() {

    $('#submitBtn').click(function(e) {

      $(".case-heading-error-info").html("");
      $(".cat-error-info").html("");
      $(".subcat-error-info").html("");
      $(".editor1-error-info").html("");

	  var caseid = $("#caseid").val();
      var case_heading = $('#case_heading').val();
      var editor1 = $('#editor1').val();

      var error = false;
      if (case_heading == "") {
        error = true;
        $(".case-heading-error-info").html("This field is required");
      }
      if (editor1 == "") {
        error = true;
        $(".editor1-error-info").html("This field is required");
      }
      if (error == true) {
        return false;
      }
      e.preventDefault();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({

        //   url: '<?php echo site_url('Case_editor/createreasearch') ?>',
        url: global_base_url + "Case_editor/savereasech",
        type: 'POST',
        data: {
		  caseid: caseid,
          case_heading: case_heading,
          editor1: editor1,
          '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
        },
        dataType: "json",
        success: function(response) {
          console.log(response.message);
          // var result = json.stringify(response);
          if (response.message == 'success') {
            console.log("success", response);
            window.location.href = response.url;
          } else {
            console.log("failed", response);
            $('#content-container').html("Something went wrong");
          }
        }
      });
    });

  });
</script>
<script>
  $(document).ready(function() {
    // alert('goooo');
    setTimeout(function() {
      $('.alert-success').fadeOut(1000);
    }, 8000);
  });
  </script>