<style>
span.rchjemal {
    padding-left: 22px;
}

</style>

<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="error-success-message">
	
<span>
<?php 



echo $this->session->flashdata('success_message');

echo $this->session->flashdata('failed_message');



?>

<?php $user=$user->result();

//echo $user[0]->city;
 ?>
</span>
</div>


<div class="service_requests">

<form id="regForm" action="service_request/save" method="post">
	
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">




  <!-- One "tab" for each step in the form: -->
  <div class="tab">
	  <h1>What service do you need?</h1>
    
    <div class="step-1-content">
	
		
      <?php
			
			$parent=0;
			
			
			
			$query = $this->db->get_where('catagories', array('parent_id' => $parent));
			
			
			$data['categories'] = $query->result();
			
			
			$loc=explode(",",$this->user->info->city);

			$location=$loc[0];
			
			$getlocationprice=$this->db->get_where('locationprice', array('catagoryid' => $data['categories'][0]->cid,'location'=>$location));
			
			
			
			
			foreach($getlocationprice->result() as $price){
				
				 $pricearray= $price->price;
				
				}
			
			//print_r($getlocationprice->result());
			
			
			if(!empty($query->result())){
			$i=1;
			foreach($data['categories'] as $rows){
				
				
		       if(!empty($pricearray)){
				   
					$price=$pricearray;
					
				   
				   }
				 else{
					 $price=$rows->price;
					 
					 }
			?>
			<div class="step-1-content-danger">
				<?php
				if($i=='1')
				{
				?>
				<input type="radio" value="<?php echo $rows->cid."_".$rows->c_name."_".$price; ?>" name="issue_id" id="<?php echo "parent_cat".$rows->cid; ?>" checked  />
				<?php
			}
			else
			{
				?>
				<input type="radio" value="<?php echo $rows->cid."_".$rows->c_name."_".$rows->price; ?>" name="issue_id" id="<?php echo "parent_cat".$rows->cid; ?>"   />
			<?php
			}
			?>
            
            <label for="<?php echo "parent_cat".$rows->cid; ?>"><?php echo $rows->c_name; ?></label>
             </div>
            <?php
            $i++;
			}
		}
            ?>
       
		
        
    </div>
     <button type="button" class="gj" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
     <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
     
     
  </div>
  <div class="tab">
	 <h1> What do you need help with? </h1>
    
     
     <div class="step-1-content">
		
		<div id="type1"></div>
	
	
    </div>
    
   
     <button type="button"  id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" class="helpdiv" id="nextBtn" onclick="nextPrev(1)">Next</button>
  </div>
  
  
  
  <div class="tab">
  
    <h1>What type of legal assistance do you require?</h1>
    <div class="step-1-content">
        <div class="step-1-content-default">
            <input type="radio" value="Legal Consultation" name="action_list" id="subchild1"   />
            <label for="subchild1">Legal Consultation</label>
        </div>
        <div class="step-1-content-primary">
            <input type="radio" value="Legal Representation"  name="action_list" id="subchild2" checked  />
            <label for="subchild2">Legal Representation</label>
        </div>
        <div class="step-1-content-success">
            <input type="radio"  value="Petition Filing" name="action_list" id="subchild3"  />
            <label for="subchild3">Petition Filing</label>
        </div>
        <div class="step-1-content-danger">
            <input type="radio"  value="Document Preparation"  name="action_list" id="subchild4"  />
            <label for="subchild4">Document Preparation</label>
        </div>
        <div class="step-1-content-warning">
            <input type="radio" value="Legal Notice"  name="action_list" id="subchild5"  />
            <label for="subchild5">Legal Notice</label>
        </div>
        <div class="step-1-content-success">
            <input type="radio" value="Alternate Dispute Resolution"  name="action_list" id="subchild6"  />
            <label for="subchild6">Alternate Dispute Resolution</label>
        </div>
        <div class="step-1-content-primary">
            <input type="radio" value="Arbitration"  name="action_list" id="subchild7"  />
            <label for="subchild7">Arbitration</label>
        </div>
         <div class="step-1-content-danger">
            <input type="radio" value="Document Verification"  name="action_list" id="subchild8"  />
            <label for="subchild8">Document Verification</label>
        </div>
         
        
    </div>
	  <button type="button" class="gj"  id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
  </div>
  <!--
   
  <div class="tab">
  
    <h1>Tell us about yourself to serve you better</h1>
    <div class="step-1-content">
        <div class="step-1-content-default">
            <input type="radio" value="Individual looking for advocates/lawyers" name="tellus" id="subchild11" required />
            <label for="subchild11">Individual looking for advocates/lawyers</label>
        </div>
        <div class="step-1-content-primary">
            <input type="radio" value="Lawyer looking for clients/internships" name="tellus" id="subchild21" required />
            <label for="subchild21">Lawyer looking for clients/internships</label>
        </div>
       
         
        
    </div>
    
    
	  <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
  </div>
  -->
  
  <div class="tab">
    <h1>Share your contact details to proceed</h1>
    
    <div class="form-group">
	<label for="phone">Phone</label>
    <input placeholder="Phone" oninput="this.className = ''" name="phone" value="<?php echo $user[0]->phone; ?>" readonly >
   </div>
     <div class="form-group">
	<label for="name">Name</label>
    <input placeholder="Name" oninput="this.className = ''" name="name" value="<?php echo $user[0]->first_name; ?>" readonly >
    </div>
     <div class="form-group">
	<label for="email">Email</label>
		<input placeholder="Email" oninput="this.className = ''" name="email" type="email" value="<?php echo $user[0]->email; ?>" readonly >
     </div>
     
    <div class="form-group">
     <label for="location">Location</label>
     <input  oninput="this.className = ''" name="location" type="text" value="<?php echo $user[0]->city; ?>" readonly >
   
   </div>
   
   
    <div class="form-group">
     <label for="shortdescription">Short description</label>
	 <textarea  name="shortdescription" class="form-control" required></textarea>
    </div>
    
  <div class="form-group">
    <label for="shortdescription">I would like to recieve emails</label>
     <label class="radio-inline">
      <input type="radio" value="yes" name="emailtime" checked />  <span class="rchjemal">Yes</span>
    </label>
    <label class="radio-inline">
      <input type="radio" value="no" name="emailtime"  />  <span class="rchjemal">No</span>
    </label>
   
       
		
   
   </div>
    <h6 class="recieve-call-heading">I would like to recieve calls on</h6>
    
    <ul class="time-details">
		<li><input type="radio" value="Any Time" name="calltime"  checked />Any Time</li>
		<li><input type="radio" value="12PM to 3PM" name="calltime"  />12PM to 3PM</li>
		<li><input type="radio" value="3PM to 6PM" name="calltime"  />3PM to 6PM</li>
		<li><input type="radio" value="6PM to 9PM" name="calltime"   />6PM to 9PM</li>
    </ul>
    
    
     <button type="button"  id="prevBtn" onclick="nextPrev(-1)">Previous</button>
     
     <button type="submit" name="submit" id="nextBtn">Next</button>
  
  
  </div>
  
  
  
 
  <!-- Circles which indicates the steps of the form: -->
<div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
    <span class="step"></span>
  </div>
</form>

</div>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  
 
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i <= x.length; i++) {
    //x[i].className = x[i].className.replace("active","");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>



 <script>
		$(function(){

  // jQuery methods go here...
  
 
     $("#nextBtn").click(function(){
	  
	  
//alert("sdfsdf");
	
            var categoryId = $("input[name='issue_id']:checked").val().split("_");;
           
			$.ajax({
            url: "service_request/action",
            type: "GET",
            data: "categoryId="+categoryId[0],
            success: function (response) {
                //console.log(response);
                
                
               if(response=="" || response==null){
				
               // $("#type1").html(response);
              //  $(".uncommondiv").hide();
                  $(".helpdiv").click();
                  $(".gj").attr("onclick","nextPrev(-2)");
                  
       
			 }
			 else{
				// $(".helpdiv").click();
				     $("#type1").html(response);
				               // $(".helpdiv").click();
				 // $("#type2").html("No Subcatagory Available..click to procede..");
				 }
            
            
				
            
            },
            error: function(errorThrown){
        alert(errorThrown);
        alert("There is an error with AJAX!");
    }
        });
        
        
    
	
	
	  });
	  
	
	    

});
     
     </script>
