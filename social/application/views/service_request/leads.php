<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.5/dist/sweetalert2.all.min.js"></script>

<?php $this->load->view('sidebar/sidebar.php'); ?>

<div class="white-area-content separator page-right">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo $user->first_name ?>'s Leads</div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
        <!-- <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok nodisplay" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok nodisplay" id="username-exact"></span> <?php echo lang("ctn_25") ?></a></li>
          <li><a href="#" onclick="change_search(3)"><span class="glyphicon glyphicon-ok nodisplay" id="firstname-exact"></span> <?php echo lang("ctn_29") ?></a></li>
          <li><a href="#" onclick="change_search(4)"><span class="glyphicon glyphicon-ok nodisplay" id="lastname-exact"></span> <?php echo lang("ctn_30") ?></a></li>
        </ul> -->
      </div><!-- /btn-group -->
</div>
</div>

</div>
</div>

<div class="table-responsive">
<table id="friends-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header">
	<td>Catagory</td>
	<td>Subcatagory</td>
	<td>Case Description</td>
	<td>Action required</td>
	<td>User Location</td>
	<td>Lead Budget</td>
	<td>Lead View Count</td>
	<td>created at</td>
	<td>Options</td>
	
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>


</div>

 <script type="text/javascript">
 
$(document).ready(function() {
 <?php if ($user) { ?>
    var check = '<?php echo $user->profile_identification; ?>'
  var st = $('#search_type').val();
  if (check==1) {
     var urltoget = "<?php echo site_url("service_request/myleadsbyuserid/".$user->ID); ?>"

  }else{
    var urltoget = "<?php echo site_url("service_request/myleads/".$user->city); ?>"
  }
 
    var table = $('#friends-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
		"processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 5,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        { "orderable" : false },
        null,
		null,
		null,
		null,
		null,
		null,
		null,
		
		
        { "orderable" : false }
    ],
        "ajax": {
            url : urltoget,
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        
       
        
        
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});
<?php } ?>
} );
function change_search(search) 
    {
      var options = [
      "search-like", 
      "search-exact",
      "username-exact",
      "firstname-exact",
      "lastname-exact",

      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }


    function viewLead(leadid,lead_price){
      var _url = "<?php echo site_url("service_request/viewleadss/") ?>";
      var _token = "<?php echo $this->security->get_csrf_hash();?>";
      _url = _url+leadid+'/'+lead_price+'/'+_token;
      
      
      Swal.fire({
            title: 'Are you sure?',
            text: lead_price +" Points will deduct from your account.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'View Lead'
        }).then((result) => {
          if (result.isConfirmed) {
             window.location.href = _url;
            }
        });
        

    }
    function unpublishlead(leadid,lead_price){
      var _url = "<?php echo site_url("service_request/unpublishlead/") ?>";
      var _token = "<?php echo $this->security->get_csrf_hash();?>";
      _url = _url+leadid;
      
      
      Swal.fire({
            title: 'Are you sure?',
            text: "Are you want to unpublish your lead?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Unpublish Lead'
        }).then((result) => {
          if (result.isConfirmed) {
             window.location.href = _url;
            }
        });
        

    }
</script>
