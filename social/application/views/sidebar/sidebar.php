<style>
   
   span.side-text-p {
    color: #000;
}
.active.side-text-p{
   color: white;
}
</style>
<?php
$base_tmp_url = str_replace("/social", "", base_url());
?>
<div class="row">
   <div class="sidebar-block" id="homepage-links">
      <?php
      $user = $this->user_model->get_user_by_id($this->user->info->ID);
      $user = $user->row();

      $user_iden = $user->profile_identification;
      // echo $user_iden;
      // die();
      $agency_status = $user->agency_verified;
      $pageuser = $this->page_model->get_page_users_by_id($this->user->info->ID);
      $pageuser = $pageuser->row();
      ?>
      <?php $resultcore = $getprofile->result();  ?>
      <div class="side_block-tog visible-xs"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></div>
      <div class="ul_wrap">
         <ul>
           
            <li <?php if (isset($type) && $type == 0) : if ($this->uri->segment(1) == "home" || $this->uri->segment(1) == "") : ?> class="active" <?php endif;
                                                                                       endif; ?>>
               <a href="<?php echo site_url("home") ?>">
                  <span class="glyphicon glyphicon-plus"></span>
                  <span class="side-text-p"> <?php echo lang("ctn_481") ?></span>
               </a>
            </li>
            <li <?php if ($this->uri->segment(1) == "profile") : ?> class="active" <?php
                                                                              endif; ?>><a href="<?php echo site_url("profile/" . $this->user->info->username) ?>"><span class="glyphicon glyphicon-user sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_200") ?></span></a></li>
            <li <?php if ($this->uri->segment(1) == "leads") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("leads"); ?>"><span class="glyphicon glyphicon-file"></span><span class="side-text-p"> My Leads</span></a></li>
            <li <?php if ($this->uri->segment(1) == "messages") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("messages") ?>"><span class="glyphicon glyphicon-envelope sidebaricon"></span> <span class="side-text-p"><?php echo lang("ctn_482") ?></span></a> <span class="badge"><?php if (isset($countmessage) && $countmessage >= 1) {
                                                                                                                                                                                                                                                                                                         echo $countmessage;
                                                                                                                                                                                                                                                                                                      } else {
                                                                                                                                                                                                                                                                                                         echo '';
                                                                                                                                                                                                                                                                                                      } ?></span></li>
            <li class="inner_ul_top <?php if ($this->uri->segment(1) == "connectionrequests" || $this->uri->segment(1) == "invite_contacts") : ?> open <?php endif; ?>" >
               <a href="<?php echo site_url("myconnections/" . $user->ID) ?>" data-toggle="dropdown"><span class="glyphicon glyphicon-link"></span> <span class="side-text-p"><?php echo lang("ctn_867") ?></span></a>
               <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
               </button>
               <ul class="inner_ul_wrap dropdown-menu">
                  <li <?php if ($this->uri->segment(1) == "connectionrequests") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("connectionrequests") ?>"><span class="glyphicon glyphicon-info-sign"></span><span class="side-text-p"> <?php echo lang("ctn_640") ?></span></a></li>
                  <li <?php if ($this->uri->segment(1) == "invite_contacts") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("invite_contacts") ?>"><span class="glyphicon glyphicon-edit"></span><span class="side-text-p">Invite Contacts</span></a></li>
               </ul>
            </li>
            <?php if ($user_iden != 1) : ?>
               <li <?php if ($this->uri->segment(1) == "appointments") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("appointments");  ?>"><span class="glyphicon glyphicon-list-alt"></span><span class="side-text-p"> My Appointment(s)</span> </a></li>
            <?php endif; ?>
            <li class="inner_ul_top  <?php if ($this->uri->segment(1) == "edit_profile_lawyer" || $this->uri->segment(1) == "edit_profile_seeker" || $this->uri->segment(1) == "edit_profile_student"|| $this->uri->segment(1) == "edit_profile_professor"|| $this->uri->segment(1) == "usersettings" || $this->uri->segment(2) == "password" || $this->uri->segment(2) == "settings" || $this->uri->segment(1) == "socialnetwork") : ?> open <?php endif; ?>">
               <a href="javascript:void(0)" data-toggle="dropdown"><span class="glyphicon glyphicon-cog sidebaricon"></span><span class="side-text-p"> Settings</span></a>
               <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
               </button>
               <ul class="inner_ul_wrap dropdown-menu">
               <?php if ($user_iden == 0) :   
               ?>
                  <li  class="<?php if ($this->uri->segment(1) == 'edit_profile_lawyer') echo "active" ?>"><a href="<?php echo site_url("edit_profile_lawyer") ?>"><span class="glyphicon glyphicon-camera"></span>Edit Profile</a></li>
               <?php  
                  elseif($user_iden == 1) :   
               ?>
                  <li  class="<?php if ($this->uri->segment(1) == 'edit_profile_seeker') echo "active" ?>"><a href="<?php echo site_url("edit_profile_seeker") ?>"><span class="glyphicon glyphicon-camera"></span>Edit Profile</a></li>
               <?php  
                  elseif($user_iden == 2) :   
               ?>
                  <li  class="<?php if ($this->uri->segment(1) == 'edit_profile_student') echo "active" ?>"><a href="<?php echo site_url("edit_profile_student") ?>"><span class="glyphicon glyphicon-camera"></span>Edit Profile</a></li>
               <?php  
                  elseif($user_iden == 3) :   
               ?>
                  <li  class="<?php if ($this->uri->segment(1) == 'edit_profile_professor') echo "active" ?>"><a href="<?php echo site_url("edit_profile_professor") ?>"><span class="glyphicon glyphicon-camera"></span>Edit Profile</a></li>
               <?php
                  endif;
               ?>
                  <li class="<?php if ($this->uri->segment(1) == 'usersettings') echo "active" ?>"><a href="<?php echo site_url("usersettings") ?>"><span class="glyphicon glyphicon-camera"></span><?php echo lang("ctn_156") ?></a></li>
                  <li class="<?php if ($this->uri->segment(2) == 'password') echo "active" ?>"><a href="<?php echo site_url("update/password") ?>"><span class="glyphicon glyphicon-lock"></span><?php echo lang("ctn_225") ?></a></li>
                  <li class="<?php if ($this->uri->segment(2) == 'settings') echo "active" ?>"> <a href="<?php echo site_url("privacy/settings") ?>"><span class="glyphicon glyphicon-eye-open"></span><?php echo lang("ctn_629") ?></a></li>
                  <li class="<?php if ($this->uri->segment(1) == 'socialnetwork') echo "active" ?>"> <a href="<?php echo site_url("socialnetwork") ?>"><span class="glyphicon glyphicon-glass"></span><?php echo lang("ctn_422") ?></a></li>
               </ul>
            </li>
            <li class="inner_ul_top <?php if ($this->uri->segment(2) == 'account_statement' || $this->uri->segment(2) == 'subscriptionsView') echo "open";  if ($this->uri->segment(1) == 'funds' && $this->uri->segment(2)=='') echo "open" ?>">
               <a href="javascript:void(0)" data-toggle="dropdown"><span class="glyphicon glyphicon-briefcase sidebaricon"></span><span class="side-text-p"> My wallet (Amount)</span></a>
               <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
               </button>
               <ul class="inner_ul_wrap dropdown-menu">
                  <li class="<?php if ($this->uri->segment(2) == 'account_statement') echo "active" ?>"><a href="<?php echo site_url("funds/account_statement") ?>"><span class="glyphicon glyphicon-tasks"></span>Account Statement</a></li>
                  <li  class="<?php if ($this->uri->segment(1) == 'funds' && $this->uri->segment(2)=='') echo "active" ?>"><a href="<?php echo site_url("funds/account_statement") ?>"><a href="<?php echo site_url("funds") ?>"><span class="glyphicon glyphicon-usd"></span>Add Balance</a></li>
                  <!-- <li><a href="javascript:void(0)"><span class="glyphicon glyphicon-cog"></span>Payment Method</a></li> -->
                  <li  class="<?php if ($this->uri->segment(2) == 'subscriptionsView') echo "active" ?>"><a href="<?php echo site_url("funds/account_statement") ?>"><a href="<?php echo site_url("funds/subscriptionsView") ?>"><span class="glyphicon glyphicon-duplicate"> </span>Subscriptions</a></li>
                  <!-- <li><a href="javascript:void(0)"><span class="glyphicon glyphicon-cog"></span>Current offers</a></li> -->
               </ul>
            </li>
            <li <?php if ($this->uri->segment(1) == "research") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("research") ?>"><span class="glyphicon glyphicon-book"></span><span class="side-text-p"> My Reasearch</span></a></li>
            <li <?php if ($this->uri->segment(1) == "casebuilder") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("casebuilder") ?>"><span class="glyphicon glyphicon-text-size"></span><span class="side-text-p"> <?php echo lang("ctn_818") ?></span></a></li>
            <li <?php if ($this->uri->segment(1) == "case_listings") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("case_listings/create") ?>"><span class="glyphicon glyphicon-screenshot"></span><span class="side-text-p"> Case Builder</span></a></li>
            <!-- <li <?php if ($this->uri->segment(1) == "settings") : ?> class="active"<?php endif; ?>><a href="<?php echo site_url("settings") ?>"><span class="glyphicon glyphicon-wrench sidebaricon" ></span><span class="side-text-p"> <?php echo lang("ctn_827") ?></span></a></li> -->
            <?php if ($user_iden == 2) :    //nav side bar for students 
            ?>
               <li <?php if ($this->uri->segment(1) == "myaccount") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("myaccount"); ?>"><span class="glyphicon glyphicon-pencil sidebaricon"></span> <span class="side-text-p">My Account</span></a></li>
               <li <?php if ($this->uri->segment(1) == "upgrade") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("upgrade/advocate"); ?>"><span class="glyphicon glyphicon-star"></span><span class="side-text-p"> Upgrade to Advocate</span></a></li>
               <li <?php if ($this->uri->segment(1) == "research") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("research") ?>"><span class="glyphicon glyphicon-file"></span><span class="side-text-p"> My Reasearch</span></a></li>
               <li <?php if ($this->uri->segment(1) == "article/view") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("article/view"); ?>"><span class="glyphicon glyphicon-folder-open"></span><span class="side-text-p"> <?php echo lang("ctn_780") ?></span></a></li>
            <?php endif; ?>
            <li <?php if ($this->uri->segment(1) == "settings") : ?> class="active" <?php endif; ?>><a href="<?php echo $base_tmp_url . 'lawyer/q2a' ?>"><span class="glyphicon glyphicon-question-sign"></span><span class="side-text-p"> <?php echo lang("ctn_820") ?></span></a></li>
            <li <?php if ($this->uri->segment(1) == "settings") : ?> class="active" <?php endif; ?>><a href="javascript:void(0)"><span class="glyphicon glyphicon-font"></span><span class="side-text-p"> My Answer</span></a></li>
            <?php if ($user_iden == 1) :  //nav side bar for Seekers 
            ?>
               <li <?php if ($this->uri->segment(1) == "appointment") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("appointment");  ?>"><span class="glyphicon glyphicon-calendar"></span><span class="side-text-p"> Appointments list</span></a></li>
               <li <?php if ($this->uri->segment(1) == "servicerequests") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("servicerequests");  ?>"><span class="glyphicon glyphicon-eye-open"></span><span class="side-text-p"> All Requests</span></a></li>
               <li <?php if ($this->uri->segment(1) == "service_request") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("service_request");  ?>"><span class="glyphicon glyphicon-file"></span><span class="side-text-p"> <?php echo lang("ctn_826") ?></span></a></li>
               <li <?php if ($this->uri->segment(1) == "ratings") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("ratings");  ?>"><span class="glyphicon glyphicon-star-empty"></span><span class="side-text-p">Rating and Reviews</span></a></li>
            <?php endif; ?>
            <?php if ($user_iden == 0) :  //nav side bar for Lawyers
            ?>
               <?php if (empty($resultcore)) : ?>
                  <li class="active"><a href="<?php echo site_url("profile") ?>"><span class="glyphicon glyphicon-edit"></span> Complete Your Profile <?php //echo lang("ctn_481") 
                                                                                                                                                      ?></a></li>
               <?php endif; ?>
               <li class="inner_ul_top">
                  <a href="javascript:void(0)" data-toggle="dropdown"><span class="glyphicon glyphicon-equalizer sidebaricon"></span><span class="side-text-p"> Library</span></a>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <i class="fa fa-angle-down" aria-hidden="true"></i>
                  </button>
                  <ul class="inner_ul_wrap dropdown-menu">
                     <li><a href="<?php echo $base_tmp_url . 'supremecourt'; ?>" target="_blank"><span class="glyphicon glyphicon-tag"></span>Judgement</a></li>
                     <li><a href="<?php echo $base_tmp_url . 'articles'; ?>" target="_blank"><span class="glyphicon glyphicon-bookmark"></span>Articles</a></li>
                     <li><a href="<?php echo $base_tmp_url . 'maxims'; ?>" target="_blank"><span class="glyphicon glyphicon-pencil"></span>Legal Terms</a></li>
                     <li><a href="<?php echo $base_tmp_url . 'allmaxims'; ?>" target="_blank"><span class="glyphicon glyphicon-star"></span>Maxims</a></li>
                     <li><a href="<?php echo $base_tmp_url . 'bareacts'; ?>" target="_blank"><span class="glyphicon glyphicon-th-large"></span>Bareacts</a></li>
                  </ul>
               </li>
               <li <?php if ($this->uri->segment(1) == "leadershipboard") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("leadershipboard") ?>"><span class="glyphicon glyphicon-signal "></span><span class="side-text-p">Leadership Board</span></a></li>
               <li <?php if ($this->uri->segment(1) == "article") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("article/view"); ?>"><span class="glyphicon glyphicon-tags"></span><span class="side-text-p"> My Articles</span></a></li>
               <li  <?php if ($this->uri->segment(1) == "headnotes") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("headnotes"); ?>"><span class="glyphicon glyphicon-folder-open"></span>My headnotes</a></li>
               <?php
               if ($this->user->info->Is_moderator == "1") {
               ?>
                  <li class="inner_ul_top">
                     <a href="javascript:void(0)" data-toggle="dropdown"><span class="glyphicon glyphicon-wrench sidebaricon"></span><span class="side-text-p">Moderation Panel</span></a>
                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                     </button>
                     <ul class="inner_ul_wrap dropdown-menu">
                        <li><a href="javascript:void(0)"><span class="glyphicon glyphicon-cog"></span>Headnotes</a></li>
                        <li><a href="javascript:void(0)"><span class="glyphicon glyphicon-cog"></span>Articles</a></li>
                     </ul>
                  </li>
               <?php
               }
               ?>
               <!-- <li <?php if ($this->uri->segment(1) == "bonus") : ?> class="active"<?php endif; ?>><a href="<?php echo site_url("bonus/" . $this->user->info->ID) ?>"><span class=" glyphicon glyphicon-usd" ></span><span class="side-text-p"> <?php echo "Bonus Points"; ?></span></a></li> -->
            <?php endif; ?>
            <?php if ($user_iden == 3) : //nav side bar for professors 
            ?>
               <li <?php if ($this->uri->segment(1) == "bonus") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("bonus/" . $this->user->info->ID) ?>"><span class="glyphicon glyphicon-usd"></span><span class="side-text-p"> <?php echo "Bonus Points"; ?></span></a></li>

               <li <?php if ($this->uri->segment(1) == "article/view") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("article/view"); ?>"><span class="glyphicon glyphicon-folder-open"></span><span class="side-text-p"> <?php echo lang("ctn_780") ?></span></a></li>

               <li <?php if ($this->uri->segment(1) == "research") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("research") ?>"><span class="glyphicon glyphicon-file"></span><span class="side-text-p"> My Reasearch</span></a></li>

               <li <?php if ($this->uri->segment(1) == "casebuilder") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("casebuilder") ?>"><span class="glyphicon glyphicon-screenshot"></span><span class="side-text-p"> <?php echo lang("ctn_818") ?></span></a></li>
               <?php if ($this->settings->info->enable_blogs) : ?>

                  <li <?php if ($this->uri->segment(1) == "blog/your") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("blog/your") ?>"><span class="glyphicon glyphicon-pencil sidebaricon"></span><span class="side-text-p"> Blogs</span></a></li>
               <?php endif; ?>
            <?php endif; ?>
            <!--  <li><a href="<?php echo $base_tmp_url . 'lawyer/profile'; ?>"><span class="glyphicon glyphicon-file" ></span> <?php //echo lang("ctn_825") 
                                                                                                                              ?></a></li>  -->
            <!-- <li <?php if ($this->uri->segment(1) == "connectionrequests") : ?> class="active"<?php endif; ?>><a href="<?php echo site_url("connectionrequests") ?>"><span class="glyphicon glyphicon-info-sign" ></span><span class="side-text-p"> <?php echo lang("ctn_640") ?></span></a></li>
               <li <?php if ($this->uri->segment(1) == "invite_contacts") : ?> class="active"<?php endif; ?>><a href="<?php echo site_url("invite_contacts") ?>"><span class="glyphicon glyphicon-edit" ></span><span class="side-text-p">Invite Contacts</span></a></li> -->
         </ul>
         <?php //echo site_url("case_editor") 
         ?>
         <p class="sidebar-title"><?php echo lang("ctn_525") ?></p>
         <ul>
            <!-- <li <?php // if($this->uri->segment(1)=="albums"): 
                     ?> class="active"<?php //endif; 
                                                                                       ?>><a href="<?php //echo site_url("albums/" . $this->user->info->ID) 
                                                                                                                  ?>"><span class="glyphicon glyphicon-picture sidebaricon" ></span><span class="side-text-p"> <?php //echo lang("ctn_483") 
                                                                                                                                                                                                                                                                        ?></span></a></li> -->
            <!-- University Page Logics -->
            <?php if (!($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings"), $this->user))) { ?>
               <?php
               $agencyname = "";
               $pageusers = $this->user_model->get_law_firm_users($this->user->info->ID);
               $pageusers = $pageusers->row();
               if (!empty($pageusers->pageid)) {

                  $pageid = $pageusers->pageid;

                  $agencydetail = $this->user_model->get_agency_detail($pageid);

                  $agencydetail = $agencydetail->row();

                  if (!empty($agencydetail)) {

                     $agencyname = $agencydetail->name;
                  }

                  if ($user_iden == 3 || $user_iden == 2) {


                     if ($agency_status != 1) {

               ?>
                        <li <?php if ($this->uri->segment(1) == "myuniversity") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("myuniversity/" . $pageid); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agencyname; ?></span></a></li>
                        <?php
                     } else if ($agency_status == 2 || $user->is_agency_joined == 1) {


                        if (!empty($pageuser)) {


                        ?>
                           <li <?php if ($this->uri->segment(1) == "myuniversity") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("myuniversity/" . $pageid); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agencyname; ?></span></a></li>
                        <?php
                        } else {

                        ?>
                           <li <?php if ($this->uri->segment(1) == "myuniversity") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("myuniversity/" . $pageid); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agencyname; ?></span></a></li>
                        <?php
                        }
                     }
                  }
               } else {

                  $university = $this->user_model->get_university_by_id($this->user->info->ID);
                  $university = $university->row();
                  //$pageid=$agency->ID;

                  if (!empty($university)) {


                     if ($university->status == 2) {



                        ?>
                        <li <?php if ($this->uri->segment(1) == "adduniversity") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("adduniversity") ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $university->universityname; ?></span></a></li>
                     <?php
                     } else {

                     ?>
                        <li <?php if ($this->uri->segment(1) == "pendingpage") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("pendingpage"); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $university->universityname; ?></span></a></li>
            <?php
                     }
                  }
               }
            }
            ?>
            <!-- University Page Logics Ends -->
            <!-- Law Firm Page Logics Starts -->
            <?php if (!($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings"), $this->user))) { ?>
               <?php
               $pageusers = $this->user_model->get_law_firm_users($this->user->info->ID);
               $pageusers = $pageusers->row();
               if (!empty($pageusers->pageid)) {

                  $pageid = $pageusers->pageid;

                  $agencydetail = $this->user_model->get_agency_detail($pageid);

                  $agencydetail = $agencydetail->row();

                  if (!empty($agencydetail)) {

                     $agencyname = $agencydetail->name;
                  }
                  if ($user_iden == 0) {


                     if ($agency_status != 1) {

               ?>
                        <li <?php if ($this->uri->segment(1) == "mypages") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("mypages/" . $pageid); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agencyname; ?></span></a></li>
                        <?php
                     } else if ($agency_status == 2 || $user->is_agency_joined == 1) {


                        if (!empty($pageuser)) {


                        ?>
                           <li <?php if ($this->uri->segment(1) == "mypages") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("mypages/" . $pageid); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agencyname; ?></span></a></li>
                        <?php
                        } else {

                        ?>
                           <li <?php if ($this->uri->segment(1) == "mypages") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("mypages/" . $pageid); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agencyname; ?></span></a></li>
                        <?php
                        }
                     }
                  }
               } else {

                  $agency = $this->user_model->get_agency_by_id($this->user->info->ID);
                  $agency = $agency->row();
                  //$pageid=$agency->ID;

                  if (!empty($agency)) {
                     if ($agency->status == 2) {

                        ?>
                        <li <?php if ($this->uri->segment(1) == "addpage") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("addpage") ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agency->agency_name; ?></span></a></li>
                     <?php
                     } else {

                     ?>
                        <li <?php if ($this->uri->segment(1) == "pendingpage") : ?> class="active" <?php endif; ?>><a href="<?php echo site_url("pendingpage"); ?>"><span class="glyphicon glyphicon-duplicate sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_484") . " " . $agency->agency_name; ?></span></a></li>
            <?php
                     }
                  }
               }
            }
            ?>
            
            <?php if ($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>
               <p class="sidebar-title"><?php echo lang("ctn_35") ?></p>
               <ul>
               <?php endif; ?>
               <?php if ($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings"), $this->user)) : ?>
                  <li><a href="<?php echo site_url("admin") ?>"><span class="glyphicon glyphicon-tower sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_35") ?></span></a></li>
               <?php endif; ?>
               <?php if ($this->common->has_permissions(array("admin", "post_admin"), $this->user)) : ?>
                  <li <?php if (isset($type) && $type == 4) : ?>class="active" <?php endif; ?>><a href="<?php echo site_url("homep") ?>"><span class="glyphicon glyphicon-th sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_486") ?></span></a></li>
               <?php endif; ?>
               <?php if ($this->common->has_permissions(array("admin", "page_admin"), $this->user)) : ?>
                  <li><a href="<?php echo site_url("pages/all") ?>"><span class="glyphicon glyphicon-folder-open sidebaricon"></span><span class="side-text-p"> <?php echo lang("ctn_487") ?></span></a></li>
               <?php endif; ?>
               <?php if ($this->common->has_permissions(array("admin", "admin_members", "admin_payment", "admin_settings", "post_admin", "page_admin"), $this->user)) : ?>
               </ul>
            <?php endif; ?>
            <!--  <p class="sidebar-title"> -->
      </div>
      <?php
      /*
                 echo lang("ctn_526") ?></p>
      <ul>
      <?php foreach($hashtags->result() as $r) : ?>
      <li><a href="<?php echo site_url("home/index/1/" . $r->hashtag) ?>">#<?php echo $r->hashtag ?></a></li>
      <?php endforeach; 
         */
      ?>
   </div>
</div>