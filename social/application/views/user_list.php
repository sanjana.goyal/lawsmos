

<div class="row profile-mp" >
    <div class="col-md-12">

        <body style="margin: 0;font-family: Arial;">
            <section class="base-section" id="datafor" style="">
                <div id="content">
                    <section class="container" style="width:1170px;margin:0 auto;padding: 0 5px;">
                        <div class="main-section" style=" padding-top: 15px;">
                            <div class="row" style="display: flex; flex-wrap: wrap; margin: 0 -15px;">
                                <div class="col-md-2" style=" width: 16.66666667%;">
                                    <div class="intro-sec" style="">
                                        <img width="150px" src="<?=base_url()?>/uploads/images.jpeg">
                                    </div>
                                </div>
                                <div class="col-md-10" style="width: 83.33333333%;">
                                    <div class="d-flex flex-wrap" style="display: flex;flex-wrap: wrap;">
                                        <div class="col-md-5" style=" width: 41.66666667%; position: absolute; left: 160px;">
                                            <div class="info-sec" style=" padding-left: 32px;">
                                                <h2 class="main-text" style=" font-size: 16px;">Advocate</h2>
                                                <p style=" font-size: 14px;">Name:<span style="color: #919191; font-size: 14px; padding-left: 8px;"> <?php echo $data['first_name'].' '.$data['last_name']; ?></span></p>
                                                <p style="font-size: 14px;">Year of practice:<span style="color: #919191;font-size: 14px;padding-left: 8px;">5 Years </span></p>
                                                <p style="font-size: 14px;">City:<span style="color: #919191;font-size: 14px;padding-left: 8px;">Chandigarh</span></p>
                                                <p style="font-size: 14px;">Court:<span style="color: #919191;font-size: 14px;padding-left: 8px;">A.D.J Kotputali</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-5" style="width: 41.66666667%; position: absolute; left: 390px; top:30px">
                                            <div class="specialization-sec" style="margin-top: 25px;">
                                                <p style="font-size: 14px;">Areas of Specialization:<span style="font-size: 14px;color: #919191;padding-left: 10px;">Real estate & property disputes lawyers,Criminal lawyers,Court marriage lawyers</span></p>
                                                <p style="font-size: 14px;">Fields of Interest:<span style="font-size: 14px;color: #919191;padding-left: 10px;"> Real estate & property disputes lawyers,Criminal lawyers,Court marriage lawyers</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br>
                                    <hr>
                        <section class="experiecce-sec">
                            <div class="container" style="width: 1170px;margin: 0 auto;padding: 0 15px;">
                                <div class="row" style=" margin-right: -15px;margin-left: -15px;display: flex;flex-wrap: wrap;">
                                    <div class="col-md-6" style="width: 50%;">
                                        <div class="details" style="margin-top: 20px;">
                                            <div class="section__list">
                                                <div class="section__list-item">
                                                    <div class="left">
                                                        <h3 class="sub-title" style="font-size: 15px;color: #000;border-bottom: 1px solid;display: inline-block;">DISPLAY CURRENT AS:</h3>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Designation:<span style="color: #919191;padding-left: 8px;">Designated Senior</span></p>
                                                        <p class="des-name">Organization (internal link) :<span style="color: #919191;padding-left: 8px;">test</span>
                                                        </p>
                                                        <p class="des-name">Display exp: <span style="color: #919191;padding-left: 8px;">5 Years</span></p>
                                                        <p class="des-name">From: <span style="color: #919191;padding-left: 8px;"></span></p>
                                                        <p class="des-name">Till: <span style="color: #919191;padding-left: 8px;"> 2021-05-01</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style=" width: 50%;position: absolute;left: 300px;">
                                        <br>
                                        <div class="details" style="margin-top: 20px;">
                                            <div class="section__title" style="font-size: 20px;font-weight: 700;color: #176bea;text-transform: uppercase;margin-bottom: 25px;">CONTACT DETAILS</div>
                                            <div class="section__list">
                                                <div class="section__list-item">
                                                    <div class="left">
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Offiice Address:<span style="color: #919191;padding-left: 8px;">IT park Chandigarh rajiv gandhi,<br> Chandigarh, Punjab, 160001</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Phone no:<span style="color: #919191;padding-left: 8px;">+91-9068321122</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details" style="margin-top: 20px;">
                                            <div class="section__title" style="font-size: 20px;font-weight: 700;color: #176bea;text-transform: uppercase;margin-bottom: 25px;">LAWSMOS PROFILE DETAILS</div>
                                            <div class="section__list">
                                                <div class="section__list-item">
                                                    <div class="left">
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">User Since: <span style="color: #919191;padding-left: 8px;">August, 2021 </span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">User Rating:<span style="color: #919191;padding-left: 8px;">*****</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Connections:<span style="color: #919191;padding-left: 8px;">0</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">City Rank:<span style="color: #919191;padding-left: 8px;">10</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Total Consultations: <span style="color: #919191;padding-left: 8px">0</span></p>
                                                        <p class="des-name" style="font-size: 14px;margin-top: 15px;">Lawsmos Score:<span style="color: #919191;padding-left: 8px;">0</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </section>

                                </div>

                            </div>
                        </div>
                    </section>
                   </div>
               </section>
        </body>
    </div>
</div>
</div>
