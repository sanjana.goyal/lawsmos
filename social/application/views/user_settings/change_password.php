<?php 

	$base_tmp_url = str_replace("/social","",base_url());

	//redirect($base_tmp_url.'lawyer/reset'); 

?>



<div class="row-content">



<!-- <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12"> -->

<?php //include(APPPATH . "views/sidebar/sidebar.php"); ?>

<?php $this->load->view('sidebar/sidebar.php'); ?>

<!-- </div> -->



 <!-- <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 page-right"> -->



<div class="white-area-content separator page-right">

<div class="db-header clearfix">

    <div class="page-header-title"> <span class="glyphicon glyphicon-lock"></span> Change Password</div>

    <div class="db-header-extra"> 

</div>

</div>



<ol class="breadcrumb">

  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>

  <li><a href="<?php echo site_url("user_settings") ?>"><?php echo lang("ctn_224") ?></a></li>

  <li class="active"><?php echo lang("ctn_225") ?></li>

</ol>



<p><?php echo lang("ctn_237") ?></p>



<hr>





	<div class="panel panel-default">

  	<div class="panel-body">

  	<?php echo form_open(site_url("user_settings/change_password_pro"), array("class" => "form-horizontal")) ?>

            <div class="form-group">

			    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_238") ?></label>

			    <div class="col-sm-10">

			      <input type="password" class="form-control" name="current_password">

			    </div>

			</div>

			<div class="form-group">

			    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_239") ?></label>

			    <div class="col-sm-10">

			      <input type="password" class="form-control" name="new_pass1">

			    </div>

			</div>

			<div class="form-group">

			    <label for="inputEmail3" class="col-sm-2 control-label"><?php echo lang("ctn_240") ?></label>

			    <div class="col-sm-10">

			      <input type="password" class="form-control" name="new_pass2">

			    </div>

			</div>

			 <input type="submit" name="s" value="<?php echo lang("ctn_241") ?>" class="btn update_password_btn btn-primary form-control" />

			

    <?php echo form_close() ?>

    </div>

    </div>



    </div>





</div>

<!-- </div> -->

<script type="text/javascript">
		$('body').on('click','.update_password_btn',function(e){
		e.preventDefault();
		var checkpassword = validatePassword();
		if (checkpassword) {
			$(".row-content").find('.alert').remove();
			var cpass = $('input[name="current_password"]').val();
			var npass = $('input[name="new_pass1"]').val();
			var re_npass = $('input[name="new_pass2"]').val();
			$.ajax({
				url: "<?=$base_tmp_url?>/lawyer/reset",
				data: {
					"cpass" : cpass,
					"newpass" : npass,
					"renewpass" : re_npass,
				},
				type:"POST",
				success: function(res){
					console.log(res);
					if (res=='success') {
						$(".row-content").append('<div class="alert alert-success global_msg" style="display: block !important;"><b><span class="glyphicon glyphicon-ok"></span></b> Your password has been changed!</div>');
						window.location = "<?=site_url("user_settings/change_password")?>";
					}else{
						$(".row-content").append('<div class="alert alert-danger global_msg" style="display: block !important;"><b><span class="glyphicon glyphicon-remove"></span></b> '+res+'</div>');
					}
					$('.alert').show();
				}
			});
		}

	});

	function validatePassword() {
		var newPassword = $('input[name="new_pass1"]').val();
    var minNumberofChars = 10;
    var regularExpression  = /^[a-zA-Z0-9!@#$%^&*]$/;
    
    if((newPassword.length < minNumberofChars) && (!regularExpression.test(newPassword))) {
    		$(".row-content").append('<div class="alert alert-danger global_msg" style="display: block !important;"><b><span class="glyphicon glyphicon-remove"></span></b> Password should contain atleast one number and one special character with minimum 10 characters</div>');
					$('.alert').show();
        return false;
    }
    return true;
}
</script>
