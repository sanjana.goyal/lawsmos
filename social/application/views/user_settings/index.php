<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>

<style>
	div#responsive-menu-links {

		display: none;

	}



	#profile_img img {

		max-width: 100%;

		border-radius: 50%;

		max-height: 30vh;

	}



	#cover_image img {

		max-height: 50vh;

	}
</style>

<?php //print_r($this->user->info);  
?>

<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="profile-setting-row">

	<!-- <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">

<? php // include(APPPATH . "views/user_settings/sidebar.php"); 
?>

</div>

<div class="col-lg-10 col-md-9 col-sm-8 col-xs-12"> -->

	<div class="white-area-content separator page-right">

		<div class="db-header clearfix">

			<div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> <?php echo lang("ctn_224") ?></div>

			<div class="db-header-extra">

			</div>

		</div>

		<ol class="breadcrumb">

			<li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>

			<li class="active"><?php echo lang("ctn_224") ?></li>

		</ol>

		<p><?php echo lang("ctn_226") ?></p>

		<hr>

		<div class="panel panel-default">

			<div class="panel-body">

				<p class="panel-subheading"><?php echo lang("ctn_227") ?></p>

				<?php echo form_open_multipart(site_url("user_settings/pro"), array("class" => "form-horizontal")) ?>

				<?php echo validation_errors('<p class="form_error">', '</p>'); ?>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_228") ?></label>

					<div class="pro-input-wrap">

						<a href="<?php echo site_url("profile/" . $this->user->info->username) ?>"><?php echo $this->user->info->first_name . " " . $this->user->info->last_name ?></a>

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_229") ?></label>

					<div class="pro-input-wrap" id="profile_img">

						<img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->user->info->avatar ?>" />

						<?php if ($this->settings->info->avatar_upload) : ?>

							<input type="file" name="userfile" />

						<?php endif; ?>

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_615") ?></label>

					<div class="pro-input-wrap" id="cover_image">

						<img src="<?php echo base_url() ?>/<?php echo $this->settings->info->upload_path_relative ?>/<?php echo $this->user->info->profile_header ?>" width="100%" />

						<?php if ($this->settings->info->avatar_upload) : ?>

							<input type="file" name="userfile_profile" />

						<?php endif; ?>

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_230") ?></label>

					<div class="pro-input-wrap">

						<input type="email" class="form-control" name="email" value="<?php echo $this->user->info->email ?>">

					</div>

				</div>

				<div class="form-group">

					<label for="inputphone3" class="profile-label-p">Phone<?php //echo lang("ctn_230") 
																			?></label>

					<div class="pro-input-wrap">

						<input maxlength="100" type="text" class="form-control" name="phone" value="<?php echo $this->user->info->phone ?>" required>

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_231") ?></label>

					<div class="pro-input-wrap">

						<input type="text" maxlength="100" class="form-control" name="first_name" value="<?php echo $this->user->info->first_name ?>">

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_232") ?></label>

					<div class="pro-input-wrap">

						<input type="text" maxlength="100" class="form-control" name="last_name" value="<?php echo $this->user->info->last_name ?>">

					</div>

				</div>



				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_233") ?></label>

					<div class="pro-input-wrap">

						<textarea maxlength="1000" class="form-control" name="aboutme" rows="8"><?php echo nl2br($this->user->info->aboutme) ?></textarea>

					</div>

				</div>

				<p class="panel-subheading"><?php echo lang("ctn_390") ?></p>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_863") ?></label>

					<div class="pro-input-wrap">

						<!-- location_from  , location_live -->

						<input type="text" maxlength="100" class="form-control map_name" name="location_country" value="<?php echo $this->user->info->country ?>">

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_394") ?></label>

					<div class="pro-input-wrap">

						<input type="text" maxlength="100" class="form-control map_name" name="location_state" value="<?php echo $this->user->info->state ?>">

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_393") ?></label>

					<div class="pro-input-wrap">

						<input type="text" maxlength="100" class="form-control map_name" name="location_city" value="<?php echo $this->user->info->city ?>">

					</div>

				</div>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_865") ?></label>

					<div class="pro-input-wrap">

						<input type="text" maxlength="100" class="form-control" name="permannentadr" value="<?php echo $this->user->info->permanentAddr ?>">

					</div>

				</div>

				<?php $this->user->info; ?>

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_866") ?></label>

					<div class="pro-input-wrap">

						<input type="text"  maxlength="100" class="form-control map_name" id="us3-address" name="location_address" value="<?php echo $this->user->info->address_1 ?>">

						<input type="hidden" class="form-control" name="lat" value="" id="us3-lat" />

						<input type="hidden" class="form-control" name="long" value="" id="us3-lon" />

					</div>

				</div>

				<!--

		36.778261

	<div class="form-group" id="relationship_part">

	    <label for="inputEmail3" class="profile-label-p"><?php //echo lang("ctn_618") 
															?></label>

	    <div class="col-sm-10">

	      <select name="relationship_status" id="relationship" class="form-control">

	      	<option value="0"><?php /* echo lang("ctn_46") ?></option>

	      	<option value="1" <?php if($this->user->info->relationship_status == 1) echo "selected" ?>><?php echo lang("ctn_609") ?></option>

	      	<option value="2" <?php if($this->user->info->relationship_status == 2) echo "selected" ?>><?php echo lang("ctn_610") ?></option>

	      	<option value="3" <?php if($this->user->info->relationship_status == 3) echo "selected" ?>><?php echo lang("ctn_619") ?></option>

	      </select>

	      <?php if($request->num_rows() > 0) : ?>

	      	<?php $request = $request->row(); ?>

	      <p><?php echo lang("ctn_620") ?> <a href="<?php echo site_url("profile/" . $request->username) ?>"><?php echo $request->first_name . " " . $request->last_name ?></a> (<a href="<?php echo site_url("user_settings/cancel_request/" . $request->ID . "/" . $this->security->get_csrf_hash()) ?>"><?php echo lang("ctn_621") ?></a>)</p>

	      <?php endif; ?>

	      <?php foreach($requests->result() as $r) : ?>

	      	<?php

	      	if($r->relationship_status == 2) {

	      		$relationship = lang("ctn_610");

	      	} elseif($r->relationship_status == 3) {

	      		$relationship = lang("ctn_619");

	      	}

	      	?>

	      	<p><?php echo lang("ctn_622") ?> <a href=""><?php echo $r->first_name . " " . $r->last_name ?></a>(<?php echo $relationship ?>) - <a href="<?php echo site_url("user_settings/relationship_request/" . $r->ID . "/1/" . $this->security->get_csrf_hash()) ?>"><?php echo lang("ctn_623") ?></a> - <a href="<?php echo site_url("user_settings/relationship_request/" . $r->ID . "/0/" . $this->security->get_csrf_hash()) ?>"><?php echo lang("ctn_624") ?></a></p>

	      <?php endforeach; ?>

	    </div>

	</div>

	<div class="form-group <?php if(empty($relationship_user)) : ?>nodisplay<?php endif; ?>" id="relationship_user">

	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_625") ?></label>

	    <div class="col-sm-10">

	      <input type="text" name="relationship_user" class="form-control" id="name-search" value="<?php echo $relationship_user */ ?>">

	      <input type="hidden" name="userid" id="userid-search">

	    </div>

	</div>

	-->

				<div class="form-group">

					<label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_395") ?></label>

					<div class="pro-input-wrap">

						<input type="text" maxlength="100" name="zipcode" class="form-control" value="<?php echo $this->user->info->zipcode ?>">

					</div>

				</div>

				<?php foreach ($fields->result() as $r) : ?>

					<div class="form-group">



						<label for="name-in" class="profile-label-p"><?php echo $r->name ?> <?php if ($r->required) : ?>*<?php endif; ?></label>

						<div class="pro-input-wrap">

							<?php if ($r->type == 0) : ?>

								<input type="text" maxlength="100" class="form-control" id="name-in" name="cf_<?php echo $r->ID ?>" value="<?php echo $r->value ?>">

							<?php elseif ($r->type == 1) : ?>

								<textarea name="cf_<?php echo $r->ID ?>" rows="8" class="form-control"><?php echo $r->value ?></textarea>

							<?php elseif ($r->type == 2) : ?>

								<?php $options = explode(",", $r->options); ?>

								<?php $values = array_map('trim', (explode(",", $r->value))); ?>

								<?php if (count($options) > 0) : ?>

									<?php foreach ($options as $k => $v) : ?>

										<div class="form-group"><input type="checkbox" name="cf_cb_<?php echo $r->ID ?>_<?php echo $k ?>" value="1" <?php if (in_array($v, $values)) echo "checked" ?>> <?php echo $v ?></div>

									<?php endforeach; ?>

								<?php endif; ?>

							<?php elseif ($r->type == 3) : ?>

								<?php $options = explode(",", $r->options); ?>



								<?php if (count($options) > 0) : ?>

									<?php foreach ($options as $k => $v) : ?>

										<div class="form-group"><input type="radio" name="cf_radio_<?php echo $r->ID ?>" value="<?php echo $k ?>" <?php if ($r->value == $v) echo "checked" ?>> <?php echo $v ?>

											<span class="help-text"><?php echo $r->help_text ?></span>

										</div>

									<?php endforeach; ?>

								<?php endif; ?>

							<?php elseif ($r->type == 4) : ?>

								<?php $options = explode(",", $r->options); ?>

								<?php if (count($options) > 0) : ?>

									<select name="cf_<?php echo $r->ID ?>" class="form-control">

										<?php foreach ($options as $k => $v) : ?>

											<option value="<?php echo $k ?>" <?php if ($r->value == $v) echo "selected" ?>><?php echo $v ?></option>

										<?php endforeach; ?>

									</select>

								<?php endif; ?>

							<?php endif; ?>

							<!-- <span class="help-text"><?php echo $r->help_text ?></span> -->

						</div>

					</div>

				<?php endforeach; ?>

				<p><?php //echo lang("ctn_351") 
					?></p>



				<div class="map-outer">

					<p id="map_view"></p>

					<div class="container">

						<div class="map-wrap">

							<div id="us3" style="position: absolute;overflow: hidden;height: 248px;"></div>

						</div>

					</div>

				</div>

				<div class="profile-set-wrap">

					<input type="submit" name="s" value="<?php echo lang("ctn_236") ?>" class="btn  profile-set-btn" />

				</div>

				<?php echo form_close() ?>

			</div>

		</div>

	</div>

	<!-- </div> -->

</div>