<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> Profile</div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb ">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile (Step 1)</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">HELP OUR SYSTEMS IDENTIFY: </h5>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="practice_city" class=" control-label">You work as an Individual Practitioner or with a Law Firm:*</label>
                <div class=" check-inner-pp">
                    <div class="check-inner-p">
                        <input type="radio" name="lawyer_practitioner" value="1" id="lawyer_practitioner_individual" onclick="lawyerPractitioner(0)" <?php if ($core_profile['lawyer_practitioner'] == "1") {
                                                                                                                                                            echo 'checked';
                                                                                                                                                        } ?>>
                        <label for="individual">Individual</label>

                    </div>
                    <div class="check-inner-p">
                        <input type="radio" name="lawyer_practitioner" value="2" id="lawyer_practitioner_firm" onclick="lawyerPractitioner(1)" <?php if ($core_profile['lawyer_practitioner'] == "2") {
                                                                                                                                                    echo 'checked';
                                                                                                                                                } ?>>
                        <label for="firm">Firm</label>
                    </div>
                    <div class="check-inner-p">
                        <input type="radio" name="lawyer_practitioner" value="3" id="lawyer_practitioner_both" onclick="lawyerPractitioner(1)" <?php if ($core_profile['lawyer_practitioner'] == "3") {
                                                                                                                                                    echo 'checked';
                                                                                                                                                } ?>>
                        <label for="both">Both</label>
                    </div>
                </div>
                <p class="err" id="practioner_err"></p>

            </div>
            <div class="form-group">
                <label for="date_of_birth">Date of Birth:*</label>
                <input class="form-control" type="date" id="date_of_birth" value="<?php echo $core_profile['date_of_birth']; ?>" placeholder="" name="date_of_birth">
                <p class="err" id="date_of_birth_err"></p>
            </div>
            <div class="form-group lawyer_practitioner_div" style="display:none">
                <label for="registration_no" class="control-label">Firm name:*</label>
                <input maxlength="100" class="form-control" name="firm_name" value="<?php echo $core_profile['firm_name']; ?>" id="firm_name" placeholder="Enter firm name"  type="text" required="">
                <p class="err" id="firm_name_err"></p>
            </div>
            <div class="form-group lawyer_practitioner_div" style="display:none">
                <label for="registration_no" class="control-label">Firm's email address:*</label>
                <input maxlength="100" class="form-control" name="firm_email_address" value="<?php echo $core_profile['firm_email']; ?>" id="firm_email_address" placeholder="Enter firm's email address" type="email" required="">
                <input class="form-control" name="user_email" id="user_email" value="<?php echo $user_info['email']; ?>" type="hidden">
                <p class="err" id="firm_email_address_err"></p>
            </div>

            <!-- <p>YOU WILL RECEIVE INSTRUCTION ON THE GVEN EMAIL ADDRESS AS TO HOW TO SET-UP YOUR FIRM’S PROFILE ON LAWSMOS. LET’S FIRST SET-UP YOUR PROFILE.</p> -->
            <p class="lawyer_practitioner_div" style="display:none">You will recieve instruction on the given email address as to how to set-up your firm's profile on Lawsmos. Let's first set-up your profile.</p>
            <div id="notification-loaders" style="display:none;">
                <div id="loading_spinner_notification" style="text-align:center;">
                    <span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div style="overflow:auto;" class="pro-nex-prv">
                        <button type="button" id="nextBtn" class="btn first">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var profile_step = 1;
</script>
<?php include(APPPATH . "/views/user_settings/lawyer_profile/edit_profile_script.php"); ?>