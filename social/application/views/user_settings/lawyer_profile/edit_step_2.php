<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> Profile</div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile (Step 2)</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">Professional profile information: </h5>
        </div>
        <div class="card-body">
            <div class="form-group">
                <label for="practicing_lawyer" class=" control-label">Are you a practicing lawyer?:*</label>
                <div class="check-inner-pp">
                    <div class="check-inner-p">
                        <input type="radio" name="practicing_lawyer" value="1" id="practicing_lawyer_yes" onclick="lawyerPractice(1)" <?php if ($core_profile['practicing_lawyer'] == "1") {
                                                                                                                                            echo 'checked';
                                                                                                                                        } ?>>
                        <label for="practicing_lawyer_yes">Yes</label>
                    </div>
                    <div class="check-inner-p">
                        <input type="radio" name="practicing_lawyer" value="2" id="practicing_lawyer_no" onclick="lawyerPractice(2)" <?php if ($core_profile['practicing_lawyer'] == "2") {
                                                                                                                                            echo 'checked';
                                                                                                                                        } ?>>
                        <label for="practicing_lawyer_no">No</label>
                    </div>
                </div>
                <p class="err" id="practicing_lawyer_err"></p>
            </div>
            <div class="unpracticing" style="display:none;">
                <div class="form-group">
                    <label for="unpracticing_city" class="control-label">City:*</label>
                    <select class="form-control selectpicker" title="Select City" name="unpracticing_city" id="unpracticing_city">
                        <?php
                        $selected_city = explode(',', $core_profile['unpracticing_city']);
                        foreach ($all_city as $city) {
                            $selected = "";
                            if (in_array($city->id, $selected_city)) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $city->id; ?>" <?php echo $selected; ?>><?php echo $city->name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="unpracticing_city_err"></p>
                </div>
                <div class="form-group pro-first">
                    <label for="unpracticing_fields_of_interest">Fields of interest:*</label>
                    <?php
                    $selected_unpracticing_fields_of_interest = explode(',', $core_profile['unpracticing_fields_of_interest']);
                    ?>
                    <select class="form-control selectpicker" multiple title="Select Specialities" data-max-options="6" name="unpracticing_fields_of_interest[]" id="unpracticing_fields_of_interest" required="">
                        <?php
                        foreach ($catagories as $catagory) {
                            $selected = "";
                            if (in_array($catagory['cid'], $selected_unpracticing_fields_of_interest)) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $catagory['cid']; ?>" <?php echo $selected; ?>><?php echo $catagory['c_name'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="unpracticing_fields_of_interest_err"></p>
                </div>
                <div class="form-group pro-first">
                    <label for="unpracticing_area_of_specialization">Area of Specialization:*</label>
                    <?php
                    $unpracticing_area_of_specialization = explode(',', $core_profile['unpracticing_area_of_specialization']);
                    ?>
                    <select class="form-control selectpicker" multiple title="Select Specialities" data-max-options="6" name="unpracticing_area_of_specialization[]" id="unpracticing_area_of_specialization" required="">
                        <!-- <option value="0" disabled>Select Specialities</option> -->
                        <?php
                        foreach ($catagories as $catagory) {
                            $selected = "";
                            if (in_array($catagory['cid'], $unpracticing_area_of_specialization)) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $catagory['cid']; ?>" <?php echo $selected; ?>><?php echo $catagory['c_name'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="unpracticing_area_of_specialization_err"></p>
                </div>
                <div class="form-group pro-first">
                    <label for="unpracticing_working_as" class=" control-label">Working as:*</label>
                    <div class="check-inner-pp">
                        <div class="check-inner-p">
                            <input type="radio" name="unpracticing_working_as" value="1" id="individual" onclick="unpracticing_working_change(1)" <?php if ($core_profile['unpracticing_working_as'] == "1") {
                                                                                                                                                        echo 'checked';
                                                                                                                                                    } ?>>
                            <label for="individual">Individual</label>
                        </div>
                        <div class="check-inner-p">
                            <input type="radio" name="unpracticing_working_as" value="2" id="organization" onclick="unpracticing_working_change(2)" <?php if ($core_profile['unpracticing_working_as'] == "2") {
                                                                                                                                                        echo 'checked';
                                                                                                                                                    } ?>>
                            <label for="organisation">With an Organization</label>
                        </div>
                    </div>
                    <p class="err" id="unpracticing_working_as_err"></p>
                </div>
                <div class="form-group pro-first organization_div">
                    <label for="organization_name" class="control-label">Organization Name:*</label>
                    <input maxlength="100" class="form-control" name="organization_name" id="organization_name" placeholder="Enter organization name" type="text" required="" value="<?php echo $core_profile['organization_name']; ?>">
                    <p class="err" id="organization_name_err"></p>
                </div>
            </div>
            <div class="practicing" style="display:none;">
                <div class="form-group">
                    <label for="practice_city" class=" control-label">City of Practice:* </label>
                    <select class="form-control selectpicker" data-max-options="2" multiple title="Select City" name="practice_city[]" id="city_practice" required="">
                        <!-- <option value="0" disabled>Select City</option> -->
                        <?php
                        $selected_city = explode(',', $core_profile['practice_city']);
                        foreach ($all_city as $city) {
                            $selected = "";
                            if (in_array($city->id, $selected_city)) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $city->id; ?>" <?php echo $selected; ?>><?php echo $city->name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="city_err"></p>
                </div>
                <div class="form-group pro-first">
                    <label for="Specialities">Area of practice:*</label>
                    <?php
                    $selected_category_id = explode(',', $core_profile['category_id_array']);
                    ?>
                    <select class="form-control selectpicker" multiple title="Select Specialities" data-max-options="6" name="category_id[]" id="category_id" required="">
                        <!-- <option value="0" disabled>Select Specialities</option> -->
                        <?php
                        foreach ($catagories as $catagory) {
                            $selected = "";
                            if (in_array($catagory['cid'], $selected_category_id)) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $catagory['cid']; ?>" <?php echo $selected; ?>><?php echo $catagory['c_name'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="spec_err"></p>
                </div>
                <div class="field_wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="field_court">Add court of practice:*</label>
                                <?php
                                $selected_courts_id = explode(',', $core_profile['field_court_number_array']);
                                ?>
                                <select name="field_court[]" id="field_court" class="form-control selectpicker" data-max-options="6" multiple title="Select Court Name" placeholder="select court name">
                                    <!-- <option value="0" disabled>Select Court Name</option> -->
                                    <option value="others">List my court of practice</option>
                                    <?php foreach ($courts->result() as $court) {
                                        $selected = "";
                                        if (in_array($court->id, $selected_courts_id)) {
                                            $selected = "selected";
                                        }
                                    ?>
                                        <option value="<?php echo $court->id; ?>" <?php echo $selected; ?>><?php echo $court->court_name; ?></option>
                                    <?php } ?>
                                </select>
                                <p class="err" id="court_err"></p>
                            </div>
                        </div>
                    </div>
                    <div id="inputWrapper"></div>
                </div>
                <div class="form-group">
                    <label for="lang" class=" control-label">Add language of practice:*</label>
                    <?php
                    $selected_language_id = explode(',', $core_profile['language']);
                    ?>
                    <select class="form-control selectpicker" multiple title="Select Language" data-max-options="3" name="language[]" id="lang" required="">
                        <?php
                        foreach ($languages as $lang) {
                            $selected = "";
                            if (in_array($lang->ID, $selected_language_id)) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $lang->ID; ?>" <?php echo $selected; ?>><?php echo $lang->language; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="lan_err"></p>
                </div>
                <div class="form-group">
                    <label for="councel_registration_no" class="control-label">State Bar Council Registration no:*</label>
                    <input maxlength="100" class="form-control" name="councel_registration_no" min=1 oninput="validity.valid||(value='');" id="councelregno" placeholder="Enter Registration no" type="text" onkeypress="return valid_registartion_no(event)" required="" value="<?php echo $core_profile['councel_registration_no']; ?>">
                    <p class="err" id="coun_err"></p>
                </div>
                <div class="form-group">
                    <label for="year_reg" class="control-label">State Bar Council Registration Year:*</label>
                    <!-- <input class="form-control" name="year_reg" id="year_reg" placeholder="Enter Year of registration" type="text"  required=""> -->
                    <select class="form-control" name="year_reg" id="year_reg" required="">
                        <option value="0">Select Year</option>
                        <?php
                        $year = date('Y');
                        for ($i = $year; $i >= 1950; $i--) {
                            $selected = "";
                            if ($i == $core_profile['year_reg']) {
                                $selected = "selected";
                            }
                        ?>
                            <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p class="err" id="year_err"></p>
                </div>
                <div class="form-group">
                    <label class="control-label">Upload State Bar Council Registration ID:*</label>
                    <label for="councelregform" class="upload_image_stepper">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </label>
                    <input class="form-control" onchange="readURL_state_bar(this);" name="councelregform" min=1 oninput="validity.valid||(value='');" id="councelregform" placeholder="Upload State Bar Registration ID" type="file" required="">
                    <input type="hidden" name="councelregform_url" id="councelregform_url" value="<?php echo $core_profile['state_bar_form'] ?>">
                    <?php
                    $preview_image = "preview.png";
                    if (!empty($core_profile['state_bar_form'])) {
                        $preview_image = $core_profile['state_bar_form'];
                    }
                    ?>
                    <img id="state_bar" class="preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $preview_image; ?>">
                    <p class="err" id="counform_err"></p>
                </div>
                <div class="form-group">
                    <label for="practice_city" class=" control-label">Years of Practice Experience as an Advocate (cannot exceed years since registration):*</label>
                    <input maxlength="10" class="form-control" name="total_exp" id="total_exp" placeholder="Enter Total Experience" type="text" required="" value="<?php echo $core_profile['total_exp']; ?>" onkeypress="return isNumeric(event)">
                    <p class="err" id="total_exp_err"></p>

                </div>
                <div class="form-group">
                    <label for="registration_no" class="control-label">Are you a member of any Bar Association:*</label>
                    <!-- <input class="form-control" name="registration_no"  min=1  id="regno"  type="radio" required=""> -->
                    <div class=" check-inner-pp">
                        <div class="check-inner-p">
                            <input type="radio" name="registration_member" <?php if ($core_profile['bar_association_member'] == 1) {
                                                                                echo "checked";
                                                                            } ?> value="1" id="chk_yes" onclick="showRegNoDiv(1)">
                            <label for="yes"> Yes</label>

                        </div>
                        <div class="check-inner-p">
                            <input type="radio" name="registration_member" <?php if ($core_profile['bar_association_member'] == 2) {
                                                                                echo "checked";
                                                                            } ?> value="2" id="chk_no" onclick="showRegNoDiv(2)">
                            <label for="no"> No</label>

                        </div>
                    </div>
                    <p class="err" id="bar_member_err"></p>
                </div>
                <div class="bar_asscociation_wrapper">
                    <?php 
                    
                        if ($core_profile['bar_association_member'] == 1) {
                            $bar_association_name = explode(",",json_decode($core_profile['bar_association_name']));
                            
                            // echo "<pre>";
                            // print_r($bar_association_name);
                            // die("hello");
                            $registration_no = explode(",",json_decode($core_profile['registration_no']));
                            $bar_association_form = json_decode($core_profile['bar_association_form']);
                            $bar_association_name_count = count($bar_association_name);
                            for($i = 0; $i < $bar_association_name_count; $i++){
                    ?>
                    <div class="form-group bar_reg">
                        <label for="registration_no" class="control-label">Name of Bar Association:*</label>
                        <input maxlength="100" class="form-control" name="bar_registration_name[]" id="bar_registration_name_0" placeholder="Enter association name" type="text" required="" value="<?php echo $bar_association_name[$i]; ?>">
                        <p class="err" id="reg_bar_name_err_0"></p>
                    </div>
                    <div class="form-group bar_reg">
                        <label for="registration_no" class="control-label">Bar Association Registration no:*</label>
                        <input maxlength="50" class="form-control" name="registration_no[]" min=1 oninput="validity.valid||(value='');" id="regno_0" placeholder="Enter Registration no" type="text" required="" onkeypress="return valid_registartion_no(event)" value="<?php echo $registration_no[$i]; ?>">
                        <p class="err" id="reg_bar_no_err_0"></p>
                    </div>
                    <div class="form-group bar_reg">
                        <label for="councel_registration_no" class="control-label">Upload Bar Association Registration ID:*</label>
                        <label for="barregform_<?php echo $i; ?>" class="upload_image_stepper">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </label>
                        <input class="form-control" onchange="readURL_bar_association(this);" name="barregform[]" min=1 oninput="validity.valid||(value='');" id="barregform_<?php echo $i; ?>" placeholder="Upload Bar Association  Registration Form" type="file" required="" style="display:none;" value="">
                        <img id="barregform_<?php echo $i; ?>_preview" class="preview_image" src="<?php if(!empty($bar_association_form[$i])){ echo base_url("uploads/".$bar_association_form[$i]); }else{ echo base_url('/uploads/preview.png'); } ?>">
                        <p class="err" id="reg_bar_formno_err_<?php echo $i; ?>"></p>
                    </div>
                    <?php
                        }
                    }else{
                        ?>
                            <div class="form-group bar_reg" style="display:none;">
                                <label for="registration_no" class="control-label">Name of Bar Association:*</label>
                                <input maxlength="50" class="form-control" name="bar_registration_name[]" id="bar_registration_name_0" placeholder="Enter association name" type="text" required="" value="">
                                <p class="err" id="reg_bar_name_err_0"></p>
                            </div>
                            <div class="form-group bar_reg" style="display:none;">
                                <label for="registration_no" class="control-label">Bar Association Registration no:*</label>
                                <input maxlength="50" class="form-control" name="registration_no[]" min=1 oninput="validity.valid||(value='');" id="regno_0" placeholder="Enter Registration no" type="text" required="" onkeypress="return valid_registartion_no(event)" value="">
                                <p class="err" id="reg_bar_no_err_0"></p>
                            </div>
                            <div class="form-group bar_reg" style="display:none;">
                                <label for="councel_registration_no" class="control-label">Upload Bar Association Registration ID:*</label>
                                <label for="barregform_0" class="upload_image_stepper">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                </label>
                                <input class="form-control" onchange="readURL_bar_association(this);" name="barregform[]" min=1 oninput="validity.valid||(value='');" id="barregform_0" placeholder="Upload Bar Association  Registration Form" type="file" required="" style="display:none;">
                                <img id="barregform_0_preview" class="preview_image" src="<?php echo base_url('/uploads/preview.png'); ?>">
                                <p class="err" id="reg_bar_formno_err_0"></p>
                            </div>
                        <?php
                    }
                    ?>
                    <div class="form-group bar_add bar_reg" style="display:none">
                        <a href="javascript:void(0);" class="remove_button btn btn-info btn-xs" id="add_bar_association"><span class="glyphicon glyphicon-plus"></span>Add More
                        </a>
                    </div>
                </div>
            </div>

            <div class="practicing" style="display:none;">
                <p><b>DECLARATION:</b> Document upload is required to safeguard our interest in the legitimacy and genuineness of our user community. Lawsmos will not publicly publish your uploaded documents and the same shall remain private and confidential between you and Lawsmos. To know more on how Lawsmos handles your private and confidential information, read our policies on data & information handling, and privacy.</p>
                <p><input type="checkbox" name="agree_checkbox_step_2" id="agree_checkbox_step_2" value="1" checked> I agree that the information submitted by me is true, and in accordance to the terms and conditions of Lawsmos.</p>
                <p class="err" id="agree_checkbox_step_2_err" style="margin-left:0%;"></p>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="overflow:auto;" class="pro-nex-prv">
                        <button type="button" id="nextBtn" class="btn second">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var profile_step = 2;
</script>
<?php include(APPPATH . "/views/user_settings/lawyer_profile/edit_profile_script.php"); ?>
