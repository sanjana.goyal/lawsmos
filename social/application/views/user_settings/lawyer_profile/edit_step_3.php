<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> Profile</div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile (Step 3)</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">Professional profile information: </h5>
        </div>
        <div class="card-body">
            <div class="field_wrappersssss">
                <div class="row">
                    <h4 style="text-align:center;">Educational Information</h4>
                    <h4 style="text-align:center;">School & High School</h4>
                    <p style="text-align:center;"><b>(All fields are mandatory)</b></p>
                    <div class="col-md-6 col-sm-12">
                        <h4>10th</h4>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="degree">Name of School/institute</label>
                                <input maxlength="100" type="text" name="school" id="school_10" class="form-control" value="<?php echo $core_profile['school_10']; ?>" placeholder="Enter School/Institute Name" />
                                <p class="err" id="school_10_err"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="date">Name of Board/University,</label>
                                <input maxlength="100" type="text" class="form-control" name="board_10" id="board_10" class="form-control" placeholder="Enter Board/University Name" value="<?php echo $core_profile['board_10']; ?>"/>
                                <p class="err" id="board_10_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="date">Year of Passing</label>
                                <select class="form-control" name="year_pass_10" id="year_pass_10" required="">
                                    <option value="0">Year of passing</option>
                                    <?php
                                    $year = date('Y');
                                    $selected = "";
                                    for ($i = $year; $i >= 1950; $i--) {
                                        if($core_profile['year_pass_10'] == $i){
                                            $selected = "selected";
                                        }
                                    ?>
                                        <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <p class="err" id="year_pass_10_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="date" style="display:block">Marks :</label>
                                <div class=" check-inner-pp">
                                    <div class="check-inner-p">
                                        <input type="radio" name="marks_x_radio" value="1" id="marks_x_radio" onclick="marks_x_mode(0)" <?php if($core_profile['x_marks_type'] == 1){ echo "checked"; }?>>
                                        <label for="percentage">Percentage</label>
                                    </div>
                                    <div class="check-inner-p">
                                        <input type="radio" name="marks_x_radio" value="2" id="marks_x_radio" onclick="marks_x_mode(1)" <?php if($core_profile['x_marks_type'] == 2){ echo "checked"; }?>>
                                        <label for="divison">Divison</label>
                                    </div>
                                    <div class="check-inner-p">
                                        <input type="radio" name="marks_x_radio" value="3" id="marks_x_radio" onclick="marks_x_mode(2)" <?php if($core_profile['x_marks_type'] == 3){ echo "checked"; }?>>
                                        <label for="cgpa">CGPA</label>
                                    </div>
                                </div>

                                <div class="form-group" id="x_percentage_div" style="display:none">
                                    <label for="university">Percentage</label>
                                    <input type="number" name="marks_x_percentage" class="form-control" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" min="1" max="100" id="marks_x_percentage" placeholder="%" value="<?php echo $core_profile['marks_x_percentage']; ?>">
                                </div>
                                <div class="form-group" id="x_division_div" style="display:none">
                                    <label for="university">Select Division</label>
                                    <select name="x_division" id="x_division" class="form-control">
                                        <option value="0">Select Division</option>
                                        <option value="4" <?php if($core_profile['x_division'] == 1){ echo "selected"; }?>>Gold Medalist</option>
                                        <option value="1" <?php if($core_profile['x_division'] == 1){ echo "selected"; }?>>Ist</option>
                                        <option value="2" <?php if($core_profile['x_division'] == 1){ echo "selected"; }?>>2nd</option>
                                        <option value="3" <?php if($core_profile['x_division'] == 1){ echo "selected"; }?>>3rd</option>
                                    </select>

                                </div>
                                <div class="form-group" id="x_cgpa_div" style="display:none">
                                    <label for="university">CGPA</label>
                                    <input maxlength="10" type="text" name="marks_x_cgpa" class="form-control" id="marks_x_cgpa" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" min="1" max="10" placeholder="CGPA" value="<?php echo $core_profile['marks_x_cgpa']; ?>">
                                </div>
                                <p class="err" id="x_marks_err"></p>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-12">
                        <h4>12th</h4>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="degree">Name of School/ institute</label>
                                <input maxlength="100" type="text" name="school" id="school_12" class="form-control" placeholder="Enter School/ Institute Name" value="<?php echo $core_profile['school_12']; ?>"/>
                                <p class="err" id="school_12_err"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="date">Name of Board/ University,</label>
                                <input maxlength="100" type="text" class="form-control" name="board_12" id="board_12" class="form-control" placeholder="Enter Board/ University Name" value="<?php echo $core_profile['board_12']; ?>"/>
                                <p class="err" id="board_12_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="date">Year of Passing</label>
                                <select class="form-control" name="year_pass_12" id="year_pass_12" required="" disabled="">
                                    <option value="0">Year of passing</option>
                                    <?php
                                    $year = date('Y');
                                    for ($i = $year; $i >= 1950; $i--) {
                                    ?>
                                        <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <p class="err" id="year_pass_12_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="date" style="display:block">Marks :</label>
                                <div class=" check-inner-pp">
                                    <div class="check-inner-p">
                                        <input type="radio" name="marks_xii_radio" value="1" id="marks_12_percentage" onclick="marks_xii__mode(0)" <?php if($core_profile['xii_marks_type'] == 1){ echo "checked"; }?>>
                                        <label for="individual">Percentage</label>
                                    </div>
                                    <div class="check-inner-p">
                                        <input type="radio" name="marks_xii_radio" value="2" id="marks_12_division" onclick="marks_xii__mode(1)" <?php if($core_profile['xii_marks_type'] == 2){ echo "checked"; }?>>
                                        <label for="firm">Divison</label>
                                    </div>
                                    <div class="check-inner-p">
                                        <input type="radio" name="marks_xii_radio" value="3" id="marks_12_cgpa" onclick="marks_xii__mode(2)" <?php if($core_profile['xii_marks_type'] == 3){ echo "checked"; }?>>
                                        <label for="both">CGPA</label>
                                    </div>
                                </div>
                                <div class="form-group" id="xii_percentage_div" style="display:none">
                                    <label for="university">Percentage</label>
                                    <input type="number" name="marks_xii__percentage" class="form-control" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" min="1" max="100" id="marks_xii_percentage" placeholder="%" value="<?php echo $core_profile['marks_xii_percentage']; ?>">
                                </div>
                                <div class="form-group" id="xii_division_div" style="display:none">
                                    <label for="university">Select Division</label>
                                    <select name="xii_division" id="xii_division" class="form-control">
                                        <option value="0">Select Division</option>
                                        <option value="4" <?php if($core_profile['xii_division'] == 1){ echo "selected"; }?>> Gold Medalist</option>
                                        <option value="1" <?php if($core_profile['xii_division'] == 2){ echo "selected"; }?>> Ist</option>
                                        <option value="2" <?php if($core_profile['xii_division'] == 3){ echo "selected"; }?>> 2nd</option>
                                        <option value="3" <?php if($core_profile['xii_division'] == 4){ echo "selected"; }?>> 3rd</option>
                                    </select>

                                </div>
                                <div class="form-group" id="xii_cgpa_div" style="display:none">
                                    <label for="university">CGPA</label>
                                    <input type="text" name="marks_xii_cgpa" class="form-control" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" id="marks_xii__cgpa" min="1" max="10" placeholder="CGPA" value="<?php echo $core_profile['marks_xii_cgpa']; ?>">
                                </div>


                                <p class="err" id="xii_marks_err"></p>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="field_wrappersss professional_qualification">
                <h4 style="text-align:center;">Professional Qualification</h4>
                <?php
                    $degree = json_decode($core_profile['degree_array']);
                    $college = json_decode($core_profile['college_array']);
                    $university = json_decode($core_profile['university_array']);
                    $yearstart = json_decode($core_profile['yearstart_array']);
                    $yearend = json_decode($core_profile['yearend_array']);
                    $division = json_decode($core_profile['division_array']);
                    for ($j=0; $j < count($degree); $j++) {
                ?>
                <div class="row">
                    <div class="col-md-12 professional_qualification_container">
                        
                        <div class="form-group">
                            
                            <label for="degree">Enter Degree/ Course Name</label>
                            <input maxlength="100" type="text" name="degree[]" id="degree" class="form-control" placeholder="Enter Degree/ Course name" value="<?php echo $degree[$j]; ?>"/> 
                            <p class="err" id="degree_err_0"></p>
                        </div>
                        <div class="form-group">
                            <label for="date">Enter College Name</label>
                            <input maxlength="100" type="text" name="college[]" class="form-control" id="college" placeholder="Enter College name" value="<?php echo $college[$j]; ?>"/>
                            <p class="err" id="college_err_0"></p>
                        </div>
                        <div class="form-group">
                            <label for="university">Enter University Name</label>
                            <input maxlength="100" type="text" name="university[]" id="university" class="form-control" placeholder="enter university name" value="<?php echo $university[$j]; ?>"/>
                            <p class="err" id="university_err_0"></p>
                        </div>
                        <div class="form-group">
                            <label for="date">Start Date</label>
                            <select class="form-control" name="yearstart[]" id="yearstart" required="" disabled="">
                                <option value="">Year of passing</option>
                                <?php
                                $year = date('Y');
                                for ($i = $year; $i >= 1950; $i--) {
                                    $selected = "";
                                    if($yearstart[$j] == $i){
                                        $selected = "selected";
                                    }
                                ?>
                                    <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <p class="err" id="yearstart_err_0"></p>
                        </div>
                        <div class="form-group">
                            <label for="date">End Date</label>
                            <select class="form-control" name="yearend[]" id="yearend" required="" disabled="">
                                <option value="">Year of passing</option>
                                <?php
                                $year = date('Y');
                                for ($i = $year; $i >= 1950; $i--) {
                                    $selected = "";
                                    if($yearend[$j] == $i){
                                        $selected = "selected";
                                    }
                                ?>
                                    <option value="<?php echo $i ?>" <?php echo $selected; ?>><?php echo $i ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <p class="err" id="yearend_err_0"></p>
                        </div>

                        <div class="form-group">
                            <label for="university">Select Division</label>
                            <select name="division[]" id="division" class="form-control">
                                <option value="0">Select Division</option>
                                <option value="4" <?php if($division[$j] == "4"){ echo "selected"; } ?>>Gold Medalist</option>
                                <option value="1" <?php if($division[$j] == "1"){ echo "selected"; } ?>>Ist</option>
                                <option value="2" <?php if($division[$j] == "2"){ echo "selected"; } ?>>2nd</option>
                                <option value="3" <?php if($division[$j] == "3"){ echo "selected"; } ?>>3rd</option>
                            </select>
                            <p class="err" id="division_err_0"></p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Upload Marksheet/Degree:*</label>
                            <label for="marksheetform_0" class="upload_image_stepper">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                            </label>
                            <input class="form-control" id="marksheetform_0" onchange="readURL(this,'marksheet_preview_image','marksheetform_url');" name="marksheetform" min="1" oninput="validity.valid||(value='');" placeholder="Upload Marksheet" type="file" required="" style="display:none;">
                            <?php
                            $marksheet_preview_image = "preview.png";
                            if (!empty($core_profile['upload_marksheet_array'])) {
                                $marks = json_decode($core_profile['upload_marksheet_array']);
                                if ($marks) {
                                    $marksheet_preview_image = $marks[$j];
                                } else {
                                    $marksheet_preview_image = $marks;
                                }
                            }
                            ?>
                            <input type="hidden" class="marksheetform_url" name="marksheetform_url[]" value="<?php echo $marksheet_preview_image; ?>">
                            <img class="marksheet_preview_image preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $marksheet_preview_image; ?>">
                            <p class="err" id="marksheet_err_0"></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <a href="javascript:void(0);" class="add_qualification btn btn-info btn-xs"><span class="glyphicon glyphicon-plus"></span> Add More </a>
                </div>

            </div>
            <p>DECLARATION: Document upload is required to safeguard our interest in the legitimacy and genuineness of our user community. Lawsmos will not publicly publish your uploaded documents and the same shall remain private and confidential between you and Lawsmos. To know more on how Lawsmos handles your private and confidential information, read our policies on data & information handling, and privacy.</p>
            <p><input type="checkbox" name="agree_checkbox_step_3" id="agree_checkbox_step_3" value="1" checked> I agree that the information submitted by me is true, and in accordance to the terms and conditions of Lawsmos.</p>
            <p class="err" id="agree_checkbox_step_3_err" style="margin-left:0%;"></p>
            <div style="overflow:auto;" class="pro-nex-prv">
                <button type="button" id="nextBtn" class="btn third">Update</button>
            </div>
        </div>
    </div>
</div>

<script>
    var profile_step = 3;

$(document).ready(function(){
  $("#year_pass_10").change(function(){
    var year_pass10 = $("#year_pass_10").val();
    var current_year =  new Date().getFullYear();
    var html = ' <option value="0">Year of passing</option>';
    for (var i = year_pass10; i <= current_year; i++) {
      html = html+ '<option value="'+i+'">'+i+'</option>';
       
    }
    // alert(html);
    $("#year_pass_12").html(html);
     $("#year_pass_12").prop("disabled", false);
  });
  $("#year_pass_12").change(function(){
    var year_pass10 = $("#year_pass_12").val();
    var current_year =  new Date().getFullYear();
    var html = ' <option value="0">Year of passing</option>';
    for (var i = year_pass10; i <= current_year; i++) {
      html = html+ '<option value="'+i+'">'+i+'</option>';
       
    }
    // alert(html);
    $("#yearstart").html(html);
     $("#yearstart").prop("disabled", false);
  });
   $("#yearstart").change(function(){
    var year_pass10 = $("#yearstart").val();
    var current_year =  new Date().getFullYear();
    var html = ' <option value="0">Year of passing</option>';
    for (var i = year_pass10; i <= current_year; i++) {
      html = html+ '<option value="'+i+'">'+i+'</option>';
       
    }
    // alert(html);
    $("#yearend").html(html);
     $("#yearend").prop("disabled", false);
  });
});
</script>
<?php include(APPPATH . "/views/user_settings/lawyer_profile/edit_profile_script.php"); ?>
