<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> Profile</div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile (Step 4)</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">Professional Experience: </h5>
        </div>
        <div class="card-body">
            <div class="field_wrapper_experience">
                <div class="row">
                    <?php
                    $all_results = "";
                    if (!empty($core_profile['experience_organization_name'])) {
                        $all_results = json_decode($core_profile['experience_organization_name']);
                    }
                    $row_count = 0;
                    if (is_array($all_results)) {
                        $row_count = count($all_results);
                    }
                    ?>
                    <?php
                    if ($row_count == 0) {
                    ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="experience_organization_name">Name of organization:*</label>
                                <input maxlength="100" type="text" name="experience_organization_name[]" id="experience_organization_name" class="form-control" placeholder="Enter organization name" value="" />
                                <input type="hidden" id="previous_experience_count" name="previous_experience_count" value="1">
                                <p class="err" id="experience_organization_name_err_0"></p>
                            </div>
                            <div class="form-group">
                                <label for="experience_city" class="control-label">City:*</label>
                                <select class="form-control" title="Select City" name="experience_city[]" id="experience_city">
                                    <option value="">Select City</option>
                                    <?php
                                    $selected_city = explode(',', $core_profile['experience_city']);
                                    foreach ($all_city as $city) {
                                        $selected = "";
                                        if (in_array($city->id, $selected_city)) {
                                            $selected = "selected";
                                        }
                                    ?>
                                        <option value="<?php echo $city->id; ?>" <?php echo $selected; ?>><?php echo $city->name; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <p class="err" id="experience_city_err_0"></p>
                            </div>
                            <div class="form-group">
                                <label for="experience_court">Court (if applicable):*</label>
                                <?php
                                $selected_courts_id = explode(',', $core_profile['experience_court']);
                                ?>
                                <select name="experience_court[]" id="experience_court" class="form-control" title="Select Court Name" placeholder="select court name">
                                    <option value="">Select Court</option>
                                    <?php foreach ($courts->result() as $court) {
                                        $selected = "";
                                        if (in_array($court->id, $selected_courts_id)) {
                                            $selected = "selected";
                                        }
                                    ?>
                                        <option value="<?php echo $court->id; ?>" <?php echo $selected; ?>><?php echo $court->court_name; ?></option>
                                    <?php } ?>
                                </select>
                                <p class="err" id="experience_court_err_0"></p>
                            </div>
                            <div class="form-group">
                                <label for="experience_designation">Designation:*</label>
                                <select name="experience_designation[]" id="experience_designation" class="form-control" title="">
                                    <option value="">Select Designation</option>
                                    <option value="1">Designated Senior</option>
                                    <option value="2">Retired Justice</option>
                                </select>
                                <p class="err" id="experience_designation_err_0"></p>

                            </div>
                            <div class="form-group">
                                <label for="experience_start_date">Start Date:*</label>
                                <input type="date" class="form-control" id="experience_start_date" name="experience_start_date[]" class="form-control" />
                                <p class="err" id="experience_start_date_err_0"></p>
                            </div>
                            <div class="form-group">
                                <label for="experience_end_date">End Date (If you currently work here please don't select end date) :</label>
                                <input type="date" class="form-control" id="experience_end_date" name="experience_end_date[]" class="form-control" />
                                <p class="err" id="experience_end_date_err_0"></p>
                            </div>
                        </div>
                        <?php
                    } elseif ($row_count > 0) {
                        $experience_organization_name = json_decode($core_profile['experience_organization_name']);
                        $experience_city = json_decode($core_profile['experience_city']);
                        $experience_court = json_decode($core_profile['experience_court']);
                        $experience_designation = json_decode($core_profile['experience_designation']);
                        $experience_start_date = json_decode($core_profile['experience_start_date']);
                        $experience_end_date = json_decode($core_profile['experience_end_date']);
                        echo '<input type="hidden" id="previous_experience_count" name="previous_experience_count" value="' . $row_count . '">';
                        for ($i = 0; $i < $row_count; $i++) {
                        ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="experience_organization_name">Name of organization:*</label>
                                    <input maxlength="100" type="text" name="experience_organization_name[]" id="experience_organization_name" class="form-control" placeholder="Enter organization name" value="<?php echo $experience_organization_name[$i] ?>" />
                                    <p class="err" id="experience_organization_name_err_<? echo $i; ?>"></p>
                                </div>
                                <div class="form-group">
                                    <label for="experience_city" class="control-label">City:*</label>
                                    <select class="form-control" title="Select City" name="experience_city[]" id="experience_city">
                                        <option value="">Select City</option>
                                        <?php
                                        foreach ($all_city as $city) {
                                            $selected = "";
                                            if ($city->id == $experience_city[$i]) {
                                                $selected = "selected";
                                            }
                                        ?>
                                            <option value="<?php echo $city->id; ?>" <?php echo $selected; ?>><?php echo $city->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <p class="err" id="experience_city_err_<? echo $i; ?>"></p>
                                </div>
                                <div class="form-group">
                                    <label for="experience_court">Court (if applicable):*</label>
                                    <select name="experience_court[]" id="experience_court" class="form-control" title="Select Court Name" placeholder="select court name">
                                        <option value="">Select Court</option>
                                        <?php foreach ($courts->result() as $court) {
                                            $selected = "";
                                            if ($court->id == $experience_court[$i]) {
                                                $selected = "selected";
                                            }
                                        ?>
                                            <option value="<?php echo $court->id; ?>" <?php echo $selected; ?>><?php echo $court->court_name; ?></option>
                                        <?php } ?>
                                    </select>
                                    <p class="err" id="experience_court_err_<? echo $i; ?>"></p>
                                </div>
                                <div class="form-group">
                                    <label for="experience_designation">Designation:*</label>
                                    <select name="experience_designation[]" id="experience_designation" class="form-control" title="">
                                        <option value="">Select Designation</option>
                                        <option value="1" <?php if ($experience_designation[$i] == 1) {
                                                                echo "selected";
                                                            } ?>> Designated Senior</option>
                                        <option value="2" <?php if ($experience_designation[$i] == 2) {
                                                                echo "selected";
                                                            } ?>>Retired Justice</option>
                                    </select>
                                    <p class="err" id="experience_designation_err_<? echo $i; ?>"></p>

                                </div>
                                <div class="form-group">
                                    <label for="experience_start_date">Start Date:*</label>
                                    <input type="date" class="form-control" id="experience_start_date" name="experience_start_date[]" class="form-control" value="<?php echo $experience_start_date[$i] ?>" />
                                    <p class="err" id="experience_start_date_err_<? echo $i; ?>"></p>
                                </div>
                                <div class="form-group">
                                    <label for="experience_end_date">End Date (If you currently work here please don't select end date) :</label>
                                    <input type="date" class="form-control" id="experience_end_date" name="experience_end_date[]" class="form-control" value="<?php echo $experience_end_date[$i] ?>" />
                                    <p class="err" id="experience_end_date_err_<? echo $i; ?>"></p>
                                </div>
                            </div>
                    <?php
                        }
                    }
                    ?>

                    <div class="col-md-12">
                        <div class="form-group">
                            <a href="javascript:void(0);" class="add_experience btn btn-info btn-xs"><span class="glyphicon glyphicon-plus"></span> Add More </a>
                        </div>
                    </div>
                </div>
            </div>
            <div style="overflow:auto;" class="pro-nex-prv">
                <button type="button" id="nextBtn" class="btn fourth">Update</button>
            </div>
        </div>
    </div>
</div>

<script>
    var profile_step = 4;
</script>
<?php include(APPPATH . "/views/user_settings/lawyer_profile/edit_profile_script.php"); ?>