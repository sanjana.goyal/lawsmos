<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> Profile</div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb ">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile (Step 5)</li>
    </ol>

    <?php
    $monday_checkbox = "";
    $monday_start_hour = "success";
    $monday_start_min = "";
    $monday_end_hour = "";
    $monday_end_min = "";

    $tuesday_checkbox = "";
    $tuesday_start_hour = "";
    $tuesday_start_min = "";
    $tuesday_end_hour = "";
    $tuesday_end_min = "";

    $wednesday_checkbox = "";
    $wednesday_start_hour = "";
    $wednesday_start_min = "";
    $wednesday_end_hour = "";
    $wednesday_end_min = "";

    $thursday_checkbox = "";
    $thursday_start_hour = "";
    $thursday_start_min = "";
    $thursday_end_hour = "";
    $thursday_end_min = "";

    $friday_checkbox = "";
    $friday_start_hour = "";
    $friday_start_min = "";
    $friday_end_hour = "";
    $friday_end_min = "";

    $saturday_checkbox = "";
    $saturday_start_hour = "";
    $saturday_start_min = "";
    $saturday_end_hour = "";
    $saturday_end_min = "";

    $sunday_checkbox = "";
    $sunday_start_hour = "";
    $sunday_start_min = "";
    $sunday_end_hour = "";
    $sunday_end_min = "";

    $days_count = 0;
    if (!empty($consultation_timing)) {
        $days_count = count($consultation_timing);
        foreach ($consultation_timing as $timing) {
            ${$timing['day_name'] . "_checkbox"} = "checked";
            ${$timing['day_name'] . "_start_hour"} = $timing['start_hour'];
            ${$timing['day_name'] . "_start_min"} = $timing['start_min'];
            ${$timing['day_name'] . "_end_hour"} = $timing['end_hour'];
            ${$timing['day_name'] . "_end_min"} = $timing['end_min'];
        }
    }

    ?>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">Consultation Information: </h5>
        </div>
        <div class="card-body">
            <div class="field_wrapper_experience">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group consult_time">
                            <label for="registration_no" class="control-label">CONSULTATION TIMINGS: *</label>
                            <div class="col-md-12 col-xs-12">
                                <div class="row ">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-check_pp all-day-check">
                                            <h6>Days</h6>
                                            <input type="checkbox" id="consultation_day_all" value="all_days" name="consultation_day_all" <?php if($days_count == 7){ echo "checked"; } ?>>
                                            <span class="day_timing"><label for="monday">All Days</label></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="consult_hrs">
                                            <h6>From</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="consult_hrs">
                                            <h6>To</h6>
                                            <div class="copy_to_all"><a href="javascript:void(0);" class="btn" id="copy_all_days">Copy To All</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-check_pp">
                                        <h6 class="visible-xs">Days</h6>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" id="consultation_day_mon" value="monday" name="consultation_day" <?php echo $monday_checkbox; ?>>
                                        <span class="day_timing" ><label for="monday">Monday </label></span>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" name="consultation_day" id="consultation_day_tue" value="tuesday" <?php echo $tuesday_checkbox; ?>>
                                        <span class="day_timing"><label for="30 min">Tuesday</label></span>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" name="consultation_day" id="consultation_day_wed" value="wednesday" <?php echo $wednesday_checkbox; ?>>
                                        <span class="day_timing"><label for="30 min">Wednesday</label></span>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" name="consultation_day" id="consultation_day_thu" value="thursday" <?php echo $thursday_checkbox; ?>>
                                        <span class="day_timing"><label for="30 min">Thursday</label></span>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" name="consultation_day" id="consultation_day_fri" value="friday" <?php echo $friday_checkbox; ?>>
                                        <span class="day_timing"><label for="30 min">Friday</label></span>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" name="consultation_day" id="consultation_day_sat" value="saturday" <?php echo $saturday_checkbox; ?>>
                                        <span class="day_timing"><label for="30 min">Saturday</label></span>
                                    </div>
                                    <div class="form-check_pp">
                                        <input type="checkbox" class="chk" name="consultation_day" id="consultation_day_sun" value="sunday" <?php echo $sunday_checkbox; ?>>
                                        <span class="day_timing"><label for="30 min">Sunday</label></span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="consult_hrs">
                                        <h6 class="visible-xs">From</h6>
                                        <select class="start_day_hour" name="consultation_day_time_mon" id="monday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $monday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_mon" id="monday_from_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $monday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="start_day_hour" name="consultation_day_time_tue" id="tuesday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $tuesday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_tue" id="tuesday_from_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $tuesday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="start_day_hour" name="consultation_day_time_wed" id="wednesday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $wednesday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_wed" id="wednesday_from_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $wednesday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="start_day_hour" name="consultation_day_time_thu" id="thursday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $thursday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_thu" id="thursday_from_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $thursday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="start_day_hour" name="consultation_day_time_fri" id="friday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $friday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_fri" id="friday_from_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $friday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="start_day_hour" name="consultation_day_time_sat" id="saturday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $saturday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_sat" id="saturday_from_min">
                                            <option>Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $saturday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="start_day_hour" name="consultation_day_time_sun" id="sunday_from_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $sunday_start_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="start_day_min" name="consultation_day_time_sun" id="sunday_from_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $sunday_start_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="consult_hrs">
                                        <h6 class="visible-xs">To</h6>
                                        <select class="end_day_hour" name="to_consultation_day_time" id="monday_to_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $monday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="monday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $monday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="end_day_hour" name="to_consultation_day_time" id="tuesday_to_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $tuesday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="tuesday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $tuesday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="end_day_hour" name="to_consultation_day_time" id="wednesday_to_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $wednesday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="wednesday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $wednesday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="end_day_hour" name="to_consultation_day_time" id="thursday_to_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $thursday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="thursday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $thursday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="end_day_hour" name="to_consultation_day_time" id="friday_to_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $friday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="friday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $friday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="end_day_hour" name="to_consultation_day_time" id="saturday_to_hour">
                                            <option>Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $saturday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="saturday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $saturday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="consult_hrs">
                                        <select class="end_day_hour" name="to_consultation_day_time" id="sunday_to_hour">
                                            <option value="">Hour</option>
                                            <?php
                                            for ($i = 0; $i <= 23; $i++) {
                                                $selected = "";
                                                if($i == $sunday_end_hour){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <select class="end_day_min" name="to_consultation_day_time" id="sunday_to_min">
                                            <option value="">Mins</option>
                                            <?php for ($i = 0; $i <= 59; $i++) {
                                                $selected = "";
                                                if($i == $sunday_end_min){
                                                    $selected = "selected";
                                                }
                                            ?>
                                                <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p class="err" id="day_time_charges_err"></p>
                        <div class="form-group">
                            <label for="registration_no" class="control-label">IN OFFICE CONSULTATION*</label>
                            <div class="check-inner-pp">
                                <div class="check-inner-p">
                                    <input type="radio" name="office_consultation" value="1" id="office_consultation_yes" onclick="office_consultation_div(1)" <?php if($core_profile['office_consultation_type'] == "1") { echo "checked"; } ?>>
                                    <label for="individual">Yes</label>
                                </div>
                                <div class="check-inner-p">
                                    <input type="radio" name="office_consultation" value="0" id="office_consultation_no" onclick="office_consultation_div(0)" <?php if($core_profile['office_consultation_type'] == "0") { echo "checked"; } ?>>
                                    <label for="firm">No</label>
                                </div>
                            </div>
                            <div class="check-inner-pp check-inner-pp-2 office_consultation_checkbox_div" style="display:none">
                                <div class="check__inner">
                                    <input type="checkbox" name="office_charges_checkbox_30_min" id="office_charges_checkbox_30_min" value="30min" <?php if($core_profile['office_charges_checkbox_30_min'] == "30min") { echo "checked"; } ?>>
                                    <label for="30 min"> 30 min</label>
                                    <input class="form-control" name="office_charges" id="office_charges_30min" placeholder="Enter 30 Min consultation charges" max="999999999" type="number" value="<?php echo $core_profile['office_charges_30min']; ?>" required=""> &nbsp;in Rs.
                                </div>
                                <div class="check__inner">
                                    <input type="checkbox" name="office_charges_checkbox_1_hour" id="office_charges_checkbox_1_hour" value="1hour" <?php if($core_profile['office_charges_checkbox_1hour'] == "1hour") { echo "checked"; } ?>>
                                    <label for="vehicle1">1 Hour</label>
                                    <input class="form-control" name="office_charges" id="office_charges_1hour" min="0" placeholder="Enter 1 Hour consultation charges" max="999999999" type="number" value="<?php echo $core_profile['office_charges_1hour']; ?>" required=""> &nbsp; in Rs.
                                </div>
                            </div>
                            <p class="err" id="office_charges_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="registration_no" class="control-label">Client SITE CONSULTATION*</label>
                            <div class="check-inner-pp">
                                <div class="check-inner-p">
                                    <input type="radio" name="site_consultation" value="1" id="site_consultation_yes" onclick="site_consultation_div(1)" <?php if($core_profile['site_consultation_type'] == "1") { echo "checked"; } ?>>
                                    <label for="individual">Yes</label>
                                </div>
                                <div class="check-inner-p">
                                    <input type="radio" name="site_consultation" value="0" id="site_consultation_no" onclick="site_consultation_div(0)" <?php if($core_profile['site_consultation_type'] == "0") { echo "checked"; } ?>>
                                    <label for="firm">No</label>
                                </div>
                            </div>
                            <div class="check-inner-pp check-inner-pp-2 site_consultation_checkbox_div" style="display:none">
                                <div class="check__inner">
                                    <input type="checkbox" name="site_charges_30_min" id="site_charges_checkbox_30min" value="30min" <?php if($core_profile['site_charges_checkbox_30_min'] == "30min") { echo "checked"; } ?>>
                                    <label for="vehicle1"> 30 min</label>
                                    <input class="form-control" name="site_charges_30min" id="site_charges_30min" placeholder="Enter 30 Min consultation charges" max="999999999" type="number" value="<?php echo $core_profile['site_charges_30min']; ?>" required=""> &nbsp; in Rs.
                                </div>
                                <div class="check__inner">
                                    <input type="checkbox" name="site_charges_1_hour" id="site_charges_checkbox_1hour" value="1hour" <?php if($core_profile['site_charges_checkbox_1hour'] == "1hour") { echo "checked"; } ?>>
                                    <label for="vehicle1">1 Hour</label>
                                    <input class="form-control" name="site_charges_1_hour" id="site_charges_1hour" placeholder="Enter 1 Hour consultation charges" max="999999999" type="number" value="<?php echo $core_profile['site_charges_1hour']; ?>" required=""> &nbsp; in Rs.
                                </div>
                                <div class="form-group">
                                    <label for="registration_no" style="margin-top:10px;width:100%;" class="control-label" style="width:100%;">Additional Travel Allowance*</label>
                                    <div class="check-inner-p">
                                        <input type="radio" name="travel_consultation" value="1" id="travel_consultation_yes" onclick="travel_consultation_div(1)" <?php if($core_profile['travel_consultation'] == "1") { echo "checked"; } ?>>
                                        <label for="individual">Yes</label>
                                    </div>
                                    <div class="check-inner-p">
                                        <input type="radio" name="travel_consultation" value="0" id="travel_consultation_no" onclick="travel_consultation_div(0)" <?php if($core_profile['travel_consultation'] == "0") { echo "checked"; } ?>>
                                        <label for="individual">No</label>
                                    </div>
                                    <div class="form-group travel_consultation_charges" style="display:none;">
                                        <input class="form-control" name="travel_consultation_charges" id="travel_consultation_charges" placeholder="Enter additional travel consultation charges" max="999999999" type="number" value="" required="">&nbsp; in Rs.
                                    </div>
                                </div>
                            </div>
                            <p class="err" id="site_charges_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="councel_registration_no" class="control-label">Telephonic Consultation (Voice)*</label>
                            <div class="check-inner-pp">
                                <div class="check-inner-p">
                                    <input type="radio" name="telvoice_consultation" value="1" id="telvoice_consultation_yes" onclick="telvoice_consultation_div(1)" <?php if($core_profile['telvoice_consultation_type'] == "1") { echo "checked"; } ?>>
                                    <label for="individual">Yes</label>
                                </div>
                                <div class="check-inner-p">
                                    <input type="radio" name="telvoice_consultation" value="0" id="telvoice_consultation_no" onclick="telvoice_consultation_div(0)" <?php if($core_profile['telvoice_consultation_type'] == "0") { echo "checked"; } ?>>
                                    <label for="firm">No</label>
                                </div>
                            </div>
                            <div class=" check-inner-pp check-inner-pp-2 telvoice_consultation_checkbox_div" style="display:none">
                                <div class="check__inner">
                                    <input type="checkbox" name="telvoice_charges_30min" id="telvoice_charge_checkbox_30min" <?php if($core_profile['telvoice_charges_checkbox_30_min'] == "on") { echo "checked"; } ?>>
                                    <label for="vehicle1"> 30 min</label>
                                    <input class="form-control" name="telvoice_charges" id="telvoice_charges_30min" placeholder="Enter 30 Min consultation charges" max="999999999" type="number" value="<?php echo $core_profile['telvoice_charges_30min']; ?>" required="">&nbsp; in Rs.
                                </div>
                                <div class="check__inner">
                                    <input type="checkbox" name="telvoice_charges_1hour" id="telvoice_charge_checkbox_1hour" <?php if($core_profile['telvoice_charges_checkbox_1hour'] == "on") { echo "checked"; } ?>>
                                    <label for="vehicle1">1 Hour</label>
                                    <input class="form-control" name="telvoice_charges" id="telvoice_charges_1hour" min="0" oninput="validity.valid||(value='');" placeholder="Enter 1 Hour consultation charges" max="999999999" type="number" value="<?php echo $core_profile['telvoice_charges_1hour']; ?>" required="">&nbsp; in Rs.
                                </div>
                            </div>
                            <p class="err" id="telephone_charges_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="councel_registration_no" class="control-label">Telephonic Consultation (Video)*</label>
                            <div class="check-inner-pp">
                                <div class="check-inner-p">
                                    <input type="radio" name="telvideo_consultation" value="1" id="telvideo_consultation_yes" onclick="telvideo_consultation_div(1)" <?php if($core_profile['telvideo_consultation_type'] == "1") { echo "checked"; } ?>>
                                    <label for="individual">Yes</label>
                                </div>
                                <div class="check-inner-p">
                                    <input type="radio" name="telvideo_consultation" value="0" id="telvideo_consultation_yes" onclick="telvideo_consultation_div(0)" <?php if($core_profile['telvideo_consultation_type'] == "0") { echo "checked"; } ?>>
                                    <label for="firm">No</label>
                                </div>
                            </div>
                            <div class=" check-inner-pp check-inner-pp-2 telvideo_consultation_checkbox_div" style="display:none">
                                <div class="check__inner">
                                    <input type="checkbox" name="telvideo_charges_30" id="telvideo_charge_checkbox_30min" <?php if($core_profile['telvideo_charges_checkbox_30_min'] == "on") { echo "checked"; } ?>>
                                    <label for="vehicle1"> 30 min</label>
                                    <input class="form-control" name="telvideo_charges" id="telvideo_charges_30min" placeholder="Enter 30 Min consultation charges" max="999999999" type="number" value="<?php echo $core_profile['telvideo_charges_30min']; ?>" required=""> &nbsp; in Rs.
                                </div>
                                <div class="check__inner">
                                    <input type="checkbox" name="telvideo_charges_1hour" id="telvideo_charge_checkbox_1hour" <?php if($core_profile['telvideo_charges_checkbox_1hour'] == "on") { echo "checked"; } ?>>
                                    <label for="vehicle1">1 Hour</label>
                                    <input class="form-control" name="telvideo_charges" id="telvideo_charges_1hour" placeholder="Enter 1 Hour consultation charges" max="999999999" type="number" value="<?php echo $core_profile['telvideo_charges_1hour']; ?>" required=""> &nbsp; in Rs.
                                </div>
                            </div>
                            <p class="err" id="televideo_charges_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="year_reg" class="control-label">Email consultation charges:*</label>
                            <div class="check-inner-pp">
                                <div class="check-inner-p">
                                    <input type="radio" name="email_consultation" value="1" id="email_consultation_yes" onclick="email_consultation_div(1)" <?php if($core_profile['email_consultation_type'] == "1") { echo "checked"; } ?>>
                                    <label for="individual">Yes</label>
                                </div>
                                <div class="check-inner-p">
                                    <input type="radio" name="email_consultation" value="0" id="email_consultation_yes" onclick="email_consultation_div(0)" <?php if($core_profile['email_consultation_type'] == "0") { echo "checked"; } ?>>
                                    <label for="firm">No</label>
                                </div>
                            </div>
                            <div class="form-group email_consultation_div" style="display:none">
                                <label for="">Amount Per Reply</label>
                                <input max="999999999" type="number" name="email_consultation_changes" id="email_consultation_changes" value="<?php echo $core_profile['email_consultation_charges']; ?>"> &nbsp; in Rs.
                            </div>
                            <p class="err" id="email_charges_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="apperance_charges" class=" control-label">Appearance charges*</label>
                            <input class="form-control" name="apperance_charges" id="apperance_charges" min=0 oninput="validity.valid||(value='');" placeholder="Enter appearance charges" max="999999999" type="number" value="<?php echo $core_profile['apperance_charges']; ?>" required=""> &nbsp; in Rs.
                            <p class="err" id="apperance_charges_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="haur_draft_charges" class=" control-label">Hourly Drafting Charges*</label>
                            <input class="form-control" max="999999999" name="haur_draft_charges" id="haur_draft_charges" min=0 oninput="validity.valid||(value='');" placeholder="Enter drafting charges" type="number" value="<?php echo $core_profile['haur_draft_charges']; ?>" required=""> &nbsp; in Rs.
                            <p class="err" id="haur_draft_charges_err"></p>
                        </div>
                        <div class="col-md-12">
                            <div style="overflow:auto;" class="pro-nex-prv">
                                <button type="button" id="nextBtn" class="btn fifth">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var profile_step = 5;
</script>
<?php include(APPPATH . "/views/user_settings/lawyer_profile/edit_profile_script.php"); ?>