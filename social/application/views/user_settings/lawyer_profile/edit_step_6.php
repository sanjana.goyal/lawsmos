<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> Profile</div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile (Step 6)</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">Address Details: </h5>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if ($core_profile['lawyer_practitioner'] == "2" || $core_profile['lawyer_practitioner'] == "3") {
                    ?>
                        <div class="firm_address" id="firm_address">
                            <h4>Firm Address</h4>
                            <div class="form-group">
                                <label for="firm_address_1">Address 1</label>
                                <input maxlength="100" type="text" class="form-control" name="firm_address_1" id="firm_address_1" value="<?php echo $user_info['firm_address_1']; ?>" placeholder="Enter firm address 1">
                                <p class="err" id="firm_address_1_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="firm_address_2">Address 2</label>
                                <input maxlength="100" type="text" class="form-control" name="firm_address_2" id="firm_address_2" value="<?php echo $user_info['firm_address_2']; ?>" placeholder="Enter firm address 2">
                                <p class="err" id="firm_address_2_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="firm_address_3">Address 3</label>
                                <input maxlength="100" type="text" class="form-control" name="firm_address_3" id="firm_address_3" value="<?php echo $user_info['firm_address_3']; ?>" placeholder="Enter firm address 3">
                                <p class="err" id="firm_address_3_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="firm_state"><?php echo lang("ctn_394") ?></label>
                                <select class="form-control" name="firm_state" id="firm_state" onchange="getCity(1)">
                                
                                    <option value="">Select State</option>
                                    <?php 
                                        $firm_state = $user_info['firm_state'];
                                        foreach ($states as $all_states) : 
                                            $selected = "";
                                            
                                            if($firm_state == $all_states->id){
                                                $selected = "selected";
                                            }
                                    ?>
                                        <option value="<?php echo $all_states->id ?>" <?php echo $selected; ?>><?php echo $all_states->name; ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <p class="err" id="firm_state_err"></p>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3"><?php echo lang("ctn_393") ?></label>
                                <input type="hidden" name="hidden_firm_city" id="hidden_firm_city" value="<?php echo $user_info["firm_city"]; ?>">
                                <select class="form-control firm_dropdown_state" name="firm_city" id="firm_city">
                                    <option value="">Select City</option>
                                </select>
                                <p class="err" id="firm_city_err"></p>
                            </div>

                            <div class="form-group">
                                <label for="firm_country"><?php echo lang("ctn_863") ?></label>
                                <input maxlength="100" type="text" class="form-control" name="firm_country" id="firm_country" value="India" placeholder="Enter firm country name" disabled>
                                <p class="err" id="firm_country_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="firm_lat">Latitude</label>
                                <input maxlength="100" type="text" class="form-control" name="firm_lat" id="firm_lat" value="<?php echo $user_info['firm_lat']; ?>" placeholder="Enter firm latitude">
                                <p class="err" id="firm_lat_err"></p>
                            </div>
                            <div class="form-group">
                                <label for="firm_lng">Longitude</label>
                                <input maxlength="100" type="text" class="form-control" name="firm_lng" id="firm_lng" value="<?php echo $user_info['firm_lng']; ?>" placeholder="Enter firm longitude">
                                <p class="err" id="firm_lng_err"></p>

                            </div>
                            <div class="form-group">
                                <label for="firm_pincode">Pincode</label>
                                <input maxlength="100" type="text" class="form-control" name="firm_pincode" id="firm_pincode" value="<?php echo $user_info['firm_pincode']; ?>" placeholder="Enter residence pincode">
                                <p class="err" id="firm_pincode_err"></p>

                            </div>
                        </div>
                    <?php } ?>
                    <div>
                        <h4>Office Address 1</h4>
                        <div class="form-group">
                            <label for="inputEmail3">Name</label>
                            <input maxlength="100" type="text" class="form-control" name="address1_name" id="address1_name" value="<?php echo $user_info['address1_name']; ?>" placeholder="Enter Name">
                            <p class="err" id="address1_name_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Contact No</label>
                            <input maxlength="100" type="text" class="form-control" name="address1_contact_no" id="address1_contact_no" value="<?php echo $user_info['address1_contact_no']; ?>" placeholder="Enter Contact No">
                            <p class="err" id="address1_contact_no_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Address 1</label>
                            <input maxlength="100" type="text" class="form-control" name="permannentadr" id="address1" value="<?php echo $user_info['address_1']; ?>" placeholder="Enter address 1">
                            <p class="err" id="address1_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Address 2</label>
                            <input maxlength="100" type="text" class="form-control" name="permannentadr" id="address2" value="<?php echo $user_info['address_2']; ?>" placeholder="Enter address 2">
                            <p class="err" id="address2_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Address 3</label>
                            <input maxlength="100" type="text" class="form-control" name="permannentadr" id="address3" value="<?php echo $user_info['address3']; ?>" placeholder="Enter address 3">
                            <p class="err" id="address3_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3"><?php echo lang("ctn_863") ?></label>
                            <input maxlength="100" type="text" class="form-control" name="location_country" id="location_country" value="India" placeholder="Enter country name" disabled>
                            <p class="err" id="location_country_err"></p>

                        </div>
                        <div class="form-group">
                            <label for="inputEmail3"><?php echo lang("ctn_394") ?></label>
                            <select class="form-control" name="location_state" id="location_state" onchange="getCity(0)">
                                <option value="">Select State</option>
                                <?php 
                                        $location_state = $user_info['state'];
                                        foreach ($states as $all_states) : 
                                            $selected = "";
                                            
                                            if($location_state == $all_states->id){
                                                $selected = "selected";
                                            }
                                    ?>
                                    <option value="<?php echo $all_states->id ?>" <?php echo $selected; ?>><?php echo $all_states->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <p class="err" id="location_state_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3"><?php echo lang("ctn_393") ?></label>
                            <input type="hidden" name="hidden_location_city" id="hidden_location_city" value="<?php echo $user_info["city"]; ?>">
                            <select class="form-control" name="location_city" id="location_city">
                                <option value="">Select City</option>
                            </select>
                            <p class="err" id="location_city_err"></p>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3">Latitude</label>
                            <input maxlength="100" type="text" class="form-control" name="lat" id="lat" value="<?php echo $user_info['lat']; ?>" placeholder="Enter latitude">
                            <p class="err" id="lat_err"></p>

                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Longitude</label>
                            <input maxlength="100" type="text" class="form-control" name="lng" id="lng" value="<?php echo $user_info['longitude']; ?>" placeholder="Enter longitude">
                            <p class="err" id="lng_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Pincode</label>
                            <input maxlength="100" type="text" class="form-control" name="pincode" id="pincode" value="<?php echo $user_info['zipcode']; ?>" placeholder="Enter  pincode">
                            <p class="err" id="pincode_err"></p>
                        </div>
                        <input type="hidden" name="new_address" id="new_address" value="0">
                        <!-- <div class="text-center">
                            <button type="button" onclick="add_new_address()" class="btn btn-primary" id="add_address">Add another address</button>
                        </div> -->
                    </div>
                    <div class="office_address_2" id="office_address_2" style="display:block;">
                        <h4>Office Address 2 (Optional)</h4>
                        <div class="form-group">
                            <label for="inputEmail3">Name</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_address1_name" id="tmp_address1_name" value="<?php echo $user_info['tmp_address1_name']; ?>" placeholder="Enter Name">
                            <p class="err" id="tmp_address1_name_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">Contact No</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_address1_contact_no" id="tmp_address1_contact_no" value="<?php echo $user_info['tmp_address1_contact_no']; ?>" placeholder="Enter Contact No">
                            <p class="err" id="tmp_address1_contact_no_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="tmp_address1">Address 1</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_address1" id="tmp_address1" value="<?php echo $user_info['tmp_address1']; ?>" placeholder="Enter address 1">
                            <p class="err" id="tmp_address1_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="tmp_address2">Address 2</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_address2" id="tmp_address2" value="<?php echo $user_info['tmp_address2']; ?>" placeholder="Enter address 2">
                            <p class="err" id="tmp_address2_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="tmp_address3">Address 3</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_address3" id="tmp_address3" value="<?php echo $user_info['tmp_address3']; ?>" placeholder="Enter address 3">
                            <p class="err" id="tmp_address3_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="tmp_location_country"><?php echo lang("ctn_863") ?></label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_location_country" id="tmp_location_country" value="India" placeholder="Enter country name" disabled>
                            <p class="err" id="location_country_err"></p>

                        </div>
                        <div class="form-group">
                            <label for="tmp_location_state"><?php echo lang("ctn_394") ?></label>
                            <select class="form-control" name="tmp_location_state" id="tmp_location_state" onchange="getCity(2)">
                                <option value="">Select State</option>
                                <?php 
                                $tmp_location_state = $user_info['tmp_location_state'];
                                foreach ($states as $all_states) : 
                                    $selected = "";
                                    
                                    if($tmp_location_state == $all_states->id){
                                        $selected = "selected";
                                    }?>
                                    <option value="<?php echo $all_states->id ?>" <?php echo $selected; ?>><?php echo $all_states->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <p class="err" id="tmp_location_state_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="tmp_location_city"><?php echo lang("ctn_393") ?></label>
                            <input type="hidden" name="hidden_tmp_location_city" id="hidden_tmp_location_city" value="<?php echo $user_info["tmp_location_city"]; ?>">
                            <select class="form-control" name="tmp_location_city" id="tmp_location_city">
                                <option value="">Select City</option>
                            </select>
                            <p class="err" id="tmp_location_city_err"></p>
                        </div>


                        <div class="form-group">
                            <label for="tmp_lat">Latitude</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_lat" id="tmp_lat" value="<?php echo $user_info['tmp_lat']; ?>" placeholder="Enter latitude">
                            <p class="err" id="tmp_lat_err"></p>

                        </div>
                        <div class="form-group">
                            <label for="tmp_lng">Longitude</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_lng" id="tmp_lng" value="<?php echo $user_info['tmp_lng']; ?>" placeholder="Enter longitude">
                            <p class="err" id="tmp_lng_err"></p>

                        </div>
                        <div class="form-group">
                            <label for="tmp_pincode">Pincode</label>
                            <input maxlength="100" type="text" class="form-control" name="tmp_pincode" id="tmp_pincode" value="<?php echo $user_info['tmp_pincode']; ?>" placeholder="Enter  pincode">
                            <p class="err" id="tmp_pincode_err"></p>
                        </div>
                        <!-- <div class="text-center">
                            <button type="button" onclick="remove_address()" class="btn btn-primary" id="add_another_address">Remove address</button>
                        </div> -->
                    </div>
                </div>
            </div>

            <h4>IMPORTANT DECLARATION:</h4>
            <p>Lawsmos do not provide legal services to users/ clients on its own. </p>
            <p>Lawsmos do not earn case/ project commissions or a liaison fee from clients or lawyers.</p>
            <p>Lawsmos charges lawyers, law students, professors and institutions based on their use of its various online tools and internet services.</p>
            <p>Lawsmos is an e-education platform for legal professionals, students, professors, law resource suppliers and users; it also works as a listing search engine and networking platform for legal industry.</p>
            <h4>DISCLAIMER:</h4>
            <p>By entering your information and requesting your listing/ profile to be created with Lawsmos, you acknowledge that you have read, understood, and you agree to Lawsmos’s Terms and Conditions, Privacy Policy, Legal Disclaimer, and Terms of Use.</p>
            <p><input type="checkbox" name="agree_checkbox" id="agree_checkbox" value="1" checked> Yes I agree</p>
            <p class="err" id="agree_checkbox_err" style="margin-left:0%;"></p>
            <div class="col-md-12">
                <div style="overflow:auto;" class="pro-nex-prv">
                    <button type="button" id="nextBtn" class="btn sixth">Next</button>
                </div>
            </div>

        </div>
        <div style="overflow:auto;" class="pro-nex-prv">
            <button type="button" id="nextBtn" class="btn fourth">Update</button>
        </div>
    </div>
</div>
</div>

<script>
    var profile_step = 6;
</script>
<?php include(APPPATH . "/views/user_settings/lawyer_profile/edit_profile_script.php"); ?>