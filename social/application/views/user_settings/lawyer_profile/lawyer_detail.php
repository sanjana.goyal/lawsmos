<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right bg-page">
    <div class="db-header clearfix line-area">
        <div class="page-header-title setting-area"> <span class="glyphicon glyphicon-cog"></span> <?php echo lang("ctn_224") ?></div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb edit-home">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">HELP OUR SYSTEMS IDENTIFY: <button style="float:right;"><a href="<?php echo base_url('edit_profile_lawyer_step_1'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table table-bg">
                <thead>
                    <tr>
                        <th>You Work as an Individual Practitioner or with a Law Firm:</th>
                        <th>Date of Birth</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['lawyer_practitioner'] == "1") {
                                echo "Individual";
                            } elseif ($coreprofile['lawyer_practitioner'] == "2") {
                                echo "Firm";
                            } elseif ($coreprofile['lawyer_practitioner'] == "3") {
                                echo "Individual & Firm Both";
                            } else {
                                echo "N/A";
                            }

                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['date_of_birth'])) {
                                echo $coreprofile['date_of_birth'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php if ($coreprofile['lawyer_practitioner'] == "2" || $coreprofile['lawyer_practitioner'] == "3") { ?>
                <table class="table borderless-table table-bg">
                    <thead>
                        <tr>
                            <th>Firm Name:</th>
                            <th>Firm's Email Address:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['firm_name'])) {
                                    echo $coreprofile['firm_name'];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($coreprofile['firm_email'])) {
                                    echo $coreprofile['firm_email'];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php
            }
            ?>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">Professional profile information : <button style="float:right;"><a href="<?php echo base_url('edit_profile_lawyer_step_2'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table table-bg">
                <thead>
                    <tr>
                        <th>Are you a practicing lawyer?:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['practicing_lawyer'] == "1") {
                                echo "Yes";
                            } elseif ($coreprofile['practicing_lawyer'] == "2") {
                                echo "No";
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php if ($coreprofile['practicing_lawyer'] == "2") { ?>
                <table class="table borderless-table table-bg">
                    <tbody>
                        <tr>
                            <th>City of Practice:</th>
                            <th>Fields of interest:</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['unpracticing_city'])) {
                                    $unpracticing_city = explode(",", $coreprofile['unpracticing_city']);
                                    foreach ($unpracticing_city as $city) {
                                        echo getCityName($city) . ",";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($coreprofile['unpracticing_fields_of_interest'])) {
                                    $unpracticing_fields_of_interest = explode(",", $coreprofile['unpracticing_fields_of_interest']);
                                    foreach ($unpracticing_fields_of_interest as $cat_row) {
                                        echo getCategoryName($cat_row) . ",";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Area of Specialization:</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['unpracticing_area_of_specialization'])) {
                                    echo getCourtName($coreprofile['unpracticing_area_of_specialization']);
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Working as:</th>
                            <?php
                            if ($coreprofile['unpracticing_working_as'] == "2") {
                                echo "<th>Organization Name:</th>";
                            }
                            ?>

                        </tr>
                        <tr>
                            <td>
                                <?php
                                if ($coreprofile['unpracticing_working_as'] == "1") {
                                    echo "Individual";
                                } elseif ($coreprofile['unpracticing_working_as'] == "2") {
                                    echo "With an Organization";
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($coreprofile['unpracticing_working_as'] == "2") {
                                    echo $coreprofile['organization_name'];
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php
            }
            ?>
            <?php if ($coreprofile['practicing_lawyer'] == "1") { ?>
                <table class="table borderless-table">
                    <tbody>
                        <tr>
                            <th>City of Practice:</th>
                            <th>Area of practice:</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['practice_city'])) {
                                    $practice_city = explode(",", $coreprofile['practice_city']);
                                    foreach ($practice_city as $city) {
                                        echo getCityName($city) . ",";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($coreprofile['category_id_array'])) {
                                    $category_id = explode(",", $coreprofile['category_id_array']);
                                    foreach ($category_id as $category) {
                                        echo getCategoryName($category) . ",";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Court Name:</th>
                            <th>Language:</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['field_court_number_array'])) {
                                    $courts = explode(",", $coreprofile['field_court_number_array']);
                                    foreach ($courts as $court) {
                                        echo getCourtName($court);
                                        echo ",";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($coreprofile['language'])) {
                                    $language = explode(",", $coreprofile['language']);
                                    foreach ($language as $lang) {
                                        echo getLanguageName($lang) . ",";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>State Bar Council Registration no:</th>
                            <th>Year of registration:</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['councel_registration_no'])) {
                                    echo $coreprofile['councel_registration_no'];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($coreprofile['year_reg'])) {
                                    echo $coreprofile['year_reg'];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Upload State Bar Council Registration ID:</th>
                            <th>Total Experience:</th>
                        </tr>
                        <tr>
                            <td>
                                <div class="photo-upload">
                                    <?php
                                    if (!empty($coreprofile['state_bar_form'])) {
                                        $state_bar_form = "uploads/" . $coreprofile['state_bar_form'];
                                        echo "<img width='150px' src='" . base_url($state_bar_form) . "'>";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </div>
                            </td>
                            <td>
                                <?php
                                if (is_numeric($coreprofile['total_exp'])) {
                                    echo $coreprofile['total_exp'] . " Years";
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Are you a member of any Bar Association:</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($coreprofile['bar_association_member'])) {
                                    if ($coreprofile['bar_association_member'] == 1) {
                                        echo "Yes";
                                    } elseif ($coreprofile['bar_association_member'] == 2) {
                                        echo "No";
                                    } else {
                                        echo "N/A";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php if ($coreprofile['bar_association_member'] == "1") { ?>
                    <table class="table borderless-table">
                        <tbody>
                            <tr>
                                <th>Name of Bar Association:</th>
                                <th>Bar Association Registration no:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['bar_association_name'])) {
                                        $bar_association_name = json_decode($coreprofile['bar_association_name']);
                                        // print_r($bar_association_name);
                                        // die();
                                        // foreach($bar_association_name as $name){
                                        echo $bar_association_name;
                                        // }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['registration_no'])) {
                                        $registration_no = json_decode($coreprofile['registration_no']);
                                        echo $registration_no;
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Bar Association Registration ID:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['bar_association_form'])) {
                                        $bar_association_form = json_decode($coreprofile['bar_association_form']);
                                        foreach ($bar_association_form as $bar_association_id) {
                                            $bar_association_id = "uploads/" . $bar_association_id;
                                            echo "<img width='150px' src='" . base_url($bar_association_id) . "'>";
                                            // echo $bar_association_id;
                                        }

                                        // echo $coreprofile['bar_association_form'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            <?php } ?>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg ">Education Information <button style="float:right;"><a href="<?php echo base_url('edit_profile_lawyer_step_3'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr class="table-sub-title table-area">
                        <td colspan="2">School & High School</td>
                    </tr>
                    <tr>
                        <th>School:10</th>
                        <th>School:12</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['school_10'])) {
                                echo $coreprofile['school_10'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['school_12'])) {
                                echo $coreprofile['school_12'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <th>University:</th>
                        <th>University:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['board_10'])) {
                                echo $coreprofile['board_10'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['board_12'])) {
                                echo $coreprofile['board_12'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Year of completion:</th>
                        <th>Year of completion:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['year_pass_10'])) {
                                echo $coreprofile['year_pass_10'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['year_pass_12'])) {
                                echo $coreprofile['year_pass_12'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php
                                if ($coreprofile['x_marks_type'] == 1) {
                                    echo "Percentage : ";
                                } elseif ($coreprofile['x_marks_type'] == 2) {
                                    echo "Division : ";
                                } elseif ($coreprofile['x_marks_type'] == 2) {
                                    echo "CGPA : ";
                                } else {
                                    echo "Marks :";
                                }
                            ?>
                        </th>
                        <th>
                            <?php
                                if ($coreprofile['xii_marks_type'] == 1) {
                                    echo "Percentage : ";
                                } elseif ($coreprofile['xii_marks_type'] == 2) {
                                    echo "Division : ";
                                } elseif ($coreprofile['xii_marks_type'] == 2) {
                                    echo "CGPA : ";
                                } else {
                                    echo "Marks :";
                                }
                            ?>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['x_marks_type'] == 1) {
                                echo  $coreprofile['marks_x_percentage'];
                            } elseif ($coreprofile['x_marks_type'] == 2) {
                                if ($coreprofile['x_division'] == 1) {
                                    echo "1st";
                                } elseif ($coreprofile['x_division'] == 2) {
                                    echo "2nd";
                                } elseif ($coreprofile['x_division'] == 3) {
                                    echo "3rd";
                                } elseif ($coreprofile['x_division'] == 4) {
                                    echo "Gold Medalist";
                                } else {
                                    echo "N/A";
                                }
                            } elseif ($coreprofile['x_marks_type'] == 2) {
                                echo $coreprofile['marks_x_cgpa'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($coreprofile['xii_marks_type'] == 1) {
                                echo $coreprofile['marks_xii_percentage'];
                            } elseif ($coreprofile['xii_marks_type'] == 2) {
                                if ($coreprofile['xii_division'] == 1) {
                                    echo "1st";
                                } elseif ($coreprofile['xii_division'] == 2) {
                                    echo "2nd";
                                } elseif ($coreprofile['xii_division'] == 3) {
                                    echo "3rd";
                                } elseif ($coreprofile['xii_division'] == 4) {
                                    echo "Gold Medalist";
                                } else {
                                    echo "N/A";
                                }
                            } elseif ($coreprofile['xii_marks_type'] == 2) {
                                echo $coreprofile['marks_xii_cgpa'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>

                    <?php
                    $professional_qualification_count = 0;
                    if (!empty($coreprofile['degree_array'])) {
                        $degree_array = json_decode($coreprofile['degree_array']);
                        $yearstart_array = json_decode($coreprofile['yearstart_array']);
                        $yearend_array = json_decode($coreprofile['yearend_array']);
                        $college_array = json_decode($coreprofile['college_array']);
                        $university_array = json_decode($coreprofile['university_array']);
                        $division_array = json_decode($coreprofile['division_array']);
                        $upload_marksheet_array = json_decode($coreprofile['upload_marksheet_array']);
                        $professional_qualification_count = count($degree_array);
                        // echo "hello1";
                        // print_r($upload_marksheet_array);
                        // die("hello");
                    }
                    if ($professional_qualification_count > 0) {
                        for ($i = 0; $i < $professional_qualification_count; $i++) {
                    ?>

                            <tr class="table-sub-title">
                                <td colspan="2">Professional Qualification <?php echo $i + 1; ?></td>
                            </tr>
                            <tr>
                                <th>Degree/ Course Name:</th>
                                <th>College Name:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($degree_array[$i])) {
                                        echo $degree_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($college_array[$i])) {
                                        echo $college_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>University Name:</th>
                                <th>Start Year:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($university_array[$i])) {
                                        echo $university_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($yearstart_array[$i])) {
                                        echo $yearstart_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>End Year:</th>
                                <th>Division:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($yearend_array[$i])) {
                                        echo $yearend_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($division_array[$i])) {
                                        // echo "Division : ";
                                        if ($division_array[$i] == 1) {
                                            echo "1st";
                                        } elseif ($division_array[$i] == 2) {
                                            echo "2nd";
                                        } elseif ($division_array[$i] == 3) {
                                            echo "3rd";
                                        } elseif ($division_array[$i] == 4) {
                                            echo "Gold Medalist";
                                        } else {
                                            echo "N/A";
                                        }
                                        // echo $division_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php if (!empty($upload_marksheet_array[$i])) { ?>
                                <tr>
                                    <th>
                                        Marksheet:
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('uploads/' . $upload_marksheet_array[$i]) ?>" width="200px">
                                    </td>
                                </tr>
                    <?php
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg ">Professional Experience : <button style="float:right;"><a href="<?php echo base_url('edit_profile_lawyer_step_4'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <?php
            $all_results = "";
            if (!empty($coreprofile['experience_organization_name'])) {
                $all_results = json_decode($coreprofile['experience_organization_name']);
            }
            $row_count = 0;
            if (is_array($all_results)) {
                $row_count = count($all_results);
            }
            ?>
            <?php
            if ($row_count > 0) {
                $experience_organization_name = json_decode($coreprofile['experience_organization_name']);
                $experience_city = json_decode($coreprofile['experience_city']);
                $experience_court = json_decode($coreprofile['experience_court']);
                $experience_designation = json_decode($coreprofile['experience_designation']);
                $experience_start_date = json_decode($coreprofile['experience_start_date']);
                $experience_end_date = json_decode($coreprofile['experience_end_date']);
                for ($i = 0; $i < $row_count; $i++) {
            ?>

                    <table class="table borderless-table bg-area">
                        <tr>
                            <th>Name of organization</th>
                            <th>City</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($experience_organization_name[$i])) {
                                    echo $experience_organization_name[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($experience_city[$i])) {
                                    echo getCityNAme($experience_city[$i]);
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Court</th>
                            <th>Designation</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($experience_court[$i])) {
                                    echo getCourtName($experience_court[$i]);
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($experience_designation[$i])) {
                                    if ($experience_designation[$i] == "1") {
                                        echo "Designated Senior";
                                    } elseif ($experience_designation[$i] == "2") {
                                        echo "Retired Justice";
                                    } else {
                                        echo "N/A";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($experience_start_date[$i])) {
                                    echo $experience_start_date[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($experience_end_date[$i])) {
                                    echo $experience_end_date[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
            <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg ">Consultation Information : <button style="float:right;"><a href="<?php echo base_url('edit_profile_lawyer_step_5'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr class="table-sub-title table-area">
                        <td colspan="3">Consultation Timings</td>
                    </tr>
                    <tr>
                        <th>Days</th>
                        <th>From - To (24 hours)</th>
                    </tr>
                    <?php
                    if (!empty($consultation_timing)) {
                        foreach ($consultation_timing as $timing) {
                    ?>
                            <tr>
                                <td><?php echo $timing['day_name']; ?></td>
                                <td>
                                    <?php
                                        $st_time = $timing['start_hour'] . ":" . $timing['start_min'];
                                        $st_time = date("H:i",strtotime($st_time));
                                        echo $st_time . ' - ';
                                        $end_time = $timing['end_hour'] . ":" . $timing['end_min'];
                                        $end_time = date("H:i",strtotime($end_time));
                                        echo $end_time;
                                    ?>
                                </td>
                                <!-- <td><?php echo $timing['end_hour'] . ":" . $timing['end_min'] ?></td> -->
                            </tr>
                        <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="3">No result found</td>
                        </tr>
                    <?php
                    }

                    ?>
                    <!-- <tr>
                        <td>Monday</td>
                        <td>2Hour 15min</td>
                        <td>2Hour 15min</td>
                    </tr>
                    <tr>
                        <td>Tuesday</td>
                        <td>2Hour 15min</td>
                        <td>2Hour 15min</td>
                    </tr>
                    <tr>
                        <td>Friday</td>
                        <td>2Hour 15min</td>
                        <td>2Hour 15min</td>
                    </tr> -->
                    <tr>
                        <th>In Office Consultation</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['office_consultation_type'] == 1) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    if ($coreprofile['office_consultation_type'] == 1) {
                    ?>
                        <?php
                        if (!empty($coreprofile['office_charges_checkbox_30_min'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Office)</th>
                                <th>Consultation Charges(Office)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['office_charges_30min'])) {
                                        echo $coreprofile['office_charges_30min'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        <?php
                        if (!empty($coreprofile['office_charges_checkbox_1_hour'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Office)</th>
                                <th>Consultation Charges(Office)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['office_charges_1hour'])) {
                                        echo $coreprofile['office_charges_1hour'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                    <tr>
                        <th>Client Site Consultation</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['site_consultation_type'] == 1) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    if ($coreprofile['office_consultation_type'] == 1) {
                    ?>
                        <?php
                        if (!empty($coreprofile['site_charges_checkbox_30_min'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Client Site)</th>
                                <th>Consultation Charges(Client Site)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['site_charges_30min'])) {
                                        echo $coreprofile['site_charges_30min'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        <?php
                        if (!empty($coreprofile['site_charges_checkbox_1hour'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Client Site)</th>
                                <th>Consultation Charges(Client Site)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['site_charges_1hour'])) {
                                        echo $coreprofile['site_charges_1hour'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                    <tr>
                        <th>Telephonic Consultation (Voice)</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['telvoice_consultation_type'] == 1) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    if ($coreprofile['telvoice_consultation_type'] == 1) {
                    ?>
                        <?php
                        if (!empty($coreprofile['telvoice_charges_checkbox_30_min'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Voice)</th>
                                <th>Consultation Charges(Telephonic Voice)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['telvoice_charges_30min'])) {
                                        echo $coreprofile['telvoice_charges_30min'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        <?php
                        if (!empty($coreprofile['telvoice_charges_checkbox_1hour'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Voice)</th>
                                <th>Consultation Charges(Telephonic Voice)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['telvoice_charges_1hour'])) {
                                        echo $coreprofile['telvoice_charges_1hour'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                    <tr>
                        <th>Telephonic Consultation(Video)</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['telvideo_consultation_type'] == 1) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                    if ($coreprofile['telvideo_consultation_type'] == 1) {
                    ?>
                        <?php
                        if (!empty($coreprofile['telvideo_charges_checkbox_30_min'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Video)</th>
                                <th>Consultation Charges(Telephonic Video)</th>
                            </tr>
                            <tr>
                                <td>
                                    30 mins
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['telvideo_charges_1hour'])) {
                                        echo $coreprofile['telvideo_charges_1hour'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        <?php
                        if (!empty($coreprofile['telvideo_charges_checkbox_1hour'])) {
                        ?>
                            <tr>
                                <th>Consultation time(Telephonic Video)</th>
                                <th>Consultation Charges(Telephonic Video)</th>
                            </tr>
                            <tr>
                                <td>
                                    1 hour
                                </td>
                                <td>
                                    <?php
                                    if (!empty($coreprofile['office_charges_1hour'])) {
                                        echo $coreprofile['office_charges_1hour'] . " Rs.";
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                    <tr>
                        <th>Email Consultation</th>
                        <?php
                        if ($coreprofile['email_consultation_type'] == 1) {
                        ?>
                            <th>Email Consultation Charges</th>
                        <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['email_consultation_type'] == 1) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?>
                        </td>
                        <?php
                        if ($coreprofile['email_consultation_type'] == 1) {
                        ?>
                            <td>
                                <?php
                                if (!empty($coreprofile['email_consultation_charges'])) {
                                    echo $coreprofile['email_consultation_charges'] . " Rs.";
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        <?php
                        }
                        ?>
                    </tr>
                    <tr>
                        <th>Appearance charges</th>
                        <th>Hourly Drafting charges</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['apperance_charges'])) {
                                echo $coreprofile['apperance_charges'] . " Rs.";
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['haur_draft_charges'])) {
                                echo $coreprofile['haur_draft_charges'] . " Rs.";
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">Address Details : <button style="float:right;"><a href="<?php echo base_url('edit_profile_lawyer_step_6'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body text-field">
        <h4>Firm Address</h4>
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr>
                        <th>Address</th>
                        <th>City</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['firm_address_1'])) {
                                echo $user_info['firm_address_1'];
                                if (!empty($user_info['firm_address_2'])) {
                                    echo ", " . $user_info['firm_address_2'];
                                }
                                if (!empty($user_info['firm_address_3'])) {
                                    echo ", " . $user_info['firm_address_3'];
                                }
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($user_info['firm_city'])) {
                                echo getCityNAme($user_info['firm_city']);
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>State/Region/Province</th>
                        <th>Country</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['firm_state'])) {
                                echo getStateName($user_info['firm_state']);
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            India
                        </td>
                    </tr>
                    <tr>
                        <th>Latitude</th>
                        <th>Longitude</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['firm_lat'])) {
                                echo $user_info['firm_lat'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($user_info['firm_lng'])) {
                                echo $user_info['firm_lng'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Pincode</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['firm_pincode'])) {
                                echo $user_info['firm_pincode'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h4>Address 1</h4>
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr>
                        <th>Address</th>
                        <th>City</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['address_1'])) {
                                echo $user_info['address_1'];
                                if (!empty($user_info['address_2'])) {
                                    echo ", " . $user_info['address_2'];
                                }
                                if (!empty($user_info['address3'])) {
                                    echo ", " . $user_info['address3'];
                                }
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($user_info['city'])) {
                                echo getCityNAme($user_info['city']);
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>State/Region/Province</th>
                        <th>Country</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['state'])) {
                                echo getStateName($user_info['state']);
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            India
                        </td>
                    </tr>
                    <tr>
                        <th>Latitude</th>
                        <th>Longitude</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['lat'])) {
                                echo $user_info['lat'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($user_info['longitude'])) {
                                echo $user_info['longitude'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Pincode</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['zipcode'])) {
                                echo $user_info['zipcode'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php  if (!empty($user_info['tmp_address1'])) { ?>
            <h4>Address 2</h4>
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr>
                        <th>Address</th>
                        <th>City</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['tmp_address1'])) {
                                echo $user_info['tmp_address1'];
                                if (!empty($user_info['tmp_address2'])) {
                                    echo ", " . $user_info['tmp_address2'];
                                }
                                if (!empty($user_info['tmp_address3'])) {
                                    echo ", " . $user_info['tmp_address3'];
                                }
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($user_info['tmp_location_city'])) {
                                echo getCityNAme($user_info['tmp_location_city']);
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>State/Region/Province</th>
                        <th>Country</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['tmp_location_state'])) {
                                echo getStateName($user_info['tmp_location_state']);
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            India
                        </td>
                    </tr>
                    <tr>
                        <th>Latitude</th>
                        <th>Longitude</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['tmp_lat'])) {
                                echo $user_info['tmp_lat'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($user_info['tmp_lng'])) {
                                echo $user_info['tmp_lng'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Pincode</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($user_info['tmp_pincode'])) {
                                echo $user_info['tmp_pincode'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php } ?>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">Admin Logs</h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Status</th>
                        <th>Message</th>
                        <th>Created at</th>
                    </tr>
                    <?php
                    if (empty($verification_logs)) {
                    ?>
                        <tr>
                            <td colspan="4" style="text-align:center;">No results found</td>
                        </tr>
                        <?php
                    } else {
                        $count = 0;
                        foreach ($verification_logs as $verification_log) {
                            $count++;
                        ?>
                            <tr>
                                <td><?php echo $count; ?></td>
                                <td><?php echo ($verification_log['status'] == 1) ? "Approved" : ($verification_log['status'] == 2 ? "Rejected" : "N/A"); ?></td>
                                <td><?php echo !empty($verification_log['message']) ? $verification_log['message'] : "N/A"; ?></td>
                                <td><?php echo $verification_log['created_at']; ?></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>