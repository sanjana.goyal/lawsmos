<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css" />

 <?php $this->load->view('sidebar/sidebar.php'); ?>


<div class="white-area-content separator page-right">

<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-user"></span> <?php echo $user->first_name ?>'s Appointments</div>
    <div class="db-header-extra form-inline"> 

        <div class="form-group has-feedback no-margin">
<div class="input-group">
<input type="text" class="form-control input-sm" placeholder="<?php echo lang("ctn_336") ?>" id="form-search-input" />
<div class="input-group-btn">
    <input type="hidden" id="search_type" value="0">
        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
        <ul class="dropdown-menu small-text" style="min-width: 90px !important; left: -90px;">
          <li><a href="#" onclick="change_search(0)"><span class="glyphicon glyphicon-ok" id="search-like"></span> <?php echo lang("ctn_337") ?></a></li>
          <li><a href="#" onclick="change_search(1)"><span class="glyphicon glyphicon-ok nodisplay" id="search-exact"></span> <?php echo lang("ctn_338") ?></a></li>
          <li><a href="#" onclick="change_search(2)"><span class="glyphicon glyphicon-ok nodisplay" id="username-exact"></span> <?php echo lang("ctn_25") ?></a></li>
          <li><a href="#" onclick="change_search(3)"><span class="glyphicon glyphicon-ok nodisplay" id="firstname-exact"></span> <?php echo lang("ctn_29") ?></a></li>
          <li><a href="#" onclick="change_search(4)"><span class="glyphicon glyphicon-ok nodisplay" id="lastname-exact"></span> <?php echo lang("ctn_30") ?></a></li>
        </ul>
      </div><!-- /btn-group -->
</div>
</div>

</div>
</div>

<div class="table-responsive">
<table id="friends-table" class="table table-striped table-hover table-bordered">
<thead>
<tr class="table-header">
	<td>Lawyer(s) Name</td>
	<td>Lawyer(s) Contact</td>
	<td>Lawyer(s) Email</td>
	<td>Appointment Date</td>
	<td>Appointment Time</td>
	<td>Status</td>
	<td>Appointment created date</td>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>


</div>


















 <script type="text/javascript">
$(document).ready(function() {

   var st = $('#search_type').val();
    var table = $('#friends-table').DataTable({
        "dom" : "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
		"processing": false,
        "pagingType" : "full_numbers",
        "pageLength" : 15,
        "serverSide": true,
        "orderMulti": false,
        "order": [
        ],
        "columns": [
        { "orderable" : false },
        null,
		null,
		null,
		null,
		null,
        
        { "orderable" : false }
    ],
        "ajax": {
            url : "<?php echo site_url("user_settings/listappointments/" . $user->ID); ?>",
            type : 'GET',
            data : function ( d ) {
                d.search_type = $('#search_type').val();
            }
        },
        
       
        
        
        "drawCallback": function(settings, json) {
        $('[data-toggle="tooltip"]').tooltip();
      }
    });
    $('#form-search-input').on('keyup change', function () {
    table.search(this.value).draw();
});

} );
function change_search(search) 
    {
      var options = [
      "search-like", 
      "search-exact",
      "username-exact",
      "firstname-exact",
      "lastname-exact",

      ];
      set_search_icon(options[search], options);
        $('#search_type').val(search);
        $( "#form-search-input" ).trigger( "change" );
    }

function set_search_icon(icon, options) 
    {
      for(var i = 0; i<options.length;i++) {
        if(options[i] == icon) {
          $('#' + icon).fadeIn(10);
        } else {
          $('#' + options[i]).fadeOut(10);
        }
      }
    }
</script>






<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/gcal.js"></script>




<div class="container-fluid">
<div class="row">

	<div class="col-md-12 month-appoint">
		<h1 style="font-size:22px;">This Month Appointment(s) </h1>
		<div id="calendar111">
		</div>
		
	</div>
</div>
</div>

<style>
.fc-day-grid-event .fc-time {
    font-weight: 700;
    display: none;
}
</style>

<script type="text/javascript">
$(document).ready(function() {
    $('#calendar111').fullCalendar({
		
		
		color: '#e0a211',   
        textColor: '#000000',
          dayClick: function (date, allDay, jsEvent, view) {  
        $("#dialog").dialog('open');
    },
		eventSources: [
         {
             events: function(start, end, timezone, callback) {
                 $.ajax({
                 url: '<?php echo base_url() ?>user_settings/get_appointment_events_seekers',
                 dataType: 'json',
                 data: {
                 // our hypothetical feed requires UNIX timestamps
                 start: start.unix(),
                 
                 },
                 success: function(msg) {
                     var events = msg.events;
                     callback(events);
                 }
                 });
             }
         },
     ]
		

    });
    
    
    
});


</script>











