<style type="text/css">
#responsive-menu-links{
        display:none;
    }
.form_error{color:red;}

</style>
<script src="<?php echo base_url();?>scripts/custom/get_usernames.js"></script>
<div class="row profile-setting-row">

<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
<?php include(APPPATH . "views/user_settings/sidebar.php"); ?>
</div>

 <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12">


<div class="white-area-content">
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> <?php echo lang("ctn_224") ?></div>
    <div class="db-header-extra">
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <li class="active"><?php echo lang("ctn_805") ?></li>
</ol>



<hr>

<div class="panel panel-default">
<div class="panel-body">
	
 <div class="form_error">
          <?php echo validation_errors(); ?>
  </div>	
<p class="panel-subheading"><?php echo lang("ctn_227") ?></p>
<?php echo form_open_multipart(site_url("user_settings/addjudgment"), array("class" => "form-horizontal")) ?>





	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_806") ?></label>
	    <div class="pro-input-wrap">
	      <input type="text" class="form-control" name="case_heading" >
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_814") ?></label>
	    <div class="pro-input-wrap">
	      <input type="text" class="form-control" name="case_number" >
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_810") ?></label>
	    <div class="pro-input-wrap">
	      <input type="text" class="form-control" name="courtname" >
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_809") ?></label>
	    <div class="pro-input-wrap">
	      <input type="text" class="form-control" name="windate" id="txtFromDate">
	    </div>
	</div>
	
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_815") ?></label>
	    <div class="pro-input-wrap">
	      <input type="text" class="form-control" name="final_order_date" id="txtToDate" >
	    </div>
	</div>
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_816") ?></label>
	    <div class="pro-input-wrap">
	      <input type="text" class="form-control" name="nature_of_proceing" >
	    </div>
	</div>
	
	
	
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_807") ?></label>
	    <div class="pro-input-wrap">
	      <textarea class="form-control" name="case_description" rows="8"></textarea>
	    </div>
	</div>
	
	
	<div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_808") ?></label>
	    <div class="pro-input-wrap choose-btn-p">
	   
	    <?php if($this->settings->info->avatar_upload) : ?>
	     	<input type="file" name="userfile"  required/> 
	     <?php endif; ?>
	    </div>
	</div>
	
    <div class="form-group">
	    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_817") ?></label>
	    <div class="pro-input-wrap">
	      <input type="radio"  value="win" name="resultcase" >Win 
	     <input type="radio"  value="Lost" name="resultcase" >  Lost
	    </div>
	</div>
	
	
	
	<div class="profile-set-wrap">
	 <input type="submit" name="s" value="<?php echo lang("ctn_236") ?>" class="btn  profile-set-btn" />
    </div>
<?php echo form_close() ?>
</div>
</div>
</div>


</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#relationship').on("change", function() {
		var status = $('#relationship').val();
		if(status == 2 || status == 3) {
			$('#relationship_user').fadeIn(10);
		} else {
			$('#relationship_user').fadeOut(10);
		}
	});
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $("#txtFromDate").datepicker({
    	dateFormat: 'yy-mm-dd',
        minDate: 0,
        maxDate: "+60D",
        numberOfMonths: 1,
        onSelect: function(selected) {
          $("#txtToDate").datepicker("option","minDate", selected)
        }
    });
    $("#txtToDate").datepicker({ 
    	dateFormat: 'yy-mm-dd',
        minDate: 0,
        maxDate:"+60D",
        numberOfMonths: 1,
        onSelect: function(selected) {
           $("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });  
});


</script>
