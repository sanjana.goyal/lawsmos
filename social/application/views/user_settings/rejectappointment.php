
 <style>
 
 .row.styleapppoint {
    background-color: gold;
    padding: 30px;
	margin-top: 71px;	
    box-shadow: 0px 2px 16px 3px black;
}


.section {
	position: relative;
	height: 100vh;
}

.section .section-center {
	position: absolute;
	top: 50%;
	left: 0;
	right: 0;
	-webkit-transform: translateY(-50%);
	transform: translateY(-50%);
}

#booking {
	font-family: 'Montserrat', sans-serif;
	
	background-size: cover;
	background-position: center;
}



.booking-form {
	max-width: 642px;
	width: 100%;
	margin: 15%;
	box-shadow: 0px 1px 27px 2px black;
}

.booking-form .form-header {
	text-align: center;
	margin-bottom: 25px;
}

.booking-form .form-header h1 {
	font-size: 58px;
	text-transform: uppercase;
	font-weight: 700;
	color: #ffc001;
	margin: 0px;
}

.booking-form>form {
	background-color: #101113;
	padding: 30px 20px;
	border-radius: 3px;
}

.booking-form .form-group {
	position: relative;
	margin-bottom: 15px;
}

.booking-form .form-control {
	background-color: #f5f5f5;
	border: none;
	height: 45px;
	border-radius: 3px;
	-webkit-box-shadow: none;
	box-shadow: none;
	font-weight: 400;
	color: #101113;
}

.booking-form .form-control::-webkit-input-placeholder {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form .form-control:-ms-input-placeholder {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form .form-control::placeholder {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form input[type="date"].form-control:invalid {
	color: rgba(16, 17, 19, 0.3);
}

.booking-form select.form-control {
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
}

.booking-form select.form-control+.select-arrow {
	position: absolute;
	right: 0px;
	bottom: 6px;
	width: 32px;
	line-height: 32px;
	height: 32px;
	text-align: center;
	pointer-events: none;
	color: #101113;
	font-size: 14px;
}

.booking-form select.form-control+.select-arrow:after {
	content: '\279C';
	display: block;
	-webkit-transform: rotate(90deg);
	transform: rotate(90deg);
}

.booking-form .form-label {
	color: #fff;
	font-size: 12px;
	font-weight: 400;
	margin-bottom: 5px;
	display: block;
	text-transform: uppercase;
}

.booking-form .submit-btn {
	color: #101113;
	background-color: #ffc001;
	font-weight: 700;
	height: 50px;
	border: none;
	width: 100%;
	display: block;
	border-radius: 3px;
	text-transform: uppercase;
}
 </style>






<div id="booking" class="section">
		<div class="section-center">
			<div class="container">
				<div class="row">
					<div class="booking-form">
						<div class="form-header">
							
						</div>
					
					
					
					<form action="<?php echo site_url('user_settings/rejectss'); ?>" method="post">
						
						 <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />
		
		
		
						<input type="hidden" name="appointmentid" value="<?php echo $id; ?>">
		
		
						
						
						
							<div class="row">
								
							<div class="col-md-12">	
								<div class="form-group">
								<span class="form-label">Select Reason of rejection</span>
								<select class="form-control" name="reasontype" required>
									<option>Select Reason</option>
									<option value="Unavailable Time Slots">Unavailable Time Slots</option>
									<option value="others">Reason not listed</option>
								</select>
								</div>
								
								<div class="form-group">
										<span class="form-label">Reject Note</span>
										<textarea class="form-control" name="rejectnote"></textarea>
									
								</div>
								
							</div>
							
							
							
							
							
							
							
							</div>
							<div class="form-btn">
								<button type="submit" class="submit-btn">Reject Appointment</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>














<script>


	var dateToday = new Date();
var dates = $("#from").datepicker({
    //defaultDate: "-1w",
    changeMonth: true,
    numberOfMonths: 1,
    minDate: dateToday,
    maxDate: '15',
    onSelect: function(selectedDate) {
        var option = this.id == "from" ? "minDate" : "maxDate",
            instance = $(this).data("datepicker"),
            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
    }
});
  </script>







