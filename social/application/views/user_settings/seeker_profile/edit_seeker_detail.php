<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/styles/custom.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
<div class="profile-formwrap">
    <div class="container">
        <?php $this->load->view('sidebar/sidebar.php'); ?>
        <div class="con-inner-pro">
            <div id="notification-loaders" style="display:none;">
                <div id="loading_spinner_notification" style="text-align:center;">
                    <span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
                </div>
            </div>
            <form id="regForm" class="profile-form-start" enctype="multipart/form-data">
                <input type="hidden" name="typeofsubmit" value="updateprofile">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
                <div class="tab">
                    <h4>HELP US KNOW YOU BETTER TO PROVIDE ACCURATE LAWSMOS SERVICES:</h4>
                    <div class="form-group">
                        <label class=" control-label">I am an:*</label>
                        <div class=" check-inner-pp">
                            <div class="check-inner-p">
                                <!--  if (!empty($core_profile['seeker_type'])) {
                                    if ($core_profile['seeker_type'] == 1) {
                                        echo "Individual";
                                    } else if ($core_profile['seeker_type'] == 2) {
                                        echo "Firm/ Company/ Organization";
                                    } else {
                                        echo "N/A";
                                    } -->
                                <input type="radio" name="seeker_type" value="1" id="seeker_type_individual" onclick="change_seeker_type(1)" <?php  if ($core_profile['seeker_type'] == 1) {
                                        echo "checked";
                                    } ?>>
                                <label for="seeker_type_individual">Individual</label>
                            </div>
                            <div class="check-inner-p">
                                <input type="radio" name="seeker_type" value="2" id="seeker_type_organization" onclick="change_seeker_type(2)" <?php if ($core_profile['seeker_type'] == 2) {
                                        echo "checked";
                                    } ?>>
                                <label for="seeker_type_organization">Firm/ Company/ Organization</label>
                            </div>
                        </div>
                        <p class="err" id="practioner_err"></p>
                    </div>
                    <div class="form-group">
                        <label for="date_of_birth">Date of Birth:*</label>
                        <input class="form-control" type="date" id="date_of_birth" value="<?php echo $core_profile['date_of_birth']; ?>" placeholder="" name="date_of_birth">
                        <p class="err" id="date_of_birth_err"></p>
                    </div>
                    <div class="seeker_type_individual_div" style="display:none">
                        <div class="form-group">
                            <label for="address">Street</label>
                            <input type="text" class="form-control" name="address" id="address" value="<?php  echo $core_profile['address']; ?>" placeholder="Enter street address">
                            <p class="err" id="address_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="state"><?php echo lang("ctn_394") ?></label>
                            <select class="form-control" name="state" id="state" onchange="getCity(0)">
                                <option value="">Select State</option>
                                <?php foreach ($states as $all_states) : ?>
                                    <option value="<?php echo $all_states->id ?>" <?php if ($core_profile['state']==$all_states->id ) {
                                        echo "selected";
                                    } ?>><?php echo $all_states->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <p class="err" id="state_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="city"><?php echo lang("ctn_393") ?></label>
                            <select class="form-control" name="city" id="city">
                                <option value="0">Select City</option>
                                 <?php foreach ($cities as $city) : ?>
                                    <option value="<?php echo $city->id ?>" <?php if ($core_profile['city']==$city->id ) {
                                        echo "selected";
                                    } ?>><?php echo $city->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <p class="err" id="city_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="pincode">Pincode</label>
                            <input type="text" class="form-control" name="pincode" id="pincode" value="<?php echo $core_profile['pincode']; ?>" placeholder="Enter pincode">
                            <p class="err" id="pincode_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="education_level">Education Level</label>
                            <select name="education_level" id="education_level" class="form-control">
                                <option value="0" >Select Education Level</option>
                                <option value="1" <?php if ($core_profile['education_level'] == "1") {
                                            echo "selected";
                                        } ?>>Dropout</option>
                                <option value="2" <?php if ($core_profile['education_level'] == "2") {
                                            echo "selected";
                                        } ?>>High School</option>
                                <option value="3" <?php if ($core_profile['education_level'] == "3") {
                                            echo "selected";
                                        } ?>>Graduate</option>
                                <option value="4" <?php if ($core_profile['education_level'] == "4") {
                                            echo "selected";
                                        } ?>>Post Graduate</option>
                                <option value="5" <?php if ($core_profile['education_level'] == "5") {
                                            echo "selected";
                                        } ?>>Doctorate</option>
                            </select>
                            <p class="err" id="education_level_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="degree">Degree</label>
                            <input type="text" class="form-control" name="degree" id="degree" value="<?php echo $core_profile['degree']; ?>" placeholder="Enter degree">
                            <p class="err" id="degree_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="occupation">Occupation</label>
                            <input type="text" class="form-control" name="occupation" id="occupation" value="<?php echo $core_profile['occupation']; ?>" placeholder="Enter Occupation">
                            <p class="err" id="occupation_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="text" class="form-control" name="age" id="age" value="<?php echo $core_profile['age']; ?>" placeholder="Enter age">
                            <p class="err" id="age_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="lang" class=" control-label">Language:*</label>
                            <select class="form-control selectpicker" multiple title="Select Language" data-max-options="3" name="language[]" id="language" required="">
                                <option value="0">Select Language</option>
                                <?php
                                $language2 = json_decode($core_profile['language']);
                                foreach ($languages as $lang) {
                                    foreach ($language2 as $lang2) {
                                 ?>

                                    <option value="<?php echo $lang->ID; ?>" <?php if ($lang->ID==$lang2) {
                                        echo "selected";
                                    } ?>><?php echo $lang->language; ?></option>
                                <?php
                                }
                                }
                                ?>
                            </select>
                            <p class="err" id="language_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="marital_status">Marital Status:*</label>
                            <select name="marital_status" id="marital_status" class="form-control">
                                <option value="0">Select Marital Status</option>
                                <option value="1" <?php   if ($core_profile['marital_status'] == "1") {
                                            echo "selected";
                                        } ?>>Married</option>
                                <option value="2"  <?php   if ($core_profile['marital_status'] == "2") {
                                            echo "selected";
                                        } ?>>Unmarried</option>
                            </select>
                            <p class="err" id="marital_status_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="annual_income">Annual Income(In lakhs):*</label>
                            <select name="annual_income" id="annual_income" class="form-control">
                                <option value="0">Select Annual income</option>
                                <option value="1"  <?php   if ($core_profile['annual_income'] == "1") {
                                            echo "selected";
                                        } ?>>Under 2</option>
                                <option value="2" <?php   if ($core_profile['annual_income'] == "2") {
                                            echo "selected";
                                        } ?>>2-5</option>
                                <option value="3" <?php   if ($core_profile['annual_income'] == "3") {
                                            echo "selected";
                                        } ?>>5-10</option>
                                <option value="4" <?php   if ($core_profile['annual_income'] == "4") {
                                            echo "selected";
                                        } ?>>10-15</option>
                                <option value="5" <?php   if ($core_profile['annual_income'] == "5") {
                                            echo "selected";
                                        } ?>>15-20</option>
                                <option value="6" <?php   if ($core_profile['annual_income'] == "6") {
                                            echo "selected";
                                        } ?>>20-30</option>
                                <option value="7" <?php   if ($core_profile['annual_income'] == "7") {
                                            echo "selected";
                                        } ?>>30-50</option>
                                <option value="8" <?php   if ($core_profile['annual_income'] == "8") {
                                            echo "selected";
                                        } ?>>50-70</option>
                                <option value="9" <?php   if ($core_profile['annual_income'] == "9") {
                                            echo "selected";
                                        } ?>>70-1cr</option>
                                <option value="10" <?php   if ($core_profile['annual_income'] == "10") {
                                            echo "selected";
                                        } ?>>1cr and above</option>
                            </select>
                            <p class="err" id="annual_income_err"></p>
                        </div>
                        <div class="form-group pro-first">
                            <label for="categories">SELECT LAW CATEGORIES OF INTERESTS:*</label>
                            <select class="form-control selectpicker" multiple title="Select Categories" data-max-options="6" name="categories[]" id="categories" required="">
                                <option value="0">Select Categories</option>
                                <?php
                                 $categories2 = json_decode($core_profile['categories']);
                                foreach ($catagories as $catagory) {
                                    
                                ?>
                                    <option value="<?php echo $catagory['cid']; ?>" <?php foreach ($categories2 as $cat2) { if ($catagory['cid']==$cat2) {
                                        echo "selected";
                                    } } ?>><?php echo $catagory['c_name'] ?></option>
                                <?php
                           
                                }
                                ?>
                            </select>
                            <p class="err" id="categories_err"></p>
                        </div>

                    </div>
                    <div class="seeker_type_organization_div" style="display:none">
                        <div class="form-group">
                            <label for="organization_name" class="control-label">Name of the organization:*</label>
                            <input class="form-control" name="organization_name" value="<?php echo($core_profile['organization_name']); ?>" id="organization_name" placeholder="Enter organization name" type="text" required="">
                            <p class="err" id="organization_name_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="office_location" class="control-label">Head office location:*</label>
                            <input class="form-control" name="office_location" value="<?php echo $core_profile['office_location']; ?>" id="office_location" placeholder="Enter head office location" type="text" required="">
                            <p class="err" id="office_location_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="office_address">Street</label>
                            <input type="text" class="form-control" name="office_address" id="office_address" value="<?php echo $core_profile['office_address']; ?>" placeholder="Enter street address">
                            <p class="err" id="office_address_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="office_state"><?php echo lang("ctn_394") ?></label>
                            <select class="form-control" name="office_state" id="office_state" onchange="getCity(1)">
                                <option value="">Select State</option>
                                <?php foreach ($states as $all_states) : ?>
                                    <option value="<?php echo $all_states->id ?>" <?php if ($core_profile['office_state']==$all_states->id ) {
                                        echo "selected";
                                    } ?>><?php echo $all_states->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <p class="err" id="office_state_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="office_city"><?php echo lang("ctn_393") ?></label>
                            <select class="form-control" name="office_city" id="office_city">
                                 <option value="0">Select City</option>
                                 <?php foreach ($citiesoffice as $city) : ?>
                                    <option value="<?php echo $city->id ?>" <?php if ($core_profile['office_city']==$city->id ) {
                                        echo "selected";
                                    } ?>><?php echo $city->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <p class="err" id="office_city_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="office_pincode">Pincode</label>
                            <input type="text" class="form-control" name="office_pincode" id="office_pincode" value="<?php echo $core_profile['office_pincode']; ?>" placeholder="Enter pincode">
                            <p class="err" id="office_pincode_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="organization_type">Type of organization:*</label>
                            <select name="organization_type" id="organization_type" class="form-control">
                                <option value="0">Select Organization Type</option>
                                <option value="1" <?php if ($core_profile['organization_type'] == "1") {
                                            echo "selected";
                                        } ?>>Public Ltd. Company</option>
                                <option value="2" <?php if ($core_profile['organization_type'] == "2") {
                                            echo "selected";
                                        } ?>>Private Ltd. Company</option>
                                <option value="3" <?php if ($core_profile['organization_type'] == "3") {
                                            echo "selected";
                                        } ?>>State Enterprise</option>
                                <option value="4" <?php if ($core_profile['organization_type'] == "4") {
                                            echo "selected";
                                        } ?>>LLP</option>
                                <option value="5" <?php if ($core_profile['organization_type'] == "5") {
                                            echo "selected";
                                        } ?>>Partnership Firm</option>
                                <option value="6" <?php if ($core_profile['organization_type'] == "6") {
                                            echo "selected";
                                        } ?>>Proprietorship Firm</option>
                                <option value="7" <?php if ($core_profile['organization_type'] == "7") {
                                            echo "selected";
                                        } ?>>NGO</option>
                                <option value="8" <?php if ($core_profile['organization_type'] == "8") {
                                            echo "selected";
                                        } ?>>Trust</option>
                                <option value="9" <?php if ($core_profile['organization_type'] == "9") {
                                            echo "selected";
                                        } ?>>Welfare Society</option>
                            </select>
                            <p class="err" id="organization_type_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="date">Year of Inception</label>
                            <select class="form-control" name="inception_year" id="inception_year" required="">
                                <option value="0">Year of Inception</option>
                                <?php
                                $year = date('Y');
                                for ($i = $year; $i >= 1950; $i--) {
                                ?>
                                    <option value="<?php echo $i ?>" <?php if ($core_profile['inception_year']==$i) {
                                        echo "selected";
                                    } ?>><?php echo $i ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <p class="err" id="inception_year_err"></p>
                        </div>
                        <div class="form-group pro-first">
                            <label for="category">Catagory:*</label>
                            <select class="form-control selectpicker" name="category" id="category" required="">
                                <option value="0">Select Category</option>
                                <?php
                                foreach ($catagories as $catagory) {
                                ?>
                                    <option value="0">Select Categories</option>
                                <?php
                                 $categories2 = json_decode($core_profile['categories']);
                                foreach ($catagories as $catagory) {
                                    
                                ?>
                                    <option value="<?php echo $catagory['cid']; ?>" <?php  if ($catagory['cid']==$core_profile['category']) {
                                        echo "selected";
                                    }  ?>><?php echo $catagory['c_name'] ?></option>
                                <?php
                           
                                }
                                ?>
                                <?php
                                }
                                ?>
                            </select>
                            <p class="err" id="category_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="no_of_employees">No of employees</label>
                            <input type="text" class="form-control" name="no_of_employees" id="no_of_employees" value="<?php  echo $core_profile['no_of_employees']; ?>" placeholder="Enter no of employees">
                            <p class="err" id="no_of_employees_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="annual_turnover">Annual Turnover</label>
                            <input type="text" class="form-control" name="annual_turnover" id="annual_turnover" value="<?php    echo $core_profile['annual_turnover']; ?>" placeholder="Enter annual turnover">
                            <p class="err" id="annual_turnover_err"></p>
                        </div>
                        <div class="form-group">
                            <label for="organization_language" class=" control-label">Language:*</label>
                            <select class="form-control selectpicker" multiple title="Select Language" data-max-options="3" name="organization_language[]" id="organization_language" required="">
                                <option value="0">Select Language</option>
                                <?php
                                 $language = json_decode($core_profile['organization_language']);
                                foreach ($languages as $lang) {
                                ?>
                                    <option value="<?php echo $lang->ID; ?>" <?php foreach ($language as $cat2) { if ($lang->ID==$cat2) {
                                        echo "selected";
                                    } } ?>><?php echo $lang->language; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <p class="err" id="organization_language_err"></p>
                        </div>
                        <div class="form-group pro-first">
                            <label for="organization_categories">SELECT LAW CATEGORIES OF INTERESTS:*</label>
                            <select class="form-control selectpicker" multiple title="Select Categories" data-max-options="6" name="organization_categories[]" id="organization_categories" required="">
                                <option value="0">Select Categories</option>
                                <?php
                                $categories2 = json_decode($core_profile['categories']);
                                foreach ($catagories as $catagory) {
                                ?>
                                    <option value="<?php echo $catagory['cid']; ?>" <?php foreach ($categories2 as $cat2) { if ($cat2==$catagory['cid']) {
                                        echo "selected";
                                    } } ?>><?php echo $catagory['c_name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <p class="err" id="organization_categories_err"></p>
                        </div>
                    </div>
                    <p class="lawyer_practitioner_div">You will be shown customized search results, connection suggestions, study materials, posts in news feeds, and nearby activities based on your selection.</p>
                    <div class="row">
                        <div class="col-md-12">
                            <div style="overflow:auto;" class="pro-nex-prv">
                                <button type="button" id="nextBtn" class="btn first">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<script>
     function change_seeker_type(no) {
        if (no == 1) {
            $(".seeker_type_individual_div").show();
            $(".seeker_type_organization_div").hide();
        } else if (no == 2) {
            $(".seeker_type_individual_div").hide();
            $(".seeker_type_organization_div").show();
        }

    }
    $('.first').click(function() {

        var error = 0;
        var seeker_type = "";
        var date_of_birth = "";
        var address = "";
        var state = "";
        var city = "";
        var pincode = "";
        var education_level = "";
        var degree = "";
        var occupation = "";
        var age = "";
        var language = "";
        var marital_status = "";
        var annual_income = "";
        var categories = "";
        var organization_name = "";
        var office_location = "";
        var office_address = "";
        var office_state = "";
        var office_city = "";
        var office_pincode = "";
        var organization_type = "";
        var inception_year = "";
        var category = "";
        var no_of_employees = "";
        var annual_turnover = "";
        var organization_language = "";
        var organization_categories = "";

        seeker_type = $('input[name=seeker_type]:checked').val();
        date_of_birth = $('#date_of_birth').val();
        address = $('#address').val();
        state = $('#state').val();
        city = $('#city').val();
        pincode = $('#pincode').val();
        education_level = $('#education_level').val();
        degree = $('#degree').val();
        occupation = $('#occupation').val();
        age = $('#age').val();
        language = $('#language').val();
        marital_status = $('#marital_status').val();
        annual_income = $('#annual_income').val();
        categories = $('#categories').val();
        organization_name = $('#organization_name').val();
        office_location = $('#office_location').val();
        office_address = $('#office_address').val();
        office_state = $('#office_state').val();
        office_city = $('#office_city').val();
        office_pincode = $('#office_pincode').val();
        organization_type = $('#organization_type').val();
        inception_year = $('#inception_year').val();
        category = $('#category').val();
        no_of_employees = $('#no_of_employees').val();
        annual_turnover = $('#annual_turnover').val();
        organization_language = $('#organization_language').val();
        organization_categories = $('#organization_categories').val();

        // console.log(language);

        $('#seeker_type_err').html('');
        $('#address_err').html('');
        $('#date_of_birth_err').html('');
        $('#state_err').html('');
        $('#city_err').html('');
        $('#pincode_err').html('');
        $('#education_level_err').html('');
        $('#degree_err').html('');
        $('#occupation_err').html('');
        $('#age_err').html('');
        $('#language_err').html('');
        $('#marital_status_err').html('');
        $('#annual_income_err').html('');
        $('#categories_err').html('');
        $('#organization_name_err').html('');
        $('#office_location_err').html('');
        $('#office_address_err').html('');
        $('#office_state_err').html('');
        $('#office_city_err').html('');
        $('#office_pincode_err').html('');
        $('#organization_type_err').html('');
        $('#inception_year_err').html('');
        $('#category_err').html('');
        $('#no_of_employees_err').html('');
        $('#annual_turnover_err').html('');
        $('#organization_language_err').html('');
        $('#organization_categories_err').html('');

        if (seeker_type == null || seeker_type == "") {
            $('#practioner_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select type');
            error = 1;
        }

        if (date_of_birth == null || date_of_birth == "") {
            $('#date_of_birth_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select type');
            error = 1;
        }

        if (seeker_type == 1) {
            if (address == null || address == "") {
                $('#address_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter address');
                error = 1;
            }
            if (state == 0 || state == "") {
                $('#state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select state');
                error = 1;
            }
            if (city == 0 || city == "") {
                $('#city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select city');
                error = 1;
            }
            if (pincode == null || pincode == "") {
                $('#pincode_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter pincode');
                error = 1;
            }
            if (education_level == 0 || education_level == "") {
                $('#education_level_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select education level');
                error = 1;
            }
            if (degree == null || degree == "") {
                $('#degree_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter degree');
                error = 1;
            }
            if (occupation == null || occupation == "") {
                $('#occupation_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter occupation');
                error = 1;
            }
            if (age == null || age == "") {
                $('#age_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter age');
                error = 1;
            }
            if (language == null || language == "") {
                $('#language_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select language');
                error = 1;
            }
            if (marital_status == 0 || marital_status == "") {
                $('#marital_status_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select marital status');
                error = 1;
            }
            if (annual_income == 0 || annual_income == "") {
                $('#annual_income_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter annual income');
                error = 1;
            }
            if (categories == null || categories == "") {
                $('#categories_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select category');
                error = 1;
            }
        }
        if (seeker_type == 2) {
            if (organization_name == null || organization_name == "") {
                $('#organization_name_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter organization name');
                error = 1;
            }
            if (office_location == null || office_location == "") {
                $('#office_location_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office location');
                error = 1;
            }
            if (office_address == null || office_address == "") {
                $('#office_address_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter address');
                error = 1;
            }
            if (office_state == 0 || office_state == "") {
                $('#office_state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select state');
                error = 1;
            }
            if (office_city == 0 || office_city == "") {
                $('#office_city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select city');
                error = 1;
            }
            if (office_pincode == null || office_pincode == "") {
                $('#office_pincode_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter pincode');
                error = 1;
            }
            if (organization_type == 0 || organization_type == "") {
                $('#organization_type_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select organization type ');
                error = 1;
            }
            if (inception_year == 0 || inception_year == "") {
                $('#inception_year_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select inception year');
                error = 1;
            }
            if (category == 0 || category == "") {
                $('#category_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select category');
                error = 1;
            }
            if (no_of_employees == null || no_of_employees == "") {
                $('#no_of_employees_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter no of employees');
                error = 1;
            }
            if (annual_turnover == null || annual_turnover == "") {
                $('#annual_turnover_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter annual turnover');
                error = 1;
            }
            if (organization_language == null || organization_language == "") {
                $('#organization_language_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select language');
                error = 1;
            }
            if (organization_categories == null || organization_categories == "") {
                $('#organization_categories_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select category');
                error = 1;
            }
        }
        if (error == 1) {
            return false;

        } else {
            $.ajax({
                url: "<?php echo site_url("home/submit_seeker_coreprofile/") ?>",
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: {
                    'seeker_type': seeker_type,
                    'address': address,
                    'date_of_birth': date_of_birth,
                    'state': state,
                    'city': city,
                    'pincode': pincode,
                    'education_level': education_level,
                    'degree': degree,
                    'occupation': occupation,
                    'age': age,
                    'language': language,
                    'marital_status': marital_status,
                    'annual_income': annual_income,
                    'categories': categories,
                    'organization_name': organization_name,
                    'office_location': office_location,
                    'office_address': office_address,
                    'office_state': office_state,
                    'office_city': office_city,
                    'office_pincode': office_pincode,
                    'organization_type': organization_type,
                    'inception_year': inception_year,
                    'category': category,
                    'no_of_employees': no_of_employees,
                    'annual_turnover': annual_turnover,
                    'organization_language': organization_language,
                    'organization_categories': organization_categories,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success: function(data) {
                    var obj = data;

                    if (obj.status_code == true) {
                        toastr.success("Profile updated successfully.");
                        setTimeout(
                        function() 
                        {
                           window.location.href = "<?php echo base_url('edit_profile_seeker'); ?>";
                        }, 2000);
                     

                    }
                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            });
        }
    });

    var verification_level = 0;

    if (verification_level > 0) {
        window.location.href = "<?php echo base_url(); ?>";
    }



   

    $(document).ready(function() {
        seeker_type = $('input[name=seeker_type]:checked').val();
        if (seeker_type != "") {
            change_seeker_type(seeker_type);
        }
    });
</script>
<script>
    function getCity(no) {
        var error = 0;
        var type = no;
        var final_list = "";
        var state_id = "";
        if (type == 0) {
            state_id = $('#state').val();
            final_list = $('#city');
        }
        if (type == 1) {
            state_id = $('#office_state').val();
            final_list = $('#office_city');
        }

        if (state_id == 0 || state_id == "") {
            $('#res_location_state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select state');
            error = 0;
        }
        if (error == 1) {
            return false;
        } else {
            var cities = [];
            var all_city = "";
            var formData = new FormData();
            formData.append('state_id', state_id);
            formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

            $.ajax({
                url: "<?php echo site_url("home/getcitybystate/") ?>",
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: formData,
                success: function(data) {
                    var obj = data;
                    for (let index = 0; index < obj.length; index++) {
                        cities += `<option value="${obj[index].id}" id="">${obj[index].name}</option>`;

                    }
                    final_list.html(cities);
                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            });

        }


    }
    var message = localStorage.getItem('login_message');
    localStorage.removeItem("login_message");
    
    $('.profile-formwrap').find('.alert').remove();
    if (message) {
        $('.profile-formwrap').append('<div class="alert alert-success global_msg"><b><span class="glyphicon glyphicon-ok"></span></b>' + message + '</div>');
    }
</script>