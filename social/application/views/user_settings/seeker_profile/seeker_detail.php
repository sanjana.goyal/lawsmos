<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right">
    <div class="db-header clearfix">
        <div class="page-header-title"> <span class="glyphicon glyphicon-cog"></span> <?php echo lang("ctn_224") ?></div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile</li>
    </ol>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading">My Profile: <button style="float:right;"> <?php if (!empty($core_profile)) { ?><a href="<?php echo base_url('update_profile_seeker'); ?>">Edit</a><?php  }else{
                ?>
                <a href="<?php echo base_url('profile'); ?>">Edit</a
           <?php }  ?> </button></h5> 
        </div>
        <div class="card-body">
            <?php if (!empty($core_profile)) { ?>
                <table class="table borderless-table">
                    <thead>
                        <tr>
                            <th>I am an:</th>
                            <th>Date of Birth:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                if (!empty($core_profile['seeker_type'])) {
                                    if ($core_profile['seeker_type'] == 1) {
                                        echo "Individual";
                                    } else if ($core_profile['seeker_type'] == 2) {
                                        echo "Firm/ Company/ Organization";
                                    } else {
                                        echo "N/A";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($core_profile['date_of_birth'])) {
                                    echo $core_profile['date_of_birth'];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php
                if ($core_profile['seeker_type'] == 1) {
                ?>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Street:</th>
                                <th>State:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['address'])) {
                                        echo $core_profile['address'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['state'])) {
                                        echo getStateName($core_profile['state']);
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>City:</th>
                                <th>Pincode:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['city'])) {
                                        echo getCityName($core_profile['city']);
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['pincode'])) {
                                        echo $core_profile['pincode'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Education Level:</th>
                                <th>Degree:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['education_level'])) {
                                        if ($core_profile['education_level'] == "1") {
                                            echo "Dropout";
                                        } elseif ($core_profile['education_level'] == "2") {
                                            echo "High School";
                                        } elseif ($core_profile['education_level'] == "3") {
                                            echo "Graduate";
                                        } elseif ($core_profile['education_level'] == "4") {
                                            echo "Post Graduate";
                                        } elseif ($core_profile['education_level'] == "5") {
                                            echo "Doctorate";
                                        } else {
                                            echo "N/A";
                                        }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['degree'])) {
                                        echo $core_profile['degree'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Occupation:</th>
                                <th>Age:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['occupation'])) {
                                        echo $core_profile['occupation'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['age'])) {
                                        echo $core_profile['age'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Language:</th>
                                <th>Marital Status:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['language'])) {
                                        $language = json_decode($core_profile['language']);
                                        foreach ($language as $lang) {
                                            echo getLanguageName($lang) . ",";
                                        }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['marital_status'])) {
                                        if ($core_profile['marital_status'] == "1") {
                                            echo "Married";
                                        } elseif ($core_profile['marital_status'] == "2") {
                                            echo "High School";
                                        } else {
                                            echo "Unmarried";
                                        }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Annual Income(In lakhs):</th>
                                <th>Law Categories of Interests:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['annual_income'])) {
                                        if ($core_profile['annual_income'] == "1") {
                                            echo "Under 2 Lakh";
                                        } elseif ($core_profile['annual_income'] == "2") {
                                            echo "2-5 Lakh";
                                        } elseif ($core_profile['annual_income'] == "3") {
                                            echo "5-10 Lakh";
                                        } elseif ($core_profile['annual_income'] == "4") {
                                            echo "10-15 Lakh";
                                        } elseif ($core_profile['annual_income'] == "5") {
                                            echo "15-20 Lakh";
                                        } elseif ($core_profile['annual_income'] == "6") {
                                            echo "20-30 Lakh";
                                        } elseif ($core_profile['annual_income'] == "7") {
                                            echo "30-50 Lakh";
                                        } elseif ($core_profile['annual_income'] == "8") {
                                            echo "50-70 Lakh";
                                        } elseif ($core_profile['annual_income'] == "9") {
                                            echo "70 Lakh -1 Cr";
                                        } elseif ($core_profile['annual_income'] == "10") {
                                            echo "1 cr and above";
                                        } else {
                                            echo "N/A";
                                        }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['categories'])) {
                                        $categories = json_decode($core_profile['categories']);
                                        foreach ($categories as $cat) {
                                            echo getCategoryName($cat) . ",";
                                        }
                                        // echo $core_profile['categories'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php
                } elseif ($core_profile['seeker_type'] == 2) {
                ?>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Name of the organization:</th>
                                <th>Head office location:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['organization_name'])) {
                                        echo $core_profile['organization_name'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['office_location'])) {
                                        echo $core_profile['office_location'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Street:</th>
                                <th>State:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['office_address'])) {
                                        echo $core_profile['office_address'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['office_state'])) {
                                        echo getStateName($core_profile['office_state']);
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>City:</th>
                                <th>Pincode:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['office_city'])) {
                                        echo getCityName($core_profile['office_city']);
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['office_pincode'])) {
                                        echo $core_profile['office_pincode'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Type of organization:</th>
                                <th>Year of Inception:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['organization_type'])) {
                                        if ($core_profile['organization_type'] == "1") {
                                            echo "Public Ltd. Company";
                                        } elseif ($core_profile['organization_type'] == "2") {
                                            echo "Private Ltd. Company";
                                        } elseif ($core_profile['organization_type'] == "3") {
                                            echo "State Enterprise";
                                        } elseif ($core_profile['organization_type'] == "4") {
                                            echo "LLP";
                                        } elseif ($core_profile['organization_type'] == "5") {
                                            echo "Partnership Firm";
                                        } elseif ($core_profile['organization_type'] == "6") {
                                            echo "Proprietorship Firm";
                                        } elseif ($core_profile['organization_type'] == "7") {
                                            echo "NGO";
                                        } elseif ($core_profile['organization_type'] == "8") {
                                            echo "Trust";
                                        } elseif ($core_profile['organization_type'] == "9") {
                                            echo "Welfare Society";
                                        } else {
                                            echo "N/A";
                                        }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['inception_year'])) {
                                        echo $core_profile['inception_year'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Catagory:</th>
                                <th>No of employees:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['category'])) {
                                        echo getCategoryName($core_profile['category']);
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['no_of_employees'])) {
                                        echo $core_profile['no_of_employees'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                        <thead>
                            <tr>
                                <th>Annual Turnover:</th>
                                <th>Language:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['annual_turnover'])) {
                                        echo $core_profile['annual_turnover'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($core_profile['organization_language'])) {
                                        $language = json_decode($core_profile['organization_language']);
                                        foreach ($language as $lang) {
                                            echo getLanguageName($lang) . ",";
                                        }
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table borderless-table">
                    <thead>
                            <tr>
                                <th>Law Categories of Interests:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($core_profile['organization_categories'])) {
                                        $categories = json_decode($core_profile['organization_categories']);
                                        foreach ($categories as $cat) {
                                            echo getCategoryName($cat) . ",";
                                        }
                                        // echo $core_profile['categories'];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                <?php
                }
                ?>
            <?php
            } else {
            ?>
                <table class="table borderless-table">
                    <tbody>
                        <tr>
                            <td style="text-align:center">
                                <h5>No Data Found</h5>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php
            }
            ?>
        </div>
    </div>
</div>