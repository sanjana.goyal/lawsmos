

<div class="white-area-content side-bar-wrap">
<?php   			

	$user=$this->user_model->get_user_by_id($this->user->info->ID);
	$user = $user->row();
	$user_iden = $user->profile_identification;
	$createdbyuser=$this->user_model->get_agency_by_id($this->user->info->ID);
	$createdbyuser = $createdbyuser->row();

	if(!empty($createdbyuser->created_by_user_id)){
		$createdid=$createdbyuser->created_by_user_id;
	}
				
?>
<ul class="settings-sidebar">
	<?php 

	$createagency = base_url("/create/agency");
	$socialsettings = base_url("/usersettings");
	$privacysetting = base_url("/privacy/settings");
	$socialnetwork = base_url("socialnetwork");
	$connectionrequest = base_url("connectionrequests");
	// $mycase = base_url("myjudgements");
	?>
	
	<!-- <li><span class="glyphicon glyphicon-cog"></span> <a href="<?php // echo site_url("profile/" . $this->user->info->username) ?>"><?php // echo lang("ctn_491") ?></a></li> -->
	
	<?php 	if($user_iden==0 && $user->is_agency_joined==0){
		      
				if(empty($createdid)){
	?>
					<li class="<?php if(current_url()==$createagency) echo "active" ?>"><span class="glyphicon glyphicon-list-alt"></span> <a href="<?php echo site_url("create/agency") ?>"><?php echo lang("ctn_829") ?></a></li>
	<?php 	}
		

	}
	 ?>
	<li class="<?php if(current_url()==$socialsettings) echo "active" ?>"> <span class="glyphicon glyphicon-cog"></span><a href="<?php echo site_url("usersettings") ?>"><?php echo lang("ctn_156") ?></a></li>
	
	<?php 	if($user_iden==3 && $user->is_agency_joined==0){ ?>
	
					<li><span class="glyphicon glyphicon-cog"></span> <a href="<?php echo site_url("create/university") ?>">Create University Page</a></li>
	
	<?php 	}
		

	
	 ?>
	
	
	

<!--

	<?php //endif; ?>
	-->
	<li><span class="glyphicon glyphicon-lock"></span><a href="<?php echo site_url("update/password") ?>"><?php echo lang("ctn_225") ?></a></li>
	<li class="<?php if(current_url()==$privacysetting) echo "active" ?>"><span class="glyphicon glyphicon-eye-open"></span> <a href="<?php echo site_url("privacy/settings") ?>"><?php echo lang("ctn_629") ?></a></li>
	<li  class="<?php if(current_url()==$socialnetwork) echo "active" ?>"><span class="glyphicon glyphicon-glass"></span> <a href="<?php echo site_url("socialnetwork") ?>"><?php echo lang("ctn_422") ?></a></li>
	
	<li  class="<?php if(current_url()==$connectionrequest) echo "active" ?>"><span class="glyphicon glyphicon-info-sign"></span> <a href="<?php echo site_url("connectionrequests") ?>"><?php echo lang("ctn_640") ?></a></li>
	
		

	<!-- <li><span class="glyphicon glyphicon-user"></span> <a href="<?php //echo site_url("user_settings/friend_requests") ?>"><?php //echo lang("ctn_640") ?></a></li> -->
	
	<?php if($user_iden==0) : ?>
	<?php if($this->settings->info->payment_enabled) : ?>
        <!-- <li><a href="<?php echo site_url("funds") ?>"><span class="glyphicon glyphicon-briefcase sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_250") ?></a></li> -->
        <?php endif; ?>
      
     <?php endif; ?>
     
     
     
     <!-- <?php if($user_iden==0) : ?>
	
        <li  class="<?php if(current_url()==$mycase) echo "active" ?>"><a href="<?php echo site_url("myjudgements") ?>"><span class="glyphicon glyphicon-piggy-bank sidebaricon" style="color: #4490f6"></span> <?php echo lang("ctn_811") ?></a></li>
        
      
     <?php endif; ?> -->
	<!-- <li><span class="glyphicon glyphicon-file"></span> <a href="<?php //echo site_url("user_settings/page_invites") ?>"><?php// echo lang("ctn_626") ?></a></li> -->
	
	<!--
	<?php //if($this->settings->info->enable_verified_requests) : ?>
		<li><span class="glyphicon glyphicon-ok"></span> <a href="<?php //echo site_url("user_settings/verified") ?>"><?php //echo lang("ctn_743") ?></a></li>
	<?php //endif;  ?>
	-->
</ul>
</div>
