<style>
    #responsive-menu-links{
        display:none;
    }
</style>
 <?php $this->load->view('sidebar/sidebar.php'); ?>

<div class="white-area-content separator page-right ">
<div class="db-header clearfix">
    <div class="page-header-title"> <span class="glyphicon glyphicon-glass"></span> <?php echo lang("ctn_422") ?></div>
    <div class="db-header-extra">
</div>
</div>

<ol class="breadcrumb">
  <li><a href="<?php echo site_url() ?>"><?php echo lang("ctn_2") ?></a></li>
  <!-- <li><a href="<?php echo site_url("user_settings") ?>"><?php echo lang("ctn_224") ?></a></li> -->
  <li><a href="<?php echo site_url("usersettings") ?>"><?php echo lang("ctn_224") ?></a></li>
  <li class="active"><?php echo lang("ctn_422") ?></li>
</ol>

<div class="panel panel-default">
  	<div class="panel-body">
  	<?php echo form_open(site_url("user_settings/social_networks_pro"), array("class" => "form-horizontal")) ?>
            <div class="form-group">
			    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_426") ?></label>
			    <div class="pro-input-wrap">
			      <input type="text" class="form-control" name="twitter" value="<?php echo $user_data->twitter ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_427") ?></label>
			    <div class="pro-input-wrap">
			      <input type="text" class="form-control" name="facebook" value="<?php echo $user_data->facebook ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_428") ?></label>
			    <div class="pro-input-wrap">
			      <input type="text" class="form-control" name="google" value="<?php echo $user_data->google ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_429") ?></label>
			    <div class="pro-input-wrap">
			      <input type="text" class="form-control" name="linkedin" value="<?php echo $user_data->linkedin ?>">
			    </div>
			</div>
			<div class="form-group">
			    <label for="inputEmail3" class="profile-label-p"><?php echo lang("ctn_425") ?></label>
			    <div class="pro-input-wrap">
			      <input type="text" class="form-control" name="website" value="<?php echo $user_data->website ?>">
			    </div>
			</div>
			<div class="profile-set-wrap">
			 <input type="submit" name="s" value="<?php echo lang("ctn_13") ?>" class="btn  profile-set-btn" />
    <?php echo form_close() ?>
    </div>
    </div>
    </div>

</div>

</div>