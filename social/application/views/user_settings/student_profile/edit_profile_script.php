<script type="text/javascript">
    $(document).ready(function() {
        // console.log(currentTab);
        var maxField = 1; //Input fields increment limitation
        var addButton = $('.add_court'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper

        //<a href="javascript:void(0);" class="remove_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> Add More </a>
        var fieldHTML = '<div><div class="row"><div class="col-md-10"><input id ="add_court_name" maxlength="100" type="text" maxlength="100" name="field_court[]" value="" class="col-sm-3 form-control" placeholder="enter court name"/></div><div class="col-md-2"><div class="form-group"><a href="javascript:void(0);" class="remove_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span> remove </a></div>';

        var x = 1; //Initial field counter is 1

        prefilled_old_data();

    });
</script>
<script>
    function prefilled_old_data() {
        if (profile_step  == 1) {
            prefilled_step_1();
        } else if (profile_step == 2) {
            prefilled_step_2();
        } else if (profile_step == 3) {
            prefilled_step_3();
        } else if (profile_step == 4) {
            prefilled_step_4();
        } else if (profile_step == 5) {
            prefilled_step_5();
        } else if (profile_step == 6) {
            prefilled_step_6();
        }

    }

    function prefilled_step_1() {
        var lawyer_practitioner = "<?php echo $core_profile['lawyer_practitioner']; ?>";
        if (lawyer_practitioner != "") {
            if (lawyer_practitioner == 2 || lawyer_practitioner == 3) {
                $(".lawyer_practitioner_div").show();
            }
        }
    }

    function prefilled_step_2() {
        var bar_association_member = "<?php echo $core_profile['bar_association_member']; ?>";
        var practicing_lawyer = "<?php echo $core_profile['practicing_lawyer']; ?>";
        var unpracticing_working = "<?php echo $core_profile['unpracticing_working_as']; ?>";
        // alert(bar_association_member);
        if (practicing_lawyer == 1 || practicing_lawyer == 2) {
            lawyerPractice(practicing_lawyer);
        }
        if (practicing_lawyer == 1 || practicing_lawyer == 2) {
            unpracticing_working_change(unpracticing_working);
        }
        if (bar_association_member == 1) {
            $(".bar_reg").show();
        }
    }

    function prefilled_step_3() {
        // alert("prefilled_step_3");
        var x_marks_type = "<?php echo $core_profile['x_marks_type']; ?>";
        var xii_marks_type = "<?php echo $core_profile['xii_marks_type']; ?>";
        if (x_marks_type == 1 || x_marks_type == 2 || x_marks_type == 3) {
            marks_x_mode(x_marks_type-1);
        }
        if (xii_marks_type == 1 || xii_marks_type == 2 || xii_marks_type == 3) {
            marks_xii__mode(xii_marks_type-1);
        }
    }

    function prefilled_step_4() {
        // alert("prefilled_step_4");
    }

    function prefilled_step_5() {
        var office_consultation_type = $('input[name=office_consultation]:checked').val();
        var site_consultation_type = $('input[name=site_consultation]:checked').val();
        var travel_consultation_type = $('input[name=travel_consultation]:checked').val();
        var telvoice_consultation_type = $('input[name=telvoice_consultation]:checked').val();
        var telvideo_consultation_type = $('input[name=telvideo_consultation]:checked').val();
        var email_consultation_type = $('input[name=email_consultation]:checked').val();
        

        office_consultation_div(office_consultation_type);
        site_consultation_div(site_consultation_type);
        travel_consultation_div();
        telvoice_consultation_div(telvoice_consultation_type);
        telvideo_consultation_div(telvideo_consultation_type);
        email_consultation_div(email_consultation_type);
       
    }

    function prefilled_step_6() {
        var firm_state = $("#firm_state").val();
        var location_state = $("#location_state").val();
        var tmp_location_state = $("#tmp_location_state").val();
        if(firm_state > 0){
            getCity(1);
        }
        if(location_state > 0){ 
            getCity(0);
        }
        if(tmp_location_state > 0){
            getCity(2);
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var maxField = 4; //Input fields increment limitation
        var addExperience = $('.add_experience'); //Add button selector
        var wrapper_experience = $('.field_wrapper_experience'); //Input field wrapper

        if($("#previous_experience_count").val()){
            var x = $("#previous_experience_count").val();
        }else{
            var x = 1; //Initial field counter is 1
        }

        //Once add button is clicked
        $(addExperience).click(function() {
            //Check maximum number of input fields
            if (x < maxField) {
                var fieldHTML = `<div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label for="experience_organization_name">Name of organization :*</label>
                           <input type="text" maxlength="100" name="experience_organization_name[]" id="experience_organization_name" class="form-control" placeholder="Enter organization name"   value=""/>
                           <p class ="err" id="experience_organization_name_err_${x}"></p>
                        </div>
                        <div class="form-group">
                           <label for="experience_city" class="control-label">City:*</label>
                           <select class="form-control selectpicker" title="Select City" name="experience_city[]" id="experience_city" >
                           <option value="" >Select City</option>
                              <?php
                                $selected_city = explode(',', $core_profile['experience_city']);
                                foreach ($all_city as $city) {

                                ?>
                                    <option value="<?php echo $city->id; ?>" ><?php echo $city->name; ?></option>
                                    <?php
                                }
                                    ?> 
                           </select>
                           <p class ="err" id="experience_city_err_${x}"></p>   
                        </div>
                        <div class="form-group">
                           <label for="experience_court">Court (if applicable) :*</label>
                           <select name="experience_court[]" id="experience_court" class="form-control selectpicker"  title="Select Court Name" placeholder="select court name">
                           <option value="" >Select Court</option>
                              <?php foreach ($courts->result() as $court) {

                                ?>
                                 <option value="<?php echo $court->id; ?>" ><?php echo $court->court_name; ?></option>
                              <?php } ?>
                           </select>
                           <p class ="err" id="experience_court_err_${x}"></p>
                        </div>
                        <div class="form-group">
                           <label for="experience_designation">Designation :*</label>
                           <select name="experience_designation[]" id="experience_designation" class="form-control" title="" >
                              <option value="">Select Designation</option>
                              <option value="1">Designated Senior</option>
                              <option value="2">Retired Justice</option>
                           </select>
                           <p class="err" id="experience_designation_err_${x}"></p>
                        </div>
                        <div class="form-group">
                           <label for="experience_start_date">Start Date :*</label>
                           <input type="date" class="form-control" id="experience_start_date" name="experience_start_date[]" class="form-control"  />
                           <p class ="err" id="experience_start_date_err_${x}"></p>
                        </div>
                        <div class="form-group">
                           <label for="experience_end_date">End Date (If you currently work here please don't select end date) :</label>
                           <input type="date" class="form-control" id="experience_end_date" name="experience_end_date[]" class="form-control"  />
                           <p class ="err" id="experience_end_date_err_${x}"></p>
                        </div>
                     </div>
                  
   <div class="col-md-12 text-center"><a href="javascript:void(0);" class="remove_experience_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 20px;"><span class="glyphicon glyphicon-minus"></span> Remove </a></div></div>`;


                x++; //Increment field counter
                $(wrapper_experience).append(fieldHTML); //Add field html
            }
        });

        //Once remove button is clicked
        $(wrapper_experience).on('click', '.remove_experience_button', function(e) {
            e.preventDefault();
            //$(this).parent("div").remove(); //Remove field html

            $(this).parent("div").parent("div").remove();
            x--; //Decrement field counter
        });
    });
</script>
<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)

    function prefill_firm_data() {
        $.ajax({
            url: "<?php echo site_url("home/getFirmAddress/") ?>",
            type: "get",
            dataType: "json",
            data: {},
            success: function(data) {
                console.log("data", data);
                if (data == null) {
                    return false;
                }
                if (data.address_1 != "") {
                    $("#firm_address_1").val(data.address_1);
                    $("#firm_address_1").prop("disabled", true);
                }
                if (data.address_2 != "") {
                    $("#firm_address_2").val(data.address_2);
                    $("#firm_address_2").prop("disabled", true);
                }
                if (data.address3 != "") {
                    $("#firm_address_3").val(data.address3);
                    $("#firm_address_3").prop("disabled", true);
                }

                if (data.state != "") {
                    $("#firm_state").val(data.state);
                    $("#firm_state").prop("disabled", true);
                    getCity(1);
                }
                if (data.city != "") {
                    $("#firm_city").val(data.city);
                    $("#firm_city").prop("disabled", true);
                }
                if (data.country != "") {
                    $("#firm_country").val(data.country);
                    $("#firm_country").prop("disabled", true);
                }
                if (data.lat != "") {
                    $("#firm_lat").val(data.lat);
                    $("#firm_lat").prop("disabled", true);
                }
                if (data.longitude != "") {
                    $("#firm_lng").val(data.longitude);
                    $("#firm_lng").prop("disabled", true);
                }
                if (data.zipcode != "") {
                    $("#firm_pincode").val(data.zipcode);
                    $("#firm_pincode").prop("disabled", true);
                }
            },
            error: function(error) {
                console.log(`Error ${error}`);
            }
        });
    }

    function showfirm() {
        $("#firm_address").show();
        $("#firm_address_exist").val("1");
        prefill_firm_data();
    }

    function add_new_address() {
        $("#add_address").hide();
        $("#office_address_2").show();
        $("#new_address").val('1');
    }

    function remove_address() {
        $("#add_address").show();
        $("#office_address_2").hide();
        $("#new_address").val('0');
    }


    $('.first').click(function() {

        var error = 0;
        var lawyer_practitioner = "";
        var firm_name = "";
        var firm_email = "";
        var user_email = "";
        var date_of_birth = "";

        lawyer_practitioner = $('input[name=lawyer_practitioner]:checked').val();

        firm_name = $('#firm_name').val();
        firm_email = $('#firm_email_address').val();
        user_email = $('#user_email').val();
        date_of_birth = $('#date_of_birth').val();

        $('#firm_name_err').html('');
        $('#practioner_err').html('');
        $('#firm_email_address_err').html('');
        $('#date_of_birth_err').html('');

        if (date_of_birth == null || date_of_birth == "") {
            $('#date_of_birth_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select date of birth');
            error = 1;
        }

        if (lawyer_practitioner == null || lawyer_practitioner == "") {
            $('#practioner_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select practitioner type');
            error = 1;
        }

        if (lawyer_practitioner == 2 || lawyer_practitioner == 3) {
            if (firm_name == null || firm_name == "") {
                $('#firm_name_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm name');
                error = 1;
            }

            if (firm_email == null || firm_email == "") {
                $('#firm_email_address_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm`s email Address');
                error = 1;
            } else {
                if (firm_email == user_email) {
                    $('#firm_email_address_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Firm email address must be different with your email address');
                    error = 1;
                }
            }


        }
        if (error == 1) {
            return false;

        } else {
            $.ajax({
                url: "<?php echo site_url("home/step1/") ?>",
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: {
                    'lawyer_practitioner': lawyer_practitioner,
                    'date_of_birth': date_of_birth,
                    'firm_name': firm_name,
                    'firm_email': firm_email,
                    'update_verification_level': false,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success: function(data) {
                    var obj = data;

                    if (obj.status_code == true) {
                        window.location.href = "<?php echo base_url('edit_profile_lawyer'); ?>";
                    }
                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            });
        }
    });
    $('.second').click(function() {

        var error = 0;


        $('#practicing_lawyer_err').html('');
        $('#unpracticing_city_err').html('');
        $('#unpracticing_fields_of_interest_err').html('');
        $('#unpracticing_area_of_specialization_err').html('');
        $('#unpracticing_working_as_err').html('');
        $('#organization_err').html('');
        $('#city_err').html('');
        $('#spec_err').html('');
        $('#court_err').html('');
        $('#lan_err').html('');
        $('#coun_err').html('');
        $('#year_err').html('');
        $('#counform_err').html('');
        $('#bar_member_err').html('');
        $('#total_exp_err').html('');
        $('#reg_bar_name_err').html('');
        $('#reg_bar_no_err').html('');
        $('#reg_bar_formno_err').html('');
        $('#agree_checkbox_step_2_err').html('');


        var bar_association_name = [];
        var barregform = [];
        var barregform_0 = "";
        var barregform_1 = "";
        var regno = [];
        var add_court = "";

        var practicing_lawyer = $('input[name=practicing_lawyer]:checked').val();
        var unpracticing_city = $('#unpracticing_city').val();
        var unpracticing_fields_of_interest = $('#unpracticing_fields_of_interest').val();
        var unpracticing_area_of_specialization = $('#unpracticing_area_of_specialization').val();
        var unpracticing_working_as = $('input[name=unpracticing_working_as]:checked').val();
        var organization_name = $('#organization_name').val();
        var practice_city = $('#city_practice').val();
        var cat_type_id = $('#category_id').val();
        var court_name = $('#field_court').val();
        var add_court_field = $('input[name^=add_court_field]').map(function(idx, elem) {
            return $(elem).val();
        }).get();
        var lang = $('#lang').val();
        var councelregno = $('#councelregno').val();
        var total_exp = $('#total_exp').val();
        var year_reg = $('#year_reg option:selected').val();
        var bar_association_member = $('input[name=registration_member]:checked').val();
        var councelregform = $('#councelregform').prop('files')[0];
        var councelregform_url = $('#councelregform_url').val();
        barregform_0 = $('#barregform_0').prop('files')[0];

        // console.log("file",barregform_0);

        if (practicing_lawyer != 1 && practicing_lawyer != 2) {
            $("#practicing_lawyer_err").html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select this field');
            error = 1;
        }
        if (practicing_lawyer == 1) {
            if (practice_city == null || practice_city == "") {
                $('#city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select atleast 1 city or max 2 cities');
                error = 1;
            }
            if (cat_type_id == null || cat_type_id == "") {
                $('#spec_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select atlest 1 specialities or maximum 6');
                error = 1;
            }
            if (court_name == null || court_name == "") {
                $('#court_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select atlest 1 court name or max 6');
                error = 1;
            }
            if (lang == null || lang == "") {
                $('#lan_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select atlest 1 language or max 3');
                error = 1;
            }
            if (councelregno == null || councelregno == "") {
                $('#coun_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please provide Council no');
                error = 1;
            }
            if (year_reg <= 0) {
                $('#year_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select year of registration');
                error = 1;
            }
            if (councelregform == "" || councelregform == null) {
                if (councelregform_url == "") {
                    $('#counform_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please upload state Bar Registration ID');
                    error = 1;
                }

            }
            if (total_exp == null || total_exp == "") {
                $('#total_exp_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter total experience');
                error = 1;
            } else {
                if ((total_exp != "") && (year_reg > 0)) {
                    var current_year = new Date().getFullYear();
                    var year_difference = current_year - year_reg;
                    if (total_exp > year_difference) {
                        $('#total_exp_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Total experience should be according to year of registration');
                        error = 1;
                    }
                }
            }
            if (bar_association_member == null || bar_association_member == "") {
                $('#bar_member_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select Bar Association members');
                error = 1;
            } else {
                if (bar_association_member == "1") {


                    $('input[name="bar_registration_name[]"]').each(function(i, item) {
                        bar_association_name.push(item.value);
                    });
                    $('input[name="registration_no[]"]').each(function(i, item) {
                        regno.push(item.value);
                    });
                    $('input[name="barregform[]"]').each(function(i, item) {
                        barregform.push(item.value);
                    });


                    if (barregform.length > 1) {
                        barregform_1 = $('#barregform_1').prop('files')[0];
                    }



                    for (let index = 0; index < bar_association_name.length; index++) {
                        $('#reg_bar_name_err_' + index).html('');

                        if (bar_association_name[index] == "" || bar_association_name[index] == null) {
                            $('#reg_bar_name_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter Bar Association name');
                            error = 1;
                        }

                    }
                    for (let index = 0; index < regno.length; index++) {
                        $('#reg_bar_no_err_' + index).html('');
                        if (regno[index] == "" || regno[index] == null) {
                            $('#reg_bar_no_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter Bar Association no');
                            error = 1;
                        }

                    }
                    // for (let index = 0; index < barregform.length; index++) {
                    //     $('#reg_bar_formno_err_' + index).html('');
                    //     if (barregform[index] == "" || barregform[index] == null) {
                    //         $('#reg_bar_formno_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select Bar Association form');
                    //         error = 1;
                    //     }

                    // }
                }
            }
            if ($("#agree_checkbox_step_2").prop("checked") == false) {
                $('#agree_checkbox_step_2_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>This field is required');
                error = 1;
            }
        }
        if (practicing_lawyer == 2) {
            if (unpracticing_city == null || unpracticing_city == "") {
                $('#unpracticing_city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select city field');
                error = 1;
            }
            if (unpracticing_fields_of_interest == null || unpracticing_fields_of_interest == "") {
                $('#unpracticing_fields_of_interest_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select fields of interest');
                error = 1;
            }
            if (unpracticing_area_of_specialization == null || unpracticing_area_of_specialization == "") {
                $('#unpracticing_area_of_specialization_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select area of speacialization');
                error = 1;
            }
            if (unpracticing_working_as == null || unpracticing_working_as == "") {
                $('#unpracticing_working_as_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select working as');
                error = 1;
            }
            if (unpracticing_working_as == 2) {
                if (organization_name == null || organization_name == "") {
                    $('#organization_name_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter organization name');
                    error = 1;
                }
            }
        }




        if (add_court == null || add_court == "") {
            add_court = "";
        }



        if (error == 1) {
            return false;
        } else {

            var formData = new FormData();
            formData.append('practicing_lawyer', practicing_lawyer);
            formData.append('unpracticing_city', unpracticing_city);
            formData.append('unpracticing_fields_of_interest', unpracticing_fields_of_interest);
            formData.append('unpracticing_area_of_specialization', unpracticing_area_of_specialization);
            formData.append('unpracticing_working_as', unpracticing_working_as);
            formData.append('organization_name', organization_name);
            formData.append('cat_type_id', cat_type_id);
            formData.append('lang', lang);
            formData.append('councelregno', councelregno);
            formData.append('year_reg', year_reg);
            formData.append('total_exp', total_exp);
            formData.append('practice_city', practice_city);
            formData.append('court_name', court_name);
            formData.append('add_court_field', add_court_field);
            formData.append('councelregform', councelregform);
            formData.append('councelregform_url', councelregform_url);
            formData.append('add_court', add_court);
            formData.append('bar_association_member', bar_association_member);
            formData.append('regno', regno);
            formData.append('bar_association_name', bar_association_name);
            formData.append('barregform_0', barregform_0);
            formData.append('barregform_1', barregform_1);
            formData.append('update_verification_level', false);
            formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

            $.ajax({
                url: "<?php echo site_url("home/step2/") ?>",
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: formData,
                success: function(data) {
                    var obj = data;

                    if (obj.status_code == true) {
                        window.location.href = "<?php echo base_url('edit_profile_lawyer'); ?>";
                    }
                    if (obj.status_code == 1 && obj.message == "state_bar_no_already_exist") {
                        $('#coun_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>State Bar Council Registration no is already registrated with us');
                    }
                    if (obj.status_code == 2 && obj.message == "bar_reg_no_already_exist") {
                        $('#reg_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>This Bar Association Registration no is already registrated with us');
                    }


                },
                error: function(error) {
                    console.log(error);
                    console.log(`Error ${error}`);
                }
            });
        }

    });

    var limit = 6;
    let selectValue = null;
    $('input.single-checkbox').on('change', function(evt) {
        if ($('.single-checkbox:checked').length > limit) {
            this.checked = false;
        }
    });


    function readURL_state_bar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#state_bar')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL(input, classname, hidden = '') {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(input).siblings('.' + classname).attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

            var formData = new FormData();
            formData.append('marksheetform', input.files[0]);
            formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

            $.ajax({
                url: "<?php echo site_url("home/uploadmarksheetform/") ?>",
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function() {
                    // $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: formData,
                success: function(data) {
                    console.log(data);
                    var obj = data;
                    if (obj.status == 1) {
                        $(input).siblings('.' + hidden).val(obj.message);
                    }

                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            });
        }
    }

    function readURL_bar_association(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#' + input.id + '_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_profile_pic(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#profile_pic_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_cover_pic(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#cover_pic_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_office_picture1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#office_picture1_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_office_picture2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#office_picture2_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_office_picture3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#office_picture3_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL_office_picture4(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#office_picture4_preview')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.third').click(function() {
        // 10th detail
        var error = 0;

        $('#school_10_err').html('');
        $('#board_10_err').html('');
        $('#year_pass_10_err').html('');
        $('#x_marks_err').html('');

        $('#school_12_err').html('');
        $('#board_12_err').html('');
        $('#year_pass_12_err').html('');
        $('#xii_marks_err').html('');

        $('#degree_err').html('');
        $('#yearstart_err').html('');
        $('#yearend_err').html('');
        $('#university_err').html('');
        $('#college_err').html('');
        $('#division_err').html('');
        $('.err').html('');
        $('#agree_checkbox_step_3_err').html('');




        var marks_x_percentage = "";
        var x_division = "";
        var marks_x_cgpa = "";

        var marks_xii_percentage = "";
        var xii_division = "";
        var marks_xii_cgpa = "";


        var school_10 = $('#school_10').val();
        var board_10 = $('#board_10').val();
        var year_pass_10 = $('#year_pass_10').val();
        var x_marks_mode = $('input[name=marks_x_radio]:checked').val();




        // 12th detail
        var school_12 = $('#school_12').val();
        var board_12 = $('#board_12').val();
        var year_pass_12 = $('#year_pass_12').val();
        var xii_marks_mode = $('input[name=marks_xii_radio]:checked').val();

        var degree = [];
        var yearstart = [];
        var yearend = [];
        var college = [];
        var university = [];
        var division = [];
        var marksheetform_url = [];


        $('input[name="degree[]"]').each(function(i, item) {
            degree.push(item.value);
        });
        $('select[name="yearstart[]"]').each(function(i, item) {
            yearstart.push(item.value);
        });
        $('select[name="yearend[]"]').each(function(i, item) {
            yearend.push(item.value);
        });
        $('input[name="college[]"]').each(function(i, item) {
            college.push(item.value);
        });
        $('input[name="university[]"]').each(function(i, item) {
            university.push(item.value);
        });
        $('select[name="division[]"]').each(function(i, item) {
            division.push(item.value);
        });
        $('input[name="marksheetform_url[]"]').each(function(i, item) {
            marksheetform_url.push(item.value);
        });




        //10 school validation
        if (school_10 == "" || school_10 == null) {
            $('#school_10_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please Fill School Name');
            error = 1;
        }
        if (board_10 == "" || board_10 == null) {
            $('#board_10_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please Fill School Name');
            error = 1;;
        }
        if (year_pass_10 <= 0) {
            $('#year_pass_10_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please Select Year Of Passing ');
            error = 1;;
        }
        if (x_marks_mode == "" || x_marks_mode == null) {
            $('#x_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select marks detail');
            error = 1;;
        } else {
            if (x_marks_mode == 1) {
                marks_x_percentage = $('#marks_x_percentage').val();
                if (marks_x_percentage == "" || marks_x_percentage == null) {
                    $('#x_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter marks in percentage');
                    error = 1;
                }
            }
            if (x_marks_mode == 2) {
                var x_division = $('#x_division').val();
                if (x_division == "" || x_division == 0) {
                    $('#x_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select divison');
                    error = 1;
                }
            }
            if (x_marks_mode == 3) {
                marks_x_cgpa = $('#marks_x_cgpa').val();
                if (marks_x_cgpa == "" || marks_x_cgpa == null) {
                    $('#x_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter in marks in cgpa');
                    error = 1;
                }
            }
        }

        //12 school validation
        if (school_12 == "" || school_12 == null) {
            $('#school_12_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter school name');
            error = 1;
        }
        if (board_12 == "" || board_12 == null) {
            $('#board_12_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter board name');
            error = 1;
        }

        if (year_pass_12 <= 0) {
            $('#year_pass_12_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select year of passing ');
            error = 1;
        }

        if (xii_marks_mode == "" || xii_marks_mode == null) {
            $('#xii_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select marks detail');
            error = 1;
        } else {

            if (xii_marks_mode == 1) {
                marks_xii_percentage = $('#marks_xii__percentage').val();
                if (marks_xii_percentage == "" || marks_xii_percentage == null) {
                    $('#xii_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter marks in percentage');
                    error = 1;
                }
            }
            if (xii_marks_mode == 2) {
                xii_division = $('#xii_division').val();
                if (xii_division == 0) {
                    $('#xii_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select divison');
                    error = 1;
                }
            }
            if (xii_marks_mode == 3) {
                marks_xii_cgpa = $('#marks_xii__cgpa').val();
                if (marks_xii_cgpa == "" || marks_xii_cgpa == null) {
                    $('#xii_marks_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter in marks in cgpa');
                    error = 1;
                }
            }
        }




        // //qualification validation

        // if(degree == "" || degree == null){
        //    console.log("school_10",school_10);
        //    $('#degree_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter degree detail');
        //    error = 1;
        // }

        // if(yearstart == "" || yearstart == null){
        //    console.log("school_10",school_10);
        //    $('#yearstart_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter yearstart detail');
        //    error = 1;
        // }


        // if(yearend == "" || yearend == null){
        //    console.log("school_10",school_10);
        //    $('#yearend_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter yearend detail');
        //    error = 1;
        // }


        // if(university == "" || university == null){
        //    console.log("school_10",school_10);
        //    $('#university_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter university detail');
        //    error = 1;
        // }


        // if(college == "" || college == null){
        //    console.log("school_10",school_10);
        //    $('#college_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter university detail');
        //    error = 1;
        // }


        for (let index = 0; index < degree.length; index++) {
            $('#degree_err_' + index).html('');
            if (degree[index] == "") {
                $('#degree_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter degree/course name.');
                error = 1;
            }
        }

        for (let index = 0; index < yearstart.length; index++) {
            $('#yearstart_err_' + index).html('');
            if (yearstart[index] == "") {
                $('#yearstart_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select start date.');
                error = 1;
            }
        }

        for (let index = 0; index < yearend.length; index++) {
            $('#yearend_err_' + index).html('');
            if (yearend[index] == "") {
                $('#yearend_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select stendart date.');
                error = 1;
            }
        }

        for (let index = 0; index < college.length; index++) {
            $('#college_err_' + index).html('');
            if (college[index] == "") {
                $('#college_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter college name.');
                error = 1;
            }
        }

        for (let index = 0; index < university.length; index++) {
            $('#university_err_' + index).html('');
            if (university[index] == 0) {
                $('#university_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter university name.');
                error = 1;
            }
        }

        for (let index = 0; index < division.length; index++) {
            $('#division_err_' + index).html('');
            if (division[index] == 0) {
                $('#division_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select division.');
                error = 1;
            }
        }

        for (let index = 0; index < marksheetform_url.length; index++) {
            $('#marksheet_err_' + index).html('');
            if (marksheetform_url[index] == "") {
                $('#marksheet_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please upload marksheet for given qualification.');
                error = 1;
            }
        }
        if ($("#agree_checkbox_step_3").prop("checked") == false) {
            $('#agree_checkbox_step_3_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>This field is required');
            error = 1;
        }

        if (error == 1) {
            console.log("error", error);
            return false;
        } else {
            $.ajax({
                url: "<?php echo site_url("home/step3/") ?>",
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: {
                    'school_10': school_10,
                    'board_10': board_10,
                    'year_pass_10': year_pass_10,
                    'x_marks_type': x_marks_mode,
                    'marks_x_percentage': marks_x_percentage,
                    'x_division': x_division,
                    'marks_x_cgpa': marks_x_cgpa,
                    'school_12': school_12,
                    'board_12': board_12,
                    'year_pass_12': year_pass_12,
                    'xii_marks_type': xii_marks_mode,
                    'marks_xii_percentage': marks_xii_percentage,
                    'xii_division': xii_division,
                    'marks_xii_cgpa': marks_xii_cgpa,
                    'degree': degree,
                    'yearstart': yearstart,
                    'yearend': yearend,
                    'college': college,
                    'university': university,
                    'division': division,
                    'marksheetform_url': marksheetform_url,
                    'update_verification_level': false,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success: function(data) {
                    var obj = data;
                    if (obj.status == true) {
                        window.location.href = "<?php echo base_url('edit_profile_lawyer'); ?>";
                    }
                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            })
        }



    });
    $('.fourth').click(function() {
        var error = 0;

        var experience_organization_name = [];
        var experience_city = [];
        var experience_court = [];
        var experience_designation = [];
        var experience_start_date = [];
        var experience_end_date = [];


        $('input[name="experience_organization_name[]"]').each(function(i, item) {
            experience_organization_name.push(item.value);
        });
        $('select[name="experience_city[]"]').each(function(i, item) {
            experience_city.push(item.value);
        });
        $('select[name="experience_court[]"]').each(function(i, item) {
            experience_court.push(item.value);
        });
        $('select[name="experience_designation[]"]').each(function(i, item) {
            console.log("i", i)
            console.log("item", item)
            experience_designation.push(item.value);
        });
        $('input[name="experience_start_date[]"]').each(function(i, item) {
            experience_start_date.push(item.value);
        });
        $('input[name="experience_end_date[]"]').each(function(i, item) {
            experience_end_date.push(item.value);
        });
        // //qualification validation

        for (let index = 0; index < experience_organization_name.length; index++) {
            $('#experience_organization_name_err_' + index).html('');
            if (experience_organization_name[index] == "") {
                $('#experience_organization_name_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter organization name.');
                error = 1;
            }
        }

        for (let index = 0; index < experience_city.length; index++) {
            $('#experience_city_err_' + index).html('');
            if (experience_city[index] == "") {
                $('#experience_city_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select city name.');
                error = 1;
            }
        }

        for (let index = 0; index < experience_court.length; index++) {
            $('#experience_court_err_' + index).html('');
            if (experience_court[index] == "") {
                $('#experience_court_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select court name.');
                error = 1;
            }
        }

        for (let index = 0; index < experience_designation.length; index++) {
            $('#experience_designation_err_' + index).html('');
            if (experience_designation[index] == "") {
                $('#experience_designation_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter your designation.');
                error = 1;
            }
        }

        for (let index = 0; index < experience_start_date.length; index++) {
            $('#experience_start_date_err_' + index).html('');
            if (experience_start_date[index] == 0) {
                $('#experience_start_date_err_' + index).html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter start date.');
                error = 1;
            }
        }

        if (error == 1) {
            return false;
        } else {
            $.ajax({
                url: "<?php echo site_url("home/step4/") ?>",
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: {
                    'experience_organization_name': experience_organization_name,
                    'experience_city': experience_city,
                    'experience_court': experience_court,
                    'experience_designation': experience_designation,
                    'experience_start_date': experience_start_date,
                    'experience_end_date': experience_end_date,
                    'update_verification_level': false,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success: function(data) {
                    var obj = data;
                    if (obj.status == true) {
                        window.location.href = "<?php echo base_url('edit_profile_lawyer'); ?>";
                    }
                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            })
        }
    });
    $('.fifth').click(function() {

        var consultation_day = [];
        var days = "";
        var days_value = [];
        var start_time_hour = [];
        var start_time_min = [];
        var end_time_hour = [];
        var end_time_min = [];
        var start_time = "";
        var end_time = "";

        var error = 0;

        $('#day_time_charges_err').html('');
        $('#office_charges_err').html('');
        $('#site_charges_err').html('');
        $('#telephone_charges_err').html('');
        $('#televideo_charges_err').html('');
        $('#email_charges_err').html('');
        $('#apperance_charges_err').html('');
        $('#haur_draft_charges_err').html('');


        var office_charges_checkbox_30_min = "";
        var office_charges_checkbox_1hour = "";
        var office_charges_30min = "";
        var office_charges_1hour = "";
        var site_charges_checkbox_30_min = "";
        var site_charges_checkbox_1hour = "";
        var site_charges_30min = "";
        var site_charges_1hour = "";


        var telvoice_charges_checkbox_30_min = "";
        var telvoice_charges_checkbox_1hour = "";
        var telvoice_charges_30min = "";
        var telvoice_charges_1hour = "";

        var telvideo_charges_checkbox_30_min = "";
        var telvideo_charges_checkbox_1hour = "";
        var telvideo_charges_30min = "";
        var telvideo_charges_1hour = "";
        var email_consultation_choice = "";
        var email_consultation_changes = "";
        var travel_consultation = "";

        $("input[name='consultation_day']:checked").each(function() {
            consultation_day.push($(this).val());
        });

        if (consultation_day.length <= 0) {

            $('#day_time_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select day');
            error = 1;
        }
        if (consultation_day.length > 0) {
            // console.log("length",consultation_day.length);
            for (let index = 0; index <= consultation_day.length; index++) {
                if (($(`#${consultation_day[index]}_from_hour`).val() == '' && $(`#${consultation_day[index]}_from_min`).val() == '') || ($(`#${consultation_day[index]}_to_hour`).val() == '' && $(`#${consultation_day[index]}_to_min`).val() == '')) {
                    $('#day_time_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select start time & end time for the selected days');
                    error = 1;
                } else {
                    days_value.push(consultation_day[index]);
                    start_time_hour.push($(`#${consultation_day[index]}_from_hour`).val());
                    start_time_min.push($(`#${consultation_day[index]}_from_min`).val());
                    end_time_hour.push($(`#${consultation_day[index]}_to_hour`).val());
                    end_time_min.push($(`#${consultation_day[index]}_to_min`).val());


                    start_time_hour = (start_time_hour == "" || start_time_hour == null) ? 0 : start_time_hour;
                    start_time_min = (start_time_min == "" || start_time_min == null) ? 0 : start_time_min;
                    end_time_hour = (end_time_hour == "" || end_time_hour == null) ? 0 : end_time_hour;
                    end_time_min = (end_time_min == "" || end_time_min == null) ? 0 : end_time_min;



                    start_time = parseInt(start_time_hour) * 60 + parseInt(start_time_min);
                    end_time = parseInt(end_time_hour) * 60 + parseInt(end_time_min);

                    if (start_time > end_time) {
                        $('#day_time_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>End time always greater then start time');
                        error = 1;
                    }


                }


            }


        }

        var office_consultation_type = $('input[name=office_consultation]:checked').val();
        var site_consultation_type = $('input[name=site_consultation]:checked').val();
        var telvoice_consultation_type = $('input[name=telvoice_consultation]:checked').val();
        var telvideo_consultation_type = $('input[name=telvideo_consultation]:checked').val();
        var email_consultation_type = $('input[name=email_consultation]:checked').val();
        var apperance_charges = $('#apperance_charges').val();
        var haur_draft_charges = $('#haur_draft_charges').val()







        if (office_consultation_type == "" || office_consultation_type == null) {
            $('#office_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office consultation charges');
            error = 1;
        }
        if (office_consultation_type == 0) {
            $('#office_charges_err').html('');
        } else {
            office_charges_checkbox_30_min = $('#office_charges_checkbox_30_min').val();
            office_charges_checkbox_1hour = $('#office_charges_checkbox_1_hour').val();
            office_charges_30min = $('#office_charges_30min').val();
            office_charges_1hour = $('#office_charges_1hour').val();

            if (($("#office_charges_checkbox_30_min").prop("checked") == false) && ($("#office_charges_checkbox_1_hour").prop("checked") == false)) {
                $('#office_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select office consultation charges mode');
                error = 1;
            } else {
                if ($("#office_charges_checkbox_30_min").prop("checked") == true) {
                    if (office_charges_30min == "" || office_charges_30min == null) {
                        $('#office_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office consultation charges');
                        error = 1;
                    }
                }
                if ($("#office_charges_checkbox_1_hour").prop("checked") == true) {
                    if (office_charges_1hour == "" || office_charges_1hour == null) {
                        $('#office_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office consultation charges');
                        error = 1;
                    }
                }


            }


        }
        if (site_consultation_type == "" || site_consultation_type == null) {
            $('#site_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter site consultation charges');
            error = 1;
        }
        if (site_consultation_type == 0) {
            $('#site_charges_err').html('');
        } else {
            site_charges_checkbox_30_min = $('#site_charges_checkbox_30min').val();
            site_charges_checkbox_1hour = $('#site_charges_checkbox_1hour').val();
            site_charges_30min = $('#site_charges_30min').val();
            site_charges_1hour = $('#site_charges_1hour').val();
            travel_consultation = $('input[name=travel_consultation]:checked').val();


            if (($("#site_charges_checkbox_30min").prop("checked") == false) && ($("#site_charges_checkbox_1hour").prop("checked") == false)) {
                $('#site_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select site consultation charges mode');
                error = 1;
            } else if (travel_consultation == "" || travel_consultation == null) {
                $('#site_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select site travel consultation charges');
                error = 1;
            } else {
                if ($("#site_charges_checkbox_30min").prop("checked") == true) {
                    if (site_charges_30min == "" || site_charges_30min == null) {
                        $('#site_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter site consultation charges');
                        error = 1;
                    }
                }
                if ($("#site_charges_checkbox_1hour").prop("checked") == true) {
                    if (site_charges_1hour == "" || site_charges_1hour == null) {
                        $('#site_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter site consultation charges');
                        error = 1;
                    }
                }


            }


        }

        if (telvoice_consultation_type == "" || telvoice_consultation_type == null) {
            $('#telephone_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter telephone voice charges');
            error = 1;
        }
        if (telvoice_consultation_type == 0) {
            $('#telephone_charges_err').html('');
        } else {
            telvoice_charges_checkbox_30_min = $('#telvoice_charge_checkbox_30min').val();
            telvoice_charges_checkbox_1hour = $('#telvoice_charge_checkbox_1hour').val();
            telvoice_charges_30min = $('#telvoice_charges_30min').val();
            telvoice_charges_1hour = $('#telvoice_charges_1hour').val();

            if (($("#telvoice_charge_checkbox_30min").prop("checked") == false) && ($("#telvoice_charge_checkbox_1hour").prop("checked") == false)) {
                $('#telephone_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select telephone consultation charges mode');
                error = 1;
            } else {
                if ($("#telvoice_charge_checkbox_30min").prop("checked") == true) {
                    if (telvoice_charges_30min == "" || telvoice_charges_30min == null) {
                        $('#telephone_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter telephone consultation charges');
                        error = 1;
                    }
                }
                if ($("#telvoice_charge_checkbox_1hour").prop("checked") == true) {
                    if (telvoice_charges_1hour == "" || telvoice_charges_1hour == null) {
                        $('#telephone_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter telephone consultation charges');
                        error = 1;
                    }
                }
            }


        }
        if (telvideo_consultation_type == "" || telvideo_consultation_type == null) {
            $('#televideo_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter telephone video charges');
            error = 1;
        }
        if (telvideo_consultation_type == 0) {
            $('#televideo_charges_err').html('');
        } else {
            telvideo_charges_checkbox_30_min = $('#telvideo_charge_checkbox_30min').val();
            telvideo_charges_checkbox_1hour = $('#telvideo_charge_checkbox_1hour').val();
            telvideo_charges_30min = $('#telvideo_charges_30min').val();
            telvideo_charges_1hour = $('#telvideo_charges_1hour').val();

            if (($("#telvideo_charge_checkbox_30min").prop("checked") == false) && ($("#telvideo_charge_checkbox_1hour").prop("checked") == false)) {
                $('#televideo_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select video conference consultation charges mode');
                error = 1;
            } else {
                if ($("#telvideo_charge_checkbox_30min").prop("checked") == true) {
                    if (telvideo_charges_30min == "" || telvideo_charges_30min == null) {
                        $('#televideo_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter video conference consultation charges');
                        error = 1;
                    }
                }
                if ($("#telvideo_charge_checkbox_1hour").prop("checked") == true) {
                    if (telvideo_charges_1hour == "" || telvideo_charges_1hour == null) {
                        $('#televideo_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter video conference consultation charges');
                        error = 1;
                    }
                }
            }


        }
        if (email_consultation_type == "" || email_consultation_type == null) {
            $('#email_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter email charges');
            error = 1;
        } else {
            email_consultation_choice = $('input[name=email_consultation]:checked').val();
            email_consultation_changes = $('#email_consultation_changes').val();

            if (email_consultation_choice == 1) {
                if (email_consultation_changes == "" || email_consultation_changes == null) {
                    $('#email_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter email charges detail');
                    error = 1;
                }
            }
        }

        if (apperance_charges.length == "" || apperance_charges == null) {
            $('#apperance_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please fill apperance charges filed');
            error = 1;
        }
        if (haur_draft_charges == "" || haur_draft_charges == null) {
            $('#haur_draft_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please fill haur draft charges filed');
            error = 1;
        }


        if (error == 1) {
            return false;
        } else {

            $.ajax({
                url: "<?php echo site_url("home/step5/") ?>",
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: {
                    'office_consultation_type': office_consultation_type,
                    'office_charges_checkbox_30_min': office_charges_checkbox_30_min,
                    'office_charges_checkbox_1hour': office_charges_checkbox_1hour,
                    'office_charges_30min': office_charges_30min,
                    'office_charges_1hour': office_charges_1hour,
                    'site_consultation_type': site_consultation_type,
                    'site_charges_checkbox_30_min': site_charges_checkbox_30_min,
                    'site_charges_checkbox_1hour': site_charges_checkbox_1hour,
                    'site_charges_30min': site_charges_30min,
                    'site_charges_1hour': site_charges_1hour,
                    'telvoice_consultation_type': telvoice_consultation_type,
                    'telvoice_charges_checkbox_30_min': telvoice_charges_checkbox_30_min,
                    'telvoice_charges_checkbox_1hour': telvoice_charges_checkbox_1hour,
                    'telvoice_charges_30min': telvoice_charges_30min,
                    'telvoice_charges_1hour': telvoice_charges_1hour,
                    'telvideo_consultation_type': telvideo_consultation_type,
                    'telvideo_charges_checkbox_30_min': telvideo_charges_checkbox_30_min,
                    'telvideo_charges_checkbox_1hour': telvideo_charges_checkbox_1hour,
                    'telvideo_charges_30min': telvideo_charges_30min,
                    'telvideo_charges_1hour': telvideo_charges_1hour,
                    'email_consultation_type': email_consultation_type,
                    'email_consultation_charges': email_consultation_changes,
                    'travel_consultation': travel_consultation,
                    'apperance_charges': apperance_charges,
                    'haur_draft_charges': haur_draft_charges,
                    'days_value': days_value,
                    'start_time_hour': start_time_hour,
                    'start_time_min': start_time_min,
                    'end_time_hour': end_time_hour,
                    'end_time_min': end_time_min,
                    'update_verification_level': false,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success: function(data) {
                    var obj = data;
                    if (obj.status == true) {
                        window.location.href = "<?php echo base_url('edit_profile_lawyer'); ?>";
                    }


                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            });
        }



    });

    $('.sixth').click(function() {
        var error = 0;

        $('#address1_name_err').html('');
        $('#address1_contact_no_err').html('');
        $('#address1_err').html('');
        $('#address2_err').html('');
        $('#address3_err').html('');
        $('#location_city_err').html('');
        $('#location_state_err').html('');
        $('#location_country_err').html('');
        $('#lat_err').html('');
        $('#lng_err').html('');
        $('#pincode_err').html('');

        $('#tmp_address1_name_err').html('');
        $('#tmp_address1_contact_no_err').html('');
        $('#tmp_address1_err').html('');
        $('#tmp_address2_err').html('');
        $('#tmp_address3_err').html('');
        $('#tmp_location_city_err').html('');
        $('#tmp_location_state_err').html('');
        $('#tmp_location_country_err').html('');
        $('#tmp_lat_err').html('');
        $('#tmp_lng_err').html('');
        $('#tmp_pincode_err').html('');

        $('#firm_address_1_err').html('');
        $('#firm_address_2_err').html('');
        $('#firm_address_3_err').html('');
        $('#firm_city_err').html('');
        $('#firm_state_err').html('');
        $('#firm_country_err').html('');
        $('#firm_lat_err').html('');
        $('#firm_lng_err').html('');
        $('#firm_pincode_err').html('');

        $("#agree_checkbox_err").html('');


        var address1_name = $('#address1_name').val();
        var address1_contact_no = $('#address1_contact_no').val();
        var address1 = $('#address1').val();
        var address2 = $('#address2').val()
        var address3 = $('#address3').val();
        var location_city = $('#location_city').val()
        var location_state = $('#location_state').val()
        var location_country = $('#location_country').val();
        var lat = $('#lat').val()
        var lng = $('#lng').val();
        var pincode = $('#pincode').val();

        var new_address = $("#new_address").val();

        var tmp_address1_name = $('#tmp_address1_name').val();
        var tmp_address1_contact_no = $('#tmp_address1_contact_no').val();
        var tmp_address1 = $('#tmp_address1').val();
        var tmp_address2 = $('#tmp_address2').val()
        var tmp_address3 = $('#tmp_address3').val();
        var tmp_location_city = $('#tmp_location_city').val()
        var tmp_location_state = $('#tmp_location_state').val()
        var tmp_location_country = $('#tmp_location_country').val();
        var tmp_lat = $('#tmp_lat').val()
        var tmp_lng = $('#tmp_lng').val();
        var tmp_pincode = $('#tmp_pincode').val();


        var firm_address_1 = "";
        var firm_address_2 = "";
        var firm_address_3 = "";
        var firm_city = "";
        var firm_state = "";
        var firm_country = "";
        var firm_lat = "";
        var firm_lng = "";
        var firm_pincode = "";

        if (address1_name == "" || address1_name == null) {
            $('#address1_name_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter name');
            error = 1;
        }
        if (address1_contact_no == "" || address1_contact_no == null) {
            $('#address1_contact_no_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office contact no');
            error = 1;
        }
        if (address1 == "" || address1 == null) {
            $('#address1_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office address one');
            error = 1;
        }
        if (address2 == "" || address2 == null) {
            $('#address2_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office address two');
            error = 1;
        }
        if (address3 == "" || address3 == null) {
            $('#address3_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office address three');
            error = 1;
        }
        if (location_city == "" || location_city == null) {
            $('#location_city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office city location ');
            error = 1;
        }
        if (location_state == "" || location_state == null) {
            $('#location_state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office state location');
            error = 1;
        }
        if (location_country == "" || location_country == null) {
            $('#location_country_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office country location');
            error = 1;
        }
        if (lat == "" || lat == null) {
            $('#lat_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office lat location');
            error = 1;
        }
        if (lng == "" || lng == null) {
            $('#lng_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office lng location');
            error = 1;
        }
        if (pincode == "" || pincode == null) {
            $('#pincode_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office pincode');
            error = 1;

        }
        if (new_address == "1") {


            if (tmp_address1_name == "" || tmp_address1_name == null) {
                $('#tmp_address1_name_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter name');
                error = 1;
            }
            if (tmp_address1_contact_no == "" || tmp_address1_contact_no == null) {
                $('#tmp_address1_contact_no_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office contact no');
                error = 1;
            }
            if (tmp_address1 == "" || tmp_address1 == null) {
                $('#tmp_address1_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office address one');
                error = 1;
            }
            if (tmp_address2 == "" || tmp_address2 == null) {
                $('#tmp_address2_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office address two');
                error = 1;
            }
            if (tmp_address3 == "" || tmp_address3 == null) {
                $('#tmp_address3_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office address three');
                error = 1;
            }
            if (tmp_location_city == "" || tmp_location_city == null) {
                $('#tmp_location_city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office city location ');
                error = 1;
            }
            if (tmp_location_state == "" || tmp_location_state == null) {
                $('#tmp_location_state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office state location');
                error = 1;
            }
            if (tmp_location_country == "" || tmp_location_country == null) {
                $('#tmp_location_country_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office country location');
                error = 1;
            }
            if (tmp_lat == "" || tmp_lat == null) {
                $('#tmp_lat_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office lat location');
                error = 1;
            }
            if (tmp_lng == "" || tmp_lng == null) {
                $('#tmp_lng_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office lng location');
                error = 1;
            }
            if (tmp_pincode == "" || tmp_pincode == null) {
                $('#tmp_pincode_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter office pincode');
                error = 1;

            }
        }

        firm_address_1 = $('#firm_address_1').val();
        firm_address_2 = $('#firm_address_2').val()
        firm_address_3 = $('#firm_address_3').val();
        firm_city = $('#firm_city').val()
        firm_state = $('#firm_state').val()
        firm_country = $('#firm_country').val();
        firm_lat = $('#firm_lat').val()
        firm_lng = $('#firm_lng').val();
        firm_pincode = $('#firm_pincode').val();

        var firm_address_exist = $("#firm_address_exist").val();

        if (firm_address_exist == 1) {

            if (firm_address_1 == "" || firm_address_1 == null) {
                // alert("hello");
                $('#firm_address_1_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm address one');
                error = 1;
            }
            if (firm_address_2 == "" || firm_address_2 == null) {
                $('#firm_address_2_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm address two');
                error = 1;
            }
            if (firm_address_3 == "" || firm_address_3 == null) {
                $('#firm_address_3_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm address three');
                error = 1;
            }
            if (firm_city == "" || firm_city == null) {
                $('#firm_city_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select firm city');
                error = 1;
            }
            if (firm_state == "" || firm_state == null) {
                $('#firm_state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select firm state');
                error = 1;
            }
            if (firm_country == "" || firm_country == null) {
                $('#firm_country_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm country');
                error = 1;
            }
            if (firm_lat == "" || firm_lat == null) {
                $('#firm_lat_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm latitude');
                error = 1;
            }
            if (firm_lng == "" || firm_lng == null) {
                $('#firm_lng_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm longitude');
                error = 1;
            }
            if (firm_pincode == "" || firm_pincode == null) {
                $('#firm_pincode_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please enter firm pincode');
                error = 1;
            }
        }

        if ($("#agree_checkbox").prop("checked") == false) {
            $('#agree_checkbox_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>This field is required');
            error = 1;
        }



        if (error == 1) {
            return false;
        } else {


            $.ajax({
                url: "<?php echo site_url("home/step6/") ?>",
                type: "POST",
                dataType: "json",
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: {
                    'address1_name': address1_name,
                    'address1_contact_no': address1_contact_no,
                    'address1': address1,
                    'address2': address2,
                    'address3': address3,
                    'location_city': location_city,
                    'location_state': location_state,
                    'location_country': location_country,
                    'lat': lat,
                    'lng': lng,
                    'pincode': pincode,
                    'tmp_address1_name': tmp_address1_name,
                    'tmp_address1_contact_no': tmp_address1_contact_no,
                    'tmp_address1': tmp_address1,
                    'tmp_address2': tmp_address2,
                    'tmp_address3': tmp_address3,
                    'tmp_location_city': tmp_location_city,
                    'tmp_location_state': tmp_location_state,
                    'tmp_location_country': tmp_location_country,
                    'tmp_lat': tmp_lat,
                    'tmp_lng': tmp_lng,
                    'tmp_pincode': tmp_pincode,
                    'firm_address_1': firm_address_1,
                    'firm_address_2': firm_address_2,
                    'firm_address_3': firm_address_3,
                    'firm_city': firm_city,
                    'firm_state': firm_state,
                    'firm_country': firm_country,
                    'firm_lat': firm_lat,
                    'firm_lng': firm_lng,
                    'firm_pincode': firm_pincode,
                    'update_verification_level': false,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
                },
                success: function(data) {
                  
                    var obj = data;
                    if (obj.status_code == true) {
                        window.location.href = "<?php echo base_url('edit_profile_lawyer'); ?>";
                    }
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }
    });

    //get state bar reg form upload method

    $('#councelregforms').change(function() {
        // var councelregform =  $("form#regForm").serializeArray();
        var councelregform = $('#councelregform').prop('files')[0];
        if (councelregform == "" || councelregform == null) {
            $('#counform_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please upload state Bar Registration ID');
            return false;
        } else {
            $('#court_err').html('');
        }

        // return false;

        var formData = new FormData();
        formData.append('councelregform', councelregform);
        formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');


        $.ajax({
            url: "<?php echo site_url("home/uploadstatebarform/") ?>",
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
                $('#notification-loaders').css("display", "none");
            },
            data: formData,
            success: function(data) {
                var obj = data;
                if (obj.status == 1) {
                    $('#state_bar_form').val(obj.message);
                    $('#counform_err').html('');
                }
            },
            error: function(error) {
                console.log(`Error ${error}`);
            }
        });


    });
    //get  bar association reg form upload method

    $('#barregforms').change(function() {
        var barregform = $('#barregform').prop('files')[0];

        if (barregform == "" || barregform == null) {
            barregform = null;
            $('#bar_association_form').val(barregform);

        }

        var formData = new FormData();
        formData.append('barregform', barregform);
        formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

        $.ajax({
            url: "<?php echo site_url("home/uploadbarassoform/") ?>",
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
                $('#notification-loaders').css("display", "none");
            },
            data: formData,
            success: function(data) {
                var obj = data;
                if (obj.status == 1) {
                    $('#bar_association_form').val(obj.message);
                }

            },
            error: function(error) {
                console.log(`Error ${error}`);
            }
        });


    });

</script>
<script>
   
    function showRegNoDiv(no) {
        if (no == 1) {
            $(".bar_reg").show();
        } else {
            $(".bar_reg").hide();
        }

    }

    function lawyerPractitioner(no) {
        if (no == 1) {
            $(".lawyer_practitioner_div").show();
        } else {
            $(".lawyer_practitioner_div").hide();
        }

    }

    function lawyerPractice(no) {
        if (no == 1) {
            $(".unpracticing").hide();
            $(".practicing").show();
        } else {
            $(".practicing").hide();
            $(".unpracticing").show();
        }
    }

    function unpracticing_working_change(no) {
        if (no == 2) {
            $(".organization_div").show();
        } else {
            $(".organization_div").hide();
        }
    }
    //practicing_lawyer();

    function marks_x_mode(no) {
        if (no == 0) {
            $("#x_percentage_div").show();
            $("#x_division_div").hide();
            $("#x_cgpa_div").hide();
        }
        if (no == 1) {
            $("#x_percentage_div").hide();
            $("#x_division_div").show();
            $("#x_cgpa_div").hide();
        }
        if (no == 2) {
            $("#x_percentage_div").hide();
            $("#x_division_div").hide();
            $("#x_cgpa_div").show();
        }

    }

    function marks_xii__mode(no) {
        if (no == 0) {
            $("#xii_percentage_div").show();
            $("#xii_division_div").hide();
            $("#xii_cgpa_div").hide();
        }
        if (no == 1) {
            $("#xii_percentage_div").hide();
            $("#xii_division_div").show();
            $("#xii_cgpa_div").hide();
        }
        if (no == 2) {
            $("#xii_percentage_div").hide();
            $("#xii_division_div").hide();
            $("#xii_cgpa_div").show();
        }

    }

    function office_consultation_div(no) {
        if (no == 1) {
            $(".office_consultation_checkbox_div").show();

        } else {
            $(".office_consultation_checkbox_div").hide();
        }
    }

    function site_consultation_div(no) {
        if (no == 1) {
            $(".site_consultation_checkbox_div").show();

        } else {
            $(".site_consultation_checkbox_div").hide();
        }
    }

    function travel_consultation_div(no) {
        if (no == 1) {
            $(".travel_consultation_charges").show();

        } else {
            $(".travel_consultation_charges").hide();
        }
    }

    function telvoice_consultation_div(no) {
        if (no == 1) {
            $(".telvoice_consultation_checkbox_div").show();

        } else {
            $(".telvoice_consultation_checkbox_div").hide();
        }
    }

    function telvideo_consultation_div(no) {
        if (no == 1) {
            $(".telvideo_consultation_checkbox_div").show();

        } else {
            $(".telvideo_consultation_checkbox_div").hide();
        }
    }

    function email_consultation_div(no) {
        if (no == 1) {
            $(".email_consultation_div").show();

        } else {
            $(".email_consultation_div").hide();
        }
    }

    function maxLengthCheck(object) {
        if (parseInt(object.value, 10) > parseInt(object.max, 10))
            object.value = object.max;
    }

    function isNumeric(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }

    function valid_registartion_no(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9a-zA-Z/]|\./;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        }
    }



    var max_bar_assocition_div = 0;

    $('#add_bar_association').click(function() {
        var bar_assocition = "";
        var x = 1;
        if (max_bar_assocition_div < x) {
            bar_assocition = `<div id="add_more_bar"><div class="form-group bar_reg">
                                 <label for="registration_no"  class="control-label">Name of Bar Association:*</label>
                                 <input maxlength="100" class="form-control" name="bar_registration_name[]"   id="bar_registration_name_${x}" placeholder="Enter Registration Name" type="text"  required=""> 
                                 <p class ="err" id="reg_bar_name_err_${x}"></p>
                           </div>
                           <div class="form-group bar_reg">
                                 <label for="registration_no" class="control-label">Bar Association Registration no:*</label>
                                 <input maxlength="100" class="form-control" name="registration_no[]"  min=1 oninput="validity.valid||(value='');" id="regno_${x}" placeholder="Enter Registration no" type="text"  required=""> 
                                 <p class ="err" id="reg_bar_no_err_${x}" onkeypress="return valid_registartion_no(event)"></p>
                           </div>
                           <div class="form-group bar_reg">
                                 <label for="councel_registration_no" class="control-label">Upload Bar Association Registration ID:*</label>
                                 <label  for="barregform_${x}" class="upload_image_stepper">
                                 <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                 </label>
                                 <input class="form-control"  onchange="readURL_bar_association(this);" name="barregform[]" min=1 oninput="validity.valid||(value='');"  id="barregform_${x}" placeholder="Upload Bar Association  Registration Form" type="file"  required="" style="display:none;">
                                 <img id="barregform_${x}_preview" class="preview_image" src="https://pixabay.com/illustrations/head-the-dummy-avatar-man-tie-659652/" >
                                 <p class ="err" id="reg_bar_formno_err_${x}"></p>
                           </div>
                           <div class="form-group bar_reg">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                    <a href="javascript:void(0);"  id="remove_bar_div" class="remove_button btn btn-info btn-xs"><span class="glyphicon glyphicon-minus"></span>remove </a>
                                    </div>
                                 </div>
                           </div></div>`;
            max_bar_assocition_div++;
        }
        $('.bar_asscociation_wrapper').append(bar_assocition);
        $('#add_bar_association').hide();
    });

    $(document).on('click', '#remove_bar_div', function(e) {
        e.preventDefault();
        $('#add_more_bar').remove(); //Remove field html
        $('#add_bar_association').show();
        max_bar_assocition_div--; //Decrement field counter
    });



    //check add days
    $('#consultation_day_all').on('change', function() {
        $('.chk').prop('checked', $(this).prop("checked"));
    });
    //un select days if any days un select
    $('.chk').change(function() { //".checkbox" change 
        if ($('.chk:checked').length == $('.chk').length) {
            $('#consultation_day_all').prop('checked', true);
        } else {
            $('#consultation_day_all').prop('checked', false);
        }
    });

    //copy all days time 
    $('#copy_all_days').click(function() {

        $('#day_time_charges_err').html('');
        if (!$('#consultation_day_mon').is(':checked')) {
            $('#day_time_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select day');

        } else {
            var start_day_hour = $('#monday_from_hour').val();
            var start_day_min = $('#monday_from_min').val();
            var end_day_hour = $('#monday_to_hour').val();
            var end_day_min = $('#monday_to_min').val();

            start_day_hour = (start_day_hour == "") ? 0 : start_day_hour;
            start_day_min = (start_day_min == "") ? 0 : start_day_min;
            end_day_hour = (end_day_hour == "") ? 0 : end_day_hour;
            end_day_min = (end_day_min == "") ? 0 : end_day_min;


            if ((start_day_hour == 0 && start_day_min == 0) || (end_day_hour == 0 && end_day_min == 0)) {
                $('#day_time_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select start time & end time of selected day');

            } else {
                var start_time = parseInt(start_day_hour) * 60 + parseInt(start_day_min);
                var end_time = parseInt(end_day_hour) * 60 + parseInt(end_day_min);
                if (start_time > end_time) {
                    $('#day_time_charges_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>End time always greater then start time');

                } else {

                    $(".start_day_hour").val(start_day_hour);
                    $(".start_day_min").val(start_day_min);
                    $(".end_day_hour").val(end_day_hour);
                    $(".end_day_min").val(end_day_min);

                }
            }
        }

    });

    function getCity(no) {
        var error = 0;
        var type = no;
        var final_list = "";
        var state_id = "";
        var city_id = "";
       
        if (type == 1) {
            state_id = $('#firm_state').val();
            city_id = $('#hidden_firm_city').val();
            final_list = $('#firm_city');
        }
        if (type == 0) {
            state_id = $('#location_state').val();
            city_id = $('#hidden_location_city').val();
            final_list = $('#location_city');
        }
        if (type == 2) {
            state_id = $('#tmp_location_state').val();
            city_id = $('#hidden_tmp_location_city').val();
            final_list = $('#tmp_location_city');
        }

        if (state_id == 0 || state_id == "") {
            $('#res_location_state_err').html('<span><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg></span>Please select state');
            error = 0;
        }
        if (error == 1) {
            return false;
        } else {
            var cities = [];
            var all_city = "";
            var formData = new FormData();
            formData.append('state_id', state_id);
            formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

            $.ajax({
                url: "<?php echo site_url("home/getcitybystate/") ?>",
                type: "POST",
                dataType: "json",
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function() {
                    $('#notification-loaders').fadeIn(100);
                },
                complete: function() {
                    $('#notification-loaders').css("display", "none");
                },
                data: formData,
                success: function(data) {
                    var obj = data;
                    for (let index = 0; index < obj.length; index++) {
                        cities += `<option value="${obj[index].id}" id="">${obj[index].name}</option>`;

                    }
                    final_list.html(cities);
                    final_list.val(city_id);
                },
                error: function(error) {
                    console.log(`Error ${error}`);
                }
            });

        }


    }
</script>






<script type="text/javascript">
    // Add Remove Court

    $('body').on('change', '#field_court', function() {
        var court_value = $(this).val();
        var court_field_count = $("#field_court :selected").length;
        console.log(court_field_count);
        if (court_value.indexOf('others') > -1) {
            var html = addRemoveCustomCourt(court_field_count);
            $('#inputWrapper').html(html);
        }
    });
    $('body').on('blur', '.add_court_field', function() {
        $(this).siblings('.field_court').val($(this).val());
        $('#field_court').append('<option value="' + $(this).val() + '" selected>' + $(this).val() + '</option>');
    });
    $('body').on('keyup', '.add_court_field', function() {
        $('#field_court').find('option[value="' + $(this).val() + '"]').remove();
    });

    function addRemoveCustomCourt(courtFieldCount, button = '') {
        var courtfield = '';
        var countrows = 0;
        if (courtFieldCount <= 6) {
            if ($('div#inputWrapper').has(".addrowcourt").length > 0) {
                countrows = $("#inputWrapper > .addrowcourt").length;
            }
            if (button != '' || countrows == 0) {
                courtfield += '<div class="form-group addrowcourt customCourtRow-' + courtFieldCount + '" data-field-id="' + courtFieldCount + '">';
            }
            courtfield += getdata(courtFieldCount, button, countrows);
            if (button != '' || countrows == 0) {
                courtfield += '</div>';
            }
        }
        return courtfield;
    }

    function getdata(courtFieldCount, button, countrows = 0) {
        var courtfield = '';
        var counted = 1;
        if (button == '' && countrows != 0) {
            $(".add_court_field").each(function() {
                // var courtcount = $(this).parents(".addrowcourt").data("field-id");
                courtfield += '<div class="form-group addrowcourt customCourtRow-' + courtFieldCount + '" data-field-id="' + courtFieldCount + '">';
                courtfield += '<label>';
                if (counted == 1) {
                    courtfield += 'Enter court name';
                }
                courtfield += '</label><div class="justify-flex"><input maxlength="100" type="text" name="add_court_field[]" class="col-sm-3 form-control add_court_field" placeholder="enter court name" value="' + $(this).val() + '"/><input type="hidden" name="field_court[]" class="col-sm-3 field_court form-control" placeholder="enter court name" value="' + $(this).val() + '"/>';
                if (courtFieldCount <= 5 && countrows == counted) {
                    addFieldCount = courtFieldCount + 1;
                    button = "1";
                    courtfield += '<div class="courtButtons"><a href="javascript:void(0);" class="addCourt btn btn-info btn-xs" onclick="addCourt(' + addFieldCount + ',' + button + ')"><span class="glyphicon glyphicon-plus"></span> Add </a>';
                }
                courtfield += '<a href="javascript:void(0);" class="removeCourt btn btn-info btn-xs" onclick="removeCourt(' + courtFieldCount + ')"><span class="glyphicon glyphicon-minus"></span> Remove </a></div></div>';
                courtfield += '</div>';
                courtFieldCount++;
                counted++;
            });
        } else {
            courtfield = '<label>';
            if (button == '') {
                courtfield += 'Enter court name';
            }
            courtfield += '</label><div class="justify-flex"><input maxlength="100" type="text" name="add_court_field[]" value="" class="col-sm-3 form-control add_court_field" placeholder="enter court name"/><input type="hidden" name="field_court[]" value="" class="col-sm-3 field_court form-control" placeholder="enter court name"/>';
            if (courtFieldCount <= 5) {
                addFieldCount = courtFieldCount + 1;
                button = "1";
                courtfield += '<div class="courtButtons"><a href="javascript:void(0);" class="addCourt btn btn-info btn-xs" onclick="addCourt(' + addFieldCount + ',' + button + ')"><span class="glyphicon glyphicon-plus"></span> Add </a>';
            }
            courtfield += '<a href="javascript:void(0);" class="removeCourt btn btn-info btn-xs" onclick="removeCourt(' + courtFieldCount + ')"><span class="glyphicon glyphicon-minus"></span> Remove </a></div></div>';
        }
        return courtfield;
    }

    function addCourt(courtFieldCount, button) {
        $('.courtButtons .addCourt').remove();
        var html = addRemoveCustomCourt(courtFieldCount, button);
        $('#inputWrapper').append(html);
    }

    function removeCourt(courtFieldCount) {
        var lastrowvalue = $('#inputWrapper').find('.addrowcourt').last().find('.add_court_field').val();
        var removerowvalue = $('#inputWrapper').find('.customCourtRow-' + courtFieldCount).find('.add_court_field').val();
        $('#inputWrapper').find('.customCourtRow-' + courtFieldCount).find('.add_court_field').val(lastrowvalue);
        $('#inputWrapper').find('.customCourtRow-' + courtFieldCount).find('.add_court_field').siblings('.field_court').val(lastrowvalue);
        $('#field_court').find('option[value="' + removerowvalue + '"]').remove();
        $('#inputWrapper').find('.addrowcourt').last().remove();
        var checkHtml = $('#inputWrapper').html();
        if (checkHtml == '') {
            $("#field_court").find('option[value="others"]').prop("selected", false);
        }
        $("#field_court").trigger('change');
    }

    // Add Remove Qualification

    // $('body').on('click','.add_qualification',function(){
    //    var clone_qualification = $('.professional_qualification_container').clone().insertAfter('.professional_qualification_container');
    //     $(clone_qualification).find('#num').attr('id','num'+n);
    // });
</script>

<?php $ct = 1; ?>
<script type="text/javascript">
    $(document).ready(function() {
        var maxField = 4; //Input fields increment limitation
        var addButton = $('.add_qualification'); //Add button selector
        var wrapper = $('.field_wrappersss'); //Input field wrapper

        var x = 1; //Initial field counter is 1

        //Once add button is clicked
        $(addButton).click(function() {
            //Check maximum number of input fields
            if (x < maxField) {
                var fieldHTML = `<div class="row"><div class="col-md-12"><div class="form-group"><label>Enter Degree/Course Name</label><input type="text" maxlength="100" name="degree[]" class="form-control" placeholder="Enter Degree/Course name"  /><p class ="err" id="degree_err_${x}"></p></div>
   <div class="form-group"><label>Enter College Name</label><input type="text" maxlength="100" name="college[]" class="form-control" placeholder="Enter College name"  /><p class ="err" id="college_err_${x}"></p></div>
   <div class="form-group"><label>Enter University Name</label><input type="text" maxlength="100" name="university[]" class="form-control" placeholder="enter university name"  /><p class ="err" id="university_err_${x}"></p></div>
   <div class="form-group end_date"><label>Start</label>
      <select class="form-control" name="yearstart[]" id="yearstart" required=""><option value="">Year of passing</option>
            <?php
            $year = date('Y');
            for ($i = $year; $i >= 1950; $i--) {
            ?>
            <option value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php
            }
            ?>
      </select>
   <p class ="err" id="yearstart_err_${x}"></p></div>
   <div class="form-group end_date"><label>End</label>
   <select class="form-control" name="yearend[]" id="yearend" required=""><option value="">Year of passing</option>
            <?php
            $year = date('Y');
            for ($i = $year; $i >= 1950; $i--) {
            ?>
            <option value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php
            }
            ?>
      </select>
   <p class ="err" id="yearend_err_${x}"></p></div>
   <div class="form-group"><label>Select Division</label><select name="division[]" class="form-control"><option value="0">Select Division</option><option value="4">Gold Medalist</option><option value="1">Ist</option><option value="2">2nd</option><option value="3">3rd</option></select><p class ="err" id="division_err_${x}"></p></div>


                        <div class="form-group">
                           <label class="control-label">Upload Marksheet/Degree:*</label>
                           <label for="marksheetform_${x}" class="upload_image_stepper">
                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                           </label>
                           <input class="form-control" id="marksheetform_${x}" onchange="readURL(this,'marksheet_preview_image','marksheetform_url');" name="marksheetform" min="1" oninput="validity.valid||(value='');" placeholder="Upload Marksheet" type="file" required="" style="display:none;">
                           <?php
                            $marksheet_preview_image = "preview.png";
                            if (!empty($core_profile['upload_marksheet_array'])) {
                                $marks = json_decode($core_profile['upload_marksheet_array']);
                                if (isset($marks[$ct])) {
                                    $marksheet_preview_image = $marks[$ct];
                                }
                            }
                            ?>
                           <input type="hidden" class="marksheetform_url <?= $ct ?>" name="marksheetform_url[]" value="<?php echo $marksheet_preview_image; ?>">
                           <img class="marksheet_preview_image preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $marksheet_preview_image; ?>">
                           <p class="err" id="marksheet_err_${x}"></p>
                        </div>

   <div class="col-md-12 text-center"><a href="javascript:void(${x});" class="remove_qial_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 20px;"><span class="glyphicon glyphicon-minus"></span> Remove </a></div>`;

                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if (x == maxField) {
                    $(this).hide();
                }
            }
        });

        //Once remove button is clicked
        $(wrapper).on('click', '.remove_qial_button', function(e) {
            e.preventDefault();
            //$(this).parent("div").remove(); //Remove field html

            $(this).parent("div").parent("div").parent(".row").remove();
            x--; //Decrement field counter
            if (x < maxField) {
                $('.add_qualification').show();
            }
        });
    });


    var message = localStorage.getItem('login_message');
    localStorage.removeItem("login_message");

    $('.profile-formwrap').find('.alert').remove();
    if (message) {
        $('.profile-formwrap').append('<div class="alert alert-success global_msg"><b><span class="glyphicon glyphicon-ok"></span></b>' + message + '</div>');
    }
</script>