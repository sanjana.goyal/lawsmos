<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

<style>
   .btn-group>.btn:first-child {
      margin-left: 0;
      width: 100%;
      margin-left: 0px;
   }

   .err {
      color: red;
   }

   .profile-form-start .form-group .check-inner-p input.form-control {
      width: calc(100% - 111px);
      margin-bottom: 0;
      width: 100%;
   }
</style>
<div class="profile-formwrap">
   <div class="container">
      <?php $this->load->view('sidebar/sidebar.php'); ?>
      <div class="con-inner-pro" style="    position: relative;
    left: 114px;
    width: 1092px;">
         <div class="form-stepper" style="display: none">
            <ul>
               <li class="step-active stepper" id="stepper_1">
                  <div>1</div>
               </li>
               <li class="stepper" id="stepper_2">
                  <div>2</div>
               </li>
               <li class="stepper" id="stepper_3">
                  <div>3</div>
               </li>
               <li class="stepper" id="stepper_4">
                  <div>4</div>
               </li>
               <li class="stepper" id="stepper_5">
                  <div>5</div>
               </li>
            </ul>
         </div>
         <div id="notification-loaders" style="display:none;">
            <div id="loading_spinner_notification" style="text-align:center;">
               <span class="glyphicon glyphicon-refresh" id="ajspinner_notification"></span>
            </div>
         </div>
         <form id="regForm" class="profile-form-start" enctype="multipart/form-data">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
            <div class="tab" style="display:none;">
               <h4>Edit Your Details:</h4>
               <div class="form-group">
                  <label for="practice_city" class=" control-label">Are you currently a student enrolled in a university/ college?:*</label>
                  <div class=" check-inner-pp">
                     <div class="check-inner-p">
                        <input type="radio" name="student_enrolled" value="1" id="student_enrolled_yes" <?php if ($core_profile['student_enrolled'] == "1") {
                                                                                                            echo 'checked';
                                                                                                         } ?>>
                        <label for="student_enrolled_yes">Yes</label>
                     </div>
                     <div class="check-inner-p">
                        <input type="radio" name="student_enrolled" value="2" id="student_enrolled_no" <?php if ($core_profile['student_enrolled'] == "2") {
                                                                                                            echo 'checked';
                                                                                                         } ?>>
                        <label for="student_enrolled_no">NO, I have finished my course.</label>
                     </div>
                  </div>
                  <p class="err" id="student_enrolled_err"></p>
               </div>
               <div class="form-group">
                  <label for="date_of_birth">Date of Birth:*</label>
                  <input class="form-control" type="date" id="date_of_birth" value="<?php echo $core_profile['date_of_birth']; ?>" placeholder="" name="date_of_birth">
                  <p class="err" id="date_of_birth_err"></p>
               </div>
               <p>The information filled by you below will be projected on your LAWSMOS RESUME and will go to the recruiters for the purpose of making hires. Make sure that the information is correct and truthful.</p>
               <div class="row">
                  <div class="col-md-12">
                     <div style="overflow:auto;" class="pro-nex-prv">
                        <button type="button" id="nextBtn" class="btn first">Update</button>
                     </div>
                  </div>
               </div>
            </div>

            <div class="tab tab-half" style="display:none;">
               <div class="field_wrappersssss">
                  <div class="row">
                     <h4 style="text-align:center;">Educational Details</h4>
                     <h4 style="text-align:center;">School & High School</h4>
                     <div class="col-md-6 col-sm-12">
                        <h4>10th</h4>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="degree">Name of School/institute</label>
                              <input type="text" name="school" id="school_10" class="form-control" placeholder="Enter School/Institute Name" value="<?php echo $core_profile['school_10']; ?>" />
                              <p class="err" id="school_10_err"></p>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="date">Name of Board/University,</label>
                              <input type="text" class="form-control" name="board_10" id="board_10" class="form-control" placeholder="Enter Board/University Name" value="<?php echo $core_profile['board_10']; ?>"/>
                              <p class="err" id="board_10_err"></p>
                           </div>
                           <div class="form-group">
                              <label for="date">Year of Passing</label>
                              <select class="form-control" name="year_pass_10" id="year_pass_10" required="">
                                 <option value="">Year of Passing</option>
                                 <?php
                                 $year = date('Y');
                                 for ($i = $year; $i >= 1950; $i--) {
                                 ?>
                                    <option value="<?php echo $i ?>"  <?php if($core_profile['year_pass_10']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                 <?php
                                 }
                                 ?>
                              </select>
                              <p class="err" id="year_pass_10_err"></p>
                           </div>
                           <div class="form-group">
                              <label for="date" style="display:block">Marks :</label>
                              <div class=" check-inner-pp">
                                 <div class="check-inner-p">
                                    <input type="radio" name="marks_x_radio" value="1" id="marks_x_radioper" onclick="marks_x_mode(0)">
                                    <label for="percentage">Percentage</label>
                                 </div>
                                 <div class="check-inner-p">
                                    <input type="radio" name="marks_x_radio" value="2" id="marks_x_radiodiv" onclick="marks_x_mode(1)">
                                    <label for="divison">Divison</label>
                                 </div>
                                 <div class="check-inner-p">
                                    <input type="radio" name="marks_x_radio" value="3" id="marks_x_radiocg" onclick="marks_x_mode(2)">
                                    <label for="cgpa">CGPA</label>
                                 </div>
                              </div>

                              <div class="form-group" id="x_percentage_div" style="display:none">
                                 <label for="university">Percentage</label>
                                 <input type="number" name="marks_x_percentage" class="form-control" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" min="1" max="100" id="marks_x_percentage" value="<?php echo $core_profile['marks_x_percentage']; ?>" placeholder="%">
                              </div>
                              <div class="form-group" id="x_division_div" style="display:none">
                                 <label for="university">Select Division</label>
                                 <select name="x_division[]" id="x_division" class="form-control">
                                    <option value="">Select Division</option>
                                    <option value="goldmedal" <?php if($core_profile['x_division']=='goldmedal'){echo 'selected';} ?>>Gold Medalist</option>
                                    <option value="fst" <?php if($core_profile['x_division']=='fst'){echo 'selected';} ?>>Ist</option>
                                    <option value="scnd" <?php if($core_profile['x_division']=='scnd'){echo 'selected';} ?>>2nd</option>
                                    <option value="3rd" <?php if($core_profile['x_division']=='3rd'){echo 'selected';} ?>>3rd</option>
                                 </select>

                              </div>
                              <div class="form-group" id="x_cgpa_div" style="display:none">
                                 <label for="university">CGPA</label>
                                 <input type="text"  value="<?php echo $core_profile['marks_x_cgpa']; ?>" name="marks_x_cgpa" class="form-control" id="marks_x_cgpa" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" min="1" max="10" placeholder="CGPA">
                              </div>
                              <p class="err" id="x_marks_err"></p>
                           </div>
                        </div>

                     </div>
                     <div class="col-md-6 col-sm-12">
                        <h4>12th</h4>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="degree">Name of School/ institute</label>
                              <input type="text" name="school" id="school_12" class="form-control" placeholder="Enter School/ Institute Name" value="<?php echo $core_profile['school_12']; ?>"/>
                              <p class="err" id="school_12_err" ></p>
                           </div>
                        </div>
                        <div class="col-md-12">
                           <div class="form-group">
                              <label for="date">Name of Board/ University,</label>
                              <input type="text" class="form-control" name="board_12" id="board_12" class="form-control" placeholder="Enter Board/ University Name" value="<?php echo $core_profile['board_12']; ?>"/>
                              <p class="err" id="board_12_err"></p>
                           </div>

                           <div class="form-group">
                              <label for="date">Year of Passing</label>
                              <select class="form-control" name="year_pass_12" id="year_pass_12" required="">
                                 <option value="">Year of Passing</option>
                                 <?php
                                 $year = date('Y');
                                 for ($i = $year; $i >= 1950; $i--) {
                                 ?>
                                    <option value="<?php echo $i ?>" <?php if($core_profile['year_pass_12']==$i){echo 'selected';} ?>><?php echo $i ?></option>
                                 <?php
                                 }
                                 ?>
                              </select>
                              <p class="err" id="year_pass_12_err"></p>
                           </div>
                           <div class="form-group">
                              <label for="date" style="display:block">Marks :</label>
                              <div class=" check-inner-pp">
                                 <div class="check-inner-p">
                                    <input type="radio" name="marks_xii_radio" value="1" id="marks_12_percentage" onclick="marks_xii__mode(0)">
                                    <label for="individual">Percentage</label>
                                 </div>
                                 <div class="check-inner-p">
                                    <input type="radio" name="marks_xii_radio" value="2" id="marks_12_division" onclick="marks_xii__mode(1)">
                                    <label for="firm">Divison</label>
                                 </div>
                                 <div class="check-inner-p">
                                    <input type="radio" name="marks_xii_radio" value="3" id="marks_12_cgpa" onclick="marks_xii__mode(2)">
                                    <label for="both">CGPA</label>
                                 </div>
                              </div>
                              <div class="form-group" id="xii_percentage_div" style="display:none">
                                 <label for="university">Percentage</label>
                                 <input type="number" name="marks_xii__percentage" class="form-control" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" min="1" max="100" id="marks_xii__percentage" placeholder="%" value="<?php echo $core_profile['marks_xii_percentage'] ?>">
                              </div>
                              <div class="form-group" id="xii_division_div" style="display:none">
                                 <label for="university">Select Division</label>
                                 <select name="xii_division[]" id="xii_division" class="form-control">
                                    <option value="">Select Division</option>
                                    <option value="goldmedal" <?php if($core_profile['xii_division']=='goldmedal'){echo 'selected';} ?>>Gold Medalist</option>
                                    <option value="fst" <?php if($core_profile['xii_division']=='fst'){echo 'selected';} ?>>Ist</option>
                                    <option value="scnd" <?php if($core_profile['xii_division']=='scnd'){echo 'selected';} ?>>2nd</option>
                                    <option value="3rd" <?php if($core_profile['xii_division']=='3rd'){echo 'selected';} ?>>3rd</option>
                                 </select>

                              </div>
                              <div class="form-group" id="xii_cgpa_div" style="display:none">
                                 <label for="university">CGPA</label>
                                 <input type="text" name="marks_xii__cgpa" class="form-control" onkeypress="return isNumeric(event)" oninput="maxLengthCheck(this)" id="marks_xii__cgpa" min="1" max="10" placeholder="CGPA" value="<?php echo $core_profile['marks_xii_cgpa'] ?>">
                              </div>


                              <p class="err" id="xii_marks_err"></p>
                           </div>


                        </div>
                     </div>
                  </div>
               </div>

               <div class="field_wrappersss">
                  <div class="row">
                     <h4 style="text-align:center;">College/ Degree details</h4>

                     <?php 
                    $degree_array = json_decode($core_profile['degree_array']);
                    $yearstart_array = json_decode($core_profile['yearstart_array']);
                    $yearend_array = json_decode($core_profile['yearend_array']);
                    $college_array = json_decode($core_profile['college_array']);
                    $university_array = json_decode($core_profile['university_array']);
                    $division_array = json_decode($core_profile['division_array']);
                    for($ii=0; $ii<count($degree_array);$ii++){

                   ?>

                     <div class="col-md-12">
                        <div class="form-group">
                           <label for="degree">Enter Degree/ Course Name</label>
                           <input type="text" name="degree[]" id="degree" class="form-control" placeholder="Enter Degree/ Course name" value="<?php echo($degree_array[$ii]); ?>" />
                           <p class="err" id="degree_err_0"></p>
                        </div>

                        <div class="form-group">
                           <label for="date">Enter College Name</label>
                           <input type="text" name="college[]" class="form-control" id="college" placeholder="Enter College name" value="<?php echo($college_array[$ii]); ?>" />
                           <p class="err" id="college_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="university">Enter University Name</label>
                           <input type="text" name="university[]" id="university" class="form-control" placeholder="enter university name" value="<?php echo($university_array[$ii]); ?>" />
                           <p class="err" id="university_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="date">Start Year</label>
                           <!-- <input type="text" class="form-control" name="yearstart[]" id="yearstart" class="form-control"  /> -->
                           <select class="form-control" name="yearstart[]" id="yearstart" required="">
                              <option value="">Start Year</option>
                              <?php
                              $year = date('Y');
                              for ($i = $year; $i >= 1950; $i--) {
                              ?>
                                 <option value="<?php echo $i ?>" <?php if($yearstart_array[$ii]==$i){echo 'selected';} ?>><?php echo $i ?></option>
                              <?php
                              }
                              ?>
                           </select>
                           <p class="err" id="yearstart_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="date">Year of passing</label>
                           <select class="form-control" name="yearend[]" id="yearend" required="">
                              <option value="">Year of passing</option>
                              <?php
                              $year = date('Y');
                              for ($i = $year; $i >= 1950; $i--) {
                              ?>
                                 <option value="<?php echo $i ?>" <?php if($yearend_array[$ii]==$i){echo 'selected';} ?>><?php echo $i ?></option>
                              <?php
                              }
                              ?>
                           </select>
                           <p class="err" id="yearend_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="university">Select Division</label>
                           <select name="division[]" id="division" class="form-control">
                              <option value="">Select Division</option>
                              <option value="goldmedal" <?php if($division_array[$ii]=='goldmedal'){echo 'selected';} ?>>Gold Medalist</option>
                              <option value="fst" <?php if($division_array[$ii]=='fst'){echo 'selected';} ?>>Ist</option>
                              <option value="scnd" <?php if($division_array[$ii]=='scnd'){echo 'selected';} ?>>2nd</option>
                              <option value="3rd" <?php if($division_array[$ii]=='3rd'){echo 'selected';} ?>>3rd</option>
                           </select>
                           <p class="err" id="division_err_0"></p>
                        </div>



                     </div>

                     <?php
                      }
                      ?>

                     


                     <div class="col-md-12">
                        <div class="form-group">
                           <a href="javascript:void(0);" class="add_qualification btn btn-info btn-xs"><span class="glyphicon glyphicon-plus"></span> Add More </a>
                        </div>
                         <!-- <div class="col-md-12 text-center"><a href="javascript:void(0);" class="remove_qial_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 20px;"><span class="glyphicon glyphicon-minus"></span> Remove </a></div> -->
                     </div>
                  </div>
               </div>

               <div style="overflow:auto;" class="pro-nex-prv">
                  <button style="display: none" type="button" id="prevBtn" class="btn" onclick="nextPrev(-1)">Previous</button>
                  <button type="button" id="nextBtn" class="btn second">Update</button>
               </div>
            </div>
            <?php
               $training_name_of_organization = json_decode($core_profile['training_name_of_organization']);
               $training_state = json_decode($core_profile['training_state']);
               $training_city = json_decode($core_profile['training_city']);
               $training_court = json_decode($core_profile['training_court']);
               $training_start_date = json_decode($core_profile['training_start_date']);
               $training_end_date = json_decode($core_profile['training_end_date']);
               $role_learning = json_decode($core_profile['role_learning']);
               $internship_validation = json_decode($core_profile['internship_validation']);
               $contact_person_name = json_decode($core_profile['contact_person_name']);
               $contact_person_designation = json_decode($core_profile['contact_person_designation']);
               $contact_person_phone_no = json_decode($core_profile['contact_person_phone_no']);
               $contact_person_email_address = json_decode($core_profile['contact_person_email_address']);

               //research section
               $research_title = json_decode($core_profile['research_title']);
               $research_journal_name = json_decode($core_profile['research_journal_name']);
               $research_issn_number = json_decode($core_profile['research_issn_number']);
               $research_share_link = json_decode($core_profile['research_share_link']);


               //training section
               $course_college = json_decode($core_profile['course_college']);
               $course_program_name = json_decode($core_profile['course_program_name']);
               $course_location = json_decode($core_profile['course_location']);
               $course_start_date = json_decode($core_profile['course_start_date']);
                 $course_end_date = json_decode($core_profile['course_end_date']);
               $course_certificate_url = json_decode($core_profile['course_certificate_url']);
               $course_validation = json_decode($core_profile['course_validation']);
               $course_contact_person_name = json_decode($core_profile['course_contact_person_name']);
                 $course_contact_person_designation = json_decode($core_profile['course_contact_person_designation']);
               $course_contact_person_email = json_decode($core_profile['course_contact_person_email']);
             

              //project section
              $awards = json_decode($core_profile['awards']);

            ?>
            <div class="tab" style="display:none;">
               <h4>TRAINING AND PROFESSIONAL EXPERIENCE</h4>
               <b>
                  <p>Note: Professional and training experience increase the chances of getting hired.</p>
               </b>
               <div class="intern-form">
                  <div class="wrapper_internship">
                     <div class="row">
                        <?php for ($k=0; $k < count($training_name_of_organization) ; $k++) { 
                          ?>

                        <div class="form-group">
                           <label for="training_name_of_organization">Name of organization:*</label>
                           <input type="text" name="training_name_of_organization[]" id="training_name_of_organization_0" onkeypress="search_organization(this)" class="form-control" placeholder="Enter organization name" value="<?php echo $training_name_of_organization[$k]; ?>" />
                           <p class="err" id="training_name_of_organization_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_state"><?php echo lang("ctn_394") ?>:*</label>
                           <select class="form-control" name="training_state[]" id="training_state_0" onchange="getTrainingCity(0)" onload="getTrainingCity(0)">
                              <option value="">Select State</option>
                              <?php foreach ($states as $all_states) : ?>
                                 <option value="<?php echo $all_states->id ?>" <?php if($training_state[$k]== $all_states->id ){
                                    echo "selected";
                                 } ?>><?php echo $all_states->name; ?></option>
                              <?php endforeach; ?>
                           </select>
                           <p class="err" id="training_state_err_0"></p>
                        </div>

                        <div class="form-group">
                           <label for="training_city"><?php echo lang("ctn_393") ?>:*</label>
                           <select class="form-control" name="training_city[]" id="training_city_0">
                              <option value="">Select City</option>
                           </select>
                           <p class="err" id="training_city_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_court">Name of courts attended:*</label>
                           <select name="training_court[]" id="training_court" class="form-control" title="Select Court Name" placeholder="select court name">
                              <option value="">Select Court</option>
                              <?php foreach ($courts->result() as $court) {
                              ?>
                                 <option value="<?php echo $court->id; ?>" <?php if($training_court[$k]== $court->id ){
                                    echo "selected";
                                 } ?>><?php echo $court->court_name; ?></option>
                              <?php } ?>
                           </select>
                           <p class="err" id="training_court_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_start_date">Start Date:*</label>
                           <input type="date" class="form-control" id="training_start_date" name="training_start_date[]" value="<?php echo $training_start_date[$k] ?>" />
                           <p class="err" id="training_start_date_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_end_date">End Date :*</label>
                           <input type="date" class="form-control" id="training_end_date" name="training_end_date[]"  value="<?php echo $training_end_date[$k] ?>" />
                           <p class="err" id="training_end_date_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="role_learning">Roles & Learnings (Briefly Mention Key Responsibilities, Use Words That Demonstrate Action Like Created, Drafted, Learnt, Assessed, Etc.) :*</label>
                           <input type="text" class="form-control" id="role_learning" name="role_learning[]" value="<?php echo $role_learning[$k]; ?>" />
                           <p class="err" id="role_learning_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="internship_validation">internship Validation :*</label>
                           <input type="checkbox" class="form-control" id="internship_validation" name="internship_validation[]" style="width: 16px;margin-bottom: 5px;margin-right: 10px;" <?php if ($internship_validation[$k]=='on') {
                              echo 'checked';                              # code...
                           } ?>/>
                           <span>Yes</span>
                           <p class="err" id="internship_validation_err_0"></p>
                        </div>
                        <div class="internship_validation">
                           <h5><b>Validate my internship with</b></h5>
                           <p>Internships validated by organizations add credibility and increases your LAWSMOS score by 500 points - the better you’re rank on the leadership board, the better are your chances of getting hired. Know more about lawsmos leadership board.</p>
                           <p>Contact Details of Authorised Person/ Organization.</p>
                           <div class="form-group">
                              <label for="contact_person_name">Contact Person's Name :</label>
                              <input type="text" class="form-control" id="contact_person_name" name="contact_person_name[]" value="<?php echo $contact_person_name[$k]; ?>">
                              <p class="err" id="contact_person_name_err_0"></p>
                           </div>
                           <div class="form-group">
                              <label for="contact_person_designation">Designation :</label>
                              <input type="text" class="form-control" id="contact_person_designation" name="contact_person_designation[]" value="<?php echo $contact_person_designation[$k]; ?>">
                              <p class="err" id="contact_person_designation_err_0"></p>
                           </div>
                           <div class="form-group">
                              <label for="contact_person_phone_no">Phone no. :</label>
                              <input type="text" class="form-control" id="contact_person_phone_no" name="contact_person_phone_no[]" value="<?php echo $contact_person_phone_no[$k]; ?>">
                              <p class="err" id="contact_person_phone_no_err_0"></p>
                           </div>
                           <div class="form-group">
                              <label for="contact_person_email_address">Email Address :</label>
                              <input type="text" class="form-control" id="contact_person_email_address" name="contact_person_email_address[]" value="<?php echo $contact_person_email_address[$k]; ?>">
                              <p class="err" id="contact_person_email_address_err_0"></p>
                           </div>
                           </div>
                     </div>
                  </div>
                 
                             <?php
                        } ?>
                           
                  <div class="text-center">
                     <button type="button" class="btn btn-primary add_internship" id="add_internship">Add another Internship</button>
                  </div>
               </div>




               <div class="research-papers">
                  <div class="wrapper_research">
                      <?php for ($p=0; $p < count($research_title) ; $p++) { 
                          ?>
                     <div class="row">
                        <h4>RESEARCH PAPERS</h4>
                        <div class="form-group">
                           <label for="research_title">Title :</label>
                           <input type="text" class="form-control" id="research_title" name="research_title[]" value="<?php echo $research_title[$p] ?>">
                           <p class="err" id="research_title_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="research_journal_name">Journal Name :</label>
                           <input type="text" class="form-control" id="research_journal_name" name="research_journal_name[]" value="<?php echo $research_journal_name[$p] ?>">
                           <p class="err" id="research_journal_name_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="research_issn_number">ISSN Numbar(If applicable) :</label>
                           <input type="text" class="form-control" id="research_issn_number" name="research_issn_number[]" value="<?php echo $research_issn_number[$p] ?>">
                           <p class="err" id="research_issn_number_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="research_share_link">Share Link :</label>
                           <input type="text" class="form-control" id="research_share_link" name="research_share_link[]" value="<?php echo $research_share_link[$p] ?>">
                           <p class="err" id="research_share_link_err_0"></p>
                        </div>

                     </div>
                     <?php
                        }
                     ?>
                  </div>
                  <div class="text-center">
                     <button type="button" class="btn btn-primary add_research" id="add_research">Add another Research</button>
                  </div>
               </div>
               <div class="course-section">
                  <div class="wrapper_course">
                      <?php  for ($j=0; $j < count($course_college) ; $j++) { 
                          ?>
                     <div class="row">
                        <h4>DIPLOMA/ TRAINING/ COURSES</h4>
                        <div class="form-group">
                           <label for="course_college">University/ College/ Institute/ Org :</label>
                           <input type="text" name="course_college[]" onkeypress="search_university(this)" class="form-control" id="course_college_0" placeholder="Enter College name" value="<?php echo $course_college[$j]; ?>" />
                           <p class="err" id="course_college_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_program_name">Program Name :</label>
                           <input type="text" name="course_program_name[]" id="course_program_name" class="form-control" placeholder="Enter program name" value="<?php echo $course_program_name[$j]; ?>" />
                           <p class="err" id="course_program_name_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_location">Location :</label>
                           <input type="text" name="course_location[]" id="course_location" class="form-control" placeholder="enter location name" value="<?php echo $course_location[$j]; ?>"/>
                           <p class="err" id="course_location_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_start_date">Start Date:</label>
                           <input type="date" class="form-control" id="course_start_date" name="course_start_date[]" value="<?php if($course_start_date) echo $course_start_date[$j]; ?>"/>
                           <p class="err" id="course_start_date_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_end_date">End Date :</label>
                           <input type="date" class="form-control" id="course_end_date" name="course_end_date[]" value="<?php echo $course_end_date[$j]; ?>"/>
                           <p class="err" id="course_end_date_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label class="control-label">Upload Certificate :</label>
                           <label for="course_certificate_0" class="upload_image_stepper">
                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                           </label>
                           <input class="form-control" id="course_certificate_0" onchange="readURL(this,'course_certificate_preview_image','course_certificate_url');" name="course_certificate" min="1" oninput="validity.valid||(value='');" placeholder="Upload Marksheet" type="file" required="" style="display:none;">
                           <?php
                           if($course_certificate_url)
                           {
                              $course_certificate_preview_image = $course_certificate_url[$j];
                           }else{
                               $course_certificate_preview_image = "preview.png";
                           }
                          
                           ?>
                           <input type="hidden" class="course_certificate_url" name="course_certificate_url[]" value="<?php echo $course_certificate_preview_image; ?>">
                           <img class="course_certificate_preview_image preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $course_certificate_preview_image; ?>">
                           <p class="err" id="course_certificate_url_err_0"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_validation">Request Validation :</label>
                           <input type="checkbox" class="form-control" id="course_validation" name="course_validation[]" style="width: 16px;margin-bottom: 5px;margin-right: 10px;" <?php if($course_validation[$j]=='on') echo 'checked' ?>/>
                           <span>Yes</span>
                           <p class="err" id="role_learning_err_0"></p>
                        </div>
                        <div class="course_validation">
                           <div class="form-group">
                              <label for="course_contact_person_name">Contact Person's Name :</label>
                              <input type="text" class="form-control" id="course_contact_person_name" name="course_contact_person_name[]" value="<?php echo $course_contact_person_name[$j]; ?>">
                              <p class="err" id="course_contact_person_name_err_0"></p>
                           </div>
                           <div class="form-group">
                              <label for="course_contact_person_designation">Contact Person's Designation :</label>
                              <input type="text" class="form-control" id="course_contact_person_designation" name="course_contact_person_designation[]" value="<?php echo $course_contact_person_designation[$j]; ?>">
                              <p class="err" id="course_contact_person_designation_err_0"></p>
                           </div>
                           <div class="form-group">
                              <label for="course_contact_person_email">Email address of training organization :</label>
                              <input type="text" class="form-control" id="course_contact_person_email" name="course_contact_person_email[]" value="<?php echo $course_contact_person_email[$j]; ?>">
                              <p class="err" id="course_contact_person_email_err_0"></p>
                           </div>
                        </div>
                     </div>
                     <?php
                        }
                     ?>
                     <div class="text-center">
                        <button type="button" class="btn btn-primary add_course" id="add_course">Add another Course/Training</button>
                     </div>
                  </div>
               </div>
               <div class="projects">
                  <h4>ACADEMIC/ PERSONAL PROJECTS/ STARTUPs</h4>
                  <div class="form-group">
                     <label for="project_title">Project Title:</label>
                     <input type="text" class="form-control" id="project_title" name="project_title" value="<?php echo $core_profile['project_title']; ?>" />
                     <p class="err" id="project_title_err"></p>
                  </div>
                  <div class="form-group">
                     <label for="project_start_month">Start Month</label>
                     <select name="project_start_month" id="project_start_month" class="form-control">
                        <option value="">Select Month</option>
                        <option value="January" <?php if($core_profile['project_start_month']=='January') echo 'selected' ?>>January</option>
                        <option value="February" <?php if($core_profile['project_start_month']=='February') echo 'selected' ?>>February</option>
                        <option value="March" <?php if($core_profile['project_start_month']=='March') echo 'selected' ?>>March</option>
                        <option value="April" <?php if($core_profile['project_start_month']=='April') echo 'selected' ?>>April</option>
                        <option value="May" <?php if($core_profile['project_start_month']=='May') echo 'selected' ?>>May</option>
                        <option value="June" <?php if($core_profile['project_start_month']=='June') echo 'selected' ?>>June</option>
                        <option value="July" <?php if($core_profile['project_start_month']=='July') echo 'selected' ?>>July</option>
                        <option value="August" <?php if($core_profile['project_start_month']=='August') echo 'selected' ?>>August</option>
                        <option value="September" <?php if($core_profile['project_start_month']=='September') echo 'selected' ?>>September</option>
                        <option value="October" <?php if($core_profile['project_start_month']=='October') echo 'selected' ?>>October</option>
                        <option value="November" <?php if($core_profile['project_start_month']=='November') echo 'selected' ?>>November</option>
                        <option value="December" <?php if($core_profile['project_start_month']=='December') echo 'selected' ?>>December</option>
                     </select>
                     <p class="err" id="project_start_month_err"></p>
                  </div>
                  <div class="form-group">
                     <label for="project_end_month">End Month</label>
                     <select name="project_end_month" id="project_end_month" class="form-control">
                        <option value="">Select Month</option>
                        <option value="January" <?php if($core_profile['project_end_month']=='January') echo 'selected' ?>>January</option>
                        <option value="February" <?php if($core_profile['project_end_month']=='February') echo 'selected' ?>>February</option>
                        <option value="March" <?php if($core_profile['project_end_month']=='March') echo 'selected' ?>>March</option>
                        <option value="April" <?php if($core_profile['project_end_month']=='April') echo 'selected' ?>>April</option>
                        <option value="May" <?php if($core_profile['project_end_month']=='May') echo 'selected' ?>>May</option>
                        <option value="June" <?php if($core_profile['project_end_month']=='June') echo 'selected' ?>>June</option>
                        <option value="July" <?php if($core_profile['project_end_month']=='July') echo 'selected' ?>>July</option>
                        <option value="August" <?php if($core_profile['project_end_month']=='August') echo 'selected' ?>>August</option>
                        <option value="September" <?php if($core_profile['project_end_month']=='September') echo 'selected' ?>>September</option>
                        <option value="October" <?php if($core_profile['project_end_month']=='October') echo 'selected' ?>>October</option>
                        <option value="November" <?php if($core_profile['project_end_month']=='November') echo 'selected' ?>>November</option>
                        <option value="December" <?php if($core_profile['project_end_month']=='December') echo 'selected' ?>>December</option>
                     </select>
                     <p class="err" id="project_end_month_err"></p>
                  </div>
                  <div class="form-group">
                     <label for="project_description">Project Description:</label>
                     <textarea name="project_description" id="project_description" class="form-control" style="width: 56%;"><?php echo $core_profile['project_description']; ?></textarea>
                     <!-- <input type="text" class="form-control" id="project_description" name="project_description" /> -->
                     <p class="err" id="project_description_err"></p>
                  </div>
                  <div class="form-group">
                     <label for="project_link">Project Link:</label>
                     <input type="text" class="form-control" id="project_link" name="project_link" value="<?php echo $core_profile['project_link']; ?>"/>
                     <p class="err" id="project_link_err"></p>
                  </div>
               </div>
               <div class="hobbies_section">
                  <h4>HOBBIES</h4>
                  <div class="form-group">
                     <label for="hobbies">Hobbies :</label>
                     <input type="text" class="form-control" id="hobbies" name="hobbies" value="<?php echo $core_profile['hobbies'] ?>">
                     <p class="err" id="hobbies_err"></p>
                  </div>
               </div>
               <div class="awards_section">
                  <div class="wrapper_award">
                  <h4>EXTRA-CURRICULAR AWARDS/ ACCOMPLISHMENTS</h4>
                  <?php 
                    $awards = json_decode($core_profile['awards']);
                    if(!empty($awards)){


                    for ($m=0; $m < count($awards); $m++) { 
                       ?>
                        <div class="form-group">
                        <label for="awards">Extra-Curricular Awards/ Accomplishments :</label>
                        <input type="text" class="form-control" id="awards" name="awards[]" value="<?php echo $awards[$m]; ?>">
                         <?php if ($m>0) { ?>
                            <a href="javascript:void(<?php echo $m; ?>);" class="remove_award_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 9px;margin-left: 5px;"><span class="glyphicon glyphicon-minus"></span> Remove </a>
                       <?php } ?>
                        <p class="err" id="awards_err_0"></p>
                     </div>
                       <?php
                    }
                 }else{
                  ?>

                     <div class="form-group">
                        <label for="awards">Extra-Curricular Awards/ Accomplishments :</label>
                        <input type="text" class="form-control" id="awards" name="awards[]">
                       
                        <p class="err" id="awards_err_0"></p>
                     </div>
                
                  <?php
                 }
                    ?>
                    </div>
                  <div class="text-center">
                     <button type="button" class="btn btn-primary add_award" id="add_award">Add another</button>
                  </div><br>
               </div>
               <div class="skills_section">
                  <h4>Skills</h4>
                  <div class="form-group">
                     <label for="skills">Skills :</label>
                     <input type="text" class="form-control" id="skills" name="skills" placeholder="Enter skills" value="<?php echo $core_profile['skills'] ?>">
                     <p class="err" id="skills_err"></p>
                  </div>
               </div>
               <div class="academic_awards_section">
                  <h4>Academic Awards/ Accomplishments</h4>
                  <div class="form-group">
                     <label for="academic_awards">Academic Awards/ Accomplishments :</label>
                     <input type="text" class="form-control" id="academic_awards" name="academic_awards" value="<?php echo $core_profile['academic_awards'] ?>">
                     <p class="err" id="academic_awards_err"></p>
                  </div>
               </div>

               <div class="row">
                  <div class="col-md-12">
                     <div style="overflow:auto;" class="pro-nex-prv">
                        <button style="display: none" type="button" id="prevBtn" class="btn" onclick="nextPrev(-1)">Previous</button>
                        <button type="button" id="nextBtn" class="btn third">Update</button>
                     </div>
                  </div>
               </div>
            </div>


            <div class="tab" style="display:none;">
               <h4>LAW FIELD INTERESTS:</h4>
               <div class="form-group">
                  <label for="fields_of_interest">Fields of interest:*</label>
                  <select class="form-control selectpicker" multiple title="Select fields of interest" data-max-options="6" name="fields_of_interest[]" id="fields_of_interest" required="">
                     <?php
                     $fields_of_interest = json_decode($core_profile['fields_of_interest']);
                     $career_interest = json_decode($core_profile['career_interest']);
                     foreach ($catagories as $catagory) {
                     ?>
                        <option value="<?php echo $catagory['cid']; ?>" <?php if (in_array($catagory['cid'], $fields_of_interest)) {
                           echo "selected";
                        } ?>><?php echo $catagory['c_name'] ?></option>
                     <?php
                     }
                     ?>
                  </select>
                  <p class="err" id="fields_of_interest_err"></p>
               </div>
               <div class="form-group">
                  <label for="career_interest">Career Interests</label>
                  <select class="form-control selectpicker" multiple title="Select Career interest" data-max-options="3" name="career_interest[]" id="career_interest" required="">
                     <option value="">Select Career Interest</option>
                     <option value="1" <?php if (in_array(1, $career_interest)) {
                           echo "selected";
                        } ?>>Independent Lawyer</option>
                     <option value="2" <?php if (in_array(2, $career_interest)) {
                           echo "selected";
                        } ?>>Work with a law firm(Business Law)</option>
                     <option value="3" <?php if (in_array(3, $career_interest)) {
                           echo "selected";
                        } ?>>Work with a law firm(Litigation)</option>
                     <option value="4" <?php if (in_array(4, $career_interest)) {
                           echo "selected";
                        } ?>>Work with a Corporate(In-House Counsel)</option>
                     <option value="5" <?php if (in_array(5, $career_interest)) {
                           echo "selected";
                        } ?>>Judicial Services</option>
                     <option value="6" <?php if (in_array(6, $career_interest)) {
                           echo "selected";
                        } ?>>Academics</option>
                     <option value="7" <?php if (in_array(7, $career_interest)) {
                           echo "selected";
                        } ?>>Other</option>
                  </select>
                  <p class="err" id="career_interest_err"></p>
               </div>
               <div class="col-md-12">
                  <div style="overflow:auto;" class="pro-nex-prv">
                     <button style="display: none" type="button" id="prevBtn" class="btn" onclick="nextPrev(-1)">Previous</button>
                     <button type="button" id="nextBtn" class="btn fourth">Update</button>
                  </div>
               </div>
            </div>
            <div class="tab last-tab-preview" style="display:none;">
               <h4>Almost there. Complete your profile!</h4>
               <p class="sec-top-head">Complete Your Profile and earn 3000 bonus points which can be utilized on Lawsmos for:</p>
               <p> - Projection in searches in your city. <a href="javascript:void(0)">Know more</a></p>
               <p> - Featuring on your city home page. <a href="javascript:void(0)">Know more</a></p>
               <p> - Pulling potential client leads. <a href="javascript:void(0)">Know more</a></p>
               <div class="form-group">
                  <label class="control-label">Profile_picture (TIP: A picture makes a first impression on the client, make sure that the profile picture is clear and represents you as a professional. Try to keep it formal.):*</label>
                  <label for="profile_picture" class="upload_image_stepper">
                     <i class="fa fa-plus-circle" aria-hidden="true"></i>
                  </label>
                  <input class="form-control" onchange="readURL_profile_pic(this);" name="profile_picture" min=1 oninput="validity.valid||(value='');" id="profile_picture" placeholder="Upload State Bar Registration ID" type="file" required="" style="display:none;">
                  <input type="hidden" name="profile_picture_url" id="profile_picture_url" value="">
                  <?php
                  $preview_image = "preview.png";
                  ?>
                  <img id="preview_image_profile" class="preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $core_profile['profile_picture']; ?>">
                  <p class="err" id="profile_picture_err"></p>
               </div>
               <div class="form-group">
                  <label class="control-label">Cover Photo:*</label>
                  <label for="cover_photo" class="upload_image_stepper">
                     <i class="fa fa-plus-circle" aria-hidden="true"></i>
                  </label>
                  <input class="form-control" onchange="readURL_cover_pic(this);" name="cover_photo" min=1 oninput="validity.valid||(value='');" id="cover_photo" placeholder="Upload State Bar Registration ID" type="file" required="" style="display:none;">
                  <input type="hidden" name="cover_photo_url" id="cover_photo_url" value="<?php echo $core_profile['cover_photo']; ?>">
                  <?php
                  $preview_image = "preview.png";
                  ?>
                  <img id="preview_image_cover" class="preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $core_profile['cover_photo']; ?>">
                  <p class="err" id="cover_photo_err"></p>
               </div>

               <p class="err" id="agree_checkbox_err" style="margin-left:0%;"></p>
               <div class="col-md-12">
                  <div style="overflow:auto;" class="pro-nex-prv">
                     <button style="display: none" type="button" id="prevBtn" class="btn" onclick="nextPrev(-1)">Previous</button>
                     <button type="button" id="nextBtn" class="btn fifth">Update</button>
                  </div>
               </div>
            </div>
            <!-- Circles which indicates the steps of the form: -->
            <div style="text-align:center;margin-top:40px;">
               <span class="step"></span>
               <span class="step"></span>
               <span class="step"></span>
               <span class="step"></span>
               <span class="step"></span>

            </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(document).ready(function() {
     


       var error = 0;
      var type = no;
      var final_list = "";
      var state_id = "";

      state_id = $('#training_state_0').val()
      final_list = $('#training_city_0');


      if (error == 1) {
         return false;
      } else {
         var cities = [];
         var all_city = "";
         var formData = new FormData();
         formData.append('state_id', state_id);
         formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

         $.ajax({
            url: "<?php echo site_url("home/getcitybystate/") ?>",
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
               $('#notification-loaders').css("display", "none");
            },
            data: formData,
            success: function(data) {
               var obj = data;
               for (let index = 0; index < obj.length; index++) {
                  cities += `<option value="${obj[index].id}" id="">${obj[index].name}</option>`;

               }
               final_list.html(cities);
            },
            error: function(error) {
               console.log(`Error ${error}`);
            }
         });

      }





      var maxField = 4; //Input fields increment limitation
      var addButton_qualification = $('.add_qualification'); //Add button selector
      var field_wrappersss = $('.field_wrappersss'); //Input field wrapper


      var x_add_qualification = 1; //Initial field counter is 1

      //Once add button is clicked
      $(addButton_qualification).click(function() {
         //Check maximum number of input fields
         if (x_add_qualification < maxField) {
            var fieldHTML = `<div class="row"><div class="col-md-12"><div class="form-group"><label>Enter Degree/Course Name</label><input type="text" name="degree[]" class="form-control" placeholder="Enter Degree/Course name"  /><p class ="err" id="degree_err_${x_add_qualification}"></p></div>
   <div class="form-group"><label>Enter College Name</label><input type="text" name="college[]" class="form-control" placeholder="Enter College name"  /><p class ="err" id="college_err_${x_add_qualification}"></p></div>
   <div class="form-group"><label>Enter University Name</label><input type="text" name="university[]" class="form-control" placeholder="enter university name"  /><p class ="err" id="university_err_${x_add_qualification}"></p></div>
   <div class="form-group end_date"><label>Start</label>
      <select class="form-control" name="yearstart[]" id="yearstart" required=""><option value="">Year of passing</option>
            <?php
            $year = date('Y');
            for ($i = $year; $i >= 1950; $i--) {
            ?>
            <option value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php
            }
            ?>
      </select>
   <p class ="err" id="yearstart_err_${x_add_qualification}"></p></div>
   <div class="form-group end_date"><label>End</label>
   <select class="form-control" name="yearend[]" id="yearend" required=""><option value="">Year of passing</option>
            <?php
            $year = date('Y');
            for ($i = $year; $i >= 1950; $i--) {
            ?>
            <option value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php
            }
            ?>
      </select>
   <p class ="err" id="yearend_err_${x_add_qualification}"></p></div>
   <div class="form-group"><label>Select Division</label><select name="division[]" class="form-control"><option value="">Select Division</option><option value="4">Gold Medalist</option><option value="1">Ist</option><option value="2">2nd</option><option value="3">3rd</option></select><p class ="err" id="division_err_${x_add_qualification}"></p></div>


                        

   <div class="col-md-12 text-center"><a href="javascript:void(${x_add_qualification});" class="remove_qial_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 20px;"><span class="glyphicon glyphicon-minus"></span> Remove </a></div>`;

            x_add_qualification++; //Increment field counter
            $(field_wrappersss).append(fieldHTML); //Add field html
            if (x_add_qualification == maxField) {
               $(this).hide();
            }
         }
      });

      //Once remove button is clicked
      $(field_wrappersss).on('click', '.remove_qial_button', function(e) {
         e.preventDefault();
         //$(this).parent("div").remove(); //Remove field html

         $(this).parent("div").parent("div").remove();
         x_add_qualification--; //Decrement field counter
      });
   });
</script>
<script>
   var maxField = 10; //Input fields increment limitation
   var addButton_internship = $('.add_internship'); //Add button selector
   var wrapper_internship = $('.wrapper_internship'); //Input field wrapper


   var x_add_internship = 1; //Initial field counter is 1

   //Once add button is clicked
   $(addButton_internship).click(function() {
      //Check maximum number of input fields
      if (x_add_internship < maxField) {
         var fieldHTML = `
         <div class="row">
         <h4>TRAINING AND PROFESSIONAL EXPERIENCE</h4>
                        <div class="form-group">
                           <label for="training_name_of_organization">Name of organization:*</label>
                           <input type="text" name="training_name_of_organization[]" id="training_name_of_organization_${x_add_internship}" onkeypress="search_organization(this)"  class="form-control training_name_of_organization" placeholder="Enter organization name" />
                           <p class="err" id="training_name_of_organization_err_${x_add_internship}"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_state"><?php echo lang("ctn_394") ?></label>
                           <select class="form-control" name="training_state[]" id="training_state_${x_add_internship}" onchange="getTrainingCity(${x_add_internship})">
                              <option value="">Select State</option>
                              <?php foreach ($states as $all_states) : ?>
                                 <option value="<?php echo $all_states->id ?>"><?php echo $all_states->name; ?></option>
                              <?php endforeach; ?>
                           </select>
                           <p class="err" id="training_state_err_${x_add_internship}"></p>
                        </div>

                        <div class="form-group">
                           <label for="training_city"><?php echo lang("ctn_393") ?></label>
                           <select class="form-control training_city[]" name="training_city" id="training_city_${x_add_internship}">
                              <option value="">Select City</option>
                           </select>
                           <p class="err" id="training_city_err_${x_add_internship}"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_court">Name of courts attended:*</label>
                           <select name="training_court[]" id="training_court" class="form-control" title="Select Court Name" placeholder="select court name">
                              <option value="">Select Court</option>
                              <?php foreach ($courts->result() as $court) {
                              ?>
                                 <option value="<?php echo $court->id; ?>"><?php echo $court->court_name; ?></option>
                              <?php } ?>
                           </select>
                           <p class="err" id="training_court_err_${x_add_internship}"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_start_date">Start Date:*</label>
                           <input type="date" class="form-control" id="training_start_date" name="training_start_date[]" />
                           <p class="err" id="training_start_date_err_${x_add_internship}"></p>
                        </div>
                        <div class="form-group">
                           <label for="training_end_date">End Date :</label>
                           <input type="date" class="form-control" id="training_end_date" name="training_end_date[]" />
                           <p class="err" id="training_end_date_err_${x_add_internship}"></p>
                        </div>
                        <div class="form-group">
                           <label for="role_learning">Roles & Learnings (Briefly Mention Key Responsibilities, Use Words That Demonstrate Action Like Created, Drafted, Learnt, Assessed, Etc.) :</label>
                           <input type="text" class="form-control" id="role_learning" name="role_learning[]" />
                           <p class="err" id="role_learning_err_${x_add_internship}"></p>
                        </div>
                        <div class="form-group">
                           <label for="internship_validation">internship Validation :</label>
                           <input type="checkbox" class="form-control" id="internship_validation" name="internship_validation[]" style="width: 16px;margin-bottom: 5px;margin-right: 10px;" />
                           <span>Yes</span>
                           <p class="err" id="internship_validation_err_${x_add_internship}"></p>
                        </div>
                        <div class="internship_validation">
                           <h5><b>Validate my internship with</b></h5>
                           <p>Internships validated by organizations add credibility and increases your LAWSMOS score by 500 points - the better you’re rank on the leadership board, the better are your chances of getting hired. Know more about lawsmos leadership board.</p>
                           <p>Contact Details of Authorised Person/ Organization.</p>
                           <div class="form-group">
                              <label for="contact_person_name">Contact Person's Name :</label>
                              <input type="text" class="form-control" id="contact_person_name" name="contact_person_name[]">
                              <p class="err" id="contact_person_name_err_${x_add_internship}"></p>
                           </div>
                           <div class="form-group">
                              <label for="contact_person_designation">Designation :</label>
                              <input type="text" class="form-control" id="contact_person_designation" name="contact_person_designation[]">
                              <p class="err" id="contact_person_designation_err_${x_add_internship}"></p>
                           </div>
                           <div class="form-group">
                              <label for="contact_person_phone_no">Phone no. :</label>
                              <input type="text" class="form-control" id="contact_person_phone_no" name="contact_person_phone_no[]">
                              <p class="err" id="contact_person_phone_no_err_${x_add_internship}"></p>
                           </div>
                           <div class="form-group">
                              <label for="contact_person_email_address">Email Address :</label>
                              <input type="text" class="form-control" id="contact_person_email_address" name="contact_person_email_address[]">
                              <p class="err" id="contact_person_email_address_err_${x_add_internship}"></p>
                           </div>
                        </div>
                        <div class="text-center">
                           <a href="javascript:void(${x_add_internship});" class="remove_internship_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 9px;margin-left: 5px;"><span class="glyphicon glyphicon-minus"></span> Remove </a>
                        </div>
                     </div>   
                    `;

         x_add_internship++; //Increment field counter
         $(wrapper_internship).append(fieldHTML); //Add field html
         if (x_add_internship == maxField) {
            $(this).hide();
         }
      }
   });

   //Once remove button is clicked
   $(wrapper_internship).on('click', '.remove_internship_button', function(e) {
      e.preventDefault();
      //$(this).parent("div").remove(); //Remove field html

      $(this).parent("div").parent("div").remove();
      x_add_internship--; //Decrement field counter
   });
</script>
<script>
   var maxField = 10; //Input fields increment limitation
   var addButton_award = $('.add_award'); //Add button selector
   var wrapper_award = $('.wrapper_award'); //Input field wrapper

   var x_add_award = 1; //Initial field counter is 1

   //Once add button is clicked
   $(addButton_award).click(function() {
      //Check maximum number of input fields
      if (x_add_award < maxField) {
         var fieldHTML = `
                        <div><div class="form-group">
                           <label for="awards">Extra-Curricular Awards/ Accomplishments :</label>
                           <input type="text" class="form-control" id="awards" name="awards[]" style="width: 46%;">
                           <a href="javascript:void(${x_add_award});" class="remove_award_button btn btn-info btn-xs" style="margin-bottom: 11px;margin-top: 9px;margin-left: 5px;"><span class="glyphicon glyphicon-minus"></span> Remove </a>
                           <p class="err" id="awards_err_${x_add_award}"></p></div>`;

         x_add_award++; //Increment field counter
         $(wrapper_award).append(fieldHTML); //Add field html
         if (x_add_award == maxField) {
            $(this).hide();
         }
      }
   });

   //Once remove button is clicked
   $(wrapper_award).on('click', '.remove_award_button', function(e) {
      e.preventDefault();
      //$(this).parent("div").remove(); //Remove field html

      $(this).parent("div").remove();
      wrapper_award--; //Decrement field counter
   });
</script>
<script>
   var maxField = 10; //Input fields increment limitation
   var addButton_research = $('.add_research'); //Add button selector
   var wrapper_research = $('.wrapper_research'); //Input field wrapper

   var x_add_research = 1; //Initial field counter is 1

   //Once add button is clicked
   $(addButton_research).click(function() {
      //Check maximum number of input fields
      if (x_add_research < maxField) {
         var fieldHTML = `
         <div class="row">
                        <h4>RESEARCH PAPERS</h4>
                        <div class="form-group">
                           <label for="research_title">Title :</label>
                           <input type="text" class="form-control" id="research_title" name="research_title[]">
                           <p class="err" id="research_title_err_${x_add_research}"></p>
                        </div>
                        <div class="form-group">
                           <label for="research_journal_name">Journal Name :</label>
                           <input type="text" class="form-control" id="research_journal_name" name="research_journal_name[]">
                           <p class="err" id="research_journal_name_err_${x_add_research}"></p>
                        </div>
                        <div class="form-group">
                           <label for="research_issn_number">ISSN Numbar(If applicable) :</label>
                           <input type="text" class="form-control" id="research_issn_number" name="research_issn_number[]">
                           <p class="err" id="research_issn_number_err_${x_add_research}"></p>
                        </div>
                        <div class="form-group">
                           <label for="research_share_link">Share Link :</label>
                           <input type="text" class="form-control" id="research_share_link" name="research_share_link[]">
                           <p class="err" id="research_share_link_err_${x_add_research}"></p>
                        </div>
                        <div class="text-center">
                           <a href="javascript:void(${x_add_research});" class="remove_research_button btn btn-info btn-xs" style="margin-bottom: 11px;"><span class="glyphicon glyphicon-minus"></span> Remove </a>
                        </div>
                     </div>`;

         x_add_research++; //Increment field counter
         $(wrapper_research).append(fieldHTML); //Add field html
         if (x_add_research == maxField) {
            $(this).hide();
         }
      }
   });

   //Once remove button is clicked
   $(wrapper_research).on('click', '.remove_research_button', function(e) {
      e.preventDefault();
      //$(this).parent("div").remove(); //Remove field html

      $(this).parent("div").parent("div").remove();
      x_add_research--; //Decrement field counter
   });
</script>
<script>
   var maxField = 10; //Input fields increment limitation
   var addButton_course = $('.add_course'); //Add button selector
   var wrapper_course = $('.wrapper_course'); //Input field wrapper

   var x_add_course = 1; //Initial field counter is 1

   //Once add button is clicked
   $(addButton_course).click(function() {
      //Check maximum number of input fields
      if (x_add_course < maxField) {
         var fieldHTML = `
         <div class="row">
                        <h4>DIPLOMA/ TRAINING/ COURSES</h4>
                        <div class="form-group">
                           <label for="course_college">University/ College/ Institute/ Org/</label>
                           <input type="text" name="course_college[]" class="form-control" onkeypress="search_university(this)" id="course_college_${x_add_course}" placeholder="Enter College name" />
                           <p class="err" id="course_college_err_${x_add_course}"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_program_name">Program Name</label>
                           <input type="text" name="course_program_name[]" id="course_program_name" class="form-control" placeholder="Enter program name" />
                           <p class="err" id="course_program_name_err_${x_add_course}"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_location">Location</label>
                           <input type="text" name="course_location[]" id="course_location" class="form-control" placeholder="enter location name" />
                           <p class="err" id="course_location_err_${x_add_course}"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_start_date">Start Date:*</label>
                           <input type="date" class="form-control" id="course_start_date" name="course_start_date[]" />
                           <p class="err" id="course_start_date_err_${x_add_course}"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_end_date">End Date :</label>
                           <input type="date" class="form-control" id="course_end_date" name="course_end_date[]" />
                           <p class="err" id="course_end_date_err_${x_add_course}"></p>
                        </div>
                        <div class="form-group">
                           <label class="control-label">Upload Certificate :</label>
                           <label for="course_certificate_${x_add_course}" class="upload_image_stepper">
                              <i class="fa fa-plus-circle" aria-hidden="true"></i>
                           </label>
                           <input class="form-control" id="course_certificate_${x_add_course}" onchange="readURL(this,'course_certificate_preview_image','course_certificate_url');" name="course_certificate" min="1" oninput="validity.valid||(value='');" placeholder="Upload Marksheet" type="file" required="" style="display:none;">
                           <?php
                           $course_certificate_preview_image = "preview.png";
                           ?>
                           <input type="hidden" class="course_certificate_url" name="course_certificate_url[]" value="<?php echo $course_certificate_preview_image; ?>">
                           <img class="course_certificate_preview_image preview_image" src="<?php echo base_url(); ?>/uploads/<?php echo $course_certificate_preview_image; ?>">
                           <p class="err" id="course_certificate_url_err_${x_add_course}"></p>
                        </div>
                        <div class="form-group">
                           <label for="course_validation">Request Validation :</label>
                           <input type="checkbox" class="form-control" id="course_validation" name="course_validation[]" style="width: 16px;margin-bottom: 5px;margin-right: 10px;" />
                           <span>Yes</span>
                           <p class="err" id="role_learning_err_${x_add_course}"></p>
                        </div>
                        <div class="course_validation">
                           <div class="form-group">
                              <label for="course_contact_person_name">Contact Person's Name :</label>
                              <input type="text" class="form-control" id="course_contact_person_name" name="course_contact_person_name[]">
                              <p class="err" id="course_contact_person_name_err_${x_add_course}"></p>
                           </div>
                           <div class="form-group">
                              <label for="course_contact_person_designation">Contact Person's Designation :</label>
                              <input type="text" class="form-control" id="course_contact_person_designation" name="course_contact_person_designation[]">
                              <p class="err" id="course_contact_person_designation_err_${x_add_course}"></p>
                           </div>
                           <div class="form-group">
                              <label for="course_contact_person_email">Email address of training organization :</label>
                              <input type="text" class="form-control" id="course_contact_person_email" name="course_contact_person_email[]">
                              <p class="err" id="course_contact_person_email_err_${x_add_course}"></p>
                           </div>
                        </div>
                        <div class="text-center">
                           <a href="javascript:void(${x_add_course});" class="remove_course_button btn btn-info btn-xs" style="margin-bottom: 11px;"><span class="glyphicon glyphicon-minus"></span> Remove </a>
                        </div>
                     </div>`;

         x_add_course++; //Increment field counter
         $(wrapper_course).append(fieldHTML); //Add field html
         if (x_add_course == maxField) {
            $(this).hide();
         }
      }
   });

   //Once remove button is clicked
   $(wrapper_course).on('click', '.remove_course_button', function(e) {
      e.preventDefault();
      //$(this).parent("div").remove(); //Remove field html

      $(this).parent("div").parent("div").remove();
      x_add_course--; //Decrement field counter
   });
</script>
<script>
   var currentTab = 0; // Current tab is set to be the first tab (0)
   showTab(currentTab); // Display the current tab

   function showTab(n) {

      // This function will display the specified tab of the form ...
      var x = document.getElementsByClassName("tab");

      x[n].style.display = "block";
      // ... and fix the Previous/Next buttons:
      if (n == 0) {
         document.getElementById("prevBtn").style.display = "none";
      } else {
         document.getElementById("prevBtn").style.display = "none";
      }
      if (n == (x.length - 1)) {
         document.getElementById("nextBtn").innerHTML = "Update";
      } else {
         document.getElementById("nextBtn").innerHTML = "Update";
      }
      // ... and run a function that displays the correct step indicator:
      fixStepIndicator(n)
   }

   function nextPrev(n) {


      // This function will figure out which tab to display
      var x = document.getElementsByClassName("tab");

      if (currentTab != 5) {
         // $("#stepper_"+currentTab).removeClass("step-active");

         x[currentTab].style.display = "none";

      }
      // Increase or decrease the current tab by 1:

      currentTab = currentTab + n;

      // if you have reached the end of the form... :

      // $(".stepper").removeClass("step-done");

      $(".stepper").removeClass("step-active");
      $("#stepper_" + currentTab).addClass("step-done");
      $("#stepper_" + parseInt(currentTab + 1)).addClass("step-active");
      $("#stepper_" + parseInt(currentTab + 1)).removeClass("step-done");


      if (currentTab >= x.length) {
         //...the form gets submitted:
         $('#prevBtn').hide();
         $('#nextBtn').hide();
         //  document.getElementById("regForm").submit();
         return false;
      }
      // Otherwise, display the correct tab:
      showTab(currentTab);
   }



   function fixStepIndicator(n) {
      // This function removes the "active" class of all steps...

      var i, x = document.getElementsByClassName("step");

      for (i = 0; i < x.length; i++) {
         x[i].className = x[i].className.replace(" active", "");
      }
      //... and adds the "active" class to the current step:
      x[n].className += " active";
   }

   $('.first').click(function() {

      var error = 0;
      var student_enrolled = "";
      var date_of_birth = "";
      
      student_enrolled = $('input[name=student_enrolled]:checked').val();
      date_of_birth = $('#date_of_birth').val();

      $('#student_enrolled_err').html('');
      $('#date_of_birth_err').html('');

      if (student_enrolled == null || student_enrolled == "") {
         $('#student_enrolled_err').html('Please select this field.');
         error = 1;
      }
      if(date_of_birth == null || date_of_birth == "") {
         $('#date_of_birth_err').html('Please select date of birth');
         error = 1;
      }

      if (error == 1) {
         return false;
      } else {
         $.ajax({
            url: "<?php echo site_url("home/student_step1") ?>",
            type: "POST",
            dataType: "json",
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
              
            },
            data: {
               'student_enrolled': student_enrolled,
               'date_of_birth' : date_of_birth,
               '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
            },
            success: function(data) {
               var obj = data;
               if (obj.status_code == true) {

                    toastr.success("Profile updated successfully.");
                        setTimeout(
                        function() 
                        {
                           window.location.href = "<?php echo base_url('edit_profile_student'); ?>";
                        }, 2000);
                   
               }
            },
            error: function(error) {
                $('#notification-loaders').css("display", "none");
               console.log(`Error ${error}`);
            }
         });
      }
   });


   var limit = 6;
   let selectValue = null;
   $('input.single-checkbox').on('change', function(evt) {
      if ($('.single-checkbox:checked').length > limit) {
         this.checked = false;
      }
   });


   function readURL_profile_pic(input) {
      if (input.files && input.files[0]) {
         var reader = new FileReader();

         reader.onload = function(e) {
            $('#preview_image_profile')
               .attr('src', e.target.result);
         };

         reader.readAsDataURL(input.files[0]);
      }
   }

   function readURL_cover_pic(input) {
      if (input.files && input.files[0]) {
         var reader = new FileReader();

         reader.onload = function(e) {
            $('#preview_image_cover')
               .attr('src', e.target.result);
         };

         reader.readAsDataURL(input.files[0]);
      }
   }




   $('.second').click(function() {
      // 10th detail
      var error = 0;

      $('#school_10_err').html('');
      $('#board_10_err').html('');
      $('#year_pass_10_err').html('');
      $('#x_marks_err').html('');

      $('#school_12_err').html('');
      $('#board_12_err').html('');
      $('#year_pass_12_err').html('');
      $('#xii_marks_err').html('');

      $('#degree_err').html('');
      $('#yearstart_err').html('');
      $('#yearend_err').html('');
      $('#university_err').html('');
      $('#college_err').html('');
      $('#division_err').html('');



      var marks_x_percentage = "";
      var x_division = "";
      var marks_x_cgpa = "";

      var marks_xii_percentage = "";
      var xii_division = "";
      var marks_xii_cgpa = "";


      var school_10 = $('#school_10').val();
      var board_10 = $('#board_10').val();
      var year_pass_10 = $('#year_pass_10').val();
      var x_marks_mode = $('input[name=marks_x_radio]:checked').val();




      // 12th detail
      var school_12 = $('#school_12').val();
      var board_12 = $('#board_12').val();
      var year_pass_12 = $('#year_pass_12').val();
      var xii_marks_mode = $('input[name=marks_xii_radio]:checked').val();

      var degree = [];
      var yearstart = [];
      var yearend = [];
      var college = [];
      var university = [];
      var division = [];





      $('input[name="degree[]"]').each(function(i, item) {
         degree.push(item.value);
      });
      $('select[name="yearstart[]"]').each(function(i, item) {
         yearstart.push(item.value);
      });
      $('select[name="yearend[]"]').each(function(i, item) {
         yearend.push(item.value);
      });
      $('input[name="college[]"]').each(function(i, item) {
         college.push(item.value);
      });
      $('input[name="university[]"]').each(function(i, item) {
         university.push(item.value);
      });
      $('select[name="division[]"]').each(function(i, item) {
         division.push(item.value);
      });




      //10 school validation
      if (school_10 == "" || school_10 == null) {
         $('#school_10_err').html('Please Fill School Name');
         error = 1;
      }
      if (board_10 == "" || board_10 == null) {
         $('#board_10_err').html('Please Fill School Name');
         error = 1;;
      }
      if (year_pass_10 <= 0) {
         $('#year_pass_10_err').html('Please Select Year Of Passing ');
         error = 1;;
      }
      if (x_marks_mode == "" || x_marks_mode == null) {
         $('#x_marks_err').html('Please select marks detail');
         error = 1;;
      } else {
         if (x_marks_mode == 1) {
            marks_x_percentage = $('#marks_x_percentage').val();
            if (marks_x_percentage == "" || marks_x_percentage == null) {
               $('#x_marks_err').html('Please enter marks in percentage');
               error = 1;
            }
         }
         if (x_marks_mode == 2) {
            var x_division = $('#x_division').val();
            if (x_division == "" || x_division == 0) {
               $('#x_marks_err').html('Please select divison');
               error = 1;
            }
         }
         if (x_marks_mode == 3) {
            marks_x_cgpa = $('#marks_x_cgpa').val();
            if (marks_x_cgpa == "" || marks_x_cgpa == null) {
               $('#x_marks_err').html('Please enter in marks in cgpa');
               error = 1;
            }
         }
      }

      //12 school validation
      if (school_12 == "" || school_12 == null) {
         $('#school_12_err').html('Please enter school name');
         error = 1;
      }
      if (board_12 == "" || board_12 == null) {
         $('#board_12_err').html('Please enter board name');
         error = 1;
      }

      if (year_pass_12 <= 0) {
         $('#year_pass_12_err').html('Please select year of passing ');
         error = 1;
      }

      if (xii_marks_mode == "" || xii_marks_mode == null) {
         $('#xii_marks_err').html('Please select marks detail');
         error = 1;
      } else {

         if (xii_marks_mode == 1) {
            marks_xii_percentage = $('#marks_xii__percentage').val();
            if (marks_xii_percentage == "" || marks_xii_percentage == null) {
               $('#xii_marks_err').html('Please enter marks in percentage');
               error = 1;
            }
         }
         if (xii_marks_mode == 2) {
            xii_division = $('#xii_division').val();
            if (xii_division == 0) {
               $('#xii_marks_err').html('Please select divison');
               error = 1;
            }
         }
         if (xii_marks_mode == 3) {
            marks_xii_cgpa = $('#marks_xii__cgpa').val();
            if (marks_xii_cgpa == "" || marks_xii_cgpa == null) {
               $('#xii_marks_err').html('Please enter in marks in cgpa');
               error = 1;
            }
         }
      }




      // //qualification validation

      if (degree == "" || degree == null) {
         $('#degree_err').html('Please enter degree detail');
         error = 1;
      }

      if (yearstart == "" || yearstart == null) {
         $('#yearstart_err').html('Please enter yearstart detail');
         error = 1;
      }


      if (yearend == "" || yearend == null) {
         $('#yearend_err').html('Please enter yearend detail');
         error = 1;
      }


      if (university == "" || university == null) {
         $('#university_err').html('Please enter university detail');
         error = 1;
      }


      if (college == "" || college == null) {
         $('#college_err').html('Please enter university detail');
         error = 1;
      }


      for (let index = 0; index < degree.length; index++) {
         $('#degree_err_' + index).html('');
         if (degree[index] == "") {
            $('#degree_err_' + index).html('Please enter degree/course name.');
            error = 1;
         }
      }

      for (let index = 0; index < yearstart.length; index++) {
         $('#yearstart_err_' + index).html('');
         if (yearstart[index] == "") {
            $('#yearstart_err_' + index).html('Please select start year.');
            error = 1;
         }
      }

      for (let index = 0; index < yearend.length; index++) {
         $('#yearend_err_' + index).html('');
         if (yearend[index] == "") {
            $('#yearend_err_' + index).html('Please select end year.');
            error = 1;
         }
      }

      for (let index = 0; index < college.length; index++) {
         $('#college_err_' + index).html('');
         if (college[index] == "") {
            $('#college_err_' + index).html('Please enter college name.');
            error = 1;
         }
      }

      for (let index = 0; index < university.length; index++) {
         $('#university_err_' + index).html('');
         if (university[index] == 0) {
            $('#university_err_' + index).html('Please enter university name.');
            error = 1;
         }
      }

      for (let index = 0; index < division.length; index++) {
         $('#division_err_' + index).html('');
         if (division[index] == 0) {
            $('#division_err_' + index).html('Please select division.');
            error = 1;
         }
      }
      if (error == 1) {
         return false;
      } else {
         $.ajax({
            url: "<?php echo site_url("home/student_step2/") ?>",
            type: "POST",
            dataType: "json",
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
               
            },
            data: {
               'school_10': school_10,
               'board_10': board_10,
               'year_pass_10': year_pass_10,
               'x_marks_type': x_marks_mode,
               'marks_x_percentage': marks_x_percentage,
               'x_division': x_division,
               'marks_x_cgpa': marks_x_cgpa,
               'school_12': school_12,
               'board_12': board_12,
               'year_pass_12': year_pass_12,
               'xii_marks_type': xii_marks_mode,
               'marks_xii_percentage': marks_xii_percentage,
               'xii_division': xii_division,
               'marks_xii_cgpa': marks_xii_cgpa,
               'degree': degree,
               'yearstart': yearstart,
               'yearend': yearend,
               'college': college,
               'university': university,
               'division': division,
               '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
            },
            success: function(data) {
               var obj = data;
               if (obj.status == true) {
                   toastr.success("Profile updated successfully.");
                        setTimeout(
                        function() 
                        {
                           window.location.href = "<?php echo base_url('edit_profile_student'); ?>";
                        }, 2000);
               }
            },
            error: function(error) {
                $('#notification-loaders').css("display", "none");
               console.log(`Error ${error}`);
            }
         })
      }



   });

   $('.third').click(function() {

      var error = 0;


      var training_name_of_organization = [];
      var training_state = [];
      var training_city = [];
      var training_court = [];
      var training_start_date = [];
      var training_end_date = [];
      var role_learning = [];
      var internship_validation = [];
      var contact_person_name = [];
      var contact_person_designation = [];
      var contact_person_phone_no = [];
      var contact_person_email_address = [];

      var research_title = [];
      var research_journal_name = [];
      var research_issn_number = [];
      var research_share_link = [];

      var course_college = [];
      var course_program_name = [];
      var course_location = [];
      var course_start_date = [];
      var course_end_date = [];
      var course_certificate_url = [];
      var course_validation = [];
      var course_contact_person_name = [];
      var course_contact_person_designation = [];
      var course_contact_person_email = [];

      var project_title = "";
      var project_start_month = "";
      var project_end_month = "";
      var project_description = "";
      var project_link = "";

      var hobbies = "";

      var awards = [];

      var skills = "";

      var academic_awards = "";

      $("#project_title_err").html("");
      $("#project_start_month_err").html("");
      $("#project_end_month_err").html("");
      $("#project_description_err").html("");
      $("#project_link_err").html("");

      $("#hobbies_err").html("");

      $('input[name="training_name_of_organization[]"]').each(function(i, item) {
         training_name_of_organization.push(item.value);
      });
      $('select[name="training_state[]"]').each(function(i, item) {
         training_state.push(item.value);
      });
      $('select[name="training_city[]"]').each(function(i, item) {
         training_city.push(item.value);
      });
      $('select[name="training_court[]"]').each(function(i, item) {
         training_court.push(item.value);
      });
      $('input[name="training_start_date[]"]').each(function(i, item) {
         training_start_date.push(item.value);
      });
      $('input[name="training_end_date[]"]').each(function(i, item) {
         training_end_date.push(item.value);
      });
      $('input[name="role_learning[]"]').each(function(i, item) {
         role_learning.push(item.value);
      });
      $('input[name="internship_validation[]"]').each(function(i, item) {
         internship_validation.push(item.value);
      });
      $('input[name="contact_person_name[]"]').each(function(i, item) {
         contact_person_name.push(item.value);
      });
      $('input[name="contact_person_designation[]"]').each(function(i, item) {
         contact_person_designation.push(item.value);
      });
      $('input[name="contact_person_phone_no[]"]').each(function(i, item) {
         contact_person_phone_no.push(item.value);
      });
      $('input[name="contact_person_email_address[]"]').each(function(i, item) {
         contact_person_email_address.push(item.value);
      });


      $('input[name="research_title[]"]').each(function(i, item) {
         research_title.push(item.value);
      });
      $('input[name="research_journal_name[]"]').each(function(i, item) {
         research_journal_name.push(item.value);
      });
      $('input[name="research_issn_number[]"]').each(function(i, item) {
         research_issn_number.push(item.value);
      });
      $('input[name="research_share_link[]"]').each(function(i, item) {
         research_share_link.push(item.value);
      });


      $('input[name="course_college[]"]').each(function(i, item) {
         course_college.push(item.value);
      });
      $('input[name="course_program_name[]"]').each(function(i, item) {
         course_program_name.push(item.value);
      });
      $('input[name="course_location[]"]').each(function(i, item) {
         course_location.push(item.value);
      });
      $('input[name="course_start_date[]"]').each(function(i, item) {
         course_start_date.push(item.value);
      });
      $('input[name="course_end_date[]"]').each(function(i, item) {
         course_end_date.push(item.value);
      });
      $('input[name="course_certificate_url[]"]').each(function(i, item) {
         course_certificate_url.push(item.value);
      });
      $('input[name="course_validation[]"]').each(function(i, item) {
         course_validation.push(item.value);
      });
      $('input[name="course_contact_person_name[]"]').each(function(i, item) {
         course_contact_person_name.push(item.value);
      });
      $('input[name="course_contact_person_designation[]"]').each(function(i, item) {
         course_contact_person_designation.push(item.value);
      });
      $('input[name="course_contact_person_email[]"]').each(function(i, item) {
         course_contact_person_email.push(item.value);
      });

      project_title = $("#project_title").val();
      project_start_month = $("#project_start_month").val();
      project_end_month = $("#project_end_month").val();
      project_description = $("#project_description").val();
      project_link = $("#project_link").val();

      hobbies = $("#hobbies").val();


      $('input[name="awards[]"]').each(function(i, item) {
         awards.push(item.value);
      });

      skills = $("#skills").val();

      academic_awards = $("#academic_awards").val();


      for (let index = 0; index < training_name_of_organization.length; index++) {
         $('#training_name_of_organization_err_' + index).html('');
         if (training_name_of_organization[index] == "") {
            $('#training_name_of_organization_err_' + index).html('Please enter organization name.');
            error = 1;
         }
      }
      for (let index = 0; index < training_state.length; index++) {
         $('#training_state_err_' + index).html('');
         if (training_state[index] == "") {
            $('#training_state_err_' + index).html('Please select state name.');
            error = 1;
         }
      }
      for (let index = 0; index < training_city.length; index++) {
         $('#training_city_err_' + index).html('');
         if (training_city[index] == "") {
            $('#training_city_err_' + index).html('Please select city name.');
            error = 1;
         }
      }
      for (let index = 0; index < training_court.length; index++) {
         $('#training_court_err_' + index).html('');
         if (training_court[index] == "") {
            $('#training_court_err_' + index).html('Please select court name.');
            error = 1;
         }
      }
      for (let index = 0; index < training_start_date.length; index++) {
         $('#training_start_date_err_' + index).html('');
         if (training_start_date[index] == "") {
            $('#training_start_date_err_' + index).html('Please select start date.');
            error = 1;
         }
      }
      for (let index = 0; index < training_end_date.length; index++) {
         $('#training_end_date_err_' + index).html('');
         if (training_end_date[index] == 0) {
            $('#training_end_date_err_' + index).html('Please select end date.');
            error = 1;
         }
      }
      for (let index = 0; index < role_learning.length; index++) {
         $('#role_learning_err_' + index).html('');
         if (role_learning[index] == 0) {
            $('#role_learning_err_' + index).html('Please enter role & learning.');
            error = 1;
         }
      }
      for (let index = 0; index < internship_validation.length; index++) {
         $('#internship_validation_err_' + index).html('');
         if (internship_validation[index] == 0) {
            $('#internship_validation_err_' + index).html('This field is required.');
            error = 1;
         }
      }
      for (let index = 0; index < contact_person_name.length; index++) {
         $('#contact_person_name_err_' + index).html('');
         if (contact_person_name[index] == 0) {
            $('#contact_person_name_err_' + index).html('Please enter contact person name.');
            error = 1;
         }
      }
      for (let index = 0; index < contact_person_designation.length; index++) {
         $('#contact_person_designation_err_' + index).html('');
         if (contact_person_designation[index] == 0) {
            $('#contact_person_designation_err_' + index).html('Please enter contact person designation.');
            error = 1;
         }
      }
      for (let index = 0; index < contact_person_phone_no.length; index++) {
         $('#contact_person_phone_no_err_' + index).html('');
         if (contact_person_phone_no[index] == 0) {
            $('#contact_person_phone_no_err_' + index).html('Please enter contact person phone number.');
            error = 1;
         }
      }
      for (let index = 0; index < contact_person_email_address.length; index++) {
         $('#contact_person_email_address_err_' + index).html('');
         if (contact_person_email_address[index] == 0) {
            $('#contact_person_email_address_err_' + index).html('Please enter contact person email address.');
            error = 1;
         }
      }


      // for (let index = 0; index < research_title.length; index++) {
      //    $('#research_title_err_' + index).html('');
      //    if (research_title[index] == 0) {
      //       $('#research_title_err_' + index).html('Please enter research title.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < research_journal_name.length; index++) {
      //    $('#research_journal_name_err_' + index).html('');
      //    if (research_journal_name[index] == 0) {
      //       $('#research_journal_name_err_' + index).html('Please enter research journal name.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < research_issn_number.length; index++) {
      //    $('#research_issn_number_err_' + index).html('');
      //    if (research_issn_number[index] == 0) {
      //       $('#research_issn_number_err_' + index).html('Please enter research issn number.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < research_share_link.length; index++) {
      //    $('#research_share_link_err_' + index).html('');
      //    if (research_share_link[index] == 0) {
      //       $('#research_share_link_err_' + index).html('Please enter research share link.');
      //       error = 1;
      //    }
      // }


      // for (let index = 0; index < course_college.length; index++) {
      //    $('#course_college_err_' + index).html('');
      //    if (course_college[index] == 0) {
      //       $('#course_college_err_' + index).html('Please enter college name.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_program_name.length; index++) {
      //    $('#course_program_name_err_' + index).html('');
      //    if (course_program_name[index] == 0) {
      //       $('#course_program_name_err_' + index).html('Please enter course name.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_location.length; index++) {
      //    $('#course_location_err_' + index).html('');
      //    if (course_location[index] == 0) {
      //       $('#course_location_err_' + index).html('Please enter your location.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_start_date.length; index++) {
      //    $('#course_start_date_err_' + index).html('');
      //    if (course_start_date[index] == 0) {
      //       $('#course_start_date_err_' + index).html('Please select course start date.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_end_date.length; index++) {
      //    $('#course_end_date_err_' + index).html('');
      //    if (course_end_date[index] == 0) {
      //       $('#course_end_date_err_' + index).html('Please select course end date.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_certificate_url.length; index++) {
      //    $('#course_certificate_url_err_' + index).html('');
      //    if (course_certificate_url[index] == 0) {

      //       $('#course_certificate_url_err_' + index).html('Please upload course certificate.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_contact_person_name.length; index++) {
      //    $('#course_contact_person_name_err_' + index).html('');
      //    if (course_contact_person_name[index] == 0) {
      //       $('#course_contact_person_name_err_' + index).html('Please enter contact person name.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_contact_person_designation.length; index++) {
      //    $('#course_contact_person_designation_err_' + index).html('');
      //    if (course_contact_person_designation[index] == 0) {
      //       $('#course_contact_person_designation_err_' + index).html('Please enter contact person designation.');
      //       error = 1;
      //    }
      // }
      // for (let index = 0; index < course_contact_person_email.length; index++) {
      //    $('#course_contact_person_email_err_' + index).html('');
      //    if (course_contact_person_email[index] == 0) {
      //       $('#course_contact_person_email_err_' + index).html('Please enter contact person email.');
      //       error = 1;
      //    }
      // }


      // if (project_title == "" || project_title == null) {
      //    $('#project_title_err').html('Please enter project title');
      //    error = 1;
      // }
      // if (project_start_month == "" || project_start_month == null) {
      //    $('#project_start_month_err').html('Please select start month');
      //    error = 1;
      // }
      // if (project_end_month == "" || project_end_month == null) {
      //    $('#project_end_month_err').html('Please select end month');
      //    error = 1;
      // }
      // if (project_description == "" || project_description == null) {
      //    $('#project_description_err').html('Please enter project description');
      //    error = 1;
      // }
      // if (project_link == "" || project_link == null) {
      //    $('#project_link_err').html('Please enter project link');
      //    error = 1;
      // }
      // if (hobbies == "" || hobbies == null) {
      //    $('#hobbies_err').html('Please enter your hobbies');
      //    error = 1;
      // }

      // for (let index = 0; index < awards.length; index++) {
      //    $('#awards_err_' + index).html('');
      //    if (awards[index] == 0) {
      //       $('#awards_err_' + index).html('Please enter award name.');
      //       error = 1;
      //    }
      // }

      // if (skills == "" || skills == null) {
      //    $('#skills_err').html('Please enter your skills');
      //    error = 1;
      // }

      // if (academic_awards == "" || academic_awards == null) {
      //    $('#academic_awards_err').html('Please enter academic awards');
      //    error = 1;
      // }

      if (error == 1) {
         return false;
      } else {
         
         $.ajax({
            url: "<?php echo site_url("home/student_step3/") ?>",
            type: "POST",
            dataType: "json",
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
          
            },
            data: {
               'training_name_of_organization': training_name_of_organization,
               'training_state': training_state,
               'training_city': training_city,
               'training_court': training_court,
               'training_start_date': training_start_date,
               'training_end_date': training_end_date,
               'role_learning': role_learning,
               'internship_validation': internship_validation,
               'contact_person_name': contact_person_name,
               'contact_person_designation': contact_person_designation,
               'contact_person_phone_no': contact_person_phone_no,
               'contact_person_email_address': contact_person_email_address,
               'research_title': research_title,
               'research_journal_name': research_journal_name,
               'research_issn_number': research_issn_number,
               'research_share_link': research_share_link,
               'course_college': course_college,
               'course_program_name': course_program_name,
               'course_location': course_location,
               
               'course_start_date': course_start_date,
               'course_end_date': course_end_date,
               'course_certificate_url': course_certificate_url,
               'course_validation': course_validation,
               'course_contact_person_name': course_contact_person_name,
               'course_contact_person_designation': course_contact_person_designation,
               'course_contact_person_email': course_contact_person_email,
               'project_title': project_title,
               'project_start_month': project_start_month,
               'project_end_month': project_end_month,
               'project_description': project_description,
               'project_link': project_link,
               'hobbies': hobbies,
               'awards': awards,
               'skills': skills,
               'academic_awards': academic_awards,
               '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
            },
            success: function(data) {
               var obj = data;
               if (obj.status == true) {
                    toastr.success("Profile updated successfully.");
                        setTimeout(
                        function() 
                        {
                           window.location.href = "<?php echo base_url('edit_profile_student'); ?>";
                        }, 2000);
               }
            },
            error: function(error) {
                $('#notification-loaders').css("display", "none");
               console.log(`Error ${error}`);
            }
         })
      }
   });



   $('.fourth').click(function() {
      var error = 0;

      $('#fields_of_interest_err').html('');
      $('#career_interest_err').html('');


      var fields_of_interest = $('#fields_of_interest').val();
      var career_interest = $('#career_interest').val()

      if (fields_of_interest == "" || fields_of_interest == null) {
         $('#fields_of_interest_err').html('Please select fields of interests');
         error = 1;
      }
      if (career_interest == "" || career_interest == null) {
         $('#career_interest_err').html('Please select career interests');
         error = 1;
      }

      if (error == 1) {
         return false;
      } else {


         $.ajax({
            url: "<?php echo site_url("home/student_step4/") ?>",
            type: "POST",
            dataType: "json",
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
              
            },
            data: {
               'fields_of_interest': fields_of_interest,
               'career_interest': career_interest,
               '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash() ?>'
            },
            success: function(data) {
               var obj = data;
               if (obj.status == true) {
                    toastr.success("Profile updated successfully.");
                        setTimeout(
                        function() 
                        {
                           window.location.href = "<?php echo base_url('edit_profile_student'); ?>";
                        }, 2000);
               }
            },
            error: function(error) {
                $('#notification-loaders').css("display", "none");
               console.log(`Error ${error}`);
            }
         });
      }
   });

   $('.fifth').click(function() {
      var error = 0;

      $('#profile_picture_err').html('');
      $('#cover_photo_err').html('');


      var profile_picture = $('#profile_picture').prop('files')[0];
      var cover_photo = $('#cover_photo').prop('files')[0];

      if (profile_picture == "" || profile_picture == null) {
        // $('#profile_picture_err').html('Please select profile picture');
         error = 0;
      }
      if (cover_photo == "" || cover_photo == null) {
         //$('#cover_photo_err').html('Please upload cover photo');
         error = 0;
      }

      if (error == 1) {
         return false;
      } else {

         var formData = new FormData();
         formData.append('profile_picture', profile_picture);
         formData.append('cover_photo', cover_photo);
         formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

         $.ajax({
            url: "<?php echo site_url("home/studentnew_step5/") ?>",
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
               
            },
            data: formData,
            success: function(data) {
               var obj = data;
               if (obj.status == true) {
                    toastr.success("Profile updated successfully.");
                        setTimeout(
                        function() 
                        {
                           window.location.href = "<?php echo base_url('edit_profile_student'); ?>";
                        }, 2000);
               }
            },
            error: function(error) {
               $('#notification-loaders').css("display", "none");
               console.log(`Error ${error}`);
            }
         });
      }
   });

   //get state bar reg form upload method

   var verification_level = "<?php echo $verification_level; ?>";
   // nextPrev(4);
   if (verification_level == 1) {
      nextPrev(1);
   }
   if (verification_level == 2) {
      nextPrev(2);
   }
   if (verification_level == 3) {
      nextPrev(3);
   }
   if (verification_level == 4) {
      nextPrev(4);
   }
   if (verification_level > 4 || verification_level == 5) {
      window.location.href = "<?php echo base_url(); ?>";
   }
</script>
<script>
   var no = '<?php echo $core_profile["x_marks_type"] ?>'
   if (no == 1) {
         $("#x_percentage_div").show();
         $("#x_division_div").hide();
         $("#x_cgpa_div").hide();
         $("#marks_x_radioper").prop("checked", true);
      }
      if (no == 2) {
         $("#x_percentage_div").hide();
         $("#x_division_div").show();
         $("#x_cgpa_div").hide();
          $("#marks_x_radiodiv").prop("checked", true);
      }
      if (no == 3) {
         $("#x_percentage_div").hide();
         $("#x_division_div").hide();
         $("#x_cgpa_div").show();
          $("#marks_x_radiocg").prop("checked", true);
      }
   function marks_x_mode(no) {
      if (no == 0) {
         $("#x_percentage_div").show();
         $("#x_division_div").hide();
         $("#x_cgpa_div").hide();
      }
      if (no == 1) {
         $("#x_percentage_div").hide();
         $("#x_division_div").show();
         $("#x_cgpa_div").hide();
      }
      if (no == 2) {
         $("#x_percentage_div").hide();
         $("#x_division_div").hide();
         $("#x_cgpa_div").show();
      }

   }
var no = '<?php echo $core_profile['xii_marks_type']; ?>'

if (no == 1) {
         $("#xii_percentage_div").show();
         $("#xii_division_div").hide();
         $("#xii_cgpa_div").hide();
         $("#marks_12_percentage").prop("checked", true);
         
      }
      if (no == 2) {
         $("#xii_percentage_div").hide();
         $("#xii_division_div").show();
         $("#xii_cgpa_div").hide();
         $("#marks_12_division").prop("checked", true);
      }
      if (no == 3) {
         $("#xii_percentage_div").hide();
         $("#xii_division_div").hide();
         $("#xii_cgpa_div").show();
         $("#marks_12_cgpa").prop("checked", true);
      }
   function marks_xii__mode(no) {
      if (no == 0) {
         $("#xii_percentage_div").show();
         $("#xii_division_div").hide();
         $("#xii_cgpa_div").hide();
      }
      if (no == 1) {
         $("#xii_percentage_div").hide();
         $("#xii_division_div").show();
         $("#xii_cgpa_div").hide();
      }
      if (no == 2) {
         $("#xii_percentage_div").hide();
         $("#xii_division_div").hide();
         $("#xii_cgpa_div").show();
      }

   }

   function maxLengthCheck(object) {
      if (parseInt(object.value, 10) > parseInt(object.max, 10))
         object.value = object.max;
   }

   function isNumeric(evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode(key);
      var regex = /[0-9]|\./;
      if (!regex.test(key)) {
         theEvent.returnValue = false;
         if (theEvent.preventDefault) theEvent.preventDefault();
      }
   }



   function getTrainingCity(no) {
      var error = 0;
      var type = no;
      var final_list = "";
      var state_id = "";

      state_id = $('#training_state_' + no).val();
      final_list = $('#training_city_' + no);


      if (error == 1) {
         return false;
      } else {
         var cities = [];
         var all_city = "";
         var formData = new FormData();
         formData.append('state_id', state_id);
         formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

         $.ajax({
            url: "<?php echo site_url("home/getcitybystate/") ?>",
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
               $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
               $('#notification-loaders').css("display", "none");
            },
            data: formData,
            success: function(data) {
               var obj = data;
               for (let index = 0; index < obj.length; index++) {
                  cities += `<option value="${obj[index].id}" id="">${obj[index].name}</option>`;

               }
               final_list.html(cities);
            },
            error: function(error) {
               console.log(`Error ${error}`);
            }
         });

      }


   }

   function readURL(input, classname, hidden = '') {
      if (input.files && input.files[0]) {
         var reader = new FileReader();
         reader.onload = function(e) {
            $(input).siblings('.' + classname).attr('src', e.target.result);
         };

         reader.readAsDataURL(input.files[0]);

         var formData = new FormData();
         formData.append('marksheetform', input.files[0]);
         formData.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash() ?>');

         $.ajax({
            url: "<?php echo site_url("home/uploadmarksheetform/") ?>",
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
               // $('#notification-loaders').fadeIn(100);
            },
            complete: function() {
               $('#notification-loaders').css("display", "none");
            },
            data: formData,
            success: function(data) {
               var obj = data;
               if (obj.status == 1) {
                  $(input).siblings('.' + hidden).val(obj.message);
               }

            },
            error: function(error) {
               console.log(`Error ${error}`);
            }
         });
      }
   }

   function search_organization(data) {
      var id  = $(data).attr('id');
      $("#"+id).autocomplete({
         delay: 300,
         minLength: 2,
         source: function(request, response) {
            $.ajax({
               type: "GET",
               url: global_base_url + "home/get_law_firms",
               data: {
                  query: request.term
               },
               dataType: 'JSON',
               success: function(msg) {
                  response(msg);
               }
            });
         }
      });
   }
   function search_university(data) {
      var id  = $(data).attr('id');
      $("#"+id).autocomplete({
         delay: 300,
         minLength: 2,
         source: function(request, response) {
            $.ajax({
               type: "GET",
               url: global_base_url + "home/get_universities",
               data: {
                  query: request.term
               },
               dataType: 'JSON',
               success: function(msg) {
                  response(msg);
               }
            });
         }
      });
   }

   
var message = localStorage.getItem('login_message'); localStorage.removeItem("login_message");
   console.log(message);
$('.profile-formwrap').find('.alert').remove();
if (message) {
    $('.profile-formwrap').append('<div class="alert alert-success global_msg"><b><span class="glyphicon glyphicon-ok"></span></b>'+message+'</div>');
}
    
  /* 
   localStorage.removeItem("login_message");
   console.log(message);
   $.ajax({
     url: "<?php //echo site_url("profile") ?>",
     type : 'GET',
     data: {
         login_message: message,
         check_storage:"true"
     },
     success: function(resultmessage) {
         console.log(resultmessage.length);
         if (resultmessage.length > 1) {
            $('.success-message').html('<div class="alert alert-success global_msg"><b><span class="glyphicon glyphicon-ok"></span></b>'+resultmessage+'</div>');
         }
     },error: function(resultmessage){
         console.log(resultmessage);
     } 
  });*/
   
</script>