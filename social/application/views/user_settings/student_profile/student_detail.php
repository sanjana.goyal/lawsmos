<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
<script src="<?php echo base_url(); ?>scripts/custom/get_usernames.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
<style>
    .err {
        color: red !important;
    }

    svg {
        margin-right: 10px;
        vertical-align: text-top;
        margin-top: 1px;
    }
</style>
<?php $this->load->view('sidebar/sidebar.php'); ?>
<div class="white-area-content separator page-right bg-page" >
    <div class="db-header clearfix line-area">
        <div class="page-header-title setting-area"> <span class="glyphicon glyphicon-cog"></span> <?php echo lang("ctn_224") ?></div>
        <div class="db-header-extra"></div>
    </div>
    <ol class="breadcrumb edit-home">
        <li><a href="http://lawsmos.com/social/">Home</a></li>
        <li class="active">Edit Profile</li>
    </ol>
    <?php if (isset($coreprofile['profile_picture'])&&!empty($coreprofile['profile_picture'])) { ?>
        
    <div class="card" style="min-height: 150px;
    position: relative;">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">HELP OUR SYSTEMS IDENTIFY: <button style="float:right;"><a href="<?php echo base_url('edit_student_profile?step=1'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table table-bg">
                <thead>
                    <tr>
                        <th>Are you currently a student enrolled in a university/ college:</th>
                        <th>Date of Birth</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php
                            // $getprofile = $getprofile->row_array();
                            if ($coreprofile['student_enrolled'] == "1") {
                                echo "Yes";
                            } elseif ($coreprofile['student_enrolled'] == "2") {
                                echo "No";
                            }  else {
                                echo "N/A";
                            }

                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['date_of_birth'])) {
                                echo $coreprofile['date_of_birth'];
                            } else {
                                echo "N/A";
                            }
                           
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
    </div>
    
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg ">Education Information <button style="float:right;"><a href="<?php echo base_url('edit_student_profile?step=2'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table bg-area">
                <tbody>
                    <tr class="table-sub-title table-area">
                        <td colspan="2">School & High School</td>
                    </tr>
                    <tr>
                        <th>School:10</th>
                        <th>School:12</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['school_10'])) {
                                echo $coreprofile['school_10'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['school_12'])) {
                                echo $coreprofile['school_12'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>

                    <tr>
                        <th>University:</th>
                        <th>University:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['board_10'])) {
                                echo $coreprofile['board_10'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['board_12'])) {
                                echo $coreprofile['board_12'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Year of completion:</th>
                        <th>Year of completion:</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if (!empty($coreprofile['year_pass_10'])) {
                                echo $coreprofile['year_pass_10'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($coreprofile['year_pass_12'])) {
                                echo $coreprofile['year_pass_12'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php
                                if ($coreprofile['x_marks_type'] == 1) {
                                    echo "Percentage : ";
                                } elseif ($coreprofile['x_marks_type'] == 2) {
                                    echo "Division : ";
                                } elseif ($coreprofile['x_marks_type'] == 2) {
                                    echo "CGPA : ";
                                } else {
                                    echo "Marks :";
                                }
                            ?>
                        </th>
                        <th>
                            <?php
                                if ($coreprofile['xii_marks_type'] == 1) {
                                    echo "Percentage : ";
                                } elseif ($coreprofile['xii_marks_type'] == 2) {
                                    echo "Division : ";
                                } elseif ($coreprofile['xii_marks_type'] == 2) {
                                    echo "CGPA : ";
                                } else {
                                    echo "Marks :";
                                }
                            ?>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            if ($coreprofile['x_marks_type'] == 1) {
                                echo  $coreprofile['marks_x_percentage'];
                            } elseif ($coreprofile['x_marks_type'] == 2) {
                                if ($coreprofile['x_division'] == 1) {
                                    echo "1st";
                                } elseif ($coreprofile['x_division'] == 2) {
                                    echo "2nd";
                                } elseif ($coreprofile['x_division'] == 3) {
                                    echo "3rd";
                                } elseif ($coreprofile['x_division'] == 4) {
                                    echo "Gold Medalist";
                                } else {
                                    echo "N/A";
                                }
                            } elseif ($coreprofile['x_marks_type'] == 2) {
                                echo $coreprofile['marks_x_cgpa'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($coreprofile['xii_marks_type'] == 1) {
                                echo $coreprofile['marks_xii_percentage'];
                            } elseif ($coreprofile['xii_marks_type'] == 2) {
                                if ($coreprofile['xii_division'] == 1) {
                                    echo "1st";
                                } elseif ($coreprofile['xii_division'] == 2) {
                                    echo "2nd";
                                } elseif ($coreprofile['xii_division'] == 3) {
                                    echo "3rd";
                                } elseif ($coreprofile['xii_division'] == 4) {
                                    echo "Gold Medalist";
                                } else {
                                    echo "N/A";
                                }
                            } elseif ($coreprofile['xii_marks_type'] == 2) {
                                echo $coreprofile['marks_xii_cgpa'];
                            } else {
                                echo "N/A";
                            }
                            ?>
                        </td>
                    </tr>

                    <?php
                    $professional_qualification_count = 0;
                    if (!empty($coreprofile['degree_array'])) {
                        $degree_array = json_decode($coreprofile['degree_array']);
                        $yearstart_array = json_decode($coreprofile['yearstart_array']);
                        $yearend_array = json_decode($coreprofile['yearend_array']);
                        $college_array = json_decode($coreprofile['college_array']);
                        $university_array = json_decode($coreprofile['university_array']);
                        $division_array = json_decode($coreprofile['division_array']);
                        $upload_marksheet_array = [];
                        $professional_qualification_count = count($degree_array);
                        // echo "hello1";
                        // print_r($upload_marksheet_array);
                        // die("hello");
                    }
                    if ($professional_qualification_count > 0) {
                        for ($i = 0; $i < $professional_qualification_count; $i++) {
                    ?>

                            <tr class="table-sub-title">
                                <td colspan="2">Professional Qualification <?php echo $i + 1; ?></td>
                            </tr>
                            <tr>
                                <th>Degree/ Course Name:</th>
                                <th>College Name:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($degree_array[$i])) {
                                        echo $degree_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($college_array[$i])) {
                                        echo $college_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>University Name:</th>
                                <th>Start Year:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($university_array[$i])) {
                                        echo $university_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($yearstart_array[$i])) {
                                        echo $yearstart_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>End Year:</th>
                                <th>Division:</th>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    if (!empty($yearend_array[$i])) {
                                        echo $yearend_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if (!empty($division_array[$i])) {
                                        // echo "Division : ";
                                        if ($division_array[$i] == 1) {
                                            echo "1st";
                                        } elseif ($division_array[$i] == 2) {
                                            echo "2nd";
                                        } elseif ($division_array[$i] == 3) {
                                            echo "3rd";
                                        } elseif ($division_array[$i] == 4) {
                                            echo "Gold Medalist";
                                        } else {
                                            echo "N/A";
                                        }
                                        // echo $division_array[$i];
                                    } else {
                                        echo "N/A";
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php if (!empty($upload_marksheet_array[$i])) { ?>
                                <tr>
                                    <th>
                                        Marksheet:
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('uploads/' . $upload_marksheet_array[$i]) ?>" width="200px">
                                    </td>
                                </tr>
                    <?php
                            }
                        }
                    }
                                         ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg ">Training And Professional Experience : <button style="float:right;"><a href="<?php echo base_url('edit_student_profile?step=3'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <?php
            $all_results = "";
            if (!empty($coreprofile['training_name_of_organization'])) {
                $all_results = json_decode($coreprofile['training_name_of_organization']);
            }
            $row_count = 0;
            if (is_array($all_results)) {
                $row_count = count($all_results);
            }
            ?>
            <?php
            if ($row_count > 0) {
                $experience_organization_name = json_decode($coreprofile['training_name_of_organization']);
                $experience_city = json_decode($coreprofile['training_city']);
                $experience_court = json_decode($coreprofile['training_court']);
                $experience_designation = [];
                $experience_start_date = json_decode($coreprofile['training_start_date']);
                $experience_end_date = json_decode($coreprofile['training_end_date']);

                $RolesLearnings = json_decode($coreprofile['role_learning']);
                $internship_validation = json_decode($coreprofile['internship_validation']);


                $contact_person_name = json_decode($coreprofile['contact_person_name']);
                $contact_person_designation = json_decode($coreprofile['contact_person_designation']);

                $contact_person_phone_no = json_decode($coreprofile['contact_person_phone_no']);
                $contact_person_email_address = json_decode($coreprofile['contact_person_email_address']);


                $research_title = json_decode($coreprofile['research_title']);
                $research_journal_name = json_decode($coreprofile['research_journal_name']);

                $research_issn_number = json_decode($coreprofile['research_issn_number']);
                $research_share_link = json_decode($coreprofile['research_share_link']);
                $fields_of_interest = json_decode($coreprofile['fields_of_interest']);
                $career_interest = json_decode($coreprofile['career_interest']);
                for ($i = 0; $i < $row_count; $i++) {
            ?>

                    <table class="table borderless-table bg-area">
                        <tr>
                            <th>Name of organization</th>
                            <th>City</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($experience_organization_name[$i])) {
                                    echo $experience_organization_name[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($experience_city[$i])) {
                                    echo getCityNAme($experience_city[$i]);
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Court</th>
                            <th>Designation</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($experience_court[$i])) {
                                    echo getCourtName($experience_court[$i]);
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($experience_designation[$i])) {
                                    if ($experience_designation[$i] == "1") {
                                        echo "Designated Senior";
                                    } elseif ($experience_designation[$i] == "2") {
                                        echo "Retired Justice";
                                    } else {
                                        echo "N/A";
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($experience_start_date[$i])) {
                                    echo $experience_start_date[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($experience_end_date[$i])) {
                                    echo $experience_end_date[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                         <tr>
                            <th>Roles & Learnings</th>
                            <th>internship Validation</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($RolesLearnings[$i])) {
                                    echo $RolesLearnings[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($internship_validation[$i])) {

                                    if($internship_validation[$i]){
                                         echo 'Yes';
                                    }
                                   
                                    else{
                                     echo 'No';   
                                    }
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Contact Person's Name</th>
                            <th>Designation </th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($contact_person_name[$i])) {
                                    echo $contact_person_name[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($contact_person_designation[$i])) {

                                    echo $contact_person_designation[$i];
                                      
                                   
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Phone no</th>
                            <th>Email Address </th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($contact_person_phone_no[$i])) {
                                    echo $contact_person_phone_no[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($contact_person_email_address[$i])) {

                                   echo $contact_person_email_address[$i];
                                       
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>

                         <tr>
                            <th>Research Title</th>
                            <th> Research Journal Name </th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($research_title[$i])) {
                                    echo $research_title[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($research_journal_name[$i])) {

                                   echo $research_journal_name[$i];
                                       
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>

                         <tr>
                            <th>ISSN Numbar</th>
                            <th>Share Link </th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                if (!empty($research_issn_number[$i])) {
                                    echo $research_issn_number[$i];
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($research_share_link[$i])) {

                                   echo $research_share_link[$i];
                                       
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
            <?php
                }
            }
          

            ?>
        </div>
    </div>
    position: relative;">
   <div class="card" style="min-height: 150px">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">Interests: <button style="float:right;"><a href="<?php echo base_url('edit_student_profile?step=4'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table table-bg">
                <thead>
                    <tr>
                        <th>Fields of interest</th>
                        <th>Career Interests</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                           <?php
                                if (!empty($fields_of_interest)) {
                                    $foi = array();
                                    foreach ($fields_of_interest as  $value) {
                                        $foi[] = getCategoryName($value);
                                    }
                                    echo implode(',', $foi);

                                   // echo $fields_of_interest[$i];
                                       
                                } else {
                                    echo "N/A";
                                }
                                ?>
                        </td>
                        <td>
                            <?php
                             $arryofint = ['','Independent Lawyer','Work with a law firm(Business Law)','Work with a law firm(Litigation)','Work with a Corporate(In-House Counsel)','Judicial Services','Academics','Other'];
                           if (!empty($career_interest)) {
                                $foi = array();
                                    foreach ($career_interest as  $value) {
                                         if (!empty($value)) {
                                            $foi[] = $arryofint[$value];
                                        }
                                    }
                                    echo implode(',', $foi);
                                  
                                       
                                } else {
                                    echo "N/A";
                                }
                           
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
    </div>
    <div class="card">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">Pictures: <button style="float:right;"><a href="<?php echo base_url('edit_student_profile?step=5'); ?>">Edit</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table table-bg">
                <thead>
                    <tr>
                        <th>Profile Picture</th>
                        <th>Cover Photo</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                           <?php
                                if (!empty($coreprofile['profile_picture'])) {
                                   echo '<img width="400px" src="'.base_url('uploads/'.$coreprofile['profile_picture']).'">';
                                       
                                } else {
                                    echo "N/A";
                                }
                                ?>
                        </td>
                        <td>
                            <?php
                           if (!empty($coreprofile['cover_photo'])) {
                                   echo '<img width="400px" src="'.base_url('uploads/'.$coreprofile['cover_photo']).'">';
                                       
                                } else {
                                    echo "N/A";
                                }
                                ?>
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
        <?php
    }else{
        ?>


    <div class="card" style="min-height: 150px;
    position: relative;">
        <div class="card-head">
            <h5 class="panel-subheading panal-bg">Your profile is not completed: <button style="float:right;"><a href="<?php echo base_url('profile'); ?>">Complete Profile</a></button></h5>
        </div>
        <div class="card-body">
            <table class="table borderless-table table-bg">
                <thead>
                    <tr>
                        <th>No record found</th>
                        
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
            
        </div>
    </div>
     <?php
    } ?>
    </div>